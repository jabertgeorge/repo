/**
 * Created by hemanshu on 12/4/2018
 */
import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CustomerDetails, VerifyChanges} from "../model.component";
import {KYCService} from "../service/main.service";
import {ModalComponent} from "ng2-bs3-modal/ng2-bs3-modal";

declare var swal: any;

@Component({
    selector: 'search',
    template: require('./search-page.template.html')

})

export class KYCSearchComponent implements OnInit {

    constructor(private router: Router, private route: ActivatedRoute, private appService: KYCService) {
    }

    @ViewChild('authModal') authModal: ModalComponent;
    customerId: string;
    details: CustomerDetails = new CustomerDetails();
    delta: any;
    changes: any;
    remarks: string;

    verifyChangesSubmit: VerifyChanges = new VerifyChanges();

    ngOnInit() {
        this.getDeltaInfo();
    }

    getDeltaInfo() {
        this.appService.getDeltaChanges().subscribe((info: any) => {
            if(info)
            this.delta = info.delta;
        }, (err: any) => {

        });
    }

    /*Search the Customer Data service call*/
    searchCustomer() {
        if (this.customerId && this.customerId != "" ) {
            this.appService.searchKYCDetail(this.customerId).subscribe((info: any) => {
                console.log(typeof (info) == "object");
                if (typeof (info) == "object") {
                    this.appService.setCustomerDetails(info);
                    this.appService.setCustomerId(this.customerId);
                    this.router.navigate(['./kyc-result'], {relativeTo: this.route, skipLocationChange: true});
                } else if (info == undefined) {
                    swal({
                        title: "Customer not found",
                        type: "error",
                        text: "Enter Valid Customer id"
                    });
                }
            }, (err) => {
                swal({
                    title: err._body,
                    type: "error",
                    text: "Error Occurred while searching"
                })

            });
        }

    }

    keyDownListener(e: any) {
        // If Enter is clicked
        // and something is selected
        if (e.keyCode == 13 && this.customerId != '') {
            this.searchCustomer();
        }
    }


    openModal() {
        this.authModal.open();
    }


    closeModal() {
        this.authModal.close();
    }

    displayString(val: any) {
        if (val["map"]) {
            delete val["map"];
        }
        return JSON.stringify(val);
    }

    setChangeValue(val: any) {
        this.changes = val;
        this.openModal();
    }

    verifyChange(id: string, type: string, decision: string) {

        this.verifyChangesSubmit = new VerifyChanges();
        this.verifyChangesSubmit.custId = id;
        this.verifyChangesSubmit.updateType = type;
        this.verifyChangesSubmit.isAccepted = decision;
        this.verifyChangesSubmit.remarks = this.remarks;

        let that = this;
        if (decision== 'YES') {
            swal({
                title: "Are you sure?",
                text: "You want to Authorise the following changes !",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Authorise it!",
                cancelButtonText: "No, cancel !"
            }).then(() => {
                this.appService.verifyChanges(this.verifyChangesSubmit).subscribe((info) => {
                    swal("Authorised !", "User changes has been Authorised.", "success");
                    that.closeModal();
                    that.remarks = "";
                    that.getDeltaInfo();
                }, (error: any) => {
                    swal({
                        title: 'Error', type: 'warning', text: error._body
                    });
                    that.closeModal();
                    that.remarks = "";
                });
            }, () => {
                swal("Cancelled", "User Changes Not Authorised", "error");
            });
            that.closeModal();
            that.remarks = "";
        }
        else {

            swal({
                title: "Are you sure?",
                text: "You want to Reject the following changes !",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Reject it!",
                cancelButtonText: "No, cancel !"
            }).then(() => {
                this.appService.verifyChanges(this.verifyChangesSubmit).subscribe((info) => {
                    that.getDeltaInfo();
                    swal("Rejected !", "User Changes has been Rejected.", "success");
                    that.closeModal();
                    that.remarks = "";
                }, (error: any) => {
                    swal({
                        title: 'Error', type: 'warning', text: error._body
                    });
                    that.closeModal();
                    that.remarks = "";
                });
            }, () => {
                swal("Cancelled", "User Changes Not Rejected", "error");
                that.closeModal();
                that.remarks = "";
            });

        }
    }

}
