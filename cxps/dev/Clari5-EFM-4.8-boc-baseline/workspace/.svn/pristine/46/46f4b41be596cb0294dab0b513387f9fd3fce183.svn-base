package clari5.str;

import clari5.aml.cms.newjira.DefaultSTRHandler;
import clari5.aml.cms.newjira.ISTRHandler;
import clari5.aml.commons.*;
import clari5.hfdb.*;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.platform.util.CxJson;
import clari5.platform.util.CxPrintLogger;
import clari5.report.db.StrReport;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomSTRHandler implements ISTRHandler {
    private static final CxpsLogger logger = CxpsLogger.getLogger(DefaultSTRHandler.class);

    @Override
    public String getCustId(RDBMSSession session, Incident incident) {
        String acctId = getAccountId(incident);
        List<String> accIds = new ArrayList<>();
        accIds.add(acctId);
        List<AmlAccountView> accountViews = Hfdb.getAccounts(session, accIds);
        AmlAccountView accountView = accountViews.get(0);

        assert accountView != null;
        return accountView.getCustId();
    }

    @Override
    public String getAccountId(Incident incident) {
        if (incident == null)
            logger.error("Received caseEvent is null for STR, hence accountId cannot be extracted.");
        else {
            CmsSuspicionDetails details = null;
            try {
                details = CxJson.json2obj(incident.suspicionDetails, CmsSuspicionDetails.class);
            } catch (IOException e) {
                CxPrintLogger.printExp(e);
            }
            if (details == null)
                logger.error("Received cmsSuspicionDetails as null for STR, hence accountId cannot be extracted.");
            else {
                List<CmsSuspicionDetailsAccount> list = details.getAccountsDetails();
                if (list == null || list.isEmpty())
                    logger.error("Received cms account details as null/empty for STR, hence accountId cannot be extracted.");
                else {
                    String accountId = list.get(0).getAccountId();
                    if (accountId == null)
                        throw new RuntimeException("Received accountId as null while drafting an STR.");

                    return accountId;
                }
            }
        }

        throw new RuntimeException("AccountID is found as null while drafting an STR");
    }

    @Override
    public boolean populateStrReport(RDBMSSession session, CmsLuw cmsLuw, Incident incident, String s) throws RdbmsException {
        CaseEvent caseEvent = cmsLuw.caseEvent;
        CmsSuspicionDetails cms = null;
        try {
            cms = CxJson.json2obj(incident.suspicionDetails, CmsSuspicionDetails.class);
        } catch (IOException e) {
            CxPrintLogger.printExp(e);
        }

        String acctId = getAccountId(incident);
        List<String> accIds = new ArrayList<>();
        accIds.add(acctId);
        List<AmlAccountView> accountViews = Hfdb.getAccounts(session, accIds);
        AmlAccountView accountView = accountViews.get(0);

        assert accountView != null;
        String custID = getCustId(session, incident);

        AmlCustomerViewKey key = new AmlCustomerViewKey(session);
        key.setCustId(custID.trim());
        AmlCustomerView customerView = key.select();

        CmsSuspicionDetailsGroundsOfSuspicion cmsSuspicionDetailsGroundsOfSuspicion = cms.getGroundsOfSuspicion();
        StrReport strReport = new StrReport(session);

        CmsSuspicionDetailsAccount cmsSuspicionDetailsAccounts = cms.getAccountsDetails().get(0);

        if (cmsSuspicionDetailsAccounts != null && cmsSuspicionDetailsAccounts.getTransactions() != null && cmsSuspicionDetailsAccounts.getTransactions().size() > 0) {
            CmsSuspicionDetailsAccountTransaction tran = cmsSuspicionDetailsAccounts.getTransactions().get(0);

            strReport.setNoTransactionToBeReported(cmsSuspicionDetailsAccounts.getNoTransactionToBeReported());
            strReport.setTransactionId(tran.getTransactionId());
            strReport.setRelatedAcct(tran.getRelatedAcct());
            strReport.setAdditionalRemarks(tran.getAdditionalRemarks());

        }

        //strReport.setStrId(UUID.randomUUID().toString().replace("-", ""));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-YYYY.HHmm");
        String date = dateFormat.format(new Date(incident.closedDate.getTime()));
        strReport.setStrId("STR-" + date.toUpperCase() + "-" + acctId);
        strReport.setAccountId(acctId);

        if (cmsSuspicionDetailsGroundsOfSuspicion != null) {
            strReport.setRelationshipBeginDate(cmsSuspicionDetailsGroundsOfSuspicion.getRelationshipBeginDate());
            strReport.setBeneficiaryDetails(cmsSuspicionDetailsGroundsOfSuspicion.getBeneficiaryDetails());
            strReport.setVolumeOfTransactions(cmsSuspicionDetailsGroundsOfSuspicion.getVolumeOfTransactions());
        }

        strReport.setFactName(incident.factName);
        strReport.setRiskLevel(incident.riskLevel);
        strReport.setCaseEntityId(incident.caseentityId);
        strReport.setEntityIdType(incident.entityIdType);
        strReport.setIncidentScore(incident.incidentScore);
        strReport.setEntityName(incident.entityName);
        strReport.setIncidentClosedDate((new Date(incident.closedDate.getTime())));
        strReport.setEntityId(incident.entityId);
        strReport.setCaseEntityName(incident.caseentityName);
        strReport.setCaseDisplayKey(incident.casedisplaykey);
        strReport.setNoOfStr(incident.noOfStr);
        strReport.setLastFiledStr(incident.lastFiledStr);

        strReport.setCustIdNum(customerView.getIdNum());
        strReport.setCustFax(customerView.getFax());
        strReport.setProfessionType(customerView.getProfessionType());
        strReport.setNatureOfBusiness(customerView.getNatureOfBusiness());
        strReport.setCustType(customerView.getCustType());
        strReport.setCustGender(customerView.getGender());
        strReport.setCustMobile(customerView.getMobile1());
        strReport.setCustEmail(customerView.getEmail());
        strReport.setCustMailingAddr(customerView.getMailingAddr());
        strReport.setCustResAddr(customerView.getResAddr() + "," + customerView.getNationality());
        strReport.setCustResAddrCity(customerView.getResAddrCity());
        strReport.setCustResAddrState(customerView.getResAddrState());
        strReport.setCustResAddrCountry(customerView.getResAddrCountry());
        strReport.setCustDOB(customerView.getDob());
        strReport.setCustName(customerView.getName());
        strReport.setCreatedOn((new Timestamp(System.currentTimeMillis())));
        strReport.setCustNationality(customerView.getNationality());

        strReport.setAcctPassNo(customerView.getCustPassportNo());
        //TODO business Type
        strReport.setAcctBussinessType("");
        strReport.setAcctOccupation(customerView.getOccupation());
        //TODO employer name durationRelationShip mapping
        strReport.setAcctEmployerName("");
        strReport.setAcctTelephone(customerView.getMobile1());
        strReport.setAcctDurationRelationshipCust("");

        strReport.setAccountType(accountView.getAcctCurrType());
        strReport.setOpeningDate(accountView.getOpenedDate());

        AmlBranchViewKey amlBranchViewKey = new AmlBranchViewKey(session);
        amlBranchViewKey.setBranchId(accountView.getBranchId());

        AmlBranchView branchView = amlBranchViewKey.select();
        if (branchView != null) {
            strReport.setBranchName(branchView.getName());
            strReport.setBranchAddress(branchView.getAddress());
        }
        strReport.setAcctStatus(accountView.getStatus());
        strReport.setCurrBalance(accountView.getBalance());
        strReport.setCurrencyCode(accountView.getCurrency());
        try {
            strReport.insert(session);
            List<String> eventId = getEventIdFromIncident(session.getCxConnection(), strReport, incident);
            ISTRHandler istrHandler = new CustomSTRHandler();
            insertQuery(session.getCxConnection(), strReport,
                    istrHandler.getAccountId(incident),
                    istrHandler.getCustId(session, incident),
                    (eventId.get(0) != null || !eventId.get(0).isEmpty()) ?
                            eventId.get(0) : "",
                    getTransactionCountfromEvidence(eventId.get(1).startsWith("{") &&
                            eventId.get(1).endsWith("}") ?
                            eventId.get(1) : ""),
                    sumOfTranAmt(session.getCxConnection(),
                            populateEventIds(eventId.get(1).startsWith("{") &&
                                    eventId.get(1).endsWith("}") ?
                                    eventId.get(1) : "")),
                    incident);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static double sumOfTranAmt(CxConnection connection, String eventIds) {

        if (eventIds == null || eventIds.isEmpty()) return 0.0;

        double sumOfTranAmt = 0.0;
        String sql = "select SUM(tran_amt) AS sumOfAmount from\n" +
                "  (\n" +
                "    select txn_amt as tran_amt from EVENT_FT_PAWNING where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select txn_amt as tran_amt from EVENT_FT_LEASE where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select txn_amt as tran_amt from EVENT_FT_CC where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select tran_amt as tran_amt from EVENT_FT_REMITTANCE where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select fcy_tran_amt as tran_amt from EVENT_FT_REMITTANCE where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select tran_amt as tran_amt from EVENT_FT_REMITTANCE where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select tran_amt as tran_amt from EVENT_FT_LOANTXN where event_id in (" + eventIds + ")\n" +
                "    UNION ALL\n" +
                "    select txn_amt as tran_amt from EVENT_FT_ACCOUNT_TXN where event_id in (" + eventIds + ")\n" +
                "  ) AS tran_amt_sum";

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    sumOfTranAmt = rs.getDouble("sumOfAmount");
                }
            }
            return sumOfTranAmt;
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return sumOfTranAmt;
    }

    public static String populateEventIds(String evidence) {

        if (evidence == null || evidence.isEmpty()) return null;

        String eventIds = "";
        JSONObject jsonObject = new JSONObject(evidence);
        if (jsonObject.has("evidenceData")) {
            JSONArray jsonArray = jsonObject.getJSONArray("evidenceData");
            for (int i = 0; i < jsonArray.length(); i++) {
                eventIds = Arrays.toString(new JSONArray[]{((JSONObject) jsonArray.get(i)).getJSONArray("eventIds")}).replaceAll("\"", "'").replaceAll("\\[", "").replaceAll("\\]", "");
            }
            logger.debug("set of eventid's: " + eventIds);
        }
        return eventIds != null ? eventIds : "";
    }

    public static double getTransactionCountfromEvidence(String evidence) {

        if (evidence == null || evidence.isEmpty()) return 0.0;

        double frequencyOfTxn = 0.0;
        JSONObject jsonObject = new JSONObject(evidence);
        if (jsonObject.has("evidenceData")) {
            JSONArray jsonArray = jsonObject.getJSONArray("evidenceData");
            for (int i = 0; i < jsonArray.length(); i++) {
                frequencyOfTxn += ((JSONObject) jsonArray.get(i)).getJSONArray("eventIds").length();
            }
            logger.debug("Frequency of txn: " + frequencyOfTxn);
        }
        return frequencyOfTxn;
    }

    public static List<String> getEventIdFromIncident(CxConnection connection, StrReport strReport, Incident incident) {
        List<String> incidentList = new ArrayList<>();
        String eventId = null;
        String sql = "select EVENT_ID, EVIDENCE from CL5_INCIDENT where JIRA_INCIDENTID = '" + incident.jiraIncidentid + "'";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery();) {
                while (rs.next()) {
                    incidentList.add(0, rs.getString("EVENT_ID") != null ? rs.getString("EVENT_ID") : "");
                    incidentList.add(1, rs.getString("EVIDENCE") != null ? rs.getString("EVIDENCE") : "");
                }
            }
            return incidentList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return incidentList;
    }

    public static void insertQuery(CxConnection connection, StrReport strReport,
                                   String acctId, String custId, String eventId,
                                   double tranCount, double sumOfTranAmt, Incident incident) {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO CUSTOM_STR_DATA (JIRA_INCIDENTID, " +
                "EVENT_ID, TRAN_COUNT, TOTAL_AMT, ACCTID, CUSTID, MNEMONIC, str_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
            int i = 1;
            ps.setString(i++, incident.jiraIncidentid);
            ps.setString(i++, eventId != null ? eventId : "");
            ps.setDouble(i++, tranCount);
            ps.setDouble(i++, sumOfTranAmt);
            ps.setString(i++, acctId != null ? acctId : "");
            ps.setString(i++, custId != null ? custId : "");
            ps.setString(i++, eventId.substring(0, 2));
            ps.setString(i++, strReport.getStrId() != null ? strReport.getStrId() : "");
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}