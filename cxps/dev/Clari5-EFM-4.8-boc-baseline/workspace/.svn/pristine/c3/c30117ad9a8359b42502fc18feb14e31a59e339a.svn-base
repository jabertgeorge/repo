package clari5.custom.boc.integration.builder.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.json.JSONObject;

import clari5.custom.boc.integration.db.DBTask;
import cxps.apex.utils.CxpsLogger;

public class VarSource  implements Cloneable {
	// This is added just to keep track of duplicates. Added later, else will have to make major changes.
	public static CxpsLogger logger = CxpsLogger.getLogger(Message.class);
	private String name;
	private String source;
	private String column;
	private String value;
	private List<String> keys;
	private List<String> where;
	// If there could be duplicate rows for the column selected for the key\where set.
	private boolean hasDuplicateRows = false;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getColumn() {
		return column;
	}
	// As of now column level duplication is only supported based on one row.
	public void setColumn(String column) {
		if(column.startsWith("duplicate-row")) {
			this.hasDuplicateRows = true;
			this.column = column.split("\\{")[1].replace("}", "");
		} else {
			this.column = column;
		}
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public List<String> getKeys() {
		return keys;
	}
	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
	public List<String> getWhere() {
		return where;
	}
	public void setWhere(List<String> where) {
		this.where = where;
	}
	public boolean hasDuplicateRows() {
		return hasDuplicateRows;
	}
	public void setHasDuplicateRows(boolean hasDuplicateRows) {
		this.hasDuplicateRows = hasDuplicateRows;
	}
	public VarSource clone() throws CloneNotSupportedException {
		return (VarSource) super.clone();
	}
	// This needs to be improved with a standard logic. Worst code written till now.
	// This is not proper logic,  doing this just to meet timelines. To modify.
	public String process(JSONObject jrow) throws Exception {
		String processedOutput = null;
		String valuesAfterProcessing = null;
		try {
			if(this.source.equals("Row")) {
				processedOutput=processRowAsSource(jrow);
			}
			//Commenting this part as of now coz now the entire json message will be
			//fetched from one single table
			/*else if(this.source.split("\\.")[0].equals("db")) {
				processedOutput = processRowAsDB(jrow);
			} else if(this.source.equals("") && !this.value.equals("")) {
				processedOutput = new ArrayList<String>();
				processedOutput.add(this.value);
			}*/
			valuesAfterProcessing = processValues(processedOutput, jrow); 
		} catch(Exception e) {
			throw new Exception("Exception: while processing row, "+jrow.getString("tableName")+" for values, "+this.toString()+ 
					". "+e.getMessage());
		}
		return valuesAfterProcessing;
	}
	private String processValues(String val, JSONObject jrow) throws Exception {
		//List<String> processedValues = new ArrayList<String>();
		//for(String value : values) {
		String convertedValue = val;
		try {
			String[] tokens = this.value.split("\\{");
			if (tokens.length > 1) {
				String token = tokens[1].replace("}", "").replaceAll("\"", "");
				switch (tokens[0].toLowerCase()) {
					case "direct": {
						// Conditions will be evaluated from left to right.
						String[] cases = token.split("\\,");
						for (String s : cases) {
							String[] mappings = s.split("\\:");
							String[] rule = mappings[0].split("\\|");
							String check = "";
							if (rule[0].trim().startsWith("Row")) {
								check = jrow.getString(rule[0].split("\\.")[1]);
							} else if (rule[0].trim().equals("this")) {
								check = value;
							}
							if (rule[1].trim().equals("EQ")) {
								if (check.equals(rule[2])) {
									if (mappings[1].startsWith("Row")) {
										convertedValue = jrow.getString(mappings[1].split("\\.")[1]);
									} else {
										convertedValue = mappings[1];
									}
								}
							} else if (rule[1].equals("NOT_EQ")) {
								if (!check.equals(rule[2])) {
									if (mappings[1].startsWith("Row")) {
										convertedValue = jrow.getString(mappings[1].split("\\.")[1]);
									} else {
										convertedValue = mappings[1];
									}
								}
							}
						}
						break;
					}
					case "properties": {
						// Tried with properties but space is an issue. Escaping it was risky.
						Map<String, String> properties = new HashMap<String, String>();
						String[] keysvaluepairs = value.split("\\,");
						for (String kvp : keysvaluepairs) {
							// Sometimes empty strings come from source.
							if (kvp != null && !kvp.trim().equals("")) {
								String[] keyvaluepair = kvp.split(":");
								properties.put(keyvaluepair[0].trim(), keyvaluepair[1]);
							}
						}
						convertedValue = (String) properties.get(token);
						break;
					}
					case "split": {
						String[] keyAndCount = token.split("\\:");
						if (Integer.parseInt(keyAndCount[1]) < 1) {
							convertedValue = value.split(Pattern.quote(keyAndCount[0]))[1];
						} else {
							convertedValue = value.split(Pattern.quote(keyAndCount[0]))[1].substring(0, Integer.parseInt(keyAndCount[0]) - 1);
						}
						break;
					}
					case "convertdate": {
						try {
							String[] formats = token.split("\\,");
							Date date = new SimpleDateFormat(formats[0].trim()).parse(val);
							convertedValue = new SimpleDateFormat(formats[1].trim()).format(date);
							break;
						}catch (Exception e){
//							System.out.println("Exception while formatting date "+this.column+" for eventid"+jrow.getString("event_id")+" returning blank value");
							logger.debug("Exception while formatting date "+this.column+" for eventid"+jrow.getString("event_id")+" returning blank value");
							return "";
						}
					}
				}
			}
		}catch (Exception e){

			logger.info("Exception while processing value returning original value "+val);
		}
			//processedValues.add(convertedValue);
		//}
		return convertedValue;
	}
	private String processRowAsSource(JSONObject jrow) throws Exception {
		// Check if need to pick the whole string.
		String output = "";
		String temp = "";
		String key=this.column;
		/*for(String s : this.column.split("\\+")) {
			String splits[] = s.split("\\{");
			if(splits.length > 1 && splits[1] != null) {
				char c = splits[1].charAt(0);
				if(c == 'F') {
					temp = getFromBean(jrow, splits[0]).substring
							(0, Integer.valueOf(splits[1].replaceAll("F", "").replaceAll("\\}", "")));
				} else if(c == 'L') {
					temp = getFromBean(jrow, splits[0]).substring
							(Integer.valueOf(splits[1].replaceAll("L", "").replaceAll("\\}", "")));
				} else {
					throw new Exception("Invalid substring option given for row, "+jrow);
				}
			} else {
				temp = getFromBean(jrow, splits[0]);
			}
			output += temp;
		}*/
		temp = getFromBean(jrow, key);
		return temp;
	}
	private List<String> processRowAsDB(JSONObject jrow) throws Exception{
		List<String> output = new ArrayList<String>();
		List<String> tempKeys = new ArrayList<String>();
		tempKeys.add(this.keys.get(0));
		// Hate doing this.
		String[] opt = null;
		if(this.keys.size() > 1) {
			opt = this.keys.get(1).split(":");
			tempKeys.add(opt[0]);
		}
		List<String> evaluatedWhereList = new ArrayList<String>();
		for(String entry : where) {
			if(entry.split("\\.")[0].equals("Row")) {
				evaluatedWhereList.add(this.getFromBean(jrow, entry.split("\\.")[1]));
			} else {
				evaluatedWhereList.add(entry);
			}
		}
		List<String> result = DBTask.select(this.source.split("\\.")[1], this.column, tempKeys.toArray(new String[0]), 
				evaluatedWhereList.toArray(new String[0]));
		if(result != null && result.size() != 0) {
			output = result;
		} else {
			tempKeys.clear();
			if(opt != null && opt.length > 1) {
				tempKeys.add(this.keys.get(0));
				tempKeys.add(opt[1]);
				result = DBTask.select(this.source.split("\\.")[1], this.column, tempKeys.toArray(new String[0]), 
						evaluatedWhereList.toArray(new String[0]));
				output = result;
			}
		}
		return output;
	}
	private String getFromBean(JSONObject jrow, String key) {
		String value = " ";
		if(jrow.has(key)) {
			value = String.valueOf(jrow.get(key));
		}
		return value;
	}
	@Override
	public String toString() {
		return "Name: "+this.name+" Source: "+this.source+", Column: "+this.column;
	}
}