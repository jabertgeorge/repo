<scenario app="AML" desc="Alerts to be generated when the credit or debit transaction done in X1 calendar day is greater than N times of the average credit or debit transaction done on the last X2 months." focus="AML" group="AML" insertFact="N" name="AML_009_SUDDEN_SURGE_IN_ACTIVITY_SLC" rdaActionReqd="N" solnType="SERVICE" workspace="Account">
<def-query collection="FT_AccountTxnEvent" count="-1" countParam="_op._q2.ftr.cnt" desc="FT_AccountTxnEvent" durationEnd="30" durationStart="1" durationType="CAL" durationTypeParam="_op._q2.ftr.dr_typ" endParam="_op._q2.ftr.end" fromWS="Account" id="_q2" startParam="_op._q2.ftr.st" timeUnit="D" unitParam="_op._q2.ftr.tu">
<filter>
<and>
<cond lhs="part_tran_type" op="I_EQ" valParam="_op._q2.ftr.cond.and.0.part_tran_type">"C"</cond>
<or>
<cond lhs="Channel" op="I_EQ" valParam="_op._q2.ftr.cond.and.1.or.0.Channel">"CBS"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q2.ftr.cond.and.1.or.1.Channel">"IB"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q2.ftr.cond.and.1.or.2.Channel">"MB"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q2.ftr.cond.and.1.or.3.Channel">"DC"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q2.ftr.cond.and.1.or.4.Channel">"CDM"</cond>
</or>
<cond lhs="systemCountry" op="I_EQ" valParam="_op._q2.ftr.cond.and.2.systemCountry">"SL"</cond>
<view fromWS="Account" id="__CUSTOMER_VIEW" name="CUSTOMER_VIEW">
<cond lhs="custType" op="I_EQ" valParam="_op._q2.ftr.cond.and.3.__CUSTOMER_VIEW.custType">"P"</cond>
</view>
</and>
</filter>
<aggregate>
<avg over="tranAmt"/>
</aggregate>
</def-query>
<def-query collection="FT_AccountTxnEvent" count="-1" countParam="_op._q5.ftr.cnt" desc="FT_AccountTxnEvent" durationEnd="30" durationStart="0" durationType="CAL" durationTypeParam="_op._q5.ftr.dr_typ" endParam="_op._q5.ftr.end" fromWS="Account" id="_q5" startParam="_op._q5.ftr.st" timeUnit="D" unitParam="_op._q5.ftr.tu">
<filter>
<view fromWS="Account" id="__ENTITY_TAG" name="ENTITY_TAG">
<cond lhs="TAG" op="I_EQ" valParam="_op._q5.ftr.cond.__ENTITY_TAG.TAG">"White List"</cond>
</view>
</filter>
<aggregate/>
</def-query>
<def-query collection="FT_AccountTxnEvent" count="-1" countParam="_op._q1.ftr.cnt" desc="FT_AccountTxnEvent" durationEnd="0" durationStart="0" durationType="CAL" durationTypeParam="_op._q1.ftr.dr_typ" endParam="_op._q1.ftr.end" fromWS="Account" id="_q1" startParam="_op._q1.ftr.st" timeUnit="D" unitParam="_op._q1.ftr.tu">
<filter>
<and>
<cond lhs="part_tran_type" op="I_EQ" valParam="_op._q1.ftr.cond.and.0.part_tran_type">"C"</cond>
<or>
<cond lhs="Channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.0.Channel">"CBS"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.1.Channel">"IB"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.2.Channel">"MB"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.3.Channel">"DC"</cond>
<cond lhs="Channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.4.Channel">"CDM"</cond>
</or>
<cond lhs="systemCountry" op="I_EQ" valParam="_op._q1.ftr.cond.and.2.systemCountry">"SL"</cond>
<view fromWS="Account" id="__CUSTOMER_VIEW" name="CUSTOMER_VIEW">
<cond lhs="custType" op="I_EQ" valParam="_op._q1.ftr.cond.and.3.__CUSTOMER_VIEW.custType">"P"</cond>
</view>
</and>
</filter>
<aggregate>
<sum over="tranAmt"/>
</aggregate>
</def-query>
<def-opinion duration="7D" durationCountParam="_op.life.cnt" durationUnitParam="_op.life.tu" intuWt="1.0" intuition="AML_009_SUDDEN_SURGE_IN_ACTIVITY_SLC_INTU" wt="0.6" wtParam="_op.wt">
<condition>
<and>
<query id="_q2">
<and>
<cond lhs="count" op="GT" valParam="_op.mc.and.0._q2.and.0.count">50</cond>
<cond lhs="avg(tranAmt)" op="GTE" valParam="_op.mc.and.0._q2.and.1.avg(tranAmt)">50000</cond>
</and>
</query>
<query id="_q1">
<cond constParam="_op.mc.and.1._q1.sum(tranAmt).const" lhs="sum(tranAmt)" multParam="_op.mc.and.1._q1.sum(tranAmt).mult" op="GT">8.0*_q2.avg(tranAmt)+0</cond>
</query>
<query id="_q5">
<cond lhs="count" op="EQ" valParam="_op.mc.and.2._q5.count">0</cond>
</query>
</and>
</condition>
<messages>
<msg channelId="BRANCH">Sudden Surge in Activity for Account where the cumulative amount is $cumulative_Amount$ </msg>
</messages>
<evidence-data>
<field name="cumulative_Amount" type="Integer" value="_q1.sum(tranAmt)"/>
</evidence-data>
</def-opinion>
<def-addOn desc="Customer Risk Categorisation" name="_a1" wt="0.2" wtParam="_a1.wt">
<condition>
<and>
<view fromWS="Account" id="__ENTITY_FACT_VIEW" name="ENTITY_FACT_VIEW">
<cond lhs="risk_level" op="I_EQ" valParam="_a1.mc.and.0.__ENTITY_FACT_VIEW.risk_level">"L1"</cond>
</view>
<query id="_q1">
<cond lhs="count" op="GT" valParam="_a1.mc.and.1._q1.count">0</cond>
</query>
<query id="_q2">
<cond lhs="count" op="GT" valParam="_a1.mc.and.2._q2.count">0</cond>
</query>
</and>
</condition>
<messages>
<msg channelId="BRANCH">Customer Risk Categorisation </msg>
</messages>
</def-addOn>
</scenario>
