#!/usr/bin/env bash

echo "This script is currently present in [`pwd`]. It needs to be moved to the devsetup."
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
if grep -q ^docker /etc/group
then 
	:
else 
	sudo groupadd docker
fi
echo "calling systemctl"
sudo systemctl enable docker
echo "adding alok to docker group"

sudo usermod -aG docker "`whoami`"
#echo "calling newgrp"
#newgrp - docker
#echo "calling chown"
#if [ -d /home/"$USER"/.docker ]; then
#	sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
#	sudo chmod g+rwx "$HOME/.docker" -R
#fi
echo "calling hello-world"
docker run hello-world
echo "If this installation was successful, hello-world should have run successfully on your machine. If it didn't, please check the logs above."

sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
