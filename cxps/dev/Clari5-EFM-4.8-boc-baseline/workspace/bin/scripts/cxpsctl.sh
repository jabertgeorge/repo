#!/usr/bin/env bash
# ===========================================================
# This program can be used for bringing up/down the platform
# ===========================================================

unset header 2>/dev/null
header() {

    local _message="`echo $*`"
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf -- "${_tag}
  ${_message}
${_tag}
"
}

unset info 2>/dev/null
info() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

# Container dir
[ ! -e ${DEP_BASE}/../.deployed ] && err "ERROR: Unable to locate deployment" && exit 1
export CDIR=${DEP_BASE}/tomcat

# Define a startup for the container
_startup() {

    # Startup
    info "Container [startup] for version -> [$( cat ${DEP_BASE}/../.deployed )]"

    # Create working environment
    ${w}/bin/scripts/configure.sh --put || return 1

    # Set up environment for tomcat
    . ${w}/bin/scripts/configure.sh

    [ -e ${DEP_BASE}/env.sh ] \
        && chmod +x ${DEP_BASE}/env.sh \
        && . ${DEP_BASE}/env.sh
  
    export CXPS_DEPLOYMENT=${DEP_BASE}/product/caches
    export JAVA_OPTS="${CXPS_JAVA_OPTS} -Xms1g -Xmx2g"

    ( cd ${CDIR}/bin && ./start )
}

# Define a shutdown for the container
_shutdown() {

    # Shutdown
    info "Container [shutdown]"
    ( cd ${CDIR}/bin && ./stop )
}

_getpid() {
    local _pid

    [ ! -e ${CDIR}/pid.running ] && return
    local _pid=`ps -ef | grep -w $( cat ${CDIR}/pid.running ) | grep -v grep | awk '{print $2}'`
    echo ${_pid}
}

# Define a process status for the container
_status() {
    local _mode=$1
    local _pid

    # Status
    info "Container [status]"
    _pid=$( _getpid )
    [ -z "${_pid}" ] && echo "Process is down" \
        || echo "Process is running on PID [$( echo $_pid | tr ' ' ',' )]"
}

# Define a kill for the container
_kill() {
    local _pid _temp

    # kill
    info "Container [kill]"
    _pid=$( _getpid )
    if [ -z "${_pid}" ]; then
        echo "Nothing to kill"
    else
        ( cd ${CDIR}/bin && ./kill )
        # Give it some time to reflect the changes
        sleep 1
        _pid=$( _getpid )
        ( echo ${_pid} | grep -qw $( cat ${CDIR}/pid.running )) \
            && echo "Process could not be killed" \
            || echo "Killed process with PID = [$(cat ${CDIR}/pid.running)]"
    fi
}

# -- Main menu --
[ $# -lt 1 ] && info "`basename $0` <action>" && exit 1

case $1 in
    "start")
        _startup ;;
    "stop")
        _shutdown ;;
    "kill")
        _kill ;;
    "status")
        _status ;;
    *)
        err "ERROR: Mode not supported, only [start|stop|status|kill] are supported"
        ;;
esac
