#!/usr/bin/env bash
# ===========================================================================
# File: deploy.sh
# Author: Lovish
# ===========================================================================

# Main method to be executed
main() {

    # Get BASEDIR
    export BASEDIR=$( cd `pwd` && cd `dirname $0` && pwd )

    [ -z "${CL5_REL_NAME}" ] && err "ERROR: Unable to locate release name" && return 1
    [ -z "${CL5_WS_REP}" ] && err "ERROR: Local rep in workspace not defined" && return 1
    [ -z "${DEP_BASE}" ] && err "ERROR: Deployment base not defined" && return 1

    export DEP_AREA="$( dirname ${DEP_BASE} )/${CL5_REL_NAME}"

    [ ! -d "${CL5_WS_REP}" ] && err "Error: Assemble has not happened" && return 1
    [ -e ${MISSING_ASSEMBLY_FILE} ] && err "Error: Build components missing, run fbuild.sh" && return 1

    case $1 in
        "--select"|"--choose"|"--prompt"|"--enter"|"-e"|"-s"|"-c")
            export PROMPT_FOR_DEPLOY="prompt"
            ;;
        "-h")
            echo "USAGE: $(basename $0) [--select|--choose|--prompt|--enter|-e|-s|-c]"
            return 0
            ;;
    esac

    # Make sure container is down
    ( ${BASEDIR}/cxpsctl.sh kill >/dev/null 2>&1 )

    # Make sure you start with CXPS_WORKSPACE
    cd ${w}

    rm -fr ${DEP_BASE}
    rm -fr ${DEP_AREA}
    mkdir -p ${DEP_AREA}

    [ -e "$( dirname ${DEP_BASE} )/${CL5_REL_NAME}.filter" ] \
        && export FILTER="$( dirname ${DEP_BASE} )/${CL5_REL_NAME}.filter"

    # Process all the dependencies
    header "Performing deployment for [${CL5_REL_NAME}]"
    if [ -e ${CL5_WS_REP}/.assemble.error ]; then
        cat ${CL5_WS_REP}/.assemble.error
    else
        export CL5_WS_DREP=${CL5_WS_REP}-dev
        process_deployment || ( err "ERROR: Deployment failed" && return 1 )
    fi

    # Copy if any custom directories over the installation
    DEVCDIR=${w}/bin/releases/${CL5_REL_NAME}/data/depbase
    [ -e ${DEVCDIR} ] && cp -r ${DEVCDIR}/* ${DEP_AREA} >/dev/null 2>&1

    ln -fs ${DEP_AREA} ${DEP_BASE}
}

_deploy_apps() {

    local _wsrep=$1
    local _deprep=$2
    local _appdir=$3

    local _webinf _temp

    for _app in ${_deprep}/apps/*
    do
        _name=$( basename ${_app} .war )
        if [ "${PROMPT_FOR_DEPLOY}" = "prompt" ]; then
            read -p "DEPLOY [${_name}]? " _temp
            ( [ ! "${_temp}" = "y" ] && [ ! -z "${_temp}" ] ) && continue
        else
            if [ ! -z "${FILTER}" ]; then
                grep -wq ${_name} ${FILTER} || continue
            fi
        fi

        printf "Deploying app [${_name}] ... "
        mkdir -p ${_appdir}/webapps/${_name}

        [ ! -e ${_app} ] && echo "ERROR: App [${_name}] not found" && return 1
        ( cd ${_appdir}/webapps/${_name} && jar xf ${_app} )
        
        _webinf=${_appdir}/webapps/${_name}/WEB-INF

        if [ -e ${_wsrep}/meta/${_name}-conf.list ]; then
            mkdir -p ${_webinf}/classes
            _temp=$( cat ${_wsrep}/meta/${_name}-conf.list)
            [ ! -z "${_temp}" ] \
                && ( cd ${_deprep}/conf && cp ${_temp} ${_webinf}/classes )
        fi

        if [ -e ${_wsrep}/meta/${_name}-ijar.list ]; then
            mkdir -p ${_webinf}/lib
            for _jar in $( awk -F "|" '{print $1}' ${_wsrep}/meta/${_name}-ijar.list )
            do
                [ ! -e ${_deprep}/ilib/${_jar} ] && "ERROR: Internal jar [${_jar}] not found in repository" && return 1
                ln ${_deprep}/ilib/${_jar} ${_webinf}/lib/${_jar}
            done
        fi

        if [ -e ${_wsrep}/meta/${_name}-ejar.list ]; then
            mkdir -p ${_webinf}/lib
            for _jar in $( cat ${_wsrep}/meta/${_name}-ejar.list )
            do
                [ ! -e ${_deprep}/elib/${_jar} ] && "ERROR: External jar [${_jar}] not found in repository" && return 1
                ln ${_deprep}/elib/${_jar} ${_webinf}/lib/${_jar}
            done
        fi
        success "done"
    done
}

# Creates a deployment for a selected release version
process_deployment() {
    
    # Create dirs with rep
    PORT=5000
    APPDIR=${DEP_AREA}/product/instances/app/${PORT}
    REPDIR=${DEP_AREA}/product/rep
    CREPDIR=${DEP_AREA}/product/custom-rep
    CACHEDIR=${DEP_AREA}/product/caches

    mkdir -p ${APPDIR} ${CACHEDIR}

    [ ! -e ${CL5_WS_REP}/rep ] && err "ERROR: Assemble is not proper, repository not found" && return 1
    rm -fr ${DEP_AREA}/product/rep
    ln -fs ${CL5_WS_REP}/rep ${REPDIR}

    [ -e ${CL5_WS_DREP}/rep/apps ] && ln -fs ${CL5_WS_DREP}/rep ${CREPDIR}

    # INSTALL container
    _lrep=${CL5_WS_REP}
    ( [ ! -e ${CL5_WS_DREP}/.assemble.error ] && [ -e ${CL5_WS_DREP}/deps/container ] ) && _lrep=${CL5_WS_DREP}

    [ ! -e ${_lrep}/deps/container ] && err "ERROR: Container not found" && return 1
    CNAME=$( cd ${_lrep}/deps/container/tomcat* && pwd | xargs basename )
    CVER=$( cd ${_lrep}/deps/container/${CNAME}/* && pwd | xargs basename )

    [ ! -e ${_lrep}/deps/container/${CNAME}/${CVER}/${CNAME}-${CVER}.tgz ] \
        && err "ERROR: Container [${CNAME}-${CVER}] not found" && return 1

    # Deploy container
    printf "Deploying container [${CNAME}-${CVER}] ... "
    ( cd ${APPDIR} && tar xf ${_lrep}/deps/container/${CNAME}/${CVER}/${CNAME}-${CVER}.tgz ) || return 1
    success "done"

    # Configure container
    [ ! -e ${APPDIR}/${CNAME}-${CVER}/bin/configure ] \
        && echo "ERROR: Container not compatible, configure not found" && return 1
    ( chmod +x ${APPDIR}/${CNAME}-${CVER}/bin/configure && ${APPDIR}/${CNAME}-${CVER}/bin/configure ${PORT} )

    # Create a soft link in deployment, keeping up with the tradition
    _link=$( echo ${CNAME} | sed 's/-dev$//' )
    ( cd ${DEP_AREA} && rm -f ${_link} && ln -fs ${APPDIR}/${CNAME}-${CVER} ${_link} )

    if [ "${PROMPT_FOR_DEPLOY}" = "prompt" ]; then
        header "Starting deployment of apps. To choose, press (y|ENTER), any other key to skip"
    else
        [ ! -z "${FILTER}" ] && header "Deploying using filter file [${FILTER}]"
    fi

    # Deploy all the apps
    _deploy_apps ${CL5_WS_REP} ${REPDIR} ${APPDIR}/${CNAME}-${CVER}
    [ -e ${CREPDIR} ] \
        && _deploy_apps ${CL5_WS_DREP} ${CREPDIR} ${APPDIR}/${CNAME}-${CVER}

    for container in ${CL5_WS_REP}/deps/container/* ${CL5_WS_DREP}/deps/container/*
    do
        [ ! -e ${container} ] && continue
        ( echo ${container} | grep -q tomcat ) && continue
        name=$( basename ${container} )
        version=$( basename $( cd ${container}/* && pwd ))
        dir=${DEP_AREA}/platform/instances/${name}/${version}

        printf "Deploying container [${name}-${version}] ... "
        rm -fr ${dir}
        ( mkdir -p ${dir} && cd ${dir} && tar xf ${container}/${version}/${name}-${version}.tgz ) || return 1
        success "done"
    done

    # Add remaining deps
    ROOTDIR=${CL5_WS_REP}/deps/others  
    WORKDIR=${DEP_AREA}/tmp
    mkdir -p ${WORKDIR}
    if [ -d ${ROOTDIR} ]; then
        for dir in $( cd ${ROOTDIR} && ls )
        do
            for ver in $( cd ${ROOTDIR}/${dir} && ls )
            do
                [ ! -d ${ROOTDIR}/${dir}/${ver} ] && continue
                name=${dir}-${ver} 
                printf "Deploying dependency [${name}] ... "
                rm -fr ${WORKDIR}/${name}
                ( cd ${WORKDIR} && tar xf ${ROOTDIR}/${dir}/${ver}/${name}.tgz )
                SETUP=${WORKDIR}/${name}/INSTALL/setup.sh
                ( ! ( [ -e ${SETUP} ] && chmod +x ${SETUP} && ${SETUP} ${CACHEDIR} ) )\
                    && err "skipped (no setup script)" || success "done"
            done
        done
    fi

    # Make sure boot conf is present for new Bootstrap to work (target: uac + cas)
    printf 'Creating bootdb.conf in [platform] ... '
    mkdir -p ${DEP_AREA}/platform/caches
	getBootConf > ${DEP_AREA}/platform/caches/bootdb.conf
    success "done"

    echo "${CL5_REL_NAME}" > ${DEP_AREA}/../.deployed

}

# Print the conf file
getBootConf() {

    echo '
    bootdb {
        db-type  = ${DB_TYPE}
        machine  = ${DB_IP}
        port     = ${DB_PORT}
        user     = ${DB_USER}
        sid      = ${DB_SID}
        pmseed   = ${RICE_PMSEED}
        pmtype   = ${RICE_PMTYPE}
    }' | cut -c5-

}

unset header 2>/dev/null
header() {

    local _message="`echo $*`"
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "
${_tag}
  ${_message}
${_tag}
"
}

unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

# Executes script if exists
execsh() {
    SCRIPT=`which $1 2>/dev/null`
    if [ ! -z "${SCRIPT}" ]; then
        eval $*
        return $?
    fi
}

# Check if build has failed
check() {
    local _status=$?
    if [ ${_status} -ne 0 ]; then
        echo "ERROR: DEPLOYMENT FAILED" >&2
        exit ${_status}
    fi
}

# -----------------
# Main method call
# -----------------
main $*
