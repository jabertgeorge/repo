#!/usr/bin/env bash
# ---------------------------------------------------
# Synchronizes the file from workspace to deployment
# ---------------------------------------------------

printf "Synching [$1] ..."
cd $w/dev-src
for file in `find . -name $1`
do
	for oldfile in `find $d -name $1`
	do
		cp $file $oldfile
	done
done
echo 'done'

