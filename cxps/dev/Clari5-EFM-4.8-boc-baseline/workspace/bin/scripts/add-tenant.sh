#!/usr/bin/env bash
# -----------------------------------------
# Author    : Lovish
# Since     : Mon Dec 16 11:48:15 IST 2019
# -----------------------------------------

purpose() {
    # Print purpose
    msg "-----------------------------------------------------------------------------
|                         Tenant Creation                                   |
-----------------------------------------------------------------------------

    Operations to be done
        * Create DB user for database [${DB_TYPE}] with tenant id as username
        * Grant Access to user
        * Prepare DB schema
----------------------------------------------------------------------------
    "
}

main() {

    # Get basepath
    BASEDIR=$( cd $(pwd) && cd $(dirname $0) && pwd )

    # Print purpose and operations in the script
    purpose

    # Take tenant id as input
    while [ -z "${tenant}" ]
    do
        tenant=$( _read "Please provide the tenant-id to be added: " )
        echo $tenant | grep -qxE '^[a-zA-Z0-9_]+$'
        if [ $? -ne 0 ]; then
            echo "Tenant Id provided contains characters besides alphanumeric and underscore"
            echo "Provide a correct tenant Id"
            tenant=""
        fi
    done

    export tenant
    printable_tenant=$( tput bold && printf "${tenant}\033" )

    _user=${DB_USER}
    export DB_USER=${tenant}
    [[ "${DB_TYPE}" = "mysql" ]] && export DB_SID=${DB_USER}

    if [[ ! "$1" = "--no-create" ]]; then
        _header "Creating tenant user in database"
        ${BASEDIR}/credb.sh --tenant
        [ $? -ne 0 ] && err "Failed to create tenant user" && return 1
        ( _provide_access )
        confirmation "Press <ENTER> to proceed <CTRL-C> to quit"
    fi

    _header "Preparing Tenant Schema (Database)"
    # Define the main variables
    TMPDIR=${w}/work
    DBDIR=${TMPDIR}/db

    # Pre-execution cleanup
    printf "Pre-execution cleanup ... "
    rm -fr ${TMPDIR}
    mkdir -p ${DBDIR}
    success "done"

    # Create config file
    printf "Creation of dbcon config file ... "
    _getConf > ${TMPDIR}/dbcon.conf
    success "done"

    # Create input
    printf "Extracting the schema packages ... "
    ( _getDbDirs ${DBDIR} ) || return 1
    success "done"

    # Execute db schema creation
    msg "Starting up execution"
    ( _execute_dsm ${TMPDIR}/dbcon.conf ${DBDIR} $* ) || return 1

    # Post-execution cleanup
    msg "Finishing up"
    printf "Post-execution cleanup ... "
    rm -fr ${TMPDIR}
    success "done"

    export DB_USER=${_user}
    [[ "${DB_TYPE}" = "mysql" ]] && export DB_SID=${DB_USER}
    _header "Register tenant in UAC"
    ( _execute_add_org ) || return 1
    success "Added tenant successfully"
}

# Execute the collected ddl/seed
_execute_add_org() {

    execsh add-org ${tenant}
}

# Execute the collected ddl/seed
_execute_dsm() {

    local _conf=$1
    local _dbDir=$2

    shift 2

    execsh dsm -cleanrun ${_conf} ${_dbDir} $*

}

# Extract all DB resources in destination directory
_getDbDirs() {

    local _dest=$1
    local _pkg

    # Extract the db packages of product
    if [ -e ${CL5_WS_REP}/rep/db ]; then
        for _pkg in ${CL5_WS_REP}/rep/db/*
        do
            [ ! -e ${_pkg} ] && continue
            [ -d ${_pkg} ] && continue
            ( echo ${_pkg} | grep -vq -- "-LOCAL.tar$" ) && continue

            ( cd ${_dest} && tar xf ${_pkg} ) || return 1
        done
    fi

    # Extract the db packages of dev dependencies
    if [ -e ${CL5_WS_REP}-dev/rep/db ]; then
        for _pkg in ${CL5_WS_REP}-dev/rep/db/*
        do
            [ ! -e ${_pkg} ] && continue
            [ -d ${_pkg} ] && continue
            ( echo ${_pkg} | grep -vq -- "-LOCAL.tar$" ) && continue

            ( cd ${_dest} && tar xf ${_pkg} ) || return 1
        done
    fi
}

# Print the conf file
_getConf() {

    local file=${w}/bin/releases/${CL5_REL_NAME}/data/dbcon.conf

    [ -e ${file} ] && cat ${file} && return 0

    echo '
    dbcon {
        class : clari5.platform.dbcon.CxConnectionPool

        db-type = ${DB_TYPE}
        db_type = ${DB_TYPE}
        machine = ${DB_IP}
        port = ${DB_PORT}
        user = '${tenant}'
        sid = ${DB_SID}
        pmseed = ${RICE_PMSEED}
        pmtype = ${RICE_PMTYPE}

        schema = rice
        auto-commit = false
        isolation = COMMITTED
        pool {
            init-size = 1
            min-size = 1
            max-size = 2
            age {
                max = 0
                max-idle-time = 10000
                max-idle-time-excess-connections = 0
            }
            statement {
                max = 50
                max-per-connection = 10
            }
            admin {
                checkout-time = 0
                num-helper-threads = 1
            }
        }
    }' | cut -c5-

}

_provide_access() {
    
    . ${w}/bin/scripts/configure.sh

    _header "Providing access on database to primary user"
    _ruser=$( _read "System user for [${DB_TYPE} on ${DB_IP}]: " )
    _rpsd=$( _read -s "Password for above user: " )

    case ${DB_TYPE} in
        "oracle")
            ;;
        "sqlserver")

            sqlcmd -S ${DB_IP} -U ${_ruser} -P${_rpsd} >/dev/null <<!
use ${tenant}
go
create user ${DB_USER} for login ${DB_USER}
go
exec sp_addrolemember N'db_datareader', N'${DB_USER}'
exec sp_addrolemember N'db_datawriter', N'${DB_USER}'
exec sp_addrolemember N'db_ddladmin', N'${DB_USER}';
go
grant execute to ${DB_USER}
go
!
            ;;
        "mysql")
            mysql -h${DB_IP} -u${_ruser} -p${_rpsd} >/dev/null <<!
grant all privileges on ${tenant}.* to '${DB_USER}'@'%';
flush privileges;
!
            ;;
    esac

}

# ----------------
# Utility methods
# ----------------

# Executes script if exists
execsh() {
    SCRIPT=`which $1`
    if [ ! -z "${SCRIPT}" ]; then
        eval $*
        return $?
    fi
}

# ------------------
# Utility functions
# ------------------

# The function prompts input based on message
# -s mode is for silent
_read()
{
    local _mode
    local _msg
    local _var

    [ $# -gt 1 ] && local _mode="$1" && shift 1
    [[ ! "${_mode}" = "-s" ]] && _mode=""
    _msg=$1

    unset _var 2>/dev/null
    while [ -z "${_var}" ]
    do
        read ${_mode} -p "${_msg}" _var
        [[ "${_mode}" = "-s" ]] && echo >&2
        [ -z "${_var}" ] \
            && unset _var 2>/dev/null \
            && err "Value cannot be left blank" >&2 \
            && continue
        echo ${_var}
        break
    done
}

# Prints a header
_header() {

    local _message=$( echo $*)
    _message=$( echo "${_message}" | sed 's/\[00m/[01;36m/g' )
    local _length=${#_message}
    local _tag=$( echo xx${_message}xx | sed 's/./-/g' )

printf "\033[01;36m
${_tag}
| ${_message} |
${_tag}
\033[00m"

}

# Prints error messages
unset err 2>/dev/null
err() {
    IFS=
    printf '\033[01;31m'
    echo $* | sed 's//[01;31m/g'
    printf '\033[00m'
}

unset success 2>/dev/null
success() {
    IFS=
    printf '\033[01;32m'
    echo $* | sed 's//[01;32m/g'
    printf '\033[00m'
}

unset warn 2>/dev/null
warn() {
    IFS=
    printf '\033[00;32m'
    echo $@ | sed 's//[00;32m/g'
    printf '\033[00m'
}

# Local message
msg() {
    IFS=
    printf '\033[01;36m'
    echo $*
    printf '\033[00m'
}

confirmation() {
    
    echo
    msg "$*"
    read
}

# ----------
# Main call
# ----------
main $*
