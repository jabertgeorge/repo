#!/usr/bin/env bash
# ---------------------------------------------------------------------------------------
# Script    : dbexec.sh
# Usage     : dbexec.sh [flags]
# Author    : Lovish
# Purpose   : This script is used to create schema for the fetched product/custom resources
# ---------------------------------------------------------------------------------------

# Execute the main program handling various conditions
main() {

    # Define the main variables
    TMPDIR=${w}/work
    DBDIR=${TMPDIR}/db

    export TENANCY=$( grep TENANCY ${w}/bin/releases/${CL5_REL_NAME}/versions.properties | cut -d= -f2 | tr -d ' ' )

    header "Database schema creation for product/${CL5_CUSTOM_ID}"

    msg "Preparing data for execution"

    # Pre-execution cleanup
    printf "Pre-execution cleanup ... "
    rm -fr ${TMPDIR}
    mkdir -p ${DBDIR}
    success "done"

    # Create config file
    printf "Creation of dbcon config file ... "
    _getConf > ${TMPDIR}/dbcon.conf
    success "done"

    # Create input
    printf "Extracting the schema packages ... "
    ( _getDbDirs ${DBDIR} ) || return 1
    success "done"

    # Execute db schema creation
    msg "Starting up execution"
    ( _execute ${TMPDIR}/dbcon.conf ${DBDIR} $* ) || return 1

    # Post-execution cleanup
    msg "Finishing up"
    printf "Post-execution cleanup ... "
    rm -fr ${TMPDIR}
    success "done"
}

# Execute the collected ddl/seed
_execute() {

    local _conf=$1
    local _dbDir=$2

    shift 2

    execsh dsm -cleanrun ${_conf} ${_dbDir} $*

}

# Extract all DB resources in destination directory
_getDbDirs() {

    local _dest=$1
    local _pkg

    # Extract the db packages of product
    if [ -e ${CL5_WS_REP}/rep/db ]; then
        for _pkg in ${CL5_WS_REP}/rep/db/*
        do
            [ ! -e ${_pkg} ] && continue
            [ -d ${_pkg} ] && continue

            [ "${TENANCY}" = "MULTIPLE" ] && ( echo ${_pkg} | grep -q 'LOCAL.tar$' ) && continue

            ( cd ${_dest} && tar xf ${_pkg} ) || return 1
        done
    fi

    # Extract the db packages of dev dependencies
    if [ -e ${CL5_WS_REP}-dev/rep/db ]; then
        for _pkg in ${CL5_WS_REP}-dev/rep/db/*
        do
            [ ! -e ${_pkg} ] && continue
            [ -d ${_pkg} ] && continue

            [ "${TENANCY}" = "MULTIPLE" ] && ( echo ${_pkg} | grep -q 'LOCAL.tar$' ) && continue

            ( cd ${_dest} && tar xf ${_pkg} ) || return 1
        done
    fi
}

# Print the conf file
_getConf() {

    local file=${w}/bin/releases/${CL5_REL_NAME}/data/dbcon.conf

    [ -e ${file} ] && cat ${file} && return 0

    echo '
    dbcon {
        class : clari5.platform.dbcon.CxConnectionPool

        db-type = ${DB_TYPE}
        db_type = ${DB_TYPE}
        machine = ${DB_IP}
        port = ${DB_PORT}
        user = ${DB_USER}
        sid = ${DB_SID}
        pmseed = ${RICE_PMSEED}
        pmtype = ${RICE_PMTYPE}

        schema = rice
        auto-commit = false
        isolation = COMMITTED
        pool {
            init-size = 1
            min-size = 1
            max-size = 2
            age {
                max = 0
                max-idle-time = 10000
                max-idle-time-excess-connections = 0
            }
            statement {
                max = 50
                max-per-connection = 10
            }
            admin {
                checkout-time = 0
                num-helper-threads = 1
            }
        }
    }' | cut -c5-

}

# ----------------
# Utility methods
# ----------------

# Executes script if exists
execsh() {
    SCRIPT=`which $1`
    if [ ! -z "${SCRIPT}" ]; then
        eval $*
        return $?
    fi
}

# Prints a main header
unset header 2>/dev/null
header() {

    local _message="`echo $*`"
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "\033[01;36m
${_tag}
  ${_message}
${_tag}
\033[00m"

}

# Prints success messages
unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

# Prints error messages
unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

# Local message
msg() {
    
    local _message=$( echo $* )
    local _tag=$(echo xx${_message}xx | sed 's/./-/g')

printf "\033[03;32m
${_tag}
| ${_message} |
${_tag}
\033[00m"

}

# -----------
# Main block
# -----------
main $*
if [ $? -ne 0 ]; then
    err "Error: Database execution failed"
    exit 1
fi
