:
# ===========================================================================
# File: assemble.sh
# Author: Lovish
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# This file downloads dependencies, provided a deps file
# It makes sure the dependencies are available in local storage
# ===========================================================================

export ERR_FILE=/tmp/.err.$$

unset header 2>/dev/null
header() {

    local _message="`echo $*`"
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "
${_tag}
  ${_message}
${_tag}
"
}

unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

unset errcat 2>/dev/null
errcat() {
    [ -e $1 ] && err "`cat ${1}`" || return
}

# Adds the tool, and runs installation
_add_dep() {
    # Args
    local _pkgGrp=$1
    local _pkgName=$2
    local _pkgVer=$3
    local _pkgType=$4

    local _remotePkgName=${_pkgName}
    local _repD=${CL5_LOCAL_REP}/${_pkgGrp}/${_pkgVer}
    local _src _pkg

    [[ ${_pkgGrp} = "cdn" ]] && _pkgGrp="others"

    # Dependencies have a different rep dir 
    if ! ( [ "${_pkgType}" = "deps" ] || [ "${_pkgType}" = "tool" ] ) ; then
        if [ -e ${_repD} ]; then
            if [[ "${_pkgType}" = "war" ]]; then
                [ -e ${_repD}/rep/apps/${_pkgName}.war ] \
                    || return 1
            fi
            if [[ "${_pkgType}" = "db" ]]; then
                [ -e ${_repD}/rep/db/${_pkgName}.tar ] \
                    || return 1
            fi

            echo ${_repD} && return
        fi

        _remotePkgName=${_pkgGrp}-bundle
    fi

    _src=${CL5_LOCAL_REP}/.downloads/${_remotePkgName}/${_pkgVer}
    _pkg=${_remotePkgName}-${_pkgVer}

    if ( ( [ "${_pkgType}" = "deps" ] || [ "${_pkgType}" = "tool" ] ) && [ ! -e ${_src}/${_pkg}.tgz ] ); then 

        _alt_path=${HOME}/cx/.downloads/${_pkg}.tgz
        if [ -e ${_alt_path} ]; then
            mkdir -p ${_src}
            cp ${_alt_path} ${_src} 2>${ERR_FILE} >&2
        fi

        _alt_path=${HOME}/.cxps-rep/${_pkgGrp}/${_pkgVer}/tools/${_pkg}
        if ( [ ! -e ${_src}/${_pkg}.tgz ] && [ -e ${_alt_path} ] ); then
            mkdir -p ${_src}
            ( cd $(dirname ${_alt_path}) && tar czf ${_src}/${_pkg}.tgz ${_pkg} ) 2>${ERR_FILE} >&2
        fi
    fi

    # Fetch latest dependency package 
    python $( which cxget.py ) ${_remotePkgName} ${_pkgVer} 2>${ERR_FILE} >&2

    # If not fetched, exit with error
    [ ! -e ${_src}/${_pkg}.tgz ] && return 1

    if ( [ "${_pkgType}" = "deps" ] || [ "${_pkgType}" = "tool" ] ) ; then 
        echo ${_src} && return
    fi
    
    mkdir -p ${_repD}
    ( cd ${_repD} && tar xzf ${_src}/${_pkg}.tgz )

    if [[ "${_pkgType}" = "war" ]]; then
        [ -e ${_repD}/rep/apps/${_pkgName}.war ] \
            || return 1
    fi
    if [[ "${_pkgType}" = "db" ]]; then
        [ -e ${_repD}/rep/db/${_pkgName}.tar ] \
            || return 1
    fi

    echo ${_repD}
}

_assemble_dep() {
    
    # Args
    local _pkgGrp=$1
    local _pkgName=$2
    local _pkgVer=$3
    local _pkgType=$4

    local _baseRep _token

    # Making sure app is present
    _baseRep=`_add_dep ${dgroup} ${dname} ${dver} ${dtype}`

    [ -z "${_baseRep}" ] && echo "[${_pkgName}:${_pkgType}] not found" >${ERR_FILE} && return 1
    
    case ${_pkgType} in
        "war")
            _token="./rep/apps/${_pkgName}.war"
            
            # Base operation on war file (assumes it is present by now)
            mkdir -p ${CL5_WS_REP}/rep/apps
            ln -fs ${_baseRep}/${_token} ${CL5_WS_REP}/${_token}

            # Copy all config files
            _token="./meta/${_pkgName}-conf.list"
            mkdir -p ${CL5_WS_REP}/meta ${CL5_WS_REP}/rep/conf
            if [ -e ${_baseRep}/${_token} ]; then
                for _conf in $( cat ${_baseRep}/${_token} )
                do
                    [ -e ${CL5_WS_REP}/rep/conf/${_conf} ] && continue
                    ln -fs ${_baseRep}/rep/conf/${_conf} ${CL5_WS_REP}/rep/conf/${_conf}
                done
                cp ${_baseRep}/${_token} ${CL5_WS_REP}/${_token}
            fi

            # Copy all internal jars
            _token="./meta/${_pkgName}-ijar.list"
            mkdir -p ${CL5_WS_REP}/meta ${CL5_WS_REP}/rep/ilib
            if [ -e ${_baseRep}/${_token} ]; then
                for _jar in $( awk -F "|" '{print $1}' ${_baseRep}/${_token} )
                do
                    [ -e ${CL5_WS_REP}/rep/ilib/${_jar} ] && continue
                    ln -fs ${_baseRep}/rep/ilib/${_jar} ${CL5_WS_REP}/rep/ilib/${_jar}
                done
                cp ${_baseRep}/${_token} ${CL5_WS_REP}/${_token}
            fi

            # Copy all external jars
            _token="./meta/${_pkgName}-ejar.list"
            mkdir -p ${CL5_WS_REP}/meta ${CL5_WS_REP}/rep/elib
            if [ -e ${_baseRep}/${_token} ]; then
                for _jar in $( cat ${_baseRep}/${_token} )
                do
                    [ -e ${CL5_WS_REP}/rep/elib/${_jar} ] && continue
                    ln -fs ${_baseRep}/rep/elib/${_jar} ${CL5_WS_REP}/rep/elib/${_jar}
                done
                ln -fs ${_baseRep}/${_token} ${CL5_WS_REP}/${_token}
            fi

            # Copy revision file
            _token="./meta/${_pkgName}.revision"
            if [ -e ${_baseRep}/${_token} ]; then
                rm -f ${CL5_WS_REP}/${_token}
                ln -fs ${_baseRep}/${_token} ${CL5_WS_REP}/${_token}
            fi

            # Copy meta file
            _token="./meta/${_pkgName}-meta.conf"
            if [ -e ${_baseRep}/${_token} ]; then
                rm -f ${CL5_WS_REP}/${_token}
                ln -fs ${_baseRep}/${_token} ${CL5_WS_REP}/${_token}
            fi
            ;;
        "deps")
            _token="deps/${_pkgGrp}/${_pkgName}/${_pkgVer}"
            _file="${_pkgName}-${_pkgVer}.tgz"

            mkdir -p ${CL5_WS_REP}/${_token}
            [ -e ${CL5_WS_REP}/${_token}/${_file} ] && return
            ln -fs ${_baseRep}/${_file} ${CL5_WS_REP}/${_token}/${_file}

            ;;
        "db")
            _token="./rep/db"

            mkdir -p ${CL5_WS_REP}/${_token}
            cp ${_baseRep}/${_token}/${_pkgName}.tar ${CL5_WS_REP}/${_token}
            ;;
        "ui")
            _token="./rep/ui"

            [ ! -e ${_baseRep}/${_token}/${_pkgName} ] \
                && echo "ERROR: ${_baseRep}/${_token}/${_pkgName} not found" && return 1

            for file in $( cd ${_baseRep}/${_token}/${_pkgName} && find . -type f )
            do
                _src=${_baseRep}/${_token}/${_pkgName}/${file}
                _name=$( basename ${file} )
                
                ( echo ${file} | grep -q '/css/.*css$' ) \
                    && mkdir -p ${CL5_WS_REP}/${_token}/cdn/css \
                    && rm -f ${CL5_WS_REP}/${_token}/cdn/css/${_name} \
                    && ln -fs ${_src} ${CL5_WS_REP}/${_token}/cdn/css/${_name} \
                    && continue

                ( echo ${file} | grep -q '/images/.*$' ) \
                    && mkdir -p ${CL5_WS_REP}/${_token}/cdn/images \
                    && rm -f ${CL5_WS_REP}/${_token}/cdn/images/${_name} \
                    && ln -fs ${_src} ${CL5_WS_REP}/${_token}/cdn/images/${_name}
            done

            ( cd ${_baseRep}/${_token}/${_pkgName} && tar chf - * ) \
                | ( cd ${CXPS_WORKSPACE}/angular/app && tar xf -)
            ;;
        "tool")
            _token=${_pkgName}-${_pkgVer}
            _file=${w}/bin/tools/${_token}/INSTALL/setup.sh

            mkdir -p ${w}/bin/tools ${w}/bin/tools/bin
            rm -fr ${w}/bin/tools/${_token}
            ( cd ${w}/bin/tools && tar xf ${_baseRep}/${_token}.tgz )
            [ -e ${_file} ] && ( chmod +x ${_file} && ${_file} ${w}/bin/tools )
            ;;
            
        *) echo "Type ${_pkgType} not supported yet for [${_prgGrp}:${_pkgName}:${_pkgVer}]"
    esac
}

process_deps() {
    local _depFile=$1

    while read line
    do
        # Ignore comments and empty lines
        ( echo ${line} | grep -q -e '^[ 	]*#' -e '^[ 	]*$' ) && continue

        dgroup=`echo $line | cut -d ':' -f1 | sed 's/[ 	]*//g'`
        dname=`echo $line | cut -d ':' -f2 | sed 's/[ 	]*//g'`
        dver=`echo $line | cut -d ':' -f3 | sed 's/[ 	]*//g'`
        dtype=`echo $line | cut -d ':' -f4 | sed 's/[ 	]*//g'`

        printf "Assembling [${dgroup}:${dname}:${dver}:${dtype}] ... "
        ( _assemble_dep ${dgroup} ${dname} ${dver} ${dtype} && success "done" )\
            || ( echo "Error: Assemble failed for [${dgroup}:${dname}:${dver}:${dtype}]" \
                >> ${CL5_WS_REP}/.assemble.error && err "error" && errcat ${ERR_FILE} )

    done < ${_depFile}
    rm -f ${ERR_FILE}
}

# -- Main Block --

# Get BASEDIR
export BASEDIR=$( cd `pwd` && cd `dirname $0` && pwd )

[ -z "${CL5_LOCAL_REP}" ] && echo "ERROR: Unable to locate cxps rep [CL5_LOCAL_REP]" && exit 1
[ -z "${CL5_REL_NAME}" ] && echo "ERROR: Unable to locate release name" && exit 1
[ -z "${CL5_WS_REP}" ] && echo "ERROR: Local rep in workspace not defined" && exit 1

# Unset JAVA_OPTS
unset JAVA_OPTS 2>/dev/null

# Check the usage, exit if not ok
CX_DEP_FILE=${w}/bin/releases/${CL5_REL_NAME}/deps.cfg

# Check the existence of the file
[ ! -f ${CX_DEP_FILE} ] && echo "ERROR: File [${CX_DEP_FILE}] not found" && exit 1

[[ "$1" = "--rversion" ]] \
    && grep -w 'war$' ${CX_DEP_FILE} | grep -v '^[ 	]*#' | cut -d: -f3 | tr -d ' ' | sort -u \
    && exit 0

# Make sure older backup dirs are not present, and have a local rep as well
rm -fr ${CL5_WS_REP}
mkdir -p ${CL5_WS_REP}

script="${w}/bin/releases/${CL5_REL_NAME}/profile.sh"
[ -e ${script} ] && chmod +x ${script} && ${script}

# Process all the dependencies
header "Performing the assemble for the release [${CL5_REL_NAME}]"
process_deps ${CX_DEP_FILE}

export CL5_WS_REP=${CL5_WS_REP}-dev
export CX_DEP_FILE=$( dirname ${CX_DEP_FILE} )/dev.cfg
rm -fr ${CL5_WS_REP}
mkdir -p ${CL5_WS_REP}

if [ -e ${CX_DEP_FILE} ]; then
    # Install dev dependencies
    header "Performing the assemble for dev [${CL5_REL_NAME}]"
    process_deps ${CX_DEP_FILE}
fi
