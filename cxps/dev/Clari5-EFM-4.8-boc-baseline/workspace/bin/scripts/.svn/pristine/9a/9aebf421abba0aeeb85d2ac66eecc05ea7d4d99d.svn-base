#!/usr/bin/env bash
# -------------------------------------------------------------------
# This script creates the database users
# -------------------------------------------------------------------

# Global Variables
REMOVE=/bin/rm

main () {

    # Get basepath
    export basepath=$( cd `pwd` && cd `dirname $0` && pwd ) 

    tenant=false
    if [ "${1}" == '--tenant' ]; then
        shift 1
        tenant=true
        user=${DB_USER}
    fi
    
    # Load environment
    chmod +x ${w}/bin/scripts/configure.sh && . ${w}/bin/scripts/configure.sh

    if [ ! -z "${user}" ]; then
        export DB_USER=${user}
        export SEC_DB_USER=${user}
    fi

    # Create db users
    get_syspw || return 1

    create_all=true
    ( echo ${DB_USER} | grep -qx ${SEC_DB_USER} ) && create_all=false

    _header "User creation"
    echo " - About to create users [$DB_USER]$( ${create_all} && echo , [$SEC_DB_USER])"
    echo " - Using DB SID $DB_SID"
    echo " - Warning!! You will lose all tables created under these users"

    _confirmation
    echo "Processing ... (Please wait!)"

    case ${DB_TYPE} in
        "oracle") create_ora_users ${create_all} ;;
        "sqlserver") create_mssql_users ${create_all} ;;
        "mysql") create_mysql_users ${create_all} ;;
    esac
}

# Create users for application and security schema
create_ora_users() {

    local create_both=$1
    local OUTFILE=/tmp/createusers.lst
    local st=0

    cat <<EOT | $APPCMD
spool $OUTFILE;
$(oracmd ${DB_USER} ${RICE_PMSEED})
$( ${create_both} && oracmd ${SEC_DB_USER} ${SECURITY_PMSEED})
spool off;
EOT

    if [ -f $OUTFILE ] ; then
        if grep -q 'ORA-' $OUTFILE
        then
            echo "Unable to create the users." 2>/dev/null
            st=1
        fi
        ${REMOVE} -f $OUTFILE
    fi

    return $st
}

create_mysql_users() {
    cat <<EOT | $APPCMD
drop database if exists ${DB_USER};
create database ${DB_USER} charset latin1;
use ${DB_USER};
grant all on ${DB_USER}.* to ${DB_USER}@'%' identified by '${RICE_PMSEED}';
flush privileges;
EOT
}

# Create users for application and security schema
create_mssql_users() {

    local create_both=$1
    local st=0

    cat <<EOT | $APPCMD
$(mssqlcmd ${DB_USER} ${RICE_PMSEED})
$( ${create_both} && mssqlcmd ${SEC_DB_USER} ${SECURITY_PMSEED})
quit
EOT
    st=$?

    return $st
}

# Define all the common grants for both application/security schema
oracmd() {
    
    local _user=$1
    local _psd=$2

cat <<EOT
DROP USER ${_user} CASCADE;
CREATE USER ${_user} PROFILE "DEFAULT" IDENTIFIED BY "${_psd}" ACCOUNT UNLOCK;
GRANT CREATE SESSION TO ${_user};
GRANT UNLIMITED TABLESPACE TO ${_user};
GRANT CREATE TABLE TO ${_user};
GRANT CREATE MINING MODEL TO ${_user};
GRANT CREATE VIEW TO ${_user};
GRANT CREATE SYNONYM TO ${_user};
GRANT CREATE TYPE TO ${_user};
GRANT CREATE SEQUENCE TO ${_user};
GRANT CREATE TRIGGER TO ${_user};
GRANT CREATE PROCEDURE TO ${_user};
GRANT CREATE JOB TO ${_user};
GRANT SELECT ANY DICTIONARY TO ${_user};
EOT

}

mssqlcmd() {

    local _user=$1
    local _psd=$2

cat <<EOT
IF NOT EXISTS (SELECT * FROM master.dbo.sysdatabases WHERE name = '${_user}')
BEGIN
create database ${_user} 
END
GO

use ${_user}
go

IF EXISTS (SELECT * FROM sys.database_principals WHERE name = '${_user}')
drop user ${_user} ;
go

IF EXISTS (SELECT * FROM sys.server_principals WHERE name = '${_user}')
drop login ${_user} ;
go

Use tempdb ;
drop database ${_user} 
go

create database ${_user} ;
go
Use ${_user}
go
create login ${_user} with password = '${_psd}' ;
go
create user ${_user} for login ${_user} ;
go
exec sp_addrolemember N'db_datareader', N'${_user}'; 
exec sp_addrolemember N'db_datawriter', N'${_user}'; 
exec sp_addrolemember N'db_ddladmin', N'${_user}'; 
go
grant execute to ${_user}
exec sp_defaultdb @loginame='${_user}', @defdb='${_user}';
go
use [msdb]
go
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = '${_user}')
create user ${_user} for login ${_user} ;
go
exec sp_addrolemember N'SQLAgentUserRole', N'${_user}'; 
go
grant execute to ${_user}
go
GRANT EXECUTE ON dbo.sp_add_job TO public;
GRANT EXECUTE ON dbo.sp_add_jobstep TO public
GRANT EXECUTE ON dbo.sp_add_schedule TO public
GRANT EXECUTE ON dbo.sp_attach_schedule TO public
GRANT EXECUTE ON dbo.sp_add_jobserver TO public
GRANT EXECUTE ON dbo.sp_start_job TO public
GRANT EXECUTE ON dbo.sp_delete_job TO public
go
use master
go
GRANT VIEW SERVER STATE TO ${_user}
go
EOT
}

# Prompt oracle admin user/password validation
get_syspw()
{
    [ ! -z "$APPCMD" ] && return

    _header "${DB_TYPE} admin credentials (not saved)"
    while :
    do
        [ -z "${DB_SYS_USER}" ] \
            && DB_SYS_USER=`_read "${DB_TYPE} system user: "`
        [ -z "${DB_SYS_PASSWD}" ] \
            && DB_SYS_PASSWD=`_read -s "${DB_TYPE} system password (_ for NIL): "`

        printf "Validating the correctness using SID [${DB_SID}] ... "
        validate_userpw $DB_SYS_USER $DB_SYS_PASSWD && echo "done!" && break \
            || return 1
    done
    case "$DB_TYPE" in
        "oracle") export APPCMD="sqlplus -s ${DB_SYS_USER}/${DB_SYS_PASSWD}@${DB_SID}" ;;
        "sqlserver") export APPCMD="sqlcmd -S ${DB_IP} -U ${DB_SYS_USER} -P ${DB_SYS_PASSWD}" ;;
        "mysql") export APPCMD="mysql -h${DB_IP} -u${DB_SYS_USER}"
            [ "$DB_SYS_PASSWD" != "_" ] && APPCMD="$APPCMD -p${DB_SYS_PASSWD}"
            ;;
    esac

    return 0
}

# Validate dbuser/passwd
validate_userpw()
{
    local _user=$1
    local _pswd=$2
    local _st=1
    local _cmd

    case ${DB_TYPE} in
        "oracle")
            OUTFILE=/tmp/.checkcon
            ${REMOVE} -f $OUTFILE
            sqlplus -s -L ${_user}/${_pswd}@$DB_SID <<EOT >/dev/null
spool $OUTFILE
set head off
select sysdate from dual;
exit;
EOT
            [ ! -f $OUTFILE ] \
                && echo "invalid userId or password" >&2 \
                || _st=0
            ${REMOVE} -f $OUTFILE
        ;;

        "sqlserver")
            sqlcmd -S ${DB_IP} -U ${_user} -P ${_pswd} <<EOT >/dev/null
select current_timestamp
go
EOT
            _st=$?
        ;;
        "mysql")
            if [ "${_pswd}" = "_" ] ; then
                mysqladmin -u${_user} ping 2>&1 | grep -q alive
            else
                mysqladmin -u${_user} -p${_pswd} ping 2>&1 | grep -q alive
            fi
            _st=$?
        ;;
    esac

    return $_st
}

# ------------------
# Utility functions
# ------------------

# The function prompts input based on message
# -s mode is for silent
_read()
{
    local _mode
    local _msg
    local _var

    [ $# -gt 1 ] && local _mode="$1" && shift 1
    [[ ! "${_mode}" = "-s" ]] && _mode=""
    _msg=$1

    unset _var 2>/dev/null
    while [ -z "${_var}" ]
    do
        read ${_mode} -p "${_msg}" _var
        [[ "${_mode}" = "-s" ]] && echo >&2
        [ -z "${_var}" ] \
            && unset _var 2>/dev/null \
            && echo "Value cannot be left blank" >&2 \
            && continue
        echo ${_var}
        break
    done
}

# Promps a confirmation to proceed
_confirmation()
{
    echo
    echo "---- Press <ENTER> to continue ----"
    read ENTER
}

# Prints a header
_header() {

    local _message=`echo $*`
    local _length=${#_message}
    local _tag=`for (( i=0 ; i<= ${_length} +3 ; ++i )); do printf "-"; done`

cat << EOF
${_tag}
  ${_message}
${_tag}

EOF

}

# -----------
# Main Block
# -----------

main $*
status=$?
if [ ${status} -ne 0 ]; then
    echo "ERROR: DB Creation failed!!!" && exit ${status}
fi
