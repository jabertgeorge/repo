#!/bin/bash
# ============================================================================
# This file is used for patching the distribution zip package
# Steps:
# - unzip the distribution zip package
# - patch the 'gen' file
# - update the distribution package
# ============================================================================


# ==================
# MAIN ACTION BLOCK
# ==================
module=gen
if [ ! -d $CX_BASE/mod/${module} ] ; then
	echo "Usage:Unable to find [$CX_BASE/mod/${module}]" >&2
	exit 1
fi

cd $CX_BASE/mod/${module}
gradle distZip

FILE=${module}-SNAPSHOT

TMPDIR=tmp
mkdir $TMPDIR
( cd $TMPDIR && unzip ../build/distributions/${FILE}.zip )

# Check if already patched
# -------------------------
if grep -q "HANDLE spec downloads" $TMPDIR/${FILE}/bin/${module}
then
	echo "Error: Already patched!" >&2
	\rm -rf $TMPDIR
	exit 2
fi

# Get till last line
filesize=`wc -l $TMPDIR/${FILE}/bin/${module} | sed -e 's/^[ 	]*//' -e 's/[ 	].*$//'`
let tillend=filesize-1

${SED} -n 1,${tillend}p $TMPDIR/${FILE}/bin/${module} > $TMPDIR/x
cat >> $TMPDIR/x <<'EOT'
# ---------------------
# HANDLE spec downloads
# ---------------------
# command=`echo "$@" | tr ' ' '\012' | grep -v '^[    ]*$' | head -1`
folder=`echo "$@" | tr ' ' '\012' | grep -v '^[     ]*$' | tail -1`
#if [ ! -z "$command" -a "$command" = "rdbms" -a ! -z "$folder" -a -f $folder/depends.txt ]
if [ ! -z "$folder" -a -f $folder/depends.txt ]
then
    cwd=`pwd`
    cd $folder
    [ ! -d depends ] && mkdir depends
    for zipfile in `cat depends.txt`
    do
        dep=`echo $zipfile | sed 's/\.zip$//'`
        python $CX_LIB/cxdownload.py $zipfile
		if [ $? -eq 1 -o ! -d depends/${dep} ] ; then
           ( cd depends && unzip -uq ${CX_REP}/${zipfile} )
        fi
    done
    cd $cwd
fi
EOT

${SED} -n ${filesize}p $TMPDIR/${FILE}/bin/${module} >> $TMPDIR/x

mv $TMPDIR/x $TMPDIR/${FILE}/bin/${module}
chmod +x $TMPDIR/${FILE}/bin/${module}

( cd $TMPDIR && zip -ru ../build/distributions/${FILE}.zip ${FILE} )

\rm -rf $TMPDIR
