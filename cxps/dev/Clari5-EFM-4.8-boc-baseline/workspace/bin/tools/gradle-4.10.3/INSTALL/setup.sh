:

main() {
    DEST=$1
    [ -z "$DEST" ] && echo "Usage : setup.sh <workspace-dir>" && return 1
    [ ! -e ${DEST}/bin ] && echo "ERROR: WS/bin not found" && return 1

    # Get BASEDIR
    CWD=`pwd`
    BASEDIR=$( cd $CWD && cd `dirname $0` && cd .. && pwd ) 2>/dev/null

    pkgV=`basename $BASEDIR`
    pkgV=`echo ${pkgV} | cut -d '-' -f2`
    refresh=1
    path=`which gradle 2>/dev/null`
    if [ ! -z "$path" ]; then
        version=`ls -l $path | sed 's,^.*gradle[-/]\([0-9\.]*\).*,\1,g'`
        if [ $version != ${pkgV} ]; then
            last=$( echo -e "${version}\n${pkgV}" | sort -V | tail -1 )
            [[ $last = $pkgV ]] && refresh=0
        fi
    else
        refresh=0
    fi

    # Create link if refresh
    [ $refresh -eq 1 ] || ( cd $DEST/bin && \rm -f gradle && ln -fs ${BASEDIR}/bin/gradle gradle )
}

# Main call
main $*

