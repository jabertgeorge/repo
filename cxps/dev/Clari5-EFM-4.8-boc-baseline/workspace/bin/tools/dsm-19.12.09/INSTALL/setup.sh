:

DEST=$1
[ -z "$DEST" ] && echo "Usage : setup.sh <workspace-dir>" && exit 1
[ ! -e ${DEST}/bin ] && echo "ERROR: WS/bin not found" && exit 1

# Get BASEDIR
CWD=`pwd`
BASEDIR=$( cd $CWD && cd `dirname $0` && cd .. && pwd ) 2>/dev/null

# Create link
( cd ${DEST}/bin && \rm -f dsm && ln -fs ${BASEDIR}/bin/dsm dsm )
( cd ${DEST}/bin && chmod +x dsm )
