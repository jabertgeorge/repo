:

unset pps 2>/dev/null
unalias pps 2>/dev/null
pps() {
    [ $# -eq 0 ] \
        && ps -fu ${USER} | grep java | grep -v grep \
        && return 0

    for type in $*
    do
        case $type in
            "-j")
                type=java
                ;;
            "-b")
                type=bash
                ;;
            "-c")
                type=sqlplus
                ;;
            *)
                type=java
                ;;
        esac
        ps -fu ${USER} | grep $type | grep -v grep
    done
}

unset kps 2>/dev/null
unalias kps 2>/dev/null
kps() {
    if [ $# -ne 0 ]; then
        for pid in $*
        do
            pps | awk '{print $2}' | grep $pid | xargs kill -9
        done
    else
        pps | awk '{print $2}' | xargs kill -9
    fi

}

unset se 2>/dev/null
unalias se 2>/dev/null
se () {
    [ $# -lt 1 ] \
        && echo "se <--|search word> <file extension> [find args]" \
        && echo "Default extension is 'java'" \
        && return 1

    FIND=`which find`
    [ $? -ne 0 ] && echo "'find' command not found" && return 1

    __flag="$1"
    [ ! -z "$2" ] && __searchWith="$2" && shift 1 || __searchWith='java'
    shift 1

    case ${__flag} in
        "--")
            _result=`${FIND} $* -name "*.${__searchWith}" -type f`
            [ -z "${_result}" ] \
                && return 1 \
                || echo ${_result} | tr ' ' '\n'
            ;;
        *)
            __searchToken="${__flag}"
            ${FIND} $* -iname "*${__searchToken}*.${__searchWith}"
            ;;
    esac

    return $?
}
