package cxps.events;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashSet;

import java.util.Set;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.platform.dbcon.QueryMapper;

import clari5.platform.dbcon.QueryMapper;

import clari5.rdbms.Rdbms;

import cxps.apex.noesis.WorkspaceInfo;
import java.text.DecimalFormat;

/*

Created by Naman
*/

public class CustomFieldDerivator {

    public static String deriveEightZeros(FT_AccountTxnEvent event) {
        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Connection con = null;
        PreparedStatement ps = null, ps1 = null;
        ResultSet result = null, result1 = null;
        int count = 0;
        try {
            con = Rdbms.getAppConnection();
            double txnAmt = event.getTxnAmt();
            String s = String.valueOf(new DecimalFormat("#").format(txnAmt));
            System.out.println(s);
            String status="N";

            String custType="";
            String custId = event.getCustId();

                if (custId != null && !"".equals(custId)) {
                    String loadquery="select * from CUSTOMER where hostCustId = ?";
                    ps = con.prepareStatement(loadquery);
                    ps.setString(1, custId);

                    result = ps.executeQuery();

                        if (result.next()) {
                            custType=result.getString("custtype");
                            System.out.println("Cust type "+custType+" for custid "+custId);
                            //return "Y";
                            if(s.endsWith("00000000") && ((custType.equals("FI")) || (custType.equals("NP")))){
                                status = "Y";

                            }else if(s.endsWith("000000") && (custType.equals("P"))){
                                status="Y";
                            }
                            return status;
                        }else {
                            System.out.println("Not goes to cust flag.");
                            return status;
                        }

                }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                if (result != null) result.close();
                if (ps != null) ps.close();
                if (con != null) con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "N"    ;
    } }

