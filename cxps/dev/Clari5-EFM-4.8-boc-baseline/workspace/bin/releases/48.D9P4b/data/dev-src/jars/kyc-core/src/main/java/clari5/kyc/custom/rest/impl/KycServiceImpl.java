package clari5.kyc.custom.rest.impl;

import clari5.hfdb.AmlCustomerView;
import clari5.hfdb.CrcRisk;
import clari5.hfdb.CrcRiskHist;
import clari5.kyc.CustFatca;
import clari5.kyc.CustKycCngHis;
import clari5.kyc.CustMakerTbl;
import clari5.kyc.custom.KycException;
import clari5.kyc.custom.rest.KycServices;
import clari5.kyc.mappers.CustMakerTblMapper;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.*;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.rdbms.Rdbms;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.core.SecurityContext;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class KycServiceImpl implements KycServices {

    @Override
    public String initializeKyc(SecurityContext ctx) throws KycException {
        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");

        try (RDBMSSession session = Rdbms.getAppSession()) {

            String userId = ctx.getUserPrincipal().getName();

            HashMap delta = new HashMap();
            List deltaCreatedLists = new ArrayList();
            CustMakerTblMapper makerTbl = session.getMapper(CustMakerTblMapper.class);
            List deltaList = makerTbl.getAllRecords();
            int index = 0;

            while (deltaList.size() > index) {
                HashMap deltaDetails = new HashMap();
                CustMakerTbl custMakerTbl = (CustMakerTbl) deltaList.get(index);

                if (custMakerTbl != null) {
                    deltaDetails.put("custId", custMakerTbl.getCustId());
                    deltaDetails.put("updateType", custMakerTbl.getUpdateType());
                    switch (custMakerTbl.getUpdateType()) {
                        case "riskLevel":

                            JSONObject changeJson = new JSONObject(custMakerTbl.getUpdates());
                            deltaDetails.put("newDetails", changeJson.optString("riskLevel"));

                            CrcRisk crcRisk = new CrcRisk(session);
                            crcRisk.setCxKey(custMakerTbl.getCustId());
                            crcRisk = crcRisk.select();

                            if (crcRisk != null) {
                                deltaDetails.put("oldDetails", crcRisk.getRiskLevel());
                            }
                            break;

                        case "fatca":
                            CustFatca custFatca = new CustFatca(session);
                            try {
                                custFatca.from(custMakerTbl.getUpdates());
                                deltaDetails.put("newDetails", custFatca);

                                CustFatca custFatcaOld = new CustFatca(session);
                                custFatcaOld.setCustId(custMakerTbl.getCustId());
                                custFatcaOld = custFatcaOld.select();
                                if (custFatcaOld != null)
                                    deltaDetails.put("oldDetails", custFatcaOld);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        case "userProfile":
                            deltaDetails.put("newDetails", (new JSONObject(custMakerTbl.getUpdates())).optString("bankRelatedFamily"));


                            AmlCustomerView cust = new AmlCustomerView(session);
                            cust.setCustId(custMakerTbl.getCustId());
                            cust = cust.select();

                            if (cust != null) {
                                deltaDetails.put("oldDetails", cust.getBankRelatedFamily());
                            }

                            break;
                    }
                    if (custMakerTbl.getUserId().equalsIgnoreCase(userId)) {
                        deltaDetails.put("canVerify", false);
                    } else {
                        deltaDetails.put("canVerify", true);
                    }
                    deltaCreatedLists.add(deltaDetails);
                }
                index++;
            }

            delta.put("delta", deltaCreatedLists);
            return new JSONObject(delta).toString();

        } catch (RdbmsException e) {
            e.printStackTrace();
            throw new KycException("Exception while communicating to DB, Please try again after some time.");
        }
    }

    @Override
    public String getCustomerDetails(String custId) throws KycException {

        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");

        try (RDBMSSession session = Rdbms.getAppSession()) {
            HashMap custDetails = new HashMap();


            AmlCustomerView cust = new AmlCustomerView(session);
            cust.setCustId(custId);
            cust = cust.select();

            if (cust != null) {

                custDetails.put("custDetails", cust);

                CustFatca custFatca = new CustFatca(session);
                custFatca.setCustId(custId);
                custFatca = custFatca.select();
                if (custFatca != null)
                    custDetails.put("custFatcaDetails", custFatca);

                CrcRisk crcRisk = new CrcRisk(session);
                crcRisk.setCxKey(custId);
                crcRisk = crcRisk.select();

                if (crcRisk != null)
                    custDetails.put("riskLevel", crcRisk.getRiskLevel());

                return new JSONObject(custDetails).toString();
            }
            throw new KycException("Invalid customer ID !!");
        } catch (RdbmsException e) {
            e.printStackTrace();
            throw new KycException("Exception while communicating to DB, Please try again after some time.");
        }
    }

    @Override
    public String updateDetails(String custDetails, SecurityContext ctx) throws KycException, Exception {
        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");

        try (RDBMSSession session = Rdbms.getAppSession()) {
            String userId = ctx.getUserPrincipal().getName();

            JSONObject changeJson = new JSONObject(custDetails);
            String custId = changeJson.optString("custId");

            int index = 0;

            JSONArray changes = changeJson.getJSONArray("changes");
            while (changes.length() > index) {
                JSONObject update = changes.optJSONObject(index).optJSONObject("updates");
                String updateType = changes.optJSONObject(index).optString("updateType");
                CustMakerTbl custMakerTbl = new CustMakerTbl(session);

                custMakerTbl.setCustId(custId);
                custMakerTbl.setUpdateType(updateType);
                custMakerTbl.setUpdates(update.toString());
                custMakerTbl.setTimestamp(new Timestamp(System.currentTimeMillis()));
                custMakerTbl.setUserId(userId);

                custMakerTbl.from(custDetails);
                if (custMakerTbl.insert() != 1) {
                    throw (new KycException("Update request already present.Please verify update request and try again. !!"));
                }
                index++;
            }


            session.commit();
            return "Success";
        } catch (RdbmsException e) {
            e.printStackTrace();
            throw (new KycException("Failed to process your update request, please try again after some time."));
        }

    }

    @Override
    public String verifyKycUpdate(String kycUpdateDetails, SecurityContext ctx) throws KycException, Exception {
        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");

        try (RDBMSSession session = Rdbms.getAppSession()) {
            String userId = ctx.getUserPrincipal().getName();
            JSONObject jsonObject = new JSONObject(kycUpdateDetails);
            String custId = jsonObject.optString("custId");
            String updateType = jsonObject.optString("updateType");
            String isAccepted = jsonObject.optString("isAccepted");
            String remark = jsonObject.optString("remark");

            CustMakerTbl custMakerTbl = new CustMakerTbl(session);
            custMakerTbl.setCustId(custId);
            custMakerTbl.setUpdateType(updateType);

            custMakerTbl = custMakerTbl.select();

            if (custMakerTbl.getUserId().equalsIgnoreCase(userId)) {
                throw new KycException("User Doesn't have authority to verify.");
            }

            if (isAccepted.equalsIgnoreCase("yes")) {

                switch (updateType) {
                    case "riskLevel":
                        updateCustomerRiskLevel(custId, custMakerTbl.getUpdates(), userId, session);
                        break;

                    case "fatca":
                        updateFatcaDetails(custMakerTbl.getUpdates(), custId, session);
                        break;

                    case "userProfile":
                        updateUserProfile(custId, custMakerTbl.getUpdates(), session);
                        break;
                }
            }
            CustKycCngHis custKycCngHis = new CustKycCngHis(session);

            custKycCngHis.setCreatedDate(custMakerTbl.getTimestamp());
            custKycCngHis.setVerifyDate(new Timestamp(System.currentTimeMillis()));
            custKycCngHis.setCustId(custId);
            custKycCngHis.setUpdateType(updateType);
            custKycCngHis.setUpdates(custMakerTbl.getUpdates());
            custKycCngHis.setCreatedUserId(custMakerTbl.getUserId());
            custKycCngHis.setVrifierUserId(userId);
            custKycCngHis.setIsAccepted(isAccepted);
            custKycCngHis.setRemark(remark);

            if (custKycCngHis.insert() == 1) {
                custMakerTbl.delete();
            }

            session.commit();
            return "Success";
        } catch (RdbmsException e) {
            e.printStackTrace();
            throw new KycException("Failed to verify the changes. Please try again after some time.");
        }
    }


    private String updateFatcaDetails(String fatcaDetails, String custID, RDBMSSession session) throws KycException, Exception {
        if (session != null) {

            CustFatca custFatca = new CustFatca(session);
            try {
                custFatca.from(fatcaDetails);
                custFatca.setCustId(custID);

                if (custFatca.insert() != 1) {
                    if (custFatca.update() != 1)
                        throw new KycException("Failed to verify the changes. Please try again after some time.");
                }

                session.commit();
                return "Success";
            } catch (RdbmsException e) {
                if (custFatca.update() == 1)
                    return "Success";
                else
                    throw new KycException("Exception while processing your request. Please contact support staff.");
            }


        }
        throw new KycException("Unable to acquire session, Please try again after some time");
    }

    private String updateCustomerRiskLevel(String custId, String details, String userId, RDBMSSession session) throws KycException, Exception {
        if (session != null) {

            try {
                CrcRisk crcRisk = new CrcRisk(session);
                crcRisk.setCxKey(custId);
                crcRisk = crcRisk.select();

                JSONObject jsonObject = new JSONObject(details);

                if (crcRisk != null) {
                    CrcRiskHist crcRiskHist = new CrcRiskHist(session);
                    crcRiskHist.setCxKey(custId);
                    //crcRiskHist.from(crcRisk.toJson());
                    crcRiskHist.setCreationTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                    crcRiskHist.setFactName(crcRisk.getFactName());
                    crcRiskHist.setDateTime(crcRisk.getDateTime());
                    crcRiskHist.setSource(crcRisk.getSource());
                    crcRiskHist.setRiskLevel(crcRisk.getRiskLevel());
                    crcRiskHist.setModuleId(crcRisk.getModuleId());
                    crcRiskHist.setScore(crcRisk.getScore());
                    crcRiskHist.setEvidence(crcRisk.getEvidence());
                    crcRiskHist.setPcFlag(crcRisk.getPcFlag());
                    crcRiskHist.setMigratedFlag(crcRisk.getMigratedFlag());
                    crcRiskHist.setUserId(crcRisk.getUserId());


                    if (crcRiskHist.insert() == 1) {
                        crcRisk.setDateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                        crcRisk.setSource("user");
                        crcRisk.setUserId("userId");
                        crcRisk.setRiskLevel(jsonObject.optString("riskLevel"));

                        if (crcRisk.update() != 1) {
                            throw (new KycException("Failed to verify the changes. Please try again after some time."));
                        }
                    }
                } else {
                    crcRisk = new CrcRisk();
                    crcRisk.setCxKey(custId);
                    crcRisk.setDateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                    crcRisk.setSource("user");
                    crcRisk.setUserId("userId");
                    crcRisk.setRiskLevel(jsonObject.optString("riskLevel"));
                    if (crcRisk.insert() != 1) {
                        throw (new KycException("Failed to verify the changes. Please try again after some time."));
                    }
                }

                session.commit();
                return "Success";
            } catch (RdbmsException e) {
                throw new KycException("Exception while processing your request. Please contact support staff.");
            }
        }
        throw new KycException("Unable to acquire session, Please try again after some time");
    }

    public String updateUserProfile(String custId, String details, RDBMSSession session) throws KycException{
        try {
            AmlCustomerView cust = new AmlCustomerView(session);
            cust.setCustId(custId);
            cust = cust.select();

            if (cust == null)
                throw new KycException("User not found !!");

            JSONObject jsonObject = new JSONObject(details);

            /*cust.setRelatedParty(jsonObject.optString("relatedParty"));*/
            CxConnection con = session.getCxConnection();

            con.query("update customer set BankRelatedFamily = '" + jsonObject.optString("bankRelatedFamily") + "' where cxcifid = '" + custId + "'", "s", new Object[]{},
                    rs -> {

                    });

            return "Success";
            //return "Customer doesn't exist !!";
        } catch (Exception e) {
            return "Exception while processing your request.";
        }
    }
}
