clari5.kyc.entity{
	cust-kyc-cng-his {
		attributes = [
			{ name : custId, type ="string:50" , key=true}

			{ name : update-type, type = "string:20"}
			{ name : updates, type = "string:400" }
			{ name : created-date, type : timestamp }
			{ name : created-userId, type = "string:50" }
			{ name : verify-date, type : timestamp , key=true}
            { name : vrifier-userId, type = "string:50" }
            { name : is-accepted, type = "string:50" }
            { name : remark, type = "string:400" }
		]
    }
}