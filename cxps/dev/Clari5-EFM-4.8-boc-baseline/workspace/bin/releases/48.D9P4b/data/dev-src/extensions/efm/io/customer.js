/* customer.js start */
var oTable;
var jsonObj;

var custSrch = "";
var custOutput = "";
var employeeTab = "";
var customerTab = "";
var accountTab = "";

custSrch = "<div class='heading' ><h3>Customer Search</h3></div>";

custSrch += "<form id='custTemplate'><table id='custListD' cellpadding='3' cellspacing='3'>"
    + "<tr class='separator'><td class='fontSize'>Customer ID</td>"
    + "<tr class='separator'><td class='fontSize'><input type='text' value='' class='validate[groupRequiredHighlight]' id='custId' size='20'/></td></tr>"
    + "<tr class='separator'><td class='fontSize'>PAN/TIN</td>"
    + "<tr class='separator'><td class='fontSize'><input type='text' value='' class='' id='taxId' size='20'/></td></tr>"
    + "<tr class='separator'><td class='fontSize'>Email Id</td></tr>"
    + "<tr class='separator'><td class='fontSize'><input type='text' size='20' class='' id='emailId'></td></tr>"
    + "<tr class='separator'><td class='fontSize'>Customer Name</td></tr>"
    + "<tr class='separator'><td class='fontSize'><input type='text' id='custName' class='' size='20'></td></tr>"
    + "<tr class='separator'><td class='fontSize'>Phone</td></tr>"
    + "<tr class='separator'><td class='fontSize'><input type='text' id='phnNum' class='validate[custom[phone]]' size='20'></td></tr>"
    + "<tr class='separator'><td class='fontSize'></tr>"
    + "<tr class='separator'><td class='fontSize'><a class='abutton search-w-def' onClick='loadCustData(this)' style='margin-left:50px;'>Search</a></td></tr></table></form>";

custOutput = "<div id='custSrchLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Customer Details</h3></div><div id='tabCust'  style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id='custSrchDtls' class='dataTableCss' style='text-align:left;' ></table></div></div></div>";

var custTreeData = "";


function initCust() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    $("#custSrchLink").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closeCustEvidence(custCount);
        }
    };
}

function requiredOneOfGroup(field, rules, i, options) {
    var fieldsWithValue = $('input[type=text]').filter(function () {
        if ($(this).val().length > 0) {
            return true;
        }
    });
    if (fieldsWithValue.length < 1) {
        return "At least one field in this set must have a value.";
    }
}

var custId = null;
var custName = null;
var taxId = null;
var passNum = null;
var emailId = null;
var adhNum = null;
var pinCode = null;
var phnNum = null;

function loadCustData(custId) {
    custId = ($("#custId").val() != "") ? $("#custId").val().trim() : null;
    custName = ($("#custName").val() != "") ? $("#custName").val().trim() : null;
    taxId = ($("#taxId").val() != "") ? $("#taxId").val().trim() : null;
    emailId = ($("#emailId").val() != "") ? $("#emailId").val().trim() : null;
    phnNum = ($("#phnNum").val() != "") ? $("#phnNum").val().trim() : null;
    if (srchValidation()) {
        var custObject = {
            "custId": custId,
            "pan": taxId,
            "email": emailId,
            "custName": custName,
            "mobile1": phnNum
        };
        //getCustDtlsOnValidate(custObject);

        var ifFormFilled = false;
        $("#analDiv").hide();
        if (custId != null)
            ifFormFilled = true;
        if (taxId != null)
            ifFormFilled = true;
        if (emailId != null)
            ifFormFilled = true;
        if (custName != null)
            ifFormFilled = true;
        if (ifFormFilled) {
            $("#custId").removeClass("validate[groupRequiredHighlight]");
            $("#custId").addClass("validate[custom[oneSpcLetterNumber]]");
            $("#custName").addClass("validate[custom[onlyLetterSp,minSize[3]]]");
            $("#taxId").addClass("validate[custom[onlyLetterNumber]]");
            $("#emailId").addClass("validate[custom[email]]");
            var isCond = $("#custTemplate").validationEngine('validate', {scroll: false});
            if (isCond) {
                getCustomerDetails(custObject, custRespHandler, true);
            }
        } else {
            $("#custId").removeClass("validate[custom[oneSpcLetterNumber]]");
            $("#custName").removeClass("validate[custom[onlyLetterSp,minSize[3]]]");
            $("#taxId").removeClass("validate[custom[onlyLetterNumber]]");
            $("#emailId").removeClass("validate[custom[email]]");
            $("#custId").addClass("validate[groupRequiredHighlight]");
            var isTrue = $("#custTemplate").validationEngine('validate', {scroll: false});
        }

    }
}

function srchValidation() {
    if (custName == null && custId == null && taxId == null && emailId == null) {
        $("#custId").addClass("hightlightDiv");
        $("#custName").addClass("hightlightDiv");
        $("#taxId").addClass("hightlightDiv");
        $("#emailId").addClass("hightlightDiv");
        return true;
    }
    else {
        $("#custId").removeClass("hightlightDiv");
        $("#custName").removeClass("hightlightDiv");
        $("#taxId").removeClass("hightlightDiv");
        $("#emailId").removeClass("hightlightDiv");
        return true;
    }
}

// function getCustTreeData(cCount,id){
// custTreeData="";
// custRmlId="customerRelam"+cCount;
// custTreeData+="<div id=\"custTree"+cCount+"\" class='treeLeftPanel' ><div id=\"custmContainer"+cCount+"\" style='margin-left:-6px;overflow-x:auto;height:auto;'></div></div><div class='treeRightPanel' ><div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id="+id+"  >Back</a></div><div id=\"custLink"+cCount+"\" style='width:100%'><div class='heading' ><h3>Customer Details</h3></div><table id=\"custDataTable"+cCount+"\" class='attrHTbl' style='height:130px;' ><tr><td>Customer Name</td><td>:</td><td id=\"custName_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;'>Constitution</td><td>:</td><td id=\"constut_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Gender</td><td>:</td><td id=\"gender_"+cCount+"\"></td></tr><tr><td style='border-left:1px solid #c0c0c0;' >PAN Number</td><td>:</td><td id=\"panNum_"+cCount+"\"></td style='border-left:1px solid #c0c0c0;'><td style='border-left:1px solid #c0c0c0;'>Phone</td><td>:</td><td id=\"phnNum_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Aadhar ID</td><td>:</td><td id=\"adhId_"+cCount+"\"></td></tr><tr><td>Date of Birth</td><td>:</td><td id=\"dob_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Date of Incorporation</td><td>:</td><td id=\"doi_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Email ID</td><td>:</td><td id=\"emailId_"+cCount+"\"></td></tr>"+
// "<tr><td style='white-space:nowrap;'>Customer Segment</td><td>:</td><td id=\"custSeg_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Nature of Business</td><td>:</td><td id=\"nob_"+cCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Annual Income</td><td>:</td><td id=\"ainc_"+cCount+"\"></td></tr><tr><td style='border-left:1px solid #c0c0c0;' >Address</td><td>:</td><td colspan='6'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:500px;' id=\"addss_"+cCount+"\" ></div></td></tr><tr style='height:10px'></tr></table></div>";

// custTreeData+="</br><div id=\"custMenu"+cCount+"\" style='width:100%;margin-top:-10px;' ><ul id=\"custBar"+cCount+"\" ><li ><a onClick=\"loadCustomerSFacts('"+cCount+"')\" href=\"#custtab1_"+cCount+"\" oncontextmenu='return false;' >Intelligence</a></li><li ><a onClick=\"loadAccountDetails('"+cCount+"')\" href=\"#custtab3_"+cCount+"\" oncontextmenu='return false;' >Accounts</a></li>"
//                +"<li ><a onClick=\"loadCustomerCaseDtls('"+cCount+"')\" href=\"#custtab2_"+cCount+"\" oncontextmenu='return false;'>Cases</a></li>"//<li><a onClick=\"loadTranDetails('"+cCount+"')\" href=\"#custtab4_"+cCount+"\" oncontextmenu='return false;'>Transactions</a></li></ul>"
//                +"<div id=\"custtab1_"+cCount+"\" style='width:100%; height:58%;background:#FFFFFF;overflow-y:auto;' ><div class='overlay' id=\"custOverlay"+cCount+"\" style='display:none;'></div><div class='boxTab' id=\"custBox"+cCount+"\" style='border:4px solid #000;' ><a class='boxclose' id=\"boxclose"+cCount+"\" onclick=\"closeCustEvidence('"+cCount+"')\" href=# ></a><br/><div id='evidenceDtls' style='height:282px;margin:-37px -28px -26px -28px;overflow:auto;' ><div id='CustevdLink1' style=\"margin:3px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\"><div class='heading' id=\"head"+cCount+"\"></div><div id=\"preData"+cCount+"\" ></div><div id=\"custEvidBlock"+cCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custEvidence"+cCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div>"

// +"<div id='CustevdLink2' style=\"margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\"><div class='heading' id=\"head"+(cCount+1)+"\" ></div><div id=\"preData"+(cCount+1)+"\" ></div><div id=\"custEvidBlock"+(cCount+1)+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custEvidence"+(cCount+1)+"\" class='dataTableCss' style='text-align:left;'></table></div></div><div id='CustevdLink3' style=\"margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\"><div class='heading' id=\"head"+(cCount+2)+"\"></div><div id=\"preData"+(cCount+2)+"\" ></div><div id=\"custEvidBlock"+(cCount+2)+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custEvidence"+(cCount+2)+"\" class='dataTableCss' style='text-align:left;'></table></div></div><div id='CustevdLink4' style=\"margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\"><div class='heading' id=\"head"+(cCount+3)+"\" ></div><div id=\"preData"+(cCount+3)+"\" ></div><div id=\"custEvidBlock"+(cCount+3)+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custEvidence"+(cCount+3)+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div></div>"

// //<div id='evidenceDtls' style='height:285px;weight:75px;margin:-37px -28px -26px -28px;overflow:auto;' ></div></div>

// +"<div id='CustSftLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Intelligence</h3></div><div id=\"tabCustSoft"+cCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custSftDtls"+cCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div>"	
//              +"<div id=\"custtab2_"+cCount+"\" style='width:100%;height:58%;display:none;background:#FFFFFF;overflow-y:auto;' ><div id='custothCaseLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Cases</h3></div><div id=\"tabOthCust"+cCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"othCaseCustDtls"+cCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div>"
//             +"<div id=\"custtab3_"+cCount+"\" style='width:100%;height:58%;display:none;background:#FFFFFF;overflow-y:auto;' ><div id='custAcctLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Accounts</h3></div><div id=\"tabAcctCust"+cCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custAcctDtls"+cCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div>"
//             +"<div id=\"custtab4_"+cCount+"\" style='width:100%;height:58%;display:none;background:#FFFFFF;overflow-y:auto;' ><div id='custTranLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Transactions</h3></div><div id=\"tabTranCust"+cCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"custTranDtls"+cCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div></div></div>";

// return custTreeData;
// }


function getCustTreeData(cCount, id) {
    custTreeData = "";
    custRmlId = "customerRelam" + cCount;
    custTreeData += "<div id=\"custTree" + cCount + "\" class='treeLeftPanel' >";
    custTreeData += "<div id=\"custmContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;height:auto;'>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div class='treeRightPanel' >";
    custTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
    custTreeData += "</div>";
    custTreeData += "<div id=\"custLink" + cCount + "\" style='width:100%'>";
    custTreeData += "<div class='heading' ><h3>Customer Details</h3>";
    custTreeData += "</div>";
    custTreeData += "<table id=\"custDataTable" + cCount + "\" class='attrHTbl' style='height:200px;overflow-y:scroll;overflow-x:scroll;display:block;' >";
    custTreeData += "<tr><td>Customer Name</td><td>:</td><td id=\"custName_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;'>Constitution</td><td>:</td><td id=\"constut_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Gender</td><td>:</td><td id=\"gender_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    custTreeData += "<tr><td style='border-left:1px solid #c0c0c0;' >PAN Number</td><td>:</td><td id=\"panNum_" + cCount + "\"></td style='border-left:1px solid #c0c0c0;'>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;'>Phone</td><td>:</td><td id=\"phnNum_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Aadhar ID/NIC</td><td>:</td><td id=\"adhId_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    custTreeData += "<tr><td>Date of Birth</td><td>:</td><td id=\"dob_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Date of Incorporation</td><td>:</td><td id=\"doi_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Email ID</td><td>:</td><td id=\"emailId_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    custTreeData += "<tr><td style='white-space:nowrap;'>Customer Segment</td><td>:</td><td id=\"custSeg_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Nature of Business</td><td>:</td><td id=\"nob_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Annual Income</td><td>:</td><td id=\"ainc_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    // New Line started
    custTreeData += "<tr><td style='white-space:nowrap;'>Facility Numer</td><td>:</td><td id=\"FacilityNumer_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Fax</td><td>:</td><td id=\"fax_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Monthly Income</td><td>:</td><td id=\"monthlyIncome_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    
    custTreeData += "<tr><td style='white-space:nowrap;'>Personal Or Not Personal</td><td>:</td><td id=\"personalNotPersonal_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Customer Id</td><td>:</td><td id=\"custId_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Host Id</td><td>:</td><td id=\"hostId_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    
    custTreeData += "<tr><td style='white-space:nowrap;'>Pawning Ticket Number</td><td>:</td><td id=\"pawningTicketNumber_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Deal Number</td><td>:</td><td id=\"dealNo_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Account Number</td><td>:</td><td id=\"AccountNumber_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    
    custTreeData += "<tr><td style='white-space:nowrap;'>Treasury Bill Number</td><td>:</td><td id=\"treasuryBillNum_" + cCount + "\"></td>";
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Address</td><td>:</td><td id=\"addss_" + cCount + "\"></td>";   
    custTreeData += "<td style='border-left:1px solid #c0c0c0;' >Locker Number</td><td>:</td><td id=\"LockerNumber_" + cCount + "\"></td>";
    custTreeData += "</tr>";
    // New Line ended
    //custTreeData += "<tr><td style='border-left:1px solid #c0c0c0;' >Address</td><td>:</td>";
    //custTreeData += "<td colspan='6'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:500px;' id=\"addss_" + cCount + "\" ></div></td>";
    //custTreeData += "</tr>";
    custTreeData += "<tr style='height:10px'></tr>";
    custTreeData += "</table>";
    custTreeData += "</div>";


    custTreeData += "</br>";
    custTreeData += "<div id=\"custMenu" + cCount + "\" style='width:100%;margin-top:-10px;' ><ul id=\"custBar" + cCount + "\" >";
    custTreeData += "<li ><a onClick=\"loadCustomerSFacts('" + cCount + "')\" href=\"#custtab1_" + cCount + "\" oncontextmenu='return false;' >Intelligence</a></li>";
    custTreeData += "<li ><a onClick=\"loadAccountDetails('" + cCount + "')\" href=\"#custtab3_" + cCount + "\" oncontextmenu='return false;' >Accounts</a></li>";
    custTreeData += "<li ><a onClick=\"loadCustomerCaseDtls('" + cCount + "')\" href=\"#custtab2_" + cCount + "\" oncontextmenu='return false;'>Cases</a></li>"//<li><a onClick=\"loadTranDetails('"+cCount+"')\" href=\"#custtab4_"+cCount+"\" oncontextmenu='return false;'>Transactions</a></li></ul>"
    custTreeData += "<div id=\"custtab1_" + cCount + "\" style='width:100%; height:58%;background:#FFFFFF;overflow-y:auto;' >";
    custTreeData += "<div class='overlay' id=\"custOverlay" + cCount + "\" style='display:none;'>";
    custTreeData += "</div>";
    custTreeData += "<div class='boxTab' id=\"custBox" + cCount + "\" style='border:4px solid #000;' >";
    custTreeData += "<a class='boxclose' id=\"boxclose" + cCount + "\" onclick=\"closeCustEvidence('" + cCount + "')\" href=# ></a><br/>";
    custTreeData += "<div id='evidenceDtls' style='height:282px;margin:-37px -28px -26px -28px;overflow:auto;' >";

    custTreeData += "<div id='CustevdLink1' style=\"margin:3px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
    custTreeData += "<div class='heading' id=\"head" + cCount + "\"></div><div id=\"preData" + cCount + "\" >";
    custTreeData += "</div>";
    custTreeData += "<div id=\"custEvidBlock" + cCount + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custEvidence" + cCount + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div id='CustevdLink2' style=\"margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
    custTreeData += "<div class='heading' id=\"head" + (cCount + 1) + "\" ></div><div id=\"preData" + (cCount + 1) + "\" >";
    custTreeData += "</div>";
    custTreeData += "<div id=\"custEvidBlock" + (cCount + 1) + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custEvidence" + (cCount + 1) + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div id='CustevdLink3' style=\"margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
    custTreeData += "<div class='heading' id=\"head" + (cCount + 2) + "\"></div><div id=\"preData" + (cCount + 2) + "\" >";
    custTreeData += "</div>";
    custTreeData += "<div id=\"custEvidBlock" + (cCount + 2) + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custEvidence" + (cCount + 2) + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div id='CustevdLink4' style=\"margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
    custTreeData += "<div class='heading' id=\"head" + (cCount + 3) + "\" ></div><div id=\"preData" + (cCount + 3) + "\" >";
    custTreeData += "</div>";
    custTreeData += "<div id=\"custEvidBlock" + (cCount + 3) + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custEvidence" + (cCount + 3) + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "</div>";
    custTreeData += "</div>";

//<div id='evidenceDtls' style='height:285px;weight:75px;margin:-37px -28px -26px -28px;overflow:auto;' ></div></div>

    custTreeData += "<div id='CustSftLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\">";
    custTreeData += "<div class=\"heading\" ><h3>Intelligence</h3>";
    custTreeData += "</div>";
    custTreeData += "<div id=\"tabCustSoft" + cCount + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custSftDtls" + cCount + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div id=\"custtab2_" + cCount + "\" style='width:100%;height:58%;display:none;background:#FFFFFF;overflow-y:auto;' >";
    custTreeData += "<div id='custothCaseLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\">";
    custTreeData += "<div class=\"heading\" ><h3>Cases</h3>";
    custTreeData += "</div>";
    custTreeData += "<div id=\"tabOthCust" + cCount + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"othCaseCustDtls" + cCount + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div id=\"custtab3_" + cCount + "\" style='width:100%;height:58%;display:none;background:#FFFFFF;overflow-y:auto;' >";
    custTreeData += "<div id='custAcctLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\">";
    custTreeData += "<div class=\"heading\" ><h3>Accounts</h3>";
    custTreeData += "</div>";
    custTreeData += "<div id=\"tabAcctCust" + cCount + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custAcctDtls" + cCount + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "<div id=\"custtab4_" + cCount + "\" style='width:100%;height:58%;display:none;background:#FFFFFF;overflow-y:auto;' >";
    custTreeData += "<div id='custTranLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\">";
    custTreeData += "<div class=\"heading\" ><h3>Transactions</h3></div>";
    custTreeData += "<div id=\"tabTranCust" + cCount + "\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
    custTreeData += "<table id=\"custTranDtls" + cCount + "\" class='dataTableCss' style='text-align:left;'>";
    custTreeData += "</table>";
    custTreeData += "</div>";
    custTreeData += "</div>";
    custTreeData += "</div>";

    custTreeData += "</div>";
    custTreeData += "</div>";

    return custTreeData;
}

function closeCustEvidence(id) {
    $('#custBox' + id).fadeIn('fast', function () {
        $('#custBox' + id).animate({'top': '-303px'}, 5, function () {
            $('#custOverlay' + id).fadeOut('fast');
        });
    });
}

function custRespHandler(resp) {
    var response = resp.custAttrList;
    if (response == "" || response == undefined) {
        displayNotification("Customer ID doesn't exist.", true, "warning");
    } else {
        clearTabs();
        addTab('Customer', '');
        $("#custSrchLink").show();
        var dataObj = makeJsonForDatatable(response, "addTab", "custId", reqstPage);
        displayData(dataObj, "tabCust", "custSrchDtls");
        toggleSrchWindow();
    }
}
var chfObj = "";
var entyObject = "";
var custCount = "";
var customerId = "";
var treeFlag = "N";
function custLoad(custId, tCount) {
    custCount = tCount;
    customerId = custId;
    $("#menuBlock").hide();
    chfObj = {
        "custId": custId
    };
    entyObject = {
        "entityKey": custId
    };

    if (cxcifId != "") {
        treeObj = {
            "custId": custId
        };
    } else {
        cxcifId = custId;
        treeObj = {
            "custId": custId
        };
    }
    getCustomerHardFact(chfObj, custHFactsRespHandler, true);
    getCustomerTreeDetail(treeObj, custTreeRespHandler, true);
}
function getCustomerHardFact(inputObject,callback,refresh){
    var payload = {refresh:refresh, input: inputObject};
    transeiveCustomer("itool", "get_customer_details", inputObject, callback);
}
function getCustomerTreeDetail(inputObject,callback,refresh){
    var payload = {refresh:refresh, input: inputObject};
    transeive("itool", "get_cust_tree", inputObject, callback);
}
function transeiveCustomer(menu, cmd, payload, callback){
    // alert("cxNetwork.js");
    var params = {};
    if(this._user != "" ) {
        params.uId = this._user;
    } else {
        alert("User ID not set.");
        return;
    }
    params.menu = menu;
    params.cmd = cmd;
    params.payload = JSON.stringify(payload);
    params.chId = "web";
    params.token = "dummy";
    var url =  configureCustomer(params.cmd);
    $.ajax({
        url:url,
        type:"POST",
        async:false,
        dataType:'json',
        data:params,
        success: function(response) {
            console.log(response,"data from server");
            callback(response);
        },
        error  : function(a,b,c){
            alert(" error : a "+ a + "b "+b + "c "+ c);
        }
    });
}
function configureCustomer(command){
    if (!allInit) {
        alert("Parameters not set.");
        return;
    }
    var url = this._serverUrl+"/CustomAceServlet";
    return url;
}

function custTreeRespHandler(resp) {
    createCustomerRlm(resp);
    loadJSFile("/cdn/efm/io/js/caseTree.js");
}

function loadCustomerSFacts(tabid) {
    custCount = tabid;
    var cobj = {
        "custId": customerId
    };
    getCustomerSoftFacts(cobj, custSFactsRespHandler, true);
}

function loadCustomerCaseDtls(tabid) {
    custCount = tabid;
    var cobj = {
        "entityKey": customerId
    };
    getOtherCaseDetails(cobj, custDtlsRespHandler, true);
}

function loadAccountDetails(tabid) {
    custCount = tabid;
    var cobj = {
        "custId": customerId
    };
    getcustAccountDetails(cobj, custAccountRespHandler, true);
}

function loadTranDetails(tabid) {
    custCount = tabid;
    var cobj = {
        "custId": customerId
    };
    getcustTranDetails(cobj, custTransactionRespHandler, true);
}

function custHFactsRespHandler(resp) {
    var respObj = resp.customerDetails;
    if (respObj != null && respObj != undefined && respObj != "") {
        $("#custName_" + custCount).text(respObj[0].name != "" ? respObj[0].name : "NA");
        $("#constut_" + custCount).text(respObj[0].constitution != "" ? respObj[0].constitution : "NA");
        $("#gender_" + custCount).text(respObj[0].gender != "" ? respObj[0].gender : "NA");
        $("#panNum_" + custCount).text(respObj[0].pan != "" ? respObj[0].pan : "NA");
        $("#adhId_" + custCount).text(respObj[0].uin != "" ? respObj[0].uin : "NA");
        $("#phnNum_" + custCount).text(respObj[0].mobile1 != "" ? respObj[0].mobile1 : "NA");
        $("#emailId_" + custCount).text(respObj[0].email != "" ? respObj[0].email : "NA");
        $("#addss_" + custCount).text(respObj[0].mailingAddr != "" ? respObj[0].mailingAddr : "NA");
        $("#ainc_" + custCount).text(respObj[0].annualTurnOver != "" ? respObj[0].annualTurnOver : "NA");
        $("#dob_" + custCount).text(respObj[0].dob != "" ? respObj[0].dob : "NA");
        $("#doi_" + custCount).text(respObj[0].dateOfIncorp != "" ? respObj[0].dateOfIncorp : "NA");
        $("#nob_" + custCount).text(respObj[0].natOfBusn != "" ? respObj[0].natOfBusn : "NA");
        $("#custSeg_" + custCount).text(respObj[0].custSeg != "" ? respObj[0].custSeg : "NA");
        //New fields added
        //$("#nic_" + custCount).text(respObj[0].nic != "" ? respObj[0].nic : "NA");
        $("#fax_" + custCount).text(respObj[0].fax != "" ? respObj[0].fax : "NA");
        $("#monthlyIncome_" + custCount).text(respObj[0].monthlyincome != "" ? respObj[0].monthlyincome : "NA");        
        $("#personalNotPersonal_" + custCount).text(respObj[0].PersonalNotPersonal != "" ? respObj[0].PersonalNotPersonal : "NA");
        $("#custId_" + custCount).text(respObj[0].custid != "" ? respObj[0].custid : "NA");        
        $("#hostId_" + custCount).text(respObj[0].hostid != "" ? respObj[0].hostid : "NA");        
        $("#pawningTicketNumber_" + custCount).text(respObj[0].PawningTicketNumber != "" ? respObj[0].PawningTicketNumber : "NA");                
        $("#dealNo_" + custCount).text(respObj[0].DealNo != "" ? respObj[0].DealNo : "NA");        
        $("#AccountNumber_" + custCount).text(respObj[0].AccountNumber != "" ? respObj[0].AccountNumber : "NA");
        $("#treasuryBillNum_" + custCount).text(respObj[0].TreasuryBillNum != "" ? respObj[0].TreasuryBillNum : "NA");        
        $("#FacilityNumer_" + custCount).text(respObj[0].FacilityNumer != "" ? respObj[0].FacilityNumer : "NA");
        $("#LockerNumber_" + custCount).text(respObj[0].LockerNumber != "" ? respObj[0].LockerNumber : "NA");
         
        getCustomerSoftFacts(chfObj, custSFactsRespHandler, true);
    } else {
        clearTabs();
        displayNotification("Please enter correct Customer Number.", true, "warning");
        return;
    }
}

function custSFactsRespHandler(resp) {
    var response = resp.customerFactsList;
    if (response == "" || response == undefined) {
        var dataResp = [];
        var myArray = new Array();
        var tableHeaders = new Array();
        tableHeaders.push({"sTitle": "Intelligence"});
        tableHeaders.push({"sTitle": "Value"});
        tableHeaders.push({"sTitle": "Updated On"});
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp, "tabCustSoft" + custCount, "CustSftDtls" + custCount);
    } else {
        var dataObj = makeJsonForDatatable(response, "showCustEvidence", "factname", customerId);
        displayData(dataObj, "tabCustSoft" + custCount, "CustSftDtls" + custCount);
    }
}

function showCustEvidence(cid, fname) {
    var evdObj = {
        "custId": cid,
        "factName": fname.split("|")[0],
        "source": fname.split("|")[1]
    };
    if (evdObj.factName == "CXPSAML" || evdObj.factName == "GBO")
        displayNotification("The evidence doesn't exist for this FACT NAME...");
    else if (evdObj.source == "RISK" || evdObj.source == "RDE" || evdObj.source == "MSTR")
            displayNotification("The evidence doesn't exist for this FACT NAME...");
    else {
        getCustEvidenceDetails(evdObj, custEvidenceRespHandler, true);
    }
}
function custEvidenceRespHandler(resp) {
    var response = resp.customerEvidence;
    resetCustEvidenceBox();
    /*if(response != "" && response != undefined){  //Added by Gaurav for checking if variable is null and undefined
     resetCustEvidenceBox();
     }*/
    if (response == "" || response == undefined) {
        var dataResp = [];
        var myArray = new Array();
        var tableHeaders = new Array();
        tableHeaders.push({"sTitle": "Opinion Name"});
        tableHeaders.push({"sTitle": "Event ID'S"});
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp, "custEvidBlock" + custCount, "custEvidence" + custCount);
    } else {

        if (response.qualifier != "" && response.qualifier != undefined) {
            var dataObj = makeJsonForDatatable(response.qualifier, "", "", "");
            $('#CustevdLink1').attr("style", "margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
            setCustEvidenceHdr(custCount, "<h3>Qualifier</h3>");
            displayData(dataObj, "custEvidBlock" + custCount, "custEvidence" + custCount);
        }

        if (response.pre != "" && response.pre != undefined) {
            var wldataText = paintCWLTable(response.pre);
            $("#preData" + (custCount + 1)).append(wldataText);
            var jsonRslts = prepareCustPreCondJson(response.pre)
            var dataObj = makeJsonForDatatable(jsonRslts.resultsList, "", "", "");
            dataObj[1] = returnCustHeaders(response.pre);
            $('#CustevdLink2').attr("style", "margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
            setCustEvidenceHdr((custCount + 1), "<h3>Pre-Condition</h3>");
            displayData(dataObj, "custEvidBlock" + (custCount + 1), "custEvidence" + (custCount + 1));
        }

        if (response.scorecard != "" && response.scorecard != undefined) {
            var dataObj = makeJsonForDatatable(response.scorecard, "", "", "");
            $('#CustevdLink3').attr("style", "margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
            setCustEvidenceHdr((custCount + 2), "<h3>ScoreCard</h3>");
            displayData(dataObj, "custEvidBlock" + (custCount + 2), "custEvidence" + (custCount + 2));
        }

        if (response.post != "" && response.post != undefined) {
            var wldataText = paintCWLTable(response.post);
            $("#preData" + (custCount + 3)).append(wldataText);
            var jsonRslts = prepareCustPostCondJson(response.post);
            var dataObj = makeJsonForDatatable(jsonRslts.resultsList, "", "", "");
            dataObj[1] = returnCustHeaders(response.post);
            $('#CustevdLink4').attr("style", "margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
            setCustEvidenceHdr((custCount + 3), "<h3>Post-Condition</h3>");
            displayData(dataObj, "custEvidBlock" + (custCount + 3), "custEvidence" + (custCount + 3));
        }

        for (var t = 0; t < response.length; t++) {
            var headerText = "";
            if (response[t].blockName != "" && response[t].blockName != undefined) {
                headerText = (response[t].blockName == "opinion" ? "Evidence For Trigger" : (response[t].blockName == "AddOn" ? "Evidence For push up" : "Evidence Details"));
                $('#CustevdLink' + (t + 1)).attr("style", "margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
                var dataObj = makeJsonForDatatable(response[t].events, "", "", "");
                if (dataObj && headerText != "") {
                    setCustEvidenceHdr((custCount + t), "<h3>" + headerText + "</h3>");
                    displayData(dataObj, "custEvidBlock" + (custCount + t), "custEvidence" + (custCount + t));
                }
            }
        }
         if(response && response.length > 0 && response[0].blockName=="opinion" && (response[0].events=="" || response[0].events==undefined)){
            displayNotification("The evidence doesn't exist for this FACT NAME...");
        }
        else{
             if(response.length === 0) {
                 displayNotification("No data available");
             }
             else {
                 $('#tabNavUl li').map(function (i, n) {
                     if ($(n).hasClass("ui-tabs-active")) {
                         var tabPosId = parseInt($(n).attr('id')) + 1;
                         $('#custBox' + tabPosId).show();
                     }
                 });
             }
            // $('#custBox'+custCount).show();
        }
    }
//    $('#custBox' + custCount).fadeIn('fast', function () {
//        $('#custOverlay' + custCount).fadeIn('fast');
//        $('#custBox' + custCount).animate({'top': '200px'}, 5, function () {
//        });
//    });
}

function returnCustHeaders(wldata) {
    var tableHeaders = new Array();
    tableHeaders.push({"sTitle": "Overall Score"});
    tableHeaders.push({"sTitle": "Doc ID"});
    var fieldss = wldata.fields + "";
    var fieldsQry = fieldss.split(",");
    for (var y = 0; y < fieldsQry.length; y++) {
        tableHeaders.push({"sTitle": fieldsQry[y]});
        tableHeaders.push({"sTitle": "Score"});
    }
    return tableHeaders
}

function setCustEvidenceHdr(count, text) {
    $("#head" + count).empty();
    $("#head" + count).append(text);
}

function resetCustEvidenceBox() {
    for (var t = 1; t < 5; t++) {
        $("#CustevdLink" + t).attr("style", "display:none;");
        $("#preData" + t).empty();
    }
}

function prepareCustPostCondJson() {
    var paintPOST = "<table border='1' style='margin-top:7px;width:30%;' >";
    paintPOST += "<tr>";
    paintPOST += "<table border='1' style='margin:0px 3px;' ><tr class='fontFmly' style='background-color:#666676;color:#FFF;'><td>Field</td><td>Risk</td></tr><tr class='fontFmly'><td>" + wldata.field + "</td><td>" + wldata.risklevel + "</td></tr></table></tr>";
    paintPOST += "</table>";
    return paintPOST;
}

function paintCWLTable(wldata) {
    var paintSAM = "<table border='1' style='margin-top:7px;width:30%;' >";
    paintSAM += "<tr>";
    paintSAM += "<table border='1' style='margin:0px 3px;' ><tr class='fontFmly' style='background-color:#666676;color:#FFF;'><td>Field</td><td>WL Name</td><td>Risk</td></tr><tr class='fontFmly' ><td>" + wldata.field + "</td><td>" + wldata.WlName + "</td><td>" + wldata.risklevel + "</td></tr></table></tr><tr class='fontFmly' style='background-color:#666676;color:#FFF;'>";
    var qry = wldata.qry;
    var fields = wldata.fields + "";
    var fieldsQry = fields.split(",");
    paintSAM += "<table border='1' style='margin:5px 3px;width:99%;' ><tr style='font-family: Verdana,Arial,sans-serif;font-size: 0.79em;background-color:#666676;color:#FFF;white-space:break;'><td>OverallScore</td><td>Doc ID</td>";
    for (var y = 0; y < fieldsQry.length; y++) {
        paintSAM += "<td>" + fieldsQry[y] + "</td><td>Score</td>";
    }
    paintSAM += "</tr><tr style='font-family: Verdana,Arial,sans-serif;font-size: 0.79em;white-space:break;' ><td>-</td><td>-</td>";
    for (var y = 0; y < qry.length; y++) {
        if (qry[y] != "")
            paintSAM += "<td>" + qry[y] + "</td><td>-</td>";
        else
            paintSAM += "<td>-</td><td>-</td>";
    }
    paintSAM += "</tr></table>";
    paintSAM += "</tr>";
    paintSAM += "</table>";
    return paintSAM;
}

function prepareCustPreCondJson(resultson) {
    var finalRslts = null;
    var resultsJs = {"resultsList": []};
    var resultJson = resultson.results;
    for (var r = 0; r < resultJson.length; r++) {
        var results = {};
        var innerJson = "{";
        innerJson += "\"OverallScore\"" + ":\"" + resultJson[r].score + "\"," + "\"docid\"" + ":\"" + resultJson[r].id + "\",";
        var matchJson = resultJson[r].matches;
        for (var z = 1; z < (matchJson.length + 1); z++) {
            if (z == 1)
                innerJson += "\"val" + z + "\":\"" + matchJson[0].val + "\"," + "\"score" + z + "\":\"" + matchJson[0].score + "\"";
            else
                innerJson += ",\"val" + z + "\":\"" + matchJson[0].val + "\"," + "\"score" + z + "\":\"" + matchJson[0].score + "\"";
        }
        innerJson += "}";
        resultsJs.resultsList.push(JSON.parse(innerJson));
    }
    return resultsJs;
}


function paintCustSAMTable(samdata) {

    var paintSAM = "<table border='1' style='margin-top:14px;width:99%;' >";
    for (var t = 0; t < samdata.length; t++) {
        if (t == 0) {
            paintSAM += "</tr><tr class='fontFmly' style='background-color:#666676;color:#FFF;'>";
            for (key in samdata[t]) {
                paintSAM += "<td>" + key.trim() + "</td>";
            }
            paintSAM += "</tr><tr class='fontFmly odd' >";
            for (key in samdata[t]) {
                paintSAM += "<td>" + samdata[t][key].trim() + "</td>";
            }
        } else {
            if (t % 2 == 0) {
                paintSAM += "<tr class='fontFmly odd' >";
            } else {
                paintSAM += "<tr class='fontFmly even' >";
            }
            for (key in samdata[t]) {
                paintSAM += "<td>" + samdata[t][key].trim() + "</td>";
            }
        }
        paintSAM += "</tr>";
    }
    paintSAM += "</table>";
    return paintSAM;
}

var moduleArray = new Array();
function custDtlsRespHandler(resp) {
    var response = resp.otherCasesList;
    if (response == "" || response == undefined) {
        var dataResp = [];
        var myArray = new Array();
        var tableHeaders = new Array();
        tableHeaders.push({"sTitle": "Case ID"});
        tableHeaders.push({"sTitle": "Module"});
        tableHeaders.push({"sTitle": "Created Date"});
        //tableHeaders.push({ "sTitle": "Closed Date" });
        //tableHeaders.push({ "sTitle": "Resolution" });
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp, "tabOthCust" + custCount, "othCaseCustDtls" + custCount);
    } else {
        var res = response;
        for (var i = 0; i < res.length; i++) {
            delete res[i]['closedDate'];
            delete res[i]['resolution'];
        }
        var resList = makeJsonForDatatable(res, "addTab", "caseId", "Case");
        parseCaseResponse(response);
        displayData(resList, "tabOthCust" + custCount, "othCaseCustDtls" + custCount);
    }
}
function getModuleFromCaseId(jiraId) {
    return moduleArray[jiraId];
}

function parseCaseResponse(inp) {
    for (var i = 0; i < inp.length; i++) {
        var caseVal = "";
        for (key in inp[i]) {
            if (key == "caseId") {
                caseVal = inp[i][key];
            } else if (key == "module") {
                moduleArray[caseVal] = inp[i][key];
            }
        }
    }
}

function custAccountRespHandler(resp) {
    var response = resp.accountList;
    if (response == "" || response == undefined) {
        var dataResp = [];
        var myArray = new Array();
        var tableHeaders = new Array();
        tableHeaders.push({"sTitle": "Account Id"});
        tableHeaders.push({"sTitle": "Account Name"});
        tableHeaders.push({"sTitle": "Created Date"});
        tableHeaders.push({"sTitle": "Account Type"});
        tableHeaders.push({"sTitle": "Scheme Code"});
        tableHeaders.push({"sTitle": "Account Status"});
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp, "tabAcctCust" + custCount, "CustAcctDtls" + custCount);
    } else {
        var dataObj = makeJsonForDatatable(response, "addTab", "acctId", "Account");
        displayData(dataObj, "tabAcctCust" + custCount, "CustAcctDtls" + custCount);
    }
}

function custTransactionRespHandler(resp) {
    var response = resp.customerTransactionDetails;
    if (response == "" || response == undefined) {
        var dataResp = [];
        var myArray = new Array();
        var tableHeaders = new Array();
        tableHeaders.push({"sTitle": "Tran Id"});
        tableHeaders.push({"sTitle": "Account Id"});
        tableHeaders.push({"sTitle": "Tran Date"});
        tableHeaders.push({"sTitle": "Tran Amount"});
        tableHeaders.push({"sTitle": "Tran Type"});
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp, "tabTranCust" + custCount, "custTranDtls" + custCount);
    } else {
        var resList = makeJsonForDatatable(response, "addTab", "tranId", "Transaction");
        displayData(resList, "tabTranCust" + custCount, "custTranDtls" + custCount);
    }
}

function createCustomerRlm(custTreeJson) {
    customerTab = "";
    accountTab = "";
    employeeTab = "";
    var custTreeVal = custTreeJson.jointCustIds;
    if (custTreeVal != undefined) {
        for (var t = 0; t < custTreeVal.length; t++) {
            if (t == 0) {
                customerTab += ",\"children\":[{\"text\":\"" + custTreeVal[t] + "\",\"id\":\"" + custCount + "#C#" + custTreeVal[t] + "\",\"parent\":\"customer\",\"icon\":\"/cdn/efm/common/images/jstree/file.png\",\"type\":\"folder\",\"state\":{\"opened\":false},\"metadata\":{\"nodeData\":{},\"editable\":false}}";
            } else {
                customerTab += ",{\"text\":\"" + custTreeVal[t] + "\",\"id\":\"" + custCount + "#C#" + custTreeVal[t] + "\",\"parent\":\"customer\",\"icon\":\"/cdn/efm/common/images/jstree/file.png\",\"type\":\"folder\",\"state\":{\"opened\":false},\"metadata\":{\"nodeData\":{},\"editable\":false}}";
            }
        }
    }
    var empTreeVal = custTreeJson.empIds;
    if (empTreeVal != undefined) {
        for (var t = 0; t < empTreeVal.length; t++) {
            if (t == 0) {
                employeeTab += ",\"children\":[{\"text\":\"" + empTreeVal[t] + "\",\"id\":\"" + empCount + "#S#" + empTreeVal[t] + "\",\"parent\":\"employee\",\"icon\":\"/cdn/efm/common/images/jstree/file.png\",\"type\":\"folder\",\"state\":{\"opened\":false},\"metadata\":{\"nodeData\":{},\"editable\":false}}";
            } else {
                employeeTab += ",{\"text\":\"" + empTreeVal[t] + "\",\"id\":\"" + empCount + "#S#" + empTreeVal[t] + "\",\"parent\":\"employee\",\"icon\":\"/cdn/efm/common/images/jstree/file.png\",\"type\":\"folder\",\"state\":{\"opened\":false},\"metadata\":{\"nodeData\":{},\"editable\":false}}";
            }
        }
    }
    var acctTreeVal = custTreeJson.acctIds;
    if (acctTreeVal != undefined) {
        for (var t = 0; t < acctTreeVal.length; t++) {
            if (t == 0) {
                accountTab += ",\"children\":[{\"text\":\"" + acctTreeVal[t] + "\",\"id\":\"" + custCount + "#A#" + acctTreeVal[t] + "\",\"parent\":\"customer\",\"icon\":\"/cdn/efm/common/images/jstree/file.png\",\"type\":\"folder\",\"state\":{\"opened\":false},\"metadata\":{\"nodeData\":{},\"editable\":false}}";
            } else {
                accountTab += ",{\"text\":\"" + acctTreeVal[t] + "\",\"id\":\"" + custCount + "#A#" + acctTreeVal[t] + "\",\"parent\":\"customer\",\"icon\":\"/cdn/efm/common/images/jstree/file.png\",\"type\":\"folder\",\"state\":{\"opened\":false},\"metadata\":{\"nodeData\":{},\"editable\":false}}";
            }
        }
    }
    customerTab += (customerTab.length > 0) ? "]" : "";
    accountTab += (accountTab.length > 0) ? "]" : "";
    employeeTab += (employeeTab.length > 0) ? "]" : "";
}

function validateFormFields(formName) {
    return $("#" + formName).validationEngine('validate');
}
/* customer.js end */
