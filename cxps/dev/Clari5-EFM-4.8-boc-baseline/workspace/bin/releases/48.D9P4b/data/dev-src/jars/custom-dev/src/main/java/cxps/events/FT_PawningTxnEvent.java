// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_ft_PAWNING", Schema="rice")
public class FT_PawningTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String txnCurrency;
       @Field(size=20) public String acctId;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String keys;
       @Field(size=50) public String custId;
       @Field(size=20) public String sysCountry;
       @Field public java.sql.Timestamp eventTime;
       @Field(size=20) public String channelId;
       @Field(size=50) public String bR;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String cxAcctId;
       @Field(size=11) public Double txnAmt;
       @Field(size=20) public String hostUserId;
       @Field(size=100) public String addEntityId2;
       @Field(size=20) public String txnId;
       @Field(size=100) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=100) public String addEntityId3;
       @Field(size=100) public String eventSubtype;
       @Field(size=20) public String txnType;
       @Field(size=20) public String lcyTxnCurncy;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=50) public String nIC;
       @Field(size=20) public String txnSrlNo;
       @Field(size=11) public Double lcyTxnAmt;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String txnSubType;


    @JsonIgnore
    public ITable<FT_PawningTxnEvent> t = AEF.getITable(this);

	public FT_PawningTxnEvent(){}

    public FT_PawningTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("PawningTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTxnCurrency(json.getString("Tran_curr"));
            setAcctId(json.getString("AccountId"));
            setMsgSource(json.getString("source"));
            setKeys(json.getString("keys"));
            setCustId(json.getString("CustomerID"));
            setSysCountry(json.getString("SYSTEMCOUNTRY"));
            setEventTime(EventHelper.toTimestamp(json.getString("eventts")));
            setChannelId(json.getString("Channel"));
            setBR(json.getString("BR"));
            setSystemTime(EventHelper.toTimestamp(json.getString("Systime")));
            setTxnDrCr(json.getString("part-tran-type"));
            setCxAcctId(json.getString("cx-acct-id"));
            setTxnAmt(EventHelper.toDouble(json.getString("Tran_amt")));
            setHostUserId(json.getString("host_user_id"));
            setAddEntityId2(json.getString("reservedfield2"));
            setTxnId(json.getString("TxnID"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("Tran_Date")));
            setAddEntityId3(json.getString("reservedfield3"));
            setEventSubtype(json.getString("eventsubtype"));
            setTxnType(json.getString("tran-type"));
            setLcyTxnCurncy(json.getString("LCY_TRAN_CURR"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setNIC(json.getString("NIC"));
            setTxnSrlNo(json.getString("Part_tran_Srl_Num"));
            setLcyTxnAmt(EventHelper.toDouble(json.getString("LCY_TRAN_AMT")));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event-name"));
            setTxnSubType(json.getString("tran-sub-type"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FP"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTxnCurrency(){ return txnCurrency; }

    public String getAcctId(){ return acctId; }

    public String getMsgSource(){ return msgSource; }

    public String getKeys(){ return keys; }

    public String getCustId(){ return custId; }

    public String getSysCountry(){ return sysCountry; }

    public java.sql.Timestamp getEventTime(){ return eventTime; }

    public String getChannelId(){ return channelId; }

    public String getBR(){ return bR; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getCxAcctId(){ return cxAcctId; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getHostUserId(){ return hostUserId; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getTxnId(){ return txnId; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getTxnType(){ return txnType; }

    public String getLcyTxnCurncy(){ return lcyTxnCurncy; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getNIC(){ return nIC; }

    public String getTxnSrlNo(){ return txnSrlNo; }

    public Double getLcyTxnAmt(){ return lcyTxnAmt; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getTxnSubType(){ return txnSubType; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTxnCurrency(String val){ this.txnCurrency = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setSysCountry(String val){ this.sysCountry = val; }
    public void setEventTime(java.sql.Timestamp val){ this.eventTime = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setBR(String val){ this.bR = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setTxnId(String val){ this.txnId = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setTxnType(String val){ this.txnType = val; }
    public void setLcyTxnCurncy(String val){ this.lcyTxnCurncy = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setNIC(String val){ this.nIC = val; }
    public void setTxnSrlNo(String val){ this.txnSrlNo = val; }
    public void setLcyTxnAmt(Double val){ this.lcyTxnAmt = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setTxnSubType(String val){ this.txnSubType = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_PawningTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_PawningTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "PawningTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}