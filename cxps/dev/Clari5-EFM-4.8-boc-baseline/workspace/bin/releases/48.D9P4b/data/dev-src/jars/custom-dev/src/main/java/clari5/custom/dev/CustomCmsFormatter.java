package clari5.custom.dev;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import cxps.eoi.IncidentEvent;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by deepak on 21/8/17.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public String getCaseAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        String moduleId = caseEvent.getModuleId();
        CmsModule cmsModule = (CmsModule) cmsJira.get(moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        //  System.out.println("for department "+departmentMap);
        String userGroup = cmsModule.getUserGroup();

        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
        System.out.println("Fetching case Assigne " + userCasesMap);
        for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
            System.out.println(" User " + entry.getKey() + " noofcases " + entry.getValue());
        }
       /* for(Map.Entry<String,Integer> entry : userCasesMap.entrySet()){
            if(entry.getValue() < noOfCases){
                noOfCases = entry.getValue();
                resNextUser = entry.getKey();
            }
        }
        System.out.println("next user for incident"+resNextUser);
        return resNextUser;
        */
        resNextUser = getUserWithLeastCase(userCasesMap);
        int count = userCasesMap.get(resNextUser);
        count += 1;
        userCasesMap.put(resNextUser, count);
        System.out.println("Nex user selected" + resNextUser);
        return resNextUser;
    }


    @Override
    public String getIncidentAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");

            throw new CmsException("Unable to get resource cmsjira");
        }
        CmsModule cmsModule = (CmsModule) cmsJira.get(caseEvent.getModuleId());
        Map<String, String> departmentMap = cmsModule.getDepartments();
        System.out.println("for module id  " + cmsModule);
        String userGroup = cmsModule.getUserGroup();
        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
        System.out.println("Check user group" + userGroup);
        System.out.println("Check user cases map" + userCasesMap);

        for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
            System.out.println(" cases " + entry.getKey() + " noofincident " + entry.getValue());
        }


      /*  for(Map.Entry<String,Integer> entry : userCasesMap.entrySet()){
            if(entry.getValue() < noOfCases){
                noOfCases = entry.getValue();
                resNextUser = entry.getKey();

            }
        }

       System.out.println("Next user for case "+resNextUser);
        return resNextUser;
    */
        resNextUser = getUserWithLeastCase(userCasesMap);
        int count = userCasesMap.get(resNextUser);
        count += 1;
        userCasesMap.put(resNextUser, count);
        System.out.println("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public String getUserWithLeastCase(Map<String, Integer> map) {
        Set<Entry<String, Integer>> set = map.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        for (Map.Entry<String, Integer> entry : list) {
            System.out.println(entry.getKey() + " ==== " + entry.getValue());
        }
        String user = list.get(0).getKey();

        System.out.println("User with least no of cases " + user);
        return user;
    }

    public static String checkSummarylength(String summary) {
        if (summary.length() > 250) {
            summary = summary.substring(0, 251) + "...";
            return summary;
        } else {
            return summary;
        }
    }


    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException {
        CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
        CxJson json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();
        String eventJson = event.convertToJson(event);
        System.out.println("Display Key: " + event.getCaseDisplayKey());
        Hocon h = new Hocon(eventJson);
        CxJson wrapper = json.get("fields");
        String displayKey = h.getString("displayKey");
        String summary = displayKey + ":" + wrapper.getString(summaryId);
        wrapper.put("summary", checkSummarylength(summary));
        return json.put("fields", wrapper);
    }
}
