package clari5.custom.boc.integration.data;

public class FT_AccountTxn extends ITableData {

    private String tableName = "FT_ACCT_TXN";
    private String event_type = "FT_AccountTxn";

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String EVENT_ID;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }



    public String getEventdate() {
        return eventdate;
    }

    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getBeneficiaryname() {
        return beneficiaryname;
    }

    public void setBeneficiaryname(String beneficiaryname) {
        this.beneficiaryname = beneficiaryname;
    }

    public String getZonedate() {
        return zonedate;
    }

    public void setZonedate(String zonedate) {
        this.zonedate = zonedate;
    }

    public String getFcnrflag() {
        return fcnrflag;
    }

    public void setFcnrflag(String fcnrflag) {
        this.fcnrflag = fcnrflag;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getOnlinebatch() {
        return onlinebatch;
    }

    public void setOnlinebatch(String onlinebatch) {
        this.onlinebatch = onlinebatch;
    }

    public String getTransubtype() {
        return transubtype;
    }

    public void setTransubtype(String transubtype) {
        this.transubtype = transubtype;
    }

    public String getCounterpartybusiness() {
        return counterpartybusiness;
    }

    public void setCounterpartybusiness(String counterpartybusiness) {
        this.counterpartybusiness = counterpartybusiness;
    }

    public String getTxnreftype() {
        return txnreftype;
    }

    public void setTxnreftype(String txnreftype) {
        this.txnreftype = txnreftype;
    }

    public String getSystemtime() {
        return systemtime;
    }

    public void setSystemtime(String systemtime) {
        this.systemtime = systemtime;
    }

    public String getTxndrcr() {
        return txndrcr;
    }

    public void setTxndrcr(String txndrcr) {
        this.txndrcr = txndrcr;
    }

    public String getAccttype() {
        return accttype;
    }

    public void setAccttype(String accttype) {
        this.accttype = accttype;
    }

    public String getHostuserid() {
        return hostuserid;
    }

    public void setHostuserid(String hostuserid) {
        this.hostuserid = hostuserid;
    }

    public String getHostid() {
        return hostid;
    }

    public void setHostid(String hostid) {
        this.hostid = hostid;
    }

    public String getCounterpartyamount() {
        return counterpartyamount;
    }

    public void setCounterpartyamount(String counterpartyamount) {
        this.counterpartyamount = counterpartyamount;
    }

    public String getTxncode() {
        return txncode;
    }

    public void setTxncode(String txncode) {
        this.txncode = txncode;
    }


    public String getDirectchannelid() {
        return directchannelid;
    }

    public void setDirectchannelid(String directchannelid) {
        this.directchannelid = directchannelid;
    }

    public String getSequencenumber() {
        return sequencenumber;
    }

    public void setSequencenumber(String sequencenumber) {
        this.sequencenumber = sequencenumber;
    }

    public String getTrancrncycode() {
        return trancrncycode;
    }

    public void setTrancrncycode(String trancrncycode) {
        this.trancrncycode = trancrncycode;
    }

    public String getTranverifieddt() {
        return tranverifieddt;
    }

    public void setTranverifieddt(String tranverifieddt) {
        this.tranverifieddt = tranverifieddt;
    }

    public String getCounterpartybic() {
        return counterpartybic;
    }

    public void setCounterpartybic(String counterpartybic) {
        this.counterpartybic = counterpartybic;
    }

    public String getPuracctnum() {
        return puracctnum;
    }

    public void setPuracctnum(String puracctnum) {
        this.puracctnum = puracctnum;
    }

    public String getCxcustid() {
        return cxcustid;
    }

    public void setCxcustid(String cxcustid) {
        this.cxcustid = cxcustid;
    }

    public String getPayeeid() {
        return payeeid;
    }

    public void setPayeeid(String payeeid) {
        this.payeeid = payeeid;
    }

    public String getAcctoccpcode() {
        return acctoccpcode;
    }

    public void setAcctoccpcode(String acctoccpcode) {
        this.acctoccpcode = acctoccpcode;
    }

    public String getAcctstatus() {
        return acctstatus;
    }

    public void setAcctstatus(String acctstatus) {
        this.acctstatus = acctstatus;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getCounterpartybankaddress() {
        return counterpartybankaddress;
    }

    public void setCounterpartybankaddress(String counterpartybankaddress) {
        this.counterpartybankaddress = counterpartybankaddress;
    }

    public String getEntitytype() {
        return entitytype;
    }

    public void setEntitytype(String entitytype) {
        this.entitytype = entitytype;
    }

    public String getZonecode() {
        return zonecode;
    }

    public void setZonecode(String zonecode) {
        this.zonecode = zonecode;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getAcctopendate() {
        return acctopendate;
    }

    public void setAcctopendate(String acctopendate) {
        this.acctopendate = acctopendate;
    }

    public String getMerchantcateg() {
        return merchantcateg;
    }

    public void setMerchantcateg(String merchantcateg) {
        this.merchantcateg = merchantcateg;
    }

    public String getInstalpha() {
        return instalpha;
    }

    public void setInstalpha(String instalpha) {
        this.instalpha = instalpha;
    }

    public String getPayeridtemp() {
        return payeridtemp;
    }

    public void setPayeridtemp(String payeridtemp) {
        this.payeridtemp = payeridtemp;
    }

    public String getCounterpartycountrycode() {
        return counterpartycountrycode;
    }

    public void setCounterpartycountrycode(String counterpartycountrycode) {
        this.counterpartycountrycode = counterpartycountrycode;
    }

    public String getTerminalipaddr() {
        return terminalipaddr;
    }

    public void setTerminalipaddr(String terminalipaddr) {
        this.terminalipaddr = terminalipaddr;
    }


    public String getEntryuser() {
        return entryuser;
    }

    public void setEntryuser(String entryuser) {
        this.entryuser = entryuser;
    }

    public String getAddentityid2() {
        return addentityid2;
    }

    public void setAddentityid2(String addentityid2) {
        this.addentityid2 = addentityid2;
    }

    public String getAddentityid1() {
        return addentityid1;
    }

    public void setAddentityid1(String addentityid1) {
        this.addentityid1 = addentityid1;
    }

    public String getAddentityid4() {
        return addentityid4;
    }

    public void setAddentityid4(String addentityid4) {
        this.addentityid4 = addentityid4;
    }

    public String getTxndate() {
        return txndate;
    }

    public void setTxndate(String txndate) {
        this.txndate = txndate;
    }

    public String getAddentityid3() {
        return addentityid3;
    }

    public void setAddentityid3(String addentityid3) {
        this.addentityid3 = addentityid3;
    }

    public String getAddentityid5() {
        return addentityid5;
    }

    public void setAddentityid5(String addentityid5) {
        this.addentityid5 = addentityid5;
    }

    public String getAccountownership() {
        return accountownership;
    }

    public void setAccountownership(String accountownership) {
        this.accountownership = accountownership;
    }

    public String getSanctionamt() {
        return sanctionamt;
    }

    public void setSanctionamt(String sanctionamt) {
        this.sanctionamt = sanctionamt;
    }

    public String getRefcurncy() {
        return refcurncy;
    }

    public void setRefcurncy(String refcurncy) {
        this.refcurncy = refcurncy;
    }

    public String getKycstatus() {
        return kycstatus;
    }

    public void setKycstatus(String kycstatus) {
        this.kycstatus = kycstatus;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getCounterpartyamountlcy() {
        return counterpartyamountlcy;
    }

    public void setCounterpartyamountlcy(String counterpartyamountlcy) {
        this.counterpartyamountlcy = counterpartyamountlcy;
    }

    public String getTrantype() {
        return trantype;
    }

    public void setTrantype(String trantype) {
        this.trantype = trantype;
    }

    public String getChanneldesc() {
        return channeldesc;
    }

    public void setChanneldesc(String channeldesc) {
        this.channeldesc = channeldesc;
    }

    public String getCounterpartybank() {
        return counterpartybank;
    }

    public void setCounterpartybank(String counterpartybank) {
        this.counterpartybank = counterpartybank;
    }

    public String getBranchiddesc() {
        return branchiddesc;
    }

    public void setBranchiddesc(String branchiddesc) {
        this.branchiddesc = branchiddesc;
    }

    public String getCredrptran() {
        return credrptran;
    }

    public void setCredrptran(String credrptran) {
        this.credrptran = credrptran;
    }

    public String getTxnrefid() {
        return txnrefid;
    }

    public void setTxnrefid(String txnrefid) {
        this.txnrefid = txnrefid;
    }

    public String getSourcebank() {
        return sourcebank;
    }

    public void setSourcebank(String sourcebank) {
        this.sourcebank = sourcebank;
    }

    public String getPayeecity() {
        return payeecity;
    }

    public void setPayeecity(String payeecity) {
        this.payeecity = payeecity;
    }

    public String getDevownerid() {
        return devownerid;
    }

    public void setDevownerid(String devownerid) {
        this.devownerid = devownerid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getTranid() {
        return tranid;
    }

    public void setTranid(String tranid) {
        this.tranid = tranid;
    }

    public String getEntityid() {
        return entityid;
    }

    public void setEntityid(String entityid) {
        this.entityid = entityid;
    }





    public String getRefamt() {
        return refamt;
    }

    public void setRefamt(String refamt) {
        this.refamt = refamt;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getDirectchannelcontrollerid() {
        return directchannelcontrollerid;
    }

    public void setDirectchannelcontrollerid(String directchannelcontrollerid) {
        this.directchannelcontrollerid = directchannelcontrollerid;
    }

    public String getPayeename() {
        return payeename;
    }

    public void setPayeename(String payeename) {
        this.payeename = payeename;
    }

    public String getCounterpartyaccount() {
        return counterpartyaccount;
    }

    public void setCounterpartyaccount(String counterpartyaccount) {
        this.counterpartyaccount = counterpartyaccount;
    }

    public String getChanneltype() {
        return channeltype;
    }

    public void setChanneltype(String channeltype) {
        this.channeltype = channeltype;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getCustreftype() {
        return custreftype;
    }

    public void setCustreftype(String custreftype) {
        this.custreftype = custreftype;
    }

    public String getCustcardid() {
        return custcardid;
    }

    public void setCustcardid(String custcardid) {
        this.custcardid = custcardid;
    }

    public String getTransrlno() {
        return transrlno;
    }

    public void setTransrlno(String transrlno) {
        this.transrlno = transrlno;
    }

    public String getAvlbal() {
        return avlbal;
    }

    public void setAvlbal(String avlbal) {
        this.avlbal = avlbal;
    }

    public String getEvtinducer() {
        return evtinducer;
    }

    public void setEvtinducer(String evtinducer) {
        this.evtinducer = evtinducer;
    }

    public String getTxnvaluedate() {
        return txnvaluedate;
    }

    public void setTxnvaluedate(String txnvaluedate) {
        this.txnvaluedate = txnvaluedate;
    }

    public String getTranposteddt() {
        return tranposteddt;
    }

    public void setTranposteddt(String tranposteddt) {
        this.tranposteddt = tranposteddt;
    }

    public String getSchemetype() {
        return schemetype;
    }

    public void setSchemetype(String schemetype) {
        this.schemetype = schemetype;
    }


    public String getAgenttype() {
        return agenttype;
    }

    public void setAgenttype(String agenttype) {
        this.agenttype = agenttype;
    }

    public String getTranentrydt() {
        return tranentrydt;
    }

    public void setTranentrydt(String tranentrydt) {
        this.tranentrydt = tranentrydt;
    }

    public String getCxcifid() {
        return cxcifid;
    }

    public void setCxcifid(String cxcifid) {
        this.cxcifid = cxcifid;
    }

    public String getActionathost() {
        return actionathost;
    }

    public void setActionathost(String actionathost) {
        this.actionathost = actionathost;
    }

    public String getSchemecode() {
        return schemecode;
    }

    public void setSchemecode(String schemecode) {
        this.schemecode = schemecode;
    }

    public String getCustRefId() {
        return custRefId;
    }

    public void setCustRefId(String custRefId) {
        this.custRefId = custRefId;
    }

    public String getEventtime() {
        return eventtime;
    }

    public void setEventtime(String eventtime) {
        this.eventtime = eventtime;
    }

    public String getChannelid() {
        return channelid;
    }

    public void setChannelid(String channelid) {
        this.channelid = channelid;
    }

    public String getBillid() {
        return billid;
    }

    public void setBillid(String billid) {
        this.billid = billid;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTxnstatus() {
        return txnstatus;
    }

    public void setTxnstatus(String txnstatus) {
        this.txnstatus = txnstatus;
    }

    public String getCounterpartyname() {
        return counterpartyname;
    }

    public void setCounterpartyname(String counterpartyname) {
        this.counterpartyname = counterpartyname;
    }

    public String getTxnamt() {
        return txnamt;
    }

    public void setTxnamt(String txnamt) {
        this.txnamt = txnamt;
    }

    public String getEffavlbal() {
        return effavlbal;
    }

    public void setEffavlbal(String effavlbal) {
        this.effavlbal = effavlbal;
    }

    public String getTrancategory() {
        return trancategory;
    }

    public void setTrancategory(String trancategory) {
        this.trancategory = trancategory;
    }

    public String getTellernumber() {
        return tellernumber;
    }

    public void setTellernumber(String tellernumber) {
        this.tellernumber = tellernumber;
    }



    public String getBalancecur() {
        return balancecur;
    }

    public void setBalancecur(String balancecur) {
        this.balancecur = balancecur;
    }

    public String getCounterpartybankcode() {
        return counterpartybankcode;
    }

    public void setCounterpartybankcode(String counterpartybankcode) {
        this.counterpartybankcode = counterpartybankcode;
    }

    public String getLedgerbal() {
        return ledgerbal;
    }

    public void setLedgerbal(String ledgerbal) {
        this.ledgerbal = ledgerbal;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public String getAcctsolid() {
        return acctsolid;
    }

    public void setAcctsolid(String acctsolid) {
        this.acctsolid = acctsolid;
    }

    public String getTxnamtcur() {
        return txnamtcur;
    }

    public void setTxnamtcur(String txnamtcur) {
        this.txnamtcur = txnamtcur;
    }

    public String getRefcurrency() {
        return refcurrency;
    }

    public void setRefcurrency(String refcurrency) {
        this.refcurrency = refcurrency;
    }

    public String getModeoprncode() {
        return modeoprncode;
    }

    public void setModeoprncode(String modeoprncode) {
        this.modeoprncode = modeoprncode;
    }

    public String getMsgname() {
        return msgname;
    }

    public void setMsgname(String msgname) {
        this.msgname = msgname;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getAccountopendate() {
        return accountopendate;
    }

    public void setAccountopendate(String accountopendate) {
        this.accountopendate = accountopendate;
    }

    public String getTrandate() {
        return trandate;
    }

    public void setTrandate(String trandate) {
        this.trandate = trandate;
    }

    public String getHdrmkrs() {
        return hdrmkrs;
    }

    public void setHdrmkrs(String hdrmkrs) {
        this.hdrmkrs = hdrmkrs;
    }

    public String getTranbrid() {
        return tranbrid;
    }

    public void setTranbrid(String tranbrid) {
        this.tranbrid = tranbrid;
    }

    public String getRemittername() {
        return remittername;
    }

    public void setRemittername(String remittername) {
        this.remittername = remittername;
    }

    public String getInstrumenttype() {
        return instrumenttype;
    }

    public void setInstrumenttype(String instrumenttype) {
        this.instrumenttype = instrumenttype;
    }

    public String getShadowbal() {
        return shadowbal;
    }

    public void setShadowbal(String shadowbal) {
        this.shadowbal = shadowbal;
    }

    public String getReftranamt() {
        return reftranamt;
    }

    public void setReftranamt(String reftranamt) {
        this.reftranamt = reftranamt;
    }

    public String getEod() {
        return eod;
    }

    public void setEod(String eod) {
        this.eod = eod;
    }

    public String getCxacctd() {
        return cxacctd;
    }

    public void setCxacctd(String cxacctd) {
        this.cxacctd = cxacctd;
    }



    public String getCounterpartycurrencylcy() {
        return counterpartycurrencylcy;
    }

    public void setCounterpartycurrencylcy(String counterpartycurrencylcy) {
        this.counterpartycurrencylcy = counterpartycurrencylcy;
    }


    public String getCounterpartycurrency() {
        return counterpartycurrency;
    }

    public void setCounterpartycurrency(String counterpartycurrency) {
        this.counterpartycurrency = counterpartycurrency;
    }

    public String getCounterpartyaddress() {
        return counterpartyaddress;
    }

    public void setCounterpartyaddress(String counterpartyaddress) {
        this.counterpartyaddress = counterpartyaddress;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String eventdate;
   // public Boolean isPostTransaction;
    public String exception;
    public String beneficiaryname;
    public String zonedate;
    public String fcnrflag;
    public String bin;
    public String onlinebatch;
    public String transubtype;
    public String counterpartybusiness;
    public String txnreftype;
    public String systemtime;
    public String txndrcr;
    public String accttype;
    public String hostuserid;
    public String hostid;
    public String counterpartyamount;
    public String txncode;
    public String counterpartycurrencylcy;


    public String directchannelid;
    public String sequencenumber;
    public String trancrncycode;
    public String tranverifieddt;
    public String counterpartybic;
    public String puracctnum;
    public String cxcustid;
 //   public String eventName;
    public String payeeid;
    public String acctoccpcode;
    public String acctstatus;
    public String systemcountry;
    public String counterpartybankaddress;
    public String entitytype;
    public String zonecode;
    public String countrycode;
    public String acctopendate;
    public String merchantcateg;
    public String instalpha;
  //  public String msgsource;
    public String payeridtemp;
    public String counterpartycountrycode;
    public String terminalipaddr;
    public String cxacctd;
    public String counterpartycurrency;

    public String entryuser;
    public String addentityid2;
    public String addentityid1;
    public String addentityid4;
    public String txndate;
    public String addentityid3;
    public String addentityid5;
    //public String eventype;
    public String accountownership;
    public String sanctionamt;
    public String refcurncy;
    public String kycstatus;
    public String agentid;
    public String counterpartyamountlcy;
    public String trantype;
    public String channeldesc;
    public String counterpartybank;
    public String branchiddesc;
    public String credrptran;
    public String txnrefid;
    public String sourcebank;
    public String payeecity;
    public String devownerid;
    public String deviceid;
    public String tranid;
    public String entityid;

    // public String eventsubtype;
    public String counterpartyaddress;
    public String sourceordestacctid;

    public String getSourceordestacctid() {
        return sourceordestacctid;
    }

    public void setSourceordestacctid(String sourceordestacctid) {
        this.sourceordestacctid = sourceordestacctid;
    }

    public String refamt;
    public String placeholder;
    public String directchannelcontrollerid;
    public String payeename;
    public String counterpartyaccount;
    public String channeltype;
    public String acctname;
    public String custreftype;
    public String custcardid;
    public String transrlno;
    public String avlbal;
    public String evtinducer;
    public String txnvaluedate;
    public String tranposteddt;
    public String schemetype;
    public String desc1;

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String agenttype;
    public String tranentrydt;
    public String cxcifid;
    public String actionathost;
    public String schemecode;
    public String custRefId;
    public String eventtime;
    public String channelid;
    public String billid;
    public String rate;
    public String txnstatus;
    public String counterpartyname;
    public String txnamt;
    public String effavlbal;
    public String trancategory;
    public String tellernumber;
    public String instrumentid;

    public String getInstrumentid() {
        return instrumentid;
    }

    public void setInstrumentid(String instrumentid) {
        this.instrumentid = instrumentid;
    }

    public String balancecur;
    public String counterpartybankcode;
    public String ledgerbal;
    public String employeeid;
    public String acctsolid;
 //   public String eventSubtype;
    public String txnamtcur;
    public String refcurrency;
    public String modeoprncode;
    public String msgname;
    public String custid;
    public String accountopendate;
    public String trandate;
    public String hdrmkrs;
    public String tranbrid;
    public String remittername;
    public String instrumenttype;
    public String shadowbal;
    public String reftranamt;
    public String eod;
    public String remarks;
    private String sys_time;

}

