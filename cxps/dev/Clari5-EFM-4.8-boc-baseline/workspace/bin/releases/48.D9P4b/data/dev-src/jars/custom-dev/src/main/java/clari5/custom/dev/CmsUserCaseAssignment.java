package clari5.custom.dev;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.platform.rdbms.RDBMS;

import java.util.*;

/**
 * @author Yashaswi
 */
public class CmsUserCaseAssignment {

    public static Map<String, Map<String, Integer>> channelUserGroupMap;

    public Map<String, Integer> process(String fact_name) throws Exception {
        CxConnection con = getConnection();
        Map<String, String> user_channel = getUserChannel(con);
        //System.out.println("user_channel map data: " + user_channel);
        Map<String, String> fact_channel = getFactChannel(con);
        //System.out.println("fact_channel map data: " + fact_channel);
        Map<String, Integer> userGroupCount = getUserGroupMap(user_channel, fact_channel, fact_name);
        //System.out.println("userGroupCount map data: " + userGroupCount);
        return userGroupCount;
    }

    public Map<String, String> getUserChannel(CxConnection connection) throws Exception {
        HashMap<String, String> chanel_user = new HashMap<>();
        connection.query(this.userChannelSelectQuery(connection.getDbType()), resultSet -> {
            while (resultSet.next()) {
                chanel_user.put(resultSet.getString("user_id"), resultSet.getString("channel_id"));
            }
        });
        return chanel_user;
    }

    public Map<String, String> getFactChannel(CxConnection connection) throws Exception {
        HashMap<String, String> fact_channel = new HashMap<>();
        connection.query(this.factChannelSelectQuery(connection.getDbType()), resultSet -> {
            while (resultSet.next()) {
                fact_channel.put(resultSet.getString("fact_name"), resultSet.getString("channel_mapping"));
            }
        });
        try {
            connection.close();
            System.out.println("data inserted closing connection...");
        } catch (Exception ex) {
        }
        return fact_channel;
    }

    public Map<String, Integer> getUserGroupMap(Map<String, String> user_channel, Map<String, String> fact_channel, String factName) {
        if (channelUserGroupMap == null) {
            channelUserGroupMap = new HashMap<>();
            Map<String, Integer> ccUserCount = new HashMap<>();
            Map<String, Integer> dcUserCount = new HashMap<>();
            Map<String, Integer> ibsUserCount = new HashMap<>();
            Map<String, Integer> mbsUserCount = new HashMap<>();
            Map<String, Integer> cbsUserCount = new HashMap<>();
            for (Map.Entry<String, String> entry : user_channel.entrySet()) {
                String userKey = entry.getKey();
                String userValue = entry.getValue();
                String[] userValueSplit = userValue.split(",");
                if (userValueSplit.length>1) {
                    for (int i = 0; i < userValueSplit.length; i++) {
                        //System.out.println("data inside userValueSplit value: "+userValueSplit[i]);
                        //System.out.println("data inside userValueSplit key: "+userKey);

                        if (userValueSplit[i].equalsIgnoreCase("CC")) {
                            ccUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("DC")) {
                            dcUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("IBS")) {
                            ibsUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("MBS")) {
                            mbsUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("CBS")) {
                            cbsUserCount.put(userKey, 0);
                        }
                    }
                } else {
                    //System.out.println("data inside userValue value: "+userValue);
                    //System.out.println("data inside userValue key: "+userKey);

                    if (userValue.equalsIgnoreCase("CC")) {
                        ccUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("DC")) {
                        dcUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("IBS")) {
                        ibsUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("MBS")) {
                        mbsUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("CBS")) {
                        cbsUserCount.put(userKey, 0);
                    }
                }
            }
            channelUserGroupMap.put("CC", ccUserCount);
            channelUserGroupMap.put("DC", dcUserCount);
            channelUserGroupMap.put("IBS", ibsUserCount);
            channelUserGroupMap.put("MBS", mbsUserCount);
            channelUserGroupMap.put("CBS", cbsUserCount);
        }

        String channel = null;
        if (fact_channel.containsKey(factName)) {
            channel = fact_channel.get(factName);

            //System.out.println("channel from fact_name is: " + channel);
        }
        Map<String, Integer> finalCountMap = new HashMap<>();

        if (channelUserGroupMap.containsKey(channel)) {
            finalCountMap = channelUserGroupMap.get(channel);
        }
        //System.out.println("inside final count map: " + finalCountMap);
        return finalCountMap;
    }

    public static CxConnection getConnection() {
        //Clari5.batchBootstrap("test", "efm-clari5.conf");
        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
        /*RDBMSSession session = null;
        session = Rdbms.getAppSession();
        System.out.println("session is: "+session);
        CxConnection con = session.getCxConnection();*/
        /*if (con == null) throw new RuntimeException("not able acquire connection from RDBMS");
        return con;*/
    }

    public String factChannelTableName() {
        return "fact_channel";
    }

    public String userChannelTableName() {
        return "user_channel";
    }

    public String userChannelSelectQuery(DbTypeEnum dbTypeEnum) {
        switch (dbTypeEnum) {
            case ORACLE:
                return "SELECT user_id, channel_id" +
                        " from " + this.userChannelTableName();
            case SQLSERVER:
                return "SELECT [user_id], [channel_id]" +
                        " from " + this.userChannelTableName();
        }
        return null;
    }

    public String factChannelSelectQuery(DbTypeEnum dbTypeEnum) {
        switch (dbTypeEnum) {
            case ORACLE:
                return "SELECT fact_name, channel_mapping" +
                        " from " + this.factChannelTableName();
            case SQLSERVER:
                return "SELECT [fact_name], [channel_mapping]" +
                        " from " + this.factChannelTableName();
        }
        return null;
    }
}
