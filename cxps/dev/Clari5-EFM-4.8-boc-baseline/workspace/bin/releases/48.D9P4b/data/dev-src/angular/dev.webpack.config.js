var webpack = require("webpack");
var path = require("path");

module.exports = {
    entry: {
        "app": "./app/main"
    },
    output: {
        path: __dirname,
        filename: "./_dist/bundle.js",
        publicPath: 'http://localhost:4242/clari5-cc'
    },
    resolve: {
        extensions: [ '.js', '.html']
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.html$/,
                use: ["html-loader?interpolate=require&-minimize"]
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    },
    watch: false,
    plugins: [
    ],
    devServer: {
        historyApiFallback: true,
        stats: 'minimal',
        inline: true,
        port: 4242
    }
}
