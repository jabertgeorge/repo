package clari5.cipher;

import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import java.util.Base64;

public class Clari5AESImpl {


    public static String encryptDecrypt(String text,String mode){
        Clari5AESImpl impl=new Clari5AESImpl();
        String output="";
        if(mode.equalsIgnoreCase("E")){
            output=impl.encrypt(text);
        }else if(mode.equalsIgnoreCase("D") ){
            output=impl.decrypt(text);
        }else{
            System.out.print("Invalid command");
        }
        return output;
    }

    public String encrypt(String plainText) {
        String cipherText="";
        if (plainText != null && !plainText.equals("")) {
            try {
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, KeyGenerator.getSecretKey());

                cipherText=Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes("UTF-8")));
            } catch (Exception e) {
                System.err.print("Error whil encrypting value [ " + plainText + " ]");
            }

        } else{
            System.out.println("Plain Text value is null or blank");
        }
        return cipherText;

    }


    public  String decrypt(String cipherText) {
        String plainText="";
        if (cipherText != null && !cipherText.equals("")) {
            try {
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, KeyGenerator.getSecretKey());

                plainText=new String(cipher.doFinal(Base64.getDecoder().decode(cipherText)));
            } catch (Exception e) {
                System.err.print("Error whil encrypting value [ " + plainText + " ]");
            }

        } else{
            System.out.println("Cipher Text value is null or blank");
        }
        return plainText;

    }

    public static void main(String[] args)
    {
        final String secretKey = "ssshhhhhhhhhhh!!!!";
        Clari5AESImpl cl=new Clari5AESImpl();
        String originalString = "12349888";
        String encryptedString = cl.encrypt(originalString) ;
        String decryptedString = cl.decrypt(encryptedString) ;

        System.out.println("original String is "+originalString);
        System.out.println("encrypted scting is "+encryptedString);
        System.out.println("Decrypted string is "+decryptedString);
    }
}
