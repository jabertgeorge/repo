package clari5.custom.dev;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxRest;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author manoj kumar shah
 *
 *
 */

public class WatchListFileMatcher {

    private String url="";
    private JSONObject obj;

    private String name;
    private String dob;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    private String nationality;
    private String nic;
    private String passport;

    public WatchListFileMatcher(String url,JSONObject obj){
        this.url = url;
        this.obj = obj;
    }


    public String getMatchScore() throws UnsupportedEncodingException {
	    String temp = "Positive";
        //url=System.getenv("DN")+"/efm/wlsearch?q="+ URLEncoder.encode(obj.toString());
        url = url + "?q="+ URLEncoder.encode(obj.toString(),"UTF-8");
        //getHttpResponse(obj,url);

        String status="";
	    String s = getHttpResponse(obj,url);
        JSONObject jsonObject = null;
        if(s == null || s.trim().equals("")){
            return "{}";
        }
        jsonObject = new JSONObject(s);
        System.out.println(s);

        double score=jsonObject.getJSONObject("matchedResults").getDouble("maxScore");
        double maxscore = score * 100;

        status=jsonObject.getJSONObject("status").get("status").toString();
        if(status.equalsIgnoreCase("success")){
            if(maxscore <= 60) {
                temp = "Positive";
            } else if(maxscore <= 80) {
                temp="Negative";
            } else {
                temp="Critical";
            }
        }
        saveWatchListAudit(score,temp);
        jsonObject.put("prodstatus",jsonObject.get("status"));
        jsonObject.put("status",temp);
        return jsonObject.toString();
    }


    public String getHttpResponse(JSONObject obj,String url) {
        HttpResponse resp=null;
        String response="";
        String instanceId=System.getenv("INSTANCEID");
        String appSecret=System.getenv("APPSECRET");
        try {
            System.out.println("WL_ONBOARDING_CUST : url = "+url);
            //resp= CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").queryString("msg", obj.toString()).basicAuth(instanceId, appSecret).asString();
            resp= CxRest.get(url).header("accept", "application/json")
                    .header("Content-Type", "application/json")
                    .header("mode", "PROG").queryString("msg", obj.toString())
                    .basicAuth(instanceId, appSecret).asString();
            response= (String) resp.getBody();
            System.out.println("WL_ONBOARDING_CUST : Response = "+response+" : "+resp.getStatus());
        } catch (UnirestException e) {
            e.printStackTrace();
            System.out.println("WL_ONBOARDING_CUST : WatchList not reachable"+e);
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("WL_ONBOARDING_CUST : Exception in parsing"+e);
        }
        return response;
    }

    public boolean saveWatchListAudit (double score, String status) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO " + tableName() + " (entity_id,created_date,name,nationality,dob,nic,passport,max_score,status) VALUES (NEXT VALUE FOR WATCHLIST_API_AUDIT_seq,GETDATE(),?, ?, ?, ?, ?,?,?);");
            int i = 1;
            ps.setString(i++, name);
            ps.setString(i++, nationality);
            ps.setString(i++, dob);
            ps.setString(i++, nic);
            ps.setString(i++, passport);
            ps.setString(i++, score+"");
            ps.setString(i++, status);
            ps.executeUpdate();
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
    public static CxConnection getConnection(){
        //Clari5.batchBootstrap("test", "efm-clari5.conf");

        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
    }
    public String tableName(){
        return "WATCHLIST_API_AUDIT";
    }

    public static void main(String [] ar){
    //WatchListFileMatcher watchListFileMatcher=new WatchListFileMatcher("");
    //System.out.print(watchListFileMatcher.getMatchScore());

    }
}
