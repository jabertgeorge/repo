cxps.events.event.nft-accountstat {
   table-name : EVENT_NFT_ACCOUNTSTAT
   event-mnemonic: NFT
   workspaces : {
           ACCOUNT : cust-ref-id,
	   CUSTOMER: cust-id
  	}
   event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    source : {db : true ,raw_name : source ,type : "string:20"} 
    msg-source : {db : true ,raw_name : msgsource ,type : "string:20"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    channel : {db : true ,raw_name : channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}  
    entity-type : {db : true ,raw_name : entitytype ,type : "string:20"}
    host-id : {db : true ,raw_name : host-id ,type : "string:20"}    
    status : {db : true ,raw_name : status ,type : "string:20"}
    event-time : {db : true ,raw_name : sys_time ,type : timestamp}
    cust-id : {db : true ,raw_name : custid ,type : "string:20"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    cust-ref-id : {db : true ,raw_name : acid ,type : "string:20"}
    init-acct-status : {db : true ,raw_name : init-acct-status ,type : "string:20"}
    final-acct-status : {db : true ,raw_name : final-acct-status ,type : "string:20"}
    source-or-dest-acct-id : {db : true ,raw_name : account-id ,type : "string:20"}
    acct-name : {db : true ,raw_name : acct-name ,type : "string:20"}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {raw_name : reservedfield4 ,type : "string:100"}
    add-entity-id5 : {raw_name : reservedfield5 ,type : "string:100"}
    	
   }
}

