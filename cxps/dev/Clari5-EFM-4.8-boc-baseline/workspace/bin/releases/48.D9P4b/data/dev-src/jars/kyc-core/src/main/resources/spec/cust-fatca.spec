clari5.kyc.entity.cust-fatca{
    attributes = [
		{ name : cust-id, type ="string:50" , key=true}

		{ name : classification, type = "string:50" }
		{ name : us-person, type = "string:50" }
		{ name : npfi, type = "string:10" }

		{ name : acct-holder-address,  type = "string:100" }
		{ name : acct-holder-city,  type = "string:50"}
		{ name : acct-holder-state,  type = "string:50" }
		{ name : acct-holder-country,  type = "string:50" }
		{ name : acct-holder-pcode,  type = "string:20" }
		{ name : acct-holder-tin,  type = "string:50" }
        { name : ahfd,  type = "string:10" }

		{ name : owner-name,  type = "string:100" }
		{ name : owner-address,  type = "string:100" }
		{ name : owner-city,  type = "string:50" }
		{ name : owner-state,  type = "string:50" }
		{ name : owner-country,  type = "string:50" }
		{ name : owner-pcode,  type = "string:20" }
		{ name : owner-tin,  type = "string:50" }
		{ name : ofd,  type = "string:10" }

		{ name : w8-available,  type = "string:10" }
		{ name : expiry-date, type : "string:20"}
	]
}
