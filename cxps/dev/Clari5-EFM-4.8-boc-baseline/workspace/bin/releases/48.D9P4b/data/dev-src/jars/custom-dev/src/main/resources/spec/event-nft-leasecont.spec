cxps.events.event.nft-leasecont {
  table-name : EVENT_NFT_LEASECONT
  event-mnemonic = NL
  workspaces : {
    ACCOUNT : cust-ref-id,
    ACCOUNT : stakeholder-id
        }

  event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    channel-id : {db : true ,raw_name : channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}
    msg-source : {db : true ,raw_name : source ,type : "string:20"}
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {raw_name : reservedfield4 ,type : "string:100"}
    add-entity-id5 : {raw_name : reservedfield5 ,type : "string:100"}
    cust-id : {db : true ,raw_name : cust_id, type : "string:20"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    cust-ref-id : {db : true ,raw_name : acid ,type : "string:20"}
    txn-date : {db : true ,raw_name : sys_time ,type : timestamp}
    stakeholder : {db : true ,raw_name : StakeHolder ,type : "string:20"}
    stakeholder-id : {db : true ,raw_name : stakeHolder_id ,type : "string:20"}
    lease-facility-amount:{type:"number:11,2",db:true,raw_name:Lease_Facility_Amount}
    cust-contribution:{type:"number:11,2",db:true,raw_name:Customer_Contribution}
    lease-creat-dt : {db : true ,raw_name : Lease_Creation_Date ,type : timestamp}
    nic:{type:"string:100",db:true,raw_name:NIC}
    br:{type:"string:100",db:true,raw_name:BR}
 }
}
