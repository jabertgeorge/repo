clari5.kyc.entity{
	cust-maker-tbl {
		attributes = [
			{ name : custId, type ="string:50" , key=true}
			{ name : update-type, type = "string:20" , key=true}
			{ name : updates, type = "string:400" }
			{ name : old-json, type = "string:400" }
			{ name : timestamp, type : timestamp }
			{ name : userId, type = "string:50" }
		]
    }
}