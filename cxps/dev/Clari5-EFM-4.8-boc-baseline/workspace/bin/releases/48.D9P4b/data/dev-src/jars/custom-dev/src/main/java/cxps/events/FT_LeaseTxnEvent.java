// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_LEASE", Schema="rice")
public class FT_LeaseTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String txnCurrency;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String keys;
       @Field public java.sql.Timestamp leaseSettlementDate;
       @Field(size=20) public String channel;
       @Field(size=20) public String sysCountry;
       @Field(size=20) public String eventTime;
       @Field(size=11) public Double lcyAmt;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String cxAcctId;
       @Field(size=11) public Double txnAmt;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=11) public Double lcyCurncy;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String txnCode;
       @Field(size=20) public String txnId;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String eventSubtype;
       @Field(size=20) public String txnType;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=50) public String stakeHolder;
       @Field(size=50) public String leasFacilityNo;
       @Field(size=20) public String txnSrlNo;
       @Field(size=20) public String custCardId;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String stakeholderId;
       @Field(size=20) public String txnSubType;


    @JsonIgnore
    public ITable<FT_LeaseTxnEvent> t = AEF.getITable(this);

	public FT_LeaseTxnEvent(){}

    public FT_LeaseTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("LeaseTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTxnCurrency(json.getString("TRAN_CURR"));
            setMsgSource(json.getString("source"));
            setKeys(json.getString("keys"));
            setLeaseSettlementDate(EventHelper.toTimestamp(json.getString("lease_settlement_Date")));
            setChannel(json.getString("channel"));
            setSysCountry(json.getString("SYSTEMCOUNTRY"));
            setEventTime(json.getString("eventts"));
            setLcyAmt(EventHelper.toDouble(json.getString("LCY_AMT")));
            setSystemTime(EventHelper.toTimestamp(json.getString("Systime")));
            setTxnDrCr(json.getString("part-tran-type"));
            setCxAcctId(json.getString("cx-acct-id"));
            setTxnAmt(EventHelper.toDouble(json.getString("TRAN_AMT")));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setLcyCurncy(EventHelper.toDouble(json.getString("LCY_CURR")));
            setAddEntityId2(json.getString("reservedfield2"));
            setTxnCode(json.getString("Tran_COde"));
            setTxnId(json.getString("TxnID"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("Tran_Date")));
            setAddEntityId3(json.getString("reservedfield3"));
            setEventSubtype(json.getString("eventsubtype"));
            setTxnType(json.getString("tran-type"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setStakeHolder(json.getString("StakeHolder"));
            setLeasFacilityNo(json.getString("Lease_Facility_Number"));
            setTxnSrlNo(json.getString("part_tran_srl_num"));
            setCustCardId(json.getString("cust-card-id"));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setStakeholderId(json.getString("stakeHolder_id"));
            setTxnSubType(json.getString("tran-sub-type"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FL"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getTxnCurrency(){ return txnCurrency; }

    public String getMsgSource(){ return msgSource; }

    public String getKeys(){ return keys; }

    public java.sql.Timestamp getLeaseSettlementDate(){ return leaseSettlementDate; }

    public String getChannel(){ return channel; }

    public String getSysCountry(){ return sysCountry; }

    public String getEventTime(){ return eventTime; }

    public Double getLcyAmt(){ return lcyAmt; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getCxAcctId(){ return cxAcctId; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public Double getLcyCurncy(){ return lcyCurncy; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getTxnCode(){ return txnCode; }

    public String getTxnId(){ return txnId; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getTxnType(){ return txnType; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getStakeHolder(){ return stakeHolder; }

    public String getLeasFacilityNo(){ return leasFacilityNo; }

    public String getTxnSrlNo(){ return txnSrlNo; }

    public String getCustCardId(){ return custCardId; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getStakeholderId(){ return stakeholderId; }

    public String getTxnSubType(){ return txnSubType; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTxnCurrency(String val){ this.txnCurrency = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setLeaseSettlementDate(java.sql.Timestamp val){ this.leaseSettlementDate = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSysCountry(String val){ this.sysCountry = val; }
    public void setEventTime(String val){ this.eventTime = val; }
    public void setLcyAmt(Double val){ this.lcyAmt = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setLcyCurncy(Double val){ this.lcyCurncy = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setTxnId(String val){ this.txnId = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setTxnType(String val){ this.txnType = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setStakeHolder(String val){ this.stakeHolder = val; }
    public void setLeasFacilityNo(String val){ this.leasFacilityNo = val; }
    public void setTxnSrlNo(String val){ this.txnSrlNo = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setStakeholderId(String val){ this.stakeholderId = val; }
    public void setTxnSubType(String val){ this.txnSubType = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_LeaseTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.stakeholderId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_LeaseTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "LeaseTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}