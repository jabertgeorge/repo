// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_LEASECONT", Schema="rice")
public class NFT_LeasecontEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String addEntityId3;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=100) public String eventSubtype;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=20) public String keys;
       @Field(size=20) public String custRefId;
       @Field(size=100) public String nic;
       @Field(size=20) public String channelId;
       @Field(size=20) public String custId;
       @Field(size=20) public String stakeholder;
       @Field(size=100) public String br;
       @Field(size=11) public Double leaseFacilityAmount;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field public java.sql.Timestamp leaseCreatDt;
       @Field(size=20) public String cxAcctId;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String stakeholderId;
       @Field(size=11) public Double custContribution;


    @JsonIgnore
    public ITable<NFT_LeasecontEvent> t = AEF.getITable(this);

	public NFT_LeasecontEvent(){}

    public NFT_LeasecontEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Leasecont");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAddEntityId2(json.getString("reservedfield2"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setMsgSource(json.getString("source"));
            setAddEntityId3(json.getString("reservedfield3"));
            setTxnDate(EventHelper.toTimestamp(json.getString("sys_time")));
            setEventSubtype(json.getString("eventsubtype"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setKeys(json.getString("keys"));
            setCustRefId(json.getString("acid"));
            setNic(json.getString("NIC"));
            setChannelId(json.getString("channel"));
            setCustId(json.getString("cust_id"));
            setStakeholder(json.getString("StakeHolder"));
            setBr(json.getString("BR"));
            setLeaseFacilityAmount(EventHelper.toDouble(json.getString("Lease_Facility_Amount")));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setLeaseCreatDt(EventHelper.toTimestamp(json.getString("Lease_Creation_Date")));
            setCxAcctId(json.getString("cx-acct-id"));
            setHostUserId(json.getString("host_user_id"));
            setStakeholderId(json.getString("stakeHolder_id"));
            setCustContribution(EventHelper.toDouble(json.getString("Customer_Contribution")));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NL"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAddEntityId2(){ return addEntityId2; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public String getMsgSource(){ return msgSource; }

    public String getAddEntityId3(){ return addEntityId3; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getKeys(){ return keys; }

    public String getCustRefId(){ return custRefId; }

    public String getNic(){ return nic; }

    public String getChannelId(){ return channelId; }

    public String getCustId(){ return custId; }

    public String getStakeholder(){ return stakeholder; }

    public String getBr(){ return br; }

    public Double getLeaseFacilityAmount(){ return leaseFacilityAmount; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public java.sql.Timestamp getLeaseCreatDt(){ return leaseCreatDt; }

    public String getCxAcctId(){ return cxAcctId; }

    public String getHostUserId(){ return hostUserId; }

    public String getStakeholderId(){ return stakeholderId; }

    public Double getCustContribution(){ return custContribution; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setCustRefId(String val){ this.custRefId = val; }
    public void setNic(String val){ this.nic = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setStakeholder(String val){ this.stakeholder = val; }
    public void setBr(String val){ this.br = val; }
    public void setLeaseFacilityAmount(Double val){ this.leaseFacilityAmount = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setLeaseCreatDt(java.sql.Timestamp val){ this.leaseCreatDt = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setStakeholderId(String val){ this.stakeholderId = val; }
    public void setCustContribution(Double val){ this.custContribution = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_LeasecontEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.stakeholderId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Leasecont");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Leasecont");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}