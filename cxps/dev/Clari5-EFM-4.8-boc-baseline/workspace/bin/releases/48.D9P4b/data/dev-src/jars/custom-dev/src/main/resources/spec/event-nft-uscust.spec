cxps.events.event.nft-uscust {
   table-name : NFT_USCUST
   event-mnemonic: NU
   workspaces : {
	   CUSTOMER: cust-id
  	}
   event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    host-id : {db : true ,raw_name : host-id ,type : "string:20"}
    channel-id : {db : true ,raw_name : channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}  
    msg-source : {db : true ,raw_name : source ,type : "string:20"} 
    txn-date : {db : true ,raw_name : sys_time ,type : timestamp}
    cust-id : {db : true ,raw_name : cust-id ,type : "string:20"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}	
    cust-creation-date : {db : true ,raw_name : CUSTCREATIONDATE ,type : "string:20"}
    permanent-addr : {db : true ,raw_name : Permanent_Addr ,type : "string:20"}
    mailing-addr : {db : true ,raw_name : Mailing_Addr ,type : "string:20"}
    tele-phn : {db : true ,raw_name : Tel_Phn ,type : "string:20"}
    citenzenship: {db : true ,raw_name : Citenzenship ,type : "string:20"}
    usnational_fatca : {db : true ,raw_name : USNATIONAL_FATCA ,type : "string:20"}
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {type : "string:100",raw_name : reservedfield4}
    add-entity-id5 : {type : "string:100",raw_name : reservedfield5}
    
   }
}


