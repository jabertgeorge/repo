// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_AccountTxnEventMapper extends EventMapper<FT_AccountTxnEvent> {

public FT_AccountTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_AccountTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_AccountTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_AccountTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_AccountTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_AccountTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_AccountTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getException());
            preparedStatement.setString(i++, obj.getBeneficiaryName());
            preparedStatement.setTimestamp(i++, obj.getZoneDate());
            preparedStatement.setString(i++, obj.getFcnrFlag());
            preparedStatement.setString(i++, obj.getBin());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setString(i++, obj.getAccountid());
            preparedStatement.setString(i++, obj.getCounterPartyBusiness());
            preparedStatement.setString(i++, obj.getTxnRefType());
            preparedStatement.setTimestamp(i++, obj.getSystemTime());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setString(i++, obj.getAcctType());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getCounterPartyAmount());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getCounterPartyCurrencyLcy());
            preparedStatement.setString(i++, obj.getDirectChannelId());
            preparedStatement.setString(i++, obj.getSequenceNumber());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setTimestamp(i++, obj.getTranVerifiedDt());
            preparedStatement.setString(i++, obj.getCounterPartyBic());
            preparedStatement.setString(i++, obj.getPurAcctNum());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getAcctOccpCode());
            preparedStatement.setString(i++, obj.getAcctStatus());
            preparedStatement.setString(i++, obj.getSystemCountry());
            preparedStatement.setString(i++, obj.getCounterPartyBankAddress());
            preparedStatement.setString(i++, obj.getStatus());
            preparedStatement.setString(i++, obj.getEntityType());
            preparedStatement.setString(i++, obj.getZoneCode());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getMerchantCateg());
            preparedStatement.setString(i++, obj.getInstAlpha());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getPayerIdTemp());
            preparedStatement.setString(i++, obj.getCounterPartyCountryCode());
            preparedStatement.setString(i++, obj.getTerminalIpAddr());
            preparedStatement.setDouble(i++, obj.getFcyTranAmt());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setString(i++, obj.getCounterPartyCurrency());
            preparedStatement.setString(i++, obj.getEntryUser());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getPartTranSrlNum());
            preparedStatement.setString(i++, obj.getAccountOwnership());
            preparedStatement.setDouble(i++, obj.getSanctionAmt());
            preparedStatement.setString(i++, obj.getEightZeros());
            preparedStatement.setDouble(i++, obj.getRefCurncy());
            preparedStatement.setString(i++, obj.getKycStatus());
            preparedStatement.setString(i++, obj.getAgentId());
            preparedStatement.setString(i++, obj.getCounterPartyAmountLcy());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getChannelDesc());
            preparedStatement.setString(i++, obj.getCounterPartyBank());
            preparedStatement.setString(i++, obj.getBranchIdDesc());
            preparedStatement.setString(i++, obj.getCreDrPtran());
            preparedStatement.setString(i++, obj.getTxnRefId());
            preparedStatement.setString(i++, obj.getSourceBank());
            preparedStatement.setString(i++, obj.getPayeeCity());
            preparedStatement.setString(i++, obj.getDevOwnerId());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getEntityId());
            preparedStatement.setString(i++, obj.getCounterPartyAddress());
            preparedStatement.setString(i++, obj.getSourceOrDestAcctId());
            preparedStatement.setDouble(i++, obj.getRefAmt());
            preparedStatement.setString(i++, obj.getPlaceHolder());
            preparedStatement.setString(i++, obj.getDirectChannelControllerId());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setString(i++, obj.getCounterPartyAccount());
            preparedStatement.setString(i++, obj.getChannelType());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getCustRefType());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setString(i++, obj.getTranSrlNo());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getEvtInducer());
            preparedStatement.setTimestamp(i++, obj.getTxnValueDate());
            preparedStatement.setTimestamp(i++, obj.getTranPostedDt());
            preparedStatement.setString(i++, obj.getSchemeType());
            preparedStatement.setString(i++, obj.getDesc());
            preparedStatement.setString(i++, obj.getAgentType());
            preparedStatement.setTimestamp(i++, obj.getTranEntryDt());
            preparedStatement.setString(i++, obj.getCxCifId());
            preparedStatement.setString(i++, obj.getActionAtHost());
            preparedStatement.setString(i++, obj.getSchemeCode());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getCustRefId());
            preparedStatement.setTimestamp(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getChannelId());
            preparedStatement.setString(i++, obj.getBillId());
            preparedStatement.setDouble(i++, obj.getRate());
            preparedStatement.setString(i++, obj.getTxnStatus());
            preparedStatement.setString(i++, obj.getCounterPartyName());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setDouble(i++, obj.getEffAvlBal());
            preparedStatement.setString(i++, obj.getTranCategory());
            preparedStatement.setString(i++, obj.getTellerNumber());
            preparedStatement.setLong(i++, obj.getInstrumentId());
            preparedStatement.setString(i++, obj.getBalanceCur());
            preparedStatement.setString(i++, obj.getCounterPartyBankCode());
            preparedStatement.setDouble(i++, obj.getLedgerBal());
            preparedStatement.setString(i++, obj.getEmployeeId());
            preparedStatement.setString(i++, obj.getAcctSolId());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getTxnAmtCur());
            preparedStatement.setString(i++, obj.getRefCurrency());
            preparedStatement.setString(i++, obj.getModeOprnCode());
            preparedStatement.setString(i++, obj.getMsgName());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setTimestamp(i++, obj.getAccountOpendate());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getHdrmkrs());
            preparedStatement.setString(i++, obj.getTranBrId());
            preparedStatement.setString(i++, obj.getRemitterName());
            preparedStatement.setString(i++, obj.getInstrumentType());
            preparedStatement.setDouble(i++, obj.getShadowBal());
            preparedStatement.setDouble(i++, obj.getRefTranAmt());
            preparedStatement.setLong(i++, obj.getEod());
            preparedStatement.setString(i++, obj.getRemarks());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_AccountTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_AccountTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_ACCOUNT_TXN"));
        putList = new ArrayList<>();

        for (FT_AccountTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "EXCEPTION",  obj.getException());
            p = this.insert(p, "BENEFICIARY_NAME",  obj.getBeneficiaryName());
            p = this.insert(p, "ZONE_DATE", String.valueOf(obj.getZoneDate()));
            p = this.insert(p, "FCNR_FLAG",  obj.getFcnrFlag());
            p = this.insert(p, "BIN",  obj.getBin());
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "ACCOUNTID",  obj.getAccountid());
            p = this.insert(p, "COUNTER_PARTY_BUSINESS",  obj.getCounterPartyBusiness());
            p = this.insert(p, "TXN_REF_TYPE",  obj.getTxnRefType());
            p = this.insert(p, "SYSTEM_TIME", String.valueOf(obj.getSystemTime()));
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "ACCT_TYPE",  obj.getAcctType());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "COUNTER_PARTY_AMOUNT", String.valueOf(obj.getCounterPartyAmount()));
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "COUNTER_PARTY_CURRENCY_LCY",  obj.getCounterPartyCurrencyLcy());
            p = this.insert(p, "DIRECT_CHANNEL_ID",  obj.getDirectChannelId());
            p = this.insert(p, "SEQUENCE_NUMBER",  obj.getSequenceNumber());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_VERIFIED_DT", String.valueOf(obj.getTranVerifiedDt()));
            p = this.insert(p, "COUNTER_PARTY_BIC",  obj.getCounterPartyBic());
            p = this.insert(p, "PUR_ACCT_NUM",  obj.getPurAcctNum());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "ACCT_OCCP_CODE",  obj.getAcctOccpCode());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            p = this.insert(p, "COUNTER_PARTY_BANK_ADDRESS",  obj.getCounterPartyBankAddress());
            p = this.insert(p, "STATUS",  obj.getStatus());
            p = this.insert(p, "ENTITY_TYPE",  obj.getEntityType());
            p = this.insert(p, "ZONE_CODE",  obj.getZoneCode());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "MERCHANT_CATEG",  obj.getMerchantCateg());
            p = this.insert(p, "INST_ALPHA",  obj.getInstAlpha());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "PAYER_ID_TEMP",  obj.getPayerIdTemp());
            p = this.insert(p, "COUNTER_PARTY_COUNTRY_CODE",  obj.getCounterPartyCountryCode());
            p = this.insert(p, "TERMINAL_IP_ADDR",  obj.getTerminalIpAddr());
            p = this.insert(p, "FCY_TRAN_AMT", String.valueOf(obj.getFcyTranAmt()));
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "COUNTER_PARTY_CURRENCY",  obj.getCounterPartyCurrency());
            p = this.insert(p, "ENTRY_USER",  obj.getEntryUser());
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "PART_TRAN_SRL_NUM",  obj.getPartTranSrlNum());
            p = this.insert(p, "ACCOUNT_OWNERSHIP",  obj.getAccountOwnership());
            p = this.insert(p, "SANCTION_AMT", String.valueOf(obj.getSanctionAmt()));
            p = this.insert(p, "EIGHT_ZEROS",  obj.getEightZeros());
            p = this.insert(p, "REF_CURNCY", String.valueOf(obj.getRefCurncy()));
            p = this.insert(p, "KYC_STATUS",  obj.getKycStatus());
            p = this.insert(p, "AGENT_ID",  obj.getAgentId());
            p = this.insert(p, "COUNTER_PARTY_AMOUNT_LCY",  obj.getCounterPartyAmountLcy());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "CHANNEL_DESC",  obj.getChannelDesc());
            p = this.insert(p, "COUNTER_PARTY_BANK",  obj.getCounterPartyBank());
            p = this.insert(p, "BRANCH_ID_DESC",  obj.getBranchIdDesc());
            p = this.insert(p, "CRE_DR_PTRAN",  obj.getCreDrPtran());
            p = this.insert(p, "TXN_REF_ID",  obj.getTxnRefId());
            p = this.insert(p, "SOURCE_BANK",  obj.getSourceBank());
            p = this.insert(p, "PAYEE_CITY",  obj.getPayeeCity());
            p = this.insert(p, "DEV_OWNER_ID",  obj.getDevOwnerId());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "ENTITY_ID",  obj.getEntityId());
            p = this.insert(p, "COUNTER_PARTY_ADDRESS",  obj.getCounterPartyAddress());
            p = this.insert(p, "SOURCE_OR_DEST_ACCT_ID",  obj.getSourceOrDestAcctId());
            p = this.insert(p, "REF_AMT", String.valueOf(obj.getRefAmt()));
            p = this.insert(p, "PLACE_HOLDER",  obj.getPlaceHolder());
            p = this.insert(p, "DIRECT_CHANNEL_CONTROLLER_ID",  obj.getDirectChannelControllerId());
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "COUNTER_PARTY_ACCOUNT",  obj.getCounterPartyAccount());
            p = this.insert(p, "CHANNEL_TYPE",  obj.getChannelType());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "CUST_REF_TYPE",  obj.getCustRefType());
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "TRAN_SRL_NO",  obj.getTranSrlNo());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "EVT_INDUCER",  obj.getEvtInducer());
            p = this.insert(p, "TXN_VALUE_DATE", String.valueOf(obj.getTxnValueDate()));
            p = this.insert(p, "TRAN_POSTED_DT", String.valueOf(obj.getTranPostedDt()));
            p = this.insert(p, "SCHEME_TYPE",  obj.getSchemeType());
            p = this.insert(p, "DESC",  obj.getDesc());
            p = this.insert(p, "AGENT_TYPE",  obj.getAgentType());
            p = this.insert(p, "TRAN_ENTRY_DT", String.valueOf(obj.getTranEntryDt()));
            p = this.insert(p, "CX_CIF_ID",  obj.getCxCifId());
            p = this.insert(p, "ACTION_AT_HOST",  obj.getActionAtHost());
            p = this.insert(p, "SCHEME_CODE",  obj.getSchemeCode());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "CUST_REF_ID",  obj.getCustRefId());
            p = this.insert(p, "EVENT_TIME", String.valueOf(obj.getEventTime()));
            p = this.insert(p, "CHANNEL_ID",  obj.getChannelId());
            p = this.insert(p, "BILL_ID",  obj.getBillId());
            p = this.insert(p, "RATE", String.valueOf(obj.getRate()));
            p = this.insert(p, "TXN_STATUS",  obj.getTxnStatus());
            p = this.insert(p, "COUNTER_PARTY_NAME",  obj.getCounterPartyName());
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "EFF_AVL_BAL", String.valueOf(obj.getEffAvlBal()));
            p = this.insert(p, "TRAN_CATEGORY",  obj.getTranCategory());
            p = this.insert(p, "TELLER_NUMBER",  obj.getTellerNumber());
            p = this.insert(p, "INSTRUMENT_ID", String.valueOf(obj.getInstrumentId()));
            p = this.insert(p, "BALANCE_CUR",  obj.getBalanceCur());
            p = this.insert(p, "COUNTER_PARTY_BANK_CODE",  obj.getCounterPartyBankCode());
            p = this.insert(p, "LEDGER_BAL", String.valueOf(obj.getLedgerBal()));
            p = this.insert(p, "EMPLOYEE_ID",  obj.getEmployeeId());
            p = this.insert(p, "ACCT_SOL_ID",  obj.getAcctSolId());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "TXN_AMT_CUR",  obj.getTxnAmtCur());
            p = this.insert(p, "REF_CURRENCY",  obj.getRefCurrency());
            p = this.insert(p, "MODE_OPRN_CODE",  obj.getModeOprnCode());
            p = this.insert(p, "MSG_NAME",  obj.getMsgName());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ACCOUNT_OPENDATE", String.valueOf(obj.getAccountOpendate()));
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "HDRMKRS",  obj.getHdrmkrs());
            p = this.insert(p, "TRAN_BR_ID",  obj.getTranBrId());
            p = this.insert(p, "REMITTER_NAME",  obj.getRemitterName());
            p = this.insert(p, "INSTRUMENT_TYPE",  obj.getInstrumentType());
            p = this.insert(p, "SHADOW_BAL", String.valueOf(obj.getShadowBal()));
            p = this.insert(p, "REF_TRAN_AMT", String.valueOf(obj.getRefTranAmt()));
            p = this.insert(p, "EOD", String.valueOf(obj.getEod()));
            p = this.insert(p, "REMARKS",  obj.getRemarks());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_ACCOUNT_TXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_AccountTxnEvent obj = new FT_AccountTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setException(rs.getString("EXCEPTION"));
    obj.setBeneficiaryName(rs.getString("BENEFICIARY_NAME"));
    obj.setZoneDate(rs.getTimestamp("ZONE_DATE"));
    obj.setFcnrFlag(rs.getString("FCNR_FLAG"));
    obj.setBin(rs.getString("BIN"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setAccountid(rs.getString("ACCOUNTID"));
    obj.setCounterPartyBusiness(rs.getString("COUNTER_PARTY_BUSINESS"));
    obj.setTxnRefType(rs.getString("TXN_REF_TYPE"));
    obj.setSystemTime(rs.getTimestamp("SYSTEM_TIME"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setAcctType(rs.getString("ACCT_TYPE"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCounterPartyAmount(rs.getDouble("COUNTER_PARTY_AMOUNT"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setCounterPartyCurrencyLcy(rs.getString("COUNTER_PARTY_CURRENCY_LCY"));
    obj.setDirectChannelId(rs.getString("DIRECT_CHANNEL_ID"));
    obj.setSequenceNumber(rs.getString("SEQUENCE_NUMBER"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranVerifiedDt(rs.getTimestamp("TRAN_VERIFIED_DT"));
    obj.setCounterPartyBic(rs.getString("COUNTER_PARTY_BIC"));
    obj.setPurAcctNum(rs.getString("PUR_ACCT_NUM"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setAcctOccpCode(rs.getString("ACCT_OCCP_CODE"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));
    obj.setCounterPartyBankAddress(rs.getString("COUNTER_PARTY_BANK_ADDRESS"));
    obj.setStatus(rs.getString("STATUS"));
    obj.setEntityType(rs.getString("ENTITY_TYPE"));
    obj.setZoneCode(rs.getString("ZONE_CODE"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setMerchantCateg(rs.getString("MERCHANT_CATEG"));
    obj.setInstAlpha(rs.getString("INST_ALPHA"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setPayerIdTemp(rs.getString("PAYER_ID_TEMP"));
    obj.setCounterPartyCountryCode(rs.getString("COUNTER_PARTY_COUNTRY_CODE"));
    obj.setTerminalIpAddr(rs.getString("TERMINAL_IP_ADDR"));
    obj.setFcyTranAmt(rs.getDouble("FCY_TRAN_AMT"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setCounterPartyCurrency(rs.getString("COUNTER_PARTY_CURRENCY"));
    obj.setEntryUser(rs.getString("ENTRY_USER"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setPartTranSrlNum(rs.getString("PART_TRAN_SRL_NUM"));
    obj.setAccountOwnership(rs.getString("ACCOUNT_OWNERSHIP"));
    obj.setSanctionAmt(rs.getDouble("SANCTION_AMT"));
    obj.setEightZeros(rs.getString("EIGHT_ZEROS"));
    obj.setRefCurncy(rs.getDouble("REF_CURNCY"));
    obj.setKycStatus(rs.getString("KYC_STATUS"));
    obj.setAgentId(rs.getString("AGENT_ID"));
    obj.setCounterPartyAmountLcy(rs.getString("COUNTER_PARTY_AMOUNT_LCY"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setChannelDesc(rs.getString("CHANNEL_DESC"));
    obj.setCounterPartyBank(rs.getString("COUNTER_PARTY_BANK"));
    obj.setBranchIdDesc(rs.getString("BRANCH_ID_DESC"));
    obj.setCreDrPtran(rs.getString("CRE_DR_PTRAN"));
    obj.setTxnRefId(rs.getString("TXN_REF_ID"));
    obj.setSourceBank(rs.getString("SOURCE_BANK"));
    obj.setPayeeCity(rs.getString("PAYEE_CITY"));
    obj.setDevOwnerId(rs.getString("DEV_OWNER_ID"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setEntityId(rs.getString("ENTITY_ID"));
    obj.setCounterPartyAddress(rs.getString("COUNTER_PARTY_ADDRESS"));
    obj.setSourceOrDestAcctId(rs.getString("SOURCE_OR_DEST_ACCT_ID"));
    obj.setRefAmt(rs.getDouble("REF_AMT"));
    obj.setPlaceHolder(rs.getString("PLACE_HOLDER"));
    obj.setDirectChannelControllerId(rs.getString("DIRECT_CHANNEL_CONTROLLER_ID"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setCounterPartyAccount(rs.getString("COUNTER_PARTY_ACCOUNT"));
    obj.setChannelType(rs.getString("CHANNEL_TYPE"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setCustRefType(rs.getString("CUST_REF_TYPE"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setTranSrlNo(rs.getString("TRAN_SRL_NO"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setEvtInducer(rs.getString("EVT_INDUCER"));
    obj.setTxnValueDate(rs.getTimestamp("TXN_VALUE_DATE"));
    obj.setTranPostedDt(rs.getTimestamp("TRAN_POSTED_DT"));
    obj.setSchemeType(rs.getString("SCHEME_TYPE"));
    obj.setDesc(rs.getString("DESC"));
    obj.setAgentType(rs.getString("AGENT_TYPE"));
    obj.setTranEntryDt(rs.getTimestamp("TRAN_ENTRY_DT"));
    obj.setCxCifId(rs.getString("CX_CIF_ID"));
    obj.setActionAtHost(rs.getString("ACTION_AT_HOST"));
    obj.setSchemeCode(rs.getString("SCHEME_CODE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setCustRefId(rs.getString("CUST_REF_ID"));
    obj.setEventTime(rs.getTimestamp("EVENT_TIME"));
    obj.setChannelId(rs.getString("CHANNEL_ID"));
    obj.setBillId(rs.getString("BILL_ID"));
    obj.setRate(rs.getDouble("RATE"));
    obj.setTxnStatus(rs.getString("TXN_STATUS"));
    obj.setCounterPartyName(rs.getString("COUNTER_PARTY_NAME"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setEffAvlBal(rs.getDouble("EFF_AVL_BAL"));
    obj.setTranCategory(rs.getString("TRAN_CATEGORY"));
    obj.setTellerNumber(rs.getString("TELLER_NUMBER"));
    obj.setInstrumentId(rs.getLong("INSTRUMENT_ID"));
    obj.setBalanceCur(rs.getString("BALANCE_CUR"));
    obj.setCounterPartyBankCode(rs.getString("COUNTER_PARTY_BANK_CODE"));
    obj.setLedgerBal(rs.getDouble("LEDGER_BAL"));
    obj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
    obj.setAcctSolId(rs.getString("ACCT_SOL_ID"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setTxnAmtCur(rs.getString("TXN_AMT_CUR"));
    obj.setRefCurrency(rs.getString("REF_CURRENCY"));
    obj.setModeOprnCode(rs.getString("MODE_OPRN_CODE"));
    obj.setMsgName(rs.getString("MSG_NAME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setAccountOpendate(rs.getTimestamp("ACCOUNT_OPENDATE"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setHdrmkrs(rs.getString("HDRMKRS"));
    obj.setTranBrId(rs.getString("TRAN_BR_ID"));
    obj.setRemitterName(rs.getString("REMITTER_NAME"));
    obj.setInstrumentType(rs.getString("INSTRUMENT_TYPE"));
    obj.setShadowBal(rs.getDouble("SHADOW_BAL"));
    obj.setRefTranAmt(rs.getDouble("REF_TRAN_AMT"));
    obj.setEod(rs.getLong("EOD"));
    obj.setRemarks(rs.getString("REMARKS"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_AccountTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_AccountTxnEvent> events;
 FT_AccountTxnEvent obj = new FT_AccountTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_ACCOUNT_TXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_AccountTxnEvent obj = new FT_AccountTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setException(getColumnValue(rs, "EXCEPTION"));
            obj.setBeneficiaryName(getColumnValue(rs, "BENEFICIARY_NAME"));
            obj.setZoneDate(EventHelper.toTimestamp(getColumnValue(rs, "ZONE_DATE")));
            obj.setFcnrFlag(getColumnValue(rs, "FCNR_FLAG"));
            obj.setBin(getColumnValue(rs, "BIN"));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setAccountid(getColumnValue(rs, "ACCOUNTID"));
            obj.setCounterPartyBusiness(getColumnValue(rs, "COUNTER_PARTY_BUSINESS"));
            obj.setTxnRefType(getColumnValue(rs, "TXN_REF_TYPE"));
            obj.setSystemTime(EventHelper.toTimestamp(getColumnValue(rs, "SYSTEM_TIME")));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setAcctType(getColumnValue(rs, "ACCT_TYPE"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCounterPartyAmount(EventHelper.toDouble(getColumnValue(rs, "COUNTER_PARTY_AMOUNT")));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setCounterPartyCurrencyLcy(getColumnValue(rs, "COUNTER_PARTY_CURRENCY_LCY"));
            obj.setDirectChannelId(getColumnValue(rs, "DIRECT_CHANNEL_ID"));
            obj.setSequenceNumber(getColumnValue(rs, "SEQUENCE_NUMBER"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranVerifiedDt(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_VERIFIED_DT")));
            obj.setCounterPartyBic(getColumnValue(rs, "COUNTER_PARTY_BIC"));
            obj.setPurAcctNum(getColumnValue(rs, "PUR_ACCT_NUM"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setAcctOccpCode(getColumnValue(rs, "ACCT_OCCP_CODE"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));
            obj.setCounterPartyBankAddress(getColumnValue(rs, "COUNTER_PARTY_BANK_ADDRESS"));
            obj.setStatus(getColumnValue(rs, "STATUS"));
            obj.setEntityType(getColumnValue(rs, "ENTITY_TYPE"));
            obj.setZoneCode(getColumnValue(rs, "ZONE_CODE"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setMerchantCateg(getColumnValue(rs, "MERCHANT_CATEG"));
            obj.setInstAlpha(getColumnValue(rs, "INST_ALPHA"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setPayerIdTemp(getColumnValue(rs, "PAYER_ID_TEMP"));
            obj.setCounterPartyCountryCode(getColumnValue(rs, "COUNTER_PARTY_COUNTRY_CODE"));
            obj.setTerminalIpAddr(getColumnValue(rs, "TERMINAL_IP_ADDR"));
            obj.setFcyTranAmt(EventHelper.toDouble(getColumnValue(rs, "FCY_TRAN_AMT")));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setCounterPartyCurrency(getColumnValue(rs, "COUNTER_PARTY_CURRENCY"));
            obj.setEntryUser(getColumnValue(rs, "ENTRY_USER"));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setPartTranSrlNum(getColumnValue(rs, "PART_TRAN_SRL_NUM"));
            obj.setAccountOwnership(getColumnValue(rs, "ACCOUNT_OWNERSHIP"));
            obj.setSanctionAmt(EventHelper.toDouble(getColumnValue(rs, "SANCTION_AMT")));
            obj.setEightZeros(getColumnValue(rs, "EIGHT_ZEROS"));
            obj.setRefCurncy(EventHelper.toDouble(getColumnValue(rs, "REF_CURNCY")));
            obj.setKycStatus(getColumnValue(rs, "KYC_STATUS"));
            obj.setAgentId(getColumnValue(rs, "AGENT_ID"));
            obj.setCounterPartyAmountLcy(getColumnValue(rs, "COUNTER_PARTY_AMOUNT_LCY"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setChannelDesc(getColumnValue(rs, "CHANNEL_DESC"));
            obj.setCounterPartyBank(getColumnValue(rs, "COUNTER_PARTY_BANK"));
            obj.setBranchIdDesc(getColumnValue(rs, "BRANCH_ID_DESC"));
            obj.setCreDrPtran(getColumnValue(rs, "CRE_DR_PTRAN"));
            obj.setTxnRefId(getColumnValue(rs, "TXN_REF_ID"));
            obj.setSourceBank(getColumnValue(rs, "SOURCE_BANK"));
            obj.setPayeeCity(getColumnValue(rs, "PAYEE_CITY"));
            obj.setDevOwnerId(getColumnValue(rs, "DEV_OWNER_ID"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setEntityId(getColumnValue(rs, "ENTITY_ID"));
            obj.setCounterPartyAddress(getColumnValue(rs, "COUNTER_PARTY_ADDRESS"));
            obj.setSourceOrDestAcctId(getColumnValue(rs, "SOURCE_OR_DEST_ACCT_ID"));
            obj.setRefAmt(EventHelper.toDouble(getColumnValue(rs, "REF_AMT")));
            obj.setPlaceHolder(getColumnValue(rs, "PLACE_HOLDER"));
            obj.setDirectChannelControllerId(getColumnValue(rs, "DIRECT_CHANNEL_CONTROLLER_ID"));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setCounterPartyAccount(getColumnValue(rs, "COUNTER_PARTY_ACCOUNT"));
            obj.setChannelType(getColumnValue(rs, "CHANNEL_TYPE"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setCustRefType(getColumnValue(rs, "CUST_REF_TYPE"));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setTranSrlNo(getColumnValue(rs, "TRAN_SRL_NO"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setEvtInducer(getColumnValue(rs, "EVT_INDUCER"));
            obj.setTxnValueDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_VALUE_DATE")));
            obj.setTranPostedDt(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_POSTED_DT")));
            obj.setSchemeType(getColumnValue(rs, "SCHEME_TYPE"));
            obj.setDesc(getColumnValue(rs, "DESC"));
            obj.setAgentType(getColumnValue(rs, "AGENT_TYPE"));
            obj.setTranEntryDt(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_ENTRY_DT")));
            obj.setCxCifId(getColumnValue(rs, "CX_CIF_ID"));
            obj.setActionAtHost(getColumnValue(rs, "ACTION_AT_HOST"));
            obj.setSchemeCode(getColumnValue(rs, "SCHEME_CODE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setCustRefId(getColumnValue(rs, "CUST_REF_ID"));
            obj.setEventTime(EventHelper.toTimestamp(getColumnValue(rs, "EVENT_TIME")));
            obj.setChannelId(getColumnValue(rs, "CHANNEL_ID"));
            obj.setBillId(getColumnValue(rs, "BILL_ID"));
            obj.setRate(EventHelper.toDouble(getColumnValue(rs, "RATE")));
            obj.setTxnStatus(getColumnValue(rs, "TXN_STATUS"));
            obj.setCounterPartyName(getColumnValue(rs, "COUNTER_PARTY_NAME"));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setEffAvlBal(EventHelper.toDouble(getColumnValue(rs, "EFF_AVL_BAL")));
            obj.setTranCategory(getColumnValue(rs, "TRAN_CATEGORY"));
            obj.setTellerNumber(getColumnValue(rs, "TELLER_NUMBER"));
            obj.setInstrumentId(EventHelper.toLong(getColumnValue(rs, "INSTRUMENT_ID")));
            obj.setBalanceCur(getColumnValue(rs, "BALANCE_CUR"));
            obj.setCounterPartyBankCode(getColumnValue(rs, "COUNTER_PARTY_BANK_CODE"));
            obj.setLedgerBal(EventHelper.toDouble(getColumnValue(rs, "LEDGER_BAL")));
            obj.setEmployeeId(getColumnValue(rs, "EMPLOYEE_ID"));
            obj.setAcctSolId(getColumnValue(rs, "ACCT_SOL_ID"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setTxnAmtCur(getColumnValue(rs, "TXN_AMT_CUR"));
            obj.setRefCurrency(getColumnValue(rs, "REF_CURRENCY"));
            obj.setModeOprnCode(getColumnValue(rs, "MODE_OPRN_CODE"));
            obj.setMsgName(getColumnValue(rs, "MSG_NAME"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setAccountOpendate(EventHelper.toTimestamp(getColumnValue(rs, "ACCOUNT_OPENDATE")));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setHdrmkrs(getColumnValue(rs, "HDRMKRS"));
            obj.setTranBrId(getColumnValue(rs, "TRAN_BR_ID"));
            obj.setRemitterName(getColumnValue(rs, "REMITTER_NAME"));
            obj.setInstrumentType(getColumnValue(rs, "INSTRUMENT_TYPE"));
            obj.setShadowBal(EventHelper.toDouble(getColumnValue(rs, "SHADOW_BAL")));
            obj.setRefTranAmt(EventHelper.toDouble(getColumnValue(rs, "REF_TRAN_AMT")));
            obj.setEod(EventHelper.toLong(getColumnValue(rs, "EOD")));
            obj.setRemarks(getColumnValue(rs, "REMARKS"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"EXCEPTION\",\"BENEFICIARY_NAME\",\"ZONE_DATE\",\"FCNR_FLAG\",\"BIN\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"ACCOUNTID\",\"COUNTER_PARTY_BUSINESS\",\"TXN_REF_TYPE\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"ACCT_TYPE\",\"HOST_USER_ID\",\"HOST_ID\",\"COUNTER_PARTY_AMOUNT\",\"TXN_CODE\",\"COUNTER_PARTY_CURRENCY_LCY\",\"DIRECT_CHANNEL_ID\",\"SEQUENCE_NUMBER\",\"TRAN_CRNCY_CODE\",\"TRAN_VERIFIED_DT\",\"COUNTER_PARTY_BIC\",\"PUR_ACCT_NUM\",\"CX_CUST_ID\",\"EVENT_NAME\",\"PAYEE_ID\",\"ACCT_OCCP_CODE\",\"ACCT_STATUS\",\"SYSTEM_COUNTRY\",\"COUNTER_PARTY_BANK_ADDRESS\",\"STATUS\",\"ENTITY_TYPE\",\"ZONE_CODE\",\"COUNTRY_CODE\",\"ACCT_OPEN_DATE\",\"MERCHANT_CATEG\",\"INST_ALPHA\",\"MSG_SOURCE\",\"PAYER_ID_TEMP\",\"COUNTER_PARTY_COUNTRY_CODE\",\"TERMINAL_IP_ADDR\",\"FCY_TRAN_AMT\",\"CX_ACCT_ID\",\"COUNTER_PARTY_CURRENCY\",\"ENTRY_USER\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"PART_TRAN_SRL_NUM\",\"ACCOUNT_OWNERSHIP\",\"SANCTION_AMT\",\"EIGHT_ZEROS\",\"REF_CURNCY\",\"KYC_STATUS\",\"AGENT_ID\",\"COUNTER_PARTY_AMOUNT_LCY\",\"TRAN_TYPE\",\"CHANNEL_DESC\",\"COUNTER_PARTY_BANK\",\"BRANCH_ID_DESC\",\"CRE_DR_PTRAN\",\"TXN_REF_ID\",\"SOURCE_BANK\",\"PAYEE_CITY\",\"DEV_OWNER_ID\",\"DEVICE_ID\",\"TRAN_ID\",\"ENTITY_ID\",\"COUNTER_PARTY_ADDRESS\",\"SOURCE_OR_DEST_ACCT_ID\",\"REF_AMT\",\"PLACE_HOLDER\",\"DIRECT_CHANNEL_CONTROLLER_ID\",\"PAYEE_NAME\",\"COUNTER_PARTY_ACCOUNT\",\"CHANNEL_TYPE\",\"ACCT_NAME\",\"CUST_REF_TYPE\",\"CUST_CARD_ID\",\"TRAN_SRL_NO\",\"AVL_BAL\",\"EVT_INDUCER\",\"TXN_VALUE_DATE\",\"TRAN_POSTED_DT\",\"SCHEME_TYPE\",\"DESC\",\"AGENT_TYPE\",\"TRAN_ENTRY_DT\",\"CX_CIF_ID\",\"ACTION_AT_HOST\",\"SCHEME_CODE\",\"KEYS\",\"CUST_REF_ID\",\"EVENT_TIME\",\"CHANNEL_ID\",\"BILL_ID\",\"RATE\",\"TXN_STATUS\",\"COUNTER_PARTY_NAME\",\"TXN_AMT\",\"EFF_AVL_BAL\",\"TRAN_CATEGORY\",\"TELLER_NUMBER\",\"INSTRUMENT_ID\",\"BALANCE_CUR\",\"COUNTER_PARTY_BANK_CODE\",\"LEDGER_BAL\",\"EMPLOYEE_ID\",\"ACCT_SOL_ID\",\"EVENT_SUBTYPE\",\"TXN_AMT_CUR\",\"REF_CURRENCY\",\"MODE_OPRN_CODE\",\"MSG_NAME\",\"CUST_ID\",\"ACCOUNT_OPENDATE\",\"TRAN_DATE\",\"HDRMKRS\",\"TRAN_BR_ID\",\"REMITTER_NAME\",\"INSTRUMENT_TYPE\",\"SHADOW_BAL\",\"REF_TRAN_AMT\",\"EOD\",\"REMARKS\"" +
              " FROM EVENT_FT_ACCOUNT_TXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [EXCEPTION],[BENEFICIARY_NAME],[ZONE_DATE],[FCNR_FLAG],[BIN],[ONLINE_BATCH],[TRAN_SUB_TYPE],[ACCOUNTID],[COUNTER_PARTY_BUSINESS],[TXN_REF_TYPE],[SYSTEM_TIME],[TXN_DR_CR],[ACCT_TYPE],[HOST_USER_ID],[HOST_ID],[COUNTER_PARTY_AMOUNT],[TXN_CODE],[COUNTER_PARTY_CURRENCY_LCY],[DIRECT_CHANNEL_ID],[SEQUENCE_NUMBER],[TRAN_CRNCY_CODE],[TRAN_VERIFIED_DT],[COUNTER_PARTY_BIC],[PUR_ACCT_NUM],[CX_CUST_ID],[EVENT_NAME],[PAYEE_ID],[ACCT_OCCP_CODE],[ACCT_STATUS],[SYSTEM_COUNTRY],[COUNTER_PARTY_BANK_ADDRESS],[STATUS],[ENTITY_TYPE],[ZONE_CODE],[COUNTRY_CODE],[ACCT_OPEN_DATE],[MERCHANT_CATEG],[INST_ALPHA],[MSG_SOURCE],[PAYER_ID_TEMP],[COUNTER_PARTY_COUNTRY_CODE],[TERMINAL_IP_ADDR],[FCY_TRAN_AMT],[CX_ACCT_ID],[COUNTER_PARTY_CURRENCY],[ENTRY_USER],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[ADD_ENTITY_ID5],[EVENT_TYPE],[PART_TRAN_SRL_NUM],[ACCOUNT_OWNERSHIP],[SANCTION_AMT],[EIGHT_ZEROS],[REF_CURNCY],[KYC_STATUS],[AGENT_ID],[COUNTER_PARTY_AMOUNT_LCY],[TRAN_TYPE],[CHANNEL_DESC],[COUNTER_PARTY_BANK],[BRANCH_ID_DESC],[CRE_DR_PTRAN],[TXN_REF_ID],[SOURCE_BANK],[PAYEE_CITY],[DEV_OWNER_ID],[DEVICE_ID],[TRAN_ID],[ENTITY_ID],[COUNTER_PARTY_ADDRESS],[SOURCE_OR_DEST_ACCT_ID],[REF_AMT],[PLACE_HOLDER],[DIRECT_CHANNEL_CONTROLLER_ID],[PAYEE_NAME],[COUNTER_PARTY_ACCOUNT],[CHANNEL_TYPE],[ACCT_NAME],[CUST_REF_TYPE],[CUST_CARD_ID],[TRAN_SRL_NO],[AVL_BAL],[EVT_INDUCER],[TXN_VALUE_DATE],[TRAN_POSTED_DT],[SCHEME_TYPE],[DESC],[AGENT_TYPE],[TRAN_ENTRY_DT],[CX_CIF_ID],[ACTION_AT_HOST],[SCHEME_CODE],[KEYS],[CUST_REF_ID],[EVENT_TIME],[CHANNEL_ID],[BILL_ID],[RATE],[TXN_STATUS],[COUNTER_PARTY_NAME],[TXN_AMT],[EFF_AVL_BAL],[TRAN_CATEGORY],[TELLER_NUMBER],[INSTRUMENT_ID],[BALANCE_CUR],[COUNTER_PARTY_BANK_CODE],[LEDGER_BAL],[EMPLOYEE_ID],[ACCT_SOL_ID],[EVENT_SUBTYPE],[TXN_AMT_CUR],[REF_CURRENCY],[MODE_OPRN_CODE],[MSG_NAME],[CUST_ID],[ACCOUNT_OPENDATE],[TRAN_DATE],[HDRMKRS],[TRAN_BR_ID],[REMITTER_NAME],[INSTRUMENT_TYPE],[SHADOW_BAL],[REF_TRAN_AMT],[EOD],[REMARKS]" +
              " FROM EVENT_FT_ACCOUNT_TXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`EXCEPTION`,`BENEFICIARY_NAME`,`ZONE_DATE`,`FCNR_FLAG`,`BIN`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`ACCOUNTID`,`COUNTER_PARTY_BUSINESS`,`TXN_REF_TYPE`,`SYSTEM_TIME`,`TXN_DR_CR`,`ACCT_TYPE`,`HOST_USER_ID`,`HOST_ID`,`COUNTER_PARTY_AMOUNT`,`TXN_CODE`,`COUNTER_PARTY_CURRENCY_LCY`,`DIRECT_CHANNEL_ID`,`SEQUENCE_NUMBER`,`TRAN_CRNCY_CODE`,`TRAN_VERIFIED_DT`,`COUNTER_PARTY_BIC`,`PUR_ACCT_NUM`,`CX_CUST_ID`,`EVENT_NAME`,`PAYEE_ID`,`ACCT_OCCP_CODE`,`ACCT_STATUS`,`SYSTEM_COUNTRY`,`COUNTER_PARTY_BANK_ADDRESS`,`STATUS`,`ENTITY_TYPE`,`ZONE_CODE`,`COUNTRY_CODE`,`ACCT_OPEN_DATE`,`MERCHANT_CATEG`,`INST_ALPHA`,`MSG_SOURCE`,`PAYER_ID_TEMP`,`COUNTER_PARTY_COUNTRY_CODE`,`TERMINAL_IP_ADDR`,`FCY_TRAN_AMT`,`CX_ACCT_ID`,`COUNTER_PARTY_CURRENCY`,`ENTRY_USER`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`PART_TRAN_SRL_NUM`,`ACCOUNT_OWNERSHIP`,`SANCTION_AMT`,`EIGHT_ZEROS`,`REF_CURNCY`,`KYC_STATUS`,`AGENT_ID`,`COUNTER_PARTY_AMOUNT_LCY`,`TRAN_TYPE`,`CHANNEL_DESC`,`COUNTER_PARTY_BANK`,`BRANCH_ID_DESC`,`CRE_DR_PTRAN`,`TXN_REF_ID`,`SOURCE_BANK`,`PAYEE_CITY`,`DEV_OWNER_ID`,`DEVICE_ID`,`TRAN_ID`,`ENTITY_ID`,`COUNTER_PARTY_ADDRESS`,`SOURCE_OR_DEST_ACCT_ID`,`REF_AMT`,`PLACE_HOLDER`,`DIRECT_CHANNEL_CONTROLLER_ID`,`PAYEE_NAME`,`COUNTER_PARTY_ACCOUNT`,`CHANNEL_TYPE`,`ACCT_NAME`,`CUST_REF_TYPE`,`CUST_CARD_ID`,`TRAN_SRL_NO`,`AVL_BAL`,`EVT_INDUCER`,`TXN_VALUE_DATE`,`TRAN_POSTED_DT`,`SCHEME_TYPE`,`DESC`,`AGENT_TYPE`,`TRAN_ENTRY_DT`,`CX_CIF_ID`,`ACTION_AT_HOST`,`SCHEME_CODE`,`KEYS`,`CUST_REF_ID`,`EVENT_TIME`,`CHANNEL_ID`,`BILL_ID`,`RATE`,`TXN_STATUS`,`COUNTER_PARTY_NAME`,`TXN_AMT`,`EFF_AVL_BAL`,`TRAN_CATEGORY`,`TELLER_NUMBER`,`INSTRUMENT_ID`,`BALANCE_CUR`,`COUNTER_PARTY_BANK_CODE`,`LEDGER_BAL`,`EMPLOYEE_ID`,`ACCT_SOL_ID`,`EVENT_SUBTYPE`,`TXN_AMT_CUR`,`REF_CURRENCY`,`MODE_OPRN_CODE`,`MSG_NAME`,`CUST_ID`,`ACCOUNT_OPENDATE`,`TRAN_DATE`,`HDRMKRS`,`TRAN_BR_ID`,`REMITTER_NAME`,`INSTRUMENT_TYPE`,`SHADOW_BAL`,`REF_TRAN_AMT`,`EOD`,`REMARKS`" +
              " FROM EVENT_FT_ACCOUNT_TXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_ACCOUNT_TXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"EXCEPTION\",\"BENEFICIARY_NAME\",\"ZONE_DATE\",\"FCNR_FLAG\",\"BIN\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"ACCOUNTID\",\"COUNTER_PARTY_BUSINESS\",\"TXN_REF_TYPE\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"ACCT_TYPE\",\"HOST_USER_ID\",\"HOST_ID\",\"COUNTER_PARTY_AMOUNT\",\"TXN_CODE\",\"COUNTER_PARTY_CURRENCY_LCY\",\"DIRECT_CHANNEL_ID\",\"SEQUENCE_NUMBER\",\"TRAN_CRNCY_CODE\",\"TRAN_VERIFIED_DT\",\"COUNTER_PARTY_BIC\",\"PUR_ACCT_NUM\",\"CX_CUST_ID\",\"EVENT_NAME\",\"PAYEE_ID\",\"ACCT_OCCP_CODE\",\"ACCT_STATUS\",\"SYSTEM_COUNTRY\",\"COUNTER_PARTY_BANK_ADDRESS\",\"STATUS\",\"ENTITY_TYPE\",\"ZONE_CODE\",\"COUNTRY_CODE\",\"ACCT_OPEN_DATE\",\"MERCHANT_CATEG\",\"INST_ALPHA\",\"MSG_SOURCE\",\"PAYER_ID_TEMP\",\"COUNTER_PARTY_COUNTRY_CODE\",\"TERMINAL_IP_ADDR\",\"FCY_TRAN_AMT\",\"CX_ACCT_ID\",\"COUNTER_PARTY_CURRENCY\",\"ENTRY_USER\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"PART_TRAN_SRL_NUM\",\"ACCOUNT_OWNERSHIP\",\"SANCTION_AMT\",\"EIGHT_ZEROS\",\"REF_CURNCY\",\"KYC_STATUS\",\"AGENT_ID\",\"COUNTER_PARTY_AMOUNT_LCY\",\"TRAN_TYPE\",\"CHANNEL_DESC\",\"COUNTER_PARTY_BANK\",\"BRANCH_ID_DESC\",\"CRE_DR_PTRAN\",\"TXN_REF_ID\",\"SOURCE_BANK\",\"PAYEE_CITY\",\"DEV_OWNER_ID\",\"DEVICE_ID\",\"TRAN_ID\",\"ENTITY_ID\",\"COUNTER_PARTY_ADDRESS\",\"SOURCE_OR_DEST_ACCT_ID\",\"REF_AMT\",\"PLACE_HOLDER\",\"DIRECT_CHANNEL_CONTROLLER_ID\",\"PAYEE_NAME\",\"COUNTER_PARTY_ACCOUNT\",\"CHANNEL_TYPE\",\"ACCT_NAME\",\"CUST_REF_TYPE\",\"CUST_CARD_ID\",\"TRAN_SRL_NO\",\"AVL_BAL\",\"EVT_INDUCER\",\"TXN_VALUE_DATE\",\"TRAN_POSTED_DT\",\"SCHEME_TYPE\",\"DESC\",\"AGENT_TYPE\",\"TRAN_ENTRY_DT\",\"CX_CIF_ID\",\"ACTION_AT_HOST\",\"SCHEME_CODE\",\"KEYS\",\"CUST_REF_ID\",\"EVENT_TIME\",\"CHANNEL_ID\",\"BILL_ID\",\"RATE\",\"TXN_STATUS\",\"COUNTER_PARTY_NAME\",\"TXN_AMT\",\"EFF_AVL_BAL\",\"TRAN_CATEGORY\",\"TELLER_NUMBER\",\"INSTRUMENT_ID\",\"BALANCE_CUR\",\"COUNTER_PARTY_BANK_CODE\",\"LEDGER_BAL\",\"EMPLOYEE_ID\",\"ACCT_SOL_ID\",\"EVENT_SUBTYPE\",\"TXN_AMT_CUR\",\"REF_CURRENCY\",\"MODE_OPRN_CODE\",\"MSG_NAME\",\"CUST_ID\",\"ACCOUNT_OPENDATE\",\"TRAN_DATE\",\"HDRMKRS\",\"TRAN_BR_ID\",\"REMITTER_NAME\",\"INSTRUMENT_TYPE\",\"SHADOW_BAL\",\"REF_TRAN_AMT\",\"EOD\",\"REMARKS\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[EXCEPTION],[BENEFICIARY_NAME],[ZONE_DATE],[FCNR_FLAG],[BIN],[ONLINE_BATCH],[TRAN_SUB_TYPE],[ACCOUNTID],[COUNTER_PARTY_BUSINESS],[TXN_REF_TYPE],[SYSTEM_TIME],[TXN_DR_CR],[ACCT_TYPE],[HOST_USER_ID],[HOST_ID],[COUNTER_PARTY_AMOUNT],[TXN_CODE],[COUNTER_PARTY_CURRENCY_LCY],[DIRECT_CHANNEL_ID],[SEQUENCE_NUMBER],[TRAN_CRNCY_CODE],[TRAN_VERIFIED_DT],[COUNTER_PARTY_BIC],[PUR_ACCT_NUM],[CX_CUST_ID],[EVENT_NAME],[PAYEE_ID],[ACCT_OCCP_CODE],[ACCT_STATUS],[SYSTEM_COUNTRY],[COUNTER_PARTY_BANK_ADDRESS],[STATUS],[ENTITY_TYPE],[ZONE_CODE],[COUNTRY_CODE],[ACCT_OPEN_DATE],[MERCHANT_CATEG],[INST_ALPHA],[MSG_SOURCE],[PAYER_ID_TEMP],[COUNTER_PARTY_COUNTRY_CODE],[TERMINAL_IP_ADDR],[FCY_TRAN_AMT],[CX_ACCT_ID],[COUNTER_PARTY_CURRENCY],[ENTRY_USER],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[ADD_ENTITY_ID5],[EVENT_TYPE],[PART_TRAN_SRL_NUM],[ACCOUNT_OWNERSHIP],[SANCTION_AMT],[EIGHT_ZEROS],[REF_CURNCY],[KYC_STATUS],[AGENT_ID],[COUNTER_PARTY_AMOUNT_LCY],[TRAN_TYPE],[CHANNEL_DESC],[COUNTER_PARTY_BANK],[BRANCH_ID_DESC],[CRE_DR_PTRAN],[TXN_REF_ID],[SOURCE_BANK],[PAYEE_CITY],[DEV_OWNER_ID],[DEVICE_ID],[TRAN_ID],[ENTITY_ID],[COUNTER_PARTY_ADDRESS],[SOURCE_OR_DEST_ACCT_ID],[REF_AMT],[PLACE_HOLDER],[DIRECT_CHANNEL_CONTROLLER_ID],[PAYEE_NAME],[COUNTER_PARTY_ACCOUNT],[CHANNEL_TYPE],[ACCT_NAME],[CUST_REF_TYPE],[CUST_CARD_ID],[TRAN_SRL_NO],[AVL_BAL],[EVT_INDUCER],[TXN_VALUE_DATE],[TRAN_POSTED_DT],[SCHEME_TYPE],[DESC],[AGENT_TYPE],[TRAN_ENTRY_DT],[CX_CIF_ID],[ACTION_AT_HOST],[SCHEME_CODE],[KEYS],[CUST_REF_ID],[EVENT_TIME],[CHANNEL_ID],[BILL_ID],[RATE],[TXN_STATUS],[COUNTER_PARTY_NAME],[TXN_AMT],[EFF_AVL_BAL],[TRAN_CATEGORY],[TELLER_NUMBER],[INSTRUMENT_ID],[BALANCE_CUR],[COUNTER_PARTY_BANK_CODE],[LEDGER_BAL],[EMPLOYEE_ID],[ACCT_SOL_ID],[EVENT_SUBTYPE],[TXN_AMT_CUR],[REF_CURRENCY],[MODE_OPRN_CODE],[MSG_NAME],[CUST_ID],[ACCOUNT_OPENDATE],[TRAN_DATE],[HDRMKRS],[TRAN_BR_ID],[REMITTER_NAME],[INSTRUMENT_TYPE],[SHADOW_BAL],[REF_TRAN_AMT],[EOD],[REMARKS]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`EXCEPTION`,`BENEFICIARY_NAME`,`ZONE_DATE`,`FCNR_FLAG`,`BIN`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`ACCOUNTID`,`COUNTER_PARTY_BUSINESS`,`TXN_REF_TYPE`,`SYSTEM_TIME`,`TXN_DR_CR`,`ACCT_TYPE`,`HOST_USER_ID`,`HOST_ID`,`COUNTER_PARTY_AMOUNT`,`TXN_CODE`,`COUNTER_PARTY_CURRENCY_LCY`,`DIRECT_CHANNEL_ID`,`SEQUENCE_NUMBER`,`TRAN_CRNCY_CODE`,`TRAN_VERIFIED_DT`,`COUNTER_PARTY_BIC`,`PUR_ACCT_NUM`,`CX_CUST_ID`,`EVENT_NAME`,`PAYEE_ID`,`ACCT_OCCP_CODE`,`ACCT_STATUS`,`SYSTEM_COUNTRY`,`COUNTER_PARTY_BANK_ADDRESS`,`STATUS`,`ENTITY_TYPE`,`ZONE_CODE`,`COUNTRY_CODE`,`ACCT_OPEN_DATE`,`MERCHANT_CATEG`,`INST_ALPHA`,`MSG_SOURCE`,`PAYER_ID_TEMP`,`COUNTER_PARTY_COUNTRY_CODE`,`TERMINAL_IP_ADDR`,`FCY_TRAN_AMT`,`CX_ACCT_ID`,`COUNTER_PARTY_CURRENCY`,`ENTRY_USER`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`PART_TRAN_SRL_NUM`,`ACCOUNT_OWNERSHIP`,`SANCTION_AMT`,`EIGHT_ZEROS`,`REF_CURNCY`,`KYC_STATUS`,`AGENT_ID`,`COUNTER_PARTY_AMOUNT_LCY`,`TRAN_TYPE`,`CHANNEL_DESC`,`COUNTER_PARTY_BANK`,`BRANCH_ID_DESC`,`CRE_DR_PTRAN`,`TXN_REF_ID`,`SOURCE_BANK`,`PAYEE_CITY`,`DEV_OWNER_ID`,`DEVICE_ID`,`TRAN_ID`,`ENTITY_ID`,`COUNTER_PARTY_ADDRESS`,`SOURCE_OR_DEST_ACCT_ID`,`REF_AMT`,`PLACE_HOLDER`,`DIRECT_CHANNEL_CONTROLLER_ID`,`PAYEE_NAME`,`COUNTER_PARTY_ACCOUNT`,`CHANNEL_TYPE`,`ACCT_NAME`,`CUST_REF_TYPE`,`CUST_CARD_ID`,`TRAN_SRL_NO`,`AVL_BAL`,`EVT_INDUCER`,`TXN_VALUE_DATE`,`TRAN_POSTED_DT`,`SCHEME_TYPE`,`DESC`,`AGENT_TYPE`,`TRAN_ENTRY_DT`,`CX_CIF_ID`,`ACTION_AT_HOST`,`SCHEME_CODE`,`KEYS`,`CUST_REF_ID`,`EVENT_TIME`,`CHANNEL_ID`,`BILL_ID`,`RATE`,`TXN_STATUS`,`COUNTER_PARTY_NAME`,`TXN_AMT`,`EFF_AVL_BAL`,`TRAN_CATEGORY`,`TELLER_NUMBER`,`INSTRUMENT_ID`,`BALANCE_CUR`,`COUNTER_PARTY_BANK_CODE`,`LEDGER_BAL`,`EMPLOYEE_ID`,`ACCT_SOL_ID`,`EVENT_SUBTYPE`,`TXN_AMT_CUR`,`REF_CURRENCY`,`MODE_OPRN_CODE`,`MSG_NAME`,`CUST_ID`,`ACCOUNT_OPENDATE`,`TRAN_DATE`,`HDRMKRS`,`TRAN_BR_ID`,`REMITTER_NAME`,`INSTRUMENT_TYPE`,`SHADOW_BAL`,`REF_TRAN_AMT`,`EOD`,`REMARKS`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

