/**
 * Created by hemanshu on 12/4/18.
 */

import {Component, OnInit} from "@angular/core";
import {
    custFatcaDetails, CustomerDetails, customerDiff, DiffJson, FatcaDetails, FatcaDiff,
    FatcaNonUSDetails, riskLevelDiff
} from "../model.component";
import {ActivatedRoute, Router} from "@angular/router";
import {KYCService} from "../service/main.service";
import any = jasmine.any;

declare var swal: any;

@Component({
    selector: 'kyc-search-result',
    template: require('./search-result.template.html'),
    styles: [require('../css/dashboard.css').toString()]
})

export class KYCSearchResultComponent implements OnInit {

    constructor(private router: Router, private route: ActivatedRoute, private appService: KYCService) {
    }

    activeScreen: string = "personalInfo";
    completeDetails: any;
    details: CustomerDetails = new CustomerDetails();
    customerId: any;
    isEdit: boolean = false;
    isEditRiskLevel : boolean = false;
    fatcaDetails: custFatcaDetails = new custFatcaDetails();
    isEditRelatedParty : boolean = false;
    mainDiff: DiffJson = new DiffJson();
    fatcaDiff: FatcaDiff = new FatcaDiff();
    // changeInPersonalInfo : boolean = false;
    riskLevelDiff: riskLevelDiff = new riskLevelDiff();
    customerDiff: customerDiff = new customerDiff();
    riskLevel: string = "";

    ngOnInit() {
        this.customerId = this.appService.getCustomerId();
        this.completeDetails = this.appService.getCustomerDetails();
        this.details = JSON.parse(JSON.stringify(this.completeDetails.custDetails));
        if (this.completeDetails.custFatcaDetails) {
            this.fatcaDetails = JSON.parse(JSON.stringify(this.completeDetails.custFatcaDetails));
        }
        if (this.completeDetails.riskLevel) {
            this.riskLevel = JSON.parse(JSON.stringify(this.completeDetails.riskLevel));
        }
        // this.changeInPersonalInfo = false;
    }


    backToSearch() {
        this.router.navigate(['../'], {skipLocationChange: true, relativeTo: this.route});
    }


    showInfo(val: string) {
        this.activeScreen = val;
    }


    editDetails() {
        this.isEditRelatedParty = !this.isEditRelatedParty;
        if (this.riskLevel && this.riskLevel != "") {
            this.isEditRiskLevel = !this.isEditRiskLevel;
        } else {
            swal({
                title: "Risk Level Not Available",
                type: "error",
                text: "Can not edit risk level for the following customer"
            });
        }
        this.isEdit = !this.isEdit;
    }

    /*  updateDiffJson(){
          if() {
              this.changeInPersonalInfo = true;
          }
      }
  */

    updateFatcaDetailModel(value : string){
        if(this.fatcaDetails.usPerson !== this.completeDetails.custFatcaDetails.usPerson){
            this.fatcaDetails = new custFatcaDetails();
            this.fatcaDetails.usPerson = value;
        } else if(this.fatcaDetails.usPerson == this.completeDetails.custFatcaDetails.usPerson){
            this.fatcaDetails = JSON.parse(JSON.stringify(this.completeDetails.custFatcaDetails));
        }
    }

    submitChanges() {

        this.mainDiff = new DiffJson();
        this.fatcaDiff = new FatcaDiff();
        this.riskLevelDiff = new riskLevelDiff();
        this.customerDiff = new customerDiff();

        this.mainDiff.custId = this.customerId;

        if (this.completeDetails.riskLevel) {
            if (this.completeDetails.riskLevel !== this.riskLevel) {
                let a = {};
                a["riskLevel"] = this.riskLevel;
                this.riskLevelDiff.updates = a;
                this.mainDiff.changes.push(this.riskLevelDiff);
            }
        }

        if (this.completeDetails.custDetails.bankRelatedFamily !== this.details.bankRelatedFamily) {
            let a = {};
            a["bankRelatedFamily"] = this.details.bankRelatedFamily;
            this.customerDiff.updates = a;
            this.mainDiff.changes.push(this.customerDiff);
        }


        if (this.completeDetails.custFatcaDetails) {
            if (JSON.stringify(this.completeDetails.custFatcaDetails) !== JSON.stringify(this.fatcaDetails)) {
                this.fatcaDetails.custId = this.customerId;
                this.fatcaDetails.classification = this.details.custType;
                delete this.fatcaDetails["map"];
                this.fatcaDiff.updates = this.fatcaDetails;
                this.mainDiff.changes.push(this.fatcaDiff);
            }
        } else {
            if (this.fatcaDetails.usPerson !== "") {
                this.fatcaDetails.custId = this.customerId;
                this.fatcaDetails.classification = this.details.custType;
                this.fatcaDiff.updates = this.fatcaDetails;
                this.mainDiff.changes.push(this.fatcaDiff);
            }else if((this.details.custType) && (this.details.custType).toUpperCase() == "FINANCIALINSTITUITION"){
                this.fatcaDetails.custId = this.customerId;
                this.fatcaDetails.classification = this.details.custType;
                this.fatcaDiff.updates = this.fatcaDetails;
                this.mainDiff.changes.push(this.fatcaDiff);
            }
        }

        let that = this;
        this.appService.submitChanges(this.mainDiff).subscribe((info: any) => {
            swal({
                title: "Submitted Successfully",
                type: "success",
                text: ""
            });
            that.backToSearch();
        }, (error: any) => {
            swal({
                title: error._body,
                type: "error",
                text: "Changes not submitted"
            });
        });


    }
}

