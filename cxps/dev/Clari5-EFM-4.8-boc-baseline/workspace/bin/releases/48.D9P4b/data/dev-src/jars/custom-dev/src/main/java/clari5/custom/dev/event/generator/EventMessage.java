package clari5.custom.dev.event.generator;

import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import com.prowidesoftware.swift.model.SwiftMessage;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class EventMessage extends MTEventGenerator {

    static Hocon url;
    static {
        url = new Hocon();
        url.loadFromContext("reqUrl.conf");
    }

    public static String splitAmount(String amount) {
        String[] splitamt = amount.split(",");
        String amountInString = splitamt.length > 1 ? splitamt[0] + "." + splitamt[1] : splitamt[0];

        return amountInString;
    }

    public static String getFormattedDate(long dateInMillis) {

        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = df.format(dateInMillis);
        date += " " + LocalTime.now();
        return date;
    }

    public static long getDate(String date) throws ParseException {
        DateFormat formatter;
        formatter = new SimpleDateFormat("yyMMdd");
        Date date1 = formatter.parse(date);
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.format(date1);
        return date1.getTime();
    }

    public static void setAmountToDouble(Map<String, String> map) {
        Iterator<String> itr = map.keySet().iterator();
        while (itr.hasNext()) {
            String keys = itr.next();
            if (map.containsKey(keys)) {
                if (keys.endsWith("_AMT") || keys.endsWith("_amt")) {
                    map.put(keys, splitAmount(map.get("TRAN_AMT")));
                }
            }
        }
    }

    public String messageMap(Map<String, String> map, SwiftMessage message) throws IOException, ParseException {
        System.out.println("data in map is: "+map +"message type is : "+ message.getType());
        System.out.println();
        if (message.toMT().isInput()){
            map.put("REM_TYPE", "I");
        } else map.put("REM_TYPE", "O");
        setDate(map);
        setAmountToDouble(map);
        CxJson json = CxJson.parse(getEvent().getJSON());
        Iterator<String> itr = map.keySet().iterator();
        json.remove("event_id");
        json.put("event_id", "mt-" + System.nanoTime());
        String msgBody = json.getString("msgBody");
        CxJson json1 = CxJson.parse(msgBody);

        while (itr.hasNext()) {
            String key = itr.next();
            json1.remove(key);
            if (key.equalsIgnoreCase("REM_ACCT_NO") || key.equalsIgnoreCase("BEN_ACCT_NO")){
                map.put(key, map.get(key).replaceAll("/", "").trim());
            }
            json1.put(key, null != map.get(key) ? map.get(key) : "");
        }
        json.remove("msgBody");
        String appendjson = json1.toString().replaceAll("\"", "'");
        appendjson = '"' + appendjson + '"';
        String newJson = json.toString();
        String json3 = newJson.substring(0, newJson.length() - 1) + ",\"msgBody\":" + appendjson + "}";
        System.out.println(json3);
        System.out.println();
        CxRest.equeue(url.getString("Request.DN"), "HOST", json.get("event_id").toString(), null != json1.get("BEN_ACCT_NO") ? json1.get("BEN_ACCT_NO").toString() : "default123", getDate(map.get("TRN_DATE")), json3);
        return json3;
    }

    public static void setDate(Map<String, String> map) throws ParseException {
        Iterator<String> itr = map.keySet().iterator();
        while (itr.hasNext()) {
            String keys = itr.next();
            if (map.containsKey(keys)) {
                if (keys.endsWith("_date") || keys.endsWith("_DATE")) {
                    map.put(keys, getFormattedDate(getDate(map.get(keys))));
                }
            }
            if (map.containsKey(keys)) {
                if (keys.endsWith("_time") || keys.endsWith("_TIME")) {
                    map.put(keys, getFormattedDate(getDate(map.get(keys))));
                }
            }
        }
        if (map.containsKey("EVENTTS")) {
            map.put("EVENTTS", getFormattedDate(getDate(map.get("EVENTTS"))));
        }
    }

    @Override
    public Map<String, String> getEventFromMessage(SwiftMessage message) {
        return null;
    }
}
