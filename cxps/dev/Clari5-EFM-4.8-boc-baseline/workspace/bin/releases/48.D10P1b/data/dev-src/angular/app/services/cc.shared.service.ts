/**
 * Created by utsav on 10/12/16.
 */

import {Observable} from "rxjs";
import {BaseRequestOptions} from "@angular/http";

declare var loggedInUserId : string;

export class SharedService {

    public static loggedInTenant : string = "GLOBAL";

    public static loggedInUserId : string = "";

    public static isLoggedIn: boolean = false;

    public static currentSelectedVersion: string;

    public static baseUrl: string = "/efm/rest/1.0";

    public static aceServletUrl : string = "/efm/AceServlet";

    public static rfmServletUrl : string = "/efm/rest/rfm";

    public static getFormattedError(errorText: string): string {
        let htmlHead = errorText.split("<style")[0];
        let htmlBody = errorText.split("</style>")[1];
        if (htmlBody === undefined) {
            htmlBody = "";
        } else {
            htmlBody = htmlBody.replace("h1", "h3").replace("h1", "h3");
        }
        return htmlHead + htmlBody;
    }


    public static extractText(response: any){
        try{
            return response.text();
        }catch(e){
            if (!response.ok && response.status === 401){
                window.location.href = window.location.origin+"/efm";
            }
        }
    }

    public static extractJson(response: any){
        try{
            return response.json();
        }catch(e){
            // Try redirect code
            if (!response.ok && response.status === 401){
                window.location.href = window.location.origin+"/efm";
            }
        }
    }

    public static handleError(error: any){
        if (error.status === 401){
            window.location.href = window.location.origin+"/efm";
        }
        return Observable.throw(error);
    }

}


/**
 * This class must be used by every service while sending custom options to
 * ensure that Finger Print Data is sent to backend for validation,
 * otherwise session will be assumed invalid, and user will be logged Out.
 *
 * Also using options during http calls is optional, as this class is used by default.
 * */
export class CustomRequestOptions extends BaseRequestOptions {
    constructor() {
        super();
        // Headers Used for Device Finger Printing
        let res = window.screen.width + 'x' + window.screen.height;
        let os_core = navigator.hardwareConcurrency;
        let platform = navigator.platform;
        this.headers.append("Content-type", "application/json");
        // this.headers.append("resolution", res);
        // this.headers.append("timezone", "" + new Date().getTimezoneOffset());
        // this.headers.append("cpu-core", "" + os_core);
        // this.headers.append("platform", platform);
        // this.headers.append('My-Custom-Header', 'MyCustomHeaderValue');
    }

    static getHeaders() : any{
        let headers : any = [];
        let res = window.screen.width + 'x' + window.screen.height;
        let os_core = navigator.hardwareConcurrency;
        let platform = navigator.platform;
        // headers.push({name:"Content-type", value:"application/json"});
        // headers.push({name : "resolution", value: res});
        // headers.push({name : "timezone", value: "" + new Date().getTimezoneOffset()});
        // headers.push({name : "cpu-core", value: "" + os_core});
        // headers.push({name : "platform", value: platform});
        return headers;
    }
}
