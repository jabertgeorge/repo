import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppRouter} from "./app.router";
import {StartingMainComponent} from "./components/starting-main.component";
import {GenesisService} from "./services/cc.genesis.service";
import {SharedService} from "./services/cc.shared.service";
import {CoreUtil} from "./services/efm.core-util.service";
import {KYCSearchResultComponent} from "./components/kyc/search-result/search-result.component";
import {KYCService} from "./components/kyc/service/main.service";
import {KYCSearchComponent} from "./components/kyc/search-page/search-page.component";
import {Ng2Bs3ModalModule} from "ng2-bs3-modal/ng2-bs3-modal";



@NgModule({
    declarations: [

        //KYC
        KYCSearchComponent,KYCSearchResultComponent , StartingMainComponent

    ],
    imports: [
        BrowserModule, HttpModule, ReactiveFormsModule, FormsModule, BrowserAnimationsModule,
         AppRouter , Ng2Bs3ModalModule
    ],
    providers: [
        KYCService, GenesisService , SharedService, CoreUtil
    ],
    bootstrap: [
        StartingMainComponent
    ]
})
export class AppModule {
}


