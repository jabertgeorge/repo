/**
 * Created By : Lalit
 * Created On : 8/1/17
 * Organisation: CustomerXPs Software Private Ltd.
 */

import {SharedService} from "./cc.shared.service";

declare var swal: any;

export class CoreUtil {

    public static sendDefaultSwal(text: string) {
        swal({
            title: "Error Occurred",
            type: "error",
            text: SharedService.getFormattedError(text)
        });
    }

    public static getFormattedAceServletBody(menu: string, cmd: string, payload: string): string {
        let formData = "uId=" + SharedService.loggedInUserId;
        formData += "&menu=" + menu;
        formData += "&cmd=" + cmd;
        formData += "&payload=" + encodeURIComponent(payload);
        formData += "&chId=web&token=dummy";
        return formData;
    }

    public static hasAceServletError(info: any): boolean {
        try {
            if (Object.keys(info).indexOf("error") !== -1) {
                swal({
                    title: "Error Occurred",
                    type: "error", text: JSON.stringify(info)
                });
                return true;
            }
        } catch (err) {
            console.error(err);
            swal({
                title: "Error Occurred",
                type: "error", text: SharedService.getFormattedError(info)
            });
            return true;
        }
        return false;
    }
    public static successfulAlert(text: string) {
        swal({
            title: "Successful",
            type: "success",
            text: SharedService.getFormattedError(text)
        });
    }
    public static notFoudAlert(text: string) {
        swal({
            title: "Not Found",
            type: "warning",
            text: SharedService.getFormattedError(text)
        });
    }
}
