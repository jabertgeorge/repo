package clari5.custom.dev.event.generator;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import com.prowidesoftware.swift.model.SwiftMessage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class EventMessage extends MTEventGenerator {

    static String reqUrl = System.getenv("DN") + "/mq/rest/cxq/load/put?";
    static Hocon mapKey;
    static int count = 0;

    static {
        mapKey = new Hocon();
        mapKey.loadFromContext("mapField.conf");
    }

    public static String splitAmount(String amount) {
        String[] splitamt = amount.split(",");
        String amountInString = splitamt.length > 1 ? splitamt[0] + "." + splitamt[1] : splitamt[0];

        return amountInString;
    }

    public static String getFormattedDate(long dateInMillis) {

        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.sss");
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = df.format(dateInMillis);
        date += " " + LocalTime.now();
        return date;
    }

    public static long getDate(String date) throws ParseException {
        DateFormat formatter;
        formatter = new SimpleDateFormat("yyMMdd");
        Date date1 = formatter.parse(date);
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.format(date1);
        return date1.getTime();
    }

    public static void setAmountToDouble(Map<String, String> map) throws SQLException {

        try {

            String tranCurr = map.get(mapKey.getString("map_field.transactionCurrency"));
            tranCurr = tranCurr.replaceAll("[^A-Z]", "");
            map.put(mapKey.getString("map_field.transactionCurrency"), tranCurr);

            String tranAmount = map.get(mapKey.getString("map_field.transactionAmount"));
            tranAmount = Character.isDigit(tranAmount.charAt(0)) ?
                    tranAmount.replaceAll("[^0-9,]", "").substring(6) :
                    tranAmount.replaceAll("[^0-9,]", "");
            double exchangeRate = getExchangeRate(tranCurr);
            double tran_amt = Double.parseDouble(splitAmount(tranAmount));
            map.put(mapKey.getString("map_field.transactionAmount"), String.format("%.2f", tran_amt * exchangeRate));
            map.put(mapKey.getString("map_field.LcyAmount"), String.format("%.2f", tran_amt * exchangeRate));

            String fcyCurrency = map.get(mapKey.getString("map_field.FcyCurrency"));
            fcyCurrency = fcyCurrency.replaceAll("[^A-Z]", "");
            map.put(mapKey.getString("map_field.FcyCurrency"), fcyCurrency);

            String fcyAmount = map.get(mapKey.getString("map_field.FcyAmount"));
            fcyAmount = Character.isDigit(fcyAmount.charAt(0)) ?
                    fcyAmount.replaceAll("[^0-9,]", "").substring(6) :
                    fcyAmount.replaceAll("[^0-9,]", "");
            map.put(mapKey.getString("map_field.FcyAmount"), splitAmount(fcyAmount));

            if (map.containsKey(mapKey.getString("map_field.RemName"))) {
                String remName = map.get(mapKey.getString("map_field.RemName"));
                remName = remName.replaceAll("[',]", "");
                map.put(mapKey.getString("map_field.RemName"), remName);
            }

            if (map.containsKey(mapKey.getString("map_field.RemAdd1"))) {
                String remAddr1 = map.get(mapKey.getString("map_field.RemAdd1"));
                remAddr1 = remAddr1.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.RemAdd1"), remAddr1);
            }

            if (map.containsKey(mapKey.getString("map_field.RemAdd2"))) {
                String remAddr2 = map.get(mapKey.getString("map_field.RemAdd2"));
                remAddr2 = remAddr2.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.RemAdd2"), remAddr2);
            }

            if (map.containsKey(mapKey.getString("map_field.RemAdd3"))) {
                String remAddr3 = map.get(mapKey.getString("map_field.RemAdd3"));
                remAddr3 = remAddr3.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.RemAdd3"), remAddr3);
            }

            if (map.containsKey(mapKey.getString("map_field.BenName"))) {
                String benName = map.get(mapKey.getString("map_field.BenName"));
                benName = benName.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.BenName"), benName);
            }

            if (map.containsKey(mapKey.getString("map_field.BenAdd1"))) {
                String benAddr1 = map.get(mapKey.getString("map_field.BenAdd1"));
                benAddr1 = benAddr1.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.BenAdd1"), benAddr1);
            }

            if (map.containsKey(mapKey.getString("map_field.BenAdd2"))) {
                String benAddr2 = map.get(mapKey.getString("map_field.BenAdd2"));
                benAddr2 = benAddr2.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.BenAdd2"), benAddr2);
            }

            if (map.containsKey(mapKey.getString("map_field.BenAdd3"))) {
                String benAddr3 = map.get(mapKey.getString("map_field.BenAdd3"));
                benAddr3 = benAddr3.replaceAll("[',\\r\\n]", "");
                map.put(mapKey.getString("map_field.BenAdd3"), benAddr3);
            }

            if (map.containsKey(mapKey.getString("map_field.Port"))) {
                String port = map.get(mapKey.getString("map_field.Port"));
                port = port.replaceAll("[',]", "");
                map.put(mapKey.getString("map_field.Port"), port);
            }
        } catch (NullPointerException npe) {
            System.out.println("data fetched as NULL ...");
        }

        /*Iterator<String> itr = map.keySet().iterator();
        while (itr.hasNext()) {
            String keys = itr.next();
            if (map.containsKey(keys)) {
                if ((keys.endsWith("_AMT") || keys.endsWith("_amt")) && (!keys.startsWith("FCY_") || !keys.startsWith("fcy_"))) {
                    String amount = map.get(keys);
                    String cur = map.get("TRAN_CURR");
                    cur = cur.replaceAll("[^A-Z]", "");
                    map.put("TRAN_CURR", cur);
                    amount = amount.replaceAll("[^0-9,]", "");
                    double exchangeRate = getExchangeRate(cur);
                    double tran_amt = Double.parseDouble(splitAmount(amount));
                    map.put(keys, String.format("%.2f", tran_amt * exchangeRate));
                }
                if ((keys.startsWith("fcy") || keys.startsWith("FCY")) && (keys.endsWith("_amt") || keys.endsWith("AMT") ||
                keys.endsWith("-amt") || keys.endsWith("-AMT"))){
                    String amount = map.get(keys);
                    amount = amount.replaceAll("[^0-9,]", "");
                    map.put(keys, splitAmount(amount));
                }
                if ((keys.startsWith("fcy") || keys.startsWith("FCY")) && (keys.endsWith("_cur") || keys.endsWith("_CUR") ||
                keys.endsWith("-cur") || keys.endsWith("-CUR"))){
                    String cur = map.get(keys);
                    cur = cur.replaceAll("[^A-Z]", "");
                    map.put(keys, cur);
                }
            }
        }*/
    }

    public static double getExchangeRate(String tranCur) {

        //System.out.println("tran currency from swift message is: " + (tranCur != null ? tranCur : ""));

        if (tranCur == null) {
            return 0.0;
        }

        double exchangeRate = 0.0;
        CxConnection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String sql = "select GBBKXR from dbo.EXCHANGE_RATE where GCPET = '" + tranCur + "'";

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                exchangeRate = rs.getDouble("GBBKXR");
            }
            return exchangeRate;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("exchange rate for given curr ;" + tranCur + " is : " + exchangeRate);
        return exchangeRate;
    }

    public static CxConnection getConnection() {
        //Clari5.batchBootstrap("test", "efm-clari5.conf");

        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
    }

    public static void populateHeaderDataAndTranDeatils(Map<String, String> map, SwiftMessage message) {
        if (message.toMT().isInput()) {
            System.out.println("Inside is input tran_ref number " + map.get("TRAN_REF_NO"));
            map.put("REM_TYPE", "O");
            if (map.containsKey("BEN_BIC")) {
                map.put("BEN_BIC", message.getReceiver());
            } else map.put("BEN_BIC", message.getReceiver());

            if (map.containsKey("BEN_CNTRY_CODE")) {
                map.put("BEN_CNTRY_CODE", message.getReceiver()
                        .substring(4, 6));
            } else map.put("BEN_CNTRY_CODE", message.getReceiver()
                    .substring(4, 6));
            if (message.getType().equalsIgnoreCase("103")) {
                //System.out.println("TRAN_REF_NO inside 103 inward--> "+map.get("TRAN_REF_NO"));
                map.put("TRAN_REF_NO", "SWIFT.OR." + (map.get("TRAN_REF_NO") != null ?
                        map.get("TRAN_REF_NO") : ""));
            } //else map.put("TRAN_REF_NO", "SWIFT.OR.");

            if (message.getType().equalsIgnoreCase("110")) {
                //System.out.println("TRAN_REF_NO inside 110 inward--> "+map.get("TRAN_REF_NO"));
                map.put("TRAN_REF_NO", "SWIFT.DRAFT.OUT." + (map.get("TRAN_REF_NO") != null ?
                        map.get("TRAN_REF_NO") : ""));
            } //else map.put("TRAN_REF_NO", "SWIFT.DRAFT.OUT.");

            if (message.getType().equalsIgnoreCase("700")) {
                //System.out.println("TRAN_REF_NO inside 700 inward--> "+map.get("TRAN_REF_NO"));
                map.put("TRAN_REF_NO", "SWIFT.LC." + (map.get("TRAN_REF_NO") != null ?
                        map.get("TRAN_REF_NO") : ""));
            } //else map.put("TRAN_REF_NO", "SWIFT.LC.");
        } else {
            map.put("REM_TYPE", "I");

            if (message.getType().equalsIgnoreCase("110")){
                map.put("REM_CNTRY_CODE", map.containsKey("REM_CNTRY_CODE") ?
                        ((!map.get("REM_CNTRY_CODE").isEmpty() ||
                                map.get("REM_CNTRY_CODE") != null) ?
                map.get("REM_CNTRY_CODE").substring(4,6) : "") : "");
            }
        }
    }

    public String messageMap(Map<String, String> map, SwiftMessage message) throws IOException, ParseException {
        System.out.println("data in map is: " + map + "message type is : " + message.getType());
        System.out.println();
        /*if (message.toMT().isInput()) {
            map.put("REM_TYPE", "I");
        } else map.put("REM_TYPE", "O");*/
        populateHeaderDataAndTranDeatils(map, message);

        if (message.getType().startsWith("7") && map.get("TRAN_REF_NO").startsWith("BT")) {
            System.out.println("ignoring message conversion because TRAN_REF_NO " + map.get("TRAN_REF_NO") + " starts with BT for message type " + message.getType());
            return "Swift message for message type " + message.getType() + " is ignored for " +
                    "Sender's Reference " + map.get("TRAN_REF_NO");
        }
        setDate(map);
        try {
            setAmountToDouble(map);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        CxJson json = CxJson.parse(getEvent().getJSON());
        Iterator<String> itr = map.keySet().iterator();
        json.remove("event_id");
        if (count == 1000) count = 0;
        json.put("event_id", "mt-" + System.nanoTime() + (++count));
        String msgBody = json.getString("msgBody");
        CxJson json1 = CxJson.parse(msgBody);

        json1.remove("tran_id");
        json1.put("tran_id", "SW" + System.nanoTime());

        while (itr.hasNext()) {
            String key = itr.next();
            json1.remove(key);
            if (key.equalsIgnoreCase("REM_ACCT_NO") || key.equalsIgnoreCase("BEN_ACCT_NO")) {
                map.put(key, map.get(key).replaceAll("/", "").trim());
            }
            json1.put(key, null != map.get(key) ? map.get(key) : "");
        }
        json.remove("msgBody");
        String appendjson = json1.toString().replaceAll("\"", "'");
        appendjson = '"' + appendjson + '"';
        String newJson = json.toString();
        String json3 = newJson.substring(0, newJson.length() - 1) + ",\"msgBody\":" + appendjson + "}";
        System.out.println(json3);
        //System.out.println("data in map inside EventMessage is: " + map);
        CxRest.equeue(reqUrl, "HOST", json.get("event_id").toString(), null != json1.get("BEN_ACCT_NO") ? json1.get("BEN_ACCT_NO").toString() : "default123", System.currentTimeMillis(), json3);
        return json3;
    }

    public static void setDate(Map<String, String> map) throws ParseException {
        Iterator<String> itr = map.keySet().iterator();
        while (itr.hasNext()) {
            String keys = itr.next();
            if (map.containsKey(keys)) {
                if (keys.endsWith("_date") || keys.endsWith("_DATE")) {
                    map.put(keys, getFormattedDate(getDate(map.get(keys))));
                }
            }
            if (map.containsKey(keys)) {
                if (keys.endsWith("_time") || keys.endsWith("_TIME")) {
                    map.put(keys, getFormattedDate(getDate(map.get(keys))));
                }
            }
        }
        if (map.containsKey("EVENTTS")) {
            map.put("EVENTTS", getFormattedDate(getDate(map.get("EVENTTS"))));
        }
    }

    @Override
    public Map<String, String> getEventFromMessage(SwiftMessage message) {
        return null;
    }
}
