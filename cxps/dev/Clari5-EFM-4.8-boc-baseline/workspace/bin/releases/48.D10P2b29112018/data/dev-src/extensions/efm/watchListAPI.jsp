<%@page language="java" contentType="application/json"%>
<%@page import="clari5.custom.dev.WatchListFileMatcher" %>
<%@page import="java.net.URLEncoder" %>
<%@page import="org.json.*" %>

<%
String name = request.getParameter("name");
String nationality = request.getParameter("nationality");
String dob = request.getParameter("dob");
String nic = request.getParameter("nic");
String passport = request.getParameter("passport");
String ruleName = "WL_ONBOARDING_CUST";

String url = System.getenv("DN")+"/efm/wlsearchwithdetails";


    JSONObject obj=new JSONObject();
    JSONArray array=new JSONArray();
    obj.put("ruleName",ruleName);
    JSONObject jo1=new JSONObject();
    jo1.put("name","name");
    jo1.put("value",name);
    array.put(jo1);
    JSONObject jo2=new JSONObject();
    jo2.put("name","dob");
    jo2.put("value",dob);
    array.put(jo2);
    JSONObject jo3=new JSONObject();
    jo3.put("name","nationality");
    jo3.put("value",nationality);
    array.put(jo3);
    JSONObject jo4=new JSONObject();
    jo4.put("name","nic");
    jo4.put("value",nic);
    array.put(jo4);
    JSONObject jo5=new JSONObject();
    jo5.put("name","passport");
    jo5.put("value",passport);
    array.put(jo5);

    obj.put("fields",array);

System.out.println("WL_ONBOARDING_CUST : "+url+"?"+obj.toString());

WatchListFileMatcher wat = new WatchListFileMatcher(url,obj);
wat.setDob(dob);
wat.setName(name);
wat.setNationality(nationality);
wat.setPassport(passport);
wat.setNic(nic);

String resp = wat.getMatchScore();
out.println(resp);
%>
