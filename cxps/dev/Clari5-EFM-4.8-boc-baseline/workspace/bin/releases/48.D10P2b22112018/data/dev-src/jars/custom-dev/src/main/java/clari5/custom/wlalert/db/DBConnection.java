package clari5.custom.wlalert.db;


import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.utils.CxpsLogger;
import java.sql.Connection;

public abstract class DBConnection {
    private static final CxpsLogger logger = CxpsLogger.getLogger(DBConnection.class);

    public  RDBMSConnection rdbmsConnection;
    public JDBCConnection jdbcConnection;

    public  RDBMSSession getSession() {
        return (RDBMSSession) rdbmsConnection.getConnection();
    }

    public Connection getJDBCConnection() {
        return (Connection) jdbcConnection.getConnection();
    }

    public  void closeRDMSSession() {
        logger.warn("RDBMS connection is closed");
        rdbmsConnection.closeConnection();
    }

    public void closeJDBCConnection() {
        logger.warn("JDBC connection is closed");
        jdbcConnection.closeConnection();
    }

}

