package clari5.custom.wlalert;

import cxps.apex.utils.CxpsLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip {
    public static CxpsLogger logger = CxpsLogger.getLogger(UnZip.class);

    public void unZipIt(String zipFile, String unzipDir) throws IOException {
        byte[] buffer = new byte[1024];

        File folder = new File(unzipDir);
        if (!folder.exists()) {
            folder.mkdir();
        }


        ZipInputStream zip = new ZipInputStream(new FileInputStream(zipFile));
        FileOutputStream fos = null;
        try {
            ZipEntry ze = zip.getNextEntry();
            while (ze != null) {

                String fileName = ze.getName();

                File newFile = new File(unzipDir + File.separator + fileName);
                logger.info("newFileName " +newFile.getAbsolutePath());
                new File(newFile.getParent()).mkdirs();

                fos = new FileOutputStream(newFile);

                int len;
                while ((len = zip.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                ze = zip.getNextEntry();
            }

            logger.info("Unzipping of file done ");
        } finally {
            fos.close();
            zip.closeEntry();
            zip.close();

        }

    }
}
