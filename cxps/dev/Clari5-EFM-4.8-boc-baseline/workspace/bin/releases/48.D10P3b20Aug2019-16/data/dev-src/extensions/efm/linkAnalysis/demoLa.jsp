<%
	response.setHeader("Pragma","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Expires","0");
	response.setDateHeader("Expires",-1);
%>
<%
	String protocol = "";
	if(request.getRequestURL().toString().trim().startsWith("https"))
	{
		protocol = "https";
	}
	else
	{
		protocol = "http";
	}
	// Pass the converse JSP URL
	String ctx = request.getContextPath();
	String hostport = request.getServerName() + ":" + request.getServerPort();
	String serverUrl = protocol+"://"+hostport+ctx;
	String userId = (String) request.getParameter("userId");
	String acctId = (String) request.getParameter("refId");
%>
<html>
	<head>
		<!--
		<link rel="stylesheet" href="../common/css/jquery/demo_table.css" type="text/css"/>
		<link rel="stylesheet" href="../common/css/jquery/jquery-ui.css" type="text/css"/>
		<link href="../common/css/jquery/validation-engine/validationEngine.jquery.css" rel="stylesheet">
		<link rel="stylesheet" href="../common/css/jquery/overlay-apple.css" type="text/css"/>
		<link href="../common/css/jquery/jquery.toastmessage.css" rel="stylesheet" />
		-->
		<script type="text/javascript" src="../common/js/lib/loadCDN.js"></script>
		<script>
			loadCSS(["ext/css/jquery/demo_table.css", "ext/css/jquery/jquery-ui.css", "ext/css/jquery/validation-engine/validationEngine.jquery.css", "ext/css/jquery/overlay-apple.css", "ext/css/jquery/jquery.toastmessage.css"]);
		</script>

  		<link rel="stylesheet" href="css/slide.css" type="text/css" />
		<link rel="stylesheet" href="../common/css/style.css" type="text/css" />

		<style>
			body {
				overflow: auto;
				overflow-x:auto;
				overflow-y:auto;
			}

			/* context menu styles */
			#contextmenu{
				margin:2px;
				padding:5px;
				border: 2px solid #000;
				background:#191919;
				color:#fff;
			}
			#contextmenu li {
				list-style-type:none;
				margin:1px;
				padding:2px;
				cursor:pointer;
			}
			#contextmenu li.hover{
				background: #ddd;
				color:#191919;
			}
			/* context menu styles end */

			.start-acct-w-24 {
				background: url("../common/images/acct-w-20.png") 3px 5px no-repeat;
				background-color: #003366 !important;
				padding-left: 35px;
				padding-right: 15px;
				margin-right:0;
				-webkit-transition: padding-right 0.7s;
				box-shadow: 0 0 20px #FFF;
			}

			.search-w-16-w-la {
				background: url("../common/images/search-w-16-w.png") no-repeat scroll 5px 1px transparent;
				padding: 1px 5px 16px 16px;
			}

			.acct-w-24-crtd {
				background: url("../common/images/acct-w-20.png") 3px 5px no-repeat;
				padding-left: 24px;
				padding-right: 15px;
				margin-right:0;
				width:100px;
				background-color: #808080 !important;
				border-radius: 2px;
				-webkit-transition: padding-right 0.7s;
				transition: padding-right 0.7s;
			}

			.cacct-w-24-crtd {
				background: url("../common/images/crdt-w-20.png") 3px 5px no-repeat;
				padding-left: 24px;
				padding-right: 15px;
				margin-right:0;
				width:105px;
				background-color: #808080 !important;
				border-radius: 0 13px 13px 0;
				-webkit-transition: padding-right 0.7s;
				transition: padding-right 0.7s;
			}

			.dnf-w-16 {
				background: url("../common/images/end-w-16-b.png") 3px 7px no-repeat;
				background-color:#C0C0C0 !important;
				padding-left: 24px;
				padding-right: 15px;
				margin-right:0;
				width:100px;
				border-radius: 13px 0 0 13px;
				-webkit-transition: padding-right 0.7s;
				transition: padding-right 0.7s;
			}

			.fav-w-16-w {
				background: url("../common/images/fav-w-16-w.png") no-repeat scroll 1px 1px transparent;
				padding: 1px 1px 16px 16px;
			}

			div.vertical-line {
				width: 1px;
				background-color: silver;
				height: 100%;
				border: 2px ridge silver ;
				border-radius: 2px;
				vertical-align:middle;
			}

			hr.vertical
			{
				margin-left:20px;
				width: 0px;
				height: 100%; /* or height in PX */
			}

			.horizontal {
				width: 50px;
				background-color: silver;
				height: 1px;
				
				border: 2px ridge silver ;
				border-radius: 2px;
			}

			.bhorizontal {
				width: 50px;
				background-color: silver;
				height: 1px;
				
				border: 2px ridge silver ;
				border-radius: 2px;
			}

			.ahorizontal {
				width: 50px;
				background-color: silver;
				height: 1px;
				border: 2px ridge silver ;
				border-radius: 2px;
			}

			#canDiv { vertical-align:middle; display:table; height:100%; margin:10px; padding:0; }

			#creditArea { display:table-cell; height:100%; width:220px; vertical-align:middle; background:#F0F0F0; }
			#creditArea table { height:100%; width:100%; border-collapse:collapse; margin:0; padding:0; }
			#creditArea td { padding:5px 0; margin:0; font-size:0.79em; text-align:right; }
			#creditArea div, span, a { vertical-align:middle; }
			#creditArea a { text-decoration:none; }

			#refAcctD { display:table-cell; height:100%; margin:0; padding:0; }

			#debtArea { display:table-cell; height:100%; width:200px; vertical-align:middle; }
			#debtArea table { height:100%; border-collapse:collapse; margin:0; padding:0; }
			#debtArea td { padding:5px 0; margin:0; font-size:0.79em;}
			#debtArea div, span, a { vertical-align:middle; }
			#debtArea a { text-decoration:none; }

			.acct-detail {	display:table-cell; height:100%; width:215px; vertical-align:middle; }
			.acct-detail table { height:100%; border-collapse:collapse; margin:0; padding:0; }
			.acct-detail td { padding:5px 0; margin:0; font-size:0.79em; }
			.acct-detail div, span, a { vertical-align:middle; }
			.acct-detail a { text-decoration:none; }


			.acct-w-24 {			   
			    border-radius: 13px;			
			}


			#toppanel {			    
			    position: relative;
			}
			#panel .content .left {
			    border-left:none;
			}
			.one-third{
				float: left;
    			width: 40%;
			}
			.one-two{
				float: left;
    			width: 50%;
			}

			.vertical-silver-line {
				
				border-left: 2px ridge silver ;
				border-radius: 2px;
				border-width: 5px; 		
				
			}
			/*#debtArea table tr:first-child{
				position: relative;
    			top: -54px;
			}
			#debtArea table tr:last-child{
				position: relative;
			    top: 46px;
			}*/
			#canv{
				height: auto !important;
			}
			#debtArea table tr:not(:first-child):not(:last-child){
				height: auto !important;
			}
			body{
				/*height: auto;*/
				 min-height: 100%;
			}
			
			.detailsInfo span{
				padding: 5px;
				
			}
			.detailsInfo span:first-child {
			    display: inline-block;
			    font-weight: bold;
			    width: 20%;
			}
			.detailsInfo span:last-child {
			    display: inline-block;
			    width: 75%;
			}
		</style>

		<script>
			var jqFile = ["ext/jquery/tools-full/jquery.tools.min.js"];
			loadFromCDN(jqFile, load);
			var jsFiles = ["ext/jquery/jquery-ui.js", "ext/jquery/gettheme.js", "ext/jquery/validation-engine/jquery.validationEngine-en.js", "ext/jquery/validation-engine/jquery.validationEngine.js", "ext/jquery/contextmenu/jquery.contextmenu.js", "ext/jquery/jquery.dataTables.min.js", "ext/jquery/jquery.toastmessage-min.js",];
			function load()
			{
				loadFromCDN(jsFiles, null);
			}
		</script>
		<script type="text/javascript" src="../common/js/lib/utils.js" ></script>
		<script type="text/javascript" src="../common/js/lib/cxNetwork.js" ></script>
		<script type="text/javascript" src="js/laService.js"></script>
		<script>
			var oTable;
			function init()
			{
				var userId = '<%=userId%>';
				var serverUrl = '<%=serverUrl%>';
				setParams(userId, serverUrl);

				$("#usrLI").html("Hi "+userId);
				//document.getElementById("acctTI").value = '<%=acctId%>';

				$("#laForm").validationEngine('attach', {scroll: false});
				$("#canv").css("height", ($(document).height()-40));

				//$("td a[rel]").overlay({effect: 'apple'});

				$('.search-w-16-w-la').css({ "rotate": '45' });

				// Expand Panel
				$("#open").click(function(){
					$("div#panel").slideDown("slow");
	
				});	
				// Collapse Panel
				$("#close").click(function(){
					$("div#panel").slideUp("slow");	
				});		
	
				// Switch buttons from "Search" to "Close Panel" on click
				$("#toggle a").click(function () {
					$("#toggle a").toggle();
				});

				//$(".dp").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});

				$("#startDTI").datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					onClose: function( selectedDate ) {
						$("#endDTI").datepicker("option", "minDate", selectedDate);
					}
				});
				$( "#endDTI" ).datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					onClose: function(selectedDate) {
						$("#startDTI").datepicker("option", "maxDate", selectedDate);
					}
				});
			}

			function showAnch(obj)
			{
				$(obj).find("a.add-w-16-w").show();
			}

			function hideAnch(obj)
			{
				$(obj).find("a.add-w-16-w").hide();
			}

			/*function showDetails(obj, res)
			{
				if ( res != undefined ) {
					var dataList = res;
					var grpdt = new Array();
					if ( dataList.length > 0 ) {
						for ( var j=0; j<dataList.length; j++ ) {
							grpdt[j] = new Array(2);
							if ( dataList[j].amt != undefined && dataList[j].amt != null )
								grpdt[j][0] = dataList[j].amt.toString();
							else
								grpdt[j][0] = "";
							if ( dataList[j].date != undefined && dataList[j].date != null )
								grpdt[j][1] = dataList[j].date.toString();
							else
								grpdt[j][1] = "";
						}
					}
					var oTable = $("#detTable").dataTable({"aaData": grpdt,
						"aoColumns": [
							{ "sTitle": "Amount" },
							{ "sTitle": "Date" }
						]
						,"bProcessing": true
		                		,"bDestroy": true
					});
				}
				$(obj).overlay({effect: 'apple'});
				$(".dataTableCss").css("width", "100%");
			}*/

			

			function searchAcct()
			{
				var isTrue = $("#laForm").validationEngine('validate', {scroll: false});
				if (!isTrue)
					return;

				// close the panel
				$("div#panel").slideUp("slow");
				$("#toggle a").toggle();

				var acctId = document.getElementById("acctTI").value;
				var startD = document.getElementById("startDTI").value;
				var endD = document.getElementById("endDTI").value;
				var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"begin\"}";
				var inputObj = JSON.parse(inObjStr);
				getAcctDetail(inputObj, searchH, true);
			}

			
			var accInformation;  //global variable to access it outside
			/*function loadInfoTest(){
				$.ajax({
					url: "jsonData/newInfo.json",
				    method: "POST",
				    //data: { id : menuId },
				    dataType: "JSON",
				    success:function(res){
				    	accInformation=res; 
				    	console.log(res,"In Success");
				    	searchH(res);
				    	$("div#panel").slideUp("slow");					    	
						$("#toggle a").toggle();
						$("body").css({'height':'auto'});
						//var height = $("#debtArea").height();
						//console.log(height,"height");

						var height = $("#debtArea table tr:first-child").height();
						console.log(height,"height");
		
				    },
				    error:function(){
				    	alert("Inn Error");

				    }
				})
			}*/

			function loadInfoTest(objs){
				///console.log(objs,"objs");
				
				var type = $(objs).parents('.filterInfo').find('select').val();
				var value = $(objs).parents('.filterInfo').find('.one-third').find('input').val();
				
				
				$.ajax({
					//url: "jsonData/newInfo.json",
					url: "LAServlet",
				    method: "POST",
				    data: { 
				    	'type' : type,
				    	'value':value
				    },
				    dataType: "JSON",
				    success:function(res){
				    	accInformation=res; 
				    	console.log(res,"In Success");
				    	searchH(res);
				    	$("div#panel").slideUp("slow");					    	
						$("#toggle a").toggle();
						$("body").css({'height':'auto'});
						//var height = $("#debtArea").height();
						//console.log(height,"height");

						var height = $("#debtArea table tr:first-child").height();
						console.log(height,"height");
		
				    },
				    error:function(){
				    	alert("Inn Error");

				    }
				})
			}

			function showDetails(obj, res)
			{
				//console.log(accInformation,"accInformation");
				//console.log(obj,"In showDetails");
				//console.log(res,"In showDetails");
				if(accInformation != undefined && accInformation != null){
					for (var i=0;i<accInformation.Account.length;i++){
						if(res == accInformation.Account[i].Acc){
							console.log(accInformation.Account[i],"Specific Details");
							var html = '';
							    html += '<div>';
							for(key in accInformation.Account[i]) {
								//console.log(accInformation,"accInformation");
							    //console.log (key, accInformation.Account[i][key]);
							   
							    if(key != "details"){
							        html += '<p>';				   								
								    html += '<span>'+key+':';
								    html += '</span>';
								    html += '<span>'+accInformation.Account[i][key];
								    html +='</span>';    
								    html += '</p>';
							    }
							    else{
							    	var detailsVal = accInformation.Account[i][key];
							    	console.log(detailsVal,"detailsVal");
							    	for(detailsKey in detailsVal[0]) {
							    		html += '<p>';
						    			html += '<span>'+detailsKey+':';
									    html += '</span>';
									    html += '<span>'+detailsVal[0][detailsKey];
									    html +='</span>';
									    html += '</p>';
							    	}

							    }
							   	
						    	
							}
							html += '</div>';
						}
					}
					$(".detailsInfo").html(html);
				}
				/*if ( res != undefined ) {
					var dataList = res;
					var grpdt = new Array();
					if ( dataList.length > 0 ) {
						for ( var j=0; j<dataList.length; j++ ) {
							grpdt[j] = new Array(2);
							if ( dataList[j].amt != undefined && dataList[j].amt != null )
								grpdt[j][0] = dataList[j].amt.toString();
							else
								grpdt[j][0] = "";
							if ( dataList[j].date != undefined && dataList[j].date != null )
								grpdt[j][1] = dataList[j].date.toString();
							else
								grpdt[j][1] = "";
						}
					}
					var oTable = $("#detTable").dataTable({"aaData": grpdt,
						"aoColumns": [
							{ "sTitle": "Amount" },
							{ "sTitle": "Date" }
						]
						,"bProcessing": true
		                		,"bDestroy": true
					});
					
				}*/
				$(obj).overlay({effect: 'apple'});
				$(".dataTableCss").css("width", "100%");
			}

			function searchH(res)
			{
				console.log(res,"Res");
				if ( res == undefined ) {
					$("#canDiv").html("");
					//alert("internal server error");
					displayNotification("Internal server error", true, "error");
					return;
				}

				if ( res.name != undefined && res.name != "" ) {
					// paint credit Area first
					
					var inObj ="";
					

					inObj += "<div id='refAcctD' >";
					inObj += "<div style='vertical-align:middle; height:100%; float:none; display:table;' >";
					inObj +=	"<div style='vertical-align:middle; display:table-cell;' >";
					
					inObj += 	"</div>";
					inObj += 	"<div style='vertical-align:middle; display:table-cell;' >";
					inObj += 		"<a class='abutton start-acct-w-24' style='vertical-align:middle;' >"+res.name+"</a>";
					inObj += 	"</div>";
					inObj += 	"<div style='vertical-align:middle; display:table-cell;' >";
					inObj +=		"<div class='horizontal' ></div>";
					inObj += 	"</div>";
					inObj += "</div>";
					inObj += "</div>";

					//inObj += "<div class='vertical-line' style='display:table-cell;' ></div>";

					//inObj += "<div id='debtArea' >";
				    inObj += "<div id='debtArea' class='vertical-silver-line'>";

				    //inObj += "<div class='vertical-line' style='display:table-cell;' ></div>";

					inObj +=	"<table>";

					if ( res.Account != undefined ) {
						var debJson = res.Account;
						if ( debJson.length > 0 ) {
							var det = "";
							for ( var j=0; j<debJson.length; j++ ) {
								//det = debJson[j].details;
								det = debJson;
								inObj += "<tr>";
								inObj += 	"<td style='text-align:right; width:50px;' ><div class='bhorizontal' ></div></td>";
								inObj += 	"<td style='text-align:left;' ><div style='display:none;' >"+JSON.stringify(det[j].Acc)+"</div><div class='abutton acct-w-24' onmouseover=\"showAnch(this)\" onmouseout=\"hideAnch(this)\" >"+debJson[j].Acc+" <span><a href='#' class='search-w-16-w-la' rel='#detailO' onClick='showDetails(this, "+JSON.stringify(det[j].Acc)+")' title='Details' ><span style='color:#fff;' >.</span></a><a href='#' class='add-w-16-w' title='Expand' style='display:none;' onClick='getNext(this, \""+det[j].Acc+"\", "+JSON.stringify(det[j].Acc)+" )' ></a></span></div></td>";
								inObj += 	"<td style='text-align:right; width:50px;' ><div class='ahorizontal' style='display:none;' ></div></td>";

								inObj += 	"<td style='text-align:left;' ><div class='abutton dnf-w-16' style='vertical-align:middle; cursor:auto; background-color:#c0c0c0; color:#191919; display:none;' >Data not found</div></td>";
								inObj+= "</tr>";
							}
						} else {
							inObj += "<tr>";
							inObj += 	"<td style='text-align:right; width:50px;' ><div class='ahorizontal' ></div></td>";

							inObj += 	"<td style='text-align:left;' ><div class='abutton dnf-w-16' style='vertical-align:middle; cursor:auto; background-color:#c0c0c0; color:#191919;' >Data not found</div></td>";
							inObj+= "</tr>";

						}
					} else {						
						displayNotification("Internal server error", true, "error");
					}
					

					inObj += "</table>"; // debt table closed
					inObj += "</div>"; // debt div closed

					$("#canDiv").html(inObj);
				} else {
					$("#canDiv").html("");
				}
				$("td a[rel]").overlay({effect: 'apple'});
				$(".acct-w-24, .acct-w-24-hvr, .cacct-w-24-crtd").contextmenu({'Switch Account':loadAcct}, 'right', 1000);
			}

			function loadAcct(e,el)
			{
				//alert(" load account ::: "+$(el).clone().children().remove().end().text());
				//alert(" load acct :: "+$(el.parentNode).find("div:nth-child(1)").text());

				var dateNArry = [];
				var dateArry = [];
				var acctId = $(el).clone().children().remove().end().text();
				var dJson = JSON.parse($(el.parentNode).find("div:nth-child(1)").text());
				if ( dJson.length > 0 ) {
					var d=0;
					var dat = 0;
					for ( var i=0; i<dJson.length; i++ ) {
						var dtLAry = dJson[i].date.split(" ");
						if ( dtLAry.length > 1 ) {
							dat = dtLAry[0];
							d = Date.parse(dat);
						} else {
							dat = dJson[i].date;
							d = Date.parse(dat);
						}
						dateArry.push(dat);
						dateNArry.push(d);
					}
				}
				var fromDate = Math.min.apply(Math, dateNArry);
				var fromDI = dateNArry.indexOf(fromDate);
				var toDate = Math.max.apply(Math, dateNArry);
				var toDateI = dateNArry.indexOf(toDate);

				var startD = dateArry[fromDI];
				var endD = dateArry[toDateI];

				var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"begin\"}";
				var inputObj = JSON.parse(inObjStr);
				getAcctDetail(inputObj, searchH, true);
			}

			var detDivObj = null;
			function getNext(tObj, acctId, dJson)
			{
				var dateNArry = [];
				var dateArry = [];
				if ( dJson.length > 0 ) {
					var d=0;
					var dat = 0;
					for ( var i=0; i<dJson.length; i++ ) {
						var dtLAry = dJson[i].date.split(" ");
						if ( dtLAry.length > 1 ) {
							dat = dtLAry[0];
							d = Date.parse(dat);
						} else {
							dat = dJson[i].date;
							d = Date.parse(dat);
						}
						dateArry.push(dat);
						dateNArry.push(d);
					}
				}
				var fromDate = Math.min.apply(Math, dateNArry);
				var fromDI = dateNArry.indexOf(fromDate);
				var toDate = Math.max.apply(Math, dateNArry);
				var toDateI = dateNArry.indexOf(toDate);

				this.detDivObj = tObj;
				var startD = dateArry[fromDI];
				var endD = dateArry[toDateI];
				var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"XYZ\"}";
				var inputObj = JSON.parse(inObjStr);
				getNextLevel(inputObj, nextH, true);
			}

			function nextH(resp)
			{
				if ( resp == undefined ) {
					return;
				}
				if ( resp.credit == undefined ) {
					return;
				}

				var res = resp.credit;
				var twoLevel = this.detDivObj.parentNode.parentNode;
				var fourLevel = twoLevel.parentNode.parentNode;
				var sixLevel = fourLevel.parentNode.parentNode;
				$(sixLevel).find(".acct-w-24-hvr").removeClass("acct-w-24-hvr").addClass("acct-w-24");

				$(fourLevel.parentNode).find(".dnf-w-16").hide();

				$(twoLevel).removeClass("acct-w-24").addClass("acct-w-24-hvr");
				$(fourLevel.parentNode).find(".ahorizontal").hide();
				$(fourLevel).find(".ahorizontal").show();
				
				if (res.length > 0) {
					var det = "";
					var nLvl = "<div class='vertical-line' style='display:table-cell; height:100%; vertical-align:middle;' ></div>";
					nLvl += "<div class='acct-detail' >";
					nLvl += 	"<table>";
						for ( var i=0; i<res.length; i++ ) {
							det = res[i].details;
							nLvl += "<tr>";
							nLvl += 	"<td style='text-align:right; width:50px;' ><div class='bhorizontal' ></div></td><td style='text-align:left;' ><div style='display:none;' >"+JSON.stringify(det)+"</div><div class='abutton acct-w-24' style='vertical-align:middle;' onmouseover=\"showAnch(this)\" onmouseout=\"hideAnch(this)\" >"+res[i].acctId+" <span style='vertical-align:middle;' ><a href='#' class='search-w-16-w-la' rel='#detailO' onClick='showDetails(this, "+JSON.stringify(det)+")' title='Details' ><span style='color:#fff;' >.</span></a><a href='#' class='add-w-16-w' title='Expand' style='display:none;' onClick='getNext(this, \""+res[i].acctId+"\", "+JSON.stringify(det)+")' ></a></span></div></td><td style='text-align:right; width:50px;' ><div class='ahorizontal' style='display:none;' ></div></td>";
							nLvl += 	"<td style='text-align:left;' ><div class='abutton dnf-w-16' style='cursor:auto; background-color:#c0c0c0; color:#191919; display:none;' >Data not found</div></td>";
							nLvl += "</tr>";
						}
					nLvl += 	"</table>";
					nLvl += "</div>";
					nLvl += "</div>";
				} else {
					$(this.detDivObj.parentNode.parentNode.parentNode.parentNode).find(".dnf-w-16").show();
					$(this.detDivObj.parentNode).next().show();
				}

				$(sixLevel.parentNode).nextAll().remove();
				$(sixLevel.parentNode.parentNode).append(nLvl);
				//$("td a[rel]").overlay({effect: 'apple'});
				$(sixLevel.parentNode).nextAll().find("td a[rel]").overlay({effect: 'apple'});
				$(".acct-w-24, .acct-w-24-hvr, .cacct-w-24-crtd").contextmenu({'Switch Account':loadAcct}, 'right', 1000);
			}

			function displayNotification(text, ifSticky, type)
			{
				var toastMessageSettings = {
					text: text,
					sticky: ifSticky,
					position: 'middle-center',
					type: type,
					closeText: ''
				};
				$().toastmessage('showToast', toastMessageSettings);
			}
		</script>
	</head>

	<body style="vertical-align:middle;" onload="init()" >
		<div id="toppanel">
			<div id="panel" style="height:140px;" >
				<div class="content clearfix">
					<!--<div class="left" style="width:600px;" >-->
						<div class="noLeft"  >
						<form id='laForm' style='padding:0 !important; margin-top:20px;' ><fieldset style='border:0px;' >
						<!--<table border="0" style="width:100%;" >
							<tr>
							<td style="vertical-align:middle; font-size:0.79em; color:#fff; padding:3px 0;" >Account ID</td>
							<td>:</td>
							<td style="vertical-align:middle; font-size:0.79em; padding:3px 0;" ><input type="text" id="acctTI" class='validate[required]' ></input><img style="vertical-align:middle;" src="../common/images/favs.png" title="Mandatory field" ></img></td>
							<td></td><td></td><td></td>
							</tr>
							<tr>
							<td style="vertical-align:middle; font-size:0.79em; color:#fff; padding:3px 0;" >Start Date</td>
							<td>:</td>
							<td style="vertical-align:middle; font-size:0.79em; padding:3px 0;" ><input type="text" id="startDTI" name="startDTI" class="validate[required]" placeholder='YYYY-MM-DD' ></input><img style="vertical-align:middle;" src="../common/images/favs.png" title="Mandatory field" ></img></td>
							<td style="vertical-align:middle; font-size:0.79em; color:#fff; padding:3px 0;" >End Date</td>
							<td>:</td>
							<td style="vertical-align:middle; font-size:0.79em; padding:3px 0;" ><input type="text" id="endDTI" name="endDTI" class="validate[required]" placeholder='YYYY-MM-DD' /><img style="vertical-align:middle;" src="../common/images/favs.png" title="Mandatory field" ></img></td>
							</tr>

							
						</table>-->

						<div class="filterInfo">
							<div class="one-two">
								<span>Please choose option for search</span>
								<select class="filter">
									<option value="select">
										--Select--
									</option>
									<option value="pan">
										PAN
									</option>
									<option value="mob">
										Mobile No.
									</option>
									<option value="email">
										Email
									</option>
								</select>
							</div>
							<div class="one-third">
								<span>Fill the value</span>
								<input type="text" value=""/>
								<!--<button onClick="loadInfoTest();">Submit
								</button>-->
							</div>
							<div>
								<button onClick="loadInfoTest(this);">Submit
								</button>
							</div>
							
						</div>
						<!--<a class="abutton search-w-def" onClick="searchAcct();" style='margin-left:3px;'>Search</a>-->
						</fieldset></form>
					</div>
				</div>
			</div>
			<div class="tab">
				<ul class="login">
					<li class="left">&nbsp;</li>
					<!-- <li><a onclick="backAnanlysis()" class="abutton back-w-def" >Back</a></li> -->
					<li id="usrLI" >Hello </li>
					<li class="sep">|</li>
					<li id="toggle">
						<a id="open" class="search-w-16-w-la" href="#" style="padding-left:30px; width:100px;" >Search</a>
						<a id="close" style="display: none;" class="bclose" href="#">Close </a>			
					</li>
					<li class="right">&nbsp;</li>
				</ul> 
			</div>
	
		</div>
		<center>
		<div id="canv" style="margin:8px 0 0 0; padding:0;" > 
		<div id="canDiv" >
		</div>
		<div style="margin-top:50px"></div>
		</div>
	</center>


		<!--<div class="apple_overlay black" id="detailO" style="height:400px;" >-->
			<div class="apple_overlay black" id="detailO" style="min-height:200px" >
			<div style="height:360px; overflow:auto;" >
				<div style="border:1px solid #808080; border-radius:0 10px 7px 7px; width:98%; margin:auto; margin-top:10px;" >
					<div class="heading" ><h3>Details </h3></div>
					<!--<div style="width:100%; margin:auto; margin-top:10px; margin-bottom:5px; height:300px;" >-->
						<div style="width:100%; margin:auto; margin-top:10px; margin-bottom:5px; min-height:300px;" >
						<div class="detailsInfo"></div>
						<table id="detTable" class="dataTableCss" style="text-align:left;" ></table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
