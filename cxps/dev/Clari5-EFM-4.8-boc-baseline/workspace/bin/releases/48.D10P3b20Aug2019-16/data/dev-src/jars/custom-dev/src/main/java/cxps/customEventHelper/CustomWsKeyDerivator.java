package cxps.customEventHelper;
public class CustomWsKeyDerivator {
    protected static final String OUTWARDPAYMENT = "O";

    public static String accountKeyForRemmitance(String remType, String remAcctNo, String benAcctNo) {
        if (remType.equalsIgnoreCase(OUTWARDPAYMENT)) {
            return remAcctNo;
        } else {
            return benAcctNo;
        }

    }
}
