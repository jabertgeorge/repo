package clari5.customAml.web;

import clari5.hfdb.ReportAckCount;
import clari5.hfdb.mappers.StrDetailsCustomMapper;
import clari5.platform.applayer.Clari5;
import clari5.platform.util.Hocon;
import clari5.utils.upload.FileUploader;
import clari5.utils.upload.IFileUpload;
import clari5.utils.upload.RequestParser;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import clari5.customAml.web.*;
import cxps.apex.utils.CxpsLogger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.ArrayList;



public class CustomAceServlet extends HttpServlet {

    CxpsLogger logger = CxpsLogger.getLogger(CustomAceServlet.class);
    private String responseStr=null;
    String custId = null;

    @Override
    public void init(ServletConfig config){
        try {
            String appName = config.getServletContext().getContextPath().substring(1);
            Clari5.bootstrap(appName, appName.toLowerCase() + "-clari5.conf");
        }
        catch(Exception ex){
            ex.printStackTrace();
            logger.error("Unable to bootstrap", ex);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    public boolean doEither(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.debug("CustomAceServlet Called");
        RequestParser requestParser = new RequestParser(request);
        try{
            requestParser.parse();
        }catch(Exception e){
            return error(response, "{ \"error\":\"Error in parsing request\" }");
        }

        String userId = request.getParameter("uId");
        String channel = request.getParameter("chId");
        String menuStr = request.getParameter("menu");
        String cmd = request.getParameter("cmd");
        String payload = request.getParameter("payload");
        logger.info("REQUEST RECD:userId["+userId+"],channel["+channel+"],menu:["+menuStr+"],cmd["+cmd+"],payload["+payload+"]");

        // =================================
        // USER ID and CHANNEL Basic check
        // =================================

        if(userId == null || "".equals(userId)){
            return error(response, "{ \"error\":\"No userId Found\" }");
        }
        if(channel == null || "".equals(channel)){
            return error(response, "{ \"error\":\"No channel Found\" }");
        }
        if(menuStr == null || "".equals(menuStr)){
            return error(response, "{ \"error\":\"No menu Found\" }");
        }

        // =================
        // Token Validation
        // =================

        String token = (String)request.getAttribute("token");
        if(!"dummy".equals(token)){
            //token = request.getParameter(TOKEN.val);
            //if(token == null){
            logger.debug("error:No token found");
            //return error(response, "{ \"error\":\"No token found\" }");
            //}
        }

        if(cmd == null || "".equals(cmd)){
            return error(response, "{ \"error\":\"No command found\" }");
        }

        if(payload == null || "".equals(payload)){
            return error(response, "{ \"error\":\""+cmd+" requires input and passed null\" }");
        }

        try {
		LoadCmd(cmd,payload);
        }
        catch(Exception ex){
            logger.error("Invalid payload:["+payload+"]");
            return error(response, "{ \"error\":\"Unable to get NIC or Customer details\" }");
        }

        if(responseStr == null){
            return error(response, "{ \"error\":\""+cmd+" returned error\" }");
        }
        return writeOut(response, responseStr);



    }

	/*
	 * Function to load command
	 */
	protected String LoadCmd(String cmd,String payload){
		logger.debug("in LoadCmd: "+cmd);
		StringBuilder sb=new StringBuilder();
		String pvalue="",xvalue="",cvalue="",bvalue="",fvalue="",lvalue="";
		String query="";
		switch(cmd){
            case "get_nic_details":
                responseStr=getNicDetails(payload);
                break;
            case "get_customer_details":
                responseStr=getCustDetails(payload);
                break;
            case "get_cust_tree":
                System.out.println("LoadCmd:get_cust_tree");
                GetCustomCustTree ct= new GetCustomCustTree(payload);
                responseStr=ct.getCustTree();
                break;
            case "get_emp_tree":
                System.out.println("LoadCmd:get_emp_tree");
                GetCustomEmpTree cte= new GetCustomEmpTree(payload);
                responseStr=cte.getCustEmpTree();
                break;
            case "get_trade_details":
                GetTradeDetail trade=new GetTradeDetail(payload);
                responseStr=trade.getTradeDetails();
                break;
            case "get_locker_details":
                GetLockerDetails locker= new GetLockerDetails(payload);
                responseStr=locker.getLockerDetails(custId);
                break;
            case "get_lease_details":
                GetLeaseDetails lease= new GetLeaseDetails(payload);
                responseStr=lease.getLeaseDetails();
            break;
            case "get_deal_details":
                GetDealDetails deal= new GetDealDetails(payload);
                responseStr=deal.getDealDetails();
                break;
            case "get_bill_details":
                GetBillDetails bill= new GetBillDetails(payload);
                responseStr=bill.getBillDetails();
                break;
            case "get_pawn_details":
                GetPawnDetails pawn= new GetPawnDetails(payload);
                responseStr=pawn.getPawnDetails();
                break;
    		default:
			break;
		}
		return responseStr;
	}

    /*
     * Function to get NIC customer details.
     */
    protected String getCustDetails(String payload){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        JSONObject jsonObject = new JSONObject(payload);
        custId = jsonObject.getString("custId");
        /*if( custId!= null && !custId.startsWith("C_")){
            custId="C_F_"+custId;
        }*/
        Connection conn = null;
        Statement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select name,constitution,gender,pan,uin,mobile1,email,mailing_addr,cfd_turn_over,dob,entity_date_of_incorporation," +
                    "nature_of_business,segment,fax,monthly_income,cust_type,cust_id,host_id " +
                    "from AML_CUSTOMER_VIEW where cust_id='"+custId+"'";
            logger.debug("query to get CustDetails : "+sql);
            ResultSet rs = stmt.executeQuery(sql);
            logger.debug("after exec : "+rs);

            while( rs.next() ){
                obj.put("name", rs.getString("name")!= null ? rs.getString("name") : "");
                obj.put("constitution", rs.getString("constitution")!= null ? rs.getString("constitution") : "");
                obj.put("gender", rs.getString("gender")!= null ? rs.getString("gender") : "");
                obj.put("pan",rs.getString("pan")!= null ? rs.getString("pan") : "");
                obj.put("uin", rs.getString("uin")!= null ? rs.getString("uin") : "");
                obj.put("mobile1", rs.getString("mobile1")!= null ? rs.getString("mobile1") : "");
                obj.put("email", rs.getString("email")!= null ? rs.getString("email") : "");
                obj.put("mailingAddr", rs.getString("mailing_addr")!= null ? rs.getString("mailing_addr") : "");
                obj.put("annualTurnOver", rs.getString("cfd_turn_over")!= null ? rs.getString("cfd_turn_over") : "");
                obj.put("dob", rs.getString("dob")!= null ? rs.getString("dob") : "");
                obj.put("dateOfIncorp", rs.getString("entity_date_of_incorporation")!= null ? rs.getString("entity_date_of_incorporation") : "");
                obj.put("natOfBusn", rs.getString("nature_of_business")!= null ? rs.getString("nature_of_business") : "");
                obj.put("custSeg", rs.getString("segment")!= null ? rs.getString("segment") : "");
                obj.put("fax", rs.getString("fax")!= null ? rs.getString("fax") : "");
                obj.put("monthlyincome", rs.getString("monthly_income")!= null ? rs.getString("monthly_income") : "");
                obj.put("PersonalNotPersonal", rs.getString("cust_type")!= null ? rs.getString("cust_type") : "");
                obj.put("custid", rs.getString("cust_id")!= null ? rs.getString("cust_id") : "");
                obj.put("hostid", rs.getString("host_id")!= null ? rs.getString("host_id") : "");
                obj.put("noOfStr", getStrDetails(custId).getString("noOfStr"));
                obj.put("lastFiledStr", getStrDetails(custId).getString("lastFiledStr"));
                jarray.put(obj);
            }
            obj1.put("customerDetails", jarray);
            logger.debug("getCustDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting cust details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";
    }

    /*
     * Function to get NIC data from respective table.
     */
    protected String getNicData(String query){
        String value="";
        Connection conn = null;
        Statement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            logger.debug("Nic query: "+query);
            ResultSet rs = stmt.executeQuery(query);
            while( rs.next() ){
                logger.debug("nic details: "+rs.getString(1));
                value=rs.getString(1);
            }
            return value;
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return value;
    }

    /*
     * Function to get NIC details.
     */
    protected String getNicDetails(String payload){
        JSONObject jsonObject = new JSONObject(payload);
        String nicID = jsonObject.getString("nicID");
        String nicDetails=getNicFromDb(nicID);
        return nicDetails;
    }

    /*
     *
     */
    protected String getNicFromDb(String nicID){
        Connection conn = null;
        Statement stmt = null;
        StringBuilder sb=new StringBuilder("{");
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String  sql="select cxCifID from customer where nationalId='"+nicID+"'";
            logger.debug("query to get nic customers: "+sql);
            ResultSet rs = stmt.executeQuery(sql);
            sb.append("\"customerIds\"").append(":[");
            int i=0;
            while( rs.next() ){
                if(i!=0){
                    sb.append(",");
                }
                logger.debug("nic details: "+rs.getString("cxCifID"));
                sb.append("\""+rs.getString("cxCifID")+"\"");
                i++;
            }
            sb.append("]}");
            logger.debug("conn: "+conn+" stmt: "+stmt+"response: "+sb.toString());
            return sb.toString();
        }catch(Exception e){
            logger.debug("exception in getting nic details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";

    }



    protected boolean writeOut(HttpServletResponse response, String message) throws IOException {
        logger.debug("Writing out["+message+"]");
        try {
            Writer writer = response.getWriter();
            if(writer != null){
                writer.write(message);
            }
            else
                logger.error("Found null http writer");
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return false;
    }

    protected boolean error(HttpServletResponse response, String message) throws IOException {
        logger.error("Error: Processing Request ["+message+"]");
        return writeOut(response, message);
    }

    public static JSONObject getStrDetails(String custId){
        if (custId == null) return null;

        JSONObject jsonObject = new JSONObject();
        RDBMSSession session = Rdbms.getAppSession();
        StrDetailsCustomMapper mapper = session.getMapper(StrDetailsCustomMapper.class);
        ReportAckCount strRptData = new ReportAckCount();
        strRptData.setEntityId(custId);
        strRptData.setReportType("STR");
        ReportAckCount strDetails = null;
        strDetails = mapper.getStrDetails(strRptData);
        jsonObject.put("noOfStr", strDetails.getAckCount() != null ? strDetails.getAckCount().toString() : "NA");
        jsonObject.put("lastFiledStr", strDetails.getUpdatedOn() != null ? strDetails.getUpdatedOn().toString() : "NA");
        return jsonObject;
    }

}
