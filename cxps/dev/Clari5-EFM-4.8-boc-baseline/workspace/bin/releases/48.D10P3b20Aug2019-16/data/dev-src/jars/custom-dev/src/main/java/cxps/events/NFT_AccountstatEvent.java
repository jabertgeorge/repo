// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCOUNTSTAT", Schema="rice")
public class NFT_AccountstatEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String entityType;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String keys;
       @Field(size=20) public String channel;
       @Field(size=20) public String custRefId;
       @Field(size=20) public String source;
       @Field public java.sql.Timestamp eventTime;
       @Field(size=20) public String cxAcctId;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String eventSubtype;
       @Field(size=20) public String sourceOrDestAcctId;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=20) public String custId;
       @Field(size=20) public String acctName;
       @Field(size=20) public String initAcctStatus;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String finalAcctStatus;
       @Field(size=20) public String status;
       @Field(size=20) public String systemCountry;


    @JsonIgnore
    public ITable<NFT_AccountstatEvent> t = AEF.getITable(this);

	public NFT_AccountstatEvent(){}

    public NFT_AccountstatEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Accountstat");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setEntityType(json.getString("entitytype"));
            setMsgSource(json.getString("msgsource"));
            setKeys(json.getString("keys"));
            setChannel(json.getString("channel"));
            setCustRefId(json.getString("acid"));
            setSource(json.getString("source"));
            setEventTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCxAcctId(json.getString("cx-acct-id"));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setAddEntityId2(json.getString("reservedfield2"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setAddEntityId3(json.getString("reservedfield3"));
            setEventSubtype(json.getString("eventsubtype"));
            setSourceOrDestAcctId(json.getString("account-id"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setCustId(json.getString("custid"));
            setAcctName(json.getString("acct-name"));
            setInitAcctStatus(json.getString("init-acct-status"));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setFinalAcctStatus(json.getString("final-acct-status"));
            setStatus(json.getString("status"));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NFT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getEntityType(){ return entityType; }

    public String getMsgSource(){ return msgSource; }

    public String getKeys(){ return keys; }

    public String getChannel(){ return channel; }

    public String getCustRefId(){ return custRefId; }

    public String getSource(){ return source; }

    public java.sql.Timestamp getEventTime(){ return eventTime; }

    public String getCxAcctId(){ return cxAcctId; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getSourceOrDestAcctId(){ return sourceOrDestAcctId; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getCustId(){ return custId; }

    public String getAcctName(){ return acctName; }

    public String getInitAcctStatus(){ return initAcctStatus; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getFinalAcctStatus(){ return finalAcctStatus; }

    public String getStatus(){ return status; }

    public String getSystemCountry(){ return systemCountry; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setEntityType(String val){ this.entityType = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setCustRefId(String val){ this.custRefId = val; }
    public void setSource(String val){ this.source = val; }
    public void setEventTime(java.sql.Timestamp val){ this.eventTime = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setSourceOrDestAcctId(String val){ this.sourceOrDestAcctId = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setInitAcctStatus(String val){ this.initAcctStatus = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setFinalAcctStatus(String val){ this.finalAcctStatus = val; }
    public void setStatus(String val){ this.status = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_AccountstatEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.custRefId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Accountstat");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Accountstat");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}