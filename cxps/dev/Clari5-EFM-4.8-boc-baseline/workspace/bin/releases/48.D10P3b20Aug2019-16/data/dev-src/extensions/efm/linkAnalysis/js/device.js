/**
 * Created by gaurav on 4/5/15.
 */
/* Device.js start */
var oTable;
var dvcSrch="";
var dvcOutput="";
var customerTab="";
var accountTab="";

dvcSrch="<div class='heading' ><h3>Device Search</h3></div>";

dvcSrch+="<form id='dvcTemplate'><table id='dvcListD' cellspacing='2' cellpadding='2'>"
    +"<tr class='separator'><td class='fontSize'>Device ID</td></tr>"
    +"<tr class='separator'><td class='fontSize'><input type='text' value='' class='validate[groupRequiredHighlight]' id='dvcNum' /></td></tr>"
    +"<tr class='separator'><td class='fontSize'>Device Name</td></tr>"
    +"<tr class='separator'><td class='fontSize'><input type='text' value='' class='' id='dvcName' /></td></tr>"
    +"<tr class='separator'><td class='fontSize'><a class='abutton search-w-def' onClick='loadDvcData(this)' style='margin-left:50px;'>Search</a></td></tr></table></form>";

dvcOutput="<div id='dvcSrchLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\"><h3>Device Details</h3></div><div id='tabDvc' style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=dvcSrchDtls' class='dataTableCss' style='text-align:left;overflow:auto;' ></table></div></div>";

var acctTreeData="";

function initDvc()
{
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#dvcListD").show();
    $("#dvcSrchLink").hide();
    document.onkeypress = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closeAcctEvidence(acctCount);
        }
    };
}

var treeObj="";
var dvcName=null;
var schType=null;
var brchId=null;
var dvcNum=null;


function loadDvcData(dvcId)
{
    dvcId=($("#dvcNum").val()!="")?$("#dvcNum").val().trim():null;
    dvcName=($("#dvcName").val()!="")?$("#dvcName").val().trim():null;
    //taxId=($("#taxId").val()!="")?$("#taxId").val().trim():null;
    //emailId=($("#emailId").val()!="")?$("#emailId").val().trim():null;
    //phnNum=($("#phnNum").val()!="")?$("#phnNum").val().trim():null;
    if(srchValidation()){
        var dvcObject={
            //"mrchntId": mrchntId,
            "custId": dvcId,
            // "pan": taxId,
            // "email": emailId,
            "dvcName":dvcName
            // "mobile1": phnNum
        };
        //getCustDtlsOnValidate(custObject);

        var ifFormFilled = false;
        $("#analDiv").hide();
        if ( dvcId != null)
            ifFormFilled = true;
        /*if ( taxId != null)
         ifFormFilled = true;
         if ( emailId != null)
         ifFormFilled = true;*/
        if ( dvcName != null)
            ifFormFilled = true;
        if (ifFormFilled) {
            $("#dvcNum").removeClass("validate[groupRequiredHighlight]");
            $("#dvcNum").addClass("validate[custom[oneSpcLetterNumber]]");
            $("#dvcName").addClass("validate[custom[onlyLetterSp,minSize[3]]]");
            //$("#taxId").addClass("validate[custom[onlyLetterNumber]]");
            //$("#emailId").addClass("validate[custom[email]]");
            var isCond = $("#dvcTemplate").validationEngine('validate', {scroll: false});
            if(isCond){
                //getCustomerDetails(mrchntObject,mrchntRespHandler,true); //responsible for server call
                //for getting dummy JSON data
                getDvcJsonData(dvcObject,dvcRespHandler,true);


                $.getScript("js/MD5.js", function(){
                    //alert("Script MD5 loaded and executed.");
                    $.getScript("js/dvcParam.js", function(){

                        //alert("Script loaded and executed.");

                        // Use anything defined in the loaded script...
                        getSoftwareParams();
                    });
                });


            }
        } else {
            $("#dvcNum").removeClass("validate[custom[oneSpcLetterNumber]]");
            //$("#custmrchnttaxId").removeClass("validate[custom[onlyLetterNumber]]");
            //$("#emailId").removeClass("validate[custom[email]]");
            $("#dvcNum").addClass("validate[groupRequiredHighlight]");
            var isTrue = $("#dvcTemplate").validationEngine('validate', {scroll: false});
        }

    }
}

function getDvcDtlsOnValidate(mrchntObject)
{
    var ifFormFilled = false;
    if ( dvcName != null)
        ifFormFilled = true;
    if ( dvcNum != null)
        ifFormFilled = true;
    if (ifFormFilled) {
        $("#dvcName").addClass("validate[custom[oneSpcLetterNumber]]");
        $("#dvcNum").addClass("validate[custom[oneSpcLetterNumber]]");
        var isAcctCond = $("#dvcTemplate").validationEngine('validate', {scroll: false});
        if(isAcctCond){
            getAccountDetails(dvcObject,dvcRespHandler,true);
        }
    } else {
        $("#dvcName").removeClass("validate[custom[oneSpcLetterNumber]]");
        $("#dvcNum").removeClass("validate[custom[oneSpcLetterNumber]]");
        $("#dvcName").addClass("validate[groupRequiredHighlight]");
        $("#dvcTemplate").validationEngine('validate', {scroll: false});
    }
}

function acctValidation(){
    if(dvcName==null  && dvcNum==null){
        $("#dvcName").addClass("hightlightDiv");
        $("#dvcNum").addClass("hightlightDiv");
        return true;
    }
    else{
        $("#dvcName").removeClass("hightlightDiv");
        $("#dvcNum").removeClass("hightlightDiv");
        return true;
    }
}

function dvcRespHandler(resp)
{
    var response=resp.custAttrList;
    console.log(response,"response with custATTrList");
    if(response=="" || response==undefined){
        displayNotification("Merchant ID doesn't exist.", true, "warning");
    }else{
        clearTabs();
        //addTab('Customer','');
        addTab('Device',''); //Added for device
        $("#dvcSrchLink").show();
        //var dataObj=makeJsonForDatatable(response, "addTab","mdvcNum",reqstPage); //Added for Device
        //var dataObj=makeJsonForDatatable(response, "addTab","dvcNum",reqstPage); //Added for Device
        var html = "";
        if(response != undefined && response != null){
           // $("#tabDvc").append("Hi");

            for(var j = 0; j < response.length; j++){
                //for(var k; k<response[j].hardwareparams.length; k++){
                    if(response[j].hardwareparams != undefined){
                        for(key in response[0].hardwareparams){
                            html += "<p style='padding-left:20px;'><label style='width:120px;'>";
                            html += key+"</label>:";
                            html += "<label >";
                            html += response[0].hardwareparams[key]+"</label></p>";
                        }
                    }
                    if(response[j].softwareparams != undefined){
                        for(key in response[0].softwareparams){
                            if(typeof response[0].softwareparams[key] != "object"){
                                html += "<p style='padding-left:20px;'><label style='width:120px;'>";
                                html += key+"</label>:";
                                html += "<label>";
                                html += response[0].softwareparams[key]+"</label></p>";
                            }
                            else{
                                for(keys in response[0].softwareparams[key]){
                                    html += "<p style='padding-left:20px;'><label style='width:120px;'>";
                                    html += keys+"</label>:";
                                    html += "<label>";
                                    html += response[0].softwareparams[key][keys]+"</label></p>";
                                }
                            }

                        }
                    }
               // }
            }
            $("#tabDvc").append(html);
        }
        //var dataObj=makeJsonForDatatable(response, "addTab","custId",reqstPage);
        //displayData(dataObj,"tabCust","custSrchDtls");
       // displayData(dataObj,"tabDvc","dvcSrchDtls");  //Added for Device
        toggleSrchWindow();
    }
}


var acctCount="";
var inputObj="";
var inputObject="";
var accountID="";
function acctLoad(mrchntId,tabCount){
    merchantID=mrchntId;
    $("#menuBlock").hide();
    acctCount=tabCount;
    inputObject={
        "mrchntId":mrchntId
    };
    if($("#refId").val()!=""){
    }
    getAccountHardFacts(inputObject,acctHFactsRespHandler,true);
    getDvcJsonData(inputObject,callback,true);
}

function requiredOneOfGroup(field, rules, i, options){
    var fieldsWithValue = $('input[type=text]').filter(function(){
        if($(this).val().length>0){
            return true;
        }
    });
    if(fieldsWithValue.length<1){
        return "At least one field in this set must have a value.";
    }
}

var custId=null;
var custName=null;
var taxId=null;
var passNum=null;
var emailId=null;
var adhNum=null;
var pinCode=null;
var phnNum=null;



/* account.js end */

/*test code for dummy*/
function getDvcJsonData(inputObject,callback){
    //alert("in Device json load Data");
    // window.open("strFinal.jsp","_self");
    $.ajax({
        //url: "http://cxpsadm-vostro-1014:5000/AML/ReportJsonServlet?reportId=1034",
        //url: "http://cxpsadm-vostro-1014:5000/AML/ReportJsonServlet?reportId="+xReportIdVal,
        url: "../io/jsonData/device.json",
        cache: false,
        method: "get",
        //resultType: "json",
        success: function(result){
            console.log(result,"result");
            var x = JSON.parse(result);
            callback(x);
        },
        error : function(a){
            alert("In error");
        }
    });
}

function displayDvcdetailData(inputObject,callback){
   // alert("in Device json load Data");
    // window.open("strFinal.jsp","_self");
    $.ajax({
        //url: "http://cxpsadm-vostro-1014:5000/AML/ReportJsonServlet?reportId=1034",
        //url: "http://cxpsadm-vostro-1014:5000/AML/ReportJsonServlet?reportId="+xReportIdVal,
        url: "../io/jsonData/device.json",
        cache: false,
        method: "get",
        //resultType: "json",
        success: function(result){
            console.log(result,"result");
            var x = JSON.parse(result);
            callback(x);
        },
        error : function(a){
            alert("In error");
        }
    });
}


