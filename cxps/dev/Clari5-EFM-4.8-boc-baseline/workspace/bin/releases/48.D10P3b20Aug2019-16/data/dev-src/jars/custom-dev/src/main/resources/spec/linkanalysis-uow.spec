clari5.customAml.web.linkanalysis {

	record {

        link-txn {
            attributes = [
                { name : amt, type: "decimal:20,2"}
                { name : tranId, type: "string:50"}
                { name : date, type: DATE}
                { name = rem-type, type= "string:160"}
                { name = tran-sub-type, type= "string:50"}
                { name = part-tran-srl-num, type= "string:5", key = true}
                { name = avl-bal, type= "string:20"}
                { name = acct-crncy, type= "string:5"}
                { name : bank-name, type= "string:5"}
                { name : remitter-name, type= "string:5"}
                { name : beneficiary-name, type= "string:5"}
                { name : country-code, type= "string:5"}
                { name : device-id, type= "string:5"}
                { name : cust-details, type : clari5.customAml.web.linkanalysis.record.cust-details}
                { name : account-details, type: clari5.customAml.web.linkanalysis.record.acct-details}
                { name : case-info, type : [clari5.customAml.web.linkanalysis.record.case-info]}
            ]
        }

        link-credit {
            attributes = [
                { name : details, type: [clari5.customAml.web.linkanalysis.record.link-txn]}
                { name : acctId, type: "string:20"}
                { name : amount, type: "decimal:20,2"}
            ]
        }

        link-debit {
            attributes = [
                { name : details, type: [clari5.customAml.web.linkanalysis.record.link-txn]}
                { name : acctId, type: "string:20"}
                { name : amount, type: "decimal:20,2"}
            ]
        }

        cust-details {
             attributes = [
                { name = name, type: "string:150" }
                { name = cust-id, type: "string:50" }
                { name = phone-n-o, type: "string:60" }
                { name = email-i-d, type: "string:50" }
                { name = customer-segment, type: "string:40" }
                { name = nature-of-business, type: "string:40" }
                { name = annual-income, type : "decimal:23,3" }
                { name = mailing-addr, type: "string:600" }
             ]
        }

        acct-details {
            attributes = [
                { name = name, type: "string:80" }
                { name = scheme-type, type: "string:40" }
                { name = currency, type: "string:10" }
                { name = opened-date, type:date }
                { name = scheme-code, type: "string:20" }
                { name = status, type: "string:20" }
                { name = cust-i-d, type: "string:20" }
                { name = acct-i-d, type: "string:50" }
            ]
        }

        case-info {
            attributes = [
                { name = case-i-d, type: "string:20" }
                { name = incident-i-d, type: "string:20" }
                { name = risk-level, type: "string:20" }
                { name = score, type: "string:20" }
                { name = entity-i-d, type: "string:20" }
                { name = created-on, type: date }
                { name = module-i-d, type: "string:20" }
                { name = fact-name, type: "string:20" }
            ]
        }
	}

	uow {
        link-customers {
            input = [
                { name : acctId, type: "string:20"}
                { name : minDate, type: DATE}
                { name : maxDate, type: DATE}
                { name : flag, type: "string:10"}
            ]
            output = [
                { name : account, type: "string:20"}
                { name : credit, type: [clari5.customAml.web.linkanalysis.record.link-credit]}
                { name : debit, type: [clari5.customAml.web.linkanalysis.record.link-debit]}
                { name : current-cust-details, type : clari5.customAml.web.linkanalysis.record.cust-details}
                { name : current-account-details, type: clari5.customAml.web.linkanalysis.record.acct-details}
                { name : current-case-info, type : [clari5.customAml.web.linkanalysis.record.case-info]}
            ]
        }
	}
}
