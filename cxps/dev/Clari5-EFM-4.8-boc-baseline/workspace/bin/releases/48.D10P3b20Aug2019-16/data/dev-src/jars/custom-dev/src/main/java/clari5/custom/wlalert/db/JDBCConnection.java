package clari5.custom.wlalert.db;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCConnection implements IDBConnection {
    private static final CxpsLogger logger = CxpsLogger.getLogger(JDBCConnection.class);
    private Connection connection;

    @Override
    public Object getConnection() {
        connection = Rdbms.getAppConnection();
        return connection;
    }

    @Override
    public void closeConnection() {
        try {
            if (connection != null) connection.close();
        } catch (SQLException sql) {
            logger.error(logger.getClass() + " Unable to close  connection ");
        }
    }

}
