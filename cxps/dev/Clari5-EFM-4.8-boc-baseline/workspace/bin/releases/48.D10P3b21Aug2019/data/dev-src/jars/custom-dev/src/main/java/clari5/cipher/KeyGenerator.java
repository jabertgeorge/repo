package clari5.cipher;

import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Properties;

public class KeyGenerator {
    private static byte[] key;
    private static SecretKeySpec secretKey;

    static {
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("key.prop");
            Properties p = new Properties();
            p.load(inputStream);
            String plainKey = p.getProperty("key");
            genSecretKey(plainKey);
            System.out.println("Key loaded successfullly" + new StringBuilder(plainKey).reverse());

        } catch (Exception e) {
            System.out.println("Problem whil loading key");
            e.printStackTrace();

        }
    }


    public static byte[] genSecretKey(String plainKey) throws Exception {

        MessageDigest sha = null;
        try {
            key = plainKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");

            System.out.print("Secret key is " + secretKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return key;
    }

    public static SecretKeySpec getSecretKey() {
        return secretKey;
    }

    public static void main(String args[]) {
        System.out.println("");


    }
}
