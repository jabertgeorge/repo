/* utils.js start */
function loadUrl(which)
{
	alert(which.href);
	document.getElementById("cPage").src = which.href;
}

function loadURL(url)
{
	alert(url);
	document.getElementById("cPage").src = url;
}

function parentURL(url){
window.parent.document.getElementById("cPage").src=url;
}

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
  separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

function toggleClassByID(id,className,removeClass){
    if(removeClass!=""){
        $("#"+id).removeClass(removeClass);
    }else{
        $("#"+id).removeClass();
    }
    $("#"+id).addClass(className);
}

function loadJSFile(jsSrc){
	var head = document.getElementsByTagName('head')[0];
	var js = document.createElement("script");
	js.type = "text/javascript";
	js.async = true;
	js.src =jsSrc;
	head.appendChild(js);
}

function displayNotification(text, ifSticky, type)
{
	var toastMessageSettings = {
		text: text,
		sticky: ifSticky,
		position: 'top-center',
		type: type,
		closeText: ''
	};
	$().toastmessage('showToast', toastMessageSettings);
}
/* utils.js end */
