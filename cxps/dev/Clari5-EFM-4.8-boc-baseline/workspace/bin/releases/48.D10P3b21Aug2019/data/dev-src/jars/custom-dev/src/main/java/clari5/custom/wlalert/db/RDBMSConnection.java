package clari5.custom.wlalert.db;

import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
public class RDBMSConnection implements IDBConnection {
    private RDBMSSession rdbmsSession;

    @Override
    public Object getConnection() {
        rdbmsSession = Rdbms.getAppSession();
        return rdbmsSession;
    }

    @Override
    public void closeConnection() {
        if (rdbmsSession != null) rdbmsSession.close();
    }
}
