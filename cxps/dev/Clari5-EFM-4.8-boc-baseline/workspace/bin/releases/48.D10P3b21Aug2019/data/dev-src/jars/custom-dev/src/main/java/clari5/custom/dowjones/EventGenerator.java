package clari5.custom.dowjones;

import java.util.List;

public class EventGenerator {

    private String  dj_id;
    private String list_name;
    private String sanctions_references_list_provider_code;
    private String description_code;
    private String max_score;
    private String rule_name;
    private String name;
    private String query_value;
    private String value;
    private String score;
    private String nameOne;
    private String queryValueOne;
    private String valueOne;
    private String scoreOne;
    private String cust_id;
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDj_id() {
        return dj_id;
    }

    public void setDj_id(String dj_id) {
        this.dj_id = dj_id;
    }

    public String getList_name() {
        return list_name;
    }

    public void setList_name(String list_name) {
        this.list_name = list_name;
    }

    public String getSanctions_references_list_provider_code() {
        return sanctions_references_list_provider_code;
    }

    public void setSanctions_references_list_provider_code(String sanctions_references_list_provider_code) {
        this.sanctions_references_list_provider_code = sanctions_references_list_provider_code;
    }

    public String getDescription_code() {
        return description_code;
    }

    public void setDescription_code(String description_code) {
        this.description_code = description_code;
    }

    public String getMax_score() {
        return max_score;
    }

    public void setMax_score(String max_score) {
        this.max_score = max_score;
    }

    public String getRule_name() {
        return rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery_value() {
        return query_value;
    }

    public void setQuery_value(String query_value) {
        this.query_value = query_value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getNameOne() {
        return nameOne;
    }

    public void setNameOne(String nameOne) {
        this.nameOne = nameOne;
    }

    public String getQueryValueOne() {
        return queryValueOne;
    }

    public void setQueryValueOne(String queryValueOne) {
        this.queryValueOne = queryValueOne;
    }

    public String getValueOne() {
        return valueOne;
    }

    public void setValueOne(String valueOne) {
        this.valueOne = valueOne;
    }

    public String getScoreOne() {
        return scoreOne;
    }

    public void setScoreOne(String scoreOne) {
        this.scoreOne = scoreOne;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }
}
