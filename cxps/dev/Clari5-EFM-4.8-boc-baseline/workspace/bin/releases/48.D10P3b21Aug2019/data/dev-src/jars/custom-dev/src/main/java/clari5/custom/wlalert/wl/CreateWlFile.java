package clari5.custom.wlalert.wl;


import clari5.custom.wlalert.db.wl.TableTemplates;
import clari5.custom.wlalert.db.wl.WlListGen;

import clari5.tools.util.Hocon;
import cxps.apex.utils.CxpsLogger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author shishir
 * @since 22/06/2018
 */
public class CreateWlFile {
    private static final CxpsLogger logger = CxpsLogger.getLogger(CreateWlFile.class);

    static Hocon hocon;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("custom-wl-indexing.conf");
    }


    public void getTableList() {
        logger.info("Start time " + LocalDateTime.now());
        List<String> list = hocon.getStringList("tables");
        logger.info("Tables List " + list.toString());

        WlListGen wlListGen = new WlListGen(hocon);
        for (String tablName : list) {
            TableTemplates tableTemplates = wlListGen.createTblTemplate(tablName.toLowerCase());

            String fileExtension = hocon.getString("fileExtension");
            if (fileExtension.equalsIgnoreCase("zip")) genZipFile(tableTemplates);
            else genCsvFile(tableTemplates);

            tableTemplates.getHeader().clear();
            tableTemplates.getValues().clear();
        }

        logger.info("End time " + LocalDateTime.now());

    }


    /**
     * @param tableTemplates table fields
     *                       This method will convert the csv to zip.
     */
    public void genZipFile(TableTemplates tableTemplates) {

        genCsvFile(tableTemplates);

        String outputPath = hocon.get("csv-file").getString("op-path");

        File fileName = new File(outputPath + tableTemplates.getListName()+"_" + tableTemplates.getTableName() + ".csv");

        ZipOutputStream zipOut=null;
        FileInputStream fis=null;
        FileOutputStream zipFileName=null;
        try {
             fis = new FileInputStream(fileName);

             zipFileName = new FileOutputStream(fileName.getParent()+File.separator+ tableTemplates.getDropdownlist()+"_" +tableTemplates.getTableName()+System.currentTimeMillis() + ".zip");

            zipOut = new ZipOutputStream(zipFileName);
            ZipEntry zipEntry = new ZipEntry(fileName.getName());
            zipOut.putNextEntry(zipEntry);


            final byte[] bytes = new byte[1024];
           // final byte[] bytes = Longs.toByteArray(fileName.length());

            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            Files.delete(Paths.get(fileName.getAbsolutePath()));

        }catch (IOException io){
            logger.error("Failed to generate the zip file"+io.getMessage() +" Cause "+io.getCause());
        }finally {
            try {
                zipOut.close();
                fis.close();
                zipFileName.close();
            }catch (IOException io){}
        }
    }


    /**
     * @param tableTemplates Table name and fields
     *                       This method will create a csv file of table contents
     */
    public void genCsvFile(TableTemplates tableTemplates) {
        String outputPath = hocon.get("csv-file").getString("op-path");

        logger.info("Table Template size " + tableTemplates.size());

        FileWriter fileWriter = null;
        try {

            File dir = new File(outputPath);
            if (!dir.exists()) dir.mkdirs();


            File file = new File(dir.getPath() +File.separator+ tableTemplates.getListName()+"_" + tableTemplates.getTableName() + ".csv");

            if (file.exists()) file.delete();

            if (file.createNewFile()) {
                logger.info("File is created " + file.getAbsolutePath());
            } else {
                logger.warn("Failed to create a file " + file.getAbsolutePath());
                return;
            }
            fileWriter = new FileWriter(file);
            int headerLength = tableTemplates.getHeader().size();
            writeCsv(fileWriter, tableTemplates.getHeader(), headerLength);
            writeCsv(fileWriter, tableTemplates.getValues(), headerLength);
        } catch (IOException io) {
            logger.error("Failed to generate wl file " + io.getMessage() + "\n Cause " + io.getCause());
        } finally {
            try {
                fileWriter.close();
            } catch (IOException io) {
                logger.error("Failed wile closing fileWritter " + io.getMessage() + "Cause" + io.getCause());
            }
        }

    }

    public void writeCsv(FileWriter writer, List<String> list, int headLength) throws IOException {
        String delimiter = hocon.get("csv-file").getString("delimiter");

        int headcount = 0;
        for (String fact : list) {

            if (headcount != headLength - 1) {
                headcount++;
                writer.write(fact + delimiter);
            } else {
                headcount = 0;
                writer.write(fact);
                writer.write("\n");
            }
        }

    }

    /*public static void main(String[] args) {

        String[]header = {"id","name","age","roll","skill","edu","dob"};
        String[]values = {"007","shishir","27","developer","java","B.TECH","21-jan"};

        TableTemplates tableTemplates = new TableTemplates();
        tableTemplates.setTableName("customer_master");
        tableTemplates.setHeader(Arrays.asList(header));
        tableTemplates.setValues(Arrays.asList(values));
        tableTemplates.setListName("customer");
        System.out.println("Start time:" + LocalDateTime.now());


        System.out.println( hocon.getString("fileExtension"));

        CreateWlFile createWlFile = new CreateWlFile();
        createWlFile.genZipFile(tableTemplates);

        System.out.println("End time:" + LocalDateTime.now());
    }*/
}
