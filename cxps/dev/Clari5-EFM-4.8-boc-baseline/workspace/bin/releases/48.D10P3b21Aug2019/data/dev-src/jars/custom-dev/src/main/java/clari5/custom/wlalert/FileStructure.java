package clari5.custom.wlalert;

import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileStructure
{
    public static CxpsLogger logger = CxpsLogger.getLogger(FileStructure.class);
    List<String> directories = new ArrayList();
    static Hocon hocon = new Hocon();

    static
    {
        hocon.loadFromContext("custom-fraudlist.conf");
    }

    static Hocon customFraudList = hocon.get("custom-fraudlist");
    String filePath;
    String archiveFilePath;
    String unzipFilePath;
    String indexingPath;

    public File[] getDirList()
    {
        File directory = new File(getInputFilePath());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        if (directory.isDirectory())
        {
            if (directory.listFiles().length <= 0) {
                for (String directorieslist : getDirectories())
                {
                    File zipFileDirectory = new File(getInputFilePath() + File.separator + directorieslist);
                    if (!zipFileDirectory.exists()) {
                        zipFileDirectory.mkdirs();
                    }
                }
            }
            return directory.listFiles();
        }
        logger.warn("No Directry found " + this.filePath);
        System.out.println("No Directry found " + this.filePath);
        return null;
    }

    public String getFileType(String dirName)
    {
        return customFraudList.get("fileType").getString(dirName);
    }

    public String getInputFilePath()
    {
        this.filePath = customFraudList.get("filePath").getString("inputFilePath");
        return this.filePath;
    }

    public String getArchiveFilePath()
    {
        this.archiveFilePath = customFraudList.get("filePath").getString("archiveFilepath");

        return this.archiveFilePath;
    }

    public String getUnzipFilePath()
    {
        return this.unzipFilePath;
    }

    public void setUnzipFilePath(String unzipFilePath)
    {
        this.unzipFilePath = unzipFilePath;
    }

    public String getIndexingPath()
    {
        this.indexingPath = customFraudList.get("filePath").getString("indexingPath");
        return this.indexingPath;
    }

    public void setIndexingPath(String indexingPath)
    {
        this.indexingPath = indexingPath;
    }

    public List<String> getDirectories()
    {
        return this.directories = customFraudList.get("filePath").getStringList("fileCategory");
    }

    public void setDirectories(List<String> directories)
    {
        this.directories = directories;
    }

    public String toString()
    {
        return "FileStructure{filePath='" + this.filePath + '\'' + ", archiveFilePath='" + this.archiveFilePath + '\'' + ", unzipFilePath='" + this.unzipFilePath + '\'' + ", indexingPath='" + this.indexingPath + '\'' + ", directories=" + this.directories + '}';
    }
}
