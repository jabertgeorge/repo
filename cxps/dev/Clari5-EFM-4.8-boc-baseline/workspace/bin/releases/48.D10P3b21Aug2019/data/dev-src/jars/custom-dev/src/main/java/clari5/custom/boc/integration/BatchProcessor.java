package clari5.custom.boc.integration;

import clari5.custom.boc.integration.audit.DatabaseAudit;
import clari5.custom.boc.integration.builder.EventBuilder;
import clari5.custom.boc.integration.config.BepCon;
import clari5.custom.boc.integration.data.ITableData;
import clari5.custom.boc.integration.db.DBTask;
import clari5.platform.applayer.CxpsDaemon;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import cxps.apex.utils.CxpsLogger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BatchProcessor extends CxpsDaemon implements ICxResource {
    public static CxpsLogger logger = CxpsLogger.getLogger(BatchProcessor.class);
    private DatabaseAudit dblogger = DatabaseAudit.getLogger();
    static ConcurrentLinkedQueue<String> tables = new ConcurrentLinkedQueue<String>();
    static ConcurrentLinkedQueue<ITableData> dataque = new ConcurrentLinkedQueue<ITableData>();
    //BlockingQueue<ITableData> dataque = new BlockingQueue<ITableData>();
    private final static Object lock = new Object();

    @Override
    public Object getData() {
        synchronized (lock) {
            DBTask.updateServerIDInfoInController();
            try {

                if (tables.isEmpty()) {
                    String tableName[] = BepCon.getConfig().getOrderOfTableProc();
                    for (String table : tableName) {
                        tables.add(table);
                    }
                }
                //Thread.sleep(10000);
            } catch (Exception e) {
                logger.info("Not able to fetch the tablename");
            }
            logger.info("TAble Added  " + Thread.currentThread().getName());
        }

        synchronized (lock) {
            if (dataque.isEmpty()) {
                String tableName = tables.poll();
                logger.debug("Table: " + tableName);
                while (tableName != null && !"".equals(tableName)) {
                    List<ITableData> data = fetchData(tableName);
                    dataque.addAll(data);
                    tableName = tables.poll();
                }
            }
        }

        if (dataque.isEmpty()) {
            return new Object();
        } else {
            return new Object();
        }
    }

    public List<ITableData> fetchData(String tableName) {
        synchronized (lock) {
            logger.info("Fetch Data from " + tableName);
            List<ITableData> list = new ArrayList<ITableData>();

            try {
                EventLoader loader = new EventLoader();
                list = loader.getFreshEvents(tableName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            logger.info("Fetch Data completed for  " + tableName + " with size " + list.size());
            return list;
        }

    }

    @Override
    public void processData(Object o) throws RuntimeFatalException {

        ITableData row = null;
        Map<ITableData, String> statusmap = new HashMap<ITableData, String>();
        while ((row = dataque.poll()) != null) {
            logger.info("dataque size" + dataque.size() + " by thread" + Thread.currentThread().getName());
            String status = "";
            try {
                //row.process();
                EventBuilder eb = new EventBuilder();
                if (eb.process(row))
                    status = "SUCCESS";
                else
                    status = "FAILED";
            } catch (Exception e) {
                logger.error("ERROR while processing event.");
                e.printStackTrace();
                logger.error("Exception caught while processing data for the row, ", row);
                status = getStackTrace(e);
                if (status.length() > 4000) {
                    status = status.substring(0, 4000);
                }
            }
            statusmap.put(row, status);
        }
        try {
            DBTask.updateStatus(statusmap);
        } catch (Exception e) {
            logger.error("Error while updating timestamp");
        }
    }

    private void updateStatus(Map<ITableData, String> statusmap) throws Exception {
        try {
            DBTask.updateStatus(statusmap);
        } catch (Exception e) {
            //////////////////////////////////////////////////////
            // If update status itself fails, what can we do. Think.
            //////////////////////////////////////////////////////
            throw e;
        }
    }

    @Override
    public void configure(Hocon h) {
    }

    @Override
    public void release() {

    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void refresh() {

    }

    public static String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    public static void main(String args[]) {
        //THread Executro cclass need to define number of threads

    }

}
