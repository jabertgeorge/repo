// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_CcTxnEventMapper extends EventMapper<FT_CcTxnEvent> {

public FT_CcTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_CcTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_CcTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_CcTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_CcTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_CcTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_CcTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTxnCurrency());
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setDouble(i++, obj.getAvlCrLmt());
            preparedStatement.setString(i++, obj.getTxnCountryCode());
            preparedStatement.setString(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getChannelId());
            preparedStatement.setString(i++, obj.getMcc());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setString(i++, obj.getTxnCountry());
            preparedStatement.setDouble(i++, obj.getLcyAmt());
            preparedStatement.setTimestamp(i++, obj.getSystemTime());
            preparedStatement.setString(i++, obj.getTxnStatus());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getLcyCurncy());
            preparedStatement.setString(i++, obj.getCardNo());
            preparedStatement.setDouble(i++, obj.getEodCreditBal());
            preparedStatement.setDouble(i++, obj.getEodCrCurr());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getTxnId());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getTxnType());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getTxnSrlNo());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setString(i++, obj.getCardType());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getMerchantId());
            preparedStatement.setString(i++, obj.getTxnSubType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_CC]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_CcTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_CcTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_CC"));
        putList = new ArrayList<>();

        for (FT_CcTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TXN_CURRENCY",  obj.getTxnCurrency());
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "AVL_CR_LMT", String.valueOf(obj.getAvlCrLmt()));
            p = this.insert(p, "TXN_COUNTRY_CODE",  obj.getTxnCountryCode());
            p = this.insert(p, "EVENT_TIME",  obj.getEventTime());
            p = this.insert(p, "CHANNEL_ID",  obj.getChannelId());
            p = this.insert(p, "MCC",  obj.getMcc());
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TXN_COUNTRY",  obj.getTxnCountry());
            p = this.insert(p, "LCY_AMT", String.valueOf(obj.getLcyAmt()));
            p = this.insert(p, "SYSTEM_TIME", String.valueOf(obj.getSystemTime()));
            p = this.insert(p, "TXN_STATUS",  obj.getTxnStatus());
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "LCY_CURNCY", String.valueOf(obj.getLcyCurncy()));
            p = this.insert(p, "CARD_NO",  obj.getCardNo());
            p = this.insert(p, "EOD_CREDIT_BAL", String.valueOf(obj.getEodCreditBal()));
            p = this.insert(p, "EOD_CR_CURR", String.valueOf(obj.getEodCrCurr()));
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "TXN_ID",  obj.getTxnId());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "TXN_TYPE",  obj.getTxnType());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "TXN_SRL_NO",  obj.getTxnSrlNo());
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "CARD_TYPE",  obj.getCardType());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "MERCHANT_ID",  obj.getMerchantId());
            p = this.insert(p, "TXN_SUB_TYPE",  obj.getTxnSubType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_CC"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_CC]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_CC]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_CcTxnEvent obj = new FT_CcTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTxnCurrency(rs.getString("TXN_CURRENCY"));
    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setAvlCrLmt(rs.getDouble("AVL_CR_LMT"));
    obj.setTxnCountryCode(rs.getString("TXN_COUNTRY_CODE"));
    obj.setEventTime(rs.getString("EVENT_TIME"));
    obj.setChannelId(rs.getString("CHANNEL_ID"));
    obj.setMcc(rs.getString("MCC"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTxnCountry(rs.getString("TXN_COUNTRY"));
    obj.setLcyAmt(rs.getDouble("LCY_AMT"));
    obj.setSystemTime(rs.getTimestamp("SYSTEM_TIME"));
    obj.setTxnStatus(rs.getString("TXN_STATUS"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setLcyCurncy(rs.getDouble("LCY_CURNCY"));
    obj.setCardNo(rs.getString("CARD_NO"));
    obj.setEodCreditBal(rs.getDouble("EOD_CREDIT_BAL"));
    obj.setEodCrCurr(rs.getDouble("EOD_CR_CURR"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setTxnId(rs.getString("TXN_ID"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setTxnType(rs.getString("TXN_TYPE"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setTxnSrlNo(rs.getString("TXN_SRL_NO"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setCardType(rs.getString("CARD_TYPE"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setMerchantId(rs.getString("MERCHANT_ID"));
    obj.setTxnSubType(rs.getString("TXN_SUB_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_CC]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_CcTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_CcTxnEvent> events;
 FT_CcTxnEvent obj = new FT_CcTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_CC"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_CcTxnEvent obj = new FT_CcTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTxnCurrency(getColumnValue(rs, "TXN_CURRENCY"));
            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setAvlCrLmt(EventHelper.toDouble(getColumnValue(rs, "AVL_CR_LMT")));
            obj.setTxnCountryCode(getColumnValue(rs, "TXN_COUNTRY_CODE"));
            obj.setEventTime(getColumnValue(rs, "EVENT_TIME"));
            obj.setChannelId(getColumnValue(rs, "CHANNEL_ID"));
            obj.setMcc(getColumnValue(rs, "MCC"));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTxnCountry(getColumnValue(rs, "TXN_COUNTRY"));
            obj.setLcyAmt(EventHelper.toDouble(getColumnValue(rs, "LCY_AMT")));
            obj.setSystemTime(EventHelper.toTimestamp(getColumnValue(rs, "SYSTEM_TIME")));
            obj.setTxnStatus(getColumnValue(rs, "TXN_STATUS"));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setLcyCurncy(EventHelper.toDouble(getColumnValue(rs, "LCY_CURNCY")));
            obj.setCardNo(getColumnValue(rs, "CARD_NO"));
            obj.setEodCreditBal(EventHelper.toDouble(getColumnValue(rs, "EOD_CREDIT_BAL")));
            obj.setEodCrCurr(EventHelper.toDouble(getColumnValue(rs, "EOD_CR_CURR")));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setTxnId(getColumnValue(rs, "TXN_ID"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setTxnType(getColumnValue(rs, "TXN_TYPE"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setTxnSrlNo(getColumnValue(rs, "TXN_SRL_NO"));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setCardType(getColumnValue(rs, "CARD_TYPE"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setMerchantId(getColumnValue(rs, "MERCHANT_ID"));
            obj.setTxnSubType(getColumnValue(rs, "TXN_SUB_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_CC]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_CC]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TXN_CURRENCY\",\"ACCT_ID\",\"MSG_SOURCE\",\"KEYS\",\"AVL_CR_LMT\",\"TXN_COUNTRY_CODE\",\"EVENT_TIME\",\"CHANNEL_ID\",\"MCC\",\"ONLINE_BATCH\",\"TXN_COUNTRY\",\"LCY_AMT\",\"SYSTEM_TIME\",\"TXN_STATUS\",\"TXN_DR_CR\",\"TXN_AMT\",\"CX_ACCT_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"LCY_CURNCY\",\"CARD_NO\",\"EOD_CREDIT_BAL\",\"EOD_CR_CURR\",\"ADD_ENTITY_ID2\",\"TXN_CODE\",\"TXN_ID\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"TXN_TYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"CUST_ID\",\"TXN_SRL_NO\",\"CUST_CARD_ID\",\"CARD_TYPE\",\"CX_CUST_ID\",\"EVENT_NAME\",\"MERCHANT_ID\",\"TXN_SUB_TYPE\"" +
              " FROM EVENT_FT_CC";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TXN_CURRENCY],[ACCT_ID],[MSG_SOURCE],[KEYS],[AVL_CR_LMT],[TXN_COUNTRY_CODE],[EVENT_TIME],[CHANNEL_ID],[MCC],[ONLINE_BATCH],[TXN_COUNTRY],[LCY_AMT],[SYSTEM_TIME],[TXN_STATUS],[TXN_DR_CR],[TXN_AMT],[CX_ACCT_ID],[HOST_USER_ID],[HOST_ID],[LCY_CURNCY],[CARD_NO],[EOD_CREDIT_BAL],[EOD_CR_CURR],[ADD_ENTITY_ID2],[TXN_CODE],[TXN_ID],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[TXN_TYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[CUST_ID],[TXN_SRL_NO],[CUST_CARD_ID],[CARD_TYPE],[CX_CUST_ID],[EVENT_NAME],[MERCHANT_ID],[TXN_SUB_TYPE]" +
              " FROM EVENT_FT_CC";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TXN_CURRENCY`,`ACCT_ID`,`MSG_SOURCE`,`KEYS`,`AVL_CR_LMT`,`TXN_COUNTRY_CODE`,`EVENT_TIME`,`CHANNEL_ID`,`MCC`,`ONLINE_BATCH`,`TXN_COUNTRY`,`LCY_AMT`,`SYSTEM_TIME`,`TXN_STATUS`,`TXN_DR_CR`,`TXN_AMT`,`CX_ACCT_ID`,`HOST_USER_ID`,`HOST_ID`,`LCY_CURNCY`,`CARD_NO`,`EOD_CREDIT_BAL`,`EOD_CR_CURR`,`ADD_ENTITY_ID2`,`TXN_CODE`,`TXN_ID`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`TXN_TYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`CUST_ID`,`TXN_SRL_NO`,`CUST_CARD_ID`,`CARD_TYPE`,`CX_CUST_ID`,`EVENT_NAME`,`MERCHANT_ID`,`TXN_SUB_TYPE`" +
              " FROM EVENT_FT_CC";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_CC (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TXN_CURRENCY\",\"ACCT_ID\",\"MSG_SOURCE\",\"KEYS\",\"AVL_CR_LMT\",\"TXN_COUNTRY_CODE\",\"EVENT_TIME\",\"CHANNEL_ID\",\"MCC\",\"ONLINE_BATCH\",\"TXN_COUNTRY\",\"LCY_AMT\",\"SYSTEM_TIME\",\"TXN_STATUS\",\"TXN_DR_CR\",\"TXN_AMT\",\"CX_ACCT_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"LCY_CURNCY\",\"CARD_NO\",\"EOD_CREDIT_BAL\",\"EOD_CR_CURR\",\"ADD_ENTITY_ID2\",\"TXN_CODE\",\"TXN_ID\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"TXN_TYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"CUST_ID\",\"TXN_SRL_NO\",\"CUST_CARD_ID\",\"CARD_TYPE\",\"CX_CUST_ID\",\"EVENT_NAME\",\"MERCHANT_ID\",\"TXN_SUB_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TXN_CURRENCY],[ACCT_ID],[MSG_SOURCE],[KEYS],[AVL_CR_LMT],[TXN_COUNTRY_CODE],[EVENT_TIME],[CHANNEL_ID],[MCC],[ONLINE_BATCH],[TXN_COUNTRY],[LCY_AMT],[SYSTEM_TIME],[TXN_STATUS],[TXN_DR_CR],[TXN_AMT],[CX_ACCT_ID],[HOST_USER_ID],[HOST_ID],[LCY_CURNCY],[CARD_NO],[EOD_CREDIT_BAL],[EOD_CR_CURR],[ADD_ENTITY_ID2],[TXN_CODE],[TXN_ID],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[TXN_TYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[CUST_ID],[TXN_SRL_NO],[CUST_CARD_ID],[CARD_TYPE],[CX_CUST_ID],[EVENT_NAME],[MERCHANT_ID],[TXN_SUB_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TXN_CURRENCY`,`ACCT_ID`,`MSG_SOURCE`,`KEYS`,`AVL_CR_LMT`,`TXN_COUNTRY_CODE`,`EVENT_TIME`,`CHANNEL_ID`,`MCC`,`ONLINE_BATCH`,`TXN_COUNTRY`,`LCY_AMT`,`SYSTEM_TIME`,`TXN_STATUS`,`TXN_DR_CR`,`TXN_AMT`,`CX_ACCT_ID`,`HOST_USER_ID`,`HOST_ID`,`LCY_CURNCY`,`CARD_NO`,`EOD_CREDIT_BAL`,`EOD_CR_CURR`,`ADD_ENTITY_ID2`,`TXN_CODE`,`TXN_ID`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`TXN_TYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`CUST_ID`,`TXN_SRL_NO`,`CUST_CARD_ID`,`CARD_TYPE`,`CX_CUST_ID`,`EVENT_NAME`,`MERCHANT_ID`,`TXN_SUB_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

