<%
	response.setHeader("Pragma","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Expires","0");
	response.setDateHeader("Expires",-1);
%>
<%
	String protocol = "";
	if(request.getRequestURL().toString().trim().startsWith("https"))
	{
		protocol = "https";
	}
	else
	{
		protocol = "http";
	}
	// Pass the converse JSP URL
	String ctx = request.getContextPath();
	String hostport = request.getServerName() + ":" + request.getServerPort();
	String serverUrl = protocol+"://"+hostport+ctx;
	String userId = (String) request.getParameter("userId");
	String acctId = (String) request.getParameter("refId");
%>
<html>
	<head>
		<script type="text/javascript" src="/cdn/efm/common/js/lib/loadCDN.js"></script>
		<script>
			loadCSS(["ext/css/jquery/demo_table.css",
				"ext/css/jquery/jquery-ui.css",
				"ext/css/jquery/validation-engine/validationEngine.jquery.css",
				"ext/css/jquery/overlay-apple.css",
				"ext/css/jquery/jquery.toastmessage.css"]);
		</script>

		<script type="text/javascript" src="js/d3.js"></script>
	    <script type="text/javascript" src="js/d3.layout.js"></script>

	    <style type="text/css">

			.node circle {
			  cursor: pointer;
			  fill: #fff;
			  stroke: steelblue;
			  stroke-width: 1.5px;
			}

			.node text {
			  font: 10px sans-serif;
			}

			path.link {
			  fill: none;
			  stroke: #ccc;
			  stroke-width: 1.5px;
			}
			path.link {
			    fill: none;
			    stroke: #000;
			    stroke-width: 4px;
			}
		</style>

  		<link rel="stylesheet" href="css/slide.css" type="text/css" />
		<link rel="stylesheet" href="/cdn/efm/common/css/style.css" type="text/css" />

		<style>
			body {
				overflow: auto;
				overflow-x:auto;
				overflow-y:auto;

				height:auto !important;
			}

			/* context menu styles */
			#contextmenu{
				margin:2px;
				padding:5px;
				border: 2px solid #000;
				background:#191919;
				color:#fff;
			}
			#contextmenu li {
				list-style-type:none;
				margin:1px;
				padding:2px;
				cursor:pointer;
			}
			#contextmenu li.hover{
				background: #ddd;
				color:#191919;
			}
			/* context menu styles end */

			.start-acct-w-24 {
				background: url("../common/images/acct-w-20.png") 3px 5px no-repeat;
				background-color: #003366 !important;
				padding-left: 35px;
				padding-right: 15px;
				margin-right:0;
				-webkit-transition: padding-right 0.7s;
				box-shadow: 0 0 20px #FFF;
			}

			.search-w-16-w-la {
				background: url("../common/images/search-w-16-w.png") no-repeat scroll 5px 1px transparent;
				padding: 1px 5px 16px 16px;
			}

			.acct-w-24-crtd {
				background: url("../common/images/acct-w-20.png") 3px 5px no-repeat;
				padding-left: 24px;
				padding-right: 15px;
				margin-right:0;
				width:100px;
				background-color: #808080 !important;
				border-radius: 2px;
				-webkit-transition: padding-right 0.7s;
				transition: padding-right 0.7s;
			}

			.cacct-w-24-crtd {
				background: url("../common/images/crdt-w-20.png") 3px 5px no-repeat;
				padding-left: 24px;
				padding-right: 15px;
				margin-right:0;
				width:105px;
				background-color: #808080 !important;
				border-radius: 0 13px 13px 0;
				-webkit-transition: padding-right 0.7s;
				transition: padding-right 0.7s;
			}

			.dnf-w-16 {
				background: url("../common/images/end-w-16-b.png") 3px 7px no-repeat;
				background-color:#C0C0C0 !important;
				padding-left: 24px;
				padding-right: 15px;
				margin-right:0;
				width:100px;
				border-radius: 13px 0 0 13px;
				-webkit-transition: padding-right 0.7s;
				transition: padding-right 0.7s;
			}

			.fav-w-16-w {
				background: url("../common/images/fav-w-16-w.png") no-repeat scroll 1px 1px transparent;
				padding: 1px 1px 16px 16px;
			}

			div.vertical-line {
				width: 1px;
				background-color: silver;
				height: 100%;
				border: 2px ridge silver ;
				border-radius: 2px;
				vertical-align:middle;
			}

			hr.vertical
			{
				margin-left:20px;
				width: 0px;
				height: 100%; /* or height in PX */
			}

			.horizontal {
				width: 50px;
				background-color: silver;
				height: 1px;
				
				border: 2px ridge silver ;
				border-radius: 2px;
			}

			.bhorizontal {
				width: 50px;
				background-color: silver;
				height: 1px;
				
				border: 2px ridge silver ;
				border-radius: 2px;
			}

			.ahorizontal {
				width: 50px;
				background-color: silver;
				height: 1px;
				border: 2px ridge silver ;
				border-radius: 2px;
			}

			#canDiv { vertical-align:middle; display:table; height:100%; margin:10px; padding:0; }

			#creditArea { display:table-cell; height:100%; width:220px; vertical-align:middle; background:#F0F0F0; }
			#creditArea table { height:100%; width:100%; border-collapse:collapse; margin:0; padding:0; }
			#creditArea td { padding:5px 0; margin:0; font-size:0.79em; text-align:right; }
			#creditArea div, span, a { vertical-align:middle; }
			#creditArea a { text-decoration:none; }

			#refAcctD { display:table-cell; height:100%; margin:0; padding:0; }

			#debtArea { display:table-cell; height:100%; width:200px; vertical-align:middle; }
			#debtArea table { height:100%; border-collapse:collapse; margin:0; padding:0; }
			#debtArea td { padding:5px 0; margin:0; font-size:0.79em;}
			#debtArea div, span, a { vertical-align:middle; }
			#debtArea a { text-decoration:none; }

			.acct-detail {	display:table-cell; height:100%; width:215px; vertical-align:middle; }
			.acct-detail table { height:100%; border-collapse:collapse; margin:0; padding:0; }
			.acct-detail td { padding:5px 0; margin:0; font-size:0.79em; }
			.acct-detail div, span, a { vertical-align:middle; }
			.acct-detail a { text-decoration:none; }
			.filterInfo {
			    border: 2px solid #888;
			    border-radius: 5px;
			    margin: 10px;
			    overflow: auto;
			    padding: 10px;
			}
			.one-third{
				float: left;
    			width: 33%;
			}
			img{
				display: none !important;
			}
			#canv{
				height: auto !important;
			}
			#debtArea table tr:not(:first-child):not(:last-child){
				height: auto !important;
			}
			body{
				/*height: auto;*/
				 min-height: 100%;
			}
			
			.detailsInfo span{
				padding: 5px;
				
			}
			.detailsInfo span:first-child {
			    display: inline-block;
			    font-weight: bold;
			    width: 20%;
			}
			.detailsInfo span:last-child {
			    display: inline-block;
			    width: 75%;
			}
			.apple_overlay {  
			    background-image: none;
			    background-color: #fff;
			    padding: 10px 5px 12px;
			    border-radius: 12px;
			}
			.apple_overlay .close {
			    right: -8px;
    			top: -15px;
			}
			.hide{
				position: fixed;
				background: #ececec;
				width: 100%;
				top: 0;
				bottom: 0;
				left: 0;
				right: 0;
				background: rgba(0, 0, 0, 0.7);
  				transition: opacity 500ms;
  				display: none;
  				padding:70px auto;
  				
			}
			.destroy{
				background-image: url(../../efm/linkAnalysis/images/close.png);
 			    position: absolute;
			    right: 0px;
			    top: 0px;
			    cursor: pointer;
			    height: 26px;
			    width: 24px;
			}
			.apple_overlay .close{
				display: none;
			}
			.container{
				padding: 5px;
				border: 1px solid #888888;

			}
			#descOfCust{
				margin: 0 auto;
    			display: block;
			}
		</style>

		<script>
			var jqFile = ["ext/jquery/tools-full/jquery.tools.min.js"];

			var jsFiles = [
			    "ext/jquery/jquery-ui.js",
				"ext/jquery/gettheme.js",
				"ext/jquery/validation-engine/jquery.validationEngine-en.js",
				"ext/jquery/validation-engine/jquery.validationEngine.js",
				"ext/jquery/contextmenu/jquery.contextmenu.js",
				"ext/jquery/jquery.dataTables.min.js",
				"ext/jquery/jquery.toastmessage-min.js"
			];
            var localJsFiles = [
                "/cdn/efm/common/js/lib/utils.js",
                "/cdn/efm/common/js/lib/cxNetwork.js",
                "js/laService.js"
            ];

            loadFromCDN(jqFile, load);

			function load(){
				loadFromCDN(jsFiles, loadLocalFiles);
			}

			function loadLocalFiles(){
                addLocalJs(localJsFiles, null);
            }

		</script>
		<%--
		<script type="text/javascript" src="../common/js/lib/utils.js" ></script>
		<script type="text/javascript" src="../common/js/lib/cxNetwork.js" ></script>
		<script type="text/javascript" src="js/laService.js"></script>
		--%>
		<script>
			var oTable;
			function init()
			{
				var userId = '<%=userId%>';
				var serverUrl = '<%=serverUrl%>';
				setParams(userId, serverUrl);

				$("#usrLI").html("Hi "+userId);
				//document.getElementById("acctTI").value = '<%=acctId%>';

				$("#laForm").validationEngine('attach', {scroll: false});
				$("#canv").css("height", ($(document).height()-40));

				//$("td a[rel]").overlay({effect: 'apple'});

				$('.search-w-16-w-la').css({ "rotate": '45' });

				// Expand Panel
				$("#open").click(function(){
					$("div#panel").slideDown("slow");
	
				});	
				// Collapse Panel
				$("#close").click(function(){
					$("div#panel").slideUp("slow");	
				});		
	
				// Switch buttons from "Search" to "Close Panel" on click
				$("#toggle a").click(function () {
					$("#toggle a").toggle();
				});

				//$(".dp").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});

				$("#startDTI").datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					onClose: function( selectedDate ) {
						$("#endDTI").datepicker("option", "minDate", selectedDate);
					}
				});
				$( "#endDTI" ).datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					onClose: function(selectedDate) {
						$("#startDTI").datepicker("option", "maxDate", selectedDate);
					}
				});

				$(".close").click(function(){
					$(".hide").hhide();
				})	

			}

			function showAnch(obj)
			{
				$(obj).find("a.add-w-16-w").show();
			}

			function hideAnch(obj)
			{
				$(obj).find("a.add-w-16-w").hide();
			}
			
			function searchAcct()
			{
				var isTrue = $("#laForm").validationEngine('validate', {scroll: false});
				if (!isTrue)
					return;

				// close the panel
				$("div#panel").slideUp("slow");
				$("#toggle a").toggle();

				var acctId = document.getElementById("acctTI").value;
				var startD = document.getElementById("startDTI").value;
				var endD = document.getElementById("endDTI").value;
				var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"begin\"}";
				var inputObj = JSON.parse(inObjStr);
				getAcctDetail(inputObj, searchH, true);
			}
			

			function loadAcct(e,el)
			{
				//alert(" load account ::: "+$(el).clone().children().remove().end().text());
				//alert(" load acct :: "+$(el.parentNode).find("div:nth-child(1)").text());

				var dateNArry = [];
				var dateArry = [];
				var acctId = $(el).clone().children().remove().end().text();
				var dJson = JSON.parse($(el.parentNode).find("div:nth-child(1)").text());
				if ( dJson.length > 0 ) {
					var d=0;
					var dat = 0;
					for ( var i=0; i<dJson.length; i++ ) {
						var dtLAry = dJson[i].date.split(" ");
						if ( dtLAry.length > 1 ) {
							dat = dtLAry[0];
							d = Date.parse(dat);
						} else {
							dat = dJson[i].date;
							d = Date.parse(dat);
						}
						dateArry.push(dat);
						dateNArry.push(d);
					}
				}
				var fromDate = Math.min.apply(Math, dateNArry);
				var fromDI = dateNArry.indexOf(fromDate);
				var toDate = Math.max.apply(Math, dateNArry);
				var toDateI = dateNArry.indexOf(toDate);

				var startD = dateArry[fromDI];
				var endD = dateArry[toDateI];

				var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"begin\"}";
				var inputObj = JSON.parse(inObjStr);
				getAcctDetail(inputObj, searchH, true);
			}

			var detDivObj = null;
			function getNext(tObj, acctId, dJson)
			{
				var dateNArry = [];
				var dateArry = [];
				if ( dJson.length > 0 ) {
					var d=0;
					var dat = 0;
					for ( var i=0; i<dJson.length; i++ ) {
						var dtLAry = dJson[i].date.split(" ");
						if ( dtLAry.length > 1 ) {
							dat = dtLAry[0];
							d = Date.parse(dat);
						} else {
							dat = dJson[i].date;
							d = Date.parse(dat);
						}
						dateArry.push(dat);
						dateNArry.push(d);
					}
				}
				var fromDate = Math.min.apply(Math, dateNArry);
				var fromDI = dateNArry.indexOf(fromDate);
				var toDate = Math.max.apply(Math, dateNArry);
				var toDateI = dateNArry.indexOf(toDate);

				this.detDivObj = tObj;
				var startD = dateArry[fromDI];
				var endD = dateArry[toDateI];
				var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"XYZ\"}";
				var inputObj = JSON.parse(inObjStr);
				getNextLevel(inputObj, nextH, true);
			}

			

			function displayNotification(text, ifSticky, type)
			{
				var toastMessageSettings = {
					text: text,
					sticky: ifSticky,
					position: 'middle-center',
					type: type,
					closeText: ''
				};
				$().toastmessage('showToast', toastMessageSettings);
			}

			function loadInfoTest(objs){
				
				///console.log(objs,"objs");
				
				var type = $(objs).parents('.filterInfo').find('select').val();
				var value = $(objs).parents('.filterInfo').find('.one-third').find('input').val();
				console.log(type,value);
				
				$.ajax({
					//url: "jsonData/newInfo.json",
					//url: "jsonData/linkAnalysis.json",
					url: "LAServlet",
				    method: "POST",
				    data: { 
				    	'type' : type,
				    	'value':value
				    },
				    dataType: "JSON",
				    success:function(res){
				    	accInformation=res; 
				    	console.log(res,"In Success");
				    	
						drawTree(accInformation);

						$("body").css({'height':'auto'});
					
		
				    },
				    error:function(){
				    	alert("In Error");

				    }
				})
			}	

			

			function description(){
				
			    var elt = d3.select(this);
			   
				showDetails(elt.text());
			}

			//function showDetails(obj, res)
			function showDetails(res)
			{
				if(accInformation.children.length>0){
					if(accInformation != undefined && accInformation != null){
						for (var i=0;i<accInformation.children.length;i++){
							if(res == accInformation.children[i].Acc){
								
								var html = '';
								    html += '<div>';
								    var keyArr = [];
								for(key in accInformation.children[i]) {
									
								    keyArr.push(key);
								    //console.log(keyArr,"keyArr");
								    for(var j=0;j<keyArr.length;j++){
								    	if(keyArr[j] == "Account"){
								    		 if(key != "Account"){
										        /*html += '<p>';				   								
											    html += '<span>'+key+':';
											    html += '</span>';
											    html += '<span>'+accInformation.children[i][key];
											    html +='</span>';    
											    html += '</p>';*/
										    }
									    else{
									    	var detailsVal = accInformation.children[i][key];
									    	//console.log(detailsVal,"detailsVal");
									    	for(detailsKey in detailsVal[0]) {
									    		html += '<p>';
								    			html += '<span>'+detailsKey+':';
											    html += '</span>';
											    html += '<span>'+detailsVal[0][detailsKey];
											    html +='</span>';
											    html += '</p>';
									    	}

									    }
								    	}
								    }
								    							   						
								}
								html += '</div>';
								$(".detailsInfo").html(html);
								$(".hide").show();
								$(".apple_overlay").show().css({"margin":"70px auto"});
								$("#detailO").overlay({effect: 'apple'}); //detailO
								$(".dataTableCss").css("width", "100%");
							}

						}
						//$(".detailsInfo").html(html);
					}
				}
				//$(obj).overlay({effect: 'apple'})
				/*$(".hide").show();
				$(".apple_overlay").show().css({"margin":"70px auto"});
				$("#detailO").overlay({effect: 'apple'}); //detailO
				$(".dataTableCss").css("width", "100%");*/
			}

			function closeIt(){
				$(".hide").hide();
			}
		</script>
	</head>

	<body style="vertical-align:middle;" onload="init()" >
		<div id="canvs" style="margin:8px 0 0 0; padding:0;" > 
			<div class="filterInfo">
				<div class="one-third">
					<span>Please choose option for search</span>
					<select class="filter">
						<option value="select">
							--Select--
						</option>
						<!--<option value="pan">
							PAN
						</option>-->
						<option value="pan">
							International Id
						</option>
						<option value="mob">
							Mobile No.
						</option>
						<option value="email">
							Email
						</option>
					</select>
				</div>
				<div class="one-third">
					<span>Fill the value</span>
					<input type="text" value=""/>
					<!--<button onclick="info">Submit
					</button>-->
					<button onclick="loadInfoTest(this)">Submit
					</button>
				</div>
			</div>
			<div>
			<div id="chart"></div>
    <script type="text/javascript">
    	
    		//console.log(accInformation,"In draw Tree");
    		var m = [20, 120, 20, 120],
		   // w = 1280 - m[1] - m[3],
		    w = 1260 - m[1] - m[3],
		    h = 800 - m[0] - m[2],
		    i = 0,
		    duration = 500,
		    root;

		var tree = d3.layout.tree()
		    .size([h, w]);

		   

		var diagonal = d3.svg.diagonal()
		    .projection(function(d) { return [d.y, d.x]; });

		//var vis = d3.select("#chart").append("svg")
		var vis = d3.select("#chart").append("svg").attr("id","descOfCust")
		    //.attr("width", w + m[1] + m[3])
		    .attr("width", "50%")
		    .attr("height", h + m[0] + m[2])
		    .attr("preserveAspectRatio","xMidYMin")		    
		  .append("g")
		    .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

		/*var vis = d3.select("#chart").append("svg")
		    .attr("width", w + m[1] + m[3])
		    .attr("height", h + m[0] + m[2])
		  .append("g")
		    .attr("transform", "translate(" + m[3] + "," + m[0] + ")");
		*/
		function drawTree(accInformation){		
			console.log(accInformation,"accInformation");
		  root = accInformation;
		  console.log(root,"root");
		  root.x0 = h / 2;
		  root.y0 = 0;

		  function collapse(d) {
		    if (d.children) {
		      d._children = d.children;
		      //d._children.forEach(collapse);
		      d.children = null;
		    }
		  }
		  if(root.status != "failed"){
		  //root.children.forEach(collapse);
		  	update(root);
		  }
		  else{
		  	displayNotification(root.reason,true);
		  }
		//});
    	}
    	
		function update(source) {


		  // Compute the new tree layout.
		  var nodes = tree.nodes(root).reverse();

		  // Normalize for fixed-depth.
		  //nodes.forEach(function(d) { d.y = d.depth * 180; });
		  nodes.forEach(function(d) { d.y = d.depth * 180; });

		  // Update the nodes…
		  var node = vis.selectAll("g.node")
		      .data(nodes, function(d) { return d.id || (d.id = ++i); });

		  // Enter any new nodes at the parent's previous position.
		  var nodeEnter = node.enter().append("g")
		      .attr("class", "node")
		      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
		      .on("click", click);

		  nodeEnter.append("circle")
		      .attr("r", 1e-6)
		      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
		      //.style("fill", function(d) { return d._children ? "steelblue" : "#fff"; });
		      

		       

		      /*nodeEnter.append("div")
		      //.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
		      .style("fill", function(d) { return d._children ? "steelblue" : "#fff"; });*/

				 nodeEnter.append("div")
		      //.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
		      //.style("fill", function(d) { return d._children ? "steelblue" : "#000"; })
		      .text(function(d){
		      	return d.Acc;
		      });

		       /*nodeEnter.append("text")
		      .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
		      .attr("dy", ".35em")
		      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
		      .text(function(d) { 
		      	//console.log(d,"d");
		      	return d.Acc; })
				//return d.Acc+document.write("\n");d.name; })
		      .style("fill-opacity", 1e-6)
		      .on("click",description);*/

		      //nodeEnter.append("text")
		      nodeEnter.append("text")
		      .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
		      .attr("dy", ".35em")
		      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
		      .attr("class","container")
		      .text(function(d) { 
		      	//console.log(d,"d");
		      	return d.Acc; })
				//return d.Acc+document.write("\n");d.name; })
		      .style("fill-opacity", 1e-6)
		      .on("click",description);


		      /*nodeEnter.append("text")
		      .text(function(d) { return d.name; });*/

		       /*nodeEnter.append("text")
		      .text(function(d) { return d.name; })
		      .on("click",wakeMe);*/

		  // Transition nodes to their new position.
		  var nodeUpdate = node.transition()
		      .duration(duration)
		      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

		  nodeUpdate.select("circle")
		      .attr("r", 4.5)
		      //.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
		      .style("fill", function(d) { return d._children ? "steelblue" : "#fff"; });

		  nodeUpdate.select("text")
		      .style("fill-opacity", 1);

		  // Transition exiting nodes to the parent's new position.
		  var nodeExit = node.exit().transition()
		      .duration(duration)
		      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
		      .remove();

		  nodeExit.select("circle")
		      .attr("r", 1e-6);

		  nodeExit.select("text")
		      .style("fill-opacity", 1e-6);

		 	    

		  // Update the links…
		  var link = vis.selectAll("path.link")
		      .data(tree.links(nodes), function(d) { return d.target.id; });

		  // Enter any new links at the parent's previous position.
		  link.enter().insert("path", "g")
		      .attr("class", "link")
		      .attr("d", function(d) {
		        var o = {x: source.x0, y: source.y0};
		        return diagonal({source: o, target: o});
		      })
		    .transition()
		      .duration(duration)
		      .attr("d", diagonal);

		  // Transition links to their new position.
		  link.transition()
		      .duration(duration)
		      .attr("d", diagonal);

		  // Transition exiting nodes to the parent's new position.
		  link.exit().transition()
		      .duration(duration)
		      .attr("d", function(d) {
		        var o = {x: source.x, y: source.y};
		        return diagonal({source: o, target: o});
		      })
		      .remove();

		  // Stash the old positions for transition.
		  nodes.forEach(function(d) {
		    d.x0 = d.x;
		    d.y0 = d.y;
		  });
		}

		// Toggle children on click.
		function click(d) {
		  if (d.children) {
		    d._children = d.children;
		    d.children = null;
		  } else {
		    d.children = d._children;
		    d._children = null;
		  }
		  update(d);
		}

    </script>
</div>
		</div>
<div class="hide">
	
		<!--<div class="apple_overlay black" id="detailO" style="height:400px;" >-->
			<div class="apple_overlay black" id="detailO" style="min-height:200px;position:relative;" >
				<a class="destroy" href="#" onclick="closeIt()"></a>
			<!--<div style="height:360px; overflow:auto;" >-->
			<div style="height:auto; overflow:auto;" >
				<div style="border:1px solid #808080; border-radius:0 10px 7px 7px; width:98%; margin:auto; margin-top:10px;" >
					<div class="heading" ><h3>Details </h3></div>
					<!--<div style="width:100%; margin:auto; margin-top:10px; margin-bottom:5px; height:300px;" >-->
						<div style="width:100%; margin:auto; margin-top:10px; margin-bottom:5px; min-height:150px;" >
						<div class="detailsInfo"></div>
						
						
					</div>
				</div>
			</div>
		</div>
		</div>
		<center>
		<div id="canv" style="margin:8px 0 0 0; padding:0;" > 
		<div id="canDiv" >
		</div>
		<div style="margin-top:50px"></div>
		</div>
	</center>
				
	</body>
</html>
