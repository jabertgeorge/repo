package clari5.custom.wlalert;

import clari5.custom.wlalert.dowjone.Records;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PFA")

public class DowJones {



    @XmlElement(name = "Records", required = true)
    protected Records records;



}
