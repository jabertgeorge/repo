/* account.js start */
var oTable;
var acctSrch="";
var acctOutput="";
var customerTab="";
var accountTab="";

acctSrch="<div class='heading' ><h3>Account Search</h3></div>";

acctSrch+="<form id='acctTemplate'><table id='acctListD' cellspacing='2' cellpadding='2'>"
	+"<tr class='separator'><td class='fontSize'>Customer ID</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' value='' class='validate[groupRequiredHighlight]' id='custName' /></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Account ID</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' value='' class='' id='accNum' /></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Branch ID</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' class='' id='brchId'></td></tr>"
	+"<tr class='separator'><td class='fontSize'></tr>"
	+"<tr class='separator'><td class='fontSize'><a class='abutton search-w-def' onClick='loadAcctData(this)' style='margin-left:50px;'>Search</a></td></tr></table></form>";

acctOutput="<div id='acctSrchLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\"><h3>Account Details</h3></div><div id='tabAccount' style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id='acctSrchDtls' class='dataTableCss' style='text-align:left;' ></table></div></div>";

var acctTreeData="";

function initAcct()
{
	var userId = top.user;
	var serverUrl = top.serverUrl;
	setParams(userId, serverUrl);
	$("#acctListD").show();
	$("#acctSrchLink").hide();
document.onkeypress = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
	closeAcctEvidence(acctCount);
    }
};
}

var treeObj="";
var custName=null;
var schType=null;
var brchId=null;
var accNum=null;
function loadAcctData(custId)
{
        custName=($("#custName").val()!="")?$("#custName").val().trim():null;
        brchId=($("#brchId").val()!="")?$("#brchId").val().trim():null;
        accNum=($("#accNum").val()!="")?$("#accNum").val().trim():null;
    if(acctValidation()){
		var acctObject={
    	    "branchId": brchId,
    	    "custId": custName,
    	    "acctId": accNum
    	};
	getAcctDtlsOnValidate(acctObject);
	if($("#custName").val()!=""){
        treeObj={
             "custId":custName
             };
	}else if(custId!=""){
	}
	}
}

function getAcctDtlsOnValidate(acctObject)
{
	var ifFormFilled = false;
	if ( custName != null)
		ifFormFilled = true;
	if ( accNum != null)
		ifFormFilled = true;
	if (ifFormFilled) {
		$("#custName").removeClass("validate[groupRequiredHighlight]");
		$("#custName").addClass("validate[custom[oneSpcLetterNumber]]");
		$("#accNum").addClass("validate[custom[oneSpcLetterNumber]]");
		var isAcctCond = $("#acctTemplate").validationEngine('validate', {scroll: false});
		if(isAcctCond){
	    		getAccountDetails(acctObject,accountRespHandler,true);
		}
	} else {
		$("#custName").removeClass("validate[custom[oneSpcLetterNumber]]");
		$("#accNum").removeClass("validate[custom[oneSpcLetterNumber]]");
		$("#custName").addClass("validate[groupRequiredHighlight]");
		$("#acctTemplate").validationEngine('validate', {scroll: false});
	}
}

function acctValidation(){
	if(custName==null  && accNum==null){
		$("#custName").addClass("hightlightDiv");
		$("#accNum").addClass("hightlightDiv");
		return true;
	}
	else{
		$("#custName").removeClass("hightlightDiv");
		$("#accNum").removeClass("hightlightDiv");
		return true;
	}
}

function accountRespHandler(resp)
{
        var response=resp.accountLists;
        if(response=="" || response==undefined){
	   displayNotification("Account ID doesn't exist.", true, "warning");
        }else{
            clearTabs();
	    addTab('Account','');
	    $("#acctSrchLink").show();
   	    var resList = makeJsonForDatatable(response, "addTab","acctId", reqstPage);
            displayData(resList,"tabAccount","acctSrchDtls");
	    toggleSrchWindow();
        }
}

var acctCount="";
var inputObj="";
var inputObject="";
var accountID="";
function acctLoad(acctId,tabCount){
	accountID=acctId;
	 $("#menuBlock").hide();
	 acctCount=tabCount;
	 inputObject={
            "acctId":acctId
	 };
	 if($("#refId").val()!=""){
	}
	getAccountHardFacts(inputObject,acctHFactsRespHandler,true);
}

function acctHFactsRespHandler(resp){
    var  cxacctcid ="";
    var respObj=resp.accountDetails;
    if(respObj!=null && respObj!=undefined && respObj!=""){
    $("#acctIdVal_"+acctCount).append("<a oncontextmenu='return false;' href='#' style='color:#7070FF;' onclick=\"addTab('Customer','"+respObj[0].custId+"')\" >"+respObj[0].custId+"</a>");
    $("#accName_"+acctCount).text(respObj[0].name!=""?respObj[0].name:"NA");
    $("#schType_"+acctCount).text(respObj[0].schemeType!=""?respObj[0].schemeType:"NA");
    $("#acctStat_"+acctCount).text(respObj[0].acctStatus!=""?respObj[0].acctStatus:"NA");
    $("#acctCur_"+acctCount).text(respObj[0].currency!=""?respObj[0].currency:"NA");
    $("#schCode_"+acctCount).text(respObj[0].schemeCode!=""?respObj[0].schemeCode:"NA");
    $("#acctOpnDt_"+acctCount).text(respObj[0].openedDate!=""?respObj[0].openedDate:"NA");
        	if(cxacctcid!=""){
	     treeObj={
             "custId":cxacctcid
             };
		}else{
		cxacctcid=respObj[0].custId;
	     treeObj={
             "custId":respObj[0].custId
             };
		}
	getCustomTreeDetail(treeObj,custTreeRespHandler,true);
	getAccountSoftFacts(inputObject,acctSFactsRespHandler,true);
    }else{
		clearTabs();
		displayNotification("Please enter correct Account Number.", true, "warning");
		return;
	}}

function custTreeRespHandler(resp) {
    createCustomerRlm(resp);
    loadJSFile("caseTree.js");
}

function acctSFactsRespHandler(resp){
    var response=resp.accountFactList;
    if(response=="" || response==undefined){
       var dataResp = [ ];
       var myArray =new Array();
       var tableHeaders = new Array();
       tableHeaders.push({ "sTitle": "Intelligence" });
       tableHeaders.push({ "sTitle": "Value" });
       tableHeaders.push({ "sTitle": "Updated On" });
       dataResp.push(myArray);
       dataResp.push(tableHeaders);
       displayData(dataResp,"tabAcctSft"+acctCount,"softFactDtls"+acctCount);
    }else{
   	    var resList = makeJsonForDatatable(response,"showAcctEvidence","factname",accountID);
        displayData(resList,"tabAcctSft"+acctCount,"softFactDtls"+acctCount);
    }
}

function showAcctEvidence(aid,fname){
	var evdObj={
		    "acctId":aid,
		    "factName":fname.split("|")[0],
		     "source":fname.split("|")[1]
		   };
    if (evdObj.source == "RISK" || evdObj.source == "RDE" || evdObj.source == "MSTR")
        displayNotification("The evidence doesn't exist for this FACT NAME...");
    else {
	    getAcctEvidenceDetails(evdObj,acctEvidenceRespHandler,true);
	    $("#evidenceDtls"+(parseInt(tabPosition)+1)).show();
	}

}
function acctEvidenceRespHandler(resp){
    var response=resp.accountEvidence;
    resetAcctEvidenceBox();
    if(response!="" || response!=undefined){
	if(response.qualifier!="" && response.qualifier!=undefined){
	var dataObj=makeJsonForDatatable(response.qualifier,"","","");
	//$('#acctevdLink "+acctCount+"_1').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
	$('#acctevdLink'+acctCount+'_1').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
	setAcctEvidenceHdr(acctCount,"<h3>Qualifier</h3>");
	//displayData(dataObj,"acctEvidBlock"+acctCount+'_1',"acctEvidence"+acctCount);
	displayData(dataObj,"acctEvidBlock"+acctCount+'_1',"acctEvidence"+acctCount);
	}

	if(response.pre!="" && response.pre!=undefined){
	var wldataText=paintAWLTable(response.pre);
	$("#preData"+(acctCount+1)).append(wldataText);
	var jsonRslts=prepareAcctPreCondJson(response.pre)
	var dataObj=makeJsonForDatatable(jsonRslts.resultsList,"","","");
	dataObj[1]=returnAcctHeaders(response.pre);
	//$('#acctevdLink +"+acctCount+"_2').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
	$('#acctevdLink' +acctCount+'_2').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
	setAcctEvidenceHdr((acctCount+1),"<h3>Pre-Condition</h3>");
	displayData(dataObj,"acctEvidBlock"+(acctCount+1)+'_2',"acctEvidence"+(acctCount+1));
	}

	if(response.scorecard!="" && response.scorecard!=undefined){
	var dataObj=makeJsonForDatatable(response.scorecard,"","","");
	//$('#acctevdLink "+acctCount+"_3').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px         7px;width:100%;display:block;");
	$('#acctevdLink'+acctCount+'_3').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px         7px;width:100%;display:block;");
	setAcctEvidenceHdr((acctCount+2),"<h3>ScoreCard</h3>");
	displayData(dataObj,"acctEvidBlock"+(acctCount+2)+'_3',"acctEvidence"+(acctCount+2));
	}

	if(response.post!="" && response.post!=undefined){
	var wldataText=paintAWLTable(response.post);
	$("#preData"+(acctCount+3)).append(wldataText);
	var jsonRslts=prepareAcctPostCondJson(response.post);
	var dataObj=makeJsonForDatatable(jsonRslts.resultsList,"","","");
	//$('#acctevdLink "+acctCount+"_4').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
	$('#acctevdLink'+acctCount+'_4').attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
	dataObj[1]=returnAcctHeaders(response.post);
	setAcctEvidenceHdr((acctCount+3),"<h3>Post-Condition</h3>");
	displayData(dataObj,"acctEvidBlock"+(acctCount+3)+'_4',"acctEvidence"+(acctCount+3));
	}
	for(var t=0;t<response.length;t++){
		var headerText="";
		if(response[t].blockName!="" && response[t].blockName!=undefined){
		headerText=(response[t].blockName=="opinion"?"Evidence For Trigger":(response[t].blockName=="AddOn"?"Evidence For push up":"Evidence Details"));
			if(response[t].events!="" && response[t].events!=undefined){
			var dataObj=makeJsonForDatatable(response[t].events,"","","");
				if(headerText!=""){
                    setAcctEvidenceHdr((acctCount+'_'+(t+1)), (acctCount+t),"<h3>"+headerText+"</h3>");
					$('#acctevdLink'+acctCount+'_'+(t+1)).attr("style","margin:16px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:block;");
				}
			displayData(dataObj,"acctEvidBlock"+(acctCount+'_'+(t+1)),"acctEvidence"+(acctCount+t));
			}
	   	}
	}
   }
   if(response && response.length > 0 && response[0].blockName=="opinion" && (response[0].events=="" || response[0].events==undefined)){
     displayNotification("The evidence doesn't exist for this FACT NAME...");
   }else {
       if(response.length === 0) {
           displayNotification("No data available");
       } else {
       $('#tabNavUl li').map(function(i,n) {
           if($(n).hasClass( "ui-tabs-active" )) {
               var tabPosId = parseInt($(n).attr('id')) +1;
			   $('#acctBox'+ tabPosId).show();
           }
       });
       }
        // $('#acctBox'+acctCount).show();
   }
}

function setAcctEvidenceHdr(accountEvdId, count,text){
	$("#acctevdLink"+accountEvdId+" > #head"+count).empty();
    $("#acctevdLink"+accountEvdId+" > #head"+count).append(text);
}

function returnAcctHeaders(wldata){
	var tableHeaders=new Array();
	tableHeaders.push({ "sTitle": "Overall Score" });
	tableHeaders.push({ "sTitle": "Doc ID" });
	var fieldss=wldata.fields+"";
	var fieldsQry=fieldss.split(",");
	for(var y=0;y<fieldsQry.length;y++){
	tableHeaders.push({ "sTitle": fieldsQry[y]});
	tableHeaders.push({ "sTitle": "Score" });
	}
return tableHeaders
}

function resetAcctEvidenceBox(){
	for(var t=1;t<5;t++){
		$('#acctevdLink'+acctCount+'_'+(t)).attr("style","display:none;");
		$("#preData"+t).empty();
	}

}

function prepareAcctPostCondJson(){
var paintPOST="<table border='1' style='margin-top:7px;width:30%;' >";
		paintPOST+="<tr>";
		paintPOST+="<table border='1' style='margin:0px 3px;' ><tr class='fontFmly' style='background-color:#666676;color:#FFF;'><td>Field</td><td>Risk</td></tr><tr class='fontFmly'><td>"+wldata.field+"</td><td>"+wldata.risklevel+"</td></tr></table></tr>";
	paintPOST+="</table>";
	return paintPOST;
}

function paintAWLTable(wldata){
	var paintSAM="<table border='1' style='margin-top:7px;width:30%;' >";
		paintSAM+="<tr>";
		paintSAM+="<table border='1' style='margin:0px 3px;' ><tr class='fontFmly' style='background-color:#666676;color:#FFF;'><td>Field</td><td>WL Name</td><td>Risk</td></tr><tr class='fontFmly' ><td>"+wldata.field+"</td><td>"+wldata.WlName+"</td><td>"+wldata.risklevel+"</td></tr></table></tr><tr class='fontFmly' style='background-color:#666676;color:#FFF;'>";
		var qry=wldata.qry;
		var fields=wldata.fields+"";
		var fieldsQry=fields.split(",");
		paintSAM+="<table border='1' style='margin:5px 3px;width:99%;' ><tr style='font-family: Verdana,Arial,sans-serif;font-size: 0.79em;white-space:break;background-color:#666676;color:#FFF;'><td>OverallScore</td><td>Doc ID</td>";
		for(var y=0;y<fieldsQry.length;y++){
			paintSAM+="<td>"+fieldsQry[y]+"</td><td>Score</td>";
		}
		paintSAM+="</tr><tr style='font-family: Verdana,Arial,sans-serif;font-size: 0.79em;white-space:break;' ><td>-</td><td>-</td>";
		for(var y=0;y<qry.length;y++){
			if(qry[y]!="")
			paintSAM+="<td>"+qry[y]+"</td><td>-</td>";
			else
			paintSAM+="<td>-</td><td>-</td>";
		}
		paintSAM+="</tr></table>";
		paintSAM+="</tr>";
	paintSAM+="</table>";
	return paintSAM;
}

function prepareAcctPreCondJson(resultson){
	var finalRslts=null;
	var resultsJs={"resultsList" : []};
	var resultJson=resultson.results;
	for(var r=0;r<resultJson.length;r++){
		var results = {};
		var innerJson="{";
	innerJson+="\"OverallScore\""+":\""+resultJson[r].score+"\","+"\"docid\""+":\""+resultJson[r].id+"\",";
		var matchJson=resultJson[r].matches;
		for(var z=1;z<(matchJson.length+1);z++){
			if(z==1)
			innerJson+="\"val"+z+"\":\""+matchJson[0].val+"\","+"\"score"+z+"\":\""+matchJson[0].score+"\"";
			else
			innerJson+=",\"val"+z+"\":\""+matchJson[0].val+"\","+"\"score"+z+"\":\""+matchJson[0].score+"\"";
		}
		innerJson+="}";
		resultsJs.resultsList.push(JSON.parse(innerJson));
	}
	return resultsJs;
}


function paintAcctSAMTable(samdata){

	var paintSAM="<table border='1' style='margin-top:14px;width:99%;' >";
	for(var t=0;t<samdata.length;t++){
		if(t==0){
		paintSAM+="</tr><tr class='fontFmly' style='background-color:#666676;color:#FFF;'>";
		for (key in samdata[t]) {
		paintSAM+="<td>"+key.trim()+"</td>";
		}
		paintSAM+="</tr><tr class='fontFmly odd' >";
		for (key in samdata[t]) {
		paintSAM+="<td>"+samdata[t][key].trim()+"</td>";
		}
		}else{
			if(t%2==0){
			paintSAM+="<tr class='fontFmly odd' >";
			}else{
			paintSAM+="<tr class='fontFmly even' >";
			}
		for (key in samdata[t]) {
		paintSAM+="<td>"+samdata[t][key].trim()+"</td>";
		}
		}
		paintSAM+="</tr>";
	}
	paintSAM+="</table>";
	return paintSAM;
}

var tranObj="";
function loadTranDtls(){
	var frDt="";
	var toDt="";
	var fromDate ="";
	var toDate ="";
	if($("#fromDate"+acctCount).val()!="" && $("#toDate"+acctCount).val()!=""){
	frDt =($("#fromDate"+acctCount).val()).split("/").reverse().join("-")
	toDt =($("#toDate"+acctCount).val()).split("/").reverse().join("-")
	}
	fromDate=(frDt!="")?frDt:null;
	toDate=(toDt!="")?toDt:null;
	tranObj={
                  "tranDateMin": fromDate,
                  "tranDateMax": toDate,
		  "acctId":accountID
		};



	var downloadUrl = transeiveDownloadS("itool","download_account_transaction_details",JSON.stringify(tranObj));
	$("#downloadTag").attr("download","TransactionDetails.xls");
	$("#downloadTag").attr("style","display: inline-block;float:right;margin:-21px 15px 0px 0px;");
	$("#downloadTag").attr("href",downloadUrl);
	getAccountTranDetails(tranObj,acctTranRespHandler,true);
}

var tranResponse="";
function acctTranRespHandler(resp){
    tranResponse=resp.accountTransactionDetails;
	$("#tableCum1").hide();
	$("#tableCum2").hide();
	$("#tranTable").attr("style","height:40px; margin-bottom:10px;");
    if(tranResponse=="" || tranResponse==undefined){
        var dataResp = [ ];
        var myArray =new Array();
        var tableHeaders = new Array();
        tableHeaders.push({ "sTitle": "TranId" });
        tableHeaders.push({ "sTitle": "TranAmount" });
        tableHeaders.push({ "sTitle": "TranDate" });
        tableHeaders.push({ "sTitle": "TranType" });
        tableHeaders.push({ "sTitle": "Debit/Credit" });
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp,"tabTranAcct"+acctCount,"acctTranDtls"+acctCount);
    }else{
	paintCumulativeTable(resp);
	var resList = makeJsonForDatatable(tranResponse, "","","Transaction");
        displayData(resList,"tabTranAcct"+acctCount,"acctTranDtls"+acctCount);
    }
}

function paintCumulativeTable(response){
	$("#tableCum1").show();
	$("#tableCum2").show();
	$("#tranTable").attr("style","height:70px; margin-bottom:10px;");
	$("#cc").html(response.cumulativeCr);
	$("#cd").html(response.cumulativeDr);
	$("#ccc").html(response.cumulativeCashCr);
	$("#ccd").html(response.cumulativeCashDr);
}

function getAcctTreeData(acount,id){
 acctTreeData="";
    custRmlId="customerRelam"+acount;
    acctTreeData+="<div id=\"acctTree"+acount+"\" class='treeLeftPanel' >";
acctTreeData+="<div id=\"acctContainer"+acount+"\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div class='treeRightPanel' >";
acctTreeData+="<div>";
acctTreeData+="<button onclick='goBack(id)' class='abutton back-w-def goBackCls' id="+id+" style='cursor:default;opacity:1;'>Back</button>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"acctLink"+acount+"\" style='width:100%'>";
acctTreeData+="<div class='heading' >";
acctTreeData+="<h3>Account Details</h3>";
acctTreeData+="</div>";
acctTreeData+="<table class='attrHTbl' style='height:80px;' cellspacing='2' cellpadding='2'>";
acctTreeData+="<tr>";
acctTreeData+="<td>Customer ID</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"acctIdVal_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="<td  style='border-left:1px solid #c0c0c0;' >Account Name</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"accName_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="<td style='border-left:1px solid #c0c0c0;' >Scheme Type</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"schType_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="</tr>";
acctTreeData+="<tr>";
acctTreeData+="<td>Account Currency</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"acctCur_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="<td style='border-left:1px solid #c0c0c0;' >Account Open Date</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"acctOpnDt_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="<td style='border-left:1px solid #c0c0c0;'>Account Status</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"acctStat_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="</tr>";
acctTreeData+="<tr>";
acctTreeData+="<td>Scheme Code</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id=\"schCode_"+acount+"\">";
acctTreeData+="</td>";
acctTreeData+="</tr>";
acctTreeData+="</table>";
acctTreeData+="</div>";

    acctTreeData+="</br>";
acctTreeData+="<div id=\"acctMenu"+acount+"\" style='width:100%;margin-top:-5px;' >";
acctTreeData+="<ul id=\"acctBar"+acount+"\" >";
acctTreeData+="<li>";
acctTreeData+="<a onClick=\"loadAccountSFacts('"+acount+"')\" href=\"#accttab1_"+acount+"\" oncontextmenu='return false;'>Intelligence</a>";
acctTreeData+="</li>";
acctTreeData+="<li>";
acctTreeData+="<a onClick=\"loadAccountTranDtls('"+acount+"')\" href=\"#accttab2_"+acount+"\" oncontextmenu='return false;'>Transactions</a>";
acctTreeData+="</li>";
acctTreeData+="</ul>";

acctTreeData+="<div id=\"accttab1_"+acount+"\" style='width:100%;height:66%;background:#FFFFFF;overflow-y:auto;' >";
acctTreeData+="<div class='overlay' id=\"acctOverlay"+acount+"\" style='display:none;'>";
acctTreeData+="</div>";
acctTreeData+="<div class='boxTab' id=\"acctBox"+acount+"\" style='border:4px solid #000;' >";
acctTreeData+="<a class='boxclose' id=\"boxclose"+acount+"\" onclick=\"closeAcctEvidence('"+acount+"')\" href=# >";
acctTreeData+="</a>";
acctTreeData+="<br/>";
acctTreeData+="<div id=\"evidenceDtls"+(acount)+"\" style='height:282px;margin:-37px -24px -26px -24px;overflow:auto;' >";
acctTreeData+="<div id='acctevdLink"+acount+"_1' style=\"margin:3px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
acctTreeData+="<div class='heading' id=\"head"+acount+"\">";
acctTreeData+="<h3>Evidence Details</h3>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"preData"+acount+"\" >";
acctTreeData+="</div>";
acctTreeData+="<div id=\"acctEvidBlock"+acount+"_1\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
acctTreeData+="<table id=\"acctEvidence"+acount+"\" class='dataTableCss' style='text-align:left;'>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div id='acctevdLink"+acount+"_2' style=\"margin:3px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
acctTreeData+="<div class='heading' id=\"head"+(acount+1)+"\">";
acctTreeData+="<h3>Evidence Details</h3>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"preData"+(acount+1)+"\" >";
acctTreeData+="</div>";
acctTreeData+="<div id=\"acctEvidBlock"+(acount)+"_2\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
acctTreeData+="<table id=\"acctEvidence"+(acount+1)+"\" class='dataTableCss' style='text-align:left;'>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div id='acctevdLink"+acount+"_3' style=\"margin:3px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
acctTreeData+="<div class='heading' id=\"head"+(acount+2)+"\">";
acctTreeData+="<h3>Evidence Details</h3>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"preData"+(acount+2)+"\" >";
acctTreeData+="</div>";
acctTreeData+="<div id=\"acctEvidBlock"+(acount)+"_3\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
acctTreeData+="<table id=\"acctEvidence"+(acount+2)+"\" class='dataTableCss' style='text-align:left;'>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div id='acctevdLink"+acount+"_4' style=\"margin:3px 0px 0px 0px; border:1px solid #808080; border-radius:0px 10px 7px 7px;width:100%;display:none;\">";
acctTreeData+="<div class='heading' id=\"head"+(acount+3)+"\">";
acctTreeData+="<h3>Evidence Details</h3>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"preData"+(acount+3)+"\" >";
acctTreeData+="</div>";
acctTreeData+="<div id=\"acctEvidBlock"+(acount)+"_4\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
acctTreeData+="<table id=\"acctEvidence"+(acount+3)+"\" class='dataTableCss' style='text-align:left;'>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div id='softFactLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\">";
acctTreeData+="<div class=\"heading\" >";
acctTreeData+="<h3>Intelligence</h3>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"tabAcctSft"+acount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
acctTreeData+="<table id=\"softFactDtls"+acount+"\" class='dataTableCss' style='text-align:left;'>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"accttab2_"+acount+"\" style='width:100%;height:66%;display:none;background:#FFFFFF;overflow-y:auto;' >";
acctTreeData+="<div id='acctTranLink' >";
acctTreeData+="<table class='attrHTbl' id='tranTable' style='height:40px; margin-bottom:10px;'>";
acctTreeData+="<tr>";
acctTreeData+="<td>From Date</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td>";
acctTreeData+="<input type='text' id=\"fromDate"+acount+"\" >";
acctTreeData+="</td>";
acctTreeData+="<td>To Date</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td>";
acctTreeData+="<input type='text' id=\"toDate"+acount+"\" >";
acctTreeData+="</td>";
acctTreeData+="</tr>";
acctTreeData+="<tr id='tableCum1' style='display:none;'>";
acctTreeData+="<td>Cumulative Credit</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id='cc'>";
acctTreeData+="</td>";
acctTreeData+="<td>Cumulative Debit</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id='cd' >";
acctTreeData+="</td>";
acctTreeData+="</tr>";
acctTreeData+="<tr id='tableCum2' style='display:none;' >";
acctTreeData+="<td>Cumulative Cash Credit</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id='ccc' >";
acctTreeData+="</td>";
acctTreeData+="<td>Cumulative Cash Debit</td>";
acctTreeData+="<td>:</td>";
acctTreeData+="<td id='ccd' >";
acctTreeData+="</td>";
acctTreeData+="</tr>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="<div style='text-align:right;' >";
acctTreeData+="<a class='abutton search-w-def' style='color: #FFFFFF !important;' onclick='loadTranDtls()'>Search</a>";
acctTreeData+="</div>";
acctTreeData+="<div style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\">";
acctTreeData+="<div class=\"heading\" >";
acctTreeData+="<h3>Transactions</h3>";
acctTreeData+="<div>";
acctTreeData+="<a download='TransactionDetails' href='javascript:tranDownload()' style=\"display:none;float:right;margin:-21px 15px 0px 0px;\"  id='downloadTag' class=\"abutton download-w-16-wc\" title=\"Download\">";
acctTreeData+="</a>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="<div id=\"tabTranAcct"+acount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' >";
acctTreeData+="<table id=\"acctTranDtls"+acount+"\" class='dataTableCss' style='text-align:left;'>";
acctTreeData+="</table>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="</div>";
acctTreeData+="</div>";
    return acctTreeData;
}


function closeAcctEvidence(id){
   $('#acctBox'+id).fadeIn('fast',function(){
      	// $('#acctBox'+id).animate({'top':'-303px'},5,function(){
        //     $('#acctOverlay'+id).fadeOut('fast');
        // });

       $('#acctBox'+id).hide();


    });
}

function loadAccountTranDtls(tabid){
        var dataResp = [ ];
        var myArray =new Array();
        var tableHeaders = new Array();
        tableHeaders.push({ "sTitle": "TranId" });
        tableHeaders.push({ "sTitle": "TranAmount" });
        tableHeaders.push({ "sTitle": "TranDate" });
        tableHeaders.push({ "sTitle": "TranType" });
        tableHeaders.push({ "sTitle": "Debit/Credit" });
        dataResp.push(myArray);
        dataResp.push(tableHeaders);
        displayData(dataResp,"tabTranAcct"+acctCount,"acctTranDtls"+acctCount);
}

function loadAccountSFacts(tabid){
	acctCount=tabid;
	var sobj={
            "acctId":accountID
	 };
	getAccountSoftFacts(sobj,acctSFactsRespHandler,true);
}

function validateFormFields(formName)
{
	return $("#"+formName).validationEngine('validate');
}
/* account.js end */
