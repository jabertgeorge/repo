clari5.custom.wlalert.db {
        entity {
                wlrecord-details {
                     attributes = [
                                { name :id ,                   type :"string:50", key=true}
                                { name :parameter-type,        type : "string:30",   key=false}
                                { name :parameter-value,       type : "string:100",  key=false}
                                { name :list-name,            type : "string:100",  key=false}
                                { name :status,                type : "string:2",  key=false}
                                { name :remarks,               type : "string:100",  key=false}
                                { name :created-on,            type : timestamp}
                                { name :updated-on,             type : timestamp}
                                ]
                     criteria-query {
                          name: WlRecordDeatails
                          summary = [id,list-name,parameter-type,parameter-value,status,remarks,created-on,updated-on]
                          where {
                               status: equal-clause
                          }
                          order = ["created-on"]
                     }


                }
        }
}
