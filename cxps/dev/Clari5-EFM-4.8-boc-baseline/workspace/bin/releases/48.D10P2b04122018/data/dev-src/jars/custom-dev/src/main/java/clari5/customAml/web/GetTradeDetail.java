package clari5.customAml.web;

import java.sql.*;
import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetTradeDetail {
    CxpsLogger logger = CxpsLogger.getLogger(GetTradeDetail.class);

    private static String faciltiyNum="";
    JSONObject obj = new JSONObject();
    JSONObject obj1 = new JSONObject();

    GetTradeDetail(String payload){
        this.faciltiyNum="";
        JSONObject jsonObject = new JSONObject(payload);
        String facNum = jsonObject.getString("faciltiyNum");
        String[] parts = facNum.split("--");
        this.faciltiyNum=parts[0];
    }

    public synchronized String getTradeDetails(){
        String tradeJarray=null;
        JSONObject obj = new JSONObject();
        String tradeDetails=null;
        try {
            logger.debug("getTradeDetails:faciltiyNum:"+this.faciltiyNum);
            if ((this.faciltiyNum) == null) {
                logger.error("Trying to fetch trade details. faciltiyNum not found");
                return null;
            }
            tradeDetails=getDetails(this.faciltiyNum);
            logger.debug("response custEmpTree:"+obj.toString());
            return tradeDetails;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tradeDetails;
    }

    protected String getDetails(String faciltiyNum){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            String sql="select * from TF_MASTER where facilty_num=? ";
            logger.debug("query to get TradeDetails : "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,faciltiyNum);
            ResultSet rs = stmt.executeQuery();
            while( rs.next() ){
                obj.put("custId", rs.getString("cxCifID")!= null ? rs.getString("cxCifID") : "");
                obj.put("core_accountId", rs.getString("CORE_ACCTID")!= null ? rs.getString("CORE_ACCTID") : "");
                obj.put("currency", rs.getString("Currency")!= null ? rs.getString("Currency") : "");
                obj.put("open_date", rs.getString("FACILTY_Open_Date")!= null ? rs.getString("FACILTY_Open_Date") : "");
                obj.put("expiry_date",rs.getString("FACILTY_expiry_Date")!= null ? rs.getString("FACILTY_expiry_Date") : "");
                obj.put("status", rs.getString("STATUS")!= null ? rs.getString("STATUS") : "");
                jarray.put(obj);
            }
            obj1.put("tradeDetails", jarray);
            logger.debug("getDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting trade details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }
}