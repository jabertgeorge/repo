package clari5.custom.wlalert;

import clari5.custom.wlalert.db.WlRecordDetail;
import clari5.custom.wlalert.db.WlrecordDetails;
import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import cxps.apex.utils.CxpsLogger;

import java.io.IOException;
import java.net.URLEncoder;


public class WlRequestPayload {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlRequestPayload.class);
    WlrecordDetails wlrecordDetails;

    WlRecord wlRecord;


    public CxJson setWlReqValues(WlrecordDetails wlrecordDetails, WlRecord wlRecord ) {

        this.wlrecordDetails = wlrecordDetails;
        this.wlRecord=wlRecord;

        CxJson wlRes = createWlRes(wlrecordDetails.getParameterType());
        if (wlRes == null) return null;

        logger.info("WL Request " + wlRes.toString());
        return wlRes;
    }


    private String getRulesName(String value) {

        switch (value) {
            case "name":
                return "NAME_MATCH_CUSTOMER";
            case "passport":
                return "PASSPORT_MATCH_CUSTOMER";
            case "nic":
                return "NIC_MATCH_CUSTOMER";
        }

        return "";

    }

    private CxJson createWlRes(String value) {
        CxJson cxJson = null;
        String ruleName = getRulesName(value);
        wlRecord.setRulename(ruleName);

        StringBuilder buildReq = new StringBuilder("{\"ruleName\":\"" + ruleName + "\",");
        buildReq.append("\"fields\":[{\"name\":\" " + wlrecordDetails.getParameterType() + "\",\"value\" :\"" + wlrecordDetails.getParameterValue() + "\"}]}");
        try {
            cxJson = CxJson.parse(buildReq.toString());
        } catch (IOException io) {
            logger.error(" [WlRequestPayload.setWlReqValues ] Failed to parse json " + buildReq.toString() + "\n Message " + io.getMessage());
        }

        return cxJson;
    }




    public Matches sendWlRequest(CxJson obj) {

        Matches matches = new Matches();

        logger.info("Request URL " + System.getenv("LOCAL_DN") + "/efm/wlsearch?q=" + obj.toString());
        String response = "";

        try {
            String url = System.getenv("LOCAL_DN") + "/efm/wlsearch?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
            String instanceId = System.getenv("INSTANCEID");
            String appSecret = System.getenv("APPSECRET");

            HttpResponse resp;
            resp = CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").basicAuth(instanceId, appSecret).asString();
            int status = resp.getStatus();
            matches.setResponseCode(resp.getStatus());
            if(status!= 200){
                matches.setRemarks("status of request [" + resp.getStatus()+"] and reason is ["+resp.getStatusText()+"]");
            }


            response = (String) resp.getBody();
            matches.setMatchResult(response);
            logger.info("WL Respose data " + response);
            return matches;
        } catch (UnirestException e) {
            matches.setRemarks("Failed to send request to wl ");
            e.printStackTrace();

        } catch (Exception e) {
            matches.setRemarks("Wl requset failed ");

            logger.error("Error While getting the matched data from WL" + e.getMessage() + "\n Cause " + e.getCause());
        }
        return null;
    }
}