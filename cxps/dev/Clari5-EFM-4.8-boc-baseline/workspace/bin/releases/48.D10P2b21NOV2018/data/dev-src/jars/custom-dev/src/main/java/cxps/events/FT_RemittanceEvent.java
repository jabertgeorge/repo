// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_REMITTANCE", Schema="rice")
public class FT_RemittanceEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=100) public String remBankCode;
       @Field(size=100) public String year;
       @Field(size=100) public String channel;
       @Field(size=100) public String benCity;
       @Field(size=100) public String branch;
       @Field(size=100) public String benCntryCode;
       @Field(size=100) public String agentCode;
       @Field(size=100) public String lcyCurr;
       @Field(size=100) public String remName;
       @Field(size=100) public String benBic;
       @Field(size=100) public String hostUserId;
       @Field(size=100) public String hostId;
       @Field(size=11) public Double tranAmt;
       @Field(size=100) public String txnCode;
       @Field(size=100) public String purposDesc;
       @Field(size=100) public String benfBank;
       @Field(size=100) public String vesselName;
       @Field(size=100) public String benAcctNo;
       @Field(size=100) public String benCustId;
       @Field(size=100) public String system;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String userTyp;
       @Field public java.sql.Timestamp createdOn;
       @Field(size=100) public String eventName;
       @Field(size=20) public String systemCountry;
       @Field(size=100) public String remType;
       @Field(size=100) public String flag;
       @Field public java.sql.Timestamp trnDate;
       @Field(size=100) public String swiftTxn;
       @Field(size=100) public String remBank;
       @Field(size=100) public String typeOfGoods;
       @Field(size=100) public String tempRefNo;
       @Field(size=100) public String swiftMsgType;
       @Field(size=37) public Double fcyTranAmt;
       @Field(size=20) public String cxAcctId;
       @Field(size=11) public Double facilityNum;
       @Field(size=100) public String acctName;
       @Field(size=100) public String addEntityId2;
       @Field(size=100) public String remCustId;
       @Field(size=100) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=100) public String addEntityId3;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=20) public String partTranSrlNum;
       @Field(size=100) public String cptyAcctNo;
       @Field public java.sql.Timestamp eventts;
       @Field(size=100) public String isrtgs;
       @Field(size=11) public Double usdEqvAmt;
       @Field(size=100) public String bankCode;
       @Field(size=11) public Double sno;
       @Field(size=100) public String tranCurr;
       @Field(size=100) public String remCity;
       @Field(size=100) public String benfIdType;
       @Field public java.sql.Timestamp updatedOn;
       @Field(size=100) public String acctType;
       @Field(size=100) public String source;
       @Field(size=11) public Double vesselNumber;
       @Field(size=11) public Double lcyAmt;
       @Field(size=100) public String agentName;
       @Field(size=100) public String accountId;
       @Field(size=100) public String swiftCode;
       @Field(size=100) public String jointAcctName;
       @Field(size=20) public String channelType;
       @Field(size=100) public String acctNoWsKey;
       @Field(size=100) public String remBnkBranch;
       @Field(size=100) public String port;
       @Field(size=100) public String acctCurr;
       @Field(size=100) public String remBic;
       @Field(size=100) public String clientAccNo;
       @Field(size=11) public Double coreCustId;
       @Field(size=10) public String fcyTranCur;
       @Field(size=100) public String remAdd1;
       @Field(size=100) public String remAdd3;
       @Field(size=20) public String keys;
       @Field(size=100) public String remAdd2;
       @Field(size=100) public String tranId;
       @Field(size=100) public String benAdd3;
       @Field(size=11) public Double coreAcctId;
       @Field(size=11) public Double acctCategory;
       @Field(size=100) public String benAdd2;
       @Field(size=100) public String remAcctNo;
       @Field(size=100) public String remIdNo;
       @Field(size=100) public String benAdd1;
       @Field(size=100) public String tranRefNo;
       @Field(size=100) public String acctEmail;
       @Field(size=100) public String acctBranch;
       @Field(size=100) public String eventSubtype;
       @Field(size=100) public String txnType;
       @Field(size=100) public String benName;
       @Field(size=100) public String servicCode;
       @Field(size=100) public String custId;
       @Field(size=100) public String purposCod;
       @Field(size=100) public String remCntryCode;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=100) public String benfBnkBranch;
       @Field(size=100) public String execCust;
       @Field(size=100) public String benfIdNo;
       @Field(size=100) public String acctAddr;


    @JsonIgnore
    public ITable<FT_RemittanceEvent> t = AEF.getITable(this);

	public FT_RemittanceEvent(){}

    public FT_RemittanceEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Remittance");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setRemBankCode(json.getString("REM_BANK_CODE"));
            setYear(json.getString("YEAR"));
            setChannel(json.getString("channel"));
            setBenCity(json.getString("BEN_CITY"));
            setBranch(json.getString("BRANCH"));
            setBenCntryCode(json.getString("BEN_CNTRY_CODE"));
            setAgentCode(json.getString("Agent_Code"));
            setLcyCurr(json.getString("LCY_CURR"));
            setRemName(json.getString("REM_NAME"));
            setBenBic(json.getString("BEN_BIC"));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setTranAmt(EventHelper.toDouble(json.getString("TRAN_AMT")));
            setTxnCode(json.getString("Transaction_Code"));
            setPurposDesc(json.getString("PURPOSE_DESC"));
            setBenfBank(json.getString("BENF_BANK"));
            setVesselName(json.getString("Vessel_Name"));
            setBenAcctNo(json.getString("BEN_ACCT_NO"));
            setBenCustId(json.getString("BEN_CUST_ID"));
            setSystem(json.getString("SYSTEM"));
            setUserTyp(json.getString("User_Type"));
            setCreatedOn(EventHelper.toTimestamp(json.getString("created_on")));
            setEventName(json.getString("event-name"));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));
            setRemType(json.getString("REM_TYPE"));
            setFlag(json.getString("flag"));
            setTrnDate(EventHelper.toTimestamp(json.getString("TRN_DATE")));
            setSwiftTxn(json.getString("Swift_Transaction"));
            setRemBank(json.getString("REMBANK"));
            setTypeOfGoods(json.getString("TYPE_OF_GOODS"));
            setTempRefNo(json.getString("TEMP_REF_NO"));
            setSwiftMsgType(json.getString("Swift_Message_Type"));
            setFcyTranAmt(EventHelper.toDouble(json.getString("fcy-tran-amt")));
            setCxAcctId(json.getString("cx_acct_id"));
            setFacilityNum(EventHelper.toDouble(json.getString("Facility_Number")));
            setAcctName(json.getString("ACCT_NAME"));
            setAddEntityId2(json.getString("reservedfield2"));
            setRemCustId(json.getString("REM_CUST_ID"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("sys_time")));
            setAddEntityId3(json.getString("reservedfield3"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setPartTranSrlNum(json.getString("part_tran_srl_num"));
            setCptyAcctNo(json.getString("CPTY_AC_NO"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setIsrtgs(json.getString("ISRTGS"));
            setUsdEqvAmt(EventHelper.toDouble(json.getString("USD_EQV_AMT")));
            setBankCode(json.getString("BANK_CODE"));
            setSno(EventHelper.toDouble(json.getString("SNO")));
            setTranCurr(json.getString("TRAN_CURR"));
            setRemCity(json.getString("REM_CITY"));
            setBenfIdType(json.getString("BENF_ID_TYPE"));
            setUpdatedOn(EventHelper.toTimestamp(json.getString("updated_on")));
            setAcctType(json.getString("ACCT_TYPE"));
            setSource(json.getString("source"));
            setVesselNumber(EventHelper.toDouble(json.getString("Vessel_Number")));
            setLcyAmt(EventHelper.toDouble(json.getString("LCY_AMOUNT")));
            setAgentName(json.getString("Agent_Name"));
            setAccountId(json.getString("account-id"));
            setSwiftCode(json.getString("Swift_Code"));
            setJointAcctName(json.getString("JOINT_ACCT_NAME"));
            setChannelType(json.getString("channel_type"));
            setRemBnkBranch(json.getString("REM_BANK_BRANCH"));
            setPort(json.getString("Port"));
            setAcctCurr(json.getString("ACCT_CURR"));
            setRemBic(json.getString("REM_BIC"));
            setClientAccNo(json.getString("CLIENT_ACC_NO"));
            setCoreCustId(EventHelper.toDouble(json.getString("CORE_CUST_ID")));
            setFcyTranCur(json.getString("fcy-tran-cur"));
            setRemAdd1(json.getString("REM_ADD1"));
            setRemAdd3(json.getString("REM_ADD3"));
            setKeys(json.getString("keys"));
            setRemAdd2(json.getString("REM_ADD2"));
            setTranId(json.getString("tran_id"));
            setBenAdd3(json.getString("BEN_ADD3"));
            setCoreAcctId(EventHelper.toDouble(json.getString("CORE_ACCOUNT_ID")));
            setAcctCategory(EventHelper.toDouble(json.getString("ACCOUNT_CATAGORY")));
            setBenAdd2(json.getString("BEN_ADD2"));
            setRemAcctNo(json.getString("REM_ACCT_NO"));
            setRemIdNo(json.getString("REM_ID_NO"));
            setBenAdd1(json.getString("BEN_ADD1"));
            setTranRefNo(json.getString("TRAN_REF_NO"));
            setAcctEmail(json.getString("ACCT_EMAIL"));
            setAcctBranch(json.getString("ACCT_BRANCH"));
            setEventSubtype(json.getString("eventsubtype"));
            setTxnType(json.getString("Tran_Type"));
            setBenName(json.getString("BEN_NAME"));
            setServicCode(json.getString("Service_Code"));
            setCustId(json.getString("CUST_ID"));
            setPurposCod(json.getString("PURPOSE_CODE"));
            setRemCntryCode(json.getString("REM_CNTRY_CODE"));
            setTranDate(EventHelper.toTimestamp(json.getString("TRAN_DATE")));
            setBenfBnkBranch(json.getString("BENF_BANK_BRANCH"));
            setExecCust(json.getString("EXEC_CUSTOMER"));
            setBenfIdNo(json.getString("BENF_ID_No"));
            setAcctAddr(json.getString("ACCT_ADDR"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setCxCustId(clari5.custom.dev.PreProcessor.getCustIdForRemmittanceEvent(this));setAcctNoWsKey(cxps.customEventHelper.CustomWsKeyDerivator.accountKeyForRemmitance(getRemType(), getRemAcctNo(),getBenAcctNo()));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getRemBankCode(){ return remBankCode; }

    public String getYear(){ return year; }

    public String getChannel(){ return channel; }

    public String getBenCity(){ return benCity; }

    public String getBranch(){ return branch; }

    public String getBenCntryCode(){ return benCntryCode; }

    public String getAgentCode(){ return agentCode; }

    public String getLcyCurr(){ return lcyCurr; }

    public String getRemName(){ return remName; }

    public String getBenBic(){ return benBic; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public Double getTranAmt(){ return tranAmt; }

    public String getTxnCode(){ return txnCode; }

    public String getPurposDesc(){ return purposDesc; }

    public String getBenfBank(){ return benfBank; }

    public String getVesselName(){ return vesselName; }

    public String getBenAcctNo(){ return benAcctNo; }

    public String getBenCustId(){ return benCustId; }

    public String getSystem(){ return system; }

    public String getUserTyp(){ return userTyp; }

    public java.sql.Timestamp getCreatedOn(){ return createdOn; }

    public String getEventName(){ return eventName; }

    public String getSystemCountry(){ return systemCountry; }

    public String getRemType(){ return remType; }

    public String getFlag(){ return flag; }

    public java.sql.Timestamp getTrnDate(){ return trnDate; }

    public String getSwiftTxn(){ return swiftTxn; }

    public String getRemBank(){ return remBank; }

    public String getTypeOfGoods(){ return typeOfGoods; }

    public String getTempRefNo(){ return tempRefNo; }

    public String getSwiftMsgType(){ return swiftMsgType; }

    public Double getFcyTranAmt(){ return fcyTranAmt; }

    public String getCxAcctId(){ return cxAcctId; }

    public Double getFacilityNum(){ return facilityNum; }

    public String getAcctName(){ return acctName; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getRemCustId(){ return remCustId; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getPartTranSrlNum(){ return partTranSrlNum; }

    public String getCptyAcctNo(){ return cptyAcctNo; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getIsrtgs(){ return isrtgs; }

    public Double getUsdEqvAmt(){ return usdEqvAmt; }

    public String getBankCode(){ return bankCode; }

    public Double getSno(){ return sno; }

    public String getTranCurr(){ return tranCurr; }

    public String getRemCity(){ return remCity; }

    public String getBenfIdType(){ return benfIdType; }

    public java.sql.Timestamp getUpdatedOn(){ return updatedOn; }

    public String getAcctType(){ return acctType; }

    public String getSource(){ return source; }

    public Double getVesselNumber(){ return vesselNumber; }

    public Double getLcyAmt(){ return lcyAmt; }

    public String getAgentName(){ return agentName; }

    public String getAccountId(){ return accountId; }

    public String getSwiftCode(){ return swiftCode; }

    public String getJointAcctName(){ return jointAcctName; }

    public String getChannelType(){ return channelType; }

    public String getRemBnkBranch(){ return remBnkBranch; }

    public String getPort(){ return port; }

    public String getAcctCurr(){ return acctCurr; }

    public String getRemBic(){ return remBic; }

    public String getClientAccNo(){ return clientAccNo; }

    public Double getCoreCustId(){ return coreCustId; }

    public String getFcyTranCur(){ return fcyTranCur; }

    public String getRemAdd1(){ return remAdd1; }

    public String getRemAdd3(){ return remAdd3; }

    public String getKeys(){ return keys; }

    public String getRemAdd2(){ return remAdd2; }

    public String getTranId(){ return tranId; }

    public String getBenAdd3(){ return benAdd3; }

    public Double getCoreAcctId(){ return coreAcctId; }

    public Double getAcctCategory(){ return acctCategory; }

    public String getBenAdd2(){ return benAdd2; }

    public String getRemAcctNo(){ return remAcctNo; }

    public String getRemIdNo(){ return remIdNo; }

    public String getBenAdd1(){ return benAdd1; }

    public String getTranRefNo(){ return tranRefNo; }

    public String getAcctEmail(){ return acctEmail; }

    public String getAcctBranch(){ return acctBranch; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getTxnType(){ return txnType; }

    public String getBenName(){ return benName; }

    public String getServicCode(){ return servicCode; }

    public String getCustId(){ return custId; }

    public String getPurposCod(){ return purposCod; }

    public String getRemCntryCode(){ return remCntryCode; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getBenfBnkBranch(){ return benfBnkBranch; }

    public String getExecCust(){ return execCust; }

    public String getBenfIdNo(){ return benfIdNo; }

    public String getAcctAddr(){ return acctAddr; }
    public String getCxCustId(){ return cxCustId; }

    public String getAcctNoWsKey(){ return acctNoWsKey; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setRemBankCode(String val){ this.remBankCode = val; }
    public void setYear(String val){ this.year = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setBenCity(String val){ this.benCity = val; }
    public void setBranch(String val){ this.branch = val; }
    public void setBenCntryCode(String val){ this.benCntryCode = val; }
    public void setAgentCode(String val){ this.agentCode = val; }
    public void setLcyCurr(String val){ this.lcyCurr = val; }
    public void setRemName(String val){ this.remName = val; }
    public void setBenBic(String val){ this.benBic = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setPurposDesc(String val){ this.purposDesc = val; }
    public void setBenfBank(String val){ this.benfBank = val; }
    public void setVesselName(String val){ this.vesselName = val; }
    public void setBenAcctNo(String val){ this.benAcctNo = val; }
    public void setBenCustId(String val){ this.benCustId = val; }
    public void setSystem(String val){ this.system = val; }
    public void setUserTyp(String val){ this.userTyp = val; }
    public void setCreatedOn(java.sql.Timestamp val){ this.createdOn = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setFlag(String val){ this.flag = val; }
    public void setTrnDate(java.sql.Timestamp val){ this.trnDate = val; }
    public void setSwiftTxn(String val){ this.swiftTxn = val; }
    public void setRemBank(String val){ this.remBank = val; }
    public void setTypeOfGoods(String val){ this.typeOfGoods = val; }
    public void setTempRefNo(String val){ this.tempRefNo = val; }
    public void setSwiftMsgType(String val){ this.swiftMsgType = val; }
    public void setFcyTranAmt(Double val){ this.fcyTranAmt = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setFacilityNum(Double val){ this.facilityNum = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setRemCustId(String val){ this.remCustId = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setPartTranSrlNum(String val){ this.partTranSrlNum = val; }
    public void setCptyAcctNo(String val){ this.cptyAcctNo = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setIsrtgs(String val){ this.isrtgs = val; }
    public void setUsdEqvAmt(Double val){ this.usdEqvAmt = val; }
    public void setBankCode(String val){ this.bankCode = val; }
    public void setSno(Double val){ this.sno = val; }
    public void setTranCurr(String val){ this.tranCurr = val; }
    public void setRemCity(String val){ this.remCity = val; }
    public void setBenfIdType(String val){ this.benfIdType = val; }
    public void setUpdatedOn(java.sql.Timestamp val){ this.updatedOn = val; }
    public void setAcctType(String val){ this.acctType = val; }
    public void setSource(String val){ this.source = val; }
    public void setVesselNumber(Double val){ this.vesselNumber = val; }
    public void setLcyAmt(Double val){ this.lcyAmt = val; }
    public void setAgentName(String val){ this.agentName = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setSwiftCode(String val){ this.swiftCode = val; }
    public void setJointAcctName(String val){ this.jointAcctName = val; }
    public void setChannelType(String val){ this.channelType = val; }
    public void setRemBnkBranch(String val){ this.remBnkBranch = val; }
    public void setPort(String val){ this.port = val; }
    public void setAcctCurr(String val){ this.acctCurr = val; }
    public void setRemBic(String val){ this.remBic = val; }
    public void setClientAccNo(String val){ this.clientAccNo = val; }
    public void setCoreCustId(Double val){ this.coreCustId = val; }
    public void setFcyTranCur(String val){ this.fcyTranCur = val; }
    public void setRemAdd1(String val){ this.remAdd1 = val; }
    public void setRemAdd3(String val){ this.remAdd3 = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setRemAdd2(String val){ this.remAdd2 = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setBenAdd3(String val){ this.benAdd3 = val; }
    public void setCoreAcctId(Double val){ this.coreAcctId = val; }
    public void setAcctCategory(Double val){ this.acctCategory = val; }
    public void setBenAdd2(String val){ this.benAdd2 = val; }
    public void setRemAcctNo(String val){ this.remAcctNo = val; }
    public void setRemIdNo(String val){ this.remIdNo = val; }
    public void setBenAdd1(String val){ this.benAdd1 = val; }
    public void setTranRefNo(String val){ this.tranRefNo = val; }
    public void setAcctEmail(String val){ this.acctEmail = val; }
    public void setAcctBranch(String val){ this.acctBranch = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setTxnType(String val){ this.txnType = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setServicCode(String val){ this.servicCode = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setPurposCod(String val){ this.purposCod = val; }
    public void setRemCntryCode(String val){ this.remCntryCode = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setBenfBnkBranch(String val){ this.benfBnkBranch = val; }
    public void setExecCust(String val){ this.execCust = val; }
    public void setBenfIdNo(String val){ this.benfIdNo = val; }
    public void setAcctAddr(String val){ this.acctAddr = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setAcctNoWsKey(String val){ this.acctNoWsKey = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getcountry(){ return systemCountry; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_RemittanceEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctNoWsKey);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Remittance");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Remittance");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}