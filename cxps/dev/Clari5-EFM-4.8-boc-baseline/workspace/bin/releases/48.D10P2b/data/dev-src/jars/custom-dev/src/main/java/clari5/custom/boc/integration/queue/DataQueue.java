package clari5.custom.boc.integration.queue;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import clari5.custom.boc.integration.data.ITableData;
import clari5.custom.boc.integration.db.DBTask;
import java.util.ArrayList;
import cxps.apex.utils.CxpsLogger;

public class DataQueue extends ConcurrentLinkedQueue<ITableData> {
	private static final long serialVersionUID = 505663506134187795L;
	protected static CxpsLogger logger = CxpsLogger.getLogger(DBTask.class);
	//private static int lastloadCount = -1;
	public List<ITableData> load(String tableName) throws Exception {
		// Looping to avoid type mismatch.
		logger.info("Fetching data for ["+tableName+" ]");
		Class<?> c= DataQueueManager.nextTable(tableName);
			List<ITableData> list=new ArrayList<ITableData>();
			if (c != null) {
				List<?> rows = DBTask.getRows(c,tableName);
				for (Object td : rows) {
					list.add((ITableData) td);
				}
			}
		logger.info("Fetched data from ["+tableName+" ] with size ["+list.size()+"]");
		return list;

	}

	public static void main(String[] args) throws Exception {
		DataQueue dq = new DataQueue();
		//dq.load();
		logger.debug("DataQueue:dq.size():"+dq.size());
	}
}
