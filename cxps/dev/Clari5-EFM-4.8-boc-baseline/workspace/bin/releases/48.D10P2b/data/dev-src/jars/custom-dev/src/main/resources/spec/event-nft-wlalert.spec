cxps.events.event.nft-wlalert {
   table-name : EVENT_NFT_WLALERT
   event-mnemonic: NW
   workspaces : {
           CUSTOMER: cust-id
  	}
   event-attributes : {
    	host-id : {db : true ,raw_name : host_id ,type : "string:20"}
    	cust-id : {db : true ,raw_name : cust_id ,type : "string:20"}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
    	rulename: {db : true ,raw_name : rulename ,type : "string:100"}
    	inputentity: {db : true ,raw_name : input_entity ,type : "string:100"}
   	matched-result: {db : true ,raw_name : matched_result,type : "string:100"}
    	matched-percentage: {db : true ,raw_name : matched_percentage ,type : "string:100"}
    	list-name: {db : true ,raw_name : list_name ,type : "string:100"}
    	matched-entity: {db : true ,raw_name : matched_entity ,type : "string:20"}
    	
   }
}

