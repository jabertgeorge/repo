package clari5.custom.boc.integration.audit;

import clari5.custom.boc.integration.exceptions.ConfigurationException;

public enum LogLevel
{
	ALL("0"),
	TRACE("1"),
	DEBUG("2"),
	INFO("3"), 
	WARN("4"), 
	ERROR("5"), 
	FATAL("6"),
	OFF("-1");
	String level;
	LogLevel(String p) {
		level = p;
	}
	public static LogLevel getLogLevel(String level) throws ConfigurationException {
		LogLevel logLevel = null;
		switch(level.trim()) {
		case "0": {
			logLevel = LogLevel.ALL;
			break;
		}
		case "1": {
			logLevel = LogLevel.TRACE;
			break;
		}
		case "2": {
			logLevel = LogLevel.DEBUG;
			break;
		}
		case "3": {
			logLevel = LogLevel.INFO;
			break;
		}
		case "4": {
			logLevel = LogLevel.WARN;
			break;
		}
		case "5": {
			logLevel = LogLevel.ERROR;
			break;
		}
		case "6": {
			logLevel = LogLevel.FATAL;
			break;
		}
		case "-1": {
			logLevel = LogLevel.OFF;
			break;
		}
		default: {
			throw new ConfigurationException("Invalid log level specified.");
		}
		}
		return logLevel;
	} 
}
