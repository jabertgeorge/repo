package clari5.custom.wlalert;

import clari5.custom.wlalert.daemon.RuleFact;
import clari5.custom.wlalert.db.IDBOperation;
import clari5.custom.wlalert.db.WlRecordDetail;
import clari5.platform.applayer.Clari5;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author lisa
 */
public class FileProcess {
    public static CxpsLogger logger = CxpsLogger.getLogger(FileProcess.class);

    static FileStructure fileStructure = new FileStructure();
    public void getFile() {
        RuleFact ruleFact = null;
        IDBOperation wlRecordDetail = new WlRecordDetail();
        File[] listOfDirectory = fileStructure.getDirList();
        boolean status = false;
        for (File file : listOfDirectory) {
            try {
                if (file.isDirectory()) {
                    status = fileunzip(file.getAbsolutePath());
                    if ((fileStructure.getFileType(file.getName()).equalsIgnoreCase("xml")) && status) {
                        System.out.println("reading xml file ");
                        ruleFact = readXmlFile();
                        wlRecordDetail.insert(ruleFact);
                    } else if ((fileStructure.getFileType(file.getName()).equalsIgnoreCase("csv")) && status) {
                        wlRecordDetail.insert(readCsvFile());
                    } else if (!status) {
                        logger.error("removing the unzipfile directory , Failed to insert the records in wl_records table as status is false");
                        //Thread.sleep(300000);
                        try {
                            if (!StringUtils.isNullOrEmpty(fileStructure.getUnzipFilePath())) {
                                FileUtils.deleteDirectory(new File(fileStructure.getUnzipFilePath()));
                            }
                        } catch (Exception io) {
                            logger.error("Failed while deleting the unzip file directory ");
                            io.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Failed to process the file " + fileStructure.toString() + "\n message " + e.getMessage());
                e.printStackTrace();
            } finally {
                if (ruleFact != null)
                    ruleFact.clear();
            }
        }
    }

    public RuleFact readXmlFile() {
        RuleFact ruleFact;
        File[] xmlFilesList = new File(fileStructure.getUnzipFilePath()).listFiles();
        XmlParser parser = new XmlParser();
        File xmlFile = xmlFilesList[0];
        ruleFact = parser.xmlparser(fileStructure.getUnzipFilePath(), xmlFile);
        return ruleFact;
    }

    public RuleFact readCsvFile() {
        RuleFact ruleFact = new RuleFact();
        CsvParser csvParser = new CsvParser();
        File[] csvFileList = new File(fileStructure.getUnzipFilePath()).listFiles();
        for (File csvFile : csvFileList) {
            ruleFact = csvParser.csvparser(fileStructure.getUnzipFilePath(), csvFile);
        }
        return ruleFact;
    }

    public boolean fileunzip(String dir) {
        File dirPath = new File(dir);
        try {
            if (dirPath.list().length <= 0) return false;
            File[] zipFileList = dirPath.listFiles();
            fileStructure.setUnzipFilePath(dir + "/unzip");
            for (File zipFile : zipFileList) {
                String zipFileName = zipFile.getAbsolutePath();
                UnZip unZip = new UnZip();
                if (!unZip.unZipIt(zipFileName, fileStructure.getUnzipFilePath())) {
                    return false;
                }
                FileUtils.copyFile(zipFile, new File(fileStructure.getIndexingPath() + "/" + zipFile.getName()));
                File archiveFile = new File(fileStructure.getArchiveFilePath());
                if (!archiveFile.exists()) {
                    archiveFile.mkdirs();
                }
                String[] rename = zipFile.getName().split("\\.");
                String newZipFileName = rename[0] + "_" + System.currentTimeMillis() + "." + rename[1];
                Files.move(Paths.get(zipFileName), Paths.get(fileStructure.getArchiveFilePath() + "/" + newZipFileName));
            }
            return true;
        } catch (IOException io) {
            logger.error("Exception  while unzipping the file [" + io.getMessage() + "] Cause [" + io.getCause() + "]");
            io.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) {
        Clari5.batchBootstrap("rdbms", "efm-clari5.conf");
        System.out.println("FileProcess.main  started");
        FileProcess fileProcess = new FileProcess();
        fileProcess.getFile();
        System.out.println("FileProcess.main  ended");
    }
}
