<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Expires", "0");
    response.setDateHeader("Expires", -1);
%>
<html>
<head>
    <title>Investigation Tool</title>
    <link rel="icon" href="/cdn/ext/images/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE"/>
    <script type="text/javascript" src="/cdn/efm/common/js/lib/loadCDN.js"></script>
    <script>
        var redirectUrl = window.location.origin + "/efm?returl=" + encodeURIComponent(window.location.href);
        if (window.parent.location.pathname !== "/efm") {
            window.location.href = redirectUrl;
        }
        // Add any required headers here and they will be added to every request.
        /* Not sure if needed in iif or not
        $.ajaxSetup({
        beforeSend: function (xhr) {
        xhr.setRequestHeader('mode', 'REST');
        }
        });
        // Error handling for any request will go through this listener.
        $(document).ajaxError(function (jqXHR) {
        if (jqXHR.statusCode === 401)
        window.location.href = redirectUrl;
        });
        */
        loadCSS([
            "ext/css/bootstrap/bootstrap.css",
            "ext/css/bootstrap/bootstrap-theme.css",
            "ext/css/jquery/jquery-ui.css",
            "ext/css/jquery/jquery-ui-1.10.4.css",
            "ext/css/jquery/validation-engine/validationEngine.jquery.css",
            "ext/css/jquery/demo_table.css",
            "ext/css/jquery/jstree/style.css",
            "ext/css/jquery/jquery.toastmessage.css",
            "ext/css/jquery/overlay-apple.css"
        ]);
    </script>
    <link rel="stylesheet" href="/cdn/efm/io/css/case.css">
    <link rel="stylesheet" href="/cdn/efm/common/css/style.css" type="text/css"/>
    <style>
        #dialog label, #dialog input {
            display: block;
        }

        #dialog label {
            margin-top: 0.5em;
        }

        #dialog input, #dialog textarea {
            width: 95%;
        }

        #tabs {
            margin-top: 1em;
        }

        #tabs li .ui-icon-close {
            float: left;
            margin: 0.4em 0.2em 0 0;
            cursor: pointer;
        }

        #add_tab {
            cursor: pointer;
        }

        .clckBut {
        }

        a:hover, a:focus {
            text-decoration: none;
        }

        .clckButactive {
            cursor: pointer;
            width: 143px;
            color: #fff;
            background-color: #14143d !important;
            opacity: .7;
            margin: 0px 3px 3px 0;
          /*border-radius: 4 px 4 px 4 px 4 px;*/
            border-top-left-radius: 4px;
            -webkit-transform: scale(1.03, 1.04);
            -webkit-transition-timing-function: ease-out;
            -webkit-transition-duration: 500ms;
            -moz-transform: scale(1.03, 1.04);
            -moz-transition-timing-function: ease-out;
            -moz-transition-duration: 500ms;
            position: relative;
            z-index: 101;
        }

        .clckButactive:hover {
            background-color: #14143d !important;
        }

        .schWindow {
            margin: 0px 148px;
            width: 18%;
            height: 100%;
            z-index: 90;
            background: #FFFFFF;
            display: none;
            position: absolute;
            text-align: center;
            border-radius: 8px 8px 8px 8px;
            vertical-align: middle;
        }

        .schWindBlck {
            margin: 0px 148px;
            width: 18%;
            height: 100%;
            z-index: -2;
            opacity: 0.0;
            display: block;
            line-height: 1;
            position: fixed;
        }

        .clSrchInsideDiv {
            background: url("/cdn/efm/common/images/bw-b-28-wc.png") no-repeat scroll 0% 0% transparent;
            width: 18px;
            height: 18px;
            margin-top: 40px;
            display: block;
            float: right;
        }

        .clsWindow {
            margin: 0 148px;
            background: #FFFFFF;
            display: block;
            position: absolute;
            text-align: center;
            width: 18px;
            height: 15%;
            float: left;
            z-index: 90;
            margin-top: 235px;
            margin-left: 375px;
            border-radius: 0px 8px 8px 0px;
        }

        .opnWindow {
            border-radius: 0px 8px 8px 0px;
            border: 1px solid #E0E0E0;
            margin: 0px 148px;
            background: #FFFFFF;
            display: block;
            position: absolute;
            text-align: center;
            width: 18px;
            height: 15%;
            float: left;
            z-index: 90;
            margin-top: 235px;
            margin-left: 158px;
        }

        .clSrchOutSideDiv {
            background: url("/cdn/efm/common/images/ff-b-28-wc.png") no-repeat scroll 0% 0% transparent;
            width: 16px;
            height: 18px;
            margin-top: 40px;
            display: block;
            float: right;
        }

        span {
            display: inline-block;
            vertical-align: middle;
            line-height: normal;
        }

        #openCloseWrap {
            font-size: 12px;
            font-weight: bold;
        }

        body {
            font: 1.1em Tahoma, sans-serif !important;
        }

        .ui-tabs .ui-tabs-nav li a {
            white-space: normal;
            height: 20px;
        }

        .tabs a:hover, .tabs a:focus, .tabs a:active {
            background: #fff;
            cursor: pointer;
        }

        .ui-tabs-selected a {
            background-color: #fff;
            color: #000;
            font-weight: bold;
            padding: 2px 8px 1px;
            border-bottom: 1px solid #fff;
            border-top: 3px solid #fabd23;
            border-left: 1px solid #fabd23;
            border-right: 1px solid #fabd23;
            margin-bottom: -1px;
            overflow: visible;
        }

        .tabMax {
            overflow: hidden;
            width: 97px;
        }

        .goBackCls {
            width: 80px;
            margin: 4px 0px 4px 91%;
            color: #FFFFFF !important;
        }

        .jstree-icon {
        }

        .jstree-themeicon {
        }

        .jstree-themeicon-custo {
        }

        .formErrorContent {
            width: 100%;
            background: #ee0101;
            position: relative;
            color: #fff;
            min-width: 160px;
            font-size: 11px;
            border: 2px solid #ddd;
            box-shadow: 0 0 6px #000;
            -moz-box-shadow: 0 0 6px #000;
            -webkit-box-shadow: 0 0 6px #000;
            -o-box-shadow: 0 0 6px #000;
            padding: 4px 10px 4px 10px;
            border-radius: 6px;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            -o-border-radius: 6px;
        }

        .boxTab {
            display: none;
        }
    </style>
    <script>
        var jqFile = ["ext/jquery/jquery-3.3.1.js"];
        loadFromCDN(jqFile, load);
        var jsFiles = [
            "ext/jquery/jquery-ui.js",
            "ext/jquery/validation-engine/jquery.validationEngine-en.js",
            "ext/jquery/validation-engine/jquery.validationEngine.js",
            "ext/jquery/jstree/jstree.min.js",
            "ext/jquery/jquery.dataTables.min.js",
            "ext/jquery/jquery.toastmessage-min.js",
            "efm/io/js/ioService.js",
            "efm/common/js/lib/utils.js",
            "efm/common/js/lib/cxNetwork.js",
            "efm/io/js/dataTabUtil.js",
            "efm/io/js/utils.js",
        ];
        function load() {
            loadFromCDN(jsFiles, null);
        }
        /*Load local js files*/
        loadFromCDN(jqFile, loadLocalFiles);
        var localJsFiles = [];
        function loadLocalFiles() {
            addLocalJs(localJsFiles, null);
        }
    </script>

</head>
<body onload="init()" style="position: fixed">
    <div style="width:101%; height:100%;" id="iDiv">
        <div id='leftPanel' style="width:12%; border:1px solid #808080; border-radius:7px; float:left; height:100%; padding:3px;background:#fcfcfc;">
            </br></br></br>
            <form id="leftForm">
                <div style='font-size:14px;'>
                    <input type="text" class="validate[custom[oneSpcLetterNumber]]" placeholder="Customer/Account ID" value=""style="margin:10px; width:120px;" id="refId" />
                </div>
            </form>
	<a id='nicDiv' class='abutton cust-w-def clckBut' style='margin-bottom:6px;' >NIC</a>
            <a id='custDiv' class='abutton cust-w-def clckBut' style='margin-bottom:6px;'>Customer</a>
            <a id='acctDiv' class='abutton acct-w-def clckBut' style='margin-bottom:6px;'>Account</a>
            <a id='empDiv' class='abutton cust-w-def clckBut' style='margin-bottom:6px;'>Staff</a>
        </div>
        <div class="overlay" id="linkOverlay" style="display:none;"></div>
        <div id="linkAnalysis" style="display:none; padding-top: 42px;" class="analTab">
            <div id="analLink" style="margin:-30px 0px 0px -12px; border:1px solid #808080; border-radius:0 10px 7px 7px;width:110%;">
                <div class="heading">
                    <h3>Confirm to load link analysis
                        <a class="boxclose" id="linkClose" onclick="linkClose()" href="#"></a>
                    </h3>
                </div>
            </div>
        </div>
        <div></div>
        <div id='slidePnl'>
            <div id='srchWindow' class='schWindow'>
                <div id='comptDiv'></div>
            </div>
            <div id='clSchWd' class='clsWindow' style='display:none; float:left; left:0;'>
                <a href='#' id='imgWindow' class='clSrchInsideDiv' onclick='toggleSrchWindow()' style=''></a>
            </div>
        </div>
        <div style="width:86.7%; border:1px solid #808080; border-radius:7px; margin-left:4px; float:left; height:100%; padding:3px; background:#fcfcfc;">
             <div id="canvas" style="height:99%;">
                 <ul id="tabNavUl"></ul>
             </div>
        </div>
    </div>
    <div style="width:100%; height:100%;display:none;" id="analDiv">
    <div>
        <a onclick="backAnanlysis()" class="abutton back-w-def goBackCls">Back</a></div>
    </div>
    <script src="customer.js"></script>
    <script src="account.js"></script><script src="otheraccts.js"></script><script src="nic.js"></script>
    <script src="indexPage.js"></script><script src="employee.js"></script><script src="case.js"></script>
    <script src="transaction.js"></script>
</body>
</html>
