package clari5.upload.ui;

import java.sql.ResultSetMetaData;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Servlet implementation class ReadDisplay
 */

public class FileValidate extends HttpServlet {
    private static final long serialVersionUID = 102831973239L;
    private Connection con = null;
    private PreparedStatement stat = null;
    private String timeStamp = null;
    private int columnNum = 0;
    private String requestFileName = null;
    private String fileBackupPath = null;
    private String requestFileNameSplit = null;
    private static CxpsLogger logger = CxpsLogger.getLogger(FileValidate.class);
    JSONObject object = new JSONObject();
    JSONArray arr = new JSONArray();


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' type='text/css' href='upload/css/main.css'>");
        out.println("</head>");
        HttpSession session;
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        ConfigFileReader reader = new ConfigFileReader("upload-ui-clari5.conf");
        String uploadBkpDirectory = reader.getFileBackUpPath();

        if (ServletFileUpload.isMultipartContent(request)) {
            try {

                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);

                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        requestFileName = new File(item.getName()).getName();
                        fileBackupPath = uploadBkpDirectory + File.separator + timeStamp + requestFileName;
                        item.write(new File(uploadBkpDirectory + File.separator + timeStamp + requestFileName));
                        logger.info("Backup path [" + uploadBkpDirectory + "]");
                    }
                }
            } catch (Exception e) {
                logger.info("Backup path is invalid [" + e.getMessage() + "] Cause :[" + e.getCause() + "]");
                response.setContentType("text/html");
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\" Backup path [" + uploadBkpDirectory + "]is invalid !!! Kindly Check and try again \");");
                out.println("window.location.href='TableDetails';");
                out.println("</script>");
            }
        }
        session = request.getSession();
        session.setAttribute("FilePath", fileBackupPath);
        session.setAttribute("FileName", requestFileName);
        int s = fileBackupPath.lastIndexOf('.');
        logger.info("Index of file [" + s + "]");
        if (s >= 0) {
            requestFileNameSplit = fileBackupPath.substring(s + 1);
        }
        String ename = new String(requestFileNameSplit);
        logger.info("File Type Obtained " + ename);
        if (ename.equals("xls") || ename.equals("xlsx")) {
            out.println("<html>");
            out.println("<head>");
            out.println(" <link rel='stylesheet' href='upload/css/bootstrap.min.css'>");
            out.println("<script src='upload/js/bootstrap.min.js'></script>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class='row'><div class='col-xs-2'></div>");
            out.println("<div class='col-xs-6'>");
            out.println("<h3>Are you sure you want to upload the file </h3><br>");
            out.println("</div>");
            out.println("<div class='col-xs-4'></div></div>");
            out.println("<div class='row'><div class='col-xs-4'></div>");
            out.println("<div class='col-xs-1'>");
            out.println("<a href='/efm/UploadDownloadFileServlet'><input type='button' value='Insert' class='btn btn-info'></a>");
            out.println("</div>");
            out.println("<div class='col-xs-6'></div></div>");
            out.println("</body>");
            out.println("</html>");

        } else {
            response.setContentType("text/html");
            out.println("<script type=\"text/javascript\">");
            out.println("alert(\"The uploaded file formate is [" + ename + "]. Kindly upload either xls or xlsx file \");");
            out.println("window.location.href='/efm/TableDetails';");
            out.println("</script>");
        }
    }


}
