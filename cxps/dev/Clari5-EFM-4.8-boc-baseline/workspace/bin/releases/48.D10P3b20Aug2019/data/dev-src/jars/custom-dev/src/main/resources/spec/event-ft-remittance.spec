cxps.events.event.ft-remittance {

  table-name : EVENT_FT_REMITTANCE
  event-mnemonic = FR
  workspaces : { 
  	            ACCOUNT :  acct-no-ws-key
		    CUSTOMER: cust-id
		    NONCUSTOMER: nic
  }
  
  event-attributes : {

    event-name : {db : true, raw_name: event-name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    cust-id : {db : true, raw_name: CUST_ID, type : "string:100"}
    flag : {db : true, raw_name: flag, type : "string:100"}
    host-user-id:{type:"string:100",db:true,raw_name:host_user_id}
    source:{type:"string:100",db:true,raw_name:source}
    eventts:{type:timestamp,db:true,raw_name:eventts}
    created_on:{type:timestamp,db:true,raw_name:created_on}
    updated_on:{type:timestamp,db:true,raw_name:updated_on}
    host-id:{type:"string:100",db:true,raw_name:host-id}
    channel-type : {db : true ,raw_name : channel_type ,type : "string:20"}
    channel:{type:"string:100",db:true,raw_name:channel}
    system:{type:"string:100",db:true,raw_name:SYSTEM}
    account-id:{type:"string:100",db:true,raw_name:account-id}
    cx-cust-id : {db : true ,raw_name : cx_cust_id ,type : "string:20", derivation:"""clari5.custom.dev.PreProcessor.getCustIdForRemmittanceEvent(this)"""}
    cx-acct-id : {db : true ,raw_name : cx_acct_id ,type : "string:20"}
    facility-num :{type:"number:11,2",db:true,raw_name:Facility_Number}
    core_acct_id :{type:"number:11,2",db:true,raw_name:CORE_ACCOUNT_ID}
    core_cust_id :{type:"number:11,2",db:true,raw_name:CORE_CUST_ID}
    rem-type:{type:"string:100",db:true,raw_name:REM_TYPE}
    branch:{type:"string:100",db:true,raw_name:BRANCH}
    txn-date : {db : true ,raw_name : sys_time ,type : timestamp}
    acct_category :{type:"number:11,2",db:true,raw_name:ACCOUNT_CATAGORY}
    txn-type:{type:"string:100",db:true,raw_name:Tran_Type}
    txn-code:{type:"string:100",db:true,raw_name:Transaction_Code}
    user_typ:{type:"string:100",db:true,raw_name:User_Type}
    year:{type:"string:100",db:true,raw_name:YEAR}
    sno:{type:"number:11,2",db:true,raw_name:SNO}
    swift_code:{type:"string:100",db:true,raw_name:Swift_Code}
    swift_msg_type:{type:"string:100",db:true,raw_name:Swift_Message_Type}
    swift_txn:{type:"string:100",db:true,raw_name:Swift_Transaction}
    tran-ref-no:{type:"string:100",db:true,raw_name:TRAN_REF_NO}
    temp-ref-no:{type:"string:100",db:true,raw_name:TEMP_REF_NO}
    tran-curr:{type:"string:100",db:true,raw_name:TRAN_CURR}
    tran_id:{type:"string:100",db:true,raw_name:tran_id}
    tran-amt:{type:"number:11,2",db:true,raw_name:TRAN_AMT}
    fcy-tran-amt : {db : true ,raw_name : fcy-tran-amt ,type : "number:37,2" }
    fcy-tran-cur : {db : true ,raw_name : fcy-tran-cur ,type : "string:10" }
    exec-cust:{type:"string:100",db:true,raw_name:EXEC_CUSTOMER}
    usd-eqv-amt:{type:"number:11,2",db:true,raw_name:USD_EQV_AMT}
    lcy-amt:{type:"number:11,2",db:true,raw_name:LCY_AMOUNT}
    lcy-curr:{type:"string:100",db:true,raw_name:LCY_CURR}
    purpos-cod:{type:"string:100",db:true,raw_name:PURPOSE_CODE}
    purpos-desc:{type:"string:100",db:true,raw_name:PURPOSE_DESC}
    rem-cust-id:{type:"string:100",db:true,raw_name:REM_CUST_ID}
    rem-name:{type:"string:100",db:true,raw_name:REM_NAME}
    rem-add1:{type:"string:100",db:true,raw_name:REM_ADD1}
    rem-add2:{type:"string:100",db:true,raw_name:REM_ADD2}
    rem-add3:{type:"string:100",db:true,raw_name:REM_ADD3}
    rem-city:{type:"string:100",db:true,raw_name:REM_CITY}
    rem-cntry-code:{type:"string:100",db:true,raw_name:REM_CNTRY_CODE}
    rem-id-no:{type:"string:100",db:true,raw_name:REM_ID_NO}
    rem-bank:{type:"string:100",db:true,raw_name:REMBANK}
    rem-bnk-branch:{type:"string:100",db:true,raw_name:REM_BANK_BRANCH}
    rem-bank-code:{type:"string:100",db:true,raw_name:REM_BANK_CODE}
    keys : {db : true ,raw_name : keys ,type : "string:20"}  
    ben-name:{type:"string:100",db:true,raw_name:BEN_NAME}
    ben-add1:{type:"string:100",db:true,raw_name:BEN_ADD1}
    ben-add2:{type:"string:100",db:true,raw_name:BEN_ADD2}
    ben-add3:{type:"string:100",db:true,raw_name:BEN_ADD3}
    ben-cntry-code:{type:"string:100",db:true,raw_name:BEN_CNTRY_CODE}
    ben-cust-id:{type:"string:100",db:true,raw_name:BEN_CUST_ID}
    ben-acct-no:{type:"string:100",db:true,raw_name:BEN_ACCT_NO}
    acct-no-ws-key:{type:"string:100",db:true,raw_name:acct-no-ws-key,derivation:"""cxps.customEventHelper.CustomWsKeyDerivator.accountKeyForRemmitance(rem-type, rem-acct-no,ben-acct-no)"""}
    ben-bic:{type:"string:100",db:true,raw_name:BEN_BIC}
    ben-city:{type:"string:100",db:true,raw_name:BEN_CITY}
    benf-id-type:{type:"string:100",db:true,raw_name:BENF_ID_TYPE}
    benf-id-no:{type:"string:100",db:true,raw_name:BENF_ID_No}
    benf-bank:{type:"string:100",db:true,raw_name:BENF_BANK}
    benf-bnk-branch:{type:"string:100",db:true,raw_name:BENF_BANK_BRANCH}
    cpty-acct-no:{type:"string:100",db:true,raw_name:CPTY_AC_NO}
    rem-acct-no:{type:"string:100",db:true,raw_name:REM_ACCT_NO}
    rem-bic:{type:"string:100",db:true,raw_name:REM_BIC}
    client-acc-no:{type:"string:100",db:true,raw_name:CLIENT_ACC_NO}
    bank-code:{type:"string:100",db:true,raw_name:BANK_CODE}
    acct_name :{type:"string:100",db:true,raw_name:ACCT_NAME}    
    acct_type :{type:"string:100",db:true,raw_name:ACCT_TYPE}    
    acct_curr:{type:"string:100",db:true,raw_name:ACCT_CURR}    
    acct_branch :{type:"string:100",db:true,raw_name:ACCT_BRANCH}    
    acct_email:{type:"string:100",db:true,raw_name:ACCT_EMAIL}  
    acct_addr :{type:"string:100",db:true,raw_name:ACCT_ADDR}
    trn-date:{type:timestamp,db:true,raw_name:TRN_DATE}
    tran-date:{type:timestamp,db:true,raw_name:TRAN_DATE}
    servic-code:{type:"string:100",db:true,raw_name:Service_Code}
    agent-code:{type:"string:100",db:true,raw_name:Agent_Code}
    agent-name:{type:"string:100",db:true,raw_name:Agent_Name}
    joint_acct_name:{type:"string:100",db:true,raw_name:JOINT_ACCT_NAME}    
    isrtgs:{type:"string:100",db:true,raw_name:ISRTGS}
    type_of_goods:{type:"string:100",db:true,raw_name:TYPE_OF_GOODS}
    vessel_number{type:"number:11,2",db:true,raw_name:Vessel_Number}
    vessel_name:{type:"string:100",db:true,raw_name:Vessel_Name}
    port:{type:"string:100",db:true,raw_name:Port}
    nic:{type:"string:100",db:true,raw_name:nic}
    add-entity-id1:{type:"string:100",db:true,raw_name:reservedfield1}
    part-tran-srl-num : {db : true ,raw_name : part_tran_srl_num ,type : "string:20"}
    add-entity-id2:{type:"string:100",db:true,raw_name:reservedfield2}
    add-entity-id3:{type:"string:100",db:true,raw_name:reservedfield3}
    add-entity-id4:{type:"string:100",db:true,raw_name:reservedfield4}
    add-entity-id5:{type:"string:100",db:true,raw_name:reservedfield5}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20", custom-getter: country}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20", custom-getter: country}
  }
}
	
