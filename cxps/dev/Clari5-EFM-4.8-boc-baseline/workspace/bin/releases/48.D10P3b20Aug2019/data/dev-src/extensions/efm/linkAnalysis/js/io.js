/* io.js start */
var ioMenu="<div id='menu' class='navigation' style='width:100%;' ><ul class='nav' id='menuBar' ><li><a onClick=\"tabNavigation('case')\" href='#' >Case</a></li><li><a onClick=\"tabNavigation('customer')\" href='#' >Customer</a></li><li><a onClick=\"tabNavigation('account')\" href='#' >Account</a></li><li><a onClick=\"tabNavigation('transaction')\" href='#' >Transaction</a></li><li><div style='padding: 14px 8px;'></div></li><li><div style='padding: 14px 8px;'></div></li><li><div ><img onclick='javascript:doneOverlay()' src='../images/traingle.jpg' title='Switch Screen' style='width:18px;height:14px;aligh:right;'></img></div></li></ul></div>";

var ioOverlay="<div id='faceboxdone' style='width:310px;height:100px;'><div ><h2>Switch Screen<img src='"+ovlyCls+"' onclick=\"closeOverlay();\" style='width:20px;height:20px;float:right;' title='Close' id='closegrupimg' /></h2><div><table><tr><td><select id='refType' style='width:110px;'><option value='case' >Case</option><option value='customer' >Customer</option><option value='account' >Account</option><option value='transaction' >Transaction</option></select></td><td><input type='text' value='' id='refId' style='width:80px;' size='20'/></td><td><button type='submit' onclick='searchCustomer()' style='width:25px;height:25px;'><img src='"+ovlySrch+"' style='width:20px;height:20px;margin-top:-2px;margin-left:-3.5px;' alt='Submit'></button></td><td><input type='button' value='Go' onClick='searchCustomer()' style='width:40px;' /></td></tr></table></div></div>";

$(document).ready(function () {
	$(document.body).append(ioOverlay);
	$("#menu").hide();
});

		function doneOverlay(){
        		var horizontal = Math.floor(window.parent.innerWidth/2)-120;
		        var vertical = Math.floor(window.parent.innerHeight/2)-120;
	            document.getElementById("faceboxdone").style.position="absolute";
	            document.getElementById("faceboxdone").style.top=parseInt(vertical)+"px";
	            document.getElementById("faceboxdone").style.left=parseInt(horizontal)+"px";
	            $("#faceboxdone").slideDown("fast");
	    }

		function searchCustomer()
		{
			var refType = document.getElementById('refType').value;
			var refId = document.getElementById('refId').value;
			var url=createUrl(refId,refType);
			window.open(url,"_self");
			$("#menu").show();
			$('#faceboxdone').slideUp('fast');
		}

	function createUrl(refId,refType){
	    var url=window.location.href;
	    if(window.location.href.indexOf("index.jsp")>0){
	    url=window.location.href.replace("index.jsp","case/case.jsp");
        url+=(refId!="")?("?refId="+refId):"";
        if(refId!="")
        url+=(refType!="")?("&jsFile="+refType):"";
        else
        url+=(refType!="")?("?jsFile="+refType):"";
        }else{
	    url=updateQueryStringParameter(window.location.href,"jsFile",refType);
	    }
		return url;
	}

	function tabNavigation(obj){
		var url=updateQueryStringParameter(window.location.href,"jsFile",obj);
		url=url.split('&')[0];
		window.open(url,"_self");
	}

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
  separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}
		
function loadCaseDetails(){
	var refType = document.getElementById('refType').value;
	var refId = document.getElementById('refId').value;
	$("#menu").show();
	$("#faceboxdone").hide();
}

function loadJSFile(jsSrc){
	var head = document.getElementsByTagName('head')[0];
	var js = document.createElement("script");
	js.type = "text/javascript";
	js.async = true;
	js.src =jsSrc;
	head.appendChild(js);
}


function closeOverlay(){
$('#faceboxdone').slideUp('fast');

if(window.location.href.indexOf("index.jsp")>0){
url=window.location.href.replace("io/index.jsp","po/index.jsp");
window.open(url,"_self");
}

}
/* io.js end */
