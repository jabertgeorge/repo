// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_TrbTxnEventMapper extends EventMapper<FT_TrbTxnEvent> {

public FT_TrbTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_TrbTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_TrbTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_TrbTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_TrbTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_TrbTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_TrbTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getBranchIdDesc());
            preparedStatement.setString(i++, obj.getCounterPartyAddress());
            preparedStatement.setString(i++, obj.getSourceBank());
            preparedStatement.setString(i++, obj.getCountrPartyAccount());
            preparedStatement.setString(i++, obj.getCounterPartyAmount());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setTimestamp(i++, obj.getSystemTime());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getAcctType());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getSourceOrDestAcctId());
            preparedStatement.setDouble(i++, obj.getRefAmt());
            preparedStatement.setString(i++, obj.getSequenceNumber());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getTranSrlNo());
            preparedStatement.setString(i++, obj.getCounterPartyBank());
            preparedStatement.setString(i++, obj.getCounterPartyBankAddress());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setTimestamp(i++, obj.getTranPostedDt());
            preparedStatement.setTimestamp(i++, obj.getTxnValueDate());
            preparedStatement.setString(i++, obj.getSystemCountry());
            preparedStatement.setString(i++, obj.getDesc());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getSchemeCode());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getCounterPartyCurrencyLcy());
            preparedStatement.setTimestamp(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getChannelId());
            preparedStatement.setString(i++, obj.getCounterPartyBusiness());
            preparedStatement.setString(i++, obj.getCounterPartyBIC());
            preparedStatement.setString(i++, obj.getTxnStatus());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getCounterPartyAmountLcy());
            preparedStatement.setString(i++, obj.getCounterPartyCountryCode());
            preparedStatement.setString(i++, obj.getTellerNumber());
            preparedStatement.setString(i++, obj.getCounterPartyName());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getCounterPartyCurrency());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getAccountOwnership());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setDouble(i++, obj.getRefCurncy());
            preparedStatement.setTimestamp(i++, obj.getAccountOpendate());
            preparedStatement.setString(i++, obj.getCounterPartyBankCode());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getInstrumentType());
            preparedStatement.setString(i++, obj.getRemarks());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_TRB_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_TrbTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_TrbTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRB_TXN"));
        putList = new ArrayList<>();

        for (FT_TrbTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "BRANCH_ID_DESC",  obj.getBranchIdDesc());
            p = this.insert(p, "COUNTERPARTY_ADDRESS",  obj.getCounterPartyAddress());
            p = this.insert(p, "SOURCE_BANK",  obj.getSourceBank());
            p = this.insert(p, "COUNTRPARTY_ACCOUNT",  obj.getCountrPartyAccount());
            p = this.insert(p, "COUNTERPARTY_AMOUNT",  obj.getCounterPartyAmount());
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "SYSTEM_TIME", String.valueOf(obj.getSystemTime()));
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "ACCT_TYPE",  obj.getAcctType());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "SOURCE_OR_DEST_ACCT_ID",  obj.getSourceOrDestAcctId());
            p = this.insert(p, "REF_AMT", String.valueOf(obj.getRefAmt()));
            p = this.insert(p, "SEQUENCE_NUMBER",  obj.getSequenceNumber());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "TRAN_SRL_NO",  obj.getTranSrlNo());
            p = this.insert(p, "COUNTERPARTY_BANK",  obj.getCounterPartyBank());
            p = this.insert(p, "COUNTERPARTY_BANK_ADDRESS",  obj.getCounterPartyBankAddress());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "TRAN_POSTED_DT", String.valueOf(obj.getTranPostedDt()));
            p = this.insert(p, "TXN_VALUE_DATE", String.valueOf(obj.getTxnValueDate()));
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            p = this.insert(p, "DESC",  obj.getDesc());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "SCHEME_CODE",  obj.getSchemeCode());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "COUNTERPARTY_CURRENCY_LCY",  obj.getCounterPartyCurrencyLcy());
            p = this.insert(p, "EVENT_TIME", String.valueOf(obj.getEventTime()));
            p = this.insert(p, "CHANNEL_ID",  obj.getChannelId());
            p = this.insert(p, "COUNTERPARTY_BUSINESS",  obj.getCounterPartyBusiness());
            p = this.insert(p, "COUNTERPARTY_BIC",  obj.getCounterPartyBIC());
            p = this.insert(p, "TXN_STATUS",  obj.getTxnStatus());
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "COUNTERPARTY_AMOUNT_LCY",  obj.getCounterPartyAmountLcy());
            p = this.insert(p, "COUNTERPARTY_COUNTRY_CODE",  obj.getCounterPartyCountryCode());
            p = this.insert(p, "TELLER_NUMBER",  obj.getTellerNumber());
            p = this.insert(p, "COUNTERPARTY_NAME",  obj.getCounterPartyName());
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "COUNTERPARTY_CURRENCY",  obj.getCounterPartyCurrency());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "ACCOUNT_OWNERSHIP",  obj.getAccountOwnership());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "REF_CURNCY", String.valueOf(obj.getRefCurncy()));
            p = this.insert(p, "ACCOUNT_OPENDATE", String.valueOf(obj.getAccountOpendate()));
            p = this.insert(p, "COUNTERPARTY_BANK_CODE",  obj.getCounterPartyBankCode());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "INSTRUMENT_TYPE",  obj.getInstrumentType());
            p = this.insert(p, "REMARKS",  obj.getRemarks());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_TRB_TXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_TRB_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_TRB_TXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_TrbTxnEvent obj = new FT_TrbTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setBranchIdDesc(rs.getString("BRANCH_ID_DESC"));
    obj.setCounterPartyAddress(rs.getString("COUNTERPARTY_ADDRESS"));
    obj.setSourceBank(rs.getString("SOURCE_BANK"));
    obj.setCountrPartyAccount(rs.getString("COUNTRPARTY_ACCOUNT"));
    obj.setCounterPartyAmount(rs.getString("COUNTERPARTY_AMOUNT"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setSystemTime(rs.getTimestamp("SYSTEM_TIME"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setAcctType(rs.getString("ACCT_TYPE"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setSourceOrDestAcctId(rs.getString("SOURCE_OR_DEST_ACCT_ID"));
    obj.setRefAmt(rs.getDouble("REF_AMT"));
    obj.setSequenceNumber(rs.getString("SEQUENCE_NUMBER"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setTranSrlNo(rs.getString("TRAN_SRL_NO"));
    obj.setCounterPartyBank(rs.getString("COUNTERPARTY_BANK"));
    obj.setCounterPartyBankAddress(rs.getString("COUNTERPARTY_BANK_ADDRESS"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setTranPostedDt(rs.getTimestamp("TRAN_POSTED_DT"));
    obj.setTxnValueDate(rs.getTimestamp("TXN_VALUE_DATE"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));
    obj.setDesc(rs.getString("DESC"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setSchemeCode(rs.getString("SCHEME_CODE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setCounterPartyCurrencyLcy(rs.getString("COUNTERPARTY_CURRENCY_LCY"));
    obj.setEventTime(rs.getTimestamp("EVENT_TIME"));
    obj.setChannelId(rs.getString("CHANNEL_ID"));
    obj.setCounterPartyBusiness(rs.getString("COUNTERPARTY_BUSINESS"));
    obj.setCounterPartyBIC(rs.getString("COUNTERPARTY_BIC"));
    obj.setTxnStatus(rs.getString("TXN_STATUS"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setCounterPartyAmountLcy(rs.getString("COUNTERPARTY_AMOUNT_LCY"));
    obj.setCounterPartyCountryCode(rs.getString("COUNTERPARTY_COUNTRY_CODE"));
    obj.setTellerNumber(rs.getString("TELLER_NUMBER"));
    obj.setCounterPartyName(rs.getString("COUNTERPARTY_NAME"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setCounterPartyCurrency(rs.getString("COUNTERPARTY_CURRENCY"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setAccountOwnership(rs.getString("ACCOUNT_OWNERSHIP"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setRefCurncy(rs.getDouble("REF_CURNCY"));
    obj.setAccountOpendate(rs.getTimestamp("ACCOUNT_OPENDATE"));
    obj.setCounterPartyBankCode(rs.getString("COUNTERPARTY_BANK_CODE"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setInstrumentType(rs.getString("INSTRUMENT_TYPE"));
    obj.setRemarks(rs.getString("REMARKS"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_TRB_TXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_TrbTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_TrbTxnEvent> events;
 FT_TrbTxnEvent obj = new FT_TrbTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRB_TXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_TrbTxnEvent obj = new FT_TrbTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setBranchIdDesc(getColumnValue(rs, "BRANCH_ID_DESC"));
            obj.setCounterPartyAddress(getColumnValue(rs, "COUNTERPARTY_ADDRESS"));
            obj.setSourceBank(getColumnValue(rs, "SOURCE_BANK"));
            obj.setCountrPartyAccount(getColumnValue(rs, "COUNTRPARTY_ACCOUNT"));
            obj.setCounterPartyAmount(getColumnValue(rs, "COUNTERPARTY_AMOUNT"));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setSystemTime(EventHelper.toTimestamp(getColumnValue(rs, "SYSTEM_TIME")));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setAcctType(getColumnValue(rs, "ACCT_TYPE"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setSourceOrDestAcctId(getColumnValue(rs, "SOURCE_OR_DEST_ACCT_ID"));
            obj.setRefAmt(EventHelper.toDouble(getColumnValue(rs, "REF_AMT")));
            obj.setSequenceNumber(getColumnValue(rs, "SEQUENCE_NUMBER"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setTranSrlNo(getColumnValue(rs, "TRAN_SRL_NO"));
            obj.setCounterPartyBank(getColumnValue(rs, "COUNTERPARTY_BANK"));
            obj.setCounterPartyBankAddress(getColumnValue(rs, "COUNTERPARTY_BANK_ADDRESS"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setTranPostedDt(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_POSTED_DT")));
            obj.setTxnValueDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_VALUE_DATE")));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));
            obj.setDesc(getColumnValue(rs, "DESC"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setSchemeCode(getColumnValue(rs, "SCHEME_CODE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setCounterPartyCurrencyLcy(getColumnValue(rs, "COUNTERPARTY_CURRENCY_LCY"));
            obj.setEventTime(EventHelper.toTimestamp(getColumnValue(rs, "EVENT_TIME")));
            obj.setChannelId(getColumnValue(rs, "CHANNEL_ID"));
            obj.setCounterPartyBusiness(getColumnValue(rs, "COUNTERPARTY_BUSINESS"));
            obj.setCounterPartyBIC(getColumnValue(rs, "COUNTERPARTY_BIC"));
            obj.setTxnStatus(getColumnValue(rs, "TXN_STATUS"));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setCounterPartyAmountLcy(getColumnValue(rs, "COUNTERPARTY_AMOUNT_LCY"));
            obj.setCounterPartyCountryCode(getColumnValue(rs, "COUNTERPARTY_COUNTRY_CODE"));
            obj.setTellerNumber(getColumnValue(rs, "TELLER_NUMBER"));
            obj.setCounterPartyName(getColumnValue(rs, "COUNTERPARTY_NAME"));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setCounterPartyCurrency(getColumnValue(rs, "COUNTERPARTY_CURRENCY"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setAccountOwnership(getColumnValue(rs, "ACCOUNT_OWNERSHIP"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setRefCurncy(EventHelper.toDouble(getColumnValue(rs, "REF_CURNCY")));
            obj.setAccountOpendate(EventHelper.toTimestamp(getColumnValue(rs, "ACCOUNT_OPENDATE")));
            obj.setCounterPartyBankCode(getColumnValue(rs, "COUNTERPARTY_BANK_CODE"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setInstrumentType(getColumnValue(rs, "INSTRUMENT_TYPE"));
            obj.setRemarks(getColumnValue(rs, "REMARKS"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRB_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRB_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"BRANCH_ID_DESC\",\"COUNTERPARTY_ADDRESS\",\"SOURCE_BANK\",\"COUNTRPARTY_ACCOUNT\",\"COUNTERPARTY_AMOUNT\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"TRAN_ID\",\"ACCT_TYPE\",\"HOST_USER_ID\",\"HOST_ID\",\"TXN_CODE\",\"SOURCE_OR_DEST_ACCT_ID\",\"REF_AMT\",\"SEQUENCE_NUMBER\",\"TRAN_CRNCY_CODE\",\"ACCT_NAME\",\"TRAN_SRL_NO\",\"COUNTERPARTY_BANK\",\"COUNTERPARTY_BANK_ADDRESS\",\"CX_CUST_ID\",\"EVENT_NAME\",\"AVL_BAL\",\"TRAN_POSTED_DT\",\"TXN_VALUE_DATE\",\"SYSTEM_COUNTRY\",\"DESC\",\"MSG_SOURCE\",\"SCHEME_CODE\",\"KEYS\",\"COUNTERPARTY_CURRENCY_LCY\",\"EVENT_TIME\",\"CHANNEL_ID\",\"COUNTERPARTY_BUSINESS\",\"COUNTERPARTY_BIC\",\"TXN_STATUS\",\"CX_ACCT_ID\",\"TXN_AMT\",\"COUNTERPARTY_AMOUNT_LCY\",\"COUNTERPARTY_COUNTRY_CODE\",\"TELLER_NUMBER\",\"COUNTERPARTY_NAME\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"COUNTERPARTY_CURRENCY\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"ACCOUNT_OWNERSHIP\",\"CUST_ID\",\"REF_CURNCY\",\"ACCOUNT_OPENDATE\",\"COUNTERPARTY_BANK_CODE\",\"TRAN_TYPE\",\"INSTRUMENT_TYPE\",\"REMARKS\"" +
              " FROM EVENT_FT_TRB_TXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [BRANCH_ID_DESC],[COUNTERPARTY_ADDRESS],[SOURCE_BANK],[COUNTRPARTY_ACCOUNT],[COUNTERPARTY_AMOUNT],[ONLINE_BATCH],[TRAN_SUB_TYPE],[SYSTEM_TIME],[TXN_DR_CR],[TRAN_ID],[ACCT_TYPE],[HOST_USER_ID],[HOST_ID],[TXN_CODE],[SOURCE_OR_DEST_ACCT_ID],[REF_AMT],[SEQUENCE_NUMBER],[TRAN_CRNCY_CODE],[ACCT_NAME],[TRAN_SRL_NO],[COUNTERPARTY_BANK],[COUNTERPARTY_BANK_ADDRESS],[CX_CUST_ID],[EVENT_NAME],[AVL_BAL],[TRAN_POSTED_DT],[TXN_VALUE_DATE],[SYSTEM_COUNTRY],[DESC],[MSG_SOURCE],[SCHEME_CODE],[KEYS],[COUNTERPARTY_CURRENCY_LCY],[EVENT_TIME],[CHANNEL_ID],[COUNTERPARTY_BUSINESS],[COUNTERPARTY_BIC],[TXN_STATUS],[CX_ACCT_ID],[TXN_AMT],[COUNTERPARTY_AMOUNT_LCY],[COUNTERPARTY_COUNTRY_CODE],[TELLER_NUMBER],[COUNTERPARTY_NAME],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[COUNTERPARTY_CURRENCY],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[ACCOUNT_OWNERSHIP],[CUST_ID],[REF_CURNCY],[ACCOUNT_OPENDATE],[COUNTERPARTY_BANK_CODE],[TRAN_TYPE],[INSTRUMENT_TYPE],[REMARKS]" +
              " FROM EVENT_FT_TRB_TXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`BRANCH_ID_DESC`,`COUNTERPARTY_ADDRESS`,`SOURCE_BANK`,`COUNTRPARTY_ACCOUNT`,`COUNTERPARTY_AMOUNT`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`SYSTEM_TIME`,`TXN_DR_CR`,`TRAN_ID`,`ACCT_TYPE`,`HOST_USER_ID`,`HOST_ID`,`TXN_CODE`,`SOURCE_OR_DEST_ACCT_ID`,`REF_AMT`,`SEQUENCE_NUMBER`,`TRAN_CRNCY_CODE`,`ACCT_NAME`,`TRAN_SRL_NO`,`COUNTERPARTY_BANK`,`COUNTERPARTY_BANK_ADDRESS`,`CX_CUST_ID`,`EVENT_NAME`,`AVL_BAL`,`TRAN_POSTED_DT`,`TXN_VALUE_DATE`,`SYSTEM_COUNTRY`,`DESC`,`MSG_SOURCE`,`SCHEME_CODE`,`KEYS`,`COUNTERPARTY_CURRENCY_LCY`,`EVENT_TIME`,`CHANNEL_ID`,`COUNTERPARTY_BUSINESS`,`COUNTERPARTY_BIC`,`TXN_STATUS`,`CX_ACCT_ID`,`TXN_AMT`,`COUNTERPARTY_AMOUNT_LCY`,`COUNTERPARTY_COUNTRY_CODE`,`TELLER_NUMBER`,`COUNTERPARTY_NAME`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`COUNTERPARTY_CURRENCY`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`ACCOUNT_OWNERSHIP`,`CUST_ID`,`REF_CURNCY`,`ACCOUNT_OPENDATE`,`COUNTERPARTY_BANK_CODE`,`TRAN_TYPE`,`INSTRUMENT_TYPE`,`REMARKS`" +
              " FROM EVENT_FT_TRB_TXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_TRB_TXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"BRANCH_ID_DESC\",\"COUNTERPARTY_ADDRESS\",\"SOURCE_BANK\",\"COUNTRPARTY_ACCOUNT\",\"COUNTERPARTY_AMOUNT\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"TRAN_ID\",\"ACCT_TYPE\",\"HOST_USER_ID\",\"HOST_ID\",\"TXN_CODE\",\"SOURCE_OR_DEST_ACCT_ID\",\"REF_AMT\",\"SEQUENCE_NUMBER\",\"TRAN_CRNCY_CODE\",\"ACCT_NAME\",\"TRAN_SRL_NO\",\"COUNTERPARTY_BANK\",\"COUNTERPARTY_BANK_ADDRESS\",\"CX_CUST_ID\",\"EVENT_NAME\",\"AVL_BAL\",\"TRAN_POSTED_DT\",\"TXN_VALUE_DATE\",\"SYSTEM_COUNTRY\",\"DESC\",\"MSG_SOURCE\",\"SCHEME_CODE\",\"KEYS\",\"COUNTERPARTY_CURRENCY_LCY\",\"EVENT_TIME\",\"CHANNEL_ID\",\"COUNTERPARTY_BUSINESS\",\"COUNTERPARTY_BIC\",\"TXN_STATUS\",\"CX_ACCT_ID\",\"TXN_AMT\",\"COUNTERPARTY_AMOUNT_LCY\",\"COUNTERPARTY_COUNTRY_CODE\",\"TELLER_NUMBER\",\"COUNTERPARTY_NAME\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"COUNTERPARTY_CURRENCY\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"ACCOUNT_OWNERSHIP\",\"CUST_ID\",\"REF_CURNCY\",\"ACCOUNT_OPENDATE\",\"COUNTERPARTY_BANK_CODE\",\"TRAN_TYPE\",\"INSTRUMENT_TYPE\",\"REMARKS\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[BRANCH_ID_DESC],[COUNTERPARTY_ADDRESS],[SOURCE_BANK],[COUNTRPARTY_ACCOUNT],[COUNTERPARTY_AMOUNT],[ONLINE_BATCH],[TRAN_SUB_TYPE],[SYSTEM_TIME],[TXN_DR_CR],[TRAN_ID],[ACCT_TYPE],[HOST_USER_ID],[HOST_ID],[TXN_CODE],[SOURCE_OR_DEST_ACCT_ID],[REF_AMT],[SEQUENCE_NUMBER],[TRAN_CRNCY_CODE],[ACCT_NAME],[TRAN_SRL_NO],[COUNTERPARTY_BANK],[COUNTERPARTY_BANK_ADDRESS],[CX_CUST_ID],[EVENT_NAME],[AVL_BAL],[TRAN_POSTED_DT],[TXN_VALUE_DATE],[SYSTEM_COUNTRY],[DESC],[MSG_SOURCE],[SCHEME_CODE],[KEYS],[COUNTERPARTY_CURRENCY_LCY],[EVENT_TIME],[CHANNEL_ID],[COUNTERPARTY_BUSINESS],[COUNTERPARTY_BIC],[TXN_STATUS],[CX_ACCT_ID],[TXN_AMT],[COUNTERPARTY_AMOUNT_LCY],[COUNTERPARTY_COUNTRY_CODE],[TELLER_NUMBER],[COUNTERPARTY_NAME],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[COUNTERPARTY_CURRENCY],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[ACCOUNT_OWNERSHIP],[CUST_ID],[REF_CURNCY],[ACCOUNT_OPENDATE],[COUNTERPARTY_BANK_CODE],[TRAN_TYPE],[INSTRUMENT_TYPE],[REMARKS]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`BRANCH_ID_DESC`,`COUNTERPARTY_ADDRESS`,`SOURCE_BANK`,`COUNTRPARTY_ACCOUNT`,`COUNTERPARTY_AMOUNT`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`SYSTEM_TIME`,`TXN_DR_CR`,`TRAN_ID`,`ACCT_TYPE`,`HOST_USER_ID`,`HOST_ID`,`TXN_CODE`,`SOURCE_OR_DEST_ACCT_ID`,`REF_AMT`,`SEQUENCE_NUMBER`,`TRAN_CRNCY_CODE`,`ACCT_NAME`,`TRAN_SRL_NO`,`COUNTERPARTY_BANK`,`COUNTERPARTY_BANK_ADDRESS`,`CX_CUST_ID`,`EVENT_NAME`,`AVL_BAL`,`TRAN_POSTED_DT`,`TXN_VALUE_DATE`,`SYSTEM_COUNTRY`,`DESC`,`MSG_SOURCE`,`SCHEME_CODE`,`KEYS`,`COUNTERPARTY_CURRENCY_LCY`,`EVENT_TIME`,`CHANNEL_ID`,`COUNTERPARTY_BUSINESS`,`COUNTERPARTY_BIC`,`TXN_STATUS`,`CX_ACCT_ID`,`TXN_AMT`,`COUNTERPARTY_AMOUNT_LCY`,`COUNTERPARTY_COUNTRY_CODE`,`TELLER_NUMBER`,`COUNTERPARTY_NAME`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`COUNTERPARTY_CURRENCY`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`ACCOUNT_OWNERSHIP`,`CUST_ID`,`REF_CURNCY`,`ACCOUNT_OPENDATE`,`COUNTERPARTY_BANK_CODE`,`TRAN_TYPE`,`INSTRUMENT_TYPE`,`REMARKS`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

