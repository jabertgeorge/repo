/* caseTree.js start */
var mstrData="[{\"text\": \"Customer Info\",\"id\": \""+custRmlId+"\",\"icon\":\"../common/images/jstree/root.png\",\"type\":\"demo\",\"state\":{\"opened\":true},\"metadata\":{\"nodeData\": {},\"editable\": false},\"children\":[{\"text\":\"Customer ID\",\"id\":\"customer\",\"parent\":\"customerRelam\",\"icon\":\"images/root.jpg\",\"type\":\"folder\",\"state\":{\"opened\":true},\"metadata\":{\"nodeData\":{},\"editable\":false}"+customerTab+"},{\"text\":\"Account ID\",\"id\":\"account\",\"parent\":\"customerRelam\",\"icon\":\"images/root.jpg\",\"type\": \"folder\",\"state\":{\"opened\":true},\"metadata\":{\"nodeData\":{},\"editable\":false}"+accountTab+"}]}]";

   $(function () {
    $("#"+treeId).jstree({
        "plugins": ["themes", "ui", "types","unique", "contextmenu"],
         "themes": {
            "name": "apple",
            "url": "themes/apple/style.css",
            "dir": "themes",
            "stripes": "true"
        },        
        "ui" : {
        	"initially_select" : [ "customerRelam3" ]
    	},
        "types": {
            "default": {
                "icon": "glyphicon glyphicon-flash"
            },
            "demo": {
                "icon": "../common/images/jstree/root.png"
            },
            "file": {
                "icon": "../common/images/jstree/file.png"
            },
            "folder": {
                "icon": "images/root.jpg"
            },
            "find": {
                "icon": "../common/images/jstree/find.png"
            }
        },
        "core": {
	    "html_titles" : true,
	    "load_open" : true,
            "multiple": false,
            "check_callback": true,
            "data": getJSONDoc(mstrData)
        },
        "contextmenu": {
            "select_node": true,
            "show_at_node": true,
            items: function (o) {
		 if(o.id!="customer" && o.id!="account" && o.id.indexOf("customerRelam")<0){
		return{
		 "create": {
                        "label":"View Ananlysis",
                        "action": function (data) {
				viewLinkAnanlysis(o.id.split("#")[2]);
				}
			}

			};
			}else{
				return {};
			}
		}
	}	
    	}).bind("activate_node.jstree", function (e, data) {
		if(data.node.id.split("#")[1]=="C"){
		addTab('Customer',data.node.id.split("#")[2]);
		}else if(data.node.id.split("#")[1]=="A"){
		addTab('Account',data.node.id.split("#")[2]);
		}
	});
  });
/* caseTree.js end */
