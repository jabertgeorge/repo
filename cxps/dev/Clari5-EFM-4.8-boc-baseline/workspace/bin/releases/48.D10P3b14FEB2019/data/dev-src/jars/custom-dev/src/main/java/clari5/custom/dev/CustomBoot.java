package clari5.custom.dev;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CustomBoot {

    public static CxConnection getConnection(){
        //Clari5.batchBootstrap("test", "efm-clari5.conf");

        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
    }

    public static boolean saveData(String Pur_ACCT_OPEN_CODE, String ANTICIPATED_VOL, String EXP_MODE_OF_TRAN, String acctid, String custid){

        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO " + tableName() + " (entity_id,created_date,pur_acct_open_code, anticipated_vol, exp_mode_of_tran, acctid, custid) VALUES (NEXT VALUE FOR TABLE_CUSTOM_BOOT_seq,GETDATE(),?, ?, ?, ?, ?);");
            int i = 1;
            ps.setString(i++, Pur_ACCT_OPEN_CODE);
            ps.setString(i++, ANTICIPATED_VOL);
            ps.setString(i++, EXP_MODE_OF_TRAN);
            ps.setString(i++, acctid);
            ps.setString(i++, custid);
            ps.executeUpdate();
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static String tableName(){
        return "table_custom_boot";
    }

/*    public static void main(String[] args) {
        System.out.println(getConnection());
        CustomBoot cb = new CustomBoot();
        String a = "we234";
        String b = "re123";
        String c = "de2345";
        String d = "fe2345";
        String e = "gfr322";
        saveData(a, b, c, d, e);
        System.out.println("Data inserted...");
    }*/
}
