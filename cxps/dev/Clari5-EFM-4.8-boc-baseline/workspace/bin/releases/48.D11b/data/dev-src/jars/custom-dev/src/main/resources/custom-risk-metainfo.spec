meta-config {

    attributes {
        responssiveness {
            belongs-to : customer
            field-name : riskCategory
            evidence : "customer.getRiskCategory()"
            desc: "responssiveness of customer"
            type: refcode.RESPONSSIVENESS
        }
        cust_type {
            belongs-to : customer
            field-name : custType
            evidence : "customer.getCustType()"
            desc : "cust type"
            type : refcode.CUST_TYPE
        }
        cust_profession_type_risk {
            belongs-to : customer
            field-name : custProfessionTypeRisk
            evidence : "customer.getCustProfessionTypeRisk()"
            desc : "cust profession type risk"
            type : refcode.RISK
        }
        country_code_risk {
            belongs-to : customer
            field-name : countryCodeRisk
            evidence : "customer.getCountryCodeRisk()"
            desc : "country code risk"
            type : refcode.RISK
        }
        source_of_funds_risk {
            belongs-to : account
            field-name : sourceOfFundsRisk
            evidence : "account.getSourceOfFundsRisk()"
            desc : "source of funds risk"
            type : refcode.RISK
        }
        constitution_risk {
            belongs-to:customer
            field-name: constitution-risk
            evidence: "customer.getConstitutionRisk()"
            desc : "constition risk"
            type : refcode.RISK
        }

    }
}