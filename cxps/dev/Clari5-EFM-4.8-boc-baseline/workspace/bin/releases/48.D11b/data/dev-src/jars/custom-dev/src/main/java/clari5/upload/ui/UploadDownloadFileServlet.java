package clari5.upload.ui;

import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import clari5.rdbms.Rdbms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.text.SimpleDateFormat;

public class UploadDownloadFileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static CxpsLogger logger = CxpsLogger.getLogger(UploadDownloadFileServlet.class);
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String status;
    private String timeStamp = null;
    private int numberOfColumnsDB = 0;
    private String insertSql = null;
    private int rowNum = 0;
    private int columnNum = 0;
    static int count = 0;
    private String requestFileName = null;
    private String fileBackupPath = null;
    private String requestFileNameSplit = null;
    Map<Integer, String> hmap = new HashMap<Integer, String>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String requestTableName;
        HttpSession session;
        ResultSetMetaData rsData;
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        session = request.getSession();
        String userId = (String) session.getAttribute("userId");
        requestTableName = (String) session.getAttribute("tableName");

        fileBackupPath = (String) session.getAttribute("FilePath");
        requestFileName = (String) session.getAttribute("FileName");
        logger.debug("UploadDownloadFileServlet : "+userId+":"+requestTableName+":"+fileBackupPath+":"+requestFileName);
        //reading the type of file (xls,xlsx)
        int s = fileBackupPath.lastIndexOf('.');
        logger.info("index" + s);
        if (s >= 0)
            requestFileNameSplit = fileBackupPath.substring(s + 1);

        //calling the respective insert function based on file type
        if (requestFileNameSplit.equalsIgnoreCase("xlsx")) {
            count = 0;
            xlsxFileInsert(requestTableName, response);
        } else if (requestFileNameSplit.equalsIgnoreCase("xls")) {
            count = 0;
            xlsFileInsert(requestTableName, response);
        }

        logger.info("File Type[" + requestFileNameSplit + "] and the table is [" + requestTableName + "]");
        //UploadUiAudit.updateAudit(requestTableName, userId, status, "", requestFileName, "Insert");
        request.setAttribute("TOTAL", count);
        request.getRequestDispatcher("/DisplayInsert").include(request, response);
    }

    private String getCellValue(Cell cell){
        String value = "",type="";
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                type="Numeric";
                value = cell.getNumericCellValue()+"";
                if(value != null && value.endsWith(".0"))
                    value = value.substring(0,value.length()-2);
                break;
            case Cell.CELL_TYPE_STRING:
                type = "String";
                value = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                type = "Formula";
                value = "";
                break;
            case Cell.CELL_TYPE_BLANK:
                type = "Blank";
                value = "";
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                type = "Boolean";
                value = cell.getBooleanCellValue()+"";
                break;
            case Cell.CELL_TYPE_ERROR:
                type = "Error";
                value = cell.getErrorCellValue()+"";
                break;
            default:
                type = "Default Date";
                value = cell.getDateCellValue().toString();
                break;
        }
        logger.debug("getCellValue : "+cell.getCellType()+":"+type+" : "+value);
        return value;
    }

    public void xlsFileInsert(String requestTableName, HttpServletResponse response) {

        HSSFWorkbook wb;
        HSSFSheet ws;
        BigInteger temp = null;
        int error = 0;
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            con = Rdbms.getAppConnection();
            wb = new HSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum();
            columnNum = ws.getRow(0).getLastCellNum();
            Iterator<Row> rowIterator = ws.iterator();

            System.out.println("UploadDownloadFileServlet : "+rowNum+" - "+columnNum);
            int rowNum = 0;
            String colNames = "",psArgs="";
            while (rowIterator.hasNext()) {
                int cellPos = 1;
                Row row1 = rowIterator.next();
                Iterator<Cell> cellIterator = row1.cellIterator();
                boolean execute = false;
                while (cellIterator.hasNext()) {
                logger.debug("ROW NUM : CELL POS : "+rowNum+"-"+cellPos);
                    Cell cell1 = cellIterator.next();
                    if(rowNum == 0){
                            colNames += getCellValue(cell1);
                            if(cellIterator.hasNext()) {
                                colNames += ",";
                            }
                    } else {
                        execute = true;
                        logger.debug("PS set string : "+cellPos+":"+getCellValue(cell1));
                        psArgs += "'"+getCellValue(cell1)+"'";
                        if(cellIterator.hasNext()) {
                            psArgs += ",";
                        }
                        cellPos++;
                    }
                }
                try {
                    if(execute) {
                        logger.debug("xls insert : "+"insert into "+requestTableName+" ("+colNames+") values ("+psArgs+")");
                        ps = con.prepareStatement("insert into "+requestTableName+" ("+colNames+") values ("+psArgs+")");
                        ps.executeUpdate();
                    }
                    count++;
                } catch (SQLException e) {
                    e.printStackTrace();
                    //error++;
                    status = "Failure";
                    logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
                }
                rowNum++;
            }
            if (error > 0) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Error in Inserting " + error + " records  Kindly check the logs for more information \" " + ");");
                out.println("</script>");

            }

            con.commit();
            con.close();
            if (error == 0) {
                status = "Success";
            }
        } catch (SQLException | IOException e) {

            status = "Failure";
            logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void xlsxFileInsert(String requestTableName, HttpServletResponse response) {
        XSSFWorkbook wb;
        XSSFSheet ws;
        int error = 0;

        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            con = Rdbms.getAppConnection();
            wb = new XSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum() + 1;
            int rowNum = 0;
            columnNum = ws.getRow(0).getLastCellNum();
            String colNames = "";
            String psArgs = "";
            Iterator<Row> rowIterator = ws.iterator();
            while (rowIterator.hasNext()) {
                int cellPos = 0;
                Row row1 = rowIterator.next();
                Iterator<Cell> cellIterator = row1.cellIterator();
                boolean execute = false;
                while (cellIterator.hasNext()) {
                    Cell cell1 = cellIterator.next();
                    if(rowNum == 0){
                        colNames += getCellValue(cell1);
                        if(cellIterator.hasNext()) {
                            colNames += ",";
                        }
                    } else {
                        execute = true;
                        logger.debug("XSSF : PS set string : "+cellPos+":"+getCellValue(cell1));
                        psArgs += "'"+getCellValue(cell1)+"'";
                        if(cellIterator.hasNext()) {
                            psArgs += ",";
                        }
                        cellPos++;
                    }

//                    if (cell1.getCellType() == HSSFCell.CELL_TYPE_STRING)  // insertion of String data
//                    {
//                        ps.setString(cellPos, cell1.getStringCellValue().trim());
//                    } else if (cell1.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//                        if (HSSFDateUtil.isCellDateFormatted(cell1)) {      // Insertion of Date cell
//                            timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(cell1.getDateCellValue());
//                            ps.setString(cellPos, timeStamp.trim());
//                        } else {
//                            BigInteger temp = BigDecimal.valueOf(cell1.getNumericCellValue()).toBigInteger();//Insertion of number field
//                            ps.setString(cellPos, String.valueOf(temp).trim());
//                        }
//                    }
                }
                try {
                    if(execute) {
                        logger.debug("xlsx insert : "+"insert into "+requestTableName+" ("+colNames+") values ("+psArgs+")");
                        ps = con.prepareStatement("insert into "+requestTableName+" ("+colNames+") values ("+psArgs+")");
                        ps.executeUpdate();
                    }
                    count++;
                } catch (SQLException e) {
                    error++;
                    status = "Failure";
                    logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
                }
                rowNum++;
            }
            if (error > 0) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Error in Inserting " + error + " records  Kindly check the logs for more information \");");
                out.println("</script>");

            }

            con.commit();
            con.close();
            if (error == 0) status = "Success";

        } catch (SQLException | IOException e) {

            status = "Failure";
            logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
            hmap.clear();
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

    }

    public static void main(String s[]){
        double d = 8.0;
        String value = d + "";
        if(value.endsWith(".0"))
            System.out.println(value.substring(0,value.length()-2));
        String resp = "{ \"status\":\"Critical\",\"prod-status\" : {\"status\": \"success\",\"errorDetails\": \"\"}, " +
                "\"matchedResults\" : {\"maxScore\" : \"1.000\",\"total\" : 1,\"matchedRecords\" : [ {\"id\" : \"116\",\"score\" : \"1.000\",\"listName\" : \"CUSTOM\"," +
                "\"fields\" : [ {\"score\" : \"1.000\",\"name\" : \"name\",\"value\" : \"vijay\",\"queryValue\" : \"vijay\"} ],\"Description\" : null} ]} " +

                ",\"prod-status\" : {\"status\": \"success\",\"errorDetails\": \"\"}, " +
                "\"matchedResults\" : {\"maxScore\" : \"1.000\",\"total\" : 1,\"matchedRecords\" : [ {\"id\" : \"116\",\"score\" : \"1.000\",\"listName\" : \"CUSTOM\"," +
                "\"fields\" : [ {\"score\" : \"1.000\",\"name\" : \"name\",\"value\" : \"vijay\",\"queryValue\" : \"vijay\"} ],\"Description\" : null} ]} " +

                "}";
        resp = "{ \"status\":\"Critical\",\"prodstatus\" : {\"status\": \"success\",\"errorDetails\": \"\"}, \"matchedResults\" : {\n" +
                "  \"maxScore\" : \"0.910\",\n" +
                "  \"total\" : 3,\n" +
                "  \"matchedRecords\" : [ {\n" +
                "    \"id\" : \"AU_17193\",\n" +
                "    \"score\" : \"0.910\",\n" +
                "    \"listName\" : \"au\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"score\" : \"1.000\",\n" +
                "      \"name\" : \"country_name\",\n" +
                "      \"value\" : \"cuba\",\n" +
                "      \"queryValue\" : \"CUBA\"\n" +
                "    }, {\n" +
                "      \"score\" : \"0.821\",\n" +
                "      \"name\" : \"name\",\n" +
                "      \"value\" : \"FAZEL MOHAMMAD MAZLOOM\",\n" +
                "      \"queryValue\" : \"FAZEL MOHAMMAD\"\n" +
                "    } ],\n" +
                "    \"Description\" : null\n" +
                "  }, {\n" +
                "    \"id\" : \"AU_247454\",\n" +
                "    \"score\" : \"0.846\",\n" +
                "    \"listName\" : \"au\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"score\" : \"1.000\",\n" +
                "      \"name\" : \"country_name\",\n" +
                "      \"value\" : \"cuba\",\n" +
                "      \"queryValue\" : \"CUBA\"\n" +
                "    }, {\n" +
                "      \"score\" : \"0.691\",\n" +
                "      \"name\" : \"name\",\n" +
                "      \"value\" : \"MAZLOOM, FAZ MOHAMMAD\",\n" +
                "      \"queryValue\" : \"FAZEL MOHAMMAD\"\n" +
                "    } ],\n" +
                "    \"Description\" : null\n" +
                "  }, {\n" +
                "    \"id\" : \"AU_247455\",\n" +
                "    \"score\" : \"0.834\",\n" +
                "    \"listName\" : \"au\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"score\" : \"1.000\",\n" +
                "      \"name\" : \"country_name\",\n" +
                "      \"value\" : \"cuba\",\n" +
                "      \"queryValue\" : \"CUBA\"\n" +
                "    }, {\n" +
                "      \"score\" : \"0.668\",\n" +
                "      \"name\" : \"name\",\n" +
                "      \"value\" : \"FAZL, MOLAH\",\n" +
                "      \"queryValue\" : \"FAZEL MOHAMMAD\"\n" +
                "    } ],\n" +
                "    \"Description\" : null\n" +
                "  } ]\n" +
                "} }";
    }

}
