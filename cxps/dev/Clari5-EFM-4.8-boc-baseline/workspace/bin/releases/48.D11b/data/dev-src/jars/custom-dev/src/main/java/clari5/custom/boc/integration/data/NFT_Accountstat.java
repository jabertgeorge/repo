package clari5.custom.boc.integration.data;

public class NFT_Accountstat extends ITableData {

    private String tableName = "NFT_ACCOUNT_STAT";
    private String event_type = "NFT_Accountstat";

    private String event_name;
    private String eventtype;
    private String eventsubtype;
    private String source;
    private String msgsource;
    private String host_user_id;
    private String channel;
    private String keys;
    private String entitytype;
    private String host_id;
    private String status;
    private String eventts;
    private String custid;
    private String cx_cust_id;
    private String cx_acct_id;
    private String acid;
    private String init_acct_status;
    private String final_acct_status;
    private String account_id;
    private String acct_name;
    private String systemcountry;
    private String reservedfield1;
    private String reservedfield2;
    private String reservedfield3;
    private String reservedfield4;
    private String reservedfield5;
    private String event_id;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getEventsubtype() {
        return eventsubtype;
    }

    public void setEventsubtype(String eventsubtype) {
        this.eventsubtype = eventsubtype;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getMsgsource() {
        return msgsource;
    }

    public void setMsgsource(String msgsource) {
        this.msgsource = msgsource;
    }

    public String getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(String host_user_id) {
        this.host_user_id = host_user_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getEntitytype() {
        return entitytype;
    }

    public void setEntitytype(String entitytype) {
        this.entitytype = entitytype;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCx_cust_id() {
        return cx_cust_id;
    }

    public void setCx_cust_id(String cx_cust_id) {
        this.cx_cust_id = cx_cust_id;
    }

    public String getCx_acct_id() {
        return cx_acct_id;
    }

    public void setCx_acct_id(String cx_acct_id) {
        this.cx_acct_id = cx_acct_id;
    }

    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    public String getInit_acct_status() {
        return init_acct_status;
    }

    public void setInit_acct_status(String init_acct_status) {
        this.init_acct_status = init_acct_status;
    }

    public String getFinal_acct_status() {
        return final_acct_status;
    }

    public void setFinal_acct_status(String final_acct_status) {
        this.final_acct_status = final_acct_status;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAcct_name() {
        return acct_name;
    }

    public void setAcct_name(String acct_name) {
        this.acct_name = acct_name;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getReservedfield1() {
        return reservedfield1;
    }

    public void setReservedfield1(String reservedfield1) {
        this.reservedfield1 = reservedfield1;
    }

    public String getReservedfield2() {
        return reservedfield2;
    }

    public void setReservedfield2(String reservedfield2) {
        this.reservedfield2 = reservedfield2;
    }

    public String getReservedfield3() {
        return reservedfield3;
    }

    public void setReservedfield3(String reservedfield3) {
        this.reservedfield3 = reservedfield3;
    }

    public String getReservedfield4() {
        return reservedfield4;
    }

    public void setReservedfield4(String reservedfield4) {
        this.reservedfield4 = reservedfield4;
    }

    public String getReservedfield5() {
        return reservedfield5;
    }

    public void setReservedfield5(String reservedfield5) {
        this.reservedfield5 = reservedfield5;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
