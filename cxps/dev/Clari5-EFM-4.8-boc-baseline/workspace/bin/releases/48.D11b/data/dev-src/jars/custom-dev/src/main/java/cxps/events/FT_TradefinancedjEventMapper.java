// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_TradefinancedjEventMapper extends EventMapper<FT_TradefinancedjEvent> {

public FT_TradefinancedjEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_TradefinancedjEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_TradefinancedjEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_TradefinancedjEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_TradefinancedjEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_TradefinancedjEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_TradefinancedjEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getUpdatedOn());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getMaxScore());
            preparedStatement.setString(i++, obj.getPartTranSrlNum());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getDescriptionCode());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getDjId());
            preparedStatement.setString(i++, obj.getSanctionsReferencesListProviderCode());
            preparedStatement.setString(i++, obj.getScore());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getSystem());
            preparedStatement.setTimestamp(i++, obj.getCreatedOn());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getRuleName());
            preparedStatement.setString(i++, obj.getName());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getListName());
            preparedStatement.setString(i++, obj.getValue());
            preparedStatement.setString(i++, obj.getQueryValue());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_TRADE_FINANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_TradefinancedjEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_TradefinancedjEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADE_FINANCE"));
        putList = new ArrayList<>();

        for (FT_TradefinancedjEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "UPDATED_ON", String.valueOf(obj.getUpdatedOn()));
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "MAX_SCORE",  obj.getMaxScore());
            p = this.insert(p, "PART_TRAN_SRL_NUM",  obj.getPartTranSrlNum());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "DESCRIPTION_CODE",  obj.getDescriptionCode());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "DJ_ID",  obj.getDjId());
            p = this.insert(p, "SANCTIONS_REFERENCES_LIST_PROVIDER_CODE",  obj.getSanctionsReferencesListProviderCode());
            p = this.insert(p, "SCORE",  obj.getScore());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "SYSTEM",  obj.getSystem());
            p = this.insert(p, "CREATED_ON", String.valueOf(obj.getCreatedOn()));
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "RULE_NAME",  obj.getRuleName());
            p = this.insert(p, "NAME",  obj.getName());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "LIST_NAME",  obj.getListName());
            p = this.insert(p, "VALUE",  obj.getValue());
            p = this.insert(p, "QUERY_VALUE",  obj.getQueryValue());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_TRADE_FINANCE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_TRADE_FINANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_TRADE_FINANCE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_TradefinancedjEvent obj = new FT_TradefinancedjEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setUpdatedOn(rs.getTimestamp("UPDATED_ON"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setMaxScore(rs.getString("MAX_SCORE"));
    obj.setPartTranSrlNum(rs.getString("PART_TRAN_SRL_NUM"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setDescriptionCode(rs.getString("DESCRIPTION_CODE"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setDjId(rs.getString("DJ_ID"));
    obj.setSanctionsReferencesListProviderCode(rs.getString("SANCTIONS_REFERENCES_LIST_PROVIDER_CODE"));
    obj.setScore(rs.getString("SCORE"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setSystem(rs.getString("SYSTEM"));
    obj.setCreatedOn(rs.getTimestamp("CREATED_ON"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setRuleName(rs.getString("RULE_NAME"));
    obj.setName(rs.getString("NAME"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setListName(rs.getString("LIST_NAME"));
    obj.setValue(rs.getString("VALUE"));
    obj.setQueryValue(rs.getString("QUERY_VALUE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_TRADE_FINANCE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_TradefinancedjEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_TradefinancedjEvent> events;
 FT_TradefinancedjEvent obj = new FT_TradefinancedjEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADE_FINANCE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_TradefinancedjEvent obj = new FT_TradefinancedjEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setUpdatedOn(EventHelper.toTimestamp(getColumnValue(rs, "UPDATED_ON")));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setMaxScore(getColumnValue(rs, "MAX_SCORE"));
            obj.setPartTranSrlNum(getColumnValue(rs, "PART_TRAN_SRL_NUM"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setDescriptionCode(getColumnValue(rs, "DESCRIPTION_CODE"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setDjId(getColumnValue(rs, "DJ_ID"));
            obj.setSanctionsReferencesListProviderCode(getColumnValue(rs, "SANCTIONS_REFERENCES_LIST_PROVIDER_CODE"));
            obj.setScore(getColumnValue(rs, "SCORE"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setSystem(getColumnValue(rs, "SYSTEM"));
            obj.setCreatedOn(EventHelper.toTimestamp(getColumnValue(rs, "CREATED_ON")));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setRuleName(getColumnValue(rs, "RULE_NAME"));
            obj.setName(getColumnValue(rs, "NAME"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setListName(getColumnValue(rs, "LIST_NAME"));
            obj.setValue(getColumnValue(rs, "VALUE"));
            obj.setQueryValue(getColumnValue(rs, "QUERY_VALUE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADE_FINANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADE_FINANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"UPDATED_ON\",\"EVENT_SUBTYPE\",\"EVENT_TYPE\",\"MAX_SCORE\",\"PART_TRAN_SRL_NUM\",\"TRAN_ID\",\"SOURCE\",\"CUST_ID\",\"DESCRIPTION_CODE\",\"EVENTTS\",\"DJ_ID\",\"SANCTIONS_REFERENCES_LIST_PROVIDER_CODE\",\"SCORE\",\"TRAN_DATE\",\"SYSTEM\",\"CREATED_ON\",\"EVENT_NAME\",\"RULE_NAME\",\"NAME\",\"HOST_ID\",\"LIST_NAME\",\"VALUE\",\"QUERY_VALUE\"" +
              " FROM EVENT_FT_TRADE_FINANCE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [UPDATED_ON],[EVENT_SUBTYPE],[EVENT_TYPE],[MAX_SCORE],[PART_TRAN_SRL_NUM],[TRAN_ID],[SOURCE],[CUST_ID],[DESCRIPTION_CODE],[EVENTTS],[DJ_ID],[SANCTIONS_REFERENCES_LIST_PROVIDER_CODE],[SCORE],[TRAN_DATE],[SYSTEM],[CREATED_ON],[EVENT_NAME],[RULE_NAME],[NAME],[HOST_ID],[LIST_NAME],[VALUE],[QUERY_VALUE]" +
              " FROM EVENT_FT_TRADE_FINANCE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`UPDATED_ON`,`EVENT_SUBTYPE`,`EVENT_TYPE`,`MAX_SCORE`,`PART_TRAN_SRL_NUM`,`TRAN_ID`,`SOURCE`,`CUST_ID`,`DESCRIPTION_CODE`,`EVENTTS`,`DJ_ID`,`SANCTIONS_REFERENCES_LIST_PROVIDER_CODE`,`SCORE`,`TRAN_DATE`,`SYSTEM`,`CREATED_ON`,`EVENT_NAME`,`RULE_NAME`,`NAME`,`HOST_ID`,`LIST_NAME`,`VALUE`,`QUERY_VALUE`" +
              " FROM EVENT_FT_TRADE_FINANCE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_TRADE_FINANCE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"UPDATED_ON\",\"EVENT_SUBTYPE\",\"EVENT_TYPE\",\"MAX_SCORE\",\"PART_TRAN_SRL_NUM\",\"TRAN_ID\",\"SOURCE\",\"CUST_ID\",\"DESCRIPTION_CODE\",\"EVENTTS\",\"DJ_ID\",\"SANCTIONS_REFERENCES_LIST_PROVIDER_CODE\",\"SCORE\",\"TRAN_DATE\",\"SYSTEM\",\"CREATED_ON\",\"EVENT_NAME\",\"RULE_NAME\",\"NAME\",\"HOST_ID\",\"LIST_NAME\",\"VALUE\",\"QUERY_VALUE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[UPDATED_ON],[EVENT_SUBTYPE],[EVENT_TYPE],[MAX_SCORE],[PART_TRAN_SRL_NUM],[TRAN_ID],[SOURCE],[CUST_ID],[DESCRIPTION_CODE],[EVENTTS],[DJ_ID],[SANCTIONS_REFERENCES_LIST_PROVIDER_CODE],[SCORE],[TRAN_DATE],[SYSTEM],[CREATED_ON],[EVENT_NAME],[RULE_NAME],[NAME],[HOST_ID],[LIST_NAME],[VALUE],[QUERY_VALUE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`UPDATED_ON`,`EVENT_SUBTYPE`,`EVENT_TYPE`,`MAX_SCORE`,`PART_TRAN_SRL_NUM`,`TRAN_ID`,`SOURCE`,`CUST_ID`,`DESCRIPTION_CODE`,`EVENTTS`,`DJ_ID`,`SANCTIONS_REFERENCES_LIST_PROVIDER_CODE`,`SCORE`,`TRAN_DATE`,`SYSTEM`,`CREATED_ON`,`EVENT_NAME`,`RULE_NAME`,`NAME`,`HOST_ID`,`LIST_NAME`,`VALUE`,`QUERY_VALUE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

