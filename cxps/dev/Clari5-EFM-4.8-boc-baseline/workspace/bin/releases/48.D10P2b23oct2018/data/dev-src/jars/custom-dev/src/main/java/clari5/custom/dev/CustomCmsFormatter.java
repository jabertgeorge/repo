package clari5.custom.dev;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import cxps.eoi.IncidentEvent;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by deepak on 21/8/17.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public String getCaseAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        String brCode = null;
        String entityId = caseEvent.getEntityId();
        System.out.println("Entity id inside getCaseAssignee is : " + entityId);
        if (!entityId.startsWith("A_F_") && !entityId.startsWith("C_F_")) {
            return super.getCaseAssignee(caseEvent);
        }
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        Map<String, Integer> userGroupMap = null;
        try {
            List list = cmsUserCaseAssignment.process(entityId);
            if (list.isEmpty()) {
                System.out.println("BR CODE not available for account id -> " + entityId);
                return super.getCaseAssignee(caseEvent);
            }
            System.out.println("list datainside CustomCmsFormatter getCaseAssignee: " + list);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Map) {
                    userGroupMap = (Map) list.get(i);
                    if (userGroupMap.isEmpty()) {
                        return super.getCaseAssignee(caseEvent);
                    }
                }
                if (list.get(i) instanceof String) {
                    brCode = (String) list.get(i);
                    if (brCode == null) {
                        return super.getCaseAssignee(caseEvent);
                    }
                }
            }
            System.out.println("data inside usergroupmap is: " + userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String moduleId = caseEvent.getModuleId();
        CmsModule cmsModule = (CmsModule) cmsJira.get(moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        //  System.out.println("for department "+departmentMap);
        String userGroup = cmsModule.getUserGroup();

        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        /*Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
        System.out.println("Fetching case Assigne " + userCasesMap);
        for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
            System.out.println(" User " + entry.getKey() + " noofcases " + entry.getValue());
        }*/
       /* for(Map.Entry<String,Integer> entry : userCasesMap.entrySet()){
            if(entry.getValue() < noOfCases){
                noOfCases = entry.getValue();
                resNextUser = entry.getKey();
            }
        }
        System.out.println("next user for incident"+resNextUser);
        return resNextUser;
        */
        try {
            resNextUser = getUserWithLeastCase(userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("dda br code inside CustomCmsFormatter getCaseAssignee() is: " + brCode);
        Map<String, Integer> map = caseMap(CmsUserCaseAssignment.channelUserGroupMap.get(brCode), resNextUser);
        CmsUserCaseAssignment.channelUserGroupMap.put(brCode, map);
        System.out.println("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public Map<String, Integer> caseMap(Map<String, Integer> map, String resNextUser) {
        Map<String, Integer> map1 = map;
        for (Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().contains(resNextUser)) {
                int count = map1.get(resNextUser);
                count += 1;
                map1.put(resNextUser, count);
            }
        }
        return map1;
    }


    @Override
    public String getIncidentAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");

            throw new CmsException("Unable to get resource cmsjira");
        }
        String brCode = null;
        String entityId = caseEvent.getEntityId();
        System.out.println("Entity id inside getIncidentAssignee is: " + entityId);
        if (!entityId.startsWith("A_F_") && !entityId.startsWith("C_F_")) {
            return super.getCaseAssignee(caseEvent);
        }
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        Map<String, Integer> userGroupMap = null;
        try {
            List list = cmsUserCaseAssignment.process(entityId);
            if (list.isEmpty()) {
                System.out.println("BRCODE not available for account Id -> " + entityId);
                return super.getIncidentAssignee(caseEvent);
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Map) {
                    userGroupMap = (Map) list.get(i);
                    if (userGroupMap.isEmpty()) {
                        return super.getIncidentAssignee(caseEvent);
                    }
                }
                if (list.get(i) instanceof String) {
                    brCode = (String) list.get(i);
                    if (brCode == null) {
                        return super.getIncidentAssignee(caseEvent);
                    }
                }
            }
            System.out.println("data inside usergroupmap is: " + userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        CmsModule cmsModule = (CmsModule) cmsJira.get(caseEvent.getModuleId());
        Map<String, String> departmentMap = cmsModule.getDepartments();
        System.out.println("for module id  " + cmsModule);
        String userGroup = cmsModule.getUserGroup();
        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        /*Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup);
        System.out.println("Check user group" + userGroup);
        System.out.println("Check user cases map" + userCasesMap);

        for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
            System.out.println(" cases " + entry.getKey() + " noofincident " + entry.getValue());
        }*/


      /*  for(Map.Entry<String,Integer> entry : userCasesMap.entrySet()){
            if(entry.getValue() < noOfCases){
                noOfCases = entry.getValue();
                resNextUser = entry.getKey();

            }
        }

       System.out.println("Next user for case "+resNextUser);
        return resNextUser;
    */
        try {
            resNextUser = getUserWithLeastCase(userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("dda br code data from CmsUserCaseAssignment is: " + brCode);
        Map<String, Integer> map = caseMap(CmsUserCaseAssignment.channelUserGroupMap.get(brCode), resNextUser);
        CmsUserCaseAssignment.channelUserGroupMap.put(brCode, map);
        System.out.println("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public String getUserWithLeastCase(Map<String, Integer> map) {
        Set<Entry<String, Integer>> set = map.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        for (Map.Entry<String, Integer> entry : list) {
            System.out.println(entry.getKey() + " ==== " + entry.getValue());
        }
        String user = list.get(0).getKey();

        System.out.println("User with least no of cases " + user);
        return user;
    }

    /*public static String checkSummarylength(String summary) {
        if (summary == null || summary.isEmpty()) return "";

        if (summary.length() > 250) {
            summary = summary.substring(0, 251) + "...";
            return summary;
        } else {
            return summary;
        }
    }*/

    /*private String checkSummaryLength(String summary) {
        if (summary.length() > 240) {
            summary = summary.substring(0, 240) + "...";
            summary = summary.replace("\\", "");                   // Summary doesn't allow escaped characters.
        }
        return summary;
    }*/


    /*public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws IOException, CmsException.Configuration {

        CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
        *//*CxJson json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        System.out.println("cmsMod.getCustomFields().get(CmsIncident-message).getJiraId() ---> " + cmsMod.getCustomFields().get("CmsIncident-message").getJiraId());
        String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();*//*
        String eventJson = event.convertToJson(event);
        System.out.println("Display Key: " + event.getCaseDisplayKey());
        Hocon h = new Hocon(eventJson);
        CxJson incident_fieldsJson = new CxJson();

        if (h.getString("summary", null) != null) {
            String jiraId_message = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();
            if (h.getString("message", null) != null) {
                incident_fieldsJson.put(jiraId_message, h.getString("message"));
            }
            incident_fieldsJson.put("summary", h.getString("displayKey") + ":" + checkSummaryLength(h.getString("summary")));
        } else {
            incident_fieldsJson.put("summary", h.getString("displayKey"));
        }
        *//*System.out.println("data inside json is: ----------> " + json.toString());
        CxJson wrapper = json.get("fields");
        try {
            String summary = "";
            System.out.println("wrapper data is: ---> " + wrapper != null ? wrapper : "wrapper is null");
            String displayKey = h.getString("displayKey");
            if (wrapper != null) {
                summary = displayKey != null ? displayKey : "" + ":" + wrapper.getString(summaryId);
                System.out.println("summary from display key is: " + summary);
                wrapper.put("summary", checkSummarylength(summary));
            } else {
                wrapper.put("summary", "summary NOT AVAILABLE");
            }
            return json.put("fields", wrapper);
        } catch (NullPointerException npe) {
            System.out.println("data coming as null ...");
        }*//*
        return incident_fieldsJson;
    }*/
}
