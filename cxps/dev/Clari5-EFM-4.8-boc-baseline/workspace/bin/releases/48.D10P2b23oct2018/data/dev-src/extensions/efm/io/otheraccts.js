var customerTab = "";
var billTab = "";
var accountTab = "";
var dealTab = "";
var chfObj = "";
var entyObject = "";
var dealCount = "";
var dealNum = "";
var billCount = "";
var billNum = "";
var treeFlag = "N";
var lockerTab = "";
var tradeTab = "";
var lockerCount = "";
var lockerNum = "";
var tradeCount = "";
var tradeNum = "";
var leaseTab = "";
var leaseCount = "";
var leaseNum = "";
var pawnTab = "";
var pawnCount = "";
var pawnNum = "";

//initbill
function initBill() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closebillEvidence(billCount);
        }
    };
}
//initdeal
function initdeal() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closeDealEvidence(dealCount);
        }
    };
}
//initlocker
function initLocker() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closelockerEvidence(lockerCount);
        }
    };
}
//inittrade
function initTrade() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closetradeEvidence(tradeCount);
        }
    };
}
//initlease
function initLease() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closeleaseEvidence(leaseCount);
        }
    };
}

//initpawn
function initPawn() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#caseListD").hide();
    document.onkeypress = function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closepawnEvidence(pawnCount);
        }
    };
}

//billTree
function getBillTreeData(cCount, id) {
    billTreeData = "";
    custRmlId = "custRelam" + cCount;
    billTreeData += "<div id=\"billTree" + cCount + "\" class='treeLeftPanel' >";
    billTreeData += "<div id=\"billContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
    billTreeData += "</div>";
    billTreeData += "</div>";

    billTreeData += "<div class='treeRightPanel' >";
    billTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
    billTreeData += "</div>";
    billTreeData += "<div id=\"billLink" + cCount + "\" style='width:100%'>";
    billTreeData += "<div class='heading' ><h3>Bill Details</h3>";
    billTreeData += "</div>";
    billTreeData += "<table id=\"billDataTable" + cCount + "\" class='attrHTbl' style='height:130px;' cellspacing='2' cellpadding='2' >";
    billTreeData += "<tr><td style='white-space:nowrap;' >Sale date</td><td>:</td>";
    billTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"sale_date_" + cCount + "\" ></div></td>";
    billTreeData += "</tr>";
    billTreeData += "<tr><td style='white-space:nowrap;' >Face Value Currency</td><td>:</td>";
    billTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"face_value_currency_" + cCount + "\" ></div></td>";
    billTreeData += "</tr>";
    billTreeData += "<tr><td style='white-space:nowrap;' >Maturity Date</td><td>:</td>";
    billTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"bill_maturity_date_" + cCount + "\" ></div></td>";
    billTreeData += "</tr>";
    billTreeData += "</table>";
    billTreeData += "</div>";
    billTreeData += "</div>";


    return billTreeData;
}
//dealTree
function getDealTreeData(cCount, id) {
    dealTreeData = "";
    custRmlId = "custRelam" + cCount;
    dealTreeData += "<div id=\"dealTree" + cCount + "\" class='treeLeftPanel' >";
    dealTreeData += "<div id=\"dealContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
    dealTreeData += "</div>";
    dealTreeData += "</div>";

    dealTreeData += "<div class='treeRightPanel' >";
    dealTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
    dealTreeData += "</div>";
    dealTreeData += "<div id=\"dealLink" + cCount + "\" style='width:100%'>";
    dealTreeData += "<div class='heading' ><h3>Deal Details</h3>";
    dealTreeData += "</div>";
    dealTreeData += "<table id=\"dealDataTable" + cCount + "\" class='attrHTbl' style='height:150px;' cellspacing='2' cellpadding='2'>";
    dealTreeData += "<tr><td style='white-space:nowrap;' >Name</td><td>:</td>";
    dealTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;'  id=\"deal_name_" + cCount + "\" ></div></td>";
    dealTreeData += "</tr>";
    dealTreeData += "<tr><td style='white-space:nowrap;' >Currency</td><td>:</td>";
    dealTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"deal_currency_" + cCount + "\" ></div></td>";
    dealTreeData += "</tr>";
    dealTreeData += "<tr><td style='white-space:nowrap;' >Deal Date</td><td>:</td>";
    dealTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"deal_date_" + cCount + "\" ></div></td>";
    dealTreeData += "</tr>";
    dealTreeData += "<tr><td style='white-space:nowrap;' >Maturity Date</td><td >:</td>";
    dealTreeData += "<td style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"deal_maturity_date_" + cCount + "\" ></div></td>";
    dealTreeData += "</tr>";
    dealTreeData += "</table>";
    dealTreeData += "</div>";
    dealTreeData += "</div>";


    return dealTreeData;
}

//lockertree
function getLockerTreeData(cCount, id) {
    lockerTreeData = "";
    custRmlId = "custRelam" + cCount;
    lockerTreeData += "<div id=\"lockerTree" + cCount + "\" class='treeLeftPanel' >";
    lockerTreeData += "<div id=\"lockerContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
    lockerTreeData += "</div>";
    lockerTreeData += "</div>";

    lockerTreeData += "<div class='treeRightPanel' >";
    lockerTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
    lockerTreeData += "</div>";
    lockerTreeData += "<div id=\"lockerLink" + cCount + "\" style='width:100%'>";
    lockerTreeData += "<div class='heading' ><h3>Locker Details</h3>";
    lockerTreeData += "</div>";
    lockerTreeData += "<table id=\"lockerDataTable" + cCount + "\" class='attrHTbl' style='height:150px;' cellspacing='2' cellpadding='2'>";
    lockerTreeData += "<tr><td style='white-space:nowrap;' >Cust ID</td><td>:</td>";
    lockerTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"locker_custId_" + cCount + "\" ></div></td>";
    lockerTreeData += "</tr>";
    lockerTreeData += "<tr><td style='white-space:nowrap;' >Locker Number</td><td>:</td>";
    lockerTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"locker_num_" + cCount + "\" ></div></td>";
    lockerTreeData += "</tr>";
    lockerTreeData += "<tr><td style='white-space:nowrap;' >Open Date</td><td>:</td>";
    lockerTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"locker_open_date_" + cCount + "\" ></div></td>";
    lockerTreeData += "</tr>";
    lockerTreeData += "<tr><td style='white-space:nowrap;' >Close Date</td><td>:</td>";
    lockerTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"locker_close_date_" + cCount + "\" ></div></td>";
    lockerTreeData += "</tr>";
    lockerTreeData += "<tr><td style='white-space:nowrap;' >Status</td><td>:</td>";
    lockerTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"locker_status_" + cCount + "\" ></div></td>";
    lockerTreeData += "</tr>";
    lockerTreeData += "<tr><td style='white-space:nowrap;' >Branch</td><td>:</td>";
    lockerTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"branch_" + cCount + "\" ></div></td>";
    lockerTreeData += "</tr>";
    lockerTreeData += "</table>";
    lockerTreeData += "</div>";
    lockerTreeData += "</div>";


    return lockerTreeData;
}
//tradeTree
function getTradeTreeData(cCount, id) {
      tradeTreeData = "";
      custRmlId = "custRelam" + cCount;
      tradeTreeData += "<div id=\"tradeTree" + cCount + "\" class='treeLeftPanel' >";
      tradeTreeData += "<div id=\"tradeContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
      tradeTreeData += "</div>";
      tradeTreeData += "</div>";

      tradeTreeData += "<div class='treeRightPanel' >";
      tradeTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
      tradeTreeData += "</div>";
      tradeTreeData += "<div id=\"tradeLink" + cCount + "\" style='width:100%'>";
      tradeTreeData += "<div class='heading' ><h3>Trade Details</h3>";
      tradeTreeData += "</div>";
      tradeTreeData += "<table id=\"tradeDataTable" + cCount + "\" class='attrHTbl' style='height:150px;' cellspacing='2' cellpadding='2'>";
      tradeTreeData += "<tr><td style='white-space:nowrap;' >Core Account ID</td><td>:</td>";
      tradeTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"core_accountId_" + cCount + "\" ></div></td>";
      tradeTreeData += "</tr>";
      tradeTreeData += "<tr><td style='white-space:nowrap;' >Currency</td><td>:</td>";
      tradeTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"trade_currency_" + cCount + "\" ></div></td>";
      tradeTreeData += "</tr>";
      tradeTreeData += "<tr><td style='white-space:nowrap;' >Open Date</td><td>:</td>";
      tradeTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"trade_open_date_" + cCount + "\" ></div></td>";
      tradeTreeData += "</tr>";
      tradeTreeData += "<tr><td style='white-space:nowrap;' >Expire Date</td><td>:</td>";
      tradeTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"expiry_date_" + cCount + "\" ></div></td>";
      tradeTreeData += "</tr>";
      tradeTreeData += "<tr><td style='white-space:nowrap;' >Status </td><td>:</td>";
      tradeTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"trade_status_" + cCount + "\" ></div></td>";
      tradeTreeData += "</tr>";
      tradeTreeData += "</table>";
      tradeTreeData += "</div>";
      tradeTreeData += "</div>";


      return tradeTreeData;
  }


//LeaseTree
function getLeaseTreeData(cCount, id) {
    leaseTreeData = "";
    custRmlId = "custRelam" + cCount;
    leaseTreeData += "<div id=\"leaseTree" + cCount + "\" class='treeLeftPanel' >";
    leaseTreeData += "<div id=\"leaseContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
    leaseTreeData += "</div>";
    leaseTreeData += "</div>";

    leaseTreeData += "<div class='treeRightPanel' >";
    leaseTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
    leaseTreeData += "</div>";
    leaseTreeData += "<div id=\"leaseLink" + cCount + "\" style='width:100%'>";
    leaseTreeData += "<div class='heading' ><h3>Lease Details</h3>";
    leaseTreeData += "</div>";
    leaseTreeData += "<table id=\"leaseDataTable" + cCount + "\" class='attrHTbl' style='height:150px;' cellspacing='2' cellpadding='2'>";
    leaseTreeData += "<tr><td style='white-space:nowrap;' >StakeHolder ID</td><td>:</td>";
    leaseTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"StakeHolder_ID_" + cCount + "\" ></div></td>";
    leaseTreeData += "</tr>";
    leaseTreeData += "<tr><td style='white-space:nowrap;' >Core CIFID</td><td>:</td>";
    leaseTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"CORE_CIFID_" + cCount + "\" ></div></td>";
    leaseTreeData += "</tr>";
    leaseTreeData += "<tr><td style='white-space:nowrap;' >Lease Creation Date</td><td>:</td>";
    leaseTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"Lease_Creation_Date_" + cCount + "\" ></div></td>";
    leaseTreeData += "</tr>";
    leaseTreeData += "<tr><td style='white-space:nowrap;' >Lease Maturity Date</td><td>:</td>";
    leaseTreeData += "<td  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"Lease_Maturity_Date_" + cCount + "\" ></div></td>";
    leaseTreeData += "</tr>";
    leaseTreeData += "</table>";
    leaseTreeData += "</div>";
    leaseTreeData += "</div>";


    return leaseTreeData;
}

//pawnTree
function getPawnTreeData(cCount, id) {
    pawnTreeData = "";
    custRmlId = "custRelam" + cCount;
    pawnTreeData += "<div id=\"pawnTree" + cCount + "\" class='treeLeftPanel' >";
    pawnTreeData += "<div id=\"pawnContainer" + cCount + "\" style='margin-left:-6px;overflow-x:auto;overflow-y:auto;height:auto;'>";
    pawnTreeData += "</div>";
    pawnTreeData += "</div>";

    pawnTreeData += "<div class='treeRightPanel' >";
    pawnTreeData += "<div><a onclick='goBack(id)' class='abutton back-w-def goBackCls' id=" + id + "  >Back</a>";
    pawnTreeData += "</div>";
    pawnTreeData += "<div id=\"pawnLink" + cCount + "\" style='width:100%'>";
    pawnTreeData += "<div class='heading' ><h3>Pawn Details</h3>";
    pawnTreeData += "</div>";
    pawnTreeData += "<table id=\"pawnDataTable" + cCount + "\" class='attrHTbl' style='height:150px;' >";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Name</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"pawn_name_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Currency</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"pawncurrency_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Scheme Code</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"scheme_code_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Scheme Type</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"pawn_scheme_type_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Pawning Account Opening Date</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"Pawning_Opening_Renew_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Open Date</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"pawn_opened_date_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Settlement Maturity Date</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"Settlement_Maturity_Date_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Status</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"pawn_status_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";
    pawnTreeData += "<tr><td style='white-space:nowrap;' >Granted Pawning Amount</td><td>:</td>";
    pawnTreeData += "<td colspan='8'  style='width: 597px; height: 25px;'><div style='word-break:break-all;width:1000px;' id=\"Granted_Pawning_Amount_" + cCount + "\" ></div></td>";
    pawnTreeData += "</tr>";


    pawnTreeData += "</table>";
    pawnTreeData += "</div>";
    pawnTreeData += "</div>";


    return pawnTreeData;
}


//billDetails
  function billLoad(billNumber, tCount) {
      billCount = tCount;
      billNum = billNumber;
      $("#menuBlock").hide();
      chfObj = {
          "billNum":billNum
      };
      entyObject = {
          "entityKey": billNum
      };

      if (cxcifId != "") {
          treeObj = {
              "billNum":billNum
          };
      } else {
          cxcifId = billNum;
          treeObj = {
              "billNum":billNum
          };
      }
     getBillHardFacts(treeObj, billHFactsRespHandler, true);
    }

function getBillHardFacts(inputObject,callback,refresh){
      var payload = {refresh:refresh, input: inputObject};
      transeiveCustom("itool", "get_bill_details", inputObject, callback);
  }
function billTreeRespHandler(resp) {
    createCustomerRlm(resp);
    loadJSFile("caseTree.js");
}

var billmember="";
function billHFactsRespHandler(resp) {
     var treeObj = "";
     var respObj = resp.billDetails;
    if (respObj != null && respObj != undefined && respObj != "") {
        //$("#custId_" + billCount).text(respObj[0].custId != "" ? respObj[0].custId : "NA");
        $("#sale_date_" + billCount).text(respObj[0].Sale_Date != "" ? respObj[0].Sale_Date : "NA");
        $("#face_value_currency_" + billCount).text(respObj[0].Face_Value_Crncy != "" ? respObj[0].Face_Value_Crncy : "NA");
        $("#bill_maturity_date_" + billCount).text(respObj[0].Maturity_Date != "" ? respObj[0].Maturity_Date : "NA");
         	if(billmember!=""){
        	     treeObj={
                     "custId":respObj[0].custId
                     };
        		}else{
        		billmember=respObj[0].custId;
        	    treeObj = {
                            "custId": billmember
                        };
        		}
        	getCustomTreeDetail(treeObj, billTreeRespHandler, true);
    } else {
        clearTabs();
        displayNotification("Please enter correct Account Number.", true, "warning");
        return;
    }
}
//dealDetails
 function dealLoad(dealNumber, tCount) {
      dealCount = tCount;
      dealNum = dealNumber;
      $("#menuBlock").hide();
      chfObj = {
          "dealNum":dealNum
      };
      entyObject = {
          "entityKey": dealNum
      };

      if (cxcifId != "") {
          treeObj = {
              "dealNum":dealNum
          };
      } else {
          cxcifId = dealNum;
          treeObj = {
              "dealNum":dealNum
          };
      }
     getDealHardFacts(treeObj, dealHFactsRespHandler, true);
    }


  function getDealHardFacts(inputObject,callback,refresh){
      var payload = {refresh:refresh, input: inputObject};
      transeiveCustom("itool", "get_deal_details", inputObject, callback);
  }

function dealTreeRespHandler(resp) {
    createCustomerRlm(resp);
    loadJSFile("caseTree.js");
}

var dealmember="";
function dealHFactsRespHandler(resp) {
    var treeObj = "";
    var respObj = resp.dealDetails;
    if (respObj != null && respObj != undefined && respObj != "") {
        //$("#custId_" + dealCount).text(respObj[0].custId != "" ? respObj[0].custId : "NA");
        $("#deal_name_" + dealCount).text(respObj[0].Name != "" ? respObj[0].Name : "NA");
        $("#deal_currency_" + dealCount).text(respObj[0].deal_Amount_crncy != "" ? respObj[0].deal_Amount_crncy : "NA");
        $("#deal_date_" + dealCount).text(respObj[0].deal_Date != "" ? respObj[0].deal_Date : "NA");
        $("#deal_maturity_date_" + dealCount).text(respObj[0].Maturity_Date != "" ? respObj[0].Maturity_Date : "NA");
         	if(dealmember!=""){
        	     treeObj={
                     "custId":respObj[0].custId
                     };
        		}else{
        		dealmember=respObj[0].custId;
        	    treeObj = {
                            "custId": dealmember
                        };
        		}
        	getCustomTreeDetail(treeObj, dealTreeRespHandler, true);
    } else {
        clearTabs();
        displayNotification("Please enter correct Account Number.", true, "warning");
        return;
    }
}

//lockerdetails
function lockerLoad(lockerNumber, tCount) {
         lockerCount = tCount;
         lockerNum = lockerNumber;
         $("#menuBlock").hide();
         chfObj = {
             "lockerNum":lockerNum
         };
         entyObject = {
             "entityKey": lockerNum
         };

         if (cxcifId != "") {
             treeObj = {
                 "lockerNum":lockerNum
             };
         } else {
             cxcifId = lockerNum;
             treeObj = {
                 "lockerNum":lockerNum
             };
         }
        getLockerHardFacts(treeObj, lockerHFactsRespHandler, true);
       }

     function getLockerHardFacts(inputObject,callback,refresh){
         var payload = {refresh:refresh, input: inputObject};
         transeiveCustom("itool", "get_locker_details", inputObject, callback);
     }

   function lockerTreeRespHandler(resp) {
       createCustomerRlm(resp);
       loadJSFile("caseTree.js");
   }
var lockmember="";
   function lockerHFactsRespHandler(resp) {
       var treeObj = "";
       var respObj = resp.lockerDetails;
       if (respObj != null && respObj != undefined && respObj != "") {
           $("#locker_custId_" + lockerCount).text(respObj[0].custId != "" ? respObj[0].custId : "NA");
           $("#locker_num_" + lockerCount).text(respObj[0].locker_num != "" ? respObj[0].locker_num : "NA");
           $("#locker_open_date_" + lockerCount).text(respObj[0].open_date != "" ? respObj[0].open_date : "NA");
           $("#locker_close_date_" + lockerCount).text(respObj[0].close_date != "" ? respObj[0].close_date : "NA");
           $("#locker_status_" + lockerCount).text(respObj[0].status != "" ? respObj[0].status : "NA");
           $("#branch_" + lockerCount).text(respObj[0].BranchCode != "" ? respObj[0].BranchCode : "NA");
                if(lockmember!=""){
                 treeObj={
                        "custId":respObj[0].custId
                        };
                }else{
                lockmember=respObj[0].custId;
                treeObj = {
                               "custId": lockmember
                           };
                }
            getCustomTreeDetail(treeObj, lockerTreeRespHandler, true);
       } else {
           clearTabs();
           displayNotification("Please enter correct Account Number.", true, "warning");
           return;
       }
   }

//tradedetails
function tradeLoad(tradeNumber, tCount) {
        tradeCount = tCount;
        tradeNum = tradeNumber;
        $("#menuBlock").hide();
        chfObj = {
            "faciltiyNum":tradeNum
        };
        entyObject = {
            "entityKey": tradeNum
        };

        if (cxcifId != "") {
            treeObj = {
                "faciltiyNum":tradeNum
            };
        } else {
            cxcifId = tradeNum;
            treeObj = {
                "faciltiyNum":tradeNum
            };
        }
       getTradeHardFacts(treeObj, tradeHFactsRespHandler, true);
      }


    function getTradeHardFacts(inputObject,callback,refresh){
        var payload = {refresh:refresh, input: inputObject};
        transeiveCustom("itool", "get_trade_details", inputObject, callback);
    }

  function tradeTreeRespHandler(resp) {
      createCustomerRlm(resp);
      loadJSFile("caseTree.js");
  }
  var trademember="";
  function tradeHFactsRespHandler(resp) {
      var treeObj = "";
      var respObj = resp.tradeDetails;
      if (respObj != null && respObj != undefined && respObj != "") {
          //$("#custId_" + tradeCount).text(respObj[0].custId != "" ? respObj[0].custId : "NA");
          $("#core_accountId_" + tradeCount).text(respObj[0].core_accountId != "" ? respObj[0].core_accountId : "NA");
          $("#trade_currency_" + tradeCount).text(respObj[0].currency != "" ? respObj[0].currency : "NA");
          $("#trade_open_date_" + tradeCount).text(respObj[0].open_date != "" ? respObj[0].open_date : "NA");
          $("#expiry_date_" + tradeCount).text(respObj[0].expiry_date != "" ? respObj[0].expiry_date : "NA");
          $("#trade_status_" + tradeCount).text(respObj[0].status != "" ? respObj[0].status : "NA");
            if(trademember!=""){
                 treeObj={
                       "custId":respObj[0].custId
                       };
                }else{
                trademember=respObj[0].custId;
                treeObj = {
                              "custId": trademember
                          };
                }
            getCustomTreeDetail(treeObj, tradeTreeRespHandler, true);
      } else {
          clearTabs();
          displayNotification("Please enter correct Account Number.", true, "warning");
          return;
      }
  }

//leaseDetails
function leaseLoad(leaseNumber, tCount) {
      leaseCount = tCount;
      leaseNum = leaseNumber;
      $("#menuBlock").hide();
      chfObj = {
          "leaseNum":leaseNum
      };
      entyObject = {
          "entityKey": leaseNum
      };

      if (cxcifId != "") {
          treeObj = {
              "leaseNum":leaseNum
          };
      } else {
          cxcifId = leaseNum;
          treeObj = {
              "leaseNum":leaseNum
          };
      }
     getLeaseHardFacts(treeObj, leaseHFactsRespHandler, true);
    }

  function getLeaseHardFacts(inputObject,callback,refresh){
      var payload = {refresh:refresh, input: inputObject};
      transeiveCustom("itool", "get_lease_details", inputObject, callback);
  }

function leaseTreeRespHandler(resp) {
    createCustomerRlm(resp);
    loadJSFile("caseTree.js");
}
var leasemember="";
function leaseHFactsRespHandler(resp) {
    var treeObj = "";
    var respObj = resp.leaseDetails;
    if (respObj != null && respObj != undefined && respObj != "") {
        //$("#custId_" + leaseCount).text(respObj[0].custId != "" ? respObj[0].custId : "NA");
        $("#StakeHolder_ID_" + leaseCount).text(respObj[0].StakeHolder_ID != "" ? respObj[0].StakeHolder_ID : "NA");
        $("#CORE_CIFID_" + leaseCount).text(respObj[0].CORE_CIFID != "" ? respObj[0].CORE_CIFID : "NA");
        $("#Lease_Creation_Date_" + leaseCount).text(respObj[0].Lease_Creation_Date != "" ? respObj[0].Lease_Creation_Date : "NA");
        $("#Lease_Maturity_Date_" + leaseCount).text(respObj[0].Lease_Maturity_Date != "" ? respObj[0].Lease_Maturity_Date : "NA");
         	if(leasemember!=""){
        	     treeObj={
                     "custId":respObj[0].custId
                     };
        		}else{
        		leasemember=respObj[0].custId;
        	    treeObj = {
                            "custId": leasemember
                        };
        		}
        	getCustomTreeDetail(treeObj, leaseTreeRespHandler, true);
    } else {
        clearTabs();
        displayNotification("Please enter correct Account Number.", true, "warning");
        return;
    }
}

//pawnDetails
function pawnLoad(pawnNumber, tCount) {
      pawnCount = tCount;
      pawnNum = pawnNumber;
      $("#menuBlock").hide();
      chfObj = {
          "pawnNum":pawnNum
      };
      entyObject = {
          "entityKey": pawnNum
      };

      if (cxcifId != "") {
          treeObj = {
              "pawnNum":pawnNum
          };
      } else {
          cxcifId = pawnNum;
          treeObj = {
              "pawnNum":pawnNum
          };
      }
     getPawnHardFacts(treeObj, pawnHFactsRespHandler, true);
    }


  function getPawnHardFacts(inputObject,callback,refresh){
      var payload = {refresh:refresh, input: inputObject};
      transeiveCustom("itool", "get_pawn_details", inputObject, callback);
  }

function pawnTreeRespHandler(resp) {
    createCustomerRlm(resp);
    loadJSFile("caseTree.js");
}

var pawnMember="";
function pawnHFactsRespHandler(resp) {
    var treeObj = "";
    var respObj = resp.pawningDetails;
    if (respObj != null && respObj != undefined && respObj != "") {
        //$("#custId_" + pawnCount).text(respObj[0].custId != "" ? respObj[0].custId : "NA");
        $("#pawn_name_" + pawnCount).text(respObj[0].name != "" ? respObj[0].name : "NA");
        $("#pawncurrency_" + pawnCount).text(respObj[0].currency != "" ? respObj[0].currency : "NA");
        $("#scheme_code_" + pawnCount).text(respObj[0].scheme_code != "" ? respObj[0].scheme_code : "NA");
        $("#Pawning_Opening_Renew_" + pawnCount).text(respObj[0].Dt_of_Pawning_Acct_Opening_Renew != "" ? respObj[0].Dt_of_Pawning_Acct_Opening_Renew : "NA");
        $("#pawn_opened_date_" + pawnCount).text(respObj[0].opened_date != "" ? respObj[0].opened_date : "NA");
        $("#pawn_scheme_type_" + pawnCount).text(respObj[0].scheme_type != "" ? respObj[0].scheme_type : "NA");
        $("#Settlement_Maturity_Date_" + pawnCount).text(respObj[0].Settlement_Maturity_Date != "" ? respObj[0].Settlement_Maturity_Date : "NA");
        $("#pawn_status_" + pawnCount).text(respObj[0].status != "" ? respObj[0].status : "NA");
        $("#Granted_Pawning_Amount_" + pawnCount).text(respObj[0].Granted_Pawning_Amount != "" ? respObj[0].Granted_Pawning_Amount : "NA");

         	if(pawnMember!=""){
        	     treeObj={
                     "custId":respObj[0].cust_id
                     };
        		}else{
        		pawnMember=respObj[0].cust_id;
        	    treeObj = {
                            "custId": pawnMember
                        };
        		}
        	getCustomTreeDetail(treeObj, pawnTreeRespHandler, true);
    } else {
        clearTabs();
        displayNotification("Please enter correct Account Number.", true, "warning");
        return;
    }
}




