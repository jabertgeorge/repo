package clari5.custom.wlalert.daemon;

import clari5.custom.wlalert.Matches;
import clari5.custom.wlalert.WlRecord;
import clari5.custom.wlalert.db.*;
import clari5.custom.wlalert.WlRequestPayload;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.*;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WlAlert extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlAlert.class);
    static WlRecordDetail wlRecordDetail = new WlRecordDetail();
    static final Set<String> Ids = new HashSet<>();
    private static final Queue<WlData> eventDatas = new ConcurrentLinkedQueue<>();
    WlRecord wlRecord;


    @Override
    public void configure(Hocon h) {

    }

    @Override
    public void release() {

    }

    @Override
    protected Object getData() throws RuntimeFatalException {
        return getNextEntity();
    }


    @Override
    protected void processData(Object o) throws RuntimeFatalException {
       // WlRecord wlRecord =


        this.wlRecord=new WlRecord();

        WlData wlPayload = (WlData) o;

        WlRequestPayload wlRequestPayload = new WlRequestPayload();


        try {
            WlrecordDetails wlrecordDetails = wlPayload.getWlrecordDetails();

            wlRecord.setListname(wlrecordDetails.getListName());


            CxJson cxJson = wlRequestPayload.setWlReqValues(wlrecordDetails,wlRecord);
            Matches matches = wlRequestPayload.sendWlRequest(cxJson);

            if (matches.getResponseCode() == 200) {
                wlrecordDetails.setStatus("P");
                wlrecordDetails.setRemarks("Sending  event to mq ");
                wlRecordDetail.update(wlrecordDetails);
            } else {

                wlrecordDetails.setStatus("E");
                wlrecordDetails.setRemarks(matches.getRemarks());
                wlRecordDetail.update(wlrecordDetails);

            }
            if(matches.getMatchResult().contains("score")) send(new JSONObject(matches.getMatchResult()), wlrecordDetails);
            else {
                wlrecordDetails.setStatus("C");
                wlrecordDetails.setRemarks("No Match Found");
                wlRecordDetail.update(wlrecordDetails);
            }

            logger.info("[getData] payload " + wlPayload.toString());


        } catch (NullPointerException np) {
            logger.warn("No fresh record in wlRecord_Details");
        }
    }


    public void send(JSONObject jsonObject, WlrecordDetails wlrecordDetails) {

        String status = jsonObject.getJSONObject("status").get("status").toString();
        if ("success".equals(status.trim())) {
            if (jsonObject.has("matchedResults")) {

                jsonObject.getJSONObject("matchedResults").getJSONArray("matchedRecords")
                        .forEach(json -> {
                            JSONObject jsonObj = (JSONObject) json;
                            String id = jsonObj.getString("id");
                            String removestr ="customer_C_F_";
                            String custId=id.replace(removestr,"");
                            wlRecord.setCustid(custId);
                            wlRecord.setListname(wlrecordDetails.getListName());
                            jsonObj.getJSONArray("fields").forEach(jsonfield -> {
                                JSONObject jsonObject1 = (JSONObject) jsonfield;
                                String  score =jsonObject1.getString("score");
                                wlRecord.setMatchedpercentage(Double.parseDouble(score)* 100);
                                wlRecord.setMatchedresult(jsonObject1.getString("value"));
                                wlRecord.setInputentity(jsonObject1.getString("queryValue"));
                            });
                            logger.warn("Result" + wlRecord.toString());
                            try {
                                CxJson mqJson = wlRecord.toJson();
                                String mqRequestString;
                                if (mqJson != null) {
                                    mqRequestString = mqJson.toString();
                                    boolean status1 = ECClient.enqueue("HOST", "C_F_11111", mqJson.getString("event_id"), mqRequestString);
                                    if(status1) {
                                        wlrecordDetails.setStatus("C");
                                        wlrecordDetails.setRemarks("Event is sent to mq for processing ");

                                    }else {
                                        wlrecordDetails.setStatus("E");
                                        wlrecordDetails.setRemarks("Error in sending the Event to mq  ");
                                    }
                                    wlRecordDetail.update(wlrecordDetails);

                                     logger.warn("Status after sending the json to mq " + status1);
                                }
                            } catch (IOException io) {
                                logger.warn("Exception while sending json " +io.getMessage()+ "Cause"+io.getCause());
                            }

                        });

            }
        }

    }


    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void refresh() {
        Ids.clear();
        eventDatas.clear();
    }

    @Override
    public ConfigType getType() {
        return null;
    }

    @Override
    public void configure(Hocon h, Hocon location) {

    }

    @Override
    public void refresh(Hocon conf, Hocon location) {

    }


    protected static WlData getNextEntity() throws NullPointerException {
        WlData data = extractEvent();
        if (data != null) {
            logger.debug("[" + Thread.currentThread().getName() + "] Retrieving  Wl data  of cust-is [" + data.getId() + "]");
            return data;
        }
        try {

            List<WlRecordDeatailsSummary> newAcctList = wlRecordDetail.getFreshRecord();

            for (WlRecordDeatailsSummary fact : newAcctList) {

                WlrecordDetails wlrecordDetails = new WlrecordDetails();
                wlrecordDetails.from(fact.toHocon());

                if (!Ids.contains(fact.getId()) || Ids.isEmpty()) {
                    Ids.add(fact.getId());
                    eventDatas.add(new WlData(wlrecordDetails, fact.getId()));
                }
            }

        } catch (Exception io) {
            logger.error("Failed to convert account summary to acct details" + io.getMessage() + "\n Cause" + io.getCause());
        }
        if ((data = extractEvent()) != null) {
            logger.debug("[" + Thread.currentThread().getName() + "] Retrieving WLRecord_Details    data  of cust-id is  [" + data.getId() + "]");
            return data;
        }

        return data;
    }

    protected synchronized static WlData extractEvent() {
        logger.debug("No of cust-id to be processed : " + eventDatas.size() + "and Current Thread Id is " + Thread.currentThread().getId());
        while (!eventDatas.isEmpty()) {
            WlData data = eventDatas.poll();
            Ids.remove(data.getId());
            return data;
        }
        return null;
    }

  /*  public static void main(String[] args) {

        JSONObject jsonObject = new JSONObject(getreponse());
        WlRecord wlRecord =new WlRecord();
        String status = jsonObject.getJSONObject("status").get("status").toString();
        if ("success".equals(status.trim())) {
            if (jsonObject.has("matchedResults")) {

                jsonObject.getJSONObject("matchedResults").getJSONArray("matchedRecords")
                        .forEach(json   -> {
                            JSONObject jsonObj = (JSONObject)json;
                            wlRecord.setCustid(jsonObj.getString("id"));
                            wlRecord.setListname(jsonObj.getString("listName"));
                            jsonObj.getJSONArray("fields").forEach(jsonfield->{
                                JSONObject jsonObject1 =(JSONObject)jsonfield;
                                wlRecord.setMatchedpercentage(jsonObject1.getString("score"));
                                wlRecord.setMatchedresult(jsonObject1.getString("value"));
                                wlRecord.setInputentity(jsonObject1.getString("queryValue"));
                            });
                            System.out.println("Result" +wlRecord.toString());
                            try {
                                CxJson mqJson =wlRecord.toJson();
                                String mqRequestString;
                                if(mqJson!=null) {
                                    mqRequestString = mqJson.toString();
                                    //Rest.equeue(reqUrl, "HOST", mqJson.getString("event_id"), "1111111111", System.currentTimeMillis(), mqRequestString);
                                    boolean status1 =ECClient.enqueue("HOST","456777",mqJson.getString("event_id"),mqRequestString);
                                    System.out.println("status" +status1);
                                }}catch (IOException io){}

                        });

            }
        }
    }
*/



    /*public static String getreponse (){
        return "{ \"status\" : {\"status\": \"success\",\"errorDetails\": \"\"}, \"matchedResults\" : {\n" +
                "  \"maxScore\" : \"1.000\",\n" +
                "  \"total\" : 3,\n" +
                "  \"matchedRecords\" : [ {\n" +
                "    \"id\" : \"FCU_05\",\n" +
                "    \"score\" : \"1.000\",\n" +
                "    \"listName\" : \"FCU\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"score\" : \"1.000\",\n" +
                "      \"name\" : \"name\",\n" +
                "      \"value\" : \"BRIJESH SUKHU GUPTA\",\n" +
                "      \"queryValue\" : \"BRIJESH SUKHU GUPTA\"\n" +
                "    } ],\n" +
                "    \"Description\" : null\n" +
                "  }, {\n" +
                "    \"id\" : \"FCU_792\",\n" +
                "    \"score\" : \"0.414\",\n" +
                "    \"listName\" : \"FCU\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"score\" : \"0.414\",\n" +
                "      \"name\" : \"name\",\n" +
                "      \"value\" : \"HIMANSHU GUPTA\",\n" +
                "      \"queryValue\" : \"BRIJESH SUKHU GUPTA\"\n" +
                "    } ],\n" +
                "    \"Description\" : null\n" +
                "  }, {\n" +
                "    \"id\" : \"FCU_111\",\n" +
                "    \"score\" : \"0.370\",\n" +
                "    \"listName\" : \"FCU\",\n" +
                "    \"fields\" : [ {\n" +
                "      \"score\" : \"0.370\",\n" +
                "      \"name\" : \"name\",\n" +
                "      \"value\" : \"AAKASH GUPTA\",\n" +
                "      \"queryValue\" : \"BRIJESH SUKHU GUPTA\"\n" +
                "    } ],\n" +
                "    \"Description\" : null\n" +
                "  } ]\n" +
                "} }";
    }*/



}
