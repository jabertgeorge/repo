package clari5.custom.wlalert;

;
import clari5.custom.wlalert.daemon.RuleFact;
import clari5.custom.wlalert.daemon.WlAlert;
import cxps.apex.utils.CxpsLogger;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class XmlParser {
    private static final CxpsLogger logger = CxpsLogger.getLogger(XmlParser.class);
    RuleFact ruleFact=new RuleFact();
    public RuleFact xmlparser(String unzipFilePath, File xmlName) {
        try {

            File[] xmlFileList = xmlName.listFiles();
            for (File xmlFiles: xmlFileList)
            {

             logger.warn("File Patch "+xmlFiles.getAbsolutePath()+"xmlFileName" + xmlFiles.getName());

                JAXBContext jaxbContext = JAXBContext.newInstance(DowJones.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                DowJones dowJones = (DowJones) jaxbUnmarshaller.unmarshal(xmlFiles);

                dowJones.records.getPerson()
                        .forEach(person -> person.getNameDetails().getName()
                                .forEach(name -> name.getNameValue()
                                        .forEach(nameValue -> {

                                            List<String> firstName = nameValue.getFirstName();
                                            List<String> middleName = nameValue.getMiddleName();
                                            List<String> lastName = nameValue.getSurname();
                                            int firstNameSize = firstName.size();
                                            int middleNameSize = middleName.size();
                                            int lastNameSize = lastName.size();
                                            int length = 0;
                                            if (firstNameSize >= middleNameSize && firstNameSize >= lastNameSize)
                                                length = firstNameSize;
                                            else if (middleNameSize >= firstNameSize && middleNameSize >= lastNameSize)
                                                length = middleNameSize;
                                            else if (lastNameSize >= firstNameSize && lastNameSize >= middleNameSize)
                                                length = lastNameSize;

                                            for (int j = 0; j < length; j++) {
                                                String fullName= "";
                                                String[] names= new String[3];

                                                /*String fistName = (firstNameSize == 0) ? "" : firstName.get(j);
                                                String middlName = (middleNameSize == 0) ? "" : middleName.get(j);
                                                String lastname = (lastNameSize == 0) ? "" : lastName.get(j);*/

                                                names[0] = (firstNameSize == 0) ? "" : firstName.get(j);
                                                names[1] = (middleNameSize == 0) ? "" : middleName.get(j);
                                                names[2] = (lastNameSize == 0) ? "" : lastName.get(j);

                                                for (int i = 0; i < 3; i++)
                                                {
                                                    if (names[i].isEmpty())continue;
                                                    else if (i==2) fullName+=names[i];
                                                    else fullName+=names[i]+" ";
                                                }

                                                ruleFact.addName(fullName);

                                            }
                                        })
                                ));


                dowJones.records.getEntity()
                        .forEach(person -> person.getNameDetails().getName()
                                .forEach(name -> name.getNameValue()
                                        .forEach(nameValue -> {

                                            List<String> entityName = nameValue.getEntityName();
                                            for (int k = 0; k < entityName.size(); k++) {
                                               ruleFact.addName(entityName.get(k));

                                            }
                                        })
                                ));
                dowJones.records.getPerson().forEach(person -> {
                    if (person.getIDNumberTypes()!= null) {
                        person.getIDNumberTypes().getID().forEach(id -> {
                            if ("Passport No.".equalsIgnoreCase(id.getIDType())) {
                                id.getIDValue().forEach(idValue -> ruleFact.addPassport(idValue.getValue()));
                            }
                        });
                    }
                });

                ruleFact.setList("dowjones");
                logger.warn("fullNameList" + ruleFact.toString());
                try {
                    FileUtils.deleteDirectory(new File(unzipFilePath));

                } catch (Exception e) {
                    logger.warn("Exception while deleting the unzipped file [" +e.getMessage()+" ] and cause is ["+e.getCause()+"]");
                }

            }
        } catch (JAXBException e) {
            logger.warn("Exception while parsing [" +e.getMessage()+" ] and cause is ["+e.getCause()+"]");
        }

    return ruleFact;

    }

    public static void main(String[] args) {
        String a="lisa";
        String b="";
        String c= "lewis";
        String[] str= new String[3];
        String fullName="";
        str[0]=a;
        str[1]=b;
        str[2]=c;
        for (int i = 0; i < str.length; i++)
            {
            if (str[i].isEmpty())continue;
            else if (i==str.length-1) fullName+=str[i];
            else fullName+=str[i]+" ";
        }

        System.out.println("["+fullName+"]");
    }
}