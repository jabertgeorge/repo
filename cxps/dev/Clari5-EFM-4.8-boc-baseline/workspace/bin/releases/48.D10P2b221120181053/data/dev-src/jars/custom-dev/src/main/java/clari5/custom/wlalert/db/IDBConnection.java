package clari5.custom.wlalert.db;

public interface IDBConnection {
    Object getConnection();

    void closeConnection();
}