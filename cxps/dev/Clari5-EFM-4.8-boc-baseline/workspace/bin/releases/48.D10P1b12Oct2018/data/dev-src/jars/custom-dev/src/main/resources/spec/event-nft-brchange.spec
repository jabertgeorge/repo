cxps.events.event.nft-brchange{
  table-name : EVENT_NFT_BRCHANGE
  event-mnemonic = NB
  workspaces : {
                 BRANCH :  branch-id,
	         CUSTOMER: cust-id
        }

  event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    channel : {db : true ,raw_name : channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}
    msg-source : {db : true ,raw_name : source ,type : "string:20"}
    eventts : {db : true ,raw_name : sys_time ,type : timestamp}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {raw_name : reservedfield4 ,type : "string:100"}
    add-entity-id5 : {raw_name : reservedfield5 ,type : "string:100"}
    cust-id : {db : true ,raw_name : cust_id, type : "string:20"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    branch-id:{type:"string:20",db:true,raw_name:Initial_Branch_Id}
    final-branch-id:{type:"string:20",db:true,raw_name:Final_Branch_Id}
    host-id:{type:"string:100",db:true,raw_name:HostId}
 }
}
