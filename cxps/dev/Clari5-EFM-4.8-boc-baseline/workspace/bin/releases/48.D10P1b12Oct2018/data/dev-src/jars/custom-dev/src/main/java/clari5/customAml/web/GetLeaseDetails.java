package clari5.customAml.web;

import java.sql.*;
import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetLeaseDetails {
    CxpsLogger logger = CxpsLogger.getLogger(GetLeaseDetails.class);

    private static String leaseNum="";
    //JSONObject obj = new JSONObject();
    //JSONObject obj1 = new JSONObject();

    GetLeaseDetails(String payload){
        this.leaseNum="";
        JSONObject jsonObject = new JSONObject(payload);
        String facNum = jsonObject.getString("leaseNum");
        String[] parts = facNum.split("--");
        this.leaseNum=parts[0];
    }

    public synchronized String getLeaseDetails(){
        JSONObject obj = new JSONObject();
        String leaseDetails=null;
        try {
            logger.debug("getLeaseDetails:leaseNum:"+this.leaseNum);
            if ((this.leaseNum) == null) {
                logger.error("Trying to fetch trade details. leaseNum not found");
                return null;
            }
            leaseDetails=getDetails(this.leaseNum);
            logger.debug("response LeaseDetails:"+obj.toString());
            return leaseDetails;
        }catch (Exception e){
            e.printStackTrace();
        }
        return leaseDetails;
    }

    protected String getDetails(String leaseNum){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            String sql="select * from LEASE_MASTER where Lease_Facility_Number=? ";
            logger.debug("query to get leaseDetails : "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,leaseNum);
            ResultSet rs = stmt.executeQuery();
            while( rs.next() ){
                obj.put("custId", rs.getString("cxCifID")!= null ? rs.getString("cxCifID") : "");
                obj.put("StakeHolder_ID", rs.getString("StakeHolder_ID")!= null ? rs.getString("StakeHolder_ID") : "");
                obj.put("CORE_CIFID", rs.getString("CORE_CIFID")!= null ? rs.getString("CORE_CIFID") : "");
                obj.put("Lease_Creation_Date", rs.getString("Lease_Creation_Date")!= null ? rs.getString("Lease_Creation_Date") : "");
                obj.put("Lease_Maturity_Date",rs.getString("Lease_Maturity_Date")!= null ? rs.getString("Lease_Maturity_Date") : "");
                jarray.put(obj);
            }
            obj1.put("leaseDetails", jarray);
            logger.debug("getDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting Lease details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }
}