package clari5.custom.wlalert;

import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;

import java.io.File;
import java.util.List;

public class FileStructure {
    public static CxpsLogger logger = CxpsLogger.getLogger(FileStructure.class);
    static Hocon hocon;
    String filePath;
    String archiveFilePath;
    static Hocon customFraudList ;
    String unzipFilePath;
    String indexingPath;
    List<String> directories;
    static {

        hocon = new Hocon();
        hocon.loadFromContext("custom-fraudlist.conf");

        customFraudList= hocon.get("custom-fraudlist");
    }

    public File[] getDirList(){

        File directory = new File(getInputFilePath());
        if (!directory.exists()) directory.mkdirs();
        if (directory.isDirectory()){
            if(directory.listFiles().length<=0){
                for (String directorieslist: getDirectories()) {
                    File zipFileDirectory = new File(getInputFilePath()+File.separator+directorieslist);
                    if (!zipFileDirectory.exists()) zipFileDirectory.mkdirs();
                }
            }
            return directory.listFiles();}
        logger.warn("No Directry found "+filePath);
        return null;
    }

    public String getFileType(String dirName){
        return customFraudList.get("fileType").getString(dirName);
    }



    public String getInputFilePath() {

        filePath =customFraudList.get("filePath").getString("inputFilePath");
        return filePath;
    }


    public String getArchiveFilePath() {
        archiveFilePath=customFraudList.get("filePath").getString("archiveFilepath");

        return archiveFilePath;
    }

    public String getUnzipFilePath() {
        return unzipFilePath;
    }

    public void setUnzipFilePath(String unzipFilePath) {
        this.unzipFilePath = unzipFilePath;
    }

    public String getIndexingPath() {
        indexingPath =customFraudList.get("filePath").getString("indexingPath");
        return indexingPath;
    }

    public void setIndexingPath(String indexingPath) {
        this.indexingPath = indexingPath;
    }
    public List<String> getDirectories() {

        return directories =customFraudList.get("filePath").getStringList("fileCategory");
    }

    public void setDirectories(List<String> directories) {
        this.directories = directories;;
    }

}


