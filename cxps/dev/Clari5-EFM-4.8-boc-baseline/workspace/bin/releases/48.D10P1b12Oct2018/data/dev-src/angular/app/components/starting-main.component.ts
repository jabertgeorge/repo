import {Component, OnInit} from "@angular/core";
import {GenesisService} from "../services/cc.genesis.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedService} from "../services/cc.shared.service";

declare var swal: any;

@Component({
    selector: 'kyc-main',
    template: require('./starting-main.template.html'),
})
export class StartingMainComponent implements OnInit {

    isLoggedIn: boolean = false;
    isLoading: boolean = true;

    constructor(private service: GenesisService,
                private r: ActivatedRoute,
                private router: Router) {
        this.isLoading = true;
    }

    ngOnInit() {
       /* this.isLoading = true;
        // To validate if User is already logged In or not.
        this.service.loginValidate().subscribe(
            (info: any) => {
                this.isLoading = false;
                this.isLoggedIn = true;
                if (this.isLoggedIn) {
                    this.router.navigate(['/efm/genesis'], {relativeTo: this.r, skipLocationChange: true});
                } else {
                    this.router.navigate(['/efm/login-app'], {relativeTo: this.r, skipLocationChange: true});
                }
            }, (error: any) => {
                this.isLoading = false;
                if (error.status === 500) {
                    swal({
                        title: "Error Occurred<br>You will now be logged Out.",
                        text: SharedService.getFormattedError(error._body),
                        type: "error"
                    }).then(() => {
                        this.service.logout().subscribe((info) => {
                        }, (error) => {
                            console.log("Error during logout");
                        });
                        this.router.navigate(['/efm/login-app'], {relativeTo: this.r, skipLocationChange: true});
                    });
                } else {
                    this.router.navigate(['/efm/login-app'], {relativeTo: this.r, skipLocationChange: true});
                }
            });*/
    }

}
