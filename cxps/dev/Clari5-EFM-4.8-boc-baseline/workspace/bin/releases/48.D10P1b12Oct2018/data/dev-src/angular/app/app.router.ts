import {ModuleWithProviders} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {StartingMainComponent} from "./components/starting-main.component";
import {KYCSearchComponent} from "./components/kyc/search-page/search-page.component";
import {KYCSearchResultComponent} from "./components/kyc/search-result/search-result.component";


const appRoutes: Routes = [

    {
        path: 'kyc',
        component: StartingMainComponent,
        children: [
            {
                path: "",
                component: KYCSearchComponent,

            },
            {
                path: "kyc-result",
                component: KYCSearchResultComponent
            }
        ]
    }


];


export const AppRouter: ModuleWithProviders = RouterModule.forRoot(appRoutes);
