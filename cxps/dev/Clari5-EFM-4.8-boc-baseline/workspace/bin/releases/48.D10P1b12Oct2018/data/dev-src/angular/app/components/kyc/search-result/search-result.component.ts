/**
 * Created by hemanshu on 12/4/18.
 */

import {Component, OnInit} from "@angular/core";
import {
    CustomerDetails, FatcaDetails, UpdatesJson, update, AccountRelatedUser

} from "../model.component";
import {ActivatedRoute, Router} from "@angular/router";
import {KYCService} from "../service/main.service";
import {KYCSearchComponent} from "../search-page/search-page.component";

declare var swal: any;

@Component({
    selector: 'kyc-search-result',
    template: require('./search-result.template.html'),
    styles: [require('../css/dashboard.css').toString()]
})

export class KYCSearchResultComponent implements OnInit {

    constructor(private router: Router, private route: ActivatedRoute, private appService: KYCService) {
    }

    activeScreen: string = "personalInfo";
    completeDetails: any;
    custDetails: CustomerDetails = new CustomerDetails();
    customerId: any;
    isEdit: boolean = false;
    isEditRiskLevel: boolean = false;
    fatcaDetails: FatcaDetails = new FatcaDetails();
    isEditRelatedParty: boolean = false;
    mainDiff: UpdatesJson = new UpdatesJson();
    fatcaDiff: update = new update();
    // changeInPersonalInfo : boolean = false;
    riskLevelDiff: update = new update();
    customerDiff: update = new update();
    riskLevel: string = "";
    canEdit: boolean = true ;
    custType: string;
    blankOwners: number[] = [];
    accountHolderUser: AccountRelatedUser = new AccountRelatedUser();
    ownerInfoArray: Array<AccountRelatedUser> = [];

    ngOnInit() {
        this.customerId = this.appService.getCustomerId();
        this.completeDetails = this.appService.getCustomerDetails();
        this.custDetails = JSON.parse(JSON.stringify(this.completeDetails.custDetails));
        this.canEdit= this.completeDetails.canEdit;
        /*Initialising owner Info Array*/
        let blankOwnerInfo = new AccountRelatedUser();
        this.ownerInfoArray.push(blankOwnerInfo);

        if (this.completeDetails.fatcaDetails) {
            this.fatcaDetails = JSON.parse(JSON.stringify(this.completeDetails.fatcaDetails));
            this.getAccountRelatedUserInfo();
        }
        if (this.completeDetails.riskLevel) {
            this.riskLevel = JSON.parse(JSON.stringify(this.completeDetails.riskLevel));
        }
    }


    /**
     * split and set account holder and owners
     */
    getAccountRelatedUserInfo() {
        if ((this.custDetails.custType == 'P' || this.custDetails.custType == 'NP') && this.fatcaDetails.usPerson == 'US') {
            let users: AccountRelatedUser[] = this.fatcaDetails.accountRelatedUsers;
            if (users.length > 1)
                this.ownerInfoArray = [];
            if (users) {
                for (let user of users) {
                    if (user.udType == 'AH')
                        this.accountHolderUser = user;
                    else
                        this.ownerInfoArray.push(user);
                }
            }
        }
    }

    /**
     * close the user profile and go back to search page
     */
    backToSearch() {
        this.router.navigate(['../'], {skipLocationChange: true, relativeTo: this.route});
    }

    /**
     * show user information
     * @param val
     */
    showInfo(val: string) {
        this.activeScreen = val;
    }

    /**
     * edit user personal details
     */
    editDetails() {
        this.isEditRelatedParty = !this.isEditRelatedParty;
        if (this.riskLevel && this.riskLevel != "") {
            this.isEditRiskLevel = !this.isEditRiskLevel;
        } else {
            swal({
                title: "Risk Level Not Available",
                type: "error",
                text: "Can not edit risk level for the following customer"
            });
        }
        this.isEdit = !this.isEdit;
    }

    /**
     * when the us person is changed
     * @param value
     */
    updateFatcaDetailModel(value: string) {
        if (this.fatcaDetails.usPerson !== this.completeDetails.fatcaDetails.usPerson) {
            this.fatcaDetails = new FatcaDetails();
            this.fatcaDetails.usPerson = value;
        } else if (this.fatcaDetails.usPerson == this.completeDetails.fatcaDetails.usPerson) {
            this.fatcaDetails = JSON.parse(JSON.stringify(this.completeDetails.fatcaDetails));
        }

    }

    /**
     * check for all teh possible diffs and merge them into one and send it to backend
     */
    submitChanges() {
        console.log(this.canEdit);
        if(this.canEdit==false) {
            swal({
                title: "No access",
                type: "error",
                text: "Do not have access to make changes"
            });
        }
        else {
            this.mainDiff = new UpdatesJson();
            this.fatcaDiff = new update();
            this.riskLevelDiff = new update();
            this.customerDiff = new update();

            this.mainDiff.custId = this.customerId;

            //adding risk level diff if any
            if (this.completeDetails.riskLevel) {
                if (this.completeDetails.riskLevel !== this.riskLevel) {
                    let a = {};
                    a["riskLevel"] = this.riskLevel;
                    this.riskLevelDiff.updates = a;
                    this.riskLevelDiff.updateType = "riskLevel"
                    this.mainDiff.changes.push(this.riskLevelDiff);
                }
            }

            //adding customer diff if any
            if (this.completeDetails.custDetails.bankRelatedFamily !== this.custDetails.bankRelatedFamily) {
                let a = {};
                a["bankRelatedFamily"] = this.custDetails.bankRelatedFamily;
                this.customerDiff.updates = a;
                this.customerDiff.updateType = "userProfile"
                this.mainDiff.changes.push(this.customerDiff);
            }

            if (this.fatcaDetails.usPerson == 'US') {
                //Do not save if there are blank fields left
                if (!this.checkBlankValues())
                    return false;

                //combine all teh users before saving
                this.setFatcaDetails();
            }
            if (this.completeDetails.fatcaDetails) {
                if (JSON.stringify(this.completeDetails.fatcaDetails) !== JSON.stringify(this.fatcaDetails)) {
                    this.fatcaDetails.custId = this.customerId;
                    this.fatcaDetails.classification = this.custDetails.custType;
                    if (this.fatcaDetails.usPerson == "US") {
                        this.fatcaDetails.accountRelatedUsers[0].udType = "AH";
                        this.fatcaDetails.accountRelatedUsers[0].name = this.custDetails.name;

                        //set udType as Ow
                        if (this.fatcaDetails.accountRelatedUsers.length > 1) {
                            for (let user of this.fatcaDetails.accountRelatedUsers) {
                                if (this.fatcaDetails.accountRelatedUsers.indexOf(user) != 0)
                                    user.udType = "Ow";
                            }
                        }
                    }
                    else {
                        this.fatcaDetails.accountRelatedUsers = [];
                    }

                    delete this.fatcaDetails["map"];
                    this.fatcaDiff.updateType = "fatca";
                    this.fatcaDiff.updates = this.fatcaDetails;
                    this.mainDiff.changes.push(this.fatcaDiff);
                }
            } else {
                this.fatcaDetails.custId = this.customerId;
                this.fatcaDetails.classification = this.custDetails.custType;
                if (this.fatcaDetails.usPerson == 'US') {
                    this.fatcaDetails.accountRelatedUsers[0].udType = "AH";
                    this.fatcaDetails.accountRelatedUsers[0].name = this.custDetails.name;

                    if (this.fatcaDetails.accountRelatedUsers.length > 1) {
                        for (let user of this.fatcaDetails.accountRelatedUsers) {
                            if (this.fatcaDetails.accountRelatedUsers.indexOf(user) != 0)
                                user.udType = "Ow";
                        }
                    }
                }
                else {
                    this.fatcaDetails.accountRelatedUsers = [];
                }
                this.fatcaDiff.updates = this.fatcaDetails;
                this.fatcaDiff.updateType = "fatca";
                this.mainDiff.changes.push(this.fatcaDiff);
            }

            let that = this;

            //submit the changes to backend
            this.appService.submitChanges(this.mainDiff).subscribe((info: any) => {
                swal({
                    title: "Submitted Successfully",
                    type: "success",
                    text: ""
                });
                that.backToSearch();
            }, (error: any) => {
                swal({
                    title: error._body,
                    type: "error",
                    text: "Changes not submitted"
                });
            });
        }
    }

    /**
     * Check for blank fields before submitting
     */
    checkBlankValues() {
        if (this.accountHolderUser.address == '' || this.accountHolderUser.city == '' || this.accountHolderUser.country == '' || this.accountHolderUser.state == ''
            || this.accountHolderUser.pcode == '' || this.accountHolderUser.tin == '') {
            swal({
                title: "Error",
                type: "error",
                text: "Blank data can not be saved"
            });
            return false;
        }
        if (this.custDetails.custType == 'NP' && this.ownerInfoArray) {
            for (let user of this.ownerInfoArray) {
                if (user.address == '' || user.city == '' || user.country == '' || user.state == ''
                    || user.pcode == '' || user.tin == '') {
                    this.blankOwners.push(this.ownerInfoArray.indexOf(user));
                }
            }
        }
        return true;
    }

    /**
     * combine all the owners and account Holder to the main fatcaDetails
     */
    setFatcaDetails() {
        this.fatcaDetails.accountRelatedUsers = [];
        this.fatcaDetails.accountRelatedUsers.push(this.accountHolderUser);
        if (this.custDetails.custType == 'NP') {
            if (this.ownerInfoArray) {
                for (let user of this.ownerInfoArray) {
                    if (this.blankOwners.indexOf(this.ownerInfoArray.indexOf(user))==-1) {
                        this.fatcaDetails.accountRelatedUsers.push(user);
                    }
                }
            }
        }
    }

    /**
     * add a new owner
     */
    addUser() {
        let user: AccountRelatedUser = new AccountRelatedUser();
        this.ownerInfoArray.push(user);
    }

    /**
     * Delete the given user
     * @param index - the index of the user to delete
     */
    deleteUser(index: number) {
        this.ownerInfoArray.splice(index, 1);
    }

}
