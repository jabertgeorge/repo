package clari5.custom.wlalert.daemon;

import java.util.ArrayList;
import java.util.List;

public class RuleFact {
    private List<String> name = new ArrayList<>();
    private List<String> passport= new ArrayList<>();
    private List<String>nic = new ArrayList<>();
    private String listName;

    public List<String> getName() {
        return name;
    }

    public void addName(String name) {
          this.name.add(name);
    }

    public List<String> getPassport() {
        return passport;
    }

    public void addPassport(String passport) {
        this.passport.add(passport);
    }

    public List<String> getNic() {
        return nic;
    }

    public void addNic(String nic) {
        this.nic.add(nic);
    }

    public String getList() {
        return listName;
    }

    public void setList(String listName) {
        this.listName = listName;
    }

    @Override
    public String toString() {
        return "RuleFact{" +
                "name=" + name.toString() +
                ", passport=" + passport.toString() +
                ", nic=" + nic.toString() +
                ", listName='" + listName + '\'' +
                '}';
    }
}
