clari5.kyc.entity{
	cust-kyc-cng-his {
		attributes = [
			{ name : custId, type ="string:100" , key=true}
			{ name : verify-date, type: timestamp , key=true}

			{ name : update-type, type = "string:50"}
			{ name : updates, type = "string:8000" }
			{ name : created-date, type : timestamp }
			{ name : created-userId, type = "string:200" }
            { name : verifier-userId, type = "string:200" }
            { name : is-accepted, type = "string:50" }
            { name : remark, type = "string:8000" }
		]
    }
}
