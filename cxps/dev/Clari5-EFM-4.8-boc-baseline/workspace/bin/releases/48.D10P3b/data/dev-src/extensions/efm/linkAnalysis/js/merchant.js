/**
 * Created by gaurav on 4/5/15.
 */
/* merchant.js start */
var oTable;
var mrchntSrch="";
var mrchntOutput="";
var customerTab="";
var accountTab="";

mrchntSrch="<div class='heading' ><h3>Merchant Search</h3></div>";

mrchntSrch+="<form id='mrchntTemplate'><table id='mrchntListD' cellspacing='2' cellpadding='2'>"
    +"<tr class='separator'><td class='fontSize'>Merchant ID</td></tr>"
    +"<tr class='separator'><td class='fontSize'><input type='text' value='' class='validate[groupRequiredHighlight]' id='mrchntNum' /></td></tr>"
    +"<tr class='separator'><td class='fontSize'>Merchant Name</td></tr>"
    +"<tr class='separator'><td class='fontSize'><input type='text' value='' class='' id='mrchntName' /></td></tr>"
    +"<tr class='separator'><td class='fontSize'><a class='abutton search-w-def' onClick='loadMrchntData(this)' style='margin-left:50px;'>Search</a></td></tr></table></form>";

mrchntOutput="<div id='mrchntSrchLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\"><h3>Merchant Details</h3></div><div id='tabMrchnt' style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id='mrchntSrchDtls' class='dataTableCss' style='text-align:left;overflow:auto;' ></table></div></div>";

var acctTreeData="";

function initMrchnt()
{
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#mrchntListD").show();
    $("#mrchntSrchLink").hide();
    document.onkeypress = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closeAcctEvidence(acctCount);
        }
    };
}

var treeObj="";
var mrchntName=null;
var schType=null;
var brchId=null;
var mrchntNum=null;
function loadMrchntDataold(mrchntId)
{
    mrchntName=($("#mrchntName").val()!="")?$("#mrchntName").val().trim():null;
    //brchId=($("#brchId").val()!="")?$("#brchId").val().trim():null;
    mrchntNum=($("#mrchntNum").val()!="")?$("#mrchntNum").val().trim():null;
    if(acctValidation()){
        var mrchntObject={
            "branchId": brchId,
            "mrchntId": mrchntName,
            "mrchntId": mrchntNum
        };
        getMrchntDtlsOnValidate(mrchntObject);
        if($("#mrchntName").val()!=""){
            treeObj={
                "mrchntId":mrchntName
            };
        }else if(mrchntId!=""){
            treeObj={
                "mrchntId":mrchntNum
            };
        }
    }
}

function loadMrchntData(mrchntId)
{
    mrchntId=($("#mrchntNum").val()!="")?$("#mrchntNum").val().trim():null;
    mrchntName=($("#mrchntName").val()!="")?$("#mrchntName").val().trim():null;
    //taxId=($("#taxId").val()!="")?$("#taxId").val().trim():null;
    //emailId=($("#emailId").val()!="")?$("#emailId").val().trim():null;
    //phnNum=($("#phnNum").val()!="")?$("#phnNum").val().trim():null;
    if(srchValidation()){
        var mrchntObject={
            //"mrchntId": mrchntId,
            "custId": mrchntId,
           // "pan": taxId,
           // "email": emailId,
            "mrchntName":mrchntName
           // "mobile1": phnNum
        };
        //getCustDtlsOnValidate(custObject);

        var ifFormFilled = false;
        $("#analDiv").hide();
        if ( mrchntId != null)
            ifFormFilled = true;
        /*if ( taxId != null)
            ifFormFilled = true;
        if ( emailId != null)
            ifFormFilled = true;*/
        if ( mrchntName != null)
            ifFormFilled = true;
        if (ifFormFilled) {
            $("#mrchntNum").removeClass("validate[groupRequiredHighlight]");
            $("#mrchntNum").addClass("validate[custom[oneSpcLetterNumber]]");
            $("#mrchntName").addClass("validate[custom[onlyLetterSp,minSize[3]]]");
            //$("#taxId").addClass("validate[custom[onlyLetterNumber]]");
            //$("#emailId").addClass("validate[custom[email]]");
            var isCond = $("#mrchntTemplate").validationEngine('validate', {scroll: false});
            if(isCond){
                //getCustomerDetails(mrchntObject,mrchntRespHandler,true); //responsible for server call
                //for getting dummy JSON data
                getJsonData(mrchntObject,mrchntRespHandler,true);
            }
        } else {
            $("#mrchntNum").removeClass("validate[custom[oneSpcLetterNumber]]");
            $("#custmrchnttaxId").removeClass("validate[custom[onlyLetterNumber]]");
            //$("#emailId").removeClass("validate[custom[email]]");
            $("#mrchntNum").addClass("validate[groupRequiredHighlight]");
            var isTrue = $("#mrchntTemplate").validationEngine('validate', {scroll: false});
        }

    }
}

function getMrchntDtlsOnValidate(mrchntObject)
{
    var ifFormFilled = false;
    if ( mrchntName != null)
        ifFormFilled = true;
    if ( mrchntNum != null)
        ifFormFilled = true;
    if (ifFormFilled) {
        $("#mrchntName").addClass("validate[custom[oneSpcLetterNumber]]");
        $("#mrchntNum").addClass("validate[custom[oneSpcLetterNumber]]");
        var isAcctCond = $("#mrchntTemplate").validationEngine('validate', {scroll: false});
        if(isAcctCond){
            getAccountDetails(mrchntObject,mrchntRespHandler,true);
        }
    } else {
        $("#mrchntName").removeClass("validate[custom[oneSpcLetterNumber]]");
        $("#mrchntNum").removeClass("validate[custom[oneSpcLetterNumber]]");
        $("#mrchntName").addClass("validate[groupRequiredHighlight]");
        $("#mrchntTemplate").validationEngine('validate', {scroll: false});
    }
}

function acctValidation(){
    if(mrchntName==null  && mrchntNum==null){
        $("#mrchntName").addClass("hightlightDiv");
        $("#mrchntNum").addClass("hightlightDiv");
        return true;
    }
    else{
        $("#mrchntName").removeClass("hightlightDiv");
        $("#mrchntNum").removeClass("hightlightDiv");
        return true;
    }
}

function mrchntRespHandler(resp)
{
    var response=resp.custAttrList;
    console.log(response,"response with custATTrList");
    if(response=="" || response==undefined){
        displayNotification("Merchant ID doesn't exist.", true, "warning");
    }else{
        clearTabs();
       //addTab('Customer','');
       addTab('Merchant',''); //Added for merchant
        $("#custSrchLink").show();
        //var dataObj=makeJsonForDatatable(response, "addTab","mrchntNum",reqstPage); //Added for merchant
        //var dataObj=makeJsonForDatatable(response, "addTab","custId",reqstPage); //Added for merchant
        var dataObj=makeJsonForDatatable(response, "addTab","custId",reqstPage);
        //displayData(dataObj,"tabCust","custSrchDtls");
        displayData(dataObj,"tabMrchnt","mrchntSrchDtls");  //Added for merchant
        toggleSrchWindow();
    }
}


var acctCount="";
var inputObj="";
var inputObject="";
var accountID="";
function acctLoad(mrchntId,tabCount){
    merchantID=mrchntId;
    $("#menuBlock").hide();
    acctCount=tabCount;
    inputObject={
        "mrchntId":mrchntId
    };
    if($("#refId").val()!=""){
    }
    getAccountHardFacts(inputObject,acctHFactsRespHandler,true);
}

function requiredOneOfGroup(field, rules, i, options){
    var fieldsWithValue = $('input[type=text]').filter(function(){
        if($(this).val().length>0){
            return true;
        }
    });
    if(fieldsWithValue.length<1){
        return "At least one field in this set must have a value.";
    }
}

var custId=null;
var custName=null;
var taxId=null;
var passNum=null;
var emailId=null;
var adhNum=null;
var pinCode=null;
var phnNum=null;



/* account.js end */

/*test code for dummy*/
function getJsonData(inputObject,callback){
    //alert("in json load Data");
    // window.open("strFinal.jsp","_self");
    $.ajax({
        //url: "http://cxpsadm-vostro-1014:5000/AML/ReportJsonServlet?reportId=1034",
        //url: "http://cxpsadm-vostro-1014:5000/AML/ReportJsonServlet?reportId="+xReportIdVal,
        url: "../io/jsonData/merchant.json",
        cache: false,
        method: "get",
        //resultType: "json",
        success: function(result){
            console.log(result,"result");
            var x = JSON.parse(result);
            callback(x);
        },
        error : function(a){
            alert("In error");
        }
    });
}


function loadData(xReportIdVal){


}