package clari5.customAml.web;

import java.sql.*;
import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetPawnDetails {
    CxpsLogger logger = CxpsLogger.getLogger(GetPawnDetails.class);

    private static String pawnNum="";
    JSONObject obj = new JSONObject();
    JSONObject obj1 = new JSONObject();

    GetPawnDetails(String payload){
        this.pawnNum="";
        JSONObject jsonObject = new JSONObject(payload);
        String facNum = jsonObject.getString("pawnNum");
        String[] parts = facNum.split("--");
        this.pawnNum=parts[0];
    }

    public synchronized String getPawnDetails(){
        JSONObject obj = new JSONObject();
        String pawnDetails=null;
        try {
            logger.debug("getPawnDetails:pawnNum:"+this.pawnNum);
            if ((this.pawnNum) == null) {
                logger.error("Trying to fetch trade details. pawnNum not found");
                return null;
            }
            pawnDetails=getDetails(this.pawnNum);
            logger.debug("response custEmpTree:"+obj.toString());
            return pawnDetails;
        }catch (Exception e){
            e.printStackTrace();
        }
        return pawnDetails;
    }

    protected String getDetails(String pawnNum){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            String sql="select a.cust_id,a.name ,a.scheme_type,a.currency,a.opened_date,a.status,a.scheme_code," +
                    "p.Dt_of_Pawning_Acct_Opening_Renew,p.Granted_Pawning_Amount,p.Settlement_Maturity_Date from " +
                    "AML_ACCOUNT_VIEW as a LEFT JOIN pawning_master as p ON a.acct_id=p.Account_ID " +
                    "where a.acct_id=? ";
            logger.debug("query to get pawnDetails : "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,pawnNum);
            ResultSet rs = stmt.executeQuery();
            while( rs.next() ){
                obj.put("cust_id", rs.getString("cust_id")!= null ? rs.getString("cust_id") : "");
                obj.put("name", rs.getString("name")!= null ? rs.getString("name") : "");
                obj.put("scheme_type", rs.getString("scheme_type")!= null ? rs.getString("scheme_type") : "");
                obj.put("currency", rs.getString("currency")!= null ? rs.getString("currency") : "");
                obj.put("opened_date",rs.getString("opened_date")!= null ? rs.getString("opened_date") : "");
                obj.put("status", rs.getString("status")!= null ? rs.getString("status") : "");
                obj.put("scheme_code", rs.getString("scheme_code")!= null ? rs.getString("scheme_code") : "");
                obj.put("Dt_of_Pawning_Acct_Opening_Renew", rs.getString("Dt_of_Pawning_Acct_Opening_Renew")!= null ? rs.getString("Dt_of_Pawning_Acct_Opening_Renew") : "");
                obj.put("Granted_Pawning_Amount", rs.getString("Granted_Pawning_Amount")!= null ? rs.getString("Granted_Pawning_Amount") : "");
                obj.put("Settlement_Maturity_Date", rs.getString("Settlement_Maturity_Date")!= null ? rs.getString("Settlement_Maturity_Date") : "");
                jarray.put(obj);
            }
            obj1.put("pawningDetails", jarray);
            logger.debug("getDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting Pawning details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }
}