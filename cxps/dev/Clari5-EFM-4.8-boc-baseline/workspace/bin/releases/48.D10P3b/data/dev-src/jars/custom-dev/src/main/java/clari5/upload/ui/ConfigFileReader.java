package clari5.upload.ui;


import java.io.IOException;
import java.util.Properties;

import cxps.apex.utils.CxpsLogger;
import clari5.platform.util.Hocon;

import static cxps.apex.utils.FileUtils.loadFromProperties;

/**
 * Created by didhin
 * Date :  8/4/16.
 */
class ConfigFileReader {
    private String fileBackUpPath = null;
    Properties props = null;
    private static CxpsLogger logger = CxpsLogger.getLogger(ConfigFileReader.class);

    /*
     * Following code reads the configuration files 
     * upload-ui-clari5.conf (contains backup path) and 
     * upload-rdbms.conf(contains db configuration)
     */
    public ConfigFileReader(String fileName) {
        configure();
    }

    public String getFileBackUpPath() {
        return fileBackUpPath;
    }
    public void configure(){
        try {
            Hocon hocon = new Hocon();
            hocon.loadFromContext("upload-ui-clari5.conf");
            fileBackUpPath = hocon.getString("upload.file-backup-path");
            logger.debug("\n*** *** *** Configuration : " + fileBackUpPath + "\n");
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
