package clari5.custom.dev;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.jira.JiraClientException;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import cxps.eoi.IncidentEvent;

import javax.print.DocFlavor;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by deepak on 21/8/17.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    static Hocon loadCustomField;
    static {
        loadCustomField = new Hocon();
        loadCustomField.loadFromContext("custom_field.conf");
    }

    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public String getCaseAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }

        //if (caseEvent.getEventId().startsWith("FA")) {
        //  return super.getCaseAssignee(caseEvent);
        //}

        String brCode = null;
        String entityId = caseEvent.getEntityId();
        System.out.println("Entity id inside getCaseAssignee is : " + entityId);
        if (!entityId.startsWith("A_F_") && !entityId.startsWith("C_F_")) {
            return super.getCaseAssignee(caseEvent);
        }
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        Map<String, Integer> userGroupMap = null;
        try {
            List list = cmsUserCaseAssignment.process(entityId);
            if (list.isEmpty()) {
                System.out.println("BR CODE not available for account id -> " + entityId);
                return super.getCaseAssignee(caseEvent);
            }
            System.out.println("list datainside CustomCmsFormatter getCaseAssignee: " + list);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Map) {
                    userGroupMap = (Map) list.get(i);
                    if (userGroupMap.isEmpty()) {
                        return super.getCaseAssignee(caseEvent);
                    }
                }
                if (list.get(i) instanceof String) {
                    brCode = (String) list.get(i);
                    if (brCode == null) {
                        return super.getCaseAssignee(caseEvent);
                    }
                }
            }
            System.out.println("data inside usergroupmap is: " + userGroupMap);
        } catch (Exception e) {
            System.out.println("data coming null... Hence calling default logic inside case assignee...");
            return super.getCaseAssignee(caseEvent);
            //e.printStackTrace();
        }
        String moduleId = caseEvent.getModuleId();
        CmsModule cmsModule = (CmsModule) cmsJira.get(moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        //  System.out.println("for department "+departmentMap);
        String userGroup = cmsModule.getUserGroup();

        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        try {
            resNextUser = getUserWithLeastCase(userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("dda br code inside CustomCmsFormatter getCaseAssignee() is: " + brCode);
        Map<String, Integer> map = caseMap(CmsUserCaseAssignment.channelUserGroupMap.get(brCode), resNextUser);
        CmsUserCaseAssignment.channelUserGroupMap.put(brCode, map);
        System.out.println("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public Map<String, Integer> caseMap(Map<String, Integer> map, String resNextUser) {
        Map<String, Integer> map1 = map;
        for (Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().contains(resNextUser)) {
                int count = map1.get(resNextUser);
                count += 1;
                map1.put(resNextUser, count);
            }
        }
        return map1;
    }


    @Override
    public String getIncidentAssignee(IncidentEvent caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");

            throw new CmsException("Unable to get resource cmsjira");
        }

        //if (caseEvent.getEventId().startsWith("FA")) {
        //  return super.getIncidentAssignee(caseEvent);
        //}

        String brCode = "";
        String entityId = caseEvent.getEntityId();
        System.out.println("Entity id inside getIncidentAssignee is: " + entityId);
        if (!entityId.startsWith("A_F_") && !entityId.startsWith("C_F_")) {
            return super.getIncidentAssignee(caseEvent);
        }
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        Map<String, Integer> userGroupMap = new HashMap<>();
        try {
            List list = cmsUserCaseAssignment.process(entityId);
            if (list.isEmpty()) {
                System.out.println("calling superClass.BRCODE not available for account Id -> " + entityId);
                return super.getIncidentAssignee(caseEvent);
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Map) {
                    userGroupMap = (Map) list.get(i);
                    System.out.println("usergroup map inside getIncidentassignee : " + userGroupMap);
                    if (userGroupMap.isEmpty()) {
                        System.out.println("calling superClass for userGroupMap as it is empty");
                        return super.getIncidentAssignee(caseEvent);
                    }
                }
                if (list.get(i) instanceof String) {
                    brCode = (String) list.get(i);
                    System.out.println("brcode is: " + brCode);
                    if (brCode == null) {
                        System.out.println("calling super as brcode not available:");
                        return super.getIncidentAssignee(caseEvent);
                    }
                }
            }
            System.out.println("data inside usergroupmap inside incident assignee is: " + userGroupMap);
        } catch (Exception e) {
            System.out.println("data coming null... hence calling default logic inside Incident assignee");
            String nextAssignee = super.getIncidentAssignee(caseEvent);
            System.out.println("Next user is " + nextAssignee);
            return super.getIncidentAssignee(caseEvent);
            //e.printStackTrace();
        }
        CmsModule cmsModule = (CmsModule) cmsJira.get(caseEvent.getModuleId());
        Map<String, String> departmentMap = cmsModule.getDepartments();
        System.out.println("for module id  " + cmsModule);
        String userGroup = cmsModule.getUserGroup();
        String departmentName = caseEvent.getProject();
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;

        try {
            resNextUser = getUserWithLeastCase(userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("incident assignee dda br code data from CmsUserCaseAssignment is: " + brCode + " for accountId is -> " + entityId);
        Map<String, Integer> map = caseMap(CmsUserCaseAssignment.channelUserGroupMap.get(brCode), resNextUser);
        CmsUserCaseAssignment.channelUserGroupMap.put(brCode, map);
        System.out.println("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public String getUserWithLeastCase(Map<String, Integer> map) {
        Set<Entry<String, Integer>> set = map.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        for (Map.Entry<String, Integer> entry : list) {
            System.out.println(entry.getKey() + " ==== " + entry.getValue());
        }
        String user = list.get(0).getKey();

        System.out.println("User with least no of cases " + user);
        return user;
    }

    public static String getClassnameFromMnemonic(String mnemonic) {

        String className = "";
        try (CxConnection con = CmsUserCaseAssignment.getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT class_name from WS_MNEMONIC WHERE mnemonic = ?");
            ps.setString(1, mnemonic);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                className = rs.getString("class_name") != null ? rs.getString("class_name")
                        : "";
            }
            return className.endsWith("Event") ? className : className + "Event";
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return className.endsWith("Event") ? className : className + "Event";
    }

    public static String getTableNamefromClassName(String className) {
        String tableName = "";
        try {
            for (Annotation annotation : Class.forName("cxps.events." + className).getAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();
                Method method = type.getMethod("Name");
                Object value = method.invoke(annotation, (Object[]) null);
                tableName = (String) value;
                System.out.println("table name from Event Name: " + tableName);
            }
            return tableName != null ? tableName : "";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return tableName != null ? tableName : "";
    }

    public static String getAccountIdForStrReport(String tableName, String eventId) {
        String accountID = "";
        try (CxConnection con = CmsUserCaseAssignment.getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT cx_acct_id from " + tableName + " WHERE  event_id= ?");
            ps.setString(1, eventId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                accountID = rs.getString("cx_acct_id") != null ? rs.getString("cx_acct_id")
                        : "";
            }
            return accountID != null ? accountID : "";
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return accountID != null ? accountID : "";
    }

    protected String getCustName(String cif) {
        String custName = "";
        try (CxConnection connection = CmsUserCaseAssignment.getConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT custName FROM CUSTOMER WHERE cxCifID = ?")) {
            int i = 1;
            ps.setString(i, cif);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    custName = rs.getString("custName") != null ? rs.getString("custName") : "Not Available";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (custName != null || !custName.isEmpty() ? custName : "Not Available");
    }


    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws IOException, CmsException {

        System.out.println("event_id from incident event is: " + event.getEventId());

        CxJson json = null;
        if (event.getEventId() == null || event.getEventId().isEmpty()) {
            json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        }

        if (!event.getEventId().startsWith("FA") && event.getEntityId().startsWith("A_")
                && !event.getEventId().startsWith("TF")) {
            CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
            json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
            String eventJson = event.convertToJson(event);
            Hocon h = new Hocon(eventJson);
            if (cmsMod.getCustomFields().containsKey("accountsDetails") && h.get("evidence") != null) {
                String jiraId_accountsDetails = cmsMod.getCustomFields().get("accountsDetails").getJiraId();
                if (json.get("fields").hasKey(jiraId_accountsDetails)) {
                    json.get("fields").put(jiraId_accountsDetails, event.getEntityId());
                }
            }
            System.out.println("json data from CMS utility is: " + json);
            if (json.get("fields").hasKey(loadCustomField.getString("fields.customerName"))) {
                json.get("fields").put(loadCustomField.getString("fields.customerName"),
                        getCustName(event.getCaseEntityId() != null ?
                        event.getCaseEntityId() : "") != null ? getCustName(event.getCaseEntityId() != null ?
                        event.getCaseEntityId() : "") : "");
            }
        } else if (!event.getEventId().startsWith("FA") && !event.getEntityId().startsWith("A_")
                && !event.getEventId().startsWith("TF")) {
            System.out.println("json data from CMS utility is: " + json);
            String className = getClassnameFromMnemonic(event.getEventId().substring(0, event.getEventId().indexOf("_")));
            String tableName = getTableNamefromClassName(className);
            String accountId = getAccountIdForStrReport(tableName, event.getEventId());

            CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
            json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
            String eventJson = event.convertToJson(event);
            Hocon h = new Hocon(eventJson);
            if (cmsMod.getCustomFields().containsKey("accountsDetails") && h.get("evidence") != null) {
                String jiraId_accountsDetails = cmsMod.getCustomFields().get("accountsDetails").getJiraId();
                if (json.get("fields").hasKey(jiraId_accountsDetails)) {
                    json.get("fields").put(jiraId_accountsDetails, accountId);
                }
            }
            if (json.get("fields").hasKey(loadCustomField.getString("fields.customerName"))) {
                json.get("fields").put(loadCustomField.getString("fields.customerName"),
                        getCustName(event.getCaseEntityId() != null ?
                        event.getCaseEntityId() : "") != null ? getCustName(event.getCaseEntityId() != null ?
                        event.getCaseEntityId() : "") : "");
            }
        } else {
            json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
            System.out.println("json data from CMS utility is: " + json);
            if (json.get("fields").hasKey(loadCustomField.getString("fields.customerName"))) {
                json.get("fields").put(loadCustomField.getString("fields.customerName"),
                        getCustName(event.getCaseEntityId() != null ?
                        event.getCaseEntityId() : "") != null ? getCustName(event.getCaseEntityId() != null ?
                        event.getCaseEntityId() : "") : "");
            }
            //json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        }
        return json;
    }
}