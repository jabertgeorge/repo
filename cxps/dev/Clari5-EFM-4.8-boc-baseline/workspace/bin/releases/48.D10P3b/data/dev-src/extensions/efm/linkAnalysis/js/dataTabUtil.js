/* dataTabUtil.js start */
var oTable;
var extHeaders=new Array();

//var headJson={"caseId":"Case ID","entityKey":"Customer ID","openedDate":"Created Date","status":"Status",
//"resolution":"Resolution","module":"Module","closedDate":"Closed Date","incidentId":"Incident ID","acctId":"Account ID","acctCurrType":"Currency Type","tranId":"Tran ID","tranDate":"Tran Date",
//"tranAmount":"Tran Amount","tranType":"Tran Type","name":"Account Name","branchId":"Branch ID","schemeType":"Scheme Type","pan":"PAN Number","custId":"Customer ID","uin":"Adhaar Number","mobile1":"Mobile Num","factName":"Intelligence","score":"Value","riskRating":"Risk Rating","timestamp":"Event Time","resZip":"Pin Code","factname":"Intelligence","factscore":"Value","facttimestamp":"Updated On","computed":"Updated On","partTranType":"Debit/Credit","schemeCode":"Scheme Code","acctType":"Account Type","acctStatus":"Account Status","opinionName":"Opinion Name","eventIds":"Event ID's","contraAcctId":"Counter Party","hostId":"Host ID","acountId":"Account ID","contraAccount":"Contra Account","drCr":"Dr/Cr","tranCurrency":"Tran Currency","tranBranchId":"Tran Branch ID","partTranSrlNo":"Part Tran Srl No.","tranSubType":"Tran Sub Type","field":"Field(value)","op":"Operator","rhs":"Choice","val":"Val","OverallScore":"Overall Score","docid":"Doc ID","val1":"Nationality","score1":"Score","val2":"Name","score2":"Score","val3":"DOB","score3":"Score","wt":"Weight","risk":"Risk","constitution":"Constitution","partTranSrlNum":"Part Tran Serial No.","dOBI":"Date of Birth","email":"Email","natureOfBusiness":"Nature of Business"};

var headJson={"caseId":"Case ID","entityKey":"Customer ID","openedDate":"Created Date","status":"Status",
    "resolution":"Resolution","module":"Module","closedDate":"Closed Date","incidentId":"Incident ID","acctId":"Account ID","acctCurrType":"Currency Type","tranId":"Tran ID","tranDate":"Tran Date",
    "tranAmount":"Tran Amount","tranType":"Tran Type","name":"Account Name","branchId":"Branch ID","schemeType":"Scheme Type","pan":"PAN Number","custId":"Customer ID","uin":"Adhaar Number","mobile1":"Mobile Num","factName":"Intelligence","score":"Value","riskRating":"Risk Rating","timestamp":"Event Time","resZip":"Pin Code","factname":"Intelligence","factscore":"Value","facttimestamp":"Updated On","computed":"Updated On","partTranType":"Debit/Credit","schemeCode":"Scheme Code","acctType":"Account Type","acctStatus":"Account Status","opinionName":"Opinion Name","eventIds":"Event ID's","contraAcctId":"Counter Party","hostId":"Host ID","acountId":"Account ID","contraAccount":"Contra Account","drCr":"Dr/Cr","tranCurrency":"Tran Currency","tranBranchId":"Tran Branch ID","partTranSrlNo":"Part Tran Srl No.","tranSubType":"Tran Sub Type","field":"Field(value)","op":"Operator","rhs":"Choice","val":"Val","OverallScore":"Overall Score","docid":"Doc ID","val1":"Nationality","score1":"Score","val2":"Name","score2":"Score","val3":"DOB","score3":"Score","wt":"Weight","risk":"Risk","constitution":"Constitution","partTranSrlNum":"Part Tran Serial No.","dOBI":"Date of Birth","email":"Email","natureOfBusiness":"Nature of Business","annualTurnOver":"Annual Turn Over","deviceId":"Device Id","mailingAddr":"Mailing Address","dateOfIncorp":"Date Of In Corporation","dob":"Date Of Birth","mrchntId":"Merchant Id","accountType":"Account Type","deviceType":"Device Type",
    "browserInfo":"Browser Info","browserName":"Browser Name","cookieEnabled":"Cookie Enabled","fonts":"Fonts","os":"Operating System","plugins":"Plugins","screenResolution":"Screen Resolution","timezone":"Time Zone","dvcNum":"Device Id","gender":"Gender","natOfBusn":"Nature of Business","noOfStr":"Number of Str","lastFiledStr":"Last Filed Str"
};


var displayData = function ( datas,id,tabId ) {
	$('#'+id).html("");
	var tbl = document.createElement("table");
	$(tbl).addClass("dataTableCss");
	$(tbl).attr("id",tabId);
	$(tbl).css("text-align", "left");
	$('#'+id).html(tbl);
	oTable = $(tbl).dataTable({"aaData": datas[0],
		"aoColumns": datas[1]
		,"bRetrieve": true
		,"bDestroy": false
	});
}

function makeJsonForDatatable(inp,methName,colLink,reqstForm){
        if( inp.length == undefined || inp.length == 0 ){
        }
        else{
                var resp = [ ];
                var myArray = getTableData(inp,methName,colLink,reqstForm);
                var tableHeaders = getTableHeaders(inp);
                resp.push(myArray);
                resp.push(tableHeaders);
                return resp;
            }
}

function getTableHeaders(inp){
        parseJsonHeaders();
    var aoColumnVal = [ ];
    var tableHeaders =  [];
    if( inp.length == undefined){
        for ( key in inp ) {
            aoColumnVal.push(key);
        }
        for ( var i = 0 ; i < aoColumnVal.length; i++ ) {
            tableHeaders.push({ "sTitle":  extHeaders[aoColumnVal[i]] });
        }
        return tableHeaders;
    }
    else {
        for ( key in inp[0] ) {
                aoColumnVal.push(key);
        }

	for ( var i = 0 ; i < aoColumnVal.length; i++ ) {
	    if(aoColumnVal[i]!="className" && aoColumnVal[i]!="incident" && aoColumnVal[i]!="cTRS" && aoColumnVal[i]!="wlmatches" && aoColumnVal[i]!="cRCvalue" && aoColumnVal[i]!="str" && aoColumnVal[i]!="summary" && aoColumnVal[i]!="timestamp" && aoColumnVal[i]!="source"){
	        tableHeaders.push({ "sTitle": extHeaders[aoColumnVal[i]] });
	    }
	}
	}
        return tableHeaders;
}

/*
function getTableData(inp,methName,colLink,reqstForm){
        var myArray = [ ];
        var xyz = [ ];
        if(inp.length == undefined){
                xyz = [ ];
                    var methCal="javascript:"+methName+"('"+inp[0]+"')";
                      xyz.push("<a style='color:#7070FF;' oncontextmenu='return false;' href="+methCal+" >" + inp[0] + "</a>");
                for(key in inp) {
                      xyz.push(inp[key]);
                }
             myArray.push(xyz);
             return myArray;
        }
        else {
            for (var i=0; i < inp.length; i++){
                xyz = [ ];
                for (key in inp[i]) {
                    if(colLink!="" && key==colLink && key!="className" && key!="incident"){
                       if(methName!=""){
                            if(methName=="showCustEvidence" || methName=="showAcctEvidence"){
                               var methCal="javascript:"+methName+"('"+reqstForm+"','"+inp[i][key]+"|"+inp[i]["source"]+"')";
                            }else{
                               var methCal="javascript:"+methName+"('"+reqstForm+"','"+inp[i][key]+"')";
                            }
                       xyz.push("<a style='color:#7070FF;' onmouseover='windowSts()'  onmouseout='windowSts()' oncontextmenu='return false;' href=\""+methCal+"\" >" + inp[i][key] + "</a>");
                       }
                    }
                    else if(key!="className" && key!="timestamp" && key!="incident" && key!="cTRS" && key!="wlmatches" && key!="cRCvalue" && key!="str" && key!="summary" && key!="source"){
                        if(key=="tranAmount"){
                            xyz.push("<div style='text-align:center;'>"+getFormattedNumber(inp[i][key])+"</div>");
                        }else{
                           xyz.push(inp[i][key]);
                        }
                    }
                }
			    myArray.push(xyz);
		    }
		}
        return myArray;
}
 */
function getTableData(inp,methName,colLink,reqstForm){
    var myArray = [ ];
    var xyz = [ ];
    var objs =[];
    if(inp.length == undefined){
        xyz = [ ];
        var methCal="javascript:"+methName+"('"+inp[0]+"')";
        xyz.push("<a style='color:#7070FF;' oncontextmenu='return false;' href="+methCal+" >" + inp[0] + "</a>");
        for(key in inp) {
            xyz.push(inp[key]);
        }
        myArray.push(xyz);
        return myArray;
    }
    else {
        for (var i=0; i < inp.length; i++){
            xyz = [ ];
            for (key in inp[i]) {
                if(typeof inp[i][key] !== "object"){
                    if(colLink!="" && key==colLink && key!="className" && key!="incident"){
                        if(methName!=""){
                            if(methName=="showCustEvidence" || methName=="showAcctEvidence"){
                                var methCal="javascript:"+methName+"('"+reqstForm+"','"+inp[i][key]+"|"+inp[i]["source"]+"')";
                            }else{
                                var methCal="javascript:"+methName+"('"+reqstForm+"','"+inp[i][key]+"')";
                            }
                            xyz.push("<a style='color:#7070FF;' onmouseover='windowSts()'  onmouseout='windowSts()' oncontextmenu='return false;' href=\""+methCal+"\" >" + inp[i][key] + "</a>");
                        }
                    }
                    else if(key!="className" && key!="timestamp" && key!="incident" && key!="cTRS" && key!="wlmatches" && key!="cRCvalue" && key!="str" && key!="summary" && key!="source"){
                    if(key=="tranAmount"){
                        xyz.push("<div style='text-align:center;'>"+getFormattedNumber(inp[i][key])+"</div>");
                    }else{
                        xyz.push(inp[i][key]);
                    }
                }
                }
                else if(typeof inp[i][key] == "object"){
                    //for (key in inp[i]["screenResolution"][0]){
                    for (key in inp[i][key][0]){
                        console.log(key);
                        console.log(inp[i][key][0][key]);
                        objs.push(inp[i][key][0][key]);
                    }
                    xyz.push(objs);
                }
            }

            myArray.push(xyz);


        }
    }
    return myArray;
}

function getFormattedNumber(num) {
	return Number(num).toPrecision();
}

function windowSts(){
window.status="";
return true;
}

function parseJsonHeaders(){
        var jsonObj=headJson;
        for(key in jsonObj){
	    extHeaders[key]=jsonObj[key];
        }
}
/* dataTabUtil.js end */
