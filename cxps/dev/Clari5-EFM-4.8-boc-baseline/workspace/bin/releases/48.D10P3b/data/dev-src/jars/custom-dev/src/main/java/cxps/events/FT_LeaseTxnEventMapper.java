// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_LeaseTxnEventMapper extends EventMapper<FT_LeaseTxnEvent> {

public FT_LeaseTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_LeaseTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_LeaseTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_LeaseTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_LeaseTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_LeaseTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_LeaseTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getTxnCurrency());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setTimestamp(i++, obj.getLeaseSettlementDate());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getSysCountry());
            preparedStatement.setString(i++, obj.getEventTime());
            preparedStatement.setDouble(i++, obj.getLcyAmt());
            preparedStatement.setTimestamp(i++, obj.getSystemTime());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getLcyCurncy());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getTxnId());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getTxnType());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getStakeHolder());
            preparedStatement.setString(i++, obj.getLeasFacilityNo());
            preparedStatement.setString(i++, obj.getTxnSrlNo());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getStakeholderId());
            preparedStatement.setString(i++, obj.getTxnSubType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_LEASE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_LeaseTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_LeaseTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_LEASE"));
        putList = new ArrayList<>();

        for (FT_LeaseTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TXN_CURRENCY",  obj.getTxnCurrency());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "LEASE_SETTLEMENT_DATE", String.valueOf(obj.getLeaseSettlementDate()));
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "SYS_COUNTRY",  obj.getSysCountry());
            p = this.insert(p, "EVENT_TIME",  obj.getEventTime());
            p = this.insert(p, "LCY_AMT", String.valueOf(obj.getLcyAmt()));
            p = this.insert(p, "SYSTEM_TIME", String.valueOf(obj.getSystemTime()));
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "LCY_CURNCY", String.valueOf(obj.getLcyCurncy()));
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "TXN_ID",  obj.getTxnId());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "TXN_TYPE",  obj.getTxnType());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "STAKE_HOLDER",  obj.getStakeHolder());
            p = this.insert(p, "LEAS_FACILITY_NO",  obj.getLeasFacilityNo());
            p = this.insert(p, "TXN_SRL_NO",  obj.getTxnSrlNo());
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "STAKEHOLDER_ID",  obj.getStakeholderId());
            p = this.insert(p, "TXN_SUB_TYPE",  obj.getTxnSubType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_LEASE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_LEASE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_LEASE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_LeaseTxnEvent obj = new FT_LeaseTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTxnCurrency(rs.getString("TXN_CURRENCY"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setLeaseSettlementDate(rs.getTimestamp("LEASE_SETTLEMENT_DATE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setSysCountry(rs.getString("SYS_COUNTRY"));
    obj.setEventTime(rs.getString("EVENT_TIME"));
    obj.setLcyAmt(rs.getDouble("LCY_AMT"));
    obj.setSystemTime(rs.getTimestamp("SYSTEM_TIME"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setLcyCurncy(rs.getDouble("LCY_CURNCY"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setTxnId(rs.getString("TXN_ID"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setTxnType(rs.getString("TXN_TYPE"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setStakeHolder(rs.getString("STAKE_HOLDER"));
    obj.setLeasFacilityNo(rs.getString("LEAS_FACILITY_NO"));
    obj.setTxnSrlNo(rs.getString("TXN_SRL_NO"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setStakeholderId(rs.getString("STAKEHOLDER_ID"));
    obj.setTxnSubType(rs.getString("TXN_SUB_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_LEASE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_LeaseTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_LeaseTxnEvent> events;
 FT_LeaseTxnEvent obj = new FT_LeaseTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_LEASE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_LeaseTxnEvent obj = new FT_LeaseTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTxnCurrency(getColumnValue(rs, "TXN_CURRENCY"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setLeaseSettlementDate(EventHelper.toTimestamp(getColumnValue(rs, "LEASE_SETTLEMENT_DATE")));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setSysCountry(getColumnValue(rs, "SYS_COUNTRY"));
            obj.setEventTime(getColumnValue(rs, "EVENT_TIME"));
            obj.setLcyAmt(EventHelper.toDouble(getColumnValue(rs, "LCY_AMT")));
            obj.setSystemTime(EventHelper.toTimestamp(getColumnValue(rs, "SYSTEM_TIME")));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setLcyCurncy(EventHelper.toDouble(getColumnValue(rs, "LCY_CURNCY")));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setTxnId(getColumnValue(rs, "TXN_ID"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setTxnType(getColumnValue(rs, "TXN_TYPE"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setStakeHolder(getColumnValue(rs, "STAKE_HOLDER"));
            obj.setLeasFacilityNo(getColumnValue(rs, "LEAS_FACILITY_NO"));
            obj.setTxnSrlNo(getColumnValue(rs, "TXN_SRL_NO"));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setStakeholderId(getColumnValue(rs, "STAKEHOLDER_ID"));
            obj.setTxnSubType(getColumnValue(rs, "TXN_SUB_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_LEASE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_LEASE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TXN_CURRENCY\",\"MSG_SOURCE\",\"KEYS\",\"LEASE_SETTLEMENT_DATE\",\"CHANNEL\",\"SYS_COUNTRY\",\"EVENT_TIME\",\"LCY_AMT\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"CX_ACCT_ID\",\"TXN_AMT\",\"TRAN_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"LCY_CURNCY\",\"ADD_ENTITY_ID2\",\"TXN_CODE\",\"TXN_ID\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"TXN_TYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"STAKE_HOLDER\",\"LEAS_FACILITY_NO\",\"TXN_SRL_NO\",\"CUST_CARD_ID\",\"CX_CUST_ID\",\"EVENT_NAME\",\"STAKEHOLDER_ID\",\"TXN_SUB_TYPE\"" +
              " FROM EVENT_FT_LEASE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TXN_CURRENCY],[MSG_SOURCE],[KEYS],[LEASE_SETTLEMENT_DATE],[CHANNEL],[SYS_COUNTRY],[EVENT_TIME],[LCY_AMT],[SYSTEM_TIME],[TXN_DR_CR],[CX_ACCT_ID],[TXN_AMT],[TRAN_ID],[HOST_USER_ID],[HOST_ID],[LCY_CURNCY],[ADD_ENTITY_ID2],[TXN_CODE],[TXN_ID],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[TXN_TYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[STAKE_HOLDER],[LEAS_FACILITY_NO],[TXN_SRL_NO],[CUST_CARD_ID],[CX_CUST_ID],[EVENT_NAME],[STAKEHOLDER_ID],[TXN_SUB_TYPE]" +
              " FROM EVENT_FT_LEASE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TXN_CURRENCY`,`MSG_SOURCE`,`KEYS`,`LEASE_SETTLEMENT_DATE`,`CHANNEL`,`SYS_COUNTRY`,`EVENT_TIME`,`LCY_AMT`,`SYSTEM_TIME`,`TXN_DR_CR`,`CX_ACCT_ID`,`TXN_AMT`,`TRAN_ID`,`HOST_USER_ID`,`HOST_ID`,`LCY_CURNCY`,`ADD_ENTITY_ID2`,`TXN_CODE`,`TXN_ID`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`TXN_TYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`STAKE_HOLDER`,`LEAS_FACILITY_NO`,`TXN_SRL_NO`,`CUST_CARD_ID`,`CX_CUST_ID`,`EVENT_NAME`,`STAKEHOLDER_ID`,`TXN_SUB_TYPE`" +
              " FROM EVENT_FT_LEASE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_LEASE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TXN_CURRENCY\",\"MSG_SOURCE\",\"KEYS\",\"LEASE_SETTLEMENT_DATE\",\"CHANNEL\",\"SYS_COUNTRY\",\"EVENT_TIME\",\"LCY_AMT\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"CX_ACCT_ID\",\"TXN_AMT\",\"TRAN_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"LCY_CURNCY\",\"ADD_ENTITY_ID2\",\"TXN_CODE\",\"TXN_ID\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"TXN_TYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"STAKE_HOLDER\",\"LEAS_FACILITY_NO\",\"TXN_SRL_NO\",\"CUST_CARD_ID\",\"CX_CUST_ID\",\"EVENT_NAME\",\"STAKEHOLDER_ID\",\"TXN_SUB_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TXN_CURRENCY],[MSG_SOURCE],[KEYS],[LEASE_SETTLEMENT_DATE],[CHANNEL],[SYS_COUNTRY],[EVENT_TIME],[LCY_AMT],[SYSTEM_TIME],[TXN_DR_CR],[CX_ACCT_ID],[TXN_AMT],[TRAN_ID],[HOST_USER_ID],[HOST_ID],[LCY_CURNCY],[ADD_ENTITY_ID2],[TXN_CODE],[TXN_ID],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[TXN_TYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[STAKE_HOLDER],[LEAS_FACILITY_NO],[TXN_SRL_NO],[CUST_CARD_ID],[CX_CUST_ID],[EVENT_NAME],[STAKEHOLDER_ID],[TXN_SUB_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TXN_CURRENCY`,`MSG_SOURCE`,`KEYS`,`LEASE_SETTLEMENT_DATE`,`CHANNEL`,`SYS_COUNTRY`,`EVENT_TIME`,`LCY_AMT`,`SYSTEM_TIME`,`TXN_DR_CR`,`CX_ACCT_ID`,`TXN_AMT`,`TRAN_ID`,`HOST_USER_ID`,`HOST_ID`,`LCY_CURNCY`,`ADD_ENTITY_ID2`,`TXN_CODE`,`TXN_ID`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`TXN_TYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`STAKE_HOLDER`,`LEAS_FACILITY_NO`,`TXN_SRL_NO`,`CUST_CARD_ID`,`CX_CUST_ID`,`EVENT_NAME`,`STAKEHOLDER_ID`,`TXN_SUB_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

