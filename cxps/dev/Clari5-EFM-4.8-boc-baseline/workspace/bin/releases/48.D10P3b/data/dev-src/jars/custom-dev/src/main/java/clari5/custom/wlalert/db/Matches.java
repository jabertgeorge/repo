package clari5.custom.wlalert.db;

import java.util.ArrayList;
import java.util.List;

public class Matches {
    List<String> matchResult = new ArrayList<>();

    public List<String> getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(List<String> matchResult) {
        this.matchResult = matchResult;
    }


    @Override
    public String toString() {
        return "Matches{" +
                "matchResult=" + matchResult.toString() +
                '}';
    }

}
