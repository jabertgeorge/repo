//this is the list of custom rule name
//insert the rulename within "" and comma seperated 
// eg: var customRuleNames = ["rulename1","rulename2"]
var customRuleNames = ["TEST"];


var resVal; //Global variable for accessing the object of res

var oTable;

function init() {
    var userId = top.user;
    var serverUrl = top.serverUrl;
    setParams(userId, serverUrl);
    $("#canDiv").css("height", ($(document).height() - 150));
    var inputObj = "";
    getInitD(inputObj, initDH, true);

    $("#ruleNameDetailsInfo").validationEngine();
}

function displayNotification(text, ifSticky, type) {
    var toastMessageSettings = {
        text: text,
        sticky: ifSticky,
        position: 'top-center',
        type: type,
        closeText: ''
    };
    $().toastmessage('showToast', toastMessageSettings);
}
var showinput = false;
/*function for getting details of selected rules*/
function takeAction() {
    var e = document.getElementById("ruleNmDD");

    var value = $(e).find('option:selected').attr('data-value');
    console.log("value", JSON.parse(value).ruleName);
    showinput = customRuleNames.includes(JSON.parse(value).ruleName);
    console.log("showinput  ", showinput);
    if (value != undefined) {
        var obj = JSON.parse(value);
        var action = "VIEW";
        if (action == "VIEW") {
            showWLViewForm(obj);
            editReq = false;
        }
    } else {
        initDH(resVal);
    }
}

function showWLViewForm(obj) {
    $("#ruleDetails").addClass("customWidth");
    var attrJson = obj.match;
    var dStyl = "t-cellDef";

    var weight = weightPer(attrJson); //function for getting weight in percentage for match weight

    var atrTb = "";

    atrTb += "<tr>";

    for (var j = 0; j < attrJson.length; j++) {
        atrTb += "<td><span>" + attrJson[j].field + "</span></td>";
        atrTb += "<td>";
        if (attrJson[j].field.toString().toUpperCase() == "DOB")
            atrTb += "<input type='text' class='dp validate[custom[date]]' placeholder='YYYY-MM-DD' style='background: url(../../common/images/calendar.png) no-repeat scroll 100% 1px;'>";
        else {
            if (showinput == true) {
                atrTb += "<input type='text'  placeholder='String' >"; //
            } else
                atrTb += "<input type='text' class='validate[required]' placeholder='String' >"; //

        }


        atrTb += "<span>&nbsp;" + attrJson[j].matchType + "</span><span>," + "&nbsp;" + "</span><span>weight&nbsp;" + (parseInt(attrJson[j].wt) / weight) * 100 + "% Min Match:</span><span>" + attrJson[j].minMatch + "%</span>";
        atrTb += "</td>";

        atrTb += "</tr>";
        if (dStyl == "t-cellDef")
            dStyl = "t-cellGray";
        else
            dStyl = "t-cellDef";
    }
    atrTb += "";

    var descAttr = "<div>";
    var keyVal = "";

    for (keys in obj) {
        if ((typeof(obj[keys]) == "string") && (obj[keys] != null) && (obj[keys] != undefined) && (obj[keys].trim() != "")) {
            if ((keys != "lists") || (keys != "match")) {
                if (keys != "deleted") {
                    descAttr += "<p>";
                    descAttr += "<span>";
                    switch (keys) {
                        case "ruleName":
                            keyVal = "Rule Name";
                            break;
                        case "desc":
                            keyVal = "Description";
                            break;
                        case "maxMatches":
                            keyVal = "Maximum Matches";
                            break;
                        case "cutOffScore":
                            keyVal = "Cut Off Score";
                            break;
                        case "timeSpan":
                            keyVal = "Time Span";
                            break;

                    }
                    descAttr += keyVal;
                    descAttr += ":</span>";
                    descAttr += "<span>";
                    if (keys == "cutOffScore") {
                        descAttr += parseFloat(obj[keys]) * 100;
                        descAttr += "%";
                    } else {
                        descAttr += obj[keys];
                    }
                    descAttr += "</span>";
                    descAttr += "</p>";
                }
            }
        }
    }
    descAttr += "</div>";

    document.getElementById("ruleDetails").innerHTML = atrTb;
    $(".wlRuleContainer").html(descAttr);
    $(".dp").datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
}
var allRuleName = [];
//Populate rule Name
function displayRuleName() {
    var e = document.getElementById("ruleNameInfo");
    $("select#ruleNameInfo option:selected").each(function() {

        if ($("select#ruleNameInfo option:selected").length == 1) {
            allRuleName.length = 0;
        }
        var value = $(this).attr('data-value');

        if (value != 'All') {
            var obj = JSON.parse(value);
            showRuleName(obj);
        } else {
            var obj = resVal.wlDetail;
            ruleList = resVal.wlDetail;
            var frm = "";
            if (ruleList != null) {
                frm += "<option value='--Select--'  >--Select--</option>";
                for (var i = 0; i < ruleList.length; i++) {

                    var lists = "";
                    for (var l = 0; l < ruleList[i].lists.length; l++) {
                        //lists.push(ruleList[i].lists[l]);
                        if (l != ruleList[i].lists.length - 1)
                            lists += ruleList[i].lists[l] + ",";
                        else
                            lists += ruleList[i].lists[l];
                    }
                    if ((ruleList[i].timeSpan != "" && ruleList[i].timeSpan != null) && (ruleList[i].desc != "" && ruleList[i].desc != null)) {
                        frm += "<option value='" + ruleList[i].ruleName + "'  data-value= '" + JSON.stringify(ruleList[i]) + "'>" + ruleList[i].ruleName + " -- " + ruleList[i].desc + ", search in  " + lists + ", against records indexed in the last " + ruleList[i].timeSpan + "</option>";
                    } else if ((ruleList[i].timeSpan == "" || ruleList[i].timeSpan == null) && (ruleList[i].desc == "" || ruleList[i].desc == null)) {
                        frm += "<option value='" + ruleList[i].ruleName + "'  data-value= '" + JSON.stringify(ruleList[i]) + "'>" + ruleList[i].ruleName + " --  search in  " + lists + "</option>";
                    } else if ((ruleList[i].timeSpan == "" || ruleList[i].timeSpan == null) && (ruleList[i].desc != "" && ruleList[i].desc != null)) {
                        frm += "<option value='" + ruleList[i].ruleName + "'  data-value= '" + JSON.stringify(ruleList[i]) + "'>" + ruleList[i].ruleName + " -- " + ruleList[i].desc + ", search in  " + lists + "</option>";
                    } else if ((ruleList[i].timeSpan != "" && ruleList[i].timeSpan != null) && (ruleList[i].desc == "" || ruleList[i].desc == null)) {
                        frm += "<option value='" + ruleList[i].ruleName + "'  data-value= '" + JSON.stringify(ruleList[i]) + "'>" + ruleList[i].ruleName + " --  search in  " + lists + ", against records indexed in the last " + ruleList[i].timeSpan + "</option>";
                    }

                }
            }
            document.getElementById("ruleNmDD").innerHTML = frm;
        }
        //showRuleName(obj);
    })

}

function showRuleName(obj) {
    var rulesName = obj.ruleNames;
    for (var i = 0; i < resVal.listDetail.length; i++) {
        if (obj.list == resVal.listDetail[i].list) {
            for (var j = 0; j < obj.ruleNames.length; j++) {
                for (var k = 0; k < resVal.wlDetail.length; k++) {
                    if (obj.ruleNames[j] == resVal.wlDetail[k].ruleName) {

                        allRuleName.push(obj.ruleNames[j]);

                    }
                }

            }
        }
    }
    var uniqueNames = [];
    $.each(allRuleName, function(i, el) {
        if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
    });
    obj.ruleNames = uniqueNames;
    var frm = "";

    //frm += "<option value='--Select--'  >--Select--</option>";
    frm += "<option value=''  >--Select--</option>";
    if (rulesName != null) {
        for (var i = 0; i < resVal.listDetail.length; i++) {
            if (obj.list == resVal.listDetail[i].list) {
                for (var j = 0; j < obj.ruleNames.length; j++) {
                    for (var k = 0; k < resVal.wlDetail.length; k++) {
                        if (obj.ruleNames[j] == resVal.wlDetail[k].ruleName) {

                            var lists = "";
                            for (var l = 0; l < resVal.wlDetail[k].lists.length; l++) {
                                if (l != resVal.wlDetail[k].lists.length - 1)
                                    lists += resVal.wlDetail[k].lists[l] + " ,";
                                else
                                    lists += resVal.wlDetail[k].lists[l];
                            }

                            if ((resVal.wlDetail[k].timeSpan != "" && resVal.wlDetail[k].timeSpan != null) && (resVal.wlDetail[k].desc != "" && resVal.wlDetail[k].desc != null)) {
                                frm += "<option value='" + resVal.wlDetail[k].ruleName + "'  data-value= '" + JSON.stringify(resVal.wlDetail[k]) + "'>" + resVal.wlDetail[k].ruleName + " -- " + resVal.wlDetail[k].desc + ", search in  " + lists + ", against records indexed in the last " + resVal.wlDetail[k].timeSpan + "</option>";
                            } else if ((resVal.wlDetail[k].timeSpan == "" || resVal.wlDetail[k].timeSpan == null) && (resVal.wlDetail[k].desc == "" || resVal.wlDetail[k].desc == null)) {
                                frm += "<option value='" + resVal.wlDetail[k].ruleName + "'  data-value= '" + JSON.stringify(resVal.wlDetail[k]) + "'>" + resVal.wlDetail[k].ruleName + " --  search in  " + lists + "</option>";
                            } else if ((resVal.wlDetail[k].timeSpan == "" || resVal.wlDetail[k].timeSpan == null) && (resVal.wlDetail[k].desc != "" && resVal.wlDetail[k].desc != null)) {
                                frm += "<option value='" + resVal.wlDetail[k].ruleName + "'  data-value= '" + JSON.stringify(resVal.wlDetail[k]) + "'>" + resVal.wlDetail[k].ruleName + " -- " + resVal.wlDetail[k].desc + ", search in  " + lists + "</option>";
                            } else if ((resVal.wlDetail[k].timeSpan != "" && resVal.wlDetail[k].timeSpan != null) && (resVal.wlDetail[k].desc == "" || resVal.wlDetail[k].desc == null)) {
                                frm += "<option value='" + resVal.wlDetail[k].ruleName + "'  data-value= '" + JSON.stringify(resVal.wlDetail[k]) + "'>" + resVal.wlDetail[k].ruleName + " --  search in  " + lists + ", against records indexed in the last " + resVal.wlDetail[k].timeSpan + "</option>";
                            }
                        }
                    }

                }
            }
        }

    }
    document.getElementById("ruleNmDD").innerHTML = frm;
}

function initDH(res) {
    resVal = res;

    if (res == undefined)
        return;
    if (trim(JSON.stringify(res)) == "")
        return;
    if (res.status == "SYSTEM_ERROR") {
        displayNotification("InternaldummyTblserver error", true, "error");
        return;
    }

    $("#formD").html("");
    if (res.attrList != undefined) {
        var json = res.attrList;

        var list = null;
        if (res.wlDetail != undefined)
            list = res.listDetail;

        var ruleList = null;
        if (res.wlDetail != undefined)
            ruleList = res.wlDetail;
        if (json.length > 0) {;
            var frm = "<form id='ocForm' ><fieldset style='border:0px;' >";
            frm += "<div class='ruleData'>";
            frm += "<table>";
            frm += "<table class='dummyTbl noBottomPadding'  id='ruleNameDetailsInfo'>";
            frm += "<tr>";
            frm += "<td>";
            frm += "<span> Lists</span>";
            frm += "</td>";
            frm += "<td>";
            frm += "<select id=\"ruleNameInfo\"  name=\"ruleList\" multiple onChange='displayRuleName()'>";
            if (list != null) {
                frm += "<option selected value='--All--' data-value='Any'  >Any</option>";
                for (var i = 0; i < list.length; i++) {
                    frm += "<option value='" + list[i].list + "'  data-value= '" + JSON.stringify(list[i]) + "' onChange='takeAction(" + JSON.stringify(ruleList[i]) + ", \"VIEW\");'>" + list[i].list + "</option>";
                }
            }
            frm += "<\/select>";
            frm += "</td>";
            frm += "</tr>";

            frm += "<tr>";
            frm += "<td ><span>Rule Name</span></td><td>";
            frm += "<select class='validate[required]' id='ruleNmDD' onChange='takeAction();'>";

            if (ruleList != null) {
                //frm += "<option value='--Select--'  >--Select--</option>";
                frm += "<option value=''>--Select--</option>";
                for (var i = 0; i < ruleList.length; i++) {

                    var lists = "";
                    for (var l = 0; l < ruleList[i].lists.length; l++) {
                        if (l != ruleList[i].lists.length - 1)
                            lists += ruleList[i].lists[l] + ", ";
                        else
                            lists += ruleList[i].lists[l];
                    }
                    if (ruleList[i].timeSpan != "" && ruleList[i].timeSpan != null) {
                        frm += "<option value='" + ruleList[i].ruleName + "'  data-value= '" + JSON.stringify(ruleList[i]) + "'>" + ruleList[i].ruleName + " -- " + ruleList[i].desc + " search in  " + lists + ", against records indexed in the last " + ruleList[i].timeSpan + "</option>";
                    } else {
                        frm += "<option value='" + ruleList[i].ruleName + "'  data-value= '" + JSON.stringify(ruleList[i]) + "'>" + ruleList[i].ruleName + " -- " + ruleList[i].desc + " search in  " + lists + "</option>";
                    }
                }
            }
            frm += "</select>";
            frm += "</td>";
            frm += "</tr>";
            frm += "</table>";
            frm += "<table class=\"dummyTbl no-margin noTopPadding\" id=\"ruleDetails\">";

            frm += "</table>";

            frm += "</div>";

            frm += "</td><td style='vertical-align:top;' >";

            frm += "<div class='wlRuleContainer'>";
            frm += "<div id='ruleDescHeading'>";

            frm += "</div>";
            frm += "<div id='ruleDescData'>";
            frm += "</div>";
            frm += "</div>";

            frm += "<div class='scoreContainer'>";
            frm += "<table id='resTbl' style='margin-top:3px; padding:10px; width:250px; text-align:left; display:none;' >";
            frm += "<tr><td style='padding:6px;' ><span>Max. Score :</span></td><td style='font-size:0.79em;' ><span id='msS' ></span></td>";
            frm += "<tr><td style='padding:6px;' ><span>Total :</span></td><td style='font-size:0.79em;' ><span id='tS' ></span></td>";
            frm += "</table>";
            frm += "</td></tr></table>";
            frm += "</div>";


            frm += "</fieldset></form>";
            frm += "<div style='margin-left:18px; margin-bottom:6px;' >";
            frm += "<a class='abutton submit-w-24' id = 'submitBtn'onClick=\"submitOnlineCustOB()\" >Submit</a>";
            $("#formD").html(frm);
            $("#ocForm").validationEngine('attach', { scroll: false });
            $(".dp").datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        }
    }
}
var ruleName;

function submitOnlineCustOB() {
    var isTrue = $("#ocForm").validationEngine('validate', { scroll: false });
    if (!isTrue)
        return;
    var aName = "";
    var aVal = "";
    var ifFormFilled = false;
    var ddObj = document.getElementById("ruleNmDD");
    ruleName = ddObj.options[ddObj.selectedIndex].value;
    var fJson = { "query": { "ruleName": ruleName } }
    var fields = []
    $(".dummyTbl").each(function(i, e) {
        $(this).find('tr').each(function() {
            aName = $(this).find('td:nth-child(1) span').text();
            aVal = $(this).find('td:nth-child(2) input').val();
            if (aName != "Rule Name") {
                if (aVal === "")
                    ifFormFilled = true;
                fields.push({
                    "name": aName,
                    "value": aVal
                })
            }
        });
    });
    fJson.query.fields = fields;
    console.log("fJson", fields);
    if (fields[1].value == "") {
        ifFormFilled = false;
    } else {
        ifFormFilled = true;
    }
    var spinDiv = document.getElementById('spinner');
    if (ifFormFilled) {
        spinDiv.className = 'spin';
        document.getElementById("submitBtn").className = 'disableBtn';
        $("#containerDiv").css("filter", "blur(1px)")
        getWlMatchD(fJson, matchedResultsH, true);
    } else {
        var toastMessageSettings = {
            text: 'Fill atleast one field..',
            sticky: true,
            position: 'top-center',
            type: 'warning',
            closeText: ''
        };
        $().toastmessage('showToast', toastMessageSettings);
    }
}

var oTable;
var rule = false;



function matchedResultsH(res) {
    rule = customRuleNames.includes(ruleName);
    console.log("rule  ", rule);
    if (res == undefined)
        return;
    if (trim(JSON.stringify(res)) == "")
        return;
    if (res.status == "SYSTEM_ERROR") {
        displayNotification("Internal server error", true, "error");
        return;
    }
    if (res.WlMatch.status.status == "NOT_LOADED") {
        //alert("rule not loaded")
        displayNotification("Rule not loaded yet,try after some time...!", false, "notice");
        return;
    }
    if (res.WlMatch == undefined)
        return;
    if (res.WlMatch.status != undefined && res.WlMatch.status.status != undefined && res.WlMatch.status.status == "error") {
        displayNotification("Internal server error", true, "error");
        return;
    }
    if (rule == true) {
        if (res.WlMatch.matchedResults != undefined) {
            var mRJson = res.WlMatch.matchedResults;
            var spinDiv = document.getElementById('spinner');
            spinDiv.className = '';
            document.getElementById("submitBtn").className = 'abutton submit-w-24';
            $("#containerDiv").css("filter", "");
            $("#resTbl").show();
            document.getElementById("msS").innerHTML = mRJson.maxScore.toString();
            document.getElementById("tS").innerHTML = mRJson.total.toString();
            var grpdt = new Array();
            if (mRJson.matchedRecords != undefined) {
                var json = mRJson.matchedRecords;
                var relation;
                var fields;
                if (json.length > 0) {
                    var i = 0;
                    for (var j = 0; j < json.length; j++) {
                        relation = JSON.parse(json[j].relations);
                        fields = json[j].fields;
                        if (relation.length == 0) {
                            grpdt[i] = new Array();
                            grpdt[i][0] = json[j].id;
                            if (fields.length > 0) {
                                for (var z = 0; z < fields.length; z++) {
                                    switch (fields[z].name) {
                                        case "name":
                                            grpdt[i][1] = fields[z].value;
                                            break;
                                        case "passport":
                                            grpdt[i][3] = fields[z].value;
                                            break;
                                        case "nic":
                                            grpdt[i][2] = fields[z].value;
                                            break;
                                    }
                                }
                            }
                            if (grpdt[i][1] == undefined) {
                                grpdt[i][1] = "NA";
                            }
                            if (grpdt[i][2] == undefined) {
                                grpdt[i][2] = "NA";
                            }
                            if (grpdt[i][3] == undefined) {
                                grpdt[i][3] = "NA";
                            }
                            grpdt[i][4] = "NA";
                            grpdt[i][5] = "NA";
                            grpdt[i][6] = "NA";
                            grpdt[i][7] = "NA";
                            grpdt[i][8] = "NA";
                            i++;
                        } else {
                            for (k = 0; k < relation.length; k++) {
                                grpdt[i] = new Array();
                                grpdt[i][0] = json[j].id;
                                if (fields.length > 0) {
                                    for (var z = 0; z < fields.length; z++) {
                                        switch (fields[z].name) {
                                            case "name":
                                                grpdt[i][1] = fields[z].value;
                                                break;
                                            case "passport":
                                                grpdt[i][3] = fields[z].value;
                                                break;
                                            case "nic":
                                                grpdt[i][2] = fields[z].value;
                                                break;
                                        }
                                    }
                                }
                                if (grpdt[i][1] == undefined) {
                                    grpdt[i][1] = "NA";
                                }
                                if (grpdt[i][2] == undefined) {
                                    grpdt[i][2] = "NA";
                                }
                                if (grpdt[i][3] == undefined) {
                                    grpdt[i][3] = "NA";
                                }
                                grpdt[i][4] = relation[k].customercif2;
                                grpdt[i][5] = relation[k].Customercif2Name;
                                grpdt[i][6] = relation[k].nic;
                                grpdt[i][7] = relation[k].passport;
                                grpdt[i][8] = relation[k].Relationship;
                                i++;
                            }
                        }
                    }
                }
            } else {
                displayNotification("Match not found", false, "notice");
            }
            oTable = $("#wlTable").dataTable({
                "aaData": grpdt,
                "aoColumns": [
                    { "sTitle": "Customer ID 1" },
                    { "sTitle": "Customer Name 1" },
                    { "sTitle": "NIC 1" },
                    { "sTitle": "Passport 1" },
                    { "sTitle": "Customer ID 2" },
                    { "sTitle": "Customer Name 2" },
                    { "sTitle": "NIC 2" },
                    { "sTitle": "Passport 2" },
                    { "sTitle": "Relationship" }
                ],
                "aaSorting": [
                    [1, "desc"]
                ],
                "bProcessing": true,
                "bDestroy": true
            });
        } else {
            $("#resTbl").hide();
        }
    } else {
        if (res.WlMatch.matchedResults != undefined) {
            var mRJson = res.WlMatch.matchedResults;
            var spinDiv = document.getElementById('spinner');
            spinDiv.className = '';
            document.getElementById("submitBtn").className = 'abutton submit-w-24';
            $("#containerDiv").css("filter", "");
            $("#resTbl").show();
            document.getElementById("msS").innerHTML = mRJson.maxScore.toString();
            document.getElementById("tS").innerHTML = mRJson.total.toString();
            var grpdt = new Array();
            if (mRJson.matchedRecords != undefined) {
                var json = mRJson.matchedRecords;
                var flds = "";
                if (json.length > 0) {
                    for (var j = 0; j < json.length; j++) {
                        grpdt[j] = new Array(8);
                        grpdt[j][0] = json[j].id;
                        //grpdt[j][1] = json[j].score;
                        grpdt[j][1] = ((((parseFloat(json[j].score)) * 100)).toFixed(1)).toString() + "%"; //((Math.round(parseFloat(dataList[j].score)*100)/100)*100).toString()+"%"
                        grpdt[j][2] = json[j].listName;
                        flds = JSON.stringify(json[j].fields).toString().replace(/'/g, "");
                        grpdt[j][3] = "<a class='search-w-16-wc abutton' title='Show Details' onClick='showDetails(this, " + flds + ", \"" + grpdt[j][0] + "\")' rel='#detailO' style='vertical-align:middle;' >";
                    }
                }
            } else {
                displayNotification("Match not found", false, "notice");
            }
            oTable = $("#wlTable").dataTable({
                "aaData": grpdt,
                "aoColumns": [
                    { "sTitle": "Record Id" },
                    { "sTitle": "Score" },
                    { "sTitle": "List Id" },
                    { "sTitle": "Details" }
                ],
                "aaSorting": [
                    [1, "desc"]
                ],
                "bProcessing": true,
                "bDestroy": true
            });
        } else {
            $("#resTbl").hide();
        }
    }
    var gCon = document.getElementById("canDiv");
    $("td a[rel]").overlay({ effect: 'apple' });
    var sH = gCon.scrollHeight;
    if (sH > 0) {
        gCon.scrollTop = gCon.scrollHeight;
    } else {
        gCon.scrollTop = gCon.offsetHeight;
    }
    $(".dataTableCss").css("width", "100%");
}

function showDetails(obj, res, id) {
    if (res != undefined) {
        var dataList = res;
        var grpdt = new Array();
        if (dataList.length > 0) {
            for (var j = 0; j < dataList.length; j++) {
                grpdt[j] = new Array(5);
                if (id != undefined && id != null)
                    grpdt[j][0] = id;
                else
                    grpdt[j][0] = "";
                if (dataList[j].score != undefined && dataList[j].score != null)
                //grpdt[j][1] = dataList[j].score.toString();
                    grpdt[j][1] = (((parseFloat(dataList[j].score) * 100)).toFixed(1)).toString() + "%"; //Math.round(num * 100) / 100
                else
                    grpdt[j][1] = "";
                if (dataList[j].name != undefined && dataList[j].name != null)
                    grpdt[j][2] = dataList[j].name.toString();
                else
                    grpdt[j][2] = "";
                if (dataList[j].value != undefined && dataList[j].value != null)
                    grpdt[j][3] = dataList[j].value.toString();
                else
                    grpdt[j][3] = "";
                if (dataList[j].queryValue != undefined && dataList[j].queryValue != null)
                    grpdt[j][4] = dataList[j].queryValue.toString();
                else
                    grpdt[j][4] = "";
            }
        }
        var oTable = $("#detTable").dataTable({
            "aaData": grpdt,
            "aoColumns": [
                { "sTitle": "Record Id" },
                { "sTitle": "Score" },
                { "sTitle": "Key" },
                { "sTitle": "Matched Value" },
                { "sTitle": "Searched Value" }
            ],
            "bProcessing": true,
            "bDestroy": true
        });
    }
    $(obj).overlay({ effect: 'apple' });
    $("#det1_2").hide();
    $(".dataTableCss").css("width", "100%");

    var recLink = "<div id='det1_rec_link'>";
    showEntityDetails(id);
    $("#recLink").html(recLink);
}

function showEntityDetails(id) {
    var inputJson = {};
    inputJson.id = id + "";
    getSuspectD(inputJson, populateSuspectDetails, true);
}

function populateSuspectDetails(res) {
    $("#det1_2").show();
    var data = res.wlSuspectDetails;
    var tb = "<table style='width:300px' class='dataTableCss'>";
    tb += "<tr style='background-color: #454545; color: #FFFFFF;'><th style='width:50%;'> Attribute </th> <th style='width:50%;'> Value </th></tr>";
    for (key in data) {
        tb += " <tr style='background-color: #D3D6FF;' ><td style='width:50%;'>";
        tb += key;
        tb += "</td><td style='width:50%; background-color: #EAEBFF'>";
        tb += data[key];
        tb += "</td></tr>";
    }
    tb += "</table>";
    $("#susTable").html(tb);
}

function weightPer(match) {
    var allWeightVal = [];
    var sum = 0;
    for (var i = 0; i < match.length; i++) {
        allWeightVal.push(match[i].wt);
        sum = sum + parseInt(match[i].wt);
    }
    return sum;
}