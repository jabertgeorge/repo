package clari5.custom.wlalert;

import cxps.apex.utils.CxpsLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip {
    public static CxpsLogger logger = CxpsLogger.getLogger(UnZip.class);

    public void unZipIt(String zipFile, String unzipDir)throws IOException {
        byte[] buffer = new byte[1024];

            File folder = new File(unzipDir);
            if (!folder.exists()) {
                folder.mkdir();
            }


            ZipInputStream zip = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry ze = zip.getNextEntry();
            while (ze != null) {

                String fileName = ze.getName();

                File newFile = new File(unzipDir + File.separator + fileName);
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zip.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                ze = zip.getNextEntry();
            }
            zip.closeEntry();
            zip.close();

            logger.info("Unzipping of file done ");

    }
}
