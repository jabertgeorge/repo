package clari5.custom.boc.integration.data;

public class FT_GoldTxn extends ITableData {

    private String tableName = "FT_GOLD_TXN";
    private String event_type = "FT_GoldTxn";

    private String event_name;
    private String eventtype;
    private String cx_cust_id;
    private String cx_acct_id;
    private String eventsubtype;
    private String host_user_id;
    private String nic;
    private String host_id;
    private String channel;
    private String keys;
    private String cust_id;
    private String tran_id;
    private String tran_date;
    private String tran_amt;
    private String tran_curr;
    private String lcy_tran_amt;
    private String lcy_tran_curr;
    private String tran_code;
    private String tran_rmks;
    private String part_tran_type;
    private String zone_date;
    private String zone_code;
    private String part_tran_srl_num;
    private String tran_type;
    private String tran_sub_type;
    private String sys_time;
    private String eventts;
    private String source;
    private String systemcountry;
    private String reservedfield1;
    private String reservedfield2;
    private String reservedfield3;
    private String reservedfield4;
    private String reservedfield5;
    private String event_id;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getCx_cust_id() {
        return cx_cust_id;
    }

    public void setCx_cust_id(String cx_cust_id) {
        this.cx_cust_id = cx_cust_id;
    }

    public String getCx_acct_id() {
        return cx_acct_id;
    }

    public void setCx_acct_id(String cx_acct_id) {
        this.cx_acct_id = cx_acct_id;
    }

    public String getEventsubtype() {
        return eventsubtype;
    }

    public void setEventsubtype(String eventsubtype) {
        this.eventsubtype = eventsubtype;
    }

    public String getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(String host_user_id) {
        this.host_user_id = host_user_id;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public void setTran_amt(String tran_amt) {
        this.tran_amt = tran_amt;
    }

    public String getTran_curr() {
        return tran_curr;
    }

    public void setTran_curr(String tran_curr) {
        this.tran_curr = tran_curr;
    }

    public String getLcy_tran_amt() {
        return lcy_tran_amt;
    }

    public void setLcy_tran_amt(String lcy_tran_amt) {
        this.lcy_tran_amt = lcy_tran_amt;
    }

    public String getLcy_tran_curr() {
        return lcy_tran_curr;
    }

    public void setLcy_tran_curr(String lcy_tran_curr) {
        this.lcy_tran_curr = lcy_tran_curr;
    }

    public String getTran_code() {
        return tran_code;
    }

    public void setTran_code(String tran_code) {
        this.tran_code = tran_code;
    }

    public String getTran_rmks() {
        return tran_rmks;
    }

    public void setTran_rmks(String tran_rmks) {
        this.tran_rmks = tran_rmks;
    }

    public String getPart_tran_type() {
        return part_tran_type;
    }

    public void setPart_tran_type(String part_tran_type) {
        this.part_tran_type = part_tran_type;
    }

    public String getZone_date() {
        return zone_date;
    }

    public void setZone_date(String zone_date) {
        this.zone_date = zone_date;
    }

    public String getZone_code() {
        return zone_code;
    }

    public void setZone_code(String zone_code) {
        this.zone_code = zone_code;
    }

    public String getPart_tran_srl_num() {
        return part_tran_srl_num;
    }

    public void setPart_tran_srl_num(String part_tran_srl_num) {
        this.part_tran_srl_num = part_tran_srl_num;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_sub_type() {
        return tran_sub_type;
    }

    public void setTran_sub_type(String tran_sub_type) {
        this.tran_sub_type = tran_sub_type;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getReservedfield1() {
        return reservedfield1;
    }

    public void setReservedfield1(String reservedfield1) {
        this.reservedfield1 = reservedfield1;
    }

    public String getReservedfield2() {
        return reservedfield2;
    }

    public void setReservedfield2(String reservedfield2) {
        this.reservedfield2 = reservedfield2;
    }

    public String getReservedfield3() {
        return reservedfield3;
    }

    public void setReservedfield3(String reservedfield3) {
        this.reservedfield3 = reservedfield3;
    }

    public String getReservedfield4() {
        return reservedfield4;
    }

    public void setReservedfield4(String reservedfield4) {
        this.reservedfield4 = reservedfield4;
    }

    public String getReservedfield5() {
        return reservedfield5;
    }

    public void setReservedfield5(String reservedfield5) {
        this.reservedfield5 = reservedfield5;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
