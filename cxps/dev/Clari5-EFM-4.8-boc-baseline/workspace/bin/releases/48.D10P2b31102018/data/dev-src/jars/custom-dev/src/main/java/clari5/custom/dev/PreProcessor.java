package clari5.custom.dev;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import cxps.customEventHelper.CustomWsKeyDerivator;
import cxps.events.FT_RemittanceEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PreProcessor {

    static CxpsLogger logger = CxpsLogger.getLogger(PreProcessor.class);

    public static CxConnection getConnection() {
        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("Connection object is NULL");
        return rdbms.getConnection();
    }

    public static String getCustIdForRemmittanceEvent(FT_RemittanceEvent event) {

        /**
         * Removing trailing zero's from REM/BEN account  number
         */

        while (true) {
            if (event.getRemType().equalsIgnoreCase("I") && event.getBenAcctNo().startsWith("0"))
                event.setBenAcctNo(event.getBenAcctNo().substring(1));

            else if (event.getRemType().equalsIgnoreCase("O") && event.getRemAcctNo().startsWith("0"))
                event.setRemAcctNo(event.getBenAcctNo().substring(1));

            else break;
        }

        String custId = null;
        CxConnection connection = getConnection();

        try {
            if (event.system.equalsIgnoreCase("SR")) {
                String acctId = CustomWsKeyDerivator.accountKeyForRemmitance(event.getRemType(), event.getRemAcctNo(), event.getBenAcctNo());
                CxKeyHelper h = Hfdb.getCxKeyHelper();
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, event.getHostId(), acctId);
                event.setCxAcctId(accountKey);
                event.setFcyTranAmt(event.getTranAmt());
                event.setTranAmt(event.getLcyAmt());
                event.setFcyTranCur(event.getTranCurr());
                event.setTranCurr(event.getLcyCurr());
                event.setEventts(event.getTxnDate());
                event.setTranId("SR" + System.nanoTime());
                PreparedStatement ps = null;
                ResultSet rs = null;
                String sql = "select acctCustID from dda where acctID = '" + accountKey + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    custId = rs.getString("acctCustID");
                }
                event.setCustId(custId.substring(4));
                logger.debug("data in cx-cust-id is -------------------> " + custId + " cx-acct-id---------------> " + event.getCxAcctId());
                return custId != null ? custId : "";
            } else {
                String acctId = CustomWsKeyDerivator.accountKeyForRemmitance(event.getRemType(), event.getRemAcctNo(), event.getBenAcctNo());
                CxKeyHelper h = Hfdb.getCxKeyHelper();
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, event.getHostId(), acctId);
                event.setCxAcctId(accountKey);
                PreparedStatement ps = null;
                ResultSet rs = null;
                String sql = "select acctCustID from dda where acctID = '" + accountKey + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    custId = rs.getString("acctCustID");
                }
                event.setCustId(custId.substring(4));
                logger.debug("data in cx-cust-id inside else is -------------------> " + custId + " cx-acct-id inside else is ---------------> " + event.getCxAcctId());
                return custId != null ? custId : "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("CustId is null...");
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return custId != null ? custId : "";
    }
}
