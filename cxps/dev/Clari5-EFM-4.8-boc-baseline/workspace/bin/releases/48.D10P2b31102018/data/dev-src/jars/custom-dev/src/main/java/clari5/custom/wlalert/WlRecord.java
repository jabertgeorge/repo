package clari5.custom.wlalert;

import clari5.platform.util.CxJson;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.time.LocalDate;
import java.util.regex.Pattern;

public class WlRecord {
    String custid ;
    String listname;
    String inputentity;
    String matchedresult;
    Double matchedpercentage;
    String rulename;


    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getListname() {
        return listname;
    }

    public void setListname(String listname) {
        this.listname = listname;
    }
    public String getInputentity() {
        return inputentity;
    }

    public void setInputentity(String inputentity) {
        this.inputentity = inputentity;
    }

    public String getMatchedresult() {
        return matchedresult;
    }

    public void setMatchedresult(String matchedresult) {
        this.matchedresult = matchedresult;
    }

    public Double getMatchedpercentage() {
        return matchedpercentage;
    }

    public void setMatchedpercentage(Double matchedpercentage) {
        this.matchedpercentage = matchedpercentage;
    }
    public String getRulename() {
        return rulename;
    }

    public void setRulename(String rulename) {
        this.rulename = rulename;
    }




    public CxJson toJson()throws IOException{
        String event_id = "wlalert"+System.currentTimeMillis();
        String msgBody = toMsgJson();
        StringBuilder sb = new StringBuilder("{");
        sb.append("\"event_id\":\""+event_id+"\",");
        sb.append("\"eventsubtype\":\"wlalert\",");
        sb.append("\"eventtype\":\"nft\",");
        sb.append("\"event-name\":\"nft_wlalert\",");
        sb.append("\"msgBody\":\""+msgBody+"\"");
        sb.append("}");

        System.out.println("string builder " +sb);

        return  CxJson.parse(sb.toString());
    }
    public String toMsgJson(){
       // SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
       // Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        //String sys_time= sdf.format(timestamp).toString();
        String sys_time= new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Date());
        StringBuilder sb = new StringBuilder("{");
        sb.append("\'sys_time\': ");
        sb.append((sys_time == null) ? "\'\'," : "\'"+sys_time+"\',");
        sb.append("\'host_id\':\'F\',"  );
        sb.append("\'rulename\': ");
        sb.append((getRulename() == null) ? "\'\'," : "\'"+getRulename()+"\',");
        sb.append("\'input_entity\': ");
        sb.append((getInputentity() == null) ? "\'\'," : "\'"+getInputentity()+"\',");
        sb.append("\'matched_result\': ");
        sb.append((getMatchedresult() == null) ? "\'\'," : "\'"+getMatchedresult()+"\',");
        sb.append("\'matched_percentage\': ");
        sb.append((getMatchedpercentage() == null) ? "\'\'," : "\'"+getMatchedpercentage()+"\',");
        sb.append("\'list_name\': ");
        sb.append((getListname() == null) ? "\'\'," : "\'"+getListname()+"\',");
        sb.append("\'cust_id\': ");
        sb.append((getCustid() == null) ? "\'\'," : "\'"+getCustid()+"\',");

        sb.append("}");
        return sb.toString().replaceAll(Pattern.quote(",}"), "}").replaceAll(Pattern.quote(",]"), "]"); }

    @Override
    public String toString() {
        return "WlRecord{" +
                "custid='" + custid + '\'' +
                ", listname='" + listname + '\'' +
                ", inputentity='" + inputentity + '\'' +
                ", matchedresult='" + matchedresult + '\'' +
                ", matchedpercentage='" + matchedpercentage + '\'' +
                ", rulename='" + rulename + '\'' +
                '}';
    }
}
