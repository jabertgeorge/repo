package clari5.custom.wlalert.db;

import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cxps.apex.utils.CxpsLogger;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WlRequestPayload {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WlRequestPayload.class);
    WlrecordDetails wlrecordDetails;
    static Matches matches = new Matches();
    final static List<String> rulesList = Arrays.asList(new String[]{"NAME_MATCH_CUSTOMER", "PASSPORT_MATCH_CUSTOMER", "NIC_MATCH_CUSTOMER"});

    public Queue<CxJson> setWlReqValues(WlrecordDetails wlrecordDetails) {
        this.wlrecordDetails = wlrecordDetails;

        Queue<CxJson> reqList = new ConcurrentLinkedQueue<>();


        for (String ruleName : rulesList) {
            reqList.add(createWlRes(ruleName));
        }

        logger.info("WL Request Record " + reqList.toString());
        return reqList;
    }


    private List<String> getRulesFieldsList(String ruleName) {
        List<String> fields;
        String[] field = new String[0];
        switch (ruleName) {

            case "NAME_MATCH_CUSTOMER":
                field = new String[]{"name"};
                break;
            case "PASSPORT_MATCH_CUSTOMER":
                field = new String[]{"passport"};
                break;
            case "NIC_MATCH_CUSTOMER":
                field = new String[]{"nic"};
                break;
        }

        return Arrays.asList(field);
    }

    private CxJson createWlRes(String ruleName) {
        CxJson cxJson = null;
        List<String> field = getRulesFieldsList(ruleName);
        for (String fields:field) {
            if (fields.equalsIgnoreCase(wlrecordDetails.getParameterType())) {
                StringBuilder buildReq = new StringBuilder("{\"ruleName\":\"" + ruleName + "\",");
                buildReq.append("\"fields\":[{\"name\":\" " + wlrecordDetails.getParameterValue() + "\",\"value\" :\"" + wlrecordDetails.getParameterValue() + "\"}]}");
            try {
                cxJson = CxJson.parse(buildReq.toString());
            } catch (IOException io) {
                logger.error(" [WlRequestPayload.setWlReqValues ] Failed to parse json " + buildReq.toString() + "\n Message " + io.getMessage());
            }
            }
        }
        return cxJson;
    }


    public Matches sendBatchWlReq(Queue queue) {


        List<String> responseList = new ArrayList<>();
        while (!queue.isEmpty()) {
            sendWlRequest((CxJson) queue.poll(), responseList);
        }

        matches.setMatchResult(responseList);
        return matches;
    }

    public void sendWlRequest(CxJson obj, List<String> responseList) {

        logger.info("Request URL "+System.getenv("LOCAL_DN") + "/efm/wlsearch?q="+obj.toString());
        String response = "";

        try {
            String url = System.getenv("LOCAL_DN") + "/efm/wlsearch?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
            String instanceId = System.getenv("INSTANCEID");
            String appSecret = System.getenv("APPSECRET");
            HttpResponse resp;
            //commented as CxRest.get is removed
            //resp = CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").basicAuth(instanceId, appSecret).asString();
            resp = Unirest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").basicAuth(instanceId, appSecret).asString();
            response = (String) resp.getBody();
            logger.info("WL Respose data " + response);
            responseList.add(response);

        } catch (UnirestException e) {
            e.printStackTrace();

        } catch (Exception e) {
            logger.error("Error While getting the matched data from WL" + e.getMessage() + "\n Cause " + e.getCause());
        }


    }


   /* public Double getMaxScore(CxJson cxJson) {
        return Double.parseDouble(cxJson.get("matchedResults").getString("maxScore"));
    }*/

}
