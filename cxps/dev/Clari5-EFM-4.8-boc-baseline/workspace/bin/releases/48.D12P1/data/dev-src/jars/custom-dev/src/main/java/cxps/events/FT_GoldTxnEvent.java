// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_GOLDTXN", Schema="rice")
public class FT_GoldTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp zoneDate;
       @Field(size=20) public String zoneCode;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String keys;
       @Field(size=20) public String channel;
       @Field(size=20) public String nic;
       @Field public java.sql.Timestamp eventTime;
       @Field(size=20) public String tranSubType;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String cxAcctId;
       @Field(size=20) public String tranId;
       @Field(size=11) public Double txnAmt;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String txnId;
       @Field(size=20) public String txnCode;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String eventSubtype;
       @Field(size=11) public Double refAmt;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=20) public String txnCurr;
       @Field(size=20) public String partTranSrlNum;
       @Field(size=20) public String custId;
       @Field(size=11) public Double refCurncy;
       @Field(size=20) public String tranSrlNo;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String tranType;
       @Field(size=20) public String remarks;
       @Field(size=20) public String systemCountry;


    @JsonIgnore
    public ITable<FT_GoldTxnEvent> t = AEF.getITable(this);

	public FT_GoldTxnEvent(){}

    public FT_GoldTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("GoldTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setZoneDate(EventHelper.toTimestamp(json.getString("zone_date")));
            setZoneCode(json.getString("zone_code"));
            setMsgSource(json.getString("source"));
            setKeys(json.getString("keys"));
            setChannel(json.getString("channel"));
            setNic(json.getString("nic"));
            setEventTime(EventHelper.toTimestamp(json.getString("eventts")));
            setTranSubType(json.getString("tran_sub_type"));
            setSystemTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setTxnDrCr(json.getString("part_tran_type"));
            setCxAcctId(json.getString("cx-acct-id"));
            setTranId(json.getString("tran_id"));
            setTxnAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setAddEntityId2(json.getString("reservedfield2"));
            setTxnId(json.getString("tran_id"));
            setTxnCode(json.getString("tran_code"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setAddEntityId3(json.getString("reservedfield3"));
            setEventSubtype(json.getString("eventsubtype"));
            setRefAmt(EventHelper.toDouble(json.getString("LCY_TRAN_AMT")));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setTxnCurr(json.getString("tran_curr"));
            setPartTranSrlNum(json.getString("part_tran_srl_num"));
            setCustId(json.getString("cust_id"));
            setRefCurncy(EventHelper.toDouble(json.getString("LCY_TRAN_CURR")));
            setTranSrlNo(json.getString("part_tran_srl_num"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setTranType(json.getString("tran_type"));
            setRemarks(json.getString("tran_rmks"));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FG"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getZoneDate(){ return zoneDate; }

    public String getZoneCode(){ return zoneCode; }

    public String getMsgSource(){ return msgSource; }

    public String getKeys(){ return keys; }

    public String getChannel(){ return channel; }

    public String getNic(){ return nic; }

    public java.sql.Timestamp getEventTime(){ return eventTime; }

    public String getTranSubType(){ return tranSubType; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getCxAcctId(){ return cxAcctId; }

    public String getTranId(){ return tranId; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getTxnId(){ return txnId; }

    public String getTxnCode(){ return txnCode; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventSubtype(){ return eventSubtype; }

    public Double getRefAmt(){ return refAmt; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getTxnCurr(){ return txnCurr; }

    public String getPartTranSrlNum(){ return partTranSrlNum; }

    public String getCustId(){ return custId; }

    public Double getRefCurncy(){ return refCurncy; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getTranType(){ return tranType; }

    public String getRemarks(){ return remarks; }

    public String getSystemCountry(){ return systemCountry; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setZoneDate(java.sql.Timestamp val){ this.zoneDate = val; }
    public void setZoneCode(String val){ this.zoneCode = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setNic(String val){ this.nic = val; }
    public void setEventTime(java.sql.Timestamp val){ this.eventTime = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setTxnId(String val){ this.txnId = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setRefAmt(Double val){ this.refAmt = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setTxnCurr(String val){ this.txnCurr = val; }
    public void setPartTranSrlNum(String val){ this.partTranSrlNum = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setRefCurncy(Double val){ this.refCurncy = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setRemarks(String val){ this.remarks = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_GoldTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.nic);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_GoldTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "GoldTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}