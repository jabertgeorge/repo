clari5.hfdb.entity.nft-uscust {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : SYNC_STATUS , type:"string:100"}

{ name = event_name, type = "string:100" }
{ name = eventtype, type = "string:100" }
{ name = eventsubtype, type = "string:100" }
{ name = host_user_id, type = "string:20" }
{ name = host_id, type = "string:20" }
{ name = channel, type = "string:20" }
{ name = keys, type = "string:20  " }
{ name = source, type = "string:20 " }
{ name = sys_time, type = "date" }
{ name = cust_id, type = "string:20" }
{ name = cx_cust_id, type = "string:20" }
{ name = cx_acct_id, type = "string:20" }
{ name = systemcountry, type = "string:20" }
{ name = custcreationdate, type = "date" }
{ name = permanent_addr, type = "string:20" }
{ name = mailing_addr, type = "string:100" }
{ name = tel_phn, type = "string:20" }
{ name = citenzenship, type = "string:20" }
{ name = usnational_fatca, type = "string:20" }
{ name = reservedfield1, type = "string:20" }
{ name = reservedfield2, type = "string:20" }
{ name = reservedfield3, type = "string:20" }
{ name = reservedfield4, type = "string:100" }
{ name = reservedfield5, type = "string:100" }

   ]
}
