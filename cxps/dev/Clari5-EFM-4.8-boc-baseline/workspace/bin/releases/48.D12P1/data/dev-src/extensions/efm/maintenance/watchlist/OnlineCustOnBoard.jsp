<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!DOCTYPE html>
    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <style>
            .spin {
                position: absolute;
                left: 50%;
                top: 54%;
                z-index: 1;
                width: 150px;
                height: 150px;
                margin: -75px 0 0 -75px;
                border: 16px solid #f3f3f3;
                border-radius: 50%;
                border-top: 16px solid #3498db;
                width: 80px;
                height: 80px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
            }
            
            .disableBtn {
                pointer-events: none;
                cursor: default;
                color: grey;
            }
            
            @-webkit-keyframes spin {
                0% {
                    -webkit-transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(360deg);
                }
            }
            
            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }
            
            @-webkit-keyframes animatebottom {
                from {
                    bottom: -100px;
                    opacity: 0
                }
                to {
                    bottom: 0px;
                    opacity: 1
                }
            }
            
            @keyframes animatebottom {
                from {
                    bottom: -100px;
                    opacity: 0
                }
                to {
                    bottom: 0;
                    opacity: 1
                }
            }
            
            #ruleDesc {
                float: left;
                width: 48%;
            }
            
            .wlRuleContainer {
                float: left;
                width: 60%;
                position: relative;
                margin-top: 9.5%;
            }
            
            .dummyTbl {
                margin-top: 3px;
                padding: 10px;
            }
            
            .dummyTbl span {
                font-size: 0.79em;
                text-transform: capitalize;
            }
            
            .customWidth td:nth-child(1),
            .dummyTbl td:nth-child(1) {
                width: 23%;
            }
            
            #wlRuleTbl tr:first-child {
                background-color: #dedede;
            }
            
            .no-margin {
                margin: 0px;
            }
            
            .noTopPadding {
                padding-top: 0px;
            }
            
            .noBottomPadding {
                padding-bottom: 0px;
            }
            
            .ruleData {
                float: left;
                width: 50%;
                /* min-height:55%; */
            }
            
            .wlRuleContainer {
                float: right;
                width: 50%;
                position: relative;
                margin-top: 1.5%;
            }
            
            .wlRuleContainer div span:first-child {
                width: 30%;
                float: left;
                text-transform: capitalize;
                font-weight: bold;
            }
            
            .wlRuleContainer p {
                border: 1px solid #888888;
                margin: 0;
                padding: 3px 3px 0;
            }
            
            .scoreContainer {
                display: block;
                float: left;
                width: 100%;
            }
            
            #ruleNameInfo {
                min-height: 75px;
            }
            
            option:checked {
                margin: 0 !Important;
            }
            
            .dummyTbl {
                width: 100%;
            }
        </style>

        <script type="text/javascript" src="/cdn/efm/common/js/lib/loadCDN.js"></script>
        <script src="/efm/JavaScriptServlet"></script>
        <script>
            var redirectUrl = window.location.origin + "/efm?returl=" + encodeURIComponent(window.location.href);
            if (window.parent.location.pathname !== "/efm") {
                window.location.href = redirectUrl;
            }
            // Add any required headers here and they will be added to every request.
            /* Not sure if needed in iif or not
        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader('mode', 'REST');
            }
        });
        // Error handling for any request will go through this listener.
        $(document).ajaxError(function (jqXHR) {
            if (jqXHR.statusCode === 401)
                window.location.href = redirectUrl;
        });
		*/

            loadCSS([
                "ext/css/bootstrap/bootstrap.css",
                "ext/css/jquery/validation-engine/validationEngine.jquery.css",
                "ext/css/jquery/demo_table.css",
                "ext/css/jquery/jquery-ui.css",
                "ext/css/jquery/overlay-apple.css",
                "ext/css/jquery/jquery.toastmessage.css"
            ]);
        </script>

        <link rel="stylesheet" href="/cdn/efm/common/css/style.css" type="text/css" />
        <link rel="stylesheet" href="/cdn/efm/maintenance/watchlist/css/style.css" type="text/css" />
	<script type="text/javascript" src="js/onlineCustOnBoardPage.js"></script>
        <script>
            var jqFile = ["ext/jquery/jquery-3.4.1.min.js"];
            loadFromCDN(jqFile, load);

            var jsFiles = [
                "ext/jquery/tools-full/jquery.tools.min.js",
                "ext/jquery/jquery-ui.js",
                "ext/jquery/jquery.dataTables.min.js",
                "ext/jquery/jquery.toastmessage-min.js",
                "ext/jquery/validation-engine/jquery.validationEngine-en.js",
                "ext/jquery/validation-engine/jquery.validationEngine.js",
                "efm/common/js/lib/cxNetwork.js",
                "efm/maintenance/watchlist/js/olcobService.js",
                "efm/common/js/lib/commons.js",
                "efm/common/js/lib/utils.js"
            ];

            function load() {
                loadFromCDN(jsFiles, null);
            }
            /*Load local js files*/
            loadFromCDN(jqFile, loadLocalFiles);

            var localJsFiles = [];

            function loadLocalFiles() {
                addLocalJs(localJsFiles, null);
            }

            var jqFileTools = ["ext/jquery/tools-full/jquery.tools.min.js"];
            loadFromCDN(jqFileTools, load);
        </script>

    </head>

    <body onload="init()">
        <div id="spinner"></div>
        <div class="container" id="containerDiv">
            <div id="canDiv" style="margin:3px 5px; overflow:auto; height:100%; margin-top:10px;">
                <div id="wlDB" style="width:96%; margin:auto;">
                    <div id="wldbDiv">
                        <div style="margin:3px 0 35px 0; border:1px solid #808080; border-radius:0 10px 7px 7px; width:100%; margin:auto;">
                            <div class="heading">
                                <h3><span style='font-size:1em;'>Important * </span><span style='font-size:0.7em;'> (Atleast one field is required.)</span></h3>
                            </div>
                            <div id="formD" style="margin:auto;"></div>
                        </div>

                        <div style="margin:auto; margin-top:25px; border:1px solid #808080; border-radius:0 10px 7px 7px; width:100%;">
                            <div class="heading">
                                <h3>Results </h3>
                            </div>
                            <div id="customui" style="width:100%; margin:auto; margin-top:10px; margin-bottom:25px;">
                                <!--<table id="wlTable" class="dataTableCss" style="text-align:left;" ></table>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="apple_overlay black" id="detailO" style="height:400px;">
                <div id="det1" style="height:360px; overflow:auto;">
                    <div id="det1_1" style="border:1px solid #808080; border-radius:0 10px 7px 7px; width:98%; margin:auto; margin-top:10px;">
                        <div class="heading">
                            <h3>Details </h3>
                        </div>
                        <!--<div style="width:100%; margin:auto; margin-top:10px; margin-bottom:5px; height:180px;" >-->
                        <div style="width:100%; margin:auto; margin-top:10px;padding-bottom: 20px; ">
                            <table id="detTable" class="dataTableCss" style="text-align:left;"></table>
                        </div>
                    </div>
                    <div id="recLink"></div>
                    <div id="det1_2" style="border:1px solid #808080; border-radius:0 10px 7px 7px; width:98%; margin:auto; margin-top:10px;display:none;">
                        <div class="heading">
                            <h3>Record details ( as per List)</h3>
                        </div>
                        <div style="width:100%; margin:auto; margin-top:10px; margin-bottom:5px; height:180px;">
                            <table id="susTable" class="dataTableCss" style="text-align:left;"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>
