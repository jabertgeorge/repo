package clari5.custom.dowjones;

import clari5.aml.web.wl.search.WlQuery;
import clari5.aml.web.wl.search.WlRecordDetails;
import clari5.aml.web.wl.search.WlResult;
import clari5.aml.web.wl.search.WlSearcher;
import clari5.aml.wle.MatchedRecord;
import clari5.aml.wle.MatchedResults;
import clari5.custom.boc.integration.BatchProcessor;
import clari5.platform.util.CxJson;
import clari5.platform.util.ECClient;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TradeFinance {

    public static CxpsLogger logger = CxpsLogger.getLogger(TradeFinance.class);
    //static String reqUrl = System.getenv("LOCAL_DN") + "/mq/rest/cxq/load/put?";
    //static List<String> fieldListCustomer = new ArrayList<>();
    static int count = 1;
    /*static {
        fieldListCustomer.addAll(Arrays.asList(SelectUpdate.query.getString("dowjones.fieldListCustomer").split(",")));
    }*/

    public static String watchListApi() {
        return null;
    }

    public  Map<String, String> callApi(Map<String, String> dataMap) {
        //dataMap.putAll(getCustomerData(connection, dataMap.get(SelectUpdate.query.getString("dowjones.getCustId"))));
        Map<String, String> responseMap = new LinkedHashMap<>();
        try {
            for (String removeFields : SelectUpdate.query.getString("dowjones.removeFieldsFromMap").split(",")) {
                responseMap.put(removeFields, dataMap.get(removeFields));
                dataMap.remove(removeFields);
            }
            logger.info("data inside datamap + " + dataMap);
            List<String> ruleList = new ArrayList<>(Arrays.asList(SelectUpdate.query.getString("dowjones.ruleName").split(",")));

            if (ruleList.size() > 0 && !ruleList.isEmpty()) {
                for (int i = 0; i < ruleList.size(); i++) {
                    JSONObject obj = new JSONObject();
                    obj.put("ruleName", ruleList.get(i));
                    for (Map.Entry<String, String> entry : dataMap.entrySet()) {
                        JSONArray jsonArray = new JSONArray();
                        JSONObject obj1 = new JSONObject();
                        obj1.put(SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".nameValue"), SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".nameDJ"));
                        obj1.put(SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".value"), dataMap.get(entry.getKey()));
                        jsonArray.put(obj1);
                        JSONObject obj2 = new JSONObject();
                        obj2.put(SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".entityName"), SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".entityDJ"));
                        obj2.put(SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".entityValue"), SelectUpdate.query.getString("dowjones.confList." + entry.getKey() + ".entityDJValue"));
                        jsonArray.put(obj2);
                        obj.put(SelectUpdate.query.getString("dowjones.fields"), jsonArray);
                        logger.info("object of dowjones: " + obj);
                        String url = System.getenv("SECURE_LOCAL_DN") + SelectUpdate.query.getString("dowjones.url");
                        url = url + "?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
                        logger.info("dowjones url: " + url);
                        String response = getHttpResponse(obj);
                        logger.info("response: " + response);
                        JSONObject jsonObject = new JSONObject(response);
                        jsonObject.put("ruleName", obj.getString("ruleName").replaceAll("\"", String.valueOf('"')));
                        jsonObject.put("cust_id", dataMap.get("cust_id"));
                        responseMap.put(entry.getKey(), jsonObject.toString());
                        logger.info("response map entry: " + responseMap);
                        //System.out.println(responseMap.get(entry.getKey()));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*System.out.println("Response Map data  inside map: -->" + responseMap);*/
        return responseMap;
    }

    public static String getHttpResponse(JSONObject obj) {
        /*System.out.println("URL with message string is --> " + url);
        HttpResponse resp = null;
        String response = "";
        String instanceId = System.getenv("INSTANCEID");
        String appSecret = System.getenv("APPSECRET");
        try {
            resp = Unirest.get(url).header("accept", "application/json")
                    .header("Content-Type", "application/json")
                    .basicAuth(instanceId, appSecret).asString();
            response = (String) resp.getBody();
            //Thread.sleep(1000);
        } catch (UnirestException e) {
            e.printStackTrace();
            //System.out.println("WL_ONBOARDING_CUST : WatchList not reachable" + e);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("WL_ONBOARDING_CUST : Exception in parsing" + e);
        }
        return response;*/

        String response = "";
        try {
            WlQuery query = new WlQuery();
            query.from(obj.toString());

            logger.info("Query object created from json : "+ obj.toString());

            WlSearcher searcher = new WlSearcher();
            WlResult result = searcher.search(query);
            MatchedResults matched = result.getMatchedResults();
            Collection<MatchedRecord> recordList = matched.getMatchedRecords();
            for(MatchedRecord record : recordList) {
                WlRecordDetails wlDetails = new WlRecordDetails() ;
                String outputDetails = wlDetails.getRecordDetails(record.getDocId());

                record.setDetails(outputDetails);

            }

            response = result.toJson();
            logger.info("Response for online DMS fetched: " + response);
        } catch (Exception e) {
            logger.info("unable to fetch response from WL for URL: ---> " + obj.toString());
            e.getCause();
            e.printStackTrace();
        }
        return response;
    }

    /*public static Map<String, String> getCustomerData(CxConnection connection, String custId) {
        custId = "C_F_" + custId;
        System.out.println("custId inside get customer data: " + custId);
        Map<String, String> customerData = new HashMap<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(SelectUpdate.query.getString("dowjones.selectCustomer"));
            int i = 1;
            ps.setString(i++, custId);
            rs = ps.executeQuery();
            while (rs.next()) {
                for (String listCustomer : fieldListCustomer) {
                    customerData.put(listCustomer, rs.getString(listCustomer) != null ? rs.getString(listCustomer) : "");
                }
            }
            return customerData;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerData;
    }*/

    public static CxJson evidenceFormatter(Map<String, String> responseMap) {

        try {
            responseMap.forEach((k, v) ->
                    {
                        if (v.startsWith("{")) {
                            JSONObject jsonObject = new JSONObject(v);
                            if (jsonObject.getJSONObject("matchedResults").has("matchedRecords")) {
                                logger.info("JSON data for watchlist response is: "+jsonObject);
                                CxJson eventJson = createEvent(createJsonFromEvent(jsonObject));
                                logger.info("event Json: " + eventJson);
                                //comented as the enqueue method became default.
                                //boolean resp = CxRest.equeue(reqUrl, "HOST", eventJson.getString("event_id"), null != eventJson.getString("cust_id") ? eventJson.getString("cust_id") : "default123", System.currentTimeMillis(), eventJson.toString());
                                ECClient.configure();
                                boolean resp = ECClient.enqueue("HOST", null != eventJson.getString("cust_id") ? eventJson.getString("cust_id") : "default123", eventJson.getString("event_id"), eventJson.toString());
                                ECClient.terminate();
                            }
                        }
                    }
            );
        } catch (JSONException jse) {
            logger.info("A JSONObject text must begin with '{'");
        }
        return null;
    }

    public static String createJsonFromEvent(JSONObject jsonObject) {
        //System.out.println("inside create transaction method");
        EventGenerator eg = new EventGenerator();
        eg.setMax_score(jsonObject.getJSONObject("matchedResults").getString("maxScore"));
        eg.setRule_name(jsonObject.getString("ruleName"));
        eg.setCust_id(jsonObject.has("cust_id") ? jsonObject.getString("cust_id") : "");
        JSONArray array = jsonObject.getJSONObject("matchedResults").has("matchedRecords") ? jsonObject.getJSONObject("matchedResults").getJSONArray("matchedRecords") : new JSONArray();
        for (int i = 0; i < array.length(); i++) {
            //  System.out.println("index value is: " + i + "for json objet data is----> " + ((JSONObject) array.get(i)));
            eg.setDj_id(((JSONObject) array.get(i)).has("id") ? ((JSONObject) array.get(i)).getString("id") : "");
            eg.setList_name(((JSONObject) array.get(i)).has("listName") ? ((JSONObject) array.get(i)).getString("listName") : "");
            eg.setSanctions_references_list_provider_code(
                    new JSONObject(((JSONObject) array.get(i)).getString("details"))
                            .has("sanctionsreferences_ListProviderCode") ?
                            new JSONObject(((JSONObject) array.get(i)).getString("details"))
                                    .getString("sanctionsreferences_ListProviderCode").replaceAll("\\|", "") : "");
            eg.setDescription_code(new JSONObject(((JSONObject) array.get(i)).getString("details"))
                    .has("descriptionCode") ? new JSONObject(((JSONObject) array.get(i))
                    .getString("details")).getString("descriptionCode").replaceAll("\\|", "") : "");

            JSONArray fieldArray = ((JSONObject) array.get(i)).getJSONArray("fields");
            for (int j = 0; j < fieldArray.length(); j++) {
                if (((JSONObject) fieldArray.get(j)).has("name") &&
                        ((JSONObject) fieldArray.get(j)).getString("name").
                                equalsIgnoreCase("entityType")) {
                    eg.setSource(((JSONObject) fieldArray.get(j)).has("queryValue") ?
                            ((JSONObject) fieldArray.get(j)).getString("queryValue") : "");
                } else {
                    eg.setScore(((JSONObject) fieldArray.get(j)).has("score") ?
                            ((JSONObject) fieldArray.get(j)).getString("score") : "");
                    eg.setName(((JSONObject) fieldArray.get(j)).has("name") ?
                            ((JSONObject) fieldArray.get(j)).getString("name") : "");
                    eg.setQuery_value(((JSONObject) fieldArray.get(j)).has("queryValue") ?
                            ((JSONObject) fieldArray.get(j)).getString("queryValue") : "");
                    eg.setValue(((JSONObject) fieldArray.get(j)).has("value") ?
                            ((JSONObject) fieldArray.get(j)).getString("value") : "");
                }
            }
        }
        return CxJson.obj2json(eg);
    }

    public static CxJson createEvent(String json) {
        CxJson eventJson = new CxJson();
        String event_id = String.valueOf(System.nanoTime() + (++count != 1000 ? count : (count = 1)));
        eventJson.put("event_id", event_id);
        eventJson.put("eventsubtype", "tradefinancedj");
        eventJson.put("event-name", "ft_tradefinancedj");
        eventJson.put("eventtype", "ft");
        //eventJson.put("source", "");
        eventJson.put("keys", "");
        try {
            CxJson finalJsonEvent = CxJson.parse(json);
            finalJsonEvent.put("host-id", "F");
            finalJsonEvent.put("sys_time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS")));
            finalJsonEvent.put("eventts", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS")));
            eventJson.put("msgBody", finalJsonEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // System.out.println("data inside eventJson is: "+eventJson);
        //System.out.println("resp after writing in queue--> "+resp);
        return eventJson;
    }

    public static String watchListResponse(Map<String, String> dataMap) {
        return null;
    }
}
