package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import cxps.noesis.core.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by anshul on 12/10/15.
 */
public class TR_StatusChangeEvent extends Event {

    private String tradingAccountId;
    private String initialAccountStatus;
    private String finalAccountStatus;
    private Double availableBalance;
    private Double marginBalanceAmount;
    private Double nonMarginBalanceAmount;
    private String remisierCode;
    private Date accountOpenDate;
    private Date sysDateTime;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");


    @Override
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setTradingAccountId((String) msgMap.get("TA_account-id"));
        setInitialAccountStatus((String) msgMap.get("init-acct-status"));
        setFinalAccountStatus((String) msgMap.get("final-acct-status"));
        setAvailableBalance((String) msgMap.get("avl-bal"));
        setMarginBalanceAmount((String) msgMap.get("margin-bal-amt"));
        setNonMarginBalanceAmount((String) msgMap.get("non-margin-bal-amt"));
        setRemisierCode((String) msgMap.get("remesier-code"));
        setAccountOpenDate((String) msgMap.get("acct-open-dt"));
        setSysDateTime((String) msgMap.get("sys-datetime"));

    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        ICXLog cxLog = CXLog.fenter("derive.FT_AccountTxnEvent", new Object[0]);
        HashSet wsInfoSet = new HashSet();
        String hostAcctKey = this.getTradingAccountId();
        if(hostAcctKey == null || hostAcctKey.trim().length() == 0) {
            cxLog.ferrexit("Nil-HostAcctKey", new Object[0]);
        }

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        String cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, this.getHostId(), hostAcctKey.trim());
        if(!CxKeyHelper.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
            cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey, new Object[0]);
        }

        WorkspaceInfo wi = new WorkspaceInfo("Account", cxAcctKey);
        wi.addParam("acctId", cxAcctKey);

        wsInfoSet.add(wi);

        return wsInfoSet;
    }

    public String getTradingAccountId() {
        return tradingAccountId;
    }

    public void setTradingAccountId(String tradingAccountId) {
        this.tradingAccountId = tradingAccountId;
    }

    public String getInitialAccountStatus() {
        return initialAccountStatus;
    }

    public void setInitialAccountStatus(String initialAccountStatus) {
        this.initialAccountStatus = initialAccountStatus;
    }

    public String getFinalAccountStatus() {
        return finalAccountStatus;
    }

    public void setFinalAccountStatus(String finalAccountStatus) {
        this.finalAccountStatus = finalAccountStatus;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        try {
            double availBal = Double.parseDouble(availableBalance);
            this.availableBalance = availBal;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:availableBalance>value:" + availableBalance);
        }
    }

    public Double getMarginBalanceAmount() {
        return marginBalanceAmount;
    }

    public void setMarginBalanceAmount(String marginBalanceAmount) {
        try {
            double marginBalanceAmt = Double.parseDouble(marginBalanceAmount);
            this.marginBalanceAmount = marginBalanceAmt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:marginBalanceAmount>value:" + marginBalanceAmount);
        }
    }

    public Double getNonMarginBalanceAmount() {
        return nonMarginBalanceAmount;
    }

    public void setNonMarginBalanceAmount(String nonMarginBalanceAmount) {
        try {
            double nonMarginBalanceAmt = Double.parseDouble(nonMarginBalanceAmount);
            this.nonMarginBalanceAmount = nonMarginBalanceAmt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:nonMarginBalanceAmount>value:" + nonMarginBalanceAmount);
        }
    }

    public String getRemisierCode() {
        return remisierCode;
    }

    public void setRemisierCode(String remisierCode) {
        this.remisierCode = remisierCode;
    }

    public Date getAccountOpenDate() {
        return accountOpenDate;
    }

    public void setAccountOpenDate(String accountOpenDate) {
        try {
            this.accountOpenDate = dateFormat1.parse(accountOpenDate);
        } catch (ParseException ex) {
            try {
                this.accountOpenDate = dateFormat2.parse(accountOpenDate);
            } catch (ParseException ex1) {
                try {
                    this.accountOpenDate = dateFormat.parse(accountOpenDate);
                } catch (ParseException e) {
                    throw new IllegalArgumentException("Invalid Value>field:accountOpenDate>value:" + accountOpenDate);
                }
            }
        }
    }

    public Date getSysDateTime() {
        return sysDateTime;
    }

    public void setSysDateTime(String sysDateTime) {
        if (StringUtils.isNullOrEmpty(sysDateTime)) {
            throw new IllegalArgumentException("Value is required: sysDateTime");
        } else {
            try {
                this.sysDateTime = dateFormat1.parse(sysDateTime);
            } catch (ParseException ex) {
                try {
                    this.sysDateTime = dateFormat2.parse(sysDateTime);
                } catch (ParseException ex1) {
                    try {
                        this.sysDateTime = dateFormat.parse(sysDateTime);
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("Invalid Value>field:sysDateTime>value:" + sysDateTime);
                    }
                }
            }
        }
    }
}
