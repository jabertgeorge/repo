package clari5.upload.ui;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;

import clari5.rdbms.Rdbms;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

import cxps.apex.utils.CxpsLogger;


/**
 * Servlet implementation class DisplayInsert
 */

public class DisplayInsert extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private ResultSetMetaData rsMetaData = null;
    private static CxpsLogger logger = CxpsLogger.getLogger(DisplayInsert.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session;
        String requestTableName;
        int numberOfColumns = 0;
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' type='text/css' href='css/pagination.css'>");
        out.println("<script type='text/javascript' src='js/paginationScript.js'>");
        out.println("</script>");
        out.println(" <link rel='stylesheet' href='css/bootstrap.min.css'>");
        out.println("<script src='js/bootstrap.min.js'></script>");
        out.println("<script src='efm/JavaScriptServlet'></script>");
        out.println("</head>");
        session = request.getSession();
        requestTableName = (String) session.getAttribute("tableName");

        int re = (int) request.getAttribute("TOTAL");

        try {
//			if (session == null || session.getAttribute("userId")== null){
//				request.getRequestDispatcher("/expiry.jsp").forward(request,response);
//			}
            /* Fetches the data inserted from the file */
            con = Rdbms.getAppConnection();
            String jdbc = "select TOP " + re + " * from " + requestTableName;
            logger.info("Select Query [" + jdbc + "] and Request Table Name is [" + requestTableName + "]");
            ps = con.prepareStatement(jdbc);
            rs = ps.executeQuery();
            rsMetaData = rs.getMetaData();
            /* Displays the fetched data */
            numberOfColumns = rsMetaData.getColumnCount();
            out.println("<body>");
            out.println("<div class='row'><div class='col-xs-5'></div><div class='col-xs-4'>");
            out.println("<h4>Records Inserted : " + re + "</h4>");
            out.println("<div class='col-xs-3'></div></div><br>");
            out.println("<div class='row'><div class='col-xs-4'></div><div class='col-xs-6'>");
            out.println("<h4>Inserted Table : " + requestTableName + "</h4>");
            out.println("</div><div class='col-xs-2'></div></div><br>");
            out.println("<div class='row'><div class='col-xs-1'></div><div class='col-xs-10'>");
            out.println("<table class='table table-bordered table-hover' id='tablepaging'>");
            out.println("<tr>");
            out.println("<thead class='bg-info'>");
            for (int i = 1; i <= numberOfColumns; i++)
                out.println("<th>" + rsMetaData.getColumnName(i) + "</th>");
            out.println("</tr>");
            out.println("</thead>");
            String data = "";
            while (rs.next()) {
                out.println("<tr style='padding: 0px 0px 0px 50px;'>");
                int c = 1;
                while (c <= numberOfColumns) {

                    data = rs.getString(rsMetaData.getColumnName(c));
                    out.println("<td>" + data + "</td>");
                    c++;
                }
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</div><div class='col-xs-1'></div></div>");
            out.println("<div class='row'><div class='col-xs-7'></div>");
            out.println("<div class='col-xs-5 pagination' id='pageNavPosition' >");
            out.println("</div><div class='col-xs-2'></div></div>");
            out.println("<script type='text/javascript'>");
            out.println("var pager = new Pager('tablepaging', 5,'pager','pageNavPosition')");
            out.println("pager.init()");
            out.println("pager.showPage(1)");
            out.println("</script></body></html>");

        } catch (SQLException e) {
            logger.info("Error in Displaying Inserted Data [" + e.getMessage() + "] Cause [" + e.getCause() + "]");
        } finally {
            try {
                if (con != null)
                    con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
    }
}
