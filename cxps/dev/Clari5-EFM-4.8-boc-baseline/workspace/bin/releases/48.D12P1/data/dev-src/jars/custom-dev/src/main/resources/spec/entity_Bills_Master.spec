cxps.noesis.glossary.entity.Bills-Master {
        attributes = [

			{name = CIFID, type = "string:20"}
			{name = Treasury-Bill-Num, type = "string:20", key=true }
			{name = Sale-Date, type = "date:20"}
			{name = Face-Value-Amount, type = "decimal:20,3"}
			{name = Face-Value-Crncy, type = "string:20"}
			{name = Face-Value-LKR, type = "string:20"}
			{name = Face-Value-Local, type = "string:20"}
			{name = Maturity-Date, type = "date"}
     ]
}

