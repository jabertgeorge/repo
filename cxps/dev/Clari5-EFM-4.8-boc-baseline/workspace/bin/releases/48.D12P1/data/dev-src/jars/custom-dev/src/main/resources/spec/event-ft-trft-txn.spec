cxps.events.event.ft-trft-txn {
    table-name : EVENT_FT_TRFT_TXN
    event-mnemonic: FTR
    workspaces : {
        CUSTOMER: cust-id
    }
    event-attributes : {
       tran-id : {db : true, raw_name: tran_id, type : "string:100"}
       tran-date : {db : true, raw_name: tran_date, type : "string:100"}
       event-name : {db : true, raw_name: event_name, type : "string:100"}	
       event-type : {db : true, raw_name: eventtype, type : "string:100"}
       event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
       host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
       msg-source : {db : true ,raw_name : source ,type : "string:20"}
       event-time : {db : true ,raw_name : eventts ,type : timestamp} 
       keys : {db : true ,raw_name : keys ,type : "string:20"} 
       system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}	
       host-id : {db : true ,raw_name : host-id ,type : "string:20"}
       channel-id : {db : true ,raw_name : channel ,type : "string:20"}
       source-or-dest-acct-id : {db : true ,raw_name : account-id ,type : "string:20"}
       acct-type : {db : true ,raw_name : Acct-type ,type : "string:20"}
       scheme-code : {db : true ,raw_name : Prod-code ,type : "string:20"}
       cust-id : {db : true ,raw_name : cust-id ,type : "string:20"}
       cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
       cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
       acct-name : {db : true ,raw_name : acct-name ,type : "string:20"}
       branch-id-desc : {db : true ,raw_name : acct-Branch-id ,type : "string:20"}
       account-ownership : {db : true ,raw_name : acct-ownership ,type : "string:20"}
       account-opendate : {db : true ,raw_name : acctopendate ,type : "timestamp"}
       avl-bal : {db : true ,raw_name : avl-bal ,type : "number:11,2"}
       avl-bal : {db : true ,raw_name : avl-bal_LCY ,type : "number:11,2"}
       tran-type : {db : true ,raw_name : tran-type ,type : "string:20"}
       tran-sub-type : {db : true ,raw_name : tran-sub-type ,type : "string:20"}
       txn-dr-cr : {db : true ,raw_name : part-tran-type ,type : "string:20"}
       txn-date : {db : true ,raw_name : tran-date ,type : timestamp}
       tran-posted-dt : {db : true ,raw_name : pstd-date ,type : timestamp}
       tran-id : {db : true ,raw_name : tran-id ,type : "string:20"}
       tran-srl-no : {db : true ,raw_name : part_tran_srl_num ,type : "string:20"}
       part-tran-srl-num : {db : true ,raw_name : part_tran_srl_num ,type : "string:20"}
       txn-code : {db : true ,raw_name : tran_code ,type : "string:20"}
       txn-value-date : {db : true ,raw_name : value-date ,type : timestamp}
       txn-amt : {db : true ,raw_name : tran-amt ,type : "number:11,2"}
       tran-crncy-code : {db : true ,raw_name : tran-crncy-code ,type : "string:20"}
       Ref-amt : {db : true ,raw_name : lcy-tran-amt,type : "number:11,2"}
       Ref-Curncy : {db : true ,raw_name : lcy-tran-crncy,type : "number:11,2"}
       desc : {db : true ,raw_name : tran-particular ,type : "string:20"}
       remarks : {db : true ,raw_name : tran-rmks ,type : "string:20"}
       instrument-type : {db : true ,raw_name : instrmnt-type ,type : "string:20"}
       system-time : {db : true ,raw_name : sys-time ,type : timestamp}
       source-bank : {db : true ,raw_name : bank-code ,type : "string:20"}
       txn-status : {db : true ,raw_name : pstd-flg ,type : "string:20"}
       online-batch : {db : true ,raw_name : online-batch ,type : "string:20"}
       countrParty-account : {db : true ,raw_name : CounterParty_Account ,type : "string:20"}
       counterParty-name : {db : true ,raw_name : CounterParty_Name ,type : "string:20"}
       counterParty-bank : {db : true ,raw_name : CounterParty_Bank ,type : "string:20"}
       counterParty-amount : {db : true ,raw_name : CounterParty_Amount ,type : "string:20"}
       counterParty-currency : {db : true ,raw_name : CounterParty_Currency ,type : "string:20"}
       counterParty-amount-lcy : {db : true ,raw_name :CounterParty _Amount_LCY ,type : "string:20"}
       counterParty-currency-lcy : {db : true ,raw_name : CounterParty_Currency_LCY ,type : "string:20"}
       counterParty-address : {db : true ,raw_name : CounterParty_Address ,type : "string:20"}
       counterParty-bank-code : {db : true ,raw_name : CounterParty_Bank_Code,type : "string:20"}
       counterParty-BIC: {db : true ,raw_name : CounterParty_BIC,type : "string:20"}
       counterParty-bank-address : {db : true ,raw_name : CounterParty_Bank_Address,type : "string:20"}
       counterParty-country-code : {db : true ,raw_name : CounterParty_Country_Code,type : "string:20"}
       counterParty-business : {db : true ,raw_name : CounterParty_Business,type : "string:20"}
       teller-number : {db : true ,raw_name : Teller_number ,type : "string:20" }
       sequence-number : {db : true ,raw_name : Sequence_number ,type : "string:20" }
       add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
       add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
       add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
       add-entity-id4 : {raw_name : reservedfield4 ,type : "string:100"}
       add-entity-id5 : {raw_name : reservedfield5 ,type : "string:100"}
   }
}

