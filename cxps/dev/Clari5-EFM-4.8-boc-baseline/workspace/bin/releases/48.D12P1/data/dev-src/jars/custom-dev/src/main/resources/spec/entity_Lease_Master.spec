cxps.noesis.glossary.entity.Lease-Master {
        attributes = [
			{name = Lease-Facility-Number, type = "string:20", key=true }
			{name = Lease-Facility-Number-Str, type = "string:50"}
			{name = StakeHolder-ID, type = "string:20"}
			{name = CORE-CIFID, type = "string:20"}
			{name = cxCifID, type = "string:50"}
			{name = Lease-Creation-Date, type = "date"}
			{name = Lease-Facility-Amount-In-LKR, type = "decimal:20,3"}
			{name = Lease-Amount, type = "decimal:20,3"}
			{name = Customer-Contribution-In-LKR, type = "decimal:20,3"}
			{name = NIC, type = "string:20"}
			{name = Interest-Rate, type = "decimal:20,3"}
			{name = Product-Code, type = "string:20"}
			{name = Product-Code-Description, type = "string:20"}
			{name = BankRelatedFamily, type = "string:20"}
			{name = CustomerName, type = "string:20"}
			{name = Leasing-Branch-Id, type = "string:20"}
			{name = STATUS-FOR-THE-FACILITY-NUMBER, type = "string:20"}
			{name = Lease-Maturity-Date, type = "date"}     
	]
}

