// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_ACCOUNT_TXN", Schema="rice")
public class FT_AccountTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String exception;
       @Field(size=20) public String beneficiaryName;
       @Field public java.sql.Timestamp zoneDate;
       @Field(size=20) public String fcnrFlag;
       @Field(size=20) public String bin;
       @Field(size=20) public String onlineBatch;
       @Field(size=20) public String tranSubType;
       @Field(size=20) public String accountid;
       @Field(size=30) public String counterPartyBusiness;
       @Field(size=20) public String txnRefType;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String acctType;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=10) public Double counterPartyAmount;
       @Field(size=20) public String txnCode;
       @Field(size=30) public String counterPartyCurrencyLcy;
       @Field(size=20) public String directChannelId;
       @Field(size=100) public String tranamtAcctNo;
       @Field(size=20) public String sequenceNumber;
       @Field(size=20) public String tranCrncyCode;
       @Field public java.sql.Timestamp tranVerifiedDt;
       @Field(size=30) public String counterPartyBic;
       @Field(size=20) public String purAcctNum;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String payeeId;
       @Field(size=20) public String acctOccpCode;
       @Field(size=20) public String acctStatus;
       @Field(size=20) public String systemCountry;
       @Field(size=300) public String counterPartyBankAddress;
       @Field(size=200) public String status;
       @Field(size=20) public String entityType;
       @Field(size=20) public String zoneCode;
       @Field(size=20) public String countryCode;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String merchantCateg;
       @Field(size=20) public String instAlpha;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String payerIdTemp;
       @Field(size=30) public String counterPartyCountryCode;
       @Field(size=20) public String terminalIpAddr;
       @Field(size=38) public Double fcyTranAmt;
       @Field(size=20) public String cxAcctId;
       @Field(size=30) public String counterPartyCurrency;
       @Field(size=20) public String entryUser;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String addEntityId5;
       @Field(size=20) public String eventType;
       @Field(size=20) public String partTranSrlNum;
       @Field(size=20) public String accountOwnership;
       @Field(size=11) public Double sanctionAmt;
       @Field(size=200) public String eightZeros;
       @Field(size=11) public Double refCurncy;
       @Field(size=20) public String kycStatus;
       @Field(size=100) public String internalTxnCode;
       @Field(size=20) public String agentId;
       @Field(size=30) public String counterPartyAmountLcy;
       @Field(size=20) public String tranType;
       @Field(size=20) public String channelDesc;
       @Field(size=30) public String counterPartyBank;
       @Field(size=20) public String branchIdDesc;
       @Field(size=20) public String creDrPtran;
       @Field(size=20) public String txnRefId;
       @Field(size=20) public String sourceBank;
       @Field(size=20) public String payeeCity;
       @Field(size=20) public String devOwnerId;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String tranId;
       @Field(size=20) public String entityId;
       @Field(size=300) public String counterPartyAddress;
       @Field(size=20) public String sourceOrDestAcctId;
       @Field(size=11) public Double refAmt;
       @Field(size=20) public String placeHolder;
       @Field(size=20) public String directChannelControllerId;
       @Field(size=20) public String payeeName;
       @Field(size=30) public String counterPartyAccount;
       @Field(size=20) public String channelType;
       @Field(size=20) public String acctName;
       @Field(size=20) public String custRefType;
       @Field(size=20) public String custCardId;
       @Field(size=20) public String tranSrlNo;
       @Field(size=11) public Double avlBal;
       @Field(size=20) public String evtInducer;
       @Field public java.sql.Timestamp txnValueDate;
       @Field public java.sql.Timestamp tranPostedDt;
       @Field(size=20) public String schemeType;
       @Field(size=20) public String desc;
       @Field(size=10) public String fcyTranCur;
       @Field(size=20) public String agentType;
       @Field public java.sql.Timestamp tranEntryDt;
       @Field(size=20) public String cxCifId;
       @Field(size=20) public String actionAtHost;
       @Field(size=20) public String schemeCode;
       @Field(size=20) public String keys;
       @Field(size=20) public String custRefId;
       @Field public java.sql.Timestamp eventTime;
       @Field(size=20) public String channelId;
       @Field(size=20) public String billId;
       @Field(size=11) public Double rate;
       @Field(size=20) public String txnStatus;
       @Field(size=100) public String counterPartyName;
       @Field(size=11) public Double txnAmt;
       @Field(size=11) public Double effAvlBal;
       @Field(size=20) public String tranCategory;
       @Field(size=30) public String tellerNumber;
       @Field(size=20) public Long instrumentId;
       @Field(size=20) public String balanceCur;
       @Field(size=30) public String counterPartyBankCode;
       @Field(size=11) public Double ledgerBal;
       @Field(size=20) public String employeeId;
       @Field(size=20) public String acctSolId;
       @Field(size=100) public String eventSubtype;
       @Field(size=20) public String txnAmtCur;
       @Field(size=20) public String refCurrency;
       @Field(size=20) public String modeOprnCode;
       @Field(size=20) public String msgName;
       @Field(size=20) public String custId;
       @Field public java.sql.Timestamp accountOpendate;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String hdrmkrs;
       @Field(size=20) public String tranBrId;
       @Field(size=20) public String remitterName;
       @Field(size=20) public String instrumentType;
       @Field(size=11) public Double shadowBal;
       @Field(size=11) public Double refTranAmt;
       @Field(size=20) public Long eod;
       @Field(size=20) public String remarks;


    @JsonIgnore
    public ITable<FT_AccountTxnEvent> t = AEF.getITable(this);

	public FT_AccountTxnEvent(){}

    public FT_AccountTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("AccountTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setException(json.getString("exception"));
            setBeneficiaryName(json.getString("beneficiary_name"));
            setZoneDate(EventHelper.toTimestamp(json.getString("zone_date")));
            setFcnrFlag(json.getString("fcnr_flag"));
            setBin(json.getString("BIN"));
            setOnlineBatch(json.getString("online-batch"));
            setTranSubType(json.getString("tran-sub-type"));
            setAccountid(json.getString("acid"));
            setCounterPartyBusiness(json.getString("counter_party_business"));
            setTxnRefType(json.getString("txn_ref_type"));
            setSystemTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setTxnDrCr(json.getString("part-tran-type"));
            setAcctType(json.getString("Acct-type"));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setCounterPartyAmount(EventHelper.toDouble(json.getString("counter_party_amount")));
            setTxnCode(json.getString("tran_code"));
            setCounterPartyCurrencyLcy(json.getString("counter_party_currency_lcy"));
            setDirectChannelId(json.getString("dc_id"));
            setSequenceNumber(json.getString("sequence_number"));
            setTranCrncyCode(json.getString("tran-crncy-code"));
            setTranVerifiedDt(EventHelper.toTimestamp(json.getString("vfd_date")));
            setCounterPartyBic(json.getString("counter_party_bic"));
            setPurAcctNum(json.getString("pur_acct_num"));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setPayeeId(json.getString("payee_id"));
            setAcctOccpCode(json.getString("acct_occp_code"));
            setAcctStatus(json.getString("Acct_Stat"));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));
            setCounterPartyBankAddress(json.getString("counter_party_bank_address"));
            setEntityType(json.getString("entity_type"));
            setZoneCode(json.getString("zone_code"));
            setCountryCode(json.getString("country_code"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setMerchantCateg(json.getString("merchant_categ"));
            setInstAlpha(json.getString("instrmnt_alpha"));
            setMsgSource(json.getString("source"));
            setPayerIdTemp(json.getString("acid"));
            setCounterPartyCountryCode(json.getString("counter_party_country_code"));
            setTerminalIpAddr(json.getString("ip_address"));
            setFcyTranAmt(EventHelper.toDouble(json.getString("fcy-tran-amt")));
            setCxAcctId(json.getString("cx-acct-id"));
            setCounterPartyCurrency(json.getString("counter_party_currency"));
            setEntryUser(json.getString("entry_user"));
            setAddEntityId2(json.getString("reservedfield2"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("sys_time")));
            setAddEntityId3(json.getString("reservedfield3"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("event_type"));
            setPartTranSrlNum(json.getString("part_tran_srl_num"));
            setAccountOwnership(json.getString("acct-ownership"));
            setSanctionAmt(EventHelper.toDouble(json.getString("sanction_amt")));
            setRefCurncy(EventHelper.toDouble(json.getString("lcy-tran-crncy")));
            setKycStatus(json.getString("KYC_Stat"));
            setInternalTxnCode(json.getString("internalTxnCode"));
            setAgentId(json.getString("pstd_user_id"));
            setCounterPartyAmountLcy(json.getString("counter_party_amount_lcy"));
            setTranType(json.getString("tran-type"));
            setChannelDesc(json.getString("channel_desc"));
            setCounterPartyBank(json.getString("counter_party_bank"));
            setBranchIdDesc(json.getString("acct-Branch-id"));
            setCreDrPtran(json.getString("cre_dr_ptran"));
            setTxnRefId(json.getString("tran_id"));
            setSourceBank(json.getString("bank-code"));
            setPayeeCity(json.getString("payeecity"));
            setDevOwnerId(json.getString("dev_owner_id"));
            setDeviceId(json.getString("device_id"));
            setTranId(json.getString("tran-id"));
            setEntityId(json.getString("entity_id"));
            setCounterPartyAddress(json.getString("counter_party_address"));
            setSourceOrDestAcctId(json.getString("account-id"));
            setRefAmt(EventHelper.toDouble(json.getString("lcy-tran-amt")));
            setPlaceHolder(json.getString("place_holder"));
            setDirectChannelControllerId(json.getString("dcc_id"));
            setPayeeName(json.getString("payeename"));
            setCounterPartyAccount(json.getString("counter_party_account"));
            setChannelType(json.getString("channel_type"));
            setAcctName(json.getString("acct-name"));
            setCustRefType(json.getString("cust_ref_type"));
            setCustCardId(json.getString("cust_card_id"));
            setTranSrlNo(json.getString("part_tran_srl_num"));
            setAvlBal(EventHelper.toDouble(json.getString("avl-bal_LCY")));
            setEvtInducer(json.getString("evt_inducer"));
            setTxnValueDate(EventHelper.toTimestamp(json.getString("value-date")));
            setTranPostedDt(EventHelper.toTimestamp(json.getString("pstd-date")));
            setSchemeType(json.getString("schm_type"));
            setDesc(json.getString("tran-particular"));
            setFcyTranCur(json.getString("fcy-tran-cur"));
            setAgentType(json.getString("agent_type"));
            setTranEntryDt(EventHelper.toTimestamp(json.getString("entry_date")));
            setActionAtHost(json.getString("module_id"));
            setSchemeCode(json.getString("Prod-code"));
            setKeys(json.getString("keys"));
            setCustRefId(json.getString("acid"));
            setEventTime(EventHelper.toTimestamp(json.getString("eventts")));
            setChannelId(json.getString("channel"));
            setBillId(json.getString("ref_num"));
            setRate(EventHelper.toDouble(json.getString("rate")));
            setTxnStatus(json.getString("pstd-flg"));
            setCounterPartyName(json.getString("counter_party_name"));
            setTxnAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setEffAvlBal(EventHelper.toDouble(json.getString("eff_avl_bal")));
            setTranCategory(json.getString("tran_category"));
            setTellerNumber(json.getString("teller_number"));
            setInstrumentId(EventHelper.toLong(json.getString("instrmnt_type")));
            setBalanceCur(json.getString("balance_cur"));
            setCounterPartyBankCode(json.getString("counter_party_bank_code"));
            setLedgerBal(EventHelper.toDouble(json.getString("clr_bal_amt")));
            setEmployeeId(json.getString("emp_id"));
            setAcctSolId(json.getString("acct_sol_id"));
            setEventSubtype(json.getString("eventsubtype"));
            setTxnAmtCur(json.getString("tran_crncy_code"));
            setRefCurrency(json.getString("ref_tran_crncy"));
            setModeOprnCode(json.getString("mode_oprn_code"));
            setMsgName(json.getString("msg_name"));
            setCustId(json.getString("cust-id"));
            setAccountOpendate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setTranDate(EventHelper.toTimestamp(json.getString("tran-date")));
            setHdrmkrs(json.getString("hdrmkrs"));
            setTranBrId(json.getString("TXN_BR-ID"));
            setRemitterName(json.getString("remitter_name"));
            setInstrumentType(json.getString("instrmnt-type"));
            setShadowBal(EventHelper.toDouble(json.getString("un_clr_bal_amt")));
            setRefTranAmt(EventHelper.toDouble(json.getString("ref_tran_amt")));
            setEod(EventHelper.toLong(json.getString("eod")));
            setRemarks(json.getString("tran-rmks"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setTranamtAcctNo(cxps.noesis.core.EventHelper.concat(txnAmt, custRefId));setStatus(cxps.events.CustomFieldDerivator.deriveEightZeros(this));setEightZeros(cxps.events.CustomFieldDerivator.deriveEightZeros(this));setCxCifId(clari5.custom.dev.fatcabootstrapping.CustomDerivator.checkInstance(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getException(){ return exception; }

    public String getBeneficiaryName(){ return beneficiaryName; }

    public java.sql.Timestamp getZoneDate(){ return zoneDate; }

    public String getFcnrFlag(){ return fcnrFlag; }

    public String getBin(){ return bin; }

    public String getOnlineBatch(){ return onlineBatch; }

    public String getTranSubType(){ return tranSubType; }

    public String getAccountid(){ return accountid; }

    public String getCounterPartyBusiness(){ return counterPartyBusiness; }

    public String getTxnRefType(){ return txnRefType; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getAcctType(){ return acctType; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public Double getCounterPartyAmount(){ return counterPartyAmount; }

    public String getTxnCode(){ return txnCode; }

    public String getCounterPartyCurrencyLcy(){ return counterPartyCurrencyLcy; }

    public String getDirectChannelId(){ return directChannelId; }

    public String getSequenceNumber(){ return sequenceNumber; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public java.sql.Timestamp getTranVerifiedDt(){ return tranVerifiedDt; }

    public String getCounterPartyBic(){ return counterPartyBic; }

    public String getPurAcctNum(){ return purAcctNum; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getPayeeId(){ return payeeId; }

    public String getAcctOccpCode(){ return acctOccpCode; }

    public String getAcctStatus(){ return acctStatus; }

    public String getSystemCountry(){ return systemCountry; }

    public String getCounterPartyBankAddress(){ return counterPartyBankAddress; }

    public String getEntityType(){ return entityType; }

    public String getZoneCode(){ return zoneCode; }

    public String getCountryCode(){ return countryCode; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getMerchantCateg(){ return merchantCateg; }

    public String getInstAlpha(){ return instAlpha; }

    public String getMsgSource(){ return msgSource; }

    public String getPayerIdTemp(){ return payerIdTemp; }

    public String getCounterPartyCountryCode(){ return counterPartyCountryCode; }

    public String getTerminalIpAddr(){ return terminalIpAddr; }

    public Double getFcyTranAmt(){ return fcyTranAmt; }

    public String getCxAcctId(){ return cxAcctId; }

    public String getCounterPartyCurrency(){ return counterPartyCurrency; }

    public String getEntryUser(){ return entryUser; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getPartTranSrlNum(){ return partTranSrlNum; }

    public String getAccountOwnership(){ return accountOwnership; }

    public Double getSanctionAmt(){ return sanctionAmt; }

    public Double getRefCurncy(){ return refCurncy; }

    public String getKycStatus(){ return kycStatus; }

    public String getInternalTxnCode(){ return internalTxnCode; }

    public String getAgentId(){ return agentId; }

    public String getCounterPartyAmountLcy(){ return counterPartyAmountLcy; }

    public String getTranType(){ return tranType; }

    public String getChannelDesc(){ return channelDesc; }

    public String getCounterPartyBank(){ return counterPartyBank; }

    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getCreDrPtran(){ return creDrPtran; }

    public String getTxnRefId(){ return txnRefId; }

    public String getSourceBank(){ return sourceBank; }

    public String getPayeeCity(){ return payeeCity; }

    public String getDevOwnerId(){ return devOwnerId; }

    public String getDeviceId(){ return deviceId; }

    public String getTranId(){ return tranId; }

    public String getEntityId(){ return entityId; }

    public String getCounterPartyAddress(){ return counterPartyAddress; }

    public String getSourceOrDestAcctId(){ return sourceOrDestAcctId; }

    public Double getRefAmt(){ return refAmt; }

    public String getPlaceHolder(){ return placeHolder; }

    public String getDirectChannelControllerId(){ return directChannelControllerId; }

    public String getPayeeName(){ return payeeName; }

    public String getCounterPartyAccount(){ return counterPartyAccount; }

    public String getChannelType(){ return channelType; }

    public String getAcctName(){ return acctName; }

    public String getCustRefType(){ return custRefType; }

    public String getCustCardId(){ return custCardId; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public Double getAvlBal(){ return avlBal; }

    public String getEvtInducer(){ return evtInducer; }

    public java.sql.Timestamp getTxnValueDate(){ return txnValueDate; }

    public java.sql.Timestamp getTranPostedDt(){ return tranPostedDt; }

    public String getSchemeType(){ return schemeType; }

    public String getDesc(){ return desc; }

    public String getFcyTranCur(){ return fcyTranCur; }

    public String getAgentType(){ return agentType; }

    public java.sql.Timestamp getTranEntryDt(){ return tranEntryDt; }

    public String getActionAtHost(){ return actionAtHost; }

    public String getSchemeCode(){ return schemeCode; }

    public String getKeys(){ return keys; }

    public String getCustRefId(){ return custRefId; }

    public java.sql.Timestamp getEventTime(){ return eventTime; }

    public String getChannelId(){ return channelId; }

    public String getBillId(){ return billId; }

    public Double getRate(){ return rate; }

    public String getTxnStatus(){ return txnStatus; }

    public String getCounterPartyName(){ return counterPartyName; }

    public Double getTxnAmt(){ return txnAmt; }

    public Double getEffAvlBal(){ return effAvlBal; }

    public String getTranCategory(){ return tranCategory; }

    public String getTellerNumber(){ return tellerNumber; }

    public Long getInstrumentId(){ return instrumentId; }

    public String getBalanceCur(){ return balanceCur; }

    public String getCounterPartyBankCode(){ return counterPartyBankCode; }

    public Double getLedgerBal(){ return ledgerBal; }

    public String getEmployeeId(){ return employeeId; }

    public String getAcctSolId(){ return acctSolId; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getTxnAmtCur(){ return txnAmtCur; }

    public String getRefCurrency(){ return refCurrency; }

    public String getModeOprnCode(){ return modeOprnCode; }

    public String getMsgName(){ return msgName; }

    public String getCustId(){ return custId; }

    public java.sql.Timestamp getAccountOpendate(){ return accountOpendate; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getHdrmkrs(){ return hdrmkrs; }

    public String getTranBrId(){ return tranBrId; }

    public String getRemitterName(){ return remitterName; }

    public String getInstrumentType(){ return instrumentType; }

    public Double getShadowBal(){ return shadowBal; }

    public Double getRefTranAmt(){ return refTranAmt; }

    public Long getEod(){ return eod; }

    public String getRemarks(){ return remarks; }
    public String getTranamtAcctNo(){ return tranamtAcctNo; }

    public String getStatus(){ return status; }

    public String getEightZeros(){ return eightZeros; }

    public String getCxCifId(){ return cxCifId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setException(String val){ this.exception = val; }
    public void setBeneficiaryName(String val){ this.beneficiaryName = val; }
    public void setZoneDate(java.sql.Timestamp val){ this.zoneDate = val; }
    public void setFcnrFlag(String val){ this.fcnrFlag = val; }
    public void setBin(String val){ this.bin = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setAccountid(String val){ this.accountid = val; }
    public void setCounterPartyBusiness(String val){ this.counterPartyBusiness = val; }
    public void setTxnRefType(String val){ this.txnRefType = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setAcctType(String val){ this.acctType = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCounterPartyAmount(Double val){ this.counterPartyAmount = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setCounterPartyCurrencyLcy(String val){ this.counterPartyCurrencyLcy = val; }
    public void setDirectChannelId(String val){ this.directChannelId = val; }
    public void setSequenceNumber(String val){ this.sequenceNumber = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranVerifiedDt(java.sql.Timestamp val){ this.tranVerifiedDt = val; }
    public void setCounterPartyBic(String val){ this.counterPartyBic = val; }
    public void setPurAcctNum(String val){ this.purAcctNum = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setAcctOccpCode(String val){ this.acctOccpCode = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }
    public void setCounterPartyBankAddress(String val){ this.counterPartyBankAddress = val; }
    public void setEntityType(String val){ this.entityType = val; }
    public void setZoneCode(String val){ this.zoneCode = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setMerchantCateg(String val){ this.merchantCateg = val; }
    public void setInstAlpha(String val){ this.instAlpha = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setPayerIdTemp(String val){ this.payerIdTemp = val; }
    public void setCounterPartyCountryCode(String val){ this.counterPartyCountryCode = val; }
    public void setTerminalIpAddr(String val){ this.terminalIpAddr = val; }
    public void setFcyTranAmt(Double val){ this.fcyTranAmt = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setCounterPartyCurrency(String val){ this.counterPartyCurrency = val; }
    public void setEntryUser(String val){ this.entryUser = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setPartTranSrlNum(String val){ this.partTranSrlNum = val; }
    public void setAccountOwnership(String val){ this.accountOwnership = val; }
    public void setSanctionAmt(Double val){ this.sanctionAmt = val; }
    public void setRefCurncy(Double val){ this.refCurncy = val; }
    public void setKycStatus(String val){ this.kycStatus = val; }
    public void setInternalTxnCode(String val){ this.internalTxnCode = val; }
    public void setAgentId(String val){ this.agentId = val; }
    public void setCounterPartyAmountLcy(String val){ this.counterPartyAmountLcy = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setChannelDesc(String val){ this.channelDesc = val; }
    public void setCounterPartyBank(String val){ this.counterPartyBank = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setCreDrPtran(String val){ this.creDrPtran = val; }
    public void setTxnRefId(String val){ this.txnRefId = val; }
    public void setSourceBank(String val){ this.sourceBank = val; }
    public void setPayeeCity(String val){ this.payeeCity = val; }
    public void setDevOwnerId(String val){ this.devOwnerId = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setEntityId(String val){ this.entityId = val; }
    public void setCounterPartyAddress(String val){ this.counterPartyAddress = val; }
    public void setSourceOrDestAcctId(String val){ this.sourceOrDestAcctId = val; }
    public void setRefAmt(Double val){ this.refAmt = val; }
    public void setPlaceHolder(String val){ this.placeHolder = val; }
    public void setDirectChannelControllerId(String val){ this.directChannelControllerId = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setCounterPartyAccount(String val){ this.counterPartyAccount = val; }
    public void setChannelType(String val){ this.channelType = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustRefType(String val){ this.custRefType = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setEvtInducer(String val){ this.evtInducer = val; }
    public void setTxnValueDate(java.sql.Timestamp val){ this.txnValueDate = val; }
    public void setTranPostedDt(java.sql.Timestamp val){ this.tranPostedDt = val; }
    public void setSchemeType(String val){ this.schemeType = val; }
    public void setDesc(String val){ this.desc = val; }
    public void setFcyTranCur(String val){ this.fcyTranCur = val; }
    public void setAgentType(String val){ this.agentType = val; }
    public void setTranEntryDt(java.sql.Timestamp val){ this.tranEntryDt = val; }
    public void setActionAtHost(String val){ this.actionAtHost = val; }
    public void setSchemeCode(String val){ this.schemeCode = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setCustRefId(String val){ this.custRefId = val; }
    public void setEventTime(java.sql.Timestamp val){ this.eventTime = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setBillId(String val){ this.billId = val; }
    public void setRate(Double val){ this.rate = val; }
    public void setTxnStatus(String val){ this.txnStatus = val; }
    public void setCounterPartyName(String val){ this.counterPartyName = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setEffAvlBal(Double val){ this.effAvlBal = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setTellerNumber(String val){ this.tellerNumber = val; }
    public void setInstrumentId(Long val){ this.instrumentId = val; }
    public void setBalanceCur(String val){ this.balanceCur = val; }
    public void setCounterPartyBankCode(String val){ this.counterPartyBankCode = val; }
    public void setLedgerBal(Double val){ this.ledgerBal = val; }
    public void setEmployeeId(String val){ this.employeeId = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setTxnAmtCur(String val){ this.txnAmtCur = val; }
    public void setRefCurrency(String val){ this.refCurrency = val; }
    public void setModeOprnCode(String val){ this.modeOprnCode = val; }
    public void setMsgName(String val){ this.msgName = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setAccountOpendate(java.sql.Timestamp val){ this.accountOpendate = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setHdrmkrs(String val){ this.hdrmkrs = val; }
    public void setTranBrId(String val){ this.tranBrId = val; }
    public void setRemitterName(String val){ this.remitterName = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setShadowBal(Double val){ this.shadowBal = val; }
    public void setRefTranAmt(Double val){ this.refTranAmt = val; }
    public void setEod(Long val){ this.eod = val; }
    public void setRemarks(String val){ this.remarks = val; }
    public void setTranamtAcctNo(String val){ this.tranamtAcctNo = val; }
    public void setStatus(String val){ this.status = val; }
    public void setEightZeros(String val){ this.eightZeros = val; }
    public void setCxCifId(String val){ this.cxCifId = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getcountry(){ return systemCountry; }
    @JsonIgnore
    public String getCountry_code(){ return countryCode; }
    @JsonIgnore
    public String getDev_owner_id(){ return devOwnerId; }
    @JsonIgnore
    public String getCust_card_id(){ return custCardId; }
    @JsonIgnore
    public java.sql.Timestamp getTran_date(){ return tranDate; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_AccountTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.custRefId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.tranamtAcctNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        String transactionKey= h.getCxKeyGivenHostKey(WorkspaceName.TRANSACTION, getHostId(), this.txnRefId);
        wsInfoSet.add(new WorkspaceInfo("Transaction", transactionKey));
        String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.agentId);
        wsInfoSet.add(new WorkspaceInfo("User", userKey));
        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        json.put("tran_sub_type",getTranSubType());
        json.put("acct_type",getAcctType());
        json.put("part_tran_srl_num",getPartTranSrlNum());
        json.put("Ref_Curncy",getRefCurncy());
        json.put("tran_type",getTranType());
        json.put("source_or_dest_acct_id",getSourceOrDestAcctId());
        json.put("Ref_amt",getRefAmt());
        json.put("tran_srl_no",getTranSrlNo());
        json.put("txn_amt",getTxnAmt());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_AccountTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "AccountTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("tranamt_acct_no",getTranamtAcctNo());
        json.put("country_code",getCountryCode());
        json.put("acct_open_date",getAcctOpenDate());
        json.put("dev_owner_id",getDevOwnerId());
        json.put("cust_card_id",getCustCardId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}