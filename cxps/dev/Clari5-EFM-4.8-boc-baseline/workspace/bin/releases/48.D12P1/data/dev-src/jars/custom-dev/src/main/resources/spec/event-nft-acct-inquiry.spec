cxps.events.event.nft-acct-inquiry {

  event-mnemonic = NA
  workspaces : { ACCOUNT : cust-ref-id, BRANCH :  branch-id, USER : user-id }

  event-attributes : {
    account-id:{type:"string:20",db:true,raw_name:account_id, evidence :true}
    host-id:{type:"string:100",db:true,raw_name:HostId, evidence :true}
    account-name:{type:"string:20",db:true,raw_name:acct_name, evidence :true}
    enquiry-type:{type:"string:20",db:true,raw_name:enquiry_type}
    user-id:{type:"string:20",db:true,raw_name:user_id, evidence :true}
    enquiry-time-stamp:{type:timestamp,db:true,raw_name:enquiry_time_stamp}
    enquiry-channel:{type:"string:20",db:true,raw_name:enquiry_channel}
    branch-id:{type:"string:20",db:true,raw_name:branch_id}
    acct-sol-id:{type:"string:20",db:true,raw_name:acct_sol_id}
    tran-br-id:{type:"string:20",db:true,raw_name:tran_br_id}
    instrument-id:{type:"string:20",db:true,raw_name:instrument_id}
    instrument-type:{type:"string:20",db:true,raw_name:instrument_type}
    cust-ref-id:{type:"string:20",db:true,raw_name:acid}
    cust-ref-type:{type:"string:20",db:true,raw_name:cust_ref_type}
    menu-id:{type:"string:20",db:true,raw_name:menu_id, evidence :true}
    branch-id-desc:{type:"string:20",db:true,raw_name:branch_id_desc, evidence :true}
    emp-id:{type:"string:20",db:true,raw_name:emp_id, evidence :true}
    channel-type:{type:"string:20",db:true,raw_name:channel_type}
    channel-desc:{type:"string:20",db:true,raw_name:channel_desc}
    add-entity1:{type:"string:20",db:true,raw_name:add_entity1}
    available-bal:{type:"number:11,2",db:true,raw_name:avl_bal, evidence :true, custom-getter:Avl_bal}    
 }
}

