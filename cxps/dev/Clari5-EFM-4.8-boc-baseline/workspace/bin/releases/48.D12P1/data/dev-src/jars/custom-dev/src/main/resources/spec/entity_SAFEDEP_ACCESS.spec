cxps.noesis.glossary.entity.SAFEDEP-ACCESS {
        attributes = [

			{name = NIC, type = "string:20"}
			{name = Locker-Number, type = "number:20", key=true }
			{name = Branch-Code, type = "string:20"}
			{name = Access-People-Name-1, type = "string:20"}
			{name = Access-People-NIC-1, type = "string:20"}
			{name = Access-People-Name-2, type = "string:20"}
			{name = Access-People-NIC-2, type = "string:20"}
			{name = Access-People-Name-3, type = "string:20"}
			{name = Access-People-NIC-3, type = "string:20"}
     ]
}

