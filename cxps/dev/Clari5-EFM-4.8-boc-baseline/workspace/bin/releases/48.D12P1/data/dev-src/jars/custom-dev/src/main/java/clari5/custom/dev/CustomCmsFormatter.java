package clari5.custom.dev;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.hfdb.Hfdb;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.jira.JiraClientException;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import org.apache.poi.util.CloseIgnoringInputStream;
import org.json.JSONObject;

import javax.print.DocFlavor;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.Map.Entry;

/**
 * Created by deepak on 21/8/17.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    public static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);

    static Hocon loadCustomField;

    static {
        loadCustomField = new Hocon();
        loadCustomField.loadFromContext("custom_field.conf");
    }

    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public String getCaseAssignee(Incident caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }

        //if (caseEvent.getEventId().startsWith("FA")) {
        //  return super.getCaseAssignee(caseEvent);
        //}

        String brCode = null;
        String entityId = caseEvent.entityId;
        logger.info("Entity id inside getCaseAssignee is : " + entityId);
        if (!entityId.startsWith("A_F_") && !entityId.startsWith("C_F_")) {
            return super.getCaseAssignee(caseEvent);
        }
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        Map<String, Integer> userGroupMap = null;
        try {
            List list = cmsUserCaseAssignment.process(entityId);
            if (list.isEmpty()) {
                logger.info("BR CODE not available for account id -> " + entityId);
                return super.getCaseAssignee(caseEvent);
            }
            logger.info("list datainside CustomCmsFormatter getCaseAssignee: " + list);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Map) {
                    userGroupMap = (Map) list.get(i);
                    if (userGroupMap.isEmpty()) {
                        return super.getCaseAssignee(caseEvent);
                    }
                }
                if (list.get(i) instanceof String) {
                    brCode = (String) list.get(i);
                    if (brCode == null) {
                        return super.getCaseAssignee(caseEvent);
                    }
                }
            }
            logger.info("data inside usergroupmap is: " + userGroupMap);
        } catch (Exception e) {
            logger.info("data coming null... Hence calling default logic inside case assignee...");
            return super.getCaseAssignee(caseEvent);
            //e.printStackTrace();
        }
        String moduleId = caseEvent.moduleId;
        CmsModule cmsModule = (CmsModule) cmsJira.get(moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        //  System.out.println("for department "+departmentMap);
        String userGroup = cmsModule.getUserGroup();

        String departmentName = caseEvent.project;
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        try {
            resNextUser = getUserWithLeastCase(userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("dda br code inside CustomCmsFormatter getCaseAssignee() is: " + brCode);
        Map<String, Integer> map = caseMap(CmsUserCaseAssignment.channelUserGroupMap.get(brCode), resNextUser);
        CmsUserCaseAssignment.channelUserGroupMap.put(brCode, map);
        logger.info("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public Map<String, Integer> caseMap(Map<String, Integer> map, String resNextUser) {
        Map<String, Integer> map1 = map;
        for (Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().contains(resNextUser)) {
                int count = map1.get(resNextUser);
                count += 1;
                map1.put(resNextUser, count);
            }
        }
        return map1;
    }


    @Override
    public String getIncidentAssignee(Incident caseEvent) throws CmsException {
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");

            throw new CmsException("Unable to get resource cmsjira");
        }

        //if (caseEvent.getEventId().startsWith("FA")) {
        //  return super.getIncidentAssignee(caseEvent);
        //}

        String brCode = "";
        String entityId = caseEvent.entityId;
        logger.info("Entity id inside getIncidentAssignee is: " + entityId);
        if (!entityId.startsWith("A_F_") && !entityId.startsWith("C_F_")) {
            return super.getIncidentAssignee(caseEvent);
        }
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        Map<String, Integer> userGroupMap = new HashMap<>();
        try {
            List list = cmsUserCaseAssignment.process(entityId);
            if (list.isEmpty()) {
                logger.info("calling superClass.BRCODE not available for account Id -> " + entityId);
                return super.getIncidentAssignee(caseEvent);
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Map) {
                    userGroupMap = (Map) list.get(i);
                    logger.info("usergroup map inside getIncidentassignee : " + userGroupMap);
                    if (userGroupMap.isEmpty()) {
                        logger.info("calling superClass for userGroupMap as it is empty");
                        return super.getIncidentAssignee(caseEvent);
                    }
                }
                if (list.get(i) instanceof String) {
                    brCode = (String) list.get(i);
                    logger.info("brcode is: " + brCode);
                    if (brCode == null) {
                        logger.info("calling super as brcode not available:");
                        return super.getIncidentAssignee(caseEvent);
                    }
                }
            }
            logger.info("data inside usergroupmap inside incident assignee is: " + userGroupMap);
        } catch (Exception e) {
            logger.info("data coming null... hence calling default logic inside Incident assignee");
            String nextAssignee = super.getIncidentAssignee(caseEvent);
            logger.info("Next user is " + nextAssignee);
            return super.getIncidentAssignee(caseEvent);
            //e.printStackTrace();
        }
        CmsModule cmsModule = (CmsModule) cmsJira.get(caseEvent.moduleId);
        Map<String, String> departmentMap = cmsModule.getDepartments();
        logger.info("for module id  " + cmsModule);
        String userGroup = cmsModule.getUserGroup();
        String departmentName = caseEvent.project;
        if (departmentMap.containsKey(departmentName)) {
            userGroup = departmentMap.get(departmentName);
        }
        JiraInstance jiraInstance = cmsModule.getInstance();
        JiraClient jiraClient;
        try {
            jiraClient = new JiraClient(jiraInstance.getInstanceParams());
        } catch (JiraClientException.Configuration e) {
            throw new CmsException.Configuration(e.getMessage());
        }
        processGroupUserCasesMap(jiraClient, userGroup);
        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;

        try {
            resNextUser = getUserWithLeastCase(userGroupMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("incident assignee dda br code data from CmsUserCaseAssignment is: " + brCode + " for accountId is -> " + entityId);
        Map<String, Integer> map = caseMap(CmsUserCaseAssignment.channelUserGroupMap.get(brCode), resNextUser);
        CmsUserCaseAssignment.channelUserGroupMap.put(brCode, map);
        logger.info("Nex user selected" + resNextUser);
        return resNextUser;
    }

    public String getUserWithLeastCase(Map<String, Integer> map) {
        Set<Entry<String, Integer>> set = map.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        for (Map.Entry<String, Integer> entry : list) {
            logger.info(entry.getKey() + " ==== " + entry.getValue());
        }
        String user = list.get(0).getKey();

        logger.info("User with least no of cases " + user);
        return user;
    }

    public static String getClassnameFromMnemonic(String mnemonic) {

        String className = "";
        try (CxConnection con = CmsUserCaseAssignment.getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT class_name from WS_MNEMONIC WHERE mnemonic = ?");
            ps.setString(1, mnemonic);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                className = rs.getString("class_name") != null ? rs.getString("class_name")
                        : "";
            }
            return className.endsWith("Event") ? className : className + "Event";
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return className.endsWith("Event") ? className : className + "Event";
    }

    public static String getTableNamefromClassName(String className) {
        String tableName = "";
        try {
            for (Annotation annotation : Class.forName("cxps.events." + className).getAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();
                Method method = type.getMethod("Name");
                Object value = method.invoke(annotation, (Object[]) null);
                tableName = (String) value;
                logger.info("table name from Event Name: " + tableName);
            }
            return tableName != null ? tableName : "";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return tableName != null ? tableName : "";
    }

    public static String getAccountIdForStrReport(String tableName, String eventId) {
        String accountID = "";
        try (CxConnection con = CmsUserCaseAssignment.getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT cx_acct_id from " + tableName + " WHERE  event_id= ?");
            ps.setString(1, eventId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                accountID = rs.getString("cx_acct_id") != null ? rs.getString("cx_acct_id")
                        : "";
            }
            return accountID != null ? accountID : "";
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return accountID != null ? accountID : "";
    }

    protected String getCustName(String cif) {
        String custName = "";
        try (CxConnection connection = CmsUserCaseAssignment.getConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT custName FROM CUSTOMER WHERE cxCifID = ?")) {
            int i = 1;
            ps.setString(i, cif);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    custName = rs.getString("custName") != null ? rs.getString("custName") : "Not Available";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (custName != null || !custName.isEmpty() ? custName : "Not Available");
    }


    public CxJson populateIncidentJson(Incident event, String jiraParentId, boolean isupdate) throws IOException, CmsException {


        CxJson json = null;
        if (event.eventId == null || event.eventId.isEmpty()) {
            json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
        }
        if (!event.eventId.startsWith("FA") && event.entityId.startsWith("A_")
                && !event.eventId.startsWith("TF")) {
            CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.moduleId);
            json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
            String eventJson = CxJson.obj2json(event);
            Hocon h = new Hocon(eventJson);
            if (cmsMod.getCustomFields().containsKey("accountsDetails") && h.getString("evidence") != null) {
                String jiraId_accountsDetails = cmsMod.getCustomFields().get("accountsDetails").getJiraId();
                if (json.get("fields").hasKey(jiraId_accountsDetails)) {
                    json.get("fields").put(jiraId_accountsDetails, event.entityId);
                }
            }
            logger.info("json data from CMS utility is: " + json);
            if (json.get("fields").hasKey(loadCustomField.getString("fields.customerName"))) {
                json.get("fields").put(loadCustomField.getString("fields.customerName"),
                        getCustName(event.caseentityId != null ?
                                event.caseentityId : "") != null ? getCustName(event.caseentityId != null ?
                                event.caseentityId : "") : "");
            }
        } else if (!event.eventId.startsWith("FA") && !event.entityId.startsWith("A_")
                && !event.eventId.startsWith("TF")) {
            logger.info("json data from CMS utility is: " + json);
            String className = getClassnameFromMnemonic(event.eventId.substring(0, event.eventId.indexOf("_")));
            String tableName = getTableNamefromClassName(className);
            String accountId = getAccountIdForStrReport(tableName, event.eventId);

            CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.moduleId);
            json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
            String eventJson = CxJson.obj2json(event);
            Hocon h = new Hocon(eventJson);
            if (cmsMod.getCustomFields().containsKey("accountsDetails") && h.getString("evidence") != null) {
                String jiraId_accountsDetails = cmsMod.getCustomFields().get("accountsDetails").getJiraId();
                if (json.get("fields").hasKey(jiraId_accountsDetails)) {
                    json.get("fields").put(jiraId_accountsDetails, accountId);
                }
            }
            if (json.get("fields").hasKey(loadCustomField.getString("fields.customerName"))) {
                json.get("fields").put(loadCustomField.getString("fields.customerName"),
                        getCustName(event.caseentityId != null ?
                                event.caseentityId : "") != null ? getCustName(event.caseentityId != null ?
                                event.caseentityId : "") : "");
            }
        } else {
            json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
            logger.info("json data from CMS utility is: " + json);
            if (json.get("fields").hasKey(loadCustomField.getString("fields.customerName"))) {
                json.get("fields").put(loadCustomField.getString("fields.customerName"),
                        getCustName(event.caseentityId != null ?
                                event.caseentityId : "") != null ? getCustName(event.caseentityId != null ?
                                event.caseentityId : "") : "");
            }
            //json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        }
        //Added the below code for the PaymentCard Decryption.
        logger.info("CustomCmsFormatter:getFormattedIncidentEntityId, json  --> " + json);
        json.get("fields").remove("customfield_10214");
        json.get("fields").put("customfield_10214", getCaseEntityId(event.entityName, event.displaykey));
        logger.info("CustomCmsFormatter:getFormattedIncidentEntityId, updated json  --> " + json);
        return json;
    }

    public String getCaseEntityId(String entityName, String displayKey) {
        try {
            if (entityName.equalsIgnoreCase("PAYMENTCARD")) {
                String wsName = "Paymentcard";
                String url = System.getenv("SECURE_DN") + "/efm/Derive.jsp?cardNumber=" + displayKey.substring(0, displayKey.indexOf("|")).trim() + "&cName=" + wsName + "&jUID=cxpsaml&src=jira";
                logger.info("URL for jsp inside getFormattedCaseEntityId is: " + url);
                displayKey += "| [Details|" + url + "]";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("CustomCmsFormatter:getFormattedIncidentEntityId, DisplayKey  --> " + displayKey);
        return displayKey;
    }

    @Override
    public void postAssignmentAction(RDBMSSession session, Incident event) throws CmsException {

        try {
            validateEntityFact(event, session);

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("cx_key", event.entityId);
            map.put("fact_name", event.factName);
            map.put("date_time", event.dateTime.toString());
            map.put("source", event.source);
            map.put("risk_level", event.riskLevel);
            map.put("module_id", event.moduleId);
            map.put("score", event.incidentScore);
            map.put("evidence", event.evidence);
            map.put("pc_flag", "Y");
            map.put("migrated_flag", "Y");

            insertEntityFact("custom_entity_fact", map, session);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void validateEntityFact(Incident event, RDBMSSession session) {

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            String query = "select cx_key, fact_name, date_time, source, risk_level, module_id, score, evidence, pc_flag, migrated_flag " +
                    "from custom_entity_fact where cx_key = ? and fact_name = ?";

            logger.info("checking table 'custom_entity_fact' for entry --> ");
            CxConnection con = session.getCxConnection();
            try (PreparedStatement pstmt = con.prepareStatement(query)) {
                pstmt.setString(1, event.entityId);
                pstmt.setString(2, event.factName);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    map.put("cx_key", rs.getString(1));
                    map.put("fact_name", rs.getString(2));
                    map.put("date_time", rs.getString(3).toString());
                    map.put("source", rs.getString(4));
                    map.put("risk_level", rs.getString(5));
                    map.put("module_id", rs.getString(6));
                    map.put("score", String.valueOf(rs.getInt(7)));
                    logger.info("Evidence--> " + rs.getClob(8).toString());
                    map.put("evidence", rs.getClob(8).toString());
                    map.put("pc_flag", rs.getString(9));
                    map.put("migrated_flag", rs.getString(10));
                }
            }
            if (map.size() > 0) {
                logger.info("Inserting the record in the hist table and deleting in main table");
                insertEntityFact("custom_entity_fact_hist", map, session);
                deleteEntityFact("custom_entity_fact", event.entityId, event.factName, session);
                map.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertEntityFact(String tableName, HashMap<String, String> map, RDBMSSession session) {

        String query = "";
        try {
            if (tableName.equalsIgnoreCase("custom_entity_fact_hist")) {
                query = "insert into " + tableName + " (cx_key, fact_name, date_time, source, risk_level, module_id," +
                        " score, evidence, pc_flag, migrated_flag, entity-fact-hist-id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            } else {
                query = "insert into " + tableName + " (cx_key, fact_name, date_time, source, risk_level, module_id," +
                        " score, evidence, pc_flag, migrated_flag) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            }
            CxConnection con = session.getCxConnection();
            try (PreparedStatement pstmt = con.prepareStatement(query)) {
                pstmt.setString(1, map.get("cx_key"));
                pstmt.setString(2, map.get("fact_name"));
                pstmt.setTimestamp(3, new Timestamp(new Date(map.get("date_time")).getTime()));
                pstmt.setString(4, map.get("source"));
                pstmt.setString(5, map.get("risk_level"));
                pstmt.setString(6, map.get("module_id"));
                pstmt.setInt(7, Integer.getInteger(map.get("score")));
                pstmt.setClob(8, getClobValue(con, map.get(" evidence")));
                pstmt.setString(9, map.get("pc_flag"));
                pstmt.setString(10, map.get("migrated_flag"));
                if (tableName.equalsIgnoreCase("custom_entity_fact_hist"))
                    pstmt.setString(11, String.valueOf(UUID.randomUUID()));
                pstmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Clob getClobValue(Connection con, String evidence) throws SQLException {
        Clob clob = con.createClob();
        clob.setString(1, evidence);
        return clob;
    }

    private void deleteEntityFact(String tableName, String cxKey, String factName, RDBMSSession session) {
        try {
            String query = "delete from " + tableName + " where cx_key = ? and fact_name = ?";
            CxConnection con = session.getCxConnection();
            try (PreparedStatement pstmt = con.prepareStatement(query)) {
                pstmt.setString(1, cxKey);
                pstmt.setString(2, factName);
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
