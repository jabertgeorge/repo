cxps.noesis.glossary.entity.CUSTOM_STR_DATA {
        attributes = [
                        {name = JIRA_INCIDENTID, type = "string:50"}
                        {name = EVENT_ID, type = "string:20"}
                        {name = ACCTID, type = "string:20"}
                        {name = CUSTID, type = "string:20"}
                        {name = MNEMONIC, type = "string:20"}
                        {name = TRAN_COUNT, type = "number:11,2"}
                        {name = TOTAL_AMT, type = "number:11,2"}
                        {name = str-id, type = "string:256", key=true}
        ]
}
