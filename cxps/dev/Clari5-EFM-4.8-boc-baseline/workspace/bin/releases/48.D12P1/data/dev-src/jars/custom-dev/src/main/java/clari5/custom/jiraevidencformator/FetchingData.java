package clari5.custom.jiraevidencformator;

import clari5.platform.util.Hocon;
import org.json.JSONObject;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static clari5.rdbms.Rdbms.getAppDbType;

public class FetchingData {
    public static String customField="";
    public static String dburl="";
    public static String user="";
    public static String password="";
    public static String drivername="";
    public static String schemaname="";

    static {

        ConfigFileReader reader = new ConfigFileReader("jiradb-module.conf");
        customField = reader.getCustomField();
        dburl = reader.getDatabase_url();
        user = reader.getDb_username();
        password = reader.getDb_password();
        drivername = reader.getDriver_name();
        schemaname = reader.getSchema_name();
        System.out.println("Static block read");
    }
    public  JSONObject fetch(String issue) throws IOException {
        Connection con =null;
        ResultSet rs=null;
        Statement stmt=null;
        String op = null;
        JSONObject json = new JSONObject();
        String table_name=schemaname+"CUSTOMFIELDVALUE";
        try {

            Class.forName(drivername);
            con= DriverManager.getConnection(dburl,user,password) ;
            stmt = con.createStatement();
            String DB_TYPE = null;
            String sql_query = null;
            DB_TYPE = dburl.split(":")[1];

            if(DB_TYPE.equalsIgnoreCase("ORACLE"))
            {
                sql_query = "select TEXTVALUE from "+table_name+" where issue=" + issue + " and CUSTOMFIELD =" + customField + "";
            }

            if(DB_TYPE.equalsIgnoreCase("MYSQL"))
            {
                sql_query = "select TEXTVALUE from CUSTOMFIELDVALUE where issue=" + issue + " and CUSTOMFIELD =" + customField + "";
            }

            if(DB_TYPE.equalsIgnoreCase("SQLSERVER"))
            {
                sql_query = "select TEXTVALUE from "+table_name+" where issue=" + issue + " and CUSTOMFIELD =" + customField + "";
            }

            rs = stmt.executeQuery(sql_query);
            while (rs.next()) {
                op = rs.getString("TEXTVALUE").toString();
            }
            if (op == null || op == ""){
                op="No Data Found";
            }
            json=json.put("evidence",op);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (con!=null){
                try {
                    con.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
        return json;
    }

   /* public static void main(String[] args) {
        FetchingData fetchingData = new FetchingData();
        try {
            JSONObject jsonObject = fetchingData.fetch("43227");
            System.out.println("===="+jsonObject);
        }catch (Exception e)
        {

        }

    }*/
}
