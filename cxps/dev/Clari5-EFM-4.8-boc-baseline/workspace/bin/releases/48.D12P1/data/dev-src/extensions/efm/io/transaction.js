/* transaction.js start */
var oTable;

var tranSrch="";
var tranOutput="";
var customerTab="";
var accountTab="";
var tranCount="";
tranSrch="<div class='heading' ><h3>Transaction Search</h3></div>";

tranSrch+="<table id='tranListD' >"
	+"<tr class='separator'><td class='fontSize'>Host System</td></tr>"
	+"<tr class='separator'><td class='fontSize'><select id='hostSys' ><option value='False Positive' >System1</option><option value='Watch List' >System2</option><option value='Suspicious Transaction' >System3</option></select></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Account ID</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' value='' id='acctId' /></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Instrument ID</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' value='' id='instId' /></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Debit/Credit</td></tr>"
	+"<tr class='separator'><td class='fontSize'><select id='tranType' ><option value='C' >Credit</option><option value='D' >Debit</option></select></td></tr>"
	+"<tr class='separator'><td class='fontSize'>From Amount</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' id='fromAmount'></td></tr>"
	+"<tr class='separator'><td class='fontSize'>To Amount</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' id='toAmount'></td></tr>"
	+"<tr class='separator'><td class='fontSize'>From Date</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' id='fromDate'></td></tr>"
	+"<tr class='separator'><td class='fontSize'>To Date</td></tr>"
	+"<tr class='separator'><td class='fontSize'><input type='text' id='toDate'></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Type</td></tr>"
	+"<tr class='separator'><td class='fontSize'><select id='type' ><option value='False Positive' >NEFT</option><option value='Watch List' >RTGS</option><option value='Watch List' >SWIFT</option></td></tr>"
	+"<tr  class='separator'><td class='fontSize'><input type='button' class='button' value='Search' onClick='loadTranData(this)' /></td></tr></table>";

tranOutput="<div id='tranSrchLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\"><h3>Transaction Details</h3></div><div id='tabTransaction' style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id='tranSrchDtls' class='dataTableCss' style='text-align:left;' ></table></div>";

var tranTreeData="";

function initTxn()
{
	var userId = top.user;
	var serverUrl = top.serverUrl;
	setParams(userId, serverUrl);
	$("#tranListD").show();
    $("#tranSrchLink").hide();
    	$("#fromDate").datepicker();
       	$("#toDate").datepicker();
       	$("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
       	$("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
       	$("#toDate").datepicker( "option", "maxDate", new Date());
}

function loadTranData(custId)
{
        toggleSrchWindow();
    	var fromDate =($("#fromDate").val()).split("/").reverse().join("-");
    	var toDate =($("#toDate").val()).split("/").reverse().join("-");
    	var fromAmount=$("#fromAmount").val();
    	var toAmount=$("#toAmount").val();
    	var hostSys=$("#hostSys").val();
    	var acctId=$("#acctId").val();
    	var instId=$("#instId").val();
    	var type=$("#type").val();
    	var tranType=$("#tranType").val();
    $("#tranSrchLink").show();
    addTab('Transaction','');
    getTransactionDetails(inputObject,transactionRespHandler,true);
}

function transactionRespHandler(resp)
{
        var response=resp.transactionLists;
        if(response=="" || response==undefined){
            var dataResp = [ ];
            var myArray =new Array();
            var tableHeaders = new Array();
            tableHeaders.push({ "sTitle": "Tran Id" });
            tableHeaders.push({ "sTitle": "Account Id" });
            tableHeaders.push({ "sTitle": "Date" });
            tableHeaders.push({ "sTitle": "Description" });
            tableHeaders.push({ "sTitle": "Amount" });
            tableHeaders.push({ "sTitle": "Tran Type" });
            dataResp.push(myArray);
            dataResp.push(tableHeaders);
            displayData(dataResp,"tabTranDtls","tranSrchDtls");
        }else{
            var resList = makeJsonForDatatable(response, "addTab","tranId",reqstPage);
            displayData(resList,"tabTranDtls","tranSrchDtls");
        }
}

function getTranTreeData(tCount,id){
tranTreeData="";
custRmlId="customerRelam"+tCount;
tranTreeData+="<div id=\"tranTree"+tCount+"\" class='treeLeftPanel' ><div id=\"tranContainer"+tCount+"\" style='margin-left:-6px;overflow-x:auto;height:100%;'></div></div><div class='treeRightPanel' ><div><button onclick='goBack(id)' class='abutton back-w-def goBackCls' id="+id+" style='cursor:hand;opacity:1;'>Back</button></div><div id=\"tranLink"+tCount+"\" style='width:101.3%;'><div id='tranSoftLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 0px 7px 7px;\"><div class=\"heading\" ><h3>Transaction Details</h3></div><div id=\"tabTranSft"+tCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id=\"tranSoftDtls"+tCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div></div>";
return tranTreeData;
}

var inputObj="";
var transactionId="";
function tranLoad(tranId,tTabCount,id){
	tranCount=tTabCount;
	transactionId=tranId;
	$("#menuBlock").hide();
	var inputObject={
	    "tranId":tranId,
		"contentType":"json"
	    };
    getTranSoftFactsDetails(inputObject,tranSoftRespHandler,true);
    getCustomerTreeDetails(treeObj,custTreeRespHandler,true);
}

function tranSoftRespHandler(resp){
        var response=resp.transactionDetails;
        if(response=="" || response==undefined){
            var dataResp = [ ];
            var myArray =new Array();
            var tableHeaders = new Array();
            tableHeaders.push({ "sTitle": "Account Id" });
            tableHeaders.push({ "sTitle": "Amount" });
            tableHeaders.push({ "sTitle": "Tran Type" });
            tableHeaders.push({ "sTitle": "Customer" });
            dataResp.push(myArray);
            dataResp.push(tableHeaders);
            displayData(dataResp,"tabTranSft"+tranCount,"tranSoftDtls"+tranCount);
        }else{
            var resList = makeJsonForDatatable(response, "addTab","acctId","Account");
            displayData(resList,"tabTranSft"+tranCount,"tranSoftDtls"+tranCount);
        }
}

function validateFormFields(formName)
{
	return $("#"+formName).validationEngine('validate');
}
/* transaction.js end */
