<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <%@ page language="java" import="clari5.custom.jiraevidencformator.FetchingData"%>
        <%@ page import="org.json.JSONObject" %>
            <% String ip=request.getParameter("id");%>
                <% FetchingData fetchingData= new FetchingData(); %>
                    <%JSONObject json  = fetchingData.fetch(ip);
   System.out.println("json---"+json);  
%>

                        <!DOCTYPE html>
                        <html>

                        <head>
                            <meta charset="UTF-8">

                            <title>Evidence</title>
                            <meta charset="utf-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


                            <script>
                                function formatTable() {
                                    var data = <%=json%>;
                                    var itemDetail = data;
                                    var str = itemDetail.evidence;
                                    if (!str.startsWith("\r\n")) {
                                        str = ("\r\n").concat(str);
                                    }
                                    var arr = [];

                                    function customStringify(str) {
                                        let counter = 0;
                                        str.split("\r\n\r\n").map(data => {
                                            arr[counter] = data;
                                            counter++;
                                        });
                                    }
                                    customStringify(str);

                                    function printArray(arr) {


                                        var resArr = [];
                                        let evidenceMessage = "";
                                        var evidenceArr = [];
                                        var headerCount;
                                        var finalHTML = "";
                                        var c;
                                        var flag = 0;
                                        var c1;
                                        var flag1 = 0;

                                        for (var i = 0; i < arr.length - 1; i++) {
                                            if (!arr[i].startsWith("\r\n")) {
                                                arr[i] = ("\r\n").concat(arr[i]);
                                            }
                                            var tableBody = "";
                                            var evidenceObj = {};
                                            var objCounter = 0;

                                            if (!arr[i].startsWith("\r\n||")) {
                                                evidenceArr = arr[i].split("\r\n|");
                                            } else {
                                                evidenceArr = arr[i].split("\r\n||");
                                            }


                                            for (var a = 1; a < evidenceArr.length; a++) {

                                                tableBody = tableBody.concat('<table class="table table-bordered ">');
                                                tableBody = tableBody.concat('<tr>');
                                                var headerVal = evidenceArr[a].split("\||");
                                                for (var k = 0; k < headerVal.length - 1; k++) {
                                                    if (headerVal[k] == "Previous Roles") {
                                                        c = k;
                                                        flag = 1;
                                                        tableBody = tableBody.concat('<td class="bold-text" style="width: 500px;">').concat(headerVal[k]).concat('</td>');
                                                    }
                                                    if (headerVal[k] == "Primary Occupation") {
                                                        c1 = k;
                                                        flag1 = 1;
                                                        tableBody = tableBody.concat('<td class="bold-text" style="width: 500px;">').concat(headerVal[k]).concat('</td>');
                                                    }

                                                }
                                                tableBody = tableBody.concat('</tr>');
                                                var dataval = headerVal[headerVal.length - 1].split("\r\n|");
                                                dataval = dataval.filter(function(e) {
                                                    return e.replace(/(\r\n|\n|\r)/gm, "")
                                                });
                                                for (var z = 0; z < dataval.length; z++) {
                                                    tableBody = tableBody.concat('<tr>');
                                                    var data = dataval[z].split("|");

                                                    for (var k = 0; k < data.length - 1; k++) {
                                                        var j = 0
                                                        if (c == k && flag == 1) {
                                                            var da = data[k].split("#_#");
                                                            console.log("=", da);
                                                            tableBody = tableBody.concat('<td style="width: 500px;">');
                                                            tableBody = tableBody.concat('<table class="table table-bordered table-striped">');
                                                            for (var j = 0; j < da.length; j++) {
                                                                tableBody = tableBody.concat('<tr>');
                                                                tableBody = tableBody.concat('<td>').concat(da[j]).concat('</td>');
                                                                tableBody = tableBody.concat('</tr>');
                                                            }


                                                            //tableBody = tableBody.concat(data[k]);

                                                            tableBody = tableBody.concat('</tbody></table>');
                                                            tableBody = tableBody.concat('</td>');


                                                        }
                                                        if (c1 == k && flag1 == 1) {

                                                            tableBody = tableBody.concat('<td style="width: 500px;">').concat(data[k]).concat('</td>');
                                                        }
                                                    }
                                                    tableBody = tableBody.concat('</tr>');
                                                }
                                            }
                                            tableBody = tableBody.concat('</tbody></table>');

                                            finalHTML = finalHTML.concat(tableBody);
                                        }
                                        document.getElementById("tbl").innerHTML = finalHTML;
                                    }
                                    printArray(arr);
                                }
                            </script>

                            <style>
                                .bold-text {
                                    font-weight: bold;
                                    color: white;
                                    background-color: #5F5F5F;
                                }
                                
                                .table-striped>tbody>tr:nth-of-type(odd) {
                                    background-color: #ccc !important;
                                }
                                
                                .table-bordered>tbody>tr>td,
                                .table-bordered>tbody>tr>th,
                                .table-bordered>tfoot>tr>td,
                                .table-bordered>tfoot>tr>th,
                                .table-bordered>thead>tr>td,
                                .table-bordered>thead>tr>th {
                                    border: 2px solid #333 !important;
                                }
                            </style>
                        </head>

                        <body onload="formatTable()">

                            <div class="container">
                                <div class="row">
                                    <div id="tbl" class="col-sm-12">

                                    </div>
                                </div>
                            </div>

                        </body>

                        </html>