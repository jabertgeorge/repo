# Generated Code
cxps.noesis.glossary.entity.Finacle-DealMaster {
	attributes = [
		
			{name = CIFID, type = "string:20"}
			{name = deal-No, type = "string:20", key=true }
			{name = deal-Date, type = "date"}
			{name = deal-Amount, type = "decimal:20,3"}
			{name = deal-Amount-crncy, type = "string:20"}
			{name = deal-Amount-LKR, type = "string:20"}
			{name = deal-Currency-Local, type = "string:20"}
			{name = Maturity-Date, type = "date"}
			{name = Value-Date, type = "date"}

	]
}
