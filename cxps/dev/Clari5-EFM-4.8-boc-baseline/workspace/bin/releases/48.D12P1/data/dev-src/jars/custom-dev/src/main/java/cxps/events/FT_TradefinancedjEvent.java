// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_TRADE_FINANCE", Schema="rice")
public class FT_TradefinancedjEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp updatedOn;
       @Field(size=100) public String eventSubtype;
       @Field(size=100) public String eventType;
       @Field(size=100) public String maxScore;
       @Field(size=100) public String partTranSrlNum;
       @Field(size=100) public String tranId;
       @Field(size=100) public String source;
       @Field(size=100) public String custId;
       @Field(size=100) public String descriptionCode;
       @Field public java.sql.Timestamp eventts;
       @Field(size=100) public String djId;
       @Field(size=100) public String sanctionsReferencesListProviderCode;
       @Field(size=100) public String score;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=100) public String system;
       @Field public java.sql.Timestamp createdOn;
       @Field(size=100) public String eventName;
       @Field(size=100) public String ruleName;
       @Field(size=100) public String name;
       @Field(size=100) public String hostId;
       @Field(size=100) public String listName;
       @Field(size=100) public String value;
       @Field(size=100) public String queryValue;


    @JsonIgnore
    public ITable<FT_TradefinancedjEvent> t = AEF.getITable(this);

	public FT_TradefinancedjEvent(){}

    public FT_TradefinancedjEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Tradefinancedj");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setUpdatedOn(EventHelper.toTimestamp(json.getString("updated_on")));
            setEventSubtype(json.getString("eventsubtype"));
            setEventType(json.getString("eventtype"));
            setMaxScore(json.getString("max_score"));
            setPartTranSrlNum(json.getString("part_tran_srl_num"));
            setTranId(json.getString("tran_id"));
            setSource(json.getString("source"));
            setCustId(json.getString("cust_id"));
            setDescriptionCode(json.getString("description_code"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setDjId(json.getString("dj_id"));
            setSanctionsReferencesListProviderCode(json.getString("sanctions_references_list_provider_code"));
            setScore(json.getString("score"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setSystem(json.getString("SYSTEM"));
            setCreatedOn(EventHelper.toTimestamp(json.getString("created_on")));
            setEventName(json.getString("event-name"));
            setRuleName(json.getString("rule_name"));
            setName(json.getString("name"));
            setHostId(json.getString("host-id"));
            setListName(json.getString("list_name"));
            setValue(json.getString("value"));
            setQueryValue(json.getString("query_value"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "TF"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getUpdatedOn(){ return updatedOn; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getEventType(){ return eventType; }

    public String getMaxScore(){ return maxScore; }

    public String getPartTranSrlNum(){ return partTranSrlNum; }

    public String getTranId(){ return tranId; }

    public String getSource(){ return source; }

    public String getCustId(){ return custId; }

    public String getDescriptionCode(){ return descriptionCode; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getDjId(){ return djId; }

    public String getSanctionsReferencesListProviderCode(){ return sanctionsReferencesListProviderCode; }

    public String getScore(){ return score; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getSystem(){ return system; }

    public java.sql.Timestamp getCreatedOn(){ return createdOn; }

    public String getEventName(){ return eventName; }

    public String getRuleName(){ return ruleName; }

    public String getName(){ return name; }

    public String getHostId(){ return hostId; }

    public String getListName(){ return listName; }

    public String getValue(){ return value; }

    public String getQueryValue(){ return queryValue; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setUpdatedOn(java.sql.Timestamp val){ this.updatedOn = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setMaxScore(String val){ this.maxScore = val; }
    public void setPartTranSrlNum(String val){ this.partTranSrlNum = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setSource(String val){ this.source = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setDescriptionCode(String val){ this.descriptionCode = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setDjId(String val){ this.djId = val; }
    public void setSanctionsReferencesListProviderCode(String val){ this.sanctionsReferencesListProviderCode = val; }
    public void setScore(String val){ this.score = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setSystem(String val){ this.system = val; }
    public void setCreatedOn(java.sql.Timestamp val){ this.createdOn = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setRuleName(String val){ this.ruleName = val; }
    public void setName(String val){ this.name = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setListName(String val){ this.listName = val; }
    public void setValue(String val){ this.value = val; }
    public void setQueryValue(String val){ this.queryValue = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_TradefinancedjEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Tradefinancedj");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Tradefinancedj");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}