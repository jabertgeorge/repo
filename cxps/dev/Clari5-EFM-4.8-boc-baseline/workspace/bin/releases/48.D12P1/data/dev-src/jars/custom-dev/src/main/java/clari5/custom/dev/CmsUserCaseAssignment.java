package clari5.custom.dev;

import clari5.custom.boc.integration.BatchProcessor;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.platform.rdbms.RDBMS;
import cxps.apex.utils.CxpsLogger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Yashaswi
 */
public class CmsUserCaseAssignment {
    public static CxpsLogger logger = CxpsLogger.getLogger(CmsUserCaseAssignment.class);

    public static Map<String, Map<String, Integer>> channelUserGroupMap;

    /*public static void main(String[] args) {
        CmsUserCaseAssignment cmsUserCaseAssignment = new CmsUserCaseAssignment();
        try {
            cmsUserCaseAssignment.process("A_F_000000SB0015");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public List process(String entityid) throws Exception {
        CxConnection con = getConnection();
        Map<String, String> map = getBranchUser(con);
        logger.info("branch user map data: " + map);
        String brCode = getBrCodeFromDda(con, entityid);
        logger.info("brcode is: " + brCode);
        List list = new ArrayList();
        if (brCode != null) {
            Map<String, Integer> userGroupCount = getUserGroupMap(map, brCode);
            logger.info("userGroupcount map is: " + userGroupCount);
            list.add(userGroupCount);
            list.add(brCode);
        }
        logger.info("List of br code and user group map: " + list);
        return list;
    }

    public Map<String, String> getBranchUser(CxConnection connection) throws Exception {
        HashMap<String, String> branch_user = new HashMap<>();
        connection.query(this.userChannelSelectQuery(connection.getDbType()), resultSet -> {
            while (resultSet.next()) {
                branch_user.put(resultSet.getString("BR_CODE"), resultSet.getString("USER_ID"));
            }
        });
        return branch_user;
    }

    public String getBrCodeFromDda(CxConnection connection, String entityId) {
        String brCode = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        try {
            if (entityId.startsWith("A_F_")) {
                sql = "select acctBrID from DDA where acctID = '" + entityId + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    brCode = rs.getString("acctBrID");
                }
                if (brCode == null || brCode.isEmpty() || brCode.trim() == "") {
                    sql = "select acctBrID from TDA where acctID = '" + entityId + "'";
                    ps = connection.prepareStatement(sql);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        brCode = rs.getString("acctBrID");
                    }
                }
                return brCode;
            } else if (entityId.startsWith("C_F_")) {
                sql = "select primarySOL from CUSTOMER where cxCifID = '" + entityId + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    brCode = rs.getString("primarySOL");
                }
                return brCode;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public Map<String, Integer> getUserGroupMap(Map<String, String> branch_user, String brCode) {

        for (Map.Entry<String, String> entry : branch_user.entrySet()) {
            String userId = entry.getValue();
            String userBrcode = entry.getKey();
            if (channelUserGroupMap == null) {
                channelUserGroupMap = new HashMap<>();
                String[] splitUserId = userId.split("-");
                if (splitUserId.length > 1) {
                    Map<String, Integer> userCount = new HashMap<>();
                    for (int i = 0; i < splitUserId.length; i++) {
                        userCount.put(splitUserId[i], 0);
                    }
                    channelUserGroupMap.put(userBrcode, userCount);
                } else {
                    Map<String, Integer> userCount = new HashMap<>();
                    userCount.put(userId, 0);
                    channelUserGroupMap.put(userBrcode, userCount);
                }
            } else {
                if (!channelUserGroupMap.containsKey(userBrcode)) {
                    String[] splitUserId = userId.split("-");
                    if (splitUserId.length > 1) {
                        Map<String, Integer> userCount = new HashMap<>();
                        for (int i = 0; i < splitUserId.length; i++) {
                            userCount.put(splitUserId[i], 0);
                        }
                        channelUserGroupMap.put(userBrcode, userCount);
                    } else {
                        Map<String, Integer> userCount = new HashMap<>();
                        userCount.put(userId, 0);
                        channelUserGroupMap.put(userBrcode, userCount);
                    }
                } else {
                    channelUserGroupMap.get(userBrcode);
                }
            }
        }
        Map<String, Integer> finalMap = new HashMap<>();
        if (channelUserGroupMap.containsKey(brCode)) {
            finalMap = channelUserGroupMap.get(brCode);
        }
        logger.info("before returning final Map: " + finalMap);
        return finalMap;
    }

    public static CxConnection getConnection() {
        Clari5.batchBootstrap("test", "efm-clari5.conf");
        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
    }

    public String userChannelTableName() {
        return "BRANCH_USER";
    }

    public String userChannelSelectQuery(DbTypeEnum dbTypeEnum) {
        switch (dbTypeEnum) {
            case ORACLE:
                return "SELECT BR_CODE, USER_ID" +
                        " from " + this.userChannelTableName();
            case SQLSERVER:
                return "SELECT [BR_CODE], [USER_ID]" +
                        " from " + this.userChannelTableName();
        }
        return null;
    }
}