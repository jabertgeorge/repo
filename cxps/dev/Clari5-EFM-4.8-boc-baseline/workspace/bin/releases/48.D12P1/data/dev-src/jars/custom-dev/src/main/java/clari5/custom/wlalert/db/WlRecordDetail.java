package clari5.custom.wlalert.db;

import clari5.custom.wlalert.daemon.RuleFact;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import cxps.apex.utils.CxpsLogger;

import clari5.custom.wlalert.db.mappers.WlrecordDetailsMapper;
//import oracle.net.aso.s;
import org.apache.ibatis.session.RowBounds;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;


public class WlRecordDetail extends DBConnection implements IDBOperation {

    private static final CxpsLogger logger = CxpsLogger.getLogger(WlRecordDetail.class);


    public WlRecordDetail() {
        rdbmsConnection = new RDBMSConnection();
    }

    @Override
    public Object getAllRecords() {
        RDBMSSession rdbmsSession = getSession();


        try {
            WlrecordDetailsMapper queue = rdbmsSession.getMapper(WlrecordDetailsMapper.class);
            RowBounds rowBounds = new RowBounds(0, 10);
            List<WlrecordDetails> wlrecordDetails = queue.getAllRecords(rowBounds);

            logger.info("WLRecord_Details table record " + wlrecordDetails.toString());
            return wlrecordDetails;

        } catch (Exception e) {
            logger.error("Failed to fetch data from WLRecord_Details table" + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override

    public Object select(Object o) {
        WlrecordDetails wlrecordDetails = new WlrecordDetails(getSession());

        String id = (String) o;
        try {

            wlrecordDetails.setId(id);
            return wlrecordDetails.select();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to fetch WLRecord_Details table data of [" + id + "] \n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;

    }

    @Override

    public int insert(Object o) {
        RDBMSSession session = getSession();
        RuleFact ruleFact = (RuleFact) o;
        WlrecordDetails wlRecordDetail = new WlrecordDetails();
        int count=0;
        try {

            ruleFact.getName().forEach(name -> {
                try {
                    wlRecordDetail.setParameterType("name");
                    wlRecordDetail.setParameterValue(name);
                    wlRecordDetail.setListName(ruleFact.getList());
                    wlRecordDetail.setStatus("F");
                    wlRecordDetail.setId(String.valueOf(System.currentTimeMillis()));
                    wlRecordDetail.insert(session);

                } catch (RdbmsException rdbms) {
                    logger.error("Failed to insert the data for name"+rdbms.getMessage());
                    //System.out.println("Failed to insert the data for name"+rdbms.getMessage());
                }

            });

            ruleFact.getNic().forEach(nic -> {
                try {
                    wlRecordDetail.setParameterType("nic");
                    wlRecordDetail.setParameterValue(nic);
                    wlRecordDetail.setListName(ruleFact.getList());
                    wlRecordDetail.setStatus("F");
                    wlRecordDetail.setId(String.valueOf(System.currentTimeMillis()));
                    wlRecordDetail.insert(session);
                } catch (RdbmsException rdbms) {
                    logger.error("Failed to insert the data for nic"+rdbms.getMessage());
                }
            });
            ruleFact.getPassport().forEach(passport -> {
                try {
                    wlRecordDetail.setParameterType("passport");
                    wlRecordDetail.setParameterValue(passport);
                    wlRecordDetail.setListName(ruleFact.getList());
                    wlRecordDetail.setStatus("F");
                    wlRecordDetail.setId(String.valueOf(System.currentTimeMillis()));
                    wlRecordDetail.insert(session);
                } catch (RdbmsException rdbms) {
                    logger.error("Failed to insert the data for passport"+rdbms.getMessage());
                }
            });


            session.commit();


        } catch (RdbmsException rdbms) {
            logger.error("Failed to insert into WLRecord_Details " + wlRecordDetail.toString() + "\n " + rdbms.getMessage());
        } finally {
            closeRDMSSession();
        }

        return 0;
    }


    @Override


    public int update(Object o) {
        RDBMSSession session = getSession();
        int count = 0;
        WlrecordDetails wlrecordDetails = (WlrecordDetails) o;
        try {
            count = wlrecordDetails.update(session);

            session.commit();

        } catch (RdbmsException rdbms) {
            logger.error("Failed to update WLRecord_Details" + wlrecordDetails.toString());
            logger.error("Message " + rdbms.getMessage() + " Cause " + rdbms.getCause());
        } finally {
            closeRDMSSession();
        }
        return count;
    }

    /**
     * @return return fresh  record from WLRecord_Details table
     */
    public List<WlRecordDeatailsSummary> getFreshRecord() {
        RDBMSSession rdbmsSession = getSession();

        try {

            WlrecordDetailsMapper mapper = rdbmsSession.getMapper(WlrecordDetailsMapper.class);
            WlRecordDeatailsCriteria criteria = new WlRecordDeatailsCriteria();
            criteria.setStatus("F");

            return mapper.searchWlRecordDeatails(criteria);

        } catch (Exception e) {
            logger.error("failed to fetch pending  record from WLRecord_Details table " + e.getMessage());
        } finally {
            closeRDMSSession();
        }
        return null;
    }

    @Override

    public int delete(Object o) {
        return 0;
    }


    public int update(String id, String status, String remarks) {
        WlrecordDetails newModifiedCustomers = (WlrecordDetails) select(id);

        newModifiedCustomers.setStatus(status);
        newModifiedCustomers.setRemarks(remarks);
        return update(newModifiedCustomers);
    }

}
