package clari5.custom.jiraevidencformator;


import java.io.IOException;
import java.util.Properties;

import cxps.apex.utils.CxpsLogger;
import clari5.platform.util.Hocon;

import static cxps.apex.utils.FileUtils.loadFromProperties;


class ConfigFileReader {
    private String database_url = null;
    private String db_username = null;
    private String db_password = null;
    private String driver_name = null;
    private String schema_name = null;
    private String customField = null;
    Properties props = null;
    private static CxpsLogger logger = CxpsLogger.getLogger(ConfigFileReader.class);

    /*
     * Following code reads the configuration files 
     * jiradb-module.conf
     */
    public ConfigFileReader(String filename) {
        configure();
    }

    public String getCustomField() {
        return customField;
    }

    public String getDatabase_url() {
        return database_url;
    }

    public String getDb_username() {
        return db_username;
    }

    public String getDb_password() {
        return db_password;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public String getSchema_name() {
        return schema_name;
    }


    public void configure(){
        try {
            Hocon hocon = new Hocon();
            hocon.loadFromContext("jiradb-module.conf");
            database_url = hocon.getString("database_url");
            db_username = hocon.getString("db_username");
            db_password = hocon.getString("db_password");
            driver_name = hocon.getString("driver_name");
            schema_name = hocon.getString("schema_name");
	        customField = hocon.getString("customField");

         System.out.println("database url is : "+database_url);

            logger.debug("\n*** *** *** database_url : " + database_url + "\n" + "db_username : " +db_username + "\n" + "db_password : " +db_password + "\n" + "driver_name : " +driver_name + "\n" + "schema_name : " +schema_name);
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }
}

