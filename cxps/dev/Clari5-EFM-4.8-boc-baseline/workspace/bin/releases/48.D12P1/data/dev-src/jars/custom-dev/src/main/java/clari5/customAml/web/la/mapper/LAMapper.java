package clari5.customAml.web.la.mapper;

import clari5.hfdb.AmlCustomerView;
import java.util.List;

/**
 * Created by cxpsaml on 12/29/14.
 */
public interface LAMapper {

    public List<AmlCustomerView> getDetailsGivenPan(String panCard);

    public List<AmlCustomerView> getDetailsGivenEmail(String email);

    public List<AmlCustomerView> getDetailsGivenMobile(String mobile);

    public List<AmlCustomerView> getAllDetailsOfACustomer(String custId);
}

