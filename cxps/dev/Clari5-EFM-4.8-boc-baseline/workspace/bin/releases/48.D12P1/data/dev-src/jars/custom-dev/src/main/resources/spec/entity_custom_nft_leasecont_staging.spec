clari5.hfdb.entity.nft-leasecont {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : SYNC_STATUS , type:"string:100"}
  { name = event_name, type = "string:100" }
  { name = eventtype, type = "string:100" }
  { name = eventsubtype, type = "string:100" }
  { name = host_user_id, type = "string:20" }
  { name = host_id, type = "string:20" }
  { name = channel, type = "string:20" }
  { name = keys, type = "string:20" }
  { name = source, type = "string:20" }
  { name = reservedfield1, type = "string:20" }
  { name = reservedfield2, type = "string:20" }
  { name = reservedfield3, type = "string:20" }
  { name = reservedfield4, type = "string:100" }
  { name = reservedfield5, type = "string:100" }
  { name = cust_id, type = "string:20" }
  { name = cx_cust_id, type = "string:20" }
  { name = cx_acct_id, type = "string:20" }
  { name = acid, type = "string:20" }
  { name = sys_time, type = "date" }
  { name = stakeholder, type = "string:20" }
  { name = stakeholder_id, type = "string:20" }
  { name = lease_facility_amount, type = "decimal:11,2" }
  { name = customer_contribution, type = "decimal:11,2" }
  { name = lease_creation_date, type = "date" }
  { name = nic, type = "string:100" }
  { name = br, type = "string:100" }

   ]
}
