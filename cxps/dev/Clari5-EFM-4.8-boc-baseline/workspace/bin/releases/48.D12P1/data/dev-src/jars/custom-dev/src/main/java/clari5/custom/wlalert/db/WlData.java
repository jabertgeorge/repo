package clari5.custom.wlalert.db;

public class WlData {
    private String id;
    private WlrecordDetails wlrecordDetails;

    public WlData(WlrecordDetails newModifiedCustomers,String id){
        this.id=id;
        this.wlrecordDetails=newModifiedCustomers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public WlrecordDetails getWlrecordDetails() {
        return wlrecordDetails;
    }

    public void setWlrecordDetails(WlrecordDetails wlrecordDetails) {
        this.wlrecordDetails = wlrecordDetails;
    }





    public String toString(){
        return "WlRecord{" +
                "id='" + id + '\'' +
                ", wlrecordDetails=" + wlrecordDetails.toString() +
                '}';
    }
}
