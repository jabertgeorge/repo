package clari5.custom.dev;

import clari5.custom.dev.Script.ExecuteScript;
import clari5.custom.dev.daemons.MTDaemon;
import clari5.custom.dev.event.generator.EventMessage;
import clari5.custom.dev.event.generator.MTEventGenerator;
import clari5.platform.util.CxJson;
import clari5.platform.util.CxRest;
import clari5.platform.util.Hocon;
import com.prowidesoftware.swift.io.parser.SwiftParser;
import com.prowidesoftware.swift.model.SwiftMessage;
import cxps.apex.utils.CxpsLogger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Stream;

public class ProcessSwiftFile {

    static Hocon eventHocon;
    static Hocon mtHocon;
    public static Hocon filePath;
    public static CxpsLogger logger = CxpsLogger.getLogger(ProcessSwiftFile.class);

    static {
        eventHocon = new Hocon();
        mtHocon = new Hocon();
        filePath = new Hocon();
        eventHocon.loadFromContext("ft_remittanceEvent.conf");
        mtHocon.loadFromContext("mt_message.conf");
        filePath.loadFromContext("mt_file_path.conf");
    }

    public Map<String, List<SwiftMessage>> parseFile(String path) throws IOException {
        Map<String, List<SwiftMessage>> messages = new HashMap<String, List<SwiftMessage>>();
        File dir = new File(path);
        if (!dir.exists())
            throw new FileNotFoundException("Directory " + path + " does not exist!!");
        for (File f : dir.listFiles()) {
            if (f.isFile()) {
                Scanner scanner = new Scanner(f);
                StringBuffer sb = new StringBuffer();
                while (scanner.hasNextLine()) {
                    // check for start and stop to make a message
                    String line = scanner.nextLine();
                    if (line.trim().length() <= 0) continue;
                    //check for beginning
                    if (line.indexOf("-begin-") >= 0) {
                        sb.append(line.substring(7, line.length())).append("\n");
                    } else if (line.indexOf("-end-") >= 0) {
                        sb.append(line.substring(0, line.length() - 5));
                        //create the message object nd store it in the map
                        SwiftMessage message = new SwiftParser().parse(sb.toString());
                        if (message.isServiceMessage()){
                            message = new SwiftParser().parse(message.getUnparsedTexts().getAsFINString());
                        }
                        if (messages.containsKey(message.getType())) {
                            if (null != messages.get(message.getType())) {
                                messages.get(message.getType()).add(message);
                            } else {
                                List<SwiftMessage> messageList = new ArrayList<>();
                                messageList.add(message);
                                messages.put(message.getType(), messageList);
                            }
                        } else {
                            List<SwiftMessage> messageList = new ArrayList<>();
                            messageList.add(message);
                            messages.put(message.getType(), messageList);
                        }
                        sb = new StringBuffer();
                    } else {
                        sb.append(line).append("\n");
                    }
                }
                scanner.close();
                String destinationPath = filePath.getString("file_path.mtPathArchive");
                Files.move(f.toPath(), Paths.get(destinationPath + "/" + f.getName() + System.currentTimeMillis()));
            } else {
                continue;
            }
        }
        return messages;
    }

    public static void createDir() {
        File mtMsg = new File(filePath.getString("file_path.mtPath"));
        if (!mtMsg.exists()) {
            mtMsg.mkdir();
        }

        File archive = new File(filePath.getString("file_path.mtPathArchive"));
        if (!archive.exists()) {
            archive.mkdir();
        }
    }

    public Map<String, String> getEventMap(SwiftMessage message) {
        return new MTEventGenerator().getEventFromMessage(message);
    }

    public static void process() throws IOException, ParseException {
        ExecuteScript.createDirectory();
        Process p = ExecuteScript.execute();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String path = null;
        if (p != null) {
            path = ExecuteScript.scriptPath.getString("path.converted");
            if (path != null && !path.isEmpty()) {
                ExecuteScript.modifyFileName(path);
            } else {
                logger.info("Please provide correct path inside script_execute.conf file");
            }
        }
        ProcessSwiftFile reader = new ProcessSwiftFile();
        createDir();
        String filePathString = filePath.getString("file_path.mtPath");
        Map<String, List<SwiftMessage>> messages = reader.parseFile(filePathString);

        //System.out.println("data inside messages map is: " + messages);
        for (String key : messages.keySet()) {
            for (SwiftMessage message : messages.get(key)) {
                EventMessage eMessage = new EventMessage();
                eMessage.messageMap(reader.getEventMap(message), message);
            }
        }
    }
    /*public static void main(String[] args) throws IOException, ParseException {
        ProcessSwiftFile reader = new ProcessSwiftFile();
        String filePathString = filePath.getString("file_path.mtPath");
        Map<String, List<SwiftMessage>> messages = reader.parseFile(filePathString);

        for (String key : messages.keySet()) {
            for (SwiftMessage message : messages.get(key)) {
                EventMessage eMessage = new EventMessage();
                eMessage.messageMap(reader.getEventMap(message), message);
            }
        }
    }*/
}