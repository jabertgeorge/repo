# Generated Code
cxps.noesis.glossary.entity.MEMO_RELATIONSHIP {
	db-name = MEMO_RELATIONSHIP
	generate = false
	db_column_quoted = true
	
	tablespace = CXPS_USERS
	attributes = [ 
		{ name = CUSTOMERCIF1, column = CUSTOMERCIF1, type = "string:250", key=true }
		{ name = CUSTOMERCIF2, column = CUSTOMERCIF2, type = "string:250", key=true }
		{ name = RELATIONSHIP, column = RELATIONSHIP, type = "string:250", key=false }
		{ name = RELATIONSHIP_LINKAGE_NO, column = RELATIONSHIP_LINKAGE_NO, type = "decimal:7,2", key=false }
		{ name = RELATIONSHIP_START_DATE, column = RELATIONSHIP_START_DATE, type = "decimal:7,2", key=false }
		{ name = RELATIONSHIP_END_DATE, column = RELATIONSHIP_END_DATE, type = "decimal:7,2", key=false }
	]
	indexes {
        ICK_MEMO_RELATIONSHIP_pe = [ CUSTOMERCIF1 ]
    }
}
