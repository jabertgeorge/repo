package cxps.events;

import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;

import java.util.Map;
import java.util.Set;

/**
 * Created by anshul on 12/10/15.
 */
public class TR_MarketChangeEvent extends Event {

    String instrumentId;
    Double currentTradedValue;

    @Override
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);

        setInstrumentId((String) msgMap.get("instrument-id"));
        setCurrentTradedValue((String) msgMap.get("current-traded-value"));
    }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        return null;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public double getCurrentTradedValue() {
        return currentTradedValue;
    }

    public void setCurrentTradedValue(String currentTradedValue) {
        try {
            double curTradedVal = Double.parseDouble(currentTradedValue);
            this.currentTradedValue = curTradedVal;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:currentTradedValue>value:" + currentTradedValue);
        }
    }
}
