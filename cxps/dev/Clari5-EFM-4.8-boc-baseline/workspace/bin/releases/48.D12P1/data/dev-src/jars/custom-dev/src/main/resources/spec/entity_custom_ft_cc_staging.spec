clari5.hfdb.entity.ft-cc-txn {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : SYNC_STATUS , type:"string:100"}
   { name = event_name, type = "string:100" }
   { name = eventtype, type = "string:100" }
   { name = eventsubtype, type = "string:100" }
   { name = host_user_id, type = "string:20" }
   { name = host_id, type = "string:20" }
   { name = channel, type = "string:20" }
   { name = keys, type = "string:20" }
   { name = cardnumber, type = "string:20" }
   { name = cardtype, type = "string:20" }
   { name = country_of_transaction, type = "string:20" }
   { name = tran_date, type = "timestamp" }
   { name = tran_amt, type = "decimal:11,2" }
   { name = lcy_amt, type = "decimal:11,2" }
   { name = lcy_curr, type = "string:11" }
   { name = tran_code, type = "string:20" }
   { name = pstd_flg, type = "string:20" }
   { name = account_id, type = "string:20" }
   { name = eod_credit_balance, type = "decimal:11,2" }
   { name = eod_credit_currency, type = "string:11" }
   { name = avl_cr_limit, type = "decimal:11,2" }
   { name = part_tran_type, type = "string:20" }
   { name = merchantcode, type = "string:20" }
   { name = merchantid, type = "string:20" }
   { name = part_tran_srl_num, type = "string:20" }
   { name = countrycode_for_transaction, type = "string:20" }
   { name = tran_type, type = "string:20" }
   { name = tran_sub_type, type = "string:20" }
   { name = tran_id, type = "string:20" }
   { name = tran_curr, type = "string:20" }
   { name = sys_time, type = "date" }
   { name = cust_id, type = "string:20" }
   { name = cx_cust_id, type = "string:20" }
   { name = cx_acct_id, type = "string:20" }
   { name = online_batch, type = "string:20" }
   { name = eventts, type = "date" }
   { name = source, type = "string:20" }
   { name = reservedfield1, type = "string:20" }
   { name = reservedfield2, type = "string:20" }
   { name = reservedfield3, type = "string:20" }
   { name = reservedfield4, type = "string:100" }
   { name = reservedfield5, type = "string:100" }

   ]
}
