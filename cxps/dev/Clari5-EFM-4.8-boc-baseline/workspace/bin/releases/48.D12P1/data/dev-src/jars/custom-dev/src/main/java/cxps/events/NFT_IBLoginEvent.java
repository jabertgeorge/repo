package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
public class NFT_IBLoginEvent extends Event{
    private static final long serialVersionUID = -1151475588611495187L;
    private String source;
    private Date sysTime;
    private String keys;
    private String userId;
    private String custId;
    private String deviceId;
    private String succFailFlg;
    private String errorCode;
    private String addrNetwork;
    private String errorDesc;
    private String timeSlot;
    private String deviceType;
    private String loginStatus;
    private Date loginTs;
    private String lastLoginIp;
    private Date lastLoginTs;
    private String mfaType;
    private String country;
    private String entity1;
    private String entity2;
    private String entity3;
    private String entity4;
    private String entity5;
    private String entity6;
    private String entity7;
    private String entity8;
    private String entity9;
    private String entity10;

    public String getCountry() {
        return this.country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getEntity1() {
        return this.entity1;
    }
    public void setEntity1(String entity1) {
        this.entity1 = entity1;
    }
    public String getEntity2() {
        return this.entity2;
    }
    public void setEntity2(String entity2) {
        this.entity2 = entity2;
    }
    public String getEntity3() {
        return this.entity3;
    }
    public void setEntity3(String entity3) {
        this.entity3 = entity3;
    }
    public String getEntity4() {
        return this.entity4;
    }
    public void setEntity4(String entity4) {
        this.entity4 = entity4;
    }
    public String getEntity5() {
        return this.entity5;
    }
    public void setEntity5(String entity5) {
        this.entity5 = entity5;
    }
    public String getEntity6() {
        return this.entity6;
    }
    public void setEntity6(String entity6) {
        this.entity6 = entity6;
    }
    public String getEntity7() {
        return this.entity7;
    }
    public void setEntity7(String entity7) {
        this.entity7 = entity7;
    }
    public String getEntity8() {
        return this.entity8;
    }
    public void setEntity8(String entity8) {
        this.entity8 = entity8;
    }
    public String getEntity9() {
        return this.entity9;
    }
    public void setEntity9(String entity9) {
        this.entity9 = entity9;
    }
    public String getEntity10() {
        return this.entity10;
    }
    public void setEntity10(String entity10) {
        this.entity10 = entity10;
    }
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    public Date getSysTime() {
        return sysTime;
    }
    public void setSysTime(Date sysTime) {
        this.sysTime = sysTime;
    }
    public void setSysTime(String sysTime) {
        if (sysTime == null || "".equals(sysTime.trim())) {
            throw new IllegalArgumentException("Value is required: sysTime");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                this.sysTime = dateFormat1.parse(sysTime);
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide sysTime in the format:dd-MM-yyyy HH:mm:ss.SSS.Invalid Value>field:sysTime>value:" +sysTime);
            }
        }
    }
    public void setEventTSStr(String eventts) {
        if (eventts == null || "".equals(eventts.trim())) {
            throw new IllegalArgumentException("Value is required: eventts");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                setEventTS(dateFormat1.parse(eventts));
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide eventts in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:eventts>value:" +eventts);
            }
        }
    }
    public String getKeys() {
        return keys;
    }
    public void setKeys(String keys) {
        this.keys = keys;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getCustId() {
        return custId;
    }
    public  void setCustId(String custId) {
        if (custId == null || "".equals(custId.trim())){
            throw new IllegalArgumentException("Value is required: CustId");
        } else {
            this.custId = custId;
        }
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public String getSuccFailFlg() {
        return succFailFlg;
    }
    public void setSuccFailFlg(String succFailFlg) {
        this.succFailFlg = succFailFlg;
    }
    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    public String getAddrNetwork() {
        return addrNetwork;
    }
    public void setAddrNetwork(String addrNetwork) {
        this.addrNetwork = addrNetwork;
    }
    public String getErrorDesc() {
        return errorDesc;
    }
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
    public String getTimeSlot() {
        return timeSlot;
    }
    public String getDeviceType() {
        return deviceType;
    }
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
    public String getLoginStatus() {
        return loginStatus;
    }
    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }
    public Date getLoginTs() {
        return loginTs;
    }
    public void setLoginTs(String loginTs) {
    }
    public void setLoginTs(Date loginTs) {
        this.loginTs = loginTs;
    }
    public String getLastLoginIp() {
        return lastLoginIp;
    }
    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
    public Date getLastLoginTs() {
        return lastLoginTs;
    }
    public String getMfaType() {
        return mfaType;
    }
    public void setMfaType(String mfaType) {
        this.mfaType = mfaType;
    }
    public void setLastLoginTs(String lastLoginTs) {
        try {
            if(lastLoginTs != null && !lastLoginTs.trim().isEmpty()) {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                this.lastLoginTs = dateFormat1.parse(lastLoginTs);
            }
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Format of lastloginTs needed:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:lastLoginTs>value:" +lastLoginTs);
        }
    }
    public void setLastLoginTs(Date lastLoginTs) {
        this.lastLoginTs = lastLoginTs;
    }
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setEventType("NFT");
        setEventSubType("IBLogin");
        setSource((String)msgMap.get("source"));
        setKeys((String)msgMap.get("keys"));
        setUserId((String) msgMap.get("user-id"));
        setCustId((String) msgMap.get("cust-id"));
        setDeviceId((String) msgMap.get("device-id"));
        setAddrNetwork((String)msgMap.get("addr-network"));
        setSuccFailFlg((String) msgMap.get("succ-fail-flg"));
        setErrorCode((String)msgMap.get("error-code"));
        setErrorDesc((String) msgMap.get("error-desc"));
        setDeviceType((String)msgMap.get("device-type"));
        setLoginStatus((String)msgMap.get("login-status"));
        setLoginTs((String)msgMap.get("login-ts"));
        setLastLoginIp((String)msgMap.get("last-login-ip"));
        setLastLoginTs((String)msgMap.get("last-login-ts"));
        setMfaType((String)msgMap.get("mfa-type"));
        setCountry((String)msgMap.get("country"));
        setEntity1((String)msgMap.get("entity1"));
        setEntity2((String)msgMap.get("entity2"));
        setEntity3((String)msgMap.get("entity3"));
        setEntity4((String)msgMap.get("entity4"));
        setEntity5((String)msgMap.get("entity5"));
        setEntity6((String)msgMap.get("entity6"));
        setEntity7((String)msgMap.get("entity7"));
        setEntity8((String)msgMap.get("entity8"));
        setEntity9((String)msgMap.get("entity9"));
        setEntity10((String)msgMap.get("entity10"));
    }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        String hostCustKey = this.getCustId();

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
        String cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, this.getHostId(), hostCustKey);

        WorkspaceInfo wi = new WorkspaceInfo(cxps.noesis.constants.Constants.KEY_REF_TYP_CUST, cxCifId);
        wi.addParam("cxCifID", cxCifId);

        wsInfoSet.add(wi);
       return wsInfoSet;
    }

    public JSONObject getFRJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("event_id",getEventId());
        jsonObject.put("cust_id",getCustId());
        jsonObject.put("event-name",getEventName());
        jsonObject.put("sys_time",getEventTS().getTime());
        jsonObject.put("EventType",getEventType());
        jsonObject.put("EventSubType",getEventSubType());

        return jsonObject;
    }
}

