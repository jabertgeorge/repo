clari5.hfdb.entity.SAFEDEP-MASTER {
        attributes:[
		{ name: Locker-Number,type:"string:20",key=true }
		{ name: CORE-CIFID-1, type:"string:20",key=false }
		{ name: CORE-CIFID-2, type="string:20",key=false }
		{ name: CORE-CIFID-3, type="string:20",key=false }
		{ name: BranchCode, type="string:20",key=false }
		{ name: Locker-Opened-Date, type=timestamp,key=false }
		{ name: Closed-Date, type=timestamp,key=false }
		{ name: Linked-FD-number, type="string:20",key=false }
		{ name: Anul-Rntl-Rec-Acct, type="number:20,2",key=false }
		{ name: NIC, type="string:20",key=false }
		{ name: Status, type="string:20",key=false }
		{ name: Cmnt-Code-1, type="string:50",key=false }
		{ name: Cmnt-Code-2, type="string:50",key=false }
		{ name: Cmnt-Code-3, type="string:50",key=false }
		{ name: Cmnt-Code-4, type="string:50",key=false }
		{ name: Cmnt-Code-5, type="string:50",key=false }
     ]
}
