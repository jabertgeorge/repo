/* case.js start */
var caseSrch="";
var jsonObj;
var jsonArray=new Array();
var caseOutput="";
var treeId="caseContainer";

caseSrch="<div class='heading' style='width:100%;text-align:left;'><h3>Case Search</h3></div>";

<!-- Case Search -->
caseSrch+="<form id='ioTemplate'><table  id='caseListD' cellpadding='3' cellspacing='3'></br>"
	+"<tr class='separator'><td class='fontSize'>Entity ID/Customer ID </td></tr>"
	+"<tr class='separator'><td ><input type='text' class='validate[required] fontSize' value='' id='entyId'/></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Case Open From date</td></tr>"
	+"<tr class='separator'><td><input type='text' id='fromDate' class='validate[required] custom[date] text-input datepicker fontSize'></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Case Open To date</td></tr>"
	+"<tr class='separator'><td><input type='text' id='toDate' class='validate[required] custom[date] text-input datepicker fontSize'></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Case Status </td></tr>"
	+"<tr class='separator'><td><select id='cstatus' class='fontSize' multiple='multiple' style='height:40px;'><option value='Open' >Open</option><option value='Close' >Close</option></select></td></tr>"
	+"<tr class='separator'><td class='fontSize'>Case Resolution </td></tr>"
	+"<tr class='separator'><td><select class='fontSize' id='cresol' multiple='multiple' style='height:40px;'><option value='FALSE_POSITIVE' >False Positive</option><option value='WHITE_LISTED' >Watch List</option><option value='Suspicious Transaction' >Suspicious Transaction</option></select></td></tr>"
	+"<tr style='height:30px;'></tr><tr class='separator'><td ><input id='caseBtSrch' onclick=\"loadCaseData(this)\" type='button' class='button' value='Search' /></td></tr></table></form>";

<!-- Data table of loading cases -->
caseOutput="<div id=\"caseSrch\" ><div class=\"heading\" style='text-align:left;'><h3>Case Details</h3></div><div id='tabCase' style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><table id='chfTable'  class='dataTableCss' style='text-align:left;'></table></div></div>";

var caseTreeData="";

function initCase()
{
	var userId = top.user;
	var serverUrl = top.serverUrl;
	setParams(userId, serverUrl);
	$("#incdntLink").hide();
	$("#fromDate").datepicker();
	$("#toDate").datepicker();
	$("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
	$("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
	$("#toDate").datepicker( "option", "maxDate", new Date());
	$("#caseSrch").hide();
	$("#gridTabs").tabs();
}

function loadCaseData(custId)
{
	toggleSrchWindow();
	var fromDate =($("#fromDate").val()).split("/").reverse().join("-");
	var toDate =($("#toDate").val()).split("/").reverse().join("-");
	var cstatus=$("#cstatus").val();
	var cresol=$("#cresol").val();
	var entyId=$("#entyId").val();
	if(validateFormFields("ioTemplate")){
	var inputObject={
	    "entityKey": entyId,
	    "openedDateMin": fromDate,
	    "openedDateMax": toDate,
		"contentType" : "json"
	};
	addTab('Case','');
	getCaseDetails(inputObject,caseRespHandler,true);

	}
}

var inputObj="";
var inputObject="";
var caseCount="";
var caseId="";
var caseObj="";
function caseLoad(caseID,ctabCount,moduleId){
	caseId=caseID;
	caseCount=ctabCount;
	$("#menuBlock").hide();
	if($("#entyId").val()!=""){
        treeObj={
             "custId":$("#entyId").val()
             };
	inputObject={
   	    "entityKey": $("#entyId").val()
   	};
	}
    inputObj={
        "caseId":caseID,
	"jiraId":caseID,
	"moduleId":moduleId,
		"contentType":"json"
    };
    getJiraCaseDetails(inputObj,caseJiraRespHandler,true);
}

function loadClosedIncidentDetails(tabid){
    caseCount=tabid;
	var cobj={
            "caseId":caseId,
		"contentType":"json"
	 };
    getClosedIncidentDtls(cobj,incidentCloseRespHandler,true);
}

function loadOpenIncidentDetails(tabid){
	caseCount=tabid;
	var cobj={
            "caseId":caseId,
		"contenType":"json"
	 };
	getOpenIncidentDtls(cobj,incidentOpenRespHandler,true);
}
function caseJiraRespHandler(resp){
	var jiraObj=resp.getCaseOutputList;
	if(jiraObj!=null || jiraObj!=undefined){
	$("#caseOpnDt_"+caseCount).text(jiraObj[0].openedDate);
	$("#custIdVal_"+caseCount).empty();
	 treeObj={
	     "custId":jiraObj[0].entityKey
	     };
	inputObject={
	    "entityKey":jiraObj[0].entityKey
	};
	$("#custIdVal_"+caseCount).append("<a href='#' style='color:#7070FF;' oncontextmenu='return false;' onclick=\"addTab('Customer','"+jiraObj[0].entityKey+"')\" >"+jiraObj[0].entityKey+"</a>");
	//$("#summary_"+caseCount).text(jiraObj[0].summary);
	$("#caseStatus_"+caseCount).text(jiraObj[0].status);
    $("#caseOpnDt_"+caseCount).text(jiraObj[0].openedDate);
   // $("#viewInvest_"+caseCount).append("<a href='#' style='color:#7070FF;' oncontextmenu='return false;' onclick=\"openJiraLink('"+jiraObj[0].jiraUrl+"');\" >View Investigation Details</a>");
    $("#viewInvest_"+caseCount).append("<a href='#' style='color:#7070FF;' oncontextmenu='return false;' onclick=\"openJiraLink('"+jiraObj[0].jiraUrl+"');\" ></a>");
	}
	var csObj={
            "caseId":caseId,
		"contenType":"json"
	 };
	//getCustomTreeDetail(treeObj,custTreeRespHandler,true);
	getCustomerTreeDetails(treeObj,custTreeRespHandler,true);
	getOpenIncidentDtls(csObj,incidentOpenRespHandler,true);
}

function openJiraLink(jiraUrl){
	window.open(jiraUrl,'JIRA', 'height=950, width=1200, toolbar=no, directories=no, status=yes, menubar=yes, scrollbars=yes, resizable=yes, modal=yes');
}

function incidentOpenRespHandler(resp){
        var response=resp.incidentsList;
        if(response=="" || response==undefined){
           var dataResp = [ ];
           var myArray =new Array();
           var tableHeaders = new Array();
           tableHeaders.push({ "sTitle": "Incident ID" });
           tableHeaders.push({ "sTitle": "Created Date" });
           tableHeaders.push({ "sTitle": "Status" });
           tableHeaders.push({ "sTitle": "Intelligence" });
           tableHeaders.push({ "sTitle": "Value" });
           tableHeaders.push({ "sTitle": "Risk Rating" });
          // tableHeaders.push({ "sTitle": "Closed Date" });
           dataResp.push(myArray);
           dataResp.push(tableHeaders);
           displayData(dataResp,"incCase"+caseCount,"incdntDtls"+caseCount);
        }else{
            var res=response;
            for(var i=0;i<res.length;i++)
            {
             delete res[i]['closedDate'];
            }
   	    var resList = makeJsonForDatatable(res,"","","");
	    displayData(resList,"incCase"+caseCount,"incdntDtls"+caseCount);
        }
}

function incidentCloseRespHandler(resp){
        var response=resp.incidentsList;
        if(response=="" || response==undefined){
           var dataResp = [ ];
           var myArray =new Array();
           var tableHeaders = new Array();
           tableHeaders.push({ "sTitle": "Incident ID" });
           tableHeaders.push({ "sTitle": "Created Date" });
           tableHeaders.push({ "sTitle": "Status" });
           tableHeaders.push({ "sTitle": "Intelligence" });
           tableHeaders.push({ "sTitle": "Value" });
           tableHeaders.push({ "sTitle": "Risk Rating" });
           tableHeaders.push({ "sTitle": "Closed Date" });
           dataResp.push(myArray);
           dataResp.push(tableHeaders);
           displayData(dataResp,"othCase"+caseCount,"othCaseDtls"+caseCount);
        }else{
   	    var resList = makeJsonForDatatable(response,"","","");
	    displayData(resList,"othCase"+caseCount,"othCaseDtls"+caseCount);
        }
}

function getCaseTreeData(xCount,id){
caseTreeData="";
custRmlId="customerRelam"+xCount;
caseTreeData=  "<div id=\"caseTree"+xCount+"\" class='treeLeftPanel' ><div id=\"caseContainer"+xCount+"\" style='margin-left:-6px;overflow-x:auto;height:100%;'></div></div><div class='treeRightPanel' ><div><button onclick='goBack(id)' class='abutton back-w-def goBackCls' id="+id+" style='cursor:hand;opacity:1;'>Back</button></div><div id=\"caseLink"+xCount+"\" style='width:101.3%'><div class='heading'><h3>Case Details</h3></div><table class='attrHTbl' >"+
               "<tr><td>Customer</td><td>:</td><td id=\"custIdVal_"+xCount+"\"></td><td style='border-left:1px solid #c0c0c0;' >Case Open date</td><td>:</td><td id=\"caseOpnDt_"+xCount+"\" ></td></tr>"+
               "<tr><td>Case Status</td><td>:</td><td id=\"caseStatus_"+xCount+"\"></td><td style='border-left:1px solid #c0c0c0;' id=\"viewInvest_"+xCount+"\"></td></tr></table></div>";
               //"<tr><td>Summary</td><td>:</td><td colspan='5' id=\"summary_"+xCount+"\" style='width: 336px; height: 25px;' ></td></tr><tr style='height:10px'></tr></table></div>";

caseTreeData+="</br><div id=\"caseMenu"+xCount+"\" style='width:101.3%;margin-top:-5px;' ><ul id=\"caseBar"+xCount+"\" ><li><a onClick=\"loadOpenIncidentDetails('"+xCount+"')\" href=\"#casetab1_"+xCount+"\" oncontextmenu='return false;' >Open Incidents</a></li><li><a onClick=\"loadClosedIncidentDetails('"+xCount+"')\" href=\"#casetab2_"+xCount+"\" oncontextmenu='return false;' >Closed Incidents</a></li></ul>"
               +"<div id=\"casetab2_"+xCount+"\" style='width: 99%; height:69%;background:#FFFFFF;overflow-y:auto;' ><div id='othCaseLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Closed Incidents</h3></div><div id=\"othCase"+xCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><br/><table id=\"othCaseDtls"+xCount+"\" class='dataTableCss' style='text-align:left;' ></table></div></div></div>"
               +"<div id=\"casetab1_"+xCount+"\" style='width: 99%; height:69%;background:#FFFFFF;overflow-y:auto;' ><div id='incdntLink' style=\"margin:10px 0 15px 0; border:1px solid #808080; border-radius:0 10px 7px 7px;\"><div class=\"heading\" ><h3>Open Incidents</h3></div><div id=\"incCase"+xCount+"\" style='width:100%; margin:auto; margin-top:10px; margin-bottom:25px;' ><br/><table id=\"incdntDtls"+xCount+"\" class='dataTableCss' style='text-align:left;'></table></div></div></div></div></div>";
return caseTreeData;
}

function getJsonData(inp){
        var myArray = [ ];
        var xyz = [ ];
            for(var i=0; i < inp.length; i++){
                 xyz = [ ];
		    for(key in inp[i]) {
		       jsonArray[key]=inp[i][key];
            }
			myArray.push(xyz);
	    }
    return myArray;
}

function validateFormFields(formName)
{	
	return $("#"+formName).validationEngine('validate');

}
/* case.js end */
