// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="NFT_STATIC_INFO_CHANGE_EVENT_TBL", Schema="rice")
public class NFT_StaticInfoChangeEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String entityType;
       @Field(size=20) public String acctId;
       @Field(size=20) public String solid;
       @Field(size=20) public String userId;
       @Field(size=20) public String initSubEntityVal;
       @Field(size=20) public String eventTime;
       @Field(size=20) public String channelId;
       @Field(size=20) public String custId;
       @Field(size=20) public String channelType;
       @Field(size=20) public String refType;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String refId;
       @Field(size=20) public String entitySubType;
       @Field(size=20) public String channelDesc;
       @Field(size=20) public String hostUserId;
       @Field(size=100) public String hostId;
       @Field(size=20) public String finalSubEntityVal;
       @Field(size=20) public String entityId;
       @Field(size=20) public String status;


    @JsonIgnore
    public ITable<NFT_StaticInfoChangeEvent> t = AEF.getITable(this);

	public NFT_StaticInfoChangeEvent(){}

    public NFT_StaticInfoChangeEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("StaticInfoChange");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setEntityType(json.getString("EntityType"));
            setAcctId(json.getString("acctId"));
            setSolid(json.getString("solid"));
            setUserId(json.getString("userId"));
            setInitSubEntityVal(json.getString("initSubEntityVal"));
            setEventTime(json.getString("eventTime"));
            setChannelId(json.getString("ChannelID"));
            setCustId(json.getString("custId"));
            setChannelType(json.getString("ChannelType"));
            setRefType(json.getString("ref_type"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setRefId(json.getString("ref_id"));
            setEntitySubType(json.getString("EntitySubType"));
            setChannelDesc(json.getString("ChannelDesc"));
            setHostUserId(json.getString("HostUserId"));
            setHostId(json.getString("hostId"));
            setFinalSubEntityVal(json.getString("finalSubEntityVal"));
            setEntityId(json.getString("EntityId"));
            setStatus(json.getString("status"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NSIC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getEntityType(){ return entityType; }

    public String getAcctId(){ return acctId; }

    public String getSolid(){ return solid; }

    public String getUserId(){ return userId; }

    public String getInitSubEntityVal(){ return initSubEntityVal; }

    public String getEventTime(){ return eventTime; }

    public String getChannelId(){ return channelId; }

    public String getCustId(){ return custId; }

    public String getChannelType(){ return channelType; }

    public String getRefType(){ return refType; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getRefId(){ return refId; }

    public String getEntitySubType(){ return entitySubType; }

    public String getChannelDesc(){ return channelDesc; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public String getFinalSubEntityVal(){ return finalSubEntityVal; }

    public String getEntityId(){ return entityId; }

    public String getStatus(){ return status; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setEntityType(String val){ this.entityType = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setSolid(String val){ this.solid = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setInitSubEntityVal(String val){ this.initSubEntityVal = val; }
    public void setEventTime(String val){ this.eventTime = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setChannelType(String val){ this.channelType = val; }
    public void setRefType(String val){ this.refType = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setRefId(String val){ this.refId = val; }
    public void setEntitySubType(String val){ this.entitySubType = val; }
    public void setChannelDesc(String val){ this.channelDesc = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setFinalSubEntityVal(String val){ this.finalSubEntityVal = val; }
    public void setEntityId(String val){ this.entityId = val; }
    public void setStatus(String val){ this.status = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getRef_type(){ return refType; }
    @JsonIgnore
    public java.sql.Timestamp getTran_date(){ return tranDate; }
    @JsonIgnore
    public String getRef_id(){ return refId; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_StaticInfoChangeEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_StaticInfoChange");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "StaticInfoChange");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}