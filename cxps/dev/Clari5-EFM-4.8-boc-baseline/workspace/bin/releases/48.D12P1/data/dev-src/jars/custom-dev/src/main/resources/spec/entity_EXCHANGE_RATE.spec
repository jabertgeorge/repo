cxps.noesis.glossary.entity.EXCHANGE_RATE {
        attributes = [
                        {name = GCPET, type = "string:350", key = true}
                        {name = GBCODE, type = "number:10"}
			{name = GBBKXR, type = "decimal:20,2"}
        ]
}
