package clari5.custom.wlalert;

import clari5.custom.wlalert.daemon.RuleFact;
import clari5.custom.wlalert.db.IDBOperation;
import clari5.custom.wlalert.db.WlRecordDetail;
import clari5.platform.applayer.Clari5;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;

/**
 * @author lisa
 */
public class FileProcess {
    public static CxpsLogger logger = CxpsLogger.getLogger(FileProcess.class);

    static FileStructure fileStructure = new FileStructure();


    public void getFile() {
        RuleFact ruleFact = null;
        IDBOperation wlRecordDetail = new WlRecordDetail();

        File[] listOfDirectory = fileStructure.getDirList();
      //  System.out.println("listOfDirectory" + Arrays.toString(listOfDirectory));
        boolean status = false;

        for (File file : listOfDirectory) {

            try {

                if (file.isDirectory()) {

               //     System.out.println(" Fileprocess.getFile file.isDirectory()");

                    status = fileunzip(file.getAbsolutePath());
                   // WlRequestPayload.java
                //    System.out.println("Fileprocess.getFile  status [" + status + "] filestructure " + fileStructure.toString());


                    if ((fileStructure.getFileType(file.getName()).equalsIgnoreCase("xml")) && status) {

                        logger.info("reading xml file ");

                        ruleFact = readXmlFile();
                        //logger.debug("Name---->"+ruleFact.getName());
                        //logger.debug("NIC---->"+ruleFact.getNic());
                        //logger.debug("Passport---->"+ruleFact.getPassport());

                        wlRecordDetail.insert(ruleFact);

                  //      System.out.println("xml record  inserted");
                    //    logger.warn("File type obtained is xml");


                    } else if ((fileStructure.getFileType(file.getName()).equalsIgnoreCase("csv")) && status) {

                      //  System.out.println("reading csv file ");

                        wlRecordDetail.insert(readCsvFile());
                        //System.out.println(" csv recorded inserted");

                        //logger.warn("File type obtained is csv");

                    } else if (!status) {
                        logger.error("removing the unzipfile directory , Failed to insert the records in wl_records table as status is false");
                        //System.out.println("removing the unzipfile file directory , Failed to insert the records in wl_records table as status is false");
                        Thread.sleep(300000);
                        try {

                            if (!StringUtils.isNullOrEmpty(fileStructure.getUnzipFilePath())) {
                                FileUtils.deleteDirectory(new File(fileStructure.getUnzipFilePath()));
                            }
                        } catch (Exception io) {

                          //  System.out.println("Failed while deleting the unzip file directory ");
                            logger.error("Failed while deleting the unzip file directory ");
                            io.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Failed to process the file " + fileStructure.toString() + "\n message " + e.getMessage());
                e.printStackTrace();
            } finally {
                if (ruleFact != null)
                    ruleFact.clear();
            }

        }
    }


    public RuleFact readXmlFile() {
        //System.out.println("Fileprocess.readxmlFile filestructure " + fileStructure.toString());

        //RuleFact ruleFact = new RuleFact();
        RuleFact ruleFact;

        File[] xmlFilesList = new File(fileStructure.getUnzipFilePath()).listFiles();


        //System.out.println("Fileprocess.readxmlFile  xml file list" + Arrays.toString(xmlFilesList));
        XmlParser parser = new XmlParser();
        //logger.debug("get unzip file path before sending to xml parser: " +fileStructure.getUnzipFilePath());
        //logger.debug("data at index 0: "+xmlFilesList[0]);
        File xmlFile = xmlFilesList[0];
        ruleFact = parser.xmlparser(fileStructure.getUnzipFilePath(), xmlFile);



        return ruleFact;


    }

    public RuleFact readCsvFile() {

        //System.out.println("FileProcess.readCsvFile filestructure " + fileStructure.toString());

        RuleFact ruleFact = new RuleFact();
        CsvParser csvParser = new CsvParser();

        //logger.debug("csv absolute path: "+fileStructure.getUnzipFilePath());
        File[] csvFileList = new File(fileStructure.getUnzipFilePath()).listFiles();
        //System.out.println("FileProcess.readCsvFIle csvFileLIist " + Arrays.toString(csvFileList));

        for (File csvFile : csvFileList) {
            ruleFact = csvParser.csvparser(fileStructure.getUnzipFilePath(), csvFile);
        }
        //logger.debug("Rule Fact for csv File" + ruleFact.toString());
        return ruleFact;
    }


    public boolean fileunzip(String dir) {
        //System.out.println("Fileprocess.fileunzip dir path  " + dir);
        File dirPath = new File(dir);
        //System.out.println("FilerProcess.fileunzip dir file list" + Arrays.toString(dirPath.list()));

        try {
            if (dirPath.list().length <= 0) return false;

            File[] zipFileList = dirPath.listFiles();

            //UnZip unZip = new UnZip();
            fileStructure.setUnzipFilePath(dir + "/unzip");

            for (File zipFile : zipFileList) {

                String zipFileName = zipFile.getAbsolutePath();

                UnZip unZip = new UnZip();

                if (!unZip.unZipIt(zipFileName, fileStructure.getUnzipFilePath())) {
                    return false;
                }
          //      System.out.println(" back from unzip it ");

                FileUtils.copyFile(zipFile, new File(fileStructure.getIndexingPath() + "/" + zipFile.getName()));
            //    System.out.println("Indexing file path " +fileStructure.getIndexingPath() + "/" + zipFile.getName());

                File archiveFile = new File(fileStructure.getArchiveFilePath());
              //  System.out.println("Archive path " + fileStructure.getArchiveFilePath());

                if (!archiveFile.exists()) {
                    archiveFile.mkdirs();
                }
                String[] rename = zipFile.getName().split("\\.");
                String newZipFileName = rename[0] + "_" + System.currentTimeMillis() + "." + rename[1];

                //System.out.println("FileProcess.getFile newZipFileName " + newZipFileName);

                Files.move(Paths.get(zipFileName), Paths.get(fileStructure.getArchiveFilePath() +"/"+ newZipFileName));

            }
            return true;

        } catch (IOException io) {
            logger.error("Exception  while unzipping the file [" + io.getMessage() + "] Cause [" + io.getCause() + "]");
            io.printStackTrace();
        }

        return false;
    }

    public static void main(String[] args) {
        Clari5.batchBootstrap("rdbms", "efm-clari5.conf");

        logger.info("FileProcess.main  started");
        FileProcess fileProcess = new FileProcess();
        fileProcess.getFile();
        logger.info("FileProcess.main  ended");
    }

}
