clari5.hfdb.entity.high-risk-countries {
    attributes:[
      { name: LAST_MODIFIED_TIME, type: "timestamp"}
      { name: VERSION_NO, type: "number:10"}
      { name: delFlag, type: "string:1"}
      { name: countryName, type: "string:10"}
      { name: blacklistFlag, type:"string:1"}
      { name: countryRiskCategory, type: "string:2"}
      { name: countryCode, type: "string:3", key = true}
	]
}

