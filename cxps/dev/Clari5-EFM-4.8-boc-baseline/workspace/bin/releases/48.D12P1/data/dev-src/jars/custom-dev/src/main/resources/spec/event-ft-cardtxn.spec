cxps.events.event.ft-cardtxn{
table-name : EVENT_FT_CARD_TXN
event-mnemonic : FC
        workspaces : {
        PAYMENTCARD:"card-no"
        NONCUSTOMER:"terminal-id+bin-num"
	NONCUSTOMER:"card-no+merchant-id"
       
}
event-attributes : {
host-id: { type:"string:2"}
tran-date: { type:date}
msg-type: { type:"string:50"}
cust-id: { type:"string:20", fr : true}
card-no: { type:"string:20", fr : true}
proc-code: { type:"string:50"}
tran-amt: { type: "number:20,3"}
txn-reference-no: {type:"string:20"}
mcc-code: { type:"string:10"}
resp-code: { type:"string:10"}
addr-network: {  type:"string:20"}
card-type-flg: { type:"string:2"}
home-crncy-code: { type:"string:20"}
inr-tran-amt: { type: "number:20,3"}
txn-crncy-code: { type:"string:20"}
channel: { type:"string:10"}
interbankcard: { type:"string:2"}
domestic-int-card-flg: { type:"string:2"}
atm-limit: { type:"number:20,3"}
pos-limit: { type:"number:20,3"}
ecom-limit: { type:"number:20,3"}
card-identifier-code: { type:"string:20"}
succ-fail-flag: { type:"string:2"}
pin-code: { type:"string:20"}
merchant-name: { type:"string:50"}
merchant-id: { type:"string:20"}
txn-type: { type:"string:10"}
source: { type:"string:50"}
error-code: { type:"string:50"}
error-description: { type:"string:50"}
terminal-id: { type:"string:20"}
txn-cntry-code: { type:"string:20"}
acq-cntry-code: { type:"string:20"}
pos-entry-mode: { type:"string:20"}
is-wallet-txn: { type:"string:2"}
wallet-type: { type:"string:50"}
apicall-type: { type:"string:20"}
account-id: { type:"string:20"}
bin-num: { type:"string:6"}
avl-bal: {db : true ,raw_name : avl_bal ,type : "number:11,2 "}
}
}

