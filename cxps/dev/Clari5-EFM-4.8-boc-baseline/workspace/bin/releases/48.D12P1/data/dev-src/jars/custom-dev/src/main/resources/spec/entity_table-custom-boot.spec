clari5.hfdb.entity.table-custom-boot {
   attributes:[
   { name: "pur-acct-open-code", type: "string:350" }
   { name: "anticipated-vol", type: "string:350" }
   { name: "exp-mode-of-tran", type: "string:350" }
   { name: "acctid", type: "string:350" }
   { name: "custid", type: "string:350" }
   { name: "created-date", type: "date" }
   { name: "entity-id", type: "number:10", key=true }
]
}