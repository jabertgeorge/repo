clari5.hfdb.entity.ft-loan-txn {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : SYNC_STATUS , type:"string:100"}
    { name = event_name, type = "string:100" }
    { name = eventsubtype, type = "string:100" }
    { name = host_user_id, type = "string:20" }
    { name = host_id, type = "string:20" }
    { name = channel_type, type = "string:20" }
    { name = channel, type = "string:20" }
    { name = keys, type = "string:20  " }
    { name = agent_type, type = "string:20" }
    { name = pstd_user_id, type = "string:20" }
    { name = bank_code, type = "string:20" }
    { name = cust_ref_type, type = "string:20" }
    { name = cx_cif_i_d, type = "string:20" }
    { name = txn_ref_type, type = "string:20" }
    { name = sys_time, type = "date" }
    { name = module_id, type = "string:20" }
    { name = loan_settlement_date, type = "date" }
    { name = tran_particular, type = "string:20" }
    { name = lcy_tran_amt, type = "decimal:11,2" }
    { name = lcy_tran_crncy, type = "string:20" }
    { name = tran_crncy_code, type = "string:20" }
    { name = tran_code, type = "string:20" }
    { name = value_date, type = "date" }
    { name = exception, type = "string:20" }
    { name = pstd_flg, type = "string:20" }
    { name = tran_rmks, type = "string:20" }
    { name = account_id, type = "string:20 " }
    { name = acct_type, type = "string:20 " }
    { name = remitter_name, type = "string:20" }
    { name = beneficiary_name, type = "string:20" }
    { name = instrmnt_type, type = "string:20" }
    { name = clr_bal_amt, type = "decimal:11,2" }
    { name = balance_cur, type = "string:20" }
    { name = un_clr_bal_amt, type = "decimal:11,2" }
    { name = avl_bal, type = "decimal:11,2" }
    { name = avl_bal_lcy, type = "decimal:11,2" }
    { name = eff_avl_bal, type = "decimal:11,2" }
    { name = part_tran_type, type = "string:20" }
    { name = evt_inducer, type = "string:20" }
    { name = ref_num, type = "string:20" }
    { name = zone_date, type = "date" }
    { name = zone_code, type = "string:20" }
    { name = payee_id, type = "string:20" }
    { name = merchant_categ, type = "string:20" }
    { name = part_tran_srl_num, type = "string:20 " }
    { name = tran_category, type = "string:20" }
    { name = acid, type = "string:20" }
    { name = instrmnt_alpha, type = "string:20" }
    { name = tran_type, type = "string:20 " }
    { name = tran_sub_type, type = "string:20" }
    { name = entry_date, type = "date" }
    { name = pstd_date, type = "date" }
    { name = vfd_date, type = "date" }
    { name = txn_br_id, type = "string:20" }
    { name = tran_id, type = "string:20" }
    { name = rate, type = "decimal:11,2" }
    { name = tran_amt, type = "decimal:11,2" }
    { name = ref_tran_amt, type = "decimal:11,2" }
    { name = ref_tran_crncy, type = "string:20" }
    { name = acct_sol_id, type = "string:20" }
    { name = schm_type, type = "string:20" }
    { name = prod_code, type = "string:20" }
    { name = place_holder, type = "string:20" }
    { name = cust_id, type = "string:20" }
    { name = cx_cust_id, type = "string:20" }
    { name = cx_acct_id, type = "string:20" }
    { name = acct_name, type = "string:20" }
    { name = acct_ownership, type = "string:20" }
    { name = acctopendate, type = "date" }
    { name = acct_branch_id, type = "string:20" }
    { name = emp_id, type = "string:20" }
    { name = cre_dr_ptran, type = "string:20" }
    { name = pur_acct_num, type = "string:20" }
    { name = payeename, type = "string:20" }
    { name = payeecity, type = "string:20" }
    { name = online_batch, type = "string:20" }
    { name = kyc_stat, type = "string:20" }
    { name = acct_stat, type = "string:20" }
    { name = sanction_amt, type = "decimal:11,2" }
    { name = fcnr_flag, type = "string:20" }
    { name = eod, type = "number:20" }
    { name = ip_address, type = "string:20" }
    { name = dc_id, type = "string:20" }
    { name = dcc_id, type = "string:20" }
    { name = device_id, type = "string:20" }
    { name = acct_occp_code, type = "string:20" }
    { name = reservedfield1, type = "string:20" }
    { name = reservedfield2, type = "string:20" }
    { name = reservedfield3, type = "string:20" }
    { name = reservedfield4, type = "string:100" }
    { name = reservedfield5, type = "string:100" }
    { name = channel_desc, type = "string:20" }
    { name = entity_id, type = "string:20" }
    { name = entity_type, type = "string:20" }
    { name = entry_user, type = "string:20" }
    { name = event_sub_type, type = "string:20" }
    { name = eventts, type = "date" }
    { name = event_type, type = "string:20" }
    { name = hdrmkrs, type = "string:20" }
    { name = mode_oprn_code, type = "string:20" }
    { name = msg_name, type = "string:20" }
    { name = source, type = "string:20 " }
    { name = tran_date, type = "date" }
    { name = bin, type = "string:20" }
    { name = systemcountry, type = "string:20" }
    { name = dev_owner_id, type = "string:20" }
    { name = cust_card_id, type = "string:20" }
    { name = acct_open_date, type = "date" }
    { name = sequence_number, type = "string:20 " }
    { name = counterpartyaccount, type = "string:20 " }
    { name = counterpartyname, type = "string:20 " }
    { name = counterpartybank, type = "string:20 " }
    { name = counterpartyamount, type = "string:20 " }
    { name = counterpartycurrency, type = "string:20 " }
    { name = counterpartyamountlcy, type = "string:20 " }
    { name = counterpartycurrencylcy, type = "string:20 " }
    { name = counterpartyaddress, type = "string:20 " }
    { name = counterpartybankcode, type = "string:20 " }
    { name = counterpartybic, type = "string:20 " }
    { name = counterpartybankaddress, type = "string:20 " }
    { name = counterpartycountrycode, type = "string:20 " }
    { name = counterpartybusiness, type = "string:20 " }
    { name = tellernumber, type = "string:20 " }
    { name = sequencenumber, type = "string:20 " }
    { name = country_code, type = "string:20 " }

   ]
}
