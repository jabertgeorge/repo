// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_CardtxnEventMapper extends EventMapper<FT_CardtxnEvent> {

public FT_CardtxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_CardtxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_CardtxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_CardtxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_CardtxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_CardtxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_CardtxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getCardTypeFlg());
            preparedStatement.setString(i++, obj.getMsgType());
            preparedStatement.setString(i++, obj.getPosEntryMode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getDomesticIntCardFlg());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setString(i++, obj.getApicallType());
            preparedStatement.setString(i++, obj.getRespCode());
            preparedStatement.setDouble(i++, obj.getInrTranAmt());
            preparedStatement.setString(i++, obj.getTxnReferenceNo());
            preparedStatement.setString(i++, obj.getErrorDescription());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getIsWalletTxn());
            preparedStatement.setString(i++, obj.getCardNo());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getTxnCntryCode());
            preparedStatement.setDouble(i++, obj.getAtmLimit());
            preparedStatement.setString(i++, obj.getTxnType());
            preparedStatement.setString(i++, obj.getProcCode());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getTxnCrncyCode());
            preparedStatement.setString(i++, obj.getPinCode());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getMccCode());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getTerminalId());
            preparedStatement.setDouble(i++, obj.getEcomLimit());
            preparedStatement.setDouble(i++, obj.getPosLimit());
            preparedStatement.setDate(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getBinNum());
            preparedStatement.setString(i++, obj.getInterbankcard());
            preparedStatement.setString(i++, obj.getWalletType());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getMerchantId());
            preparedStatement.setString(i++, obj.getSuccFailFlag());
            preparedStatement.setString(i++, obj.getMerchantName());
            preparedStatement.setString(i++, obj.getAcqCntryCode());
            preparedStatement.setString(i++, obj.getCardIdentifierCode());
            preparedStatement.setString(i++, obj.getHomeCrncyCode());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_CARD_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_CardtxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_CardtxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_CARD_TXN"));
        putList = new ArrayList<>();

        for (FT_CardtxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "CARD_TYPE_FLG",  obj.getCardTypeFlg());
            p = this.insert(p, "MSG_TYPE",  obj.getMsgType());
            p = this.insert(p, "POS_ENTRY_MODE",  obj.getPosEntryMode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "DOMESTIC_INT_CARD_FLG",  obj.getDomesticIntCardFlg());
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "APICALL_TYPE",  obj.getApicallType());
            p = this.insert(p, "RESP_CODE",  obj.getRespCode());
            p = this.insert(p, "INR_TRAN_AMT", String.valueOf(obj.getInrTranAmt()));
            p = this.insert(p, "TXN_REFERENCE_NO",  obj.getTxnReferenceNo());
            p = this.insert(p, "ERROR_DESCRIPTION",  obj.getErrorDescription());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "IS_WALLET_TXN",  obj.getIsWalletTxn());
            p = this.insert(p, "CARD_NO",  obj.getCardNo());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "TXN_CNTRY_CODE",  obj.getTxnCntryCode());
            p = this.insert(p, "ATM_LIMIT", String.valueOf(obj.getAtmLimit()));
            p = this.insert(p, "TXN_TYPE",  obj.getTxnType());
            p = this.insert(p, "PROC_CODE",  obj.getProcCode());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "TXN_CRNCY_CODE",  obj.getTxnCrncyCode());
            p = this.insert(p, "PIN_CODE",  obj.getPinCode());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "MCC_CODE",  obj.getMccCode());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "TERMINAL_ID",  obj.getTerminalId());
            p = this.insert(p, "ECOM_LIMIT", String.valueOf(obj.getEcomLimit()));
            p = this.insert(p, "POS_LIMIT", String.valueOf(obj.getPosLimit()));
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "BIN_NUM",  obj.getBinNum());
            p = this.insert(p, "INTERBANKCARD",  obj.getInterbankcard());
            p = this.insert(p, "WALLET_TYPE",  obj.getWalletType());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "MERCHANT_ID",  obj.getMerchantId());
            p = this.insert(p, "SUCC_FAIL_FLAG",  obj.getSuccFailFlag());
            p = this.insert(p, "MERCHANT_NAME",  obj.getMerchantName());
            p = this.insert(p, "ACQ_CNTRY_CODE",  obj.getAcqCntryCode());
            p = this.insert(p, "CARD_IDENTIFIER_CODE",  obj.getCardIdentifierCode());
            p = this.insert(p, "HOME_CRNCY_CODE",  obj.getHomeCrncyCode());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_CARD_TXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_CARD_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_CARD_TXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_CardtxnEvent obj = new FT_CardtxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setCardTypeFlg(rs.getString("CARD_TYPE_FLG"));
    obj.setMsgType(rs.getString("MSG_TYPE"));
    obj.setPosEntryMode(rs.getString("POS_ENTRY_MODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setDomesticIntCardFlg(rs.getString("DOMESTIC_INT_CARD_FLG"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setApicallType(rs.getString("APICALL_TYPE"));
    obj.setRespCode(rs.getString("RESP_CODE"));
    obj.setInrTranAmt(rs.getDouble("INR_TRAN_AMT"));
    obj.setTxnReferenceNo(rs.getString("TXN_REFERENCE_NO"));
    obj.setErrorDescription(rs.getString("ERROR_DESCRIPTION"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setIsWalletTxn(rs.getString("IS_WALLET_TXN"));
    obj.setCardNo(rs.getString("CARD_NO"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setTxnCntryCode(rs.getString("TXN_CNTRY_CODE"));
    obj.setAtmLimit(rs.getDouble("ATM_LIMIT"));
    obj.setTxnType(rs.getString("TXN_TYPE"));
    obj.setProcCode(rs.getString("PROC_CODE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setTxnCrncyCode(rs.getString("TXN_CRNCY_CODE"));
    obj.setPinCode(rs.getString("PIN_CODE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setMccCode(rs.getString("MCC_CODE"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setTerminalId(rs.getString("TERMINAL_ID"));
    obj.setEcomLimit(rs.getDouble("ECOM_LIMIT"));
    obj.setPosLimit(rs.getDouble("POS_LIMIT"));
    obj.setTranDate(rs.getDate("TRAN_DATE"));
    obj.setBinNum(rs.getString("BIN_NUM"));
    obj.setInterbankcard(rs.getString("INTERBANKCARD"));
    obj.setWalletType(rs.getString("WALLET_TYPE"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setMerchantId(rs.getString("MERCHANT_ID"));
    obj.setSuccFailFlag(rs.getString("SUCC_FAIL_FLAG"));
    obj.setMerchantName(rs.getString("MERCHANT_NAME"));
    obj.setAcqCntryCode(rs.getString("ACQ_CNTRY_CODE"));
    obj.setCardIdentifierCode(rs.getString("CARD_IDENTIFIER_CODE"));
    obj.setHomeCrncyCode(rs.getString("HOME_CRNCY_CODE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_CARD_TXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_CardtxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_CardtxnEvent> events;
 FT_CardtxnEvent obj = new FT_CardtxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_CARD_TXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_CardtxnEvent obj = new FT_CardtxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setCardTypeFlg(getColumnValue(rs, "CARD_TYPE_FLG"));
            obj.setMsgType(getColumnValue(rs, "MSG_TYPE"));
            obj.setPosEntryMode(getColumnValue(rs, "POS_ENTRY_MODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setDomesticIntCardFlg(getColumnValue(rs, "DOMESTIC_INT_CARD_FLG"));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setApicallType(getColumnValue(rs, "APICALL_TYPE"));
            obj.setRespCode(getColumnValue(rs, "RESP_CODE"));
            obj.setInrTranAmt(EventHelper.toDouble(getColumnValue(rs, "INR_TRAN_AMT")));
            obj.setTxnReferenceNo(getColumnValue(rs, "TXN_REFERENCE_NO"));
            obj.setErrorDescription(getColumnValue(rs, "ERROR_DESCRIPTION"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setIsWalletTxn(getColumnValue(rs, "IS_WALLET_TXN"));
            obj.setCardNo(getColumnValue(rs, "CARD_NO"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setTxnCntryCode(getColumnValue(rs, "TXN_CNTRY_CODE"));
            obj.setAtmLimit(EventHelper.toDouble(getColumnValue(rs, "ATM_LIMIT")));
            obj.setTxnType(getColumnValue(rs, "TXN_TYPE"));
            obj.setProcCode(getColumnValue(rs, "PROC_CODE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setTxnCrncyCode(getColumnValue(rs, "TXN_CRNCY_CODE"));
            obj.setPinCode(getColumnValue(rs, "PIN_CODE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setMccCode(getColumnValue(rs, "MCC_CODE"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setTerminalId(getColumnValue(rs, "TERMINAL_ID"));
            obj.setEcomLimit(EventHelper.toDouble(getColumnValue(rs, "ECOM_LIMIT")));
            obj.setPosLimit(EventHelper.toDouble(getColumnValue(rs, "POS_LIMIT")));
            obj.setTranDate(EventHelper.toDate(getColumnValue(rs, "TRAN_DATE")));
            obj.setBinNum(getColumnValue(rs, "BIN_NUM"));
            obj.setInterbankcard(getColumnValue(rs, "INTERBANKCARD"));
            obj.setWalletType(getColumnValue(rs, "WALLET_TYPE"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setMerchantId(getColumnValue(rs, "MERCHANT_ID"));
            obj.setSuccFailFlag(getColumnValue(rs, "SUCC_FAIL_FLAG"));
            obj.setMerchantName(getColumnValue(rs, "MERCHANT_NAME"));
            obj.setAcqCntryCode(getColumnValue(rs, "ACQ_CNTRY_CODE"));
            obj.setCardIdentifierCode(getColumnValue(rs, "CARD_IDENTIFIER_CODE"));
            obj.setHomeCrncyCode(getColumnValue(rs, "HOME_CRNCY_CODE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_CARD_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_CARD_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"CARD_TYPE_FLG\",\"MSG_TYPE\",\"POS_ENTRY_MODE\",\"CHANNEL\",\"DOMESTIC_INT_CARD_FLG\",\"SOURCE\",\"APICALL_TYPE\",\"RESP_CODE\",\"INR_TRAN_AMT\",\"TXN_REFERENCE_NO\",\"ERROR_DESCRIPTION\",\"ADDR_NETWORK\",\"HOST_ID\",\"IS_WALLET_TXN\",\"CARD_NO\",\"TRAN_AMT\",\"TXN_CNTRY_CODE\",\"ATM_LIMIT\",\"TXN_TYPE\",\"PROC_CODE\",\"ACCOUNT_ID\",\"TXN_CRNCY_CODE\",\"PIN_CODE\",\"CUST_ID\",\"MCC_CODE\",\"ERROR_CODE\",\"TERMINAL_ID\",\"ECOM_LIMIT\",\"POS_LIMIT\",\"TRAN_DATE\",\"BIN_NUM\",\"INTERBANKCARD\",\"WALLET_TYPE\",\"AVL_BAL\",\"MERCHANT_ID\",\"SUCC_FAIL_FLAG\",\"MERCHANT_NAME\",\"ACQ_CNTRY_CODE\",\"CARD_IDENTIFIER_CODE\",\"HOME_CRNCY_CODE\"" +
              " FROM EVENT_FT_CARD_TXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [CARD_TYPE_FLG],[MSG_TYPE],[POS_ENTRY_MODE],[CHANNEL],[DOMESTIC_INT_CARD_FLG],[SOURCE],[APICALL_TYPE],[RESP_CODE],[INR_TRAN_AMT],[TXN_REFERENCE_NO],[ERROR_DESCRIPTION],[ADDR_NETWORK],[HOST_ID],[IS_WALLET_TXN],[CARD_NO],[TRAN_AMT],[TXN_CNTRY_CODE],[ATM_LIMIT],[TXN_TYPE],[PROC_CODE],[ACCOUNT_ID],[TXN_CRNCY_CODE],[PIN_CODE],[CUST_ID],[MCC_CODE],[ERROR_CODE],[TERMINAL_ID],[ECOM_LIMIT],[POS_LIMIT],[TRAN_DATE],[BIN_NUM],[INTERBANKCARD],[WALLET_TYPE],[AVL_BAL],[MERCHANT_ID],[SUCC_FAIL_FLAG],[MERCHANT_NAME],[ACQ_CNTRY_CODE],[CARD_IDENTIFIER_CODE],[HOME_CRNCY_CODE]" +
              " FROM EVENT_FT_CARD_TXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`CARD_TYPE_FLG`,`MSG_TYPE`,`POS_ENTRY_MODE`,`CHANNEL`,`DOMESTIC_INT_CARD_FLG`,`SOURCE`,`APICALL_TYPE`,`RESP_CODE`,`INR_TRAN_AMT`,`TXN_REFERENCE_NO`,`ERROR_DESCRIPTION`,`ADDR_NETWORK`,`HOST_ID`,`IS_WALLET_TXN`,`CARD_NO`,`TRAN_AMT`,`TXN_CNTRY_CODE`,`ATM_LIMIT`,`TXN_TYPE`,`PROC_CODE`,`ACCOUNT_ID`,`TXN_CRNCY_CODE`,`PIN_CODE`,`CUST_ID`,`MCC_CODE`,`ERROR_CODE`,`TERMINAL_ID`,`ECOM_LIMIT`,`POS_LIMIT`,`TRAN_DATE`,`BIN_NUM`,`INTERBANKCARD`,`WALLET_TYPE`,`AVL_BAL`,`MERCHANT_ID`,`SUCC_FAIL_FLAG`,`MERCHANT_NAME`,`ACQ_CNTRY_CODE`,`CARD_IDENTIFIER_CODE`,`HOME_CRNCY_CODE`" +
              " FROM EVENT_FT_CARD_TXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_CARD_TXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"CARD_TYPE_FLG\",\"MSG_TYPE\",\"POS_ENTRY_MODE\",\"CHANNEL\",\"DOMESTIC_INT_CARD_FLG\",\"SOURCE\",\"APICALL_TYPE\",\"RESP_CODE\",\"INR_TRAN_AMT\",\"TXN_REFERENCE_NO\",\"ERROR_DESCRIPTION\",\"ADDR_NETWORK\",\"HOST_ID\",\"IS_WALLET_TXN\",\"CARD_NO\",\"TRAN_AMT\",\"TXN_CNTRY_CODE\",\"ATM_LIMIT\",\"TXN_TYPE\",\"PROC_CODE\",\"ACCOUNT_ID\",\"TXN_CRNCY_CODE\",\"PIN_CODE\",\"CUST_ID\",\"MCC_CODE\",\"ERROR_CODE\",\"TERMINAL_ID\",\"ECOM_LIMIT\",\"POS_LIMIT\",\"TRAN_DATE\",\"BIN_NUM\",\"INTERBANKCARD\",\"WALLET_TYPE\",\"AVL_BAL\",\"MERCHANT_ID\",\"SUCC_FAIL_FLAG\",\"MERCHANT_NAME\",\"ACQ_CNTRY_CODE\",\"CARD_IDENTIFIER_CODE\",\"HOME_CRNCY_CODE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[CARD_TYPE_FLG],[MSG_TYPE],[POS_ENTRY_MODE],[CHANNEL],[DOMESTIC_INT_CARD_FLG],[SOURCE],[APICALL_TYPE],[RESP_CODE],[INR_TRAN_AMT],[TXN_REFERENCE_NO],[ERROR_DESCRIPTION],[ADDR_NETWORK],[HOST_ID],[IS_WALLET_TXN],[CARD_NO],[TRAN_AMT],[TXN_CNTRY_CODE],[ATM_LIMIT],[TXN_TYPE],[PROC_CODE],[ACCOUNT_ID],[TXN_CRNCY_CODE],[PIN_CODE],[CUST_ID],[MCC_CODE],[ERROR_CODE],[TERMINAL_ID],[ECOM_LIMIT],[POS_LIMIT],[TRAN_DATE],[BIN_NUM],[INTERBANKCARD],[WALLET_TYPE],[AVL_BAL],[MERCHANT_ID],[SUCC_FAIL_FLAG],[MERCHANT_NAME],[ACQ_CNTRY_CODE],[CARD_IDENTIFIER_CODE],[HOME_CRNCY_CODE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`CARD_TYPE_FLG`,`MSG_TYPE`,`POS_ENTRY_MODE`,`CHANNEL`,`DOMESTIC_INT_CARD_FLG`,`SOURCE`,`APICALL_TYPE`,`RESP_CODE`,`INR_TRAN_AMT`,`TXN_REFERENCE_NO`,`ERROR_DESCRIPTION`,`ADDR_NETWORK`,`HOST_ID`,`IS_WALLET_TXN`,`CARD_NO`,`TRAN_AMT`,`TXN_CNTRY_CODE`,`ATM_LIMIT`,`TXN_TYPE`,`PROC_CODE`,`ACCOUNT_ID`,`TXN_CRNCY_CODE`,`PIN_CODE`,`CUST_ID`,`MCC_CODE`,`ERROR_CODE`,`TERMINAL_ID`,`ECOM_LIMIT`,`POS_LIMIT`,`TRAN_DATE`,`BIN_NUM`,`INTERBANKCARD`,`WALLET_TYPE`,`AVL_BAL`,`MERCHANT_ID`,`SUCC_FAIL_FLAG`,`MERCHANT_NAME`,`ACQ_CNTRY_CODE`,`CARD_IDENTIFIER_CODE`,`HOME_CRNCY_CODE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

