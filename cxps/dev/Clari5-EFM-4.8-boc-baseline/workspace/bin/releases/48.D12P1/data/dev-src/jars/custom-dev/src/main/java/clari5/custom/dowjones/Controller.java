package clari5.custom.dowjones;

import clari5.custom.dev.WatchListFileMatcher;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.rdbms.Rdbms;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;

public class Controller {
    private static final CxpsLogger logger = CxpsLogger.getLogger(Controller.class);
    public static void process() throws UnsupportedEncodingException {
        Map<String, String> selectData = null;
        try (CxConnection connection = establishConnection()){
                selectData = SelectUpdate.selectdata(connection);
                logger.info("data inside select data is: " + selectData);
                SelectUpdate.updateRemTable(connection, selectData);
        } catch (Exception e){
            e.printStackTrace();
        }
        
        try {
            for (String s : Arrays.asList(SelectUpdate.query.getString("dowjones.type").split(","))) {
                if (selectData.containsValue(s)) {
                    TradeFinance tf = new TradeFinance();
                    Map<String, String> responseMap = tf.callApi(selectData);
//                    Map<String, String> responseMap = TradeFinance.callApi(connection, selectData);
                    CxJson json = TradeFinance.evidenceFormatter(responseMap);
                    tf = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CxConnection establishConnection(){

        //Clari5.batchBootstrap("test", "efm-clari5.conf");
        RDBMSSession session = Rdbms.getAppSession();

        if (session == null) throw new RuntimeException("Not able to get session connection");

        return session.getCxConnection();
    }

    public static void main(String[] args) throws UnsupportedEncodingException, UnirestException {
        //Controller.process();
       /* HttpResponse<String> response = Unirest.post("http://192.168.5.229:10090/jira/rest/api/2/issue/")
                .header("Content-Type", "application/json")
                .header("Cache-Control", "no-cache")
                .header("Postman-Token", "2a3e5975-7d71-4988-8824-8491c004955e")
                .body("{\n\t\"fields\": {\n\t\t\"project\": \n\t\t{\n\t\t\"key\": \"AML\"\n\t},\n\t\"summary\":\"TEST API\",\n\t\"description\": \"Test Description\",\n\t\"issuetype\": {\n\t\t\"name\": \"Case\"\n\t}\n\t}\n}")
                .asString();
        System.out.println("response status is: "+response.getStatus() +" resp body " +response.getBody());*/
    }
}
