clari5.hfdb.entity.ft-lease-txn {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : SYNC_STATUS , type:"string:100"}

   { name = event_name, type = "string:100" }
   { name = eventtype, type = "string:100" }
   { name = eventsubtype, type = "string:100" }
   { name = host_user_id, type = "string:20" }
   { name = host_id, type = "string:20" }
   { name = channel, type = "string:20" }
   { name = keys, type = "string:20 " }
   { name = cx_cust_id, type = "string:20" }
   { name = cx_acct_id, type = "string:20 " }
   { name = lease_facility_number, type = "string:50" }
   { name = systemcountry, type = "string:20" }
   { name = tran_date, type = "date" }
   { name = lease_settlement_date, type = "date" }
   { name = stakeholder, type = "string:50" }
   { name = tran_amt, type = "decimal:11,2" }
   { name = lcy_amt, type = "decimal:11,2" }
   { name = lcy_curr, type = "string:20" }
   { name = tran_code, type = "string:20" }
   { name = part_tran_type, type = "string:20" }
   { name = cust_card_id, type = "string:20" }
   { name = stakeholder_id, type = "string:20" }
   { name = part_tran_srl_num, type = "string:20" }
   { name = tran_type, type = "string:20" }
   { name = tran_sub_type, type = "string:20" }
   { name = txnid, type = "string:20" }
   { name = tran_curr, type = "string:20" }
   { name = systime, type = "date" }
   { name = eventts, type = "date" }
   { name = source, type = "string:20 " }
   { name = reservedfield1, type = "string:20" }
   { name = reservedfield2, type = "string:20" }
   { name = reservedfield3, type = "string:20" }
   { name = reservedfield4, type = "string:100" }
   { name = reservedfield5, type = "string:100" }
   { name = sys_time, type = "date" }


   ]
}
