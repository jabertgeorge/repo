cxps.noesis.glossary.entity.CREDIT_CARD_MASTER {
        attributes = [
                        { name=customerid, column=CUSTOMERID, type="string:10" }
			{ name=accountid, column=ACCOUNTID, type="string:10" }
			{ name=creditcardnumber, column=CREDITCARDNUMBER, type="string:16", key = true }
			{ name=creditcardlimit, column=CREDITCARDLIMIT, type="number:22" }
			{ name=interestrate, column=INTERESTRATE, type="number:22" }
			{ name=nicregistration, column=NICREGISTRATION, type="string:10" }
			{ name=annulfee, column=ANNULFEE, type="string:10" }
			{ name=producttype, column=PRODUCTTYPE, type="string:10" }
			{ name=productdescription, column=PRODUCTDESCRIPTION, type="string:10" }
			{ name=bankrelatedparty, column=BANKRELATEDPARTY, type="string:10" }
			{ name=acctopendate, column=ACCTOPENDATE, type="timestamp" }
			{ name=renewalfee, column=RENEWALFEE, type="string:10" }
			{ name=customername, column=CUSTOMERNAME, type="string:10" }
			{ name=homebranch, column=HOMEBRANCH, type="string:10" }
        ]
}
