cxps.events.event.nft-static-info-change{
    event-mnemonic: NSIC
	event-ids = [ event-id ]
	workspaces : {
	CUSTOMER:cust-id
	}
    event-attributes : {
       host-user-id: {db : true ,raw_name : HostUserId ,type : "string:20"}
       entity-type: {db : true ,raw_name : EntityType ,type : "string:20"}
       entity-id: {db : true ,raw_name : EntityId ,type : "string:20"}
       channel-id: {db : true ,raw_name : ChannelID ,type : "string:20"}
       channel-type: {db : true ,raw_name : ChannelType ,type : "string:20"}
       channel-desc: {db : true ,raw_name : ChannelDesc ,type : "string:20"}
       ref-type: {db : true ,raw_name : ref_type ,type : "string:20" ,custom-getter:Ref_type}
       ref-id: {db : true ,raw_name : ref_id ,type : "string:20" ,custom-getter:Ref_id}
       user-id: {db : true ,raw_name : userId ,type : "string:20"}
       tran-date: {db : true ,raw_name : tran_date ,type : timestamp ,custom-getter:Tran_date}
       solid: {db : true ,raw_name : solid ,type : "string:20"}
       entity-sub-type: {db : true ,raw_name : EntitySubType ,type : "string:20"}
       init-sub-entity-val: {db : true ,raw_name : initSubEntityVal ,type : "string:20"}
       final-sub-entity-val: {db : true ,raw_name : finalSubEntityVal ,type : "string:20"}
       acct-id: {db : true ,raw_name : acctId ,type : "string:20"}
       cust-id: {db : true ,raw_name : custId ,type : "string:20"}
       status: {db : true ,raw_name : status ,type : "string:20"}
       host-id:{type:"string:100",db:true,raw_name:hostId}
       event-time: {db : true ,raw_name : eventTime ,type : "string:20"}
    }
}
