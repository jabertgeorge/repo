package clari5.custom.boc.integration.data;

public class NFT_Uscust extends ITableData {

    private String tableName = "NFT_USCUST";
    private String event_type = "NFT_Uscust";

    private String event_name;
    private String eventtype;
    private String eventsubtype;
    private String host_user_id;
    private String host_id;
    private String channel;
    private String keys;
    private String source;
    private String sys_time;
    private String cust_id;
    private String cx_cust_id;
    private String cx_acct_id;
    private String systemcountry;
    private String custcreationdate;
    private String permanent_addr;
    private String mailing_addr;
    private String tel_phn;
    private String citenzenship;
    private String usnational_fatca;
    private String reservedfield1;
    private String reservedfield2;
    private String reservedfield3;
    private String reservedfield4;
    private String reservedfield5;
    private String event_id;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getEventsubtype() {
        return eventsubtype;
    }

    public void setEventsubtype(String eventsubtype) {
        this.eventsubtype = eventsubtype;
    }

    public String getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(String host_user_id) {
        this.host_user_id = host_user_id;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getCx_cust_id() {
        return cx_cust_id;
    }

    public void setCx_cust_id(String cx_cust_id) {
        this.cx_cust_id = cx_cust_id;
    }

    public String getCx_acct_id() {
        return cx_acct_id;
    }

    public void setCx_acct_id(String cx_acct_id) {
        this.cx_acct_id = cx_acct_id;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getCustcreationdate() {
        return custcreationdate;
    }

    public void setCustcreationdate(String custcreationdate) {
        this.custcreationdate = custcreationdate;
    }

    public String getPermanent_addr() {
        return permanent_addr;
    }

    public void setPermanent_addr(String permanent_addr) {
        this.permanent_addr = permanent_addr;
    }

    public String getMailing_addr() {
        return mailing_addr;
    }

    public void setMailing_addr(String mailing_addr) {
        this.mailing_addr = mailing_addr;
    }

    public String getTel_phn() {
        return tel_phn;
    }

    public void setTel_phn(String tel_phn) {
        this.tel_phn = tel_phn;
    }

    public String getCitenzenship() {
        return citenzenship;
    }

    public void setCitenzenship(String citenzenship) {
        this.citenzenship = citenzenship;
    }

    public String getUsnational_fatca() {
        return usnational_fatca;
    }

    public void setUsnational_fatca(String usnational_fatca) {
        this.usnational_fatca = usnational_fatca;
    }

    public String getReservedfield1() {
        return reservedfield1;
    }

    public void setReservedfield1(String reservedfield1) {
        this.reservedfield1 = reservedfield1;
    }

    public String getReservedfield2() {
        return reservedfield2;
    }

    public void setReservedfield2(String reservedfield2) {
        this.reservedfield2 = reservedfield2;
    }

    public String getReservedfield3() {
        return reservedfield3;
    }

    public void setReservedfield3(String reservedfield3) {
        this.reservedfield3 = reservedfield3;
    }

    public String getReservedfield4() {
        return reservedfield4;
    }

    public void setReservedfield4(String reservedfield4) {
        this.reservedfield4 = reservedfield4;
    }

    public String getReservedfield5() {
        return reservedfield5;
    }

    public void setReservedfield5(String reservedfield5) {
        this.reservedfield5 = reservedfield5;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
