clari5.hfdb.entity.loan-master {
    attributes:[
        { name: COREAccountID, type: "string:100", key=true }
	{ name: CORECustId, type: "string:100" }
	{ name: cxCifID, type: "string:100" }
	{ name: ActualRepaymentAcct, type: "string:100" }
	{ name: SettlementDate, type: "date" }
	{ name: MaturityDate, type: "date" }
	{ name: SanctionedDate, type: "date" }
	{ name: SanctionedLoanAmt, type= "number:10,2" }
	{ name: DtofLoanOpening, type: "date" }
	{ name: CustName, type: "string:100" }
	{ name: InstallmentAmt, type= "number:10,2" }
	{ name: NICBR, type: "string:100" }
	{ name: PersonalNotPersonal, type: "string:1" }
	{ name: ProductCode, type: "string:100" }
	{ name: ProductDescription, type: "string:100" }
	{ name: BankRelatedParty, type: "string:100" }
	{ name: InterestRate, type= "decimal:10,2" }
	]
}

