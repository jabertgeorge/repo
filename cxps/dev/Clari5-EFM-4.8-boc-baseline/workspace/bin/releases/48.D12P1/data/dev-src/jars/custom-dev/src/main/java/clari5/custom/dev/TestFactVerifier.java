package clari5.custom.dev;

import clari5.aml.risk.IFactsAssessor;
import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Taher on 2/9/14.
 */
public class TestFactVerifier implements IFactsAssessor {
    public static CxpsLogger logger = CxpsLogger.getLogger(TestFactVerifier.class);
    Set<String> factMap ;
    @Override
    public Boolean isInvestigationNeededCustom(String entityKey, String softFact) {
        Boolean investigationStatus = true;
        logger.debug("Etered to methodE" );
        if((entityKey.toLowerCase()).startsWith("a")){
            //String factName = softFact.getFactName();
            logger.debug("startsWith A");
            if(factMap.contains(softFact)){
                logger.debug("fact contains ");
                investigationStatus = false;
            }
                logger.info("fact does not contains ");
        }
        return investigationStatus;
    }
    @Override
    public void init() {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("dev-fact-config.conf");
        Hocon factHocon = hocon.get("clari5.custom.dev-fact-config");
        List<String> factList = factHocon.getStringList("facts");
        factMap = new HashSet<>();
        for(String fact : factList){
            factMap.add(fact);
            logger.info("LIST OF FACTS " + fact);
        }
        logger.info("LIST OF FACTS SIZE" + factMap.size());
    }
}

