clari5.hfdb.entity.ft-pawning-txn {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : event_name , type : "string:100"}
   { name : eventtype , type : "string:100"}
   { name : eventsubtype , type : "string:100"}
   { name : host_user_id , type : "string:20"}
   { name: "hostid", type: "string:100"  }
   { name : channel , type : "string:20"}
   { name : keys , type : "string:20"}
   { name : customerid , type : "string:50"}
   { name : cx_cust_id , type : "string:20"}
   { name : cx_acct_id , type : "string:20"}
   { name : systemcountry , type : "string:20"}
   { name : tran_date , type : "date"}
   { name : nic , type : "string:50"}
   { name : br , type : "string:50"}
   { name : tran_amt , type :"decimal:11,2"}
   { name : lcy_tran_amt , type : "decimal:11,2"}
   { name : lcy_tran_curr , type : "string:20"}
   { name : accountid , type : "string:20"}
   { name : part_tran_type , type : "string:20"}
   { name : part_tran_srl_num , type : "string:20"}
   { name : tran_type , type : "string:20"}
   { name : tran_sub_type , type : "string:20"}
   { name : txnid , type : "string:20"}
   { name : tran_curr , type : "string:20"}
   { name : sys_time , type : "date"}
   { name : eventts , type : "date"}
   { name : source , type : "string:20"}
   { name : reservedfield1 , type:"string:100"}
   { name : reservedfield2 , type:"string:100"}
   { name : reservedfield3 , type:"string:100"}
   { name : reservedfield4 , type:"string:100"}
   { name : reservedfield5 , type:"string:100"}
   { name : SYNC_STATUS , type:"string:100"}
   ]
}
