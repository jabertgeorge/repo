clari5.hfdb.entity.TF-MASTER {
        attributes:[
			{ name: FACILTY-NUM, type: "string:20", key=true }
			{ name: CORE-ACCTID, type: "string:20" }
			{ name: CX-ACCTID, type: "string:20" }
			{ name: CORE-Cust-Id, type: "string:20" }
			{ name: cxCifID, type: "string:20" }
			{ name: Oust-AmT, type= "decimal:8,3" }
			{ name: Currency, type: "string:20" }
			{ name: Dateii,  type: "date" }
			{ name: STATUS,  type: "string:20" }
			{ name: FACILTY-Open-Date, type: "date" }
			{ name: FACILTY-expiry-Date, type: "date" }
			{ name: Acceptance-Commission-Rate, type= "decimal:8,3" }
			{ name: Payment-Commission-Rate,type= "decimal:8,3" }
			{ name: RelatedPartyFlg, type: "string:20" }
     ]
}

