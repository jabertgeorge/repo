package clari5.custom.util.xmltojson;


import clari5.platform.util.Hocon;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.HandlerBase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Path("xmltojson")
public class XmlToJsonConverter {
    private static CxpsLogger cxpsLogger = CxpsLogger.getLogger(XmlToJsonConverter.class);

    static Hocon xmlPathReader;

    static {
        xmlPathReader = new Hocon();
        xmlPathReader.loadFromContext("xml-path-reader.conf");
    }

    public XmlToJsonConverter() {
    }

    /**
     * URL: http://<DN>:<PORT>/xmltojsonparser/rest/xmltojson/getStringXml
     * @param xml
     * @return jsonObject
     * @throws IOException
     * @throws JSONException This method converts xml to json using jackson2
     */

    @POST
    @Path("/getStringXml")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public String toJSON(String xml) throws IOException, JSONException {

        System.out.println("inside toJSON method");
        System.out.println("xml string: " + xml);
        String isValidated = validateXmlString(xml);
        if (isValidated.equalsIgnoreCase("true")) {
            ObjectMapper xmlMapper = new XmlMapper();
            JsonNode node = xmlMapper.readTree(xml);
            ObjectMapper jsonMapper = new ObjectMapper();

            String json = jsonMapper.writeValueAsString(node);
            JSONObject jsonObject = new JSONObject(json);
            cxpsLogger.debug("[TransformXml: Conversion from XML to JSON]" + jsonObject.toString());
            System.out.println("[TransformXml: Conversion from XML to JSON]" + jsonObject.toString());
            return jsonObject.toString();
        } else {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", isValidated);
            return jsonObject.toString();
        }
    }

    /**
     * Validate the xml String.
     * @param strXML
     * @return
     */
    private String validateXmlString(String strXML) {
        try {
            SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
            InputStream stream = new ByteArrayInputStream(strXML.getBytes("UTF-8"));
            HandlerBase handlerBase = new HandlerBase();
            saxParser.parse(stream, handlerBase);
            return "true";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    /**
     * This method accepts xml in the form of String and its root tag and
     * converts xml to json using jackson2
     *
     * @param xml
     * @return jsonObject
     * @throws IOException
     * @throws JSONException
     */

    @POST
    @Path("/getStringXmlWithRootTag")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public String toJsonUsingRootTag(String xml, String rootTag) throws IOException, JSONException {
        ObjectMapper xmlMapper = new XmlMapper();
        JsonNode node = xmlMapper.readTree(xml);
        ObjectMapper jsonMapper = new ObjectMapper();
        String json = jsonMapper.writeValueAsString(node);
        JSONObject jsonObject = new JSONObject(json);
        String str = "{\"" + rootTag + "\":" + jsonObject.toString() + "}";
        jsonObject = new JSONObject(str);
        return jsonObject.toString();
    }

    /**
     * URL: http://<DN>:<PORT>/xmltojsonparser/rest/xmltojson/getXmlFilePath
     * @return
     */
    @GET
    @Path("/getXmlFilePath")
    @Produces(MediaType.APPLICATION_JSON)
    public String xmlFileToJSONObject() {
        String path = xmlPathReader.getString("xml.path");
        List<String> json = new ArrayList<>();
        try {
            File file = new File(path);
            String[] filename = file.list();
            for (String name: filename) {
                if (name.endsWith(".xml")) {
                    File xmlFile = new File(path + "/" + name);
                    XmlToJsonConverter xml = new XmlToJsonConverter();
                    Reader fileReader = new FileReader(xmlFile);
                    BufferedReader bufReader = new BufferedReader(fileReader);
                    StringBuilder sb = new StringBuilder();
                    String line = bufReader.readLine();
                    while (line != null) {
                        sb.append(line).append("\n");
                        line = bufReader.readLine();
                    }
                    String xml2String = sb.toString();
                    /*System.out.println("XML to String using BufferedReader : ");
                    System.out.println(xml2String);*/
                    bufReader.close();
                    json.add(xml.toJSON(xml2String));
                } else {
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public static void main(String[] args) {
        //String strXML ="<?xml version='1.0' encoding='UTF-8' standalone='yes'?><custDtl><name>abc</name><mobNo>9876543210</mobNo></custDtl>";
            /*String strXML ="<?xml version='1.0' encoding='UTF-8' standalone='yes'?>\n" +
                    "<custDtl>\n" +
                    "<name>abc</name>\n" +
                    "<mobNo>9876543210\n" +
                    "</custDtl>";*/
            /*String strXML = "<custDtl>\n" +
                    "<name>abc</name>\n" +
                    "<mobNo>9876543210</mobNo>\n" +
                    "</custDtl>";*/
    }
}
