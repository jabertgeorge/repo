// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_CARD_TXN", Schema="rice")
public class FT_CardtxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=2) public String cardTypeFlg;
       @Field(size=50) public String msgType;
       @Field(size=20) public String posEntryMode;
       @Field(size=10) public String channel;
       @Field(size=2) public String domesticIntCardFlg;
       @Field(size=50) public String source;
       @Field(size=20) public String apicallType;
       @Field(size=10) public String respCode;
       @Field(size=20) public Double inrTranAmt;
       @Field(size=20) public String txnReferenceNo;
       @Field(size=50) public String errorDescription;
       @Field(size=20) public String addrNetwork;
       @Field(size=2) public String hostId;
       @Field(size=2) public String isWalletTxn;
       @Field(size=20) public String cardNo;
       @Field(size=20) public Double tranAmt;
       @Field(size=20) public String txnCntryCode;
       @Field(size=20) public Double atmLimit;
       @Field(size=10) public String txnType;
       @Field(size=50) public String procCode;
       @Field(size=20) public String accountId;
       @Field(size=20) public String txnCrncyCode;
       @Field(size=20) public String pinCode;
       @Field(size=20) public String custId;
       @Field(size=10) public String mccCode;
       @Field(size=50) public String errorCode;
       @Field(size=20) public String terminalId;
       @Field(size=20) public Double ecomLimit;
       @Field(size=20) public Double posLimit;
       @Field public java.sql.Date tranDate;
       @Field(size=6) public String binNum;
       @Field(size=2) public String interbankcard;
       @Field(size=50) public String walletType;
       @Field(size=11) public Double avlBal;
       @Field(size=20) public String merchantId;
       @Field(size=2) public String succFailFlag;
       @Field(size=50) public String merchantName;
       @Field(size=20) public String acqCntryCode;
       @Field(size=20) public String cardIdentifierCode;
       @Field(size=20) public String homeCrncyCode;


    @JsonIgnore
    public ITable<FT_CardtxnEvent> t = AEF.getITable(this);

	public FT_CardtxnEvent(){}

    public FT_CardtxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Cardtxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setCardTypeFlg(json.getString("card_type_flg"));
            setMsgType(json.getString("msg_type"));
            setPosEntryMode(json.getString("pos_entry_mode"));
            setChannel(json.getString("channel"));
            setDomesticIntCardFlg(json.getString("domestic_int_card_flg"));
            setSource(json.getString("source"));
            setApicallType(json.getString("apicall_type"));
            setRespCode(json.getString("resp_code"));
            setInrTranAmt(EventHelper.toDouble(json.getString("inr_tran_amt")));
            setTxnReferenceNo(json.getString("txn_reference_no"));
            setErrorDescription(json.getString("error_description"));
            setAddrNetwork(json.getString("addr_network"));
            setHostId(json.getString("host_id"));
            setIsWalletTxn(json.getString("is_wallet_txn"));
            setCardNo(json.getString("card_no"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setTxnCntryCode(json.getString("txn_cntry_code"));
            setAtmLimit(EventHelper.toDouble(json.getString("atm_limit")));
            setTxnType(json.getString("txn_type"));
            setProcCode(json.getString("proc_code"));
            setAccountId(json.getString("account_id"));
            setTxnCrncyCode(json.getString("txn_crncy_code"));
            setPinCode(json.getString("pin_code"));
            setCustId(json.getString("cust_id"));
            setMccCode(json.getString("mcc_code"));
            setErrorCode(json.getString("error_code"));
            setTerminalId(json.getString("terminal_id"));
            setEcomLimit(EventHelper.toDouble(json.getString("ecom_limit")));
            setPosLimit(EventHelper.toDouble(json.getString("pos_limit")));
            setTranDate(EventHelper.toDate(json.getString("tran_date")));
            setBinNum(json.getString("bin_num"));
            setInterbankcard(json.getString("interbankcard"));
            setWalletType(json.getString("wallet_type"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setMerchantId(json.getString("merchant_id"));
            setSuccFailFlag(json.getString("succ_fail_flag"));
            setMerchantName(json.getString("merchant_name"));
            setAcqCntryCode(json.getString("acq_cntry_code"));
            setCardIdentifierCode(json.getString("card_identifier_code"));
            setHomeCrncyCode(json.getString("home_crncy_code"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getCardTypeFlg(){ return cardTypeFlg; }

    public String getMsgType(){ return msgType; }

    public String getPosEntryMode(){ return posEntryMode; }

    public String getChannel(){ return channel; }

    public String getDomesticIntCardFlg(){ return domesticIntCardFlg; }

    public String getSource(){ return source; }

    public String getApicallType(){ return apicallType; }

    public String getRespCode(){ return respCode; }

    public Double getInrTranAmt(){ return inrTranAmt; }

    public String getTxnReferenceNo(){ return txnReferenceNo; }

    public String getErrorDescription(){ return errorDescription; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getHostId(){ return hostId; }

    public String getIsWalletTxn(){ return isWalletTxn; }

    public String getCardNo(){ return cardNo; }

    public Double getTranAmt(){ return tranAmt; }

    public String getTxnCntryCode(){ return txnCntryCode; }

    public Double getAtmLimit(){ return atmLimit; }

    public String getTxnType(){ return txnType; }

    public String getProcCode(){ return procCode; }

    public String getAccountId(){ return accountId; }

    public String getTxnCrncyCode(){ return txnCrncyCode; }

    public String getPinCode(){ return pinCode; }

    public String getCustId(){ return custId; }

    public String getMccCode(){ return mccCode; }

    public String getErrorCode(){ return errorCode; }

    public String getTerminalId(){ return terminalId; }

    public Double getEcomLimit(){ return ecomLimit; }

    public Double getPosLimit(){ return posLimit; }

    public java.sql.Date getTranDate(){ return tranDate; }

    public String getBinNum(){ return binNum; }

    public String getInterbankcard(){ return interbankcard; }

    public String getWalletType(){ return walletType; }

    public Double getAvlBal(){ return avlBal; }

    public String getMerchantId(){ return merchantId; }

    public String getSuccFailFlag(){ return succFailFlag; }

    public String getMerchantName(){ return merchantName; }

    public String getAcqCntryCode(){ return acqCntryCode; }

    public String getCardIdentifierCode(){ return cardIdentifierCode; }

    public String getHomeCrncyCode(){ return homeCrncyCode; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setCardTypeFlg(String val){ this.cardTypeFlg = val; }
    public void setMsgType(String val){ this.msgType = val; }
    public void setPosEntryMode(String val){ this.posEntryMode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setDomesticIntCardFlg(String val){ this.domesticIntCardFlg = val; }
    public void setSource(String val){ this.source = val; }
    public void setApicallType(String val){ this.apicallType = val; }
    public void setRespCode(String val){ this.respCode = val; }
    public void setInrTranAmt(Double val){ this.inrTranAmt = val; }
    public void setTxnReferenceNo(String val){ this.txnReferenceNo = val; }
    public void setErrorDescription(String val){ this.errorDescription = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setIsWalletTxn(String val){ this.isWalletTxn = val; }
    public void setCardNo(String val){ this.cardNo = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setTxnCntryCode(String val){ this.txnCntryCode = val; }
    public void setAtmLimit(Double val){ this.atmLimit = val; }
    public void setTxnType(String val){ this.txnType = val; }
    public void setProcCode(String val){ this.procCode = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setTxnCrncyCode(String val){ this.txnCrncyCode = val; }
    public void setPinCode(String val){ this.pinCode = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setMccCode(String val){ this.mccCode = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setTerminalId(String val){ this.terminalId = val; }
    public void setEcomLimit(Double val){ this.ecomLimit = val; }
    public void setPosLimit(Double val){ this.posLimit = val; }
    public void setTranDate(java.sql.Date val){ this.tranDate = val; }
    public void setBinNum(String val){ this.binNum = val; }
    public void setInterbankcard(String val){ this.interbankcard = val; }
    public void setWalletType(String val){ this.walletType = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setSuccFailFlag(String val){ this.succFailFlag = val; }
    public void setMerchantName(String val){ this.merchantName = val; }
    public void setAcqCntryCode(String val){ this.acqCntryCode = val; }
    public void setCardIdentifierCode(String val){ this.cardIdentifierCode = val; }
    public void setHomeCrncyCode(String val){ this.homeCrncyCode = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_CardtxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.cardNo+merchantId);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.cardNo);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Cardtxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Cardtxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("card_no",getCardNo());
        json.put("cust_id",getCustId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}