cxps.noesis.glossary.entity.Pawning-Master {
        attributes = [
			{name = Account-ID, type = "string:20", key=true }
			{name = CustId, type = "string:20"}
			{name = Actual-Repayment-Acct, type = "number:20"}
			{name = Settlement-Maturity-Date, type = "date"}
			{name = Granted-Pawning-Amount, type = "decimal:20,3"}
			{name = Dt-of-Pawning-Acct-Opening-Renew, type = "date"}
			{name = CustName, type = "string:20"}
			{name = Inst-Amt, type = "decimal:20,3"}
			{name = NIC, type = "string:20"}
			{name = PersonalNotPersonal, type = "string:20"}
			{name = ProductCode, type = "string:20"}
			{name = ProductDescription, type = "string:20"}
			{name = BankRelatedParty, type = "string:20"}
			{name = Interest-Rate, type = "decimal:20,3"}			     
	]
}

