package clari5.custom.boc.integration.data;

public class FT_LoanTxn extends ITableData {

    private String tableName = "FT_LOAN_TXN";
    private String event_type = "FT_LoanTxn";

    private String event_name;
    private String eventtype;
    private String eventsubtype;
    private String host_user_id;
    private String host_id;
    private String channel_type;
    private String channel;
    private String keys;
    private String agent_type;
    private String pstd_user_id;
    private String bank_code;
    private String cust_ref_type;
    private String acid;
    private String cx_cif_i_d;
    private String txn_ref_type;
    private String tran_id;
    private String sys_time;
    private String module_id;
    private String loan_settlement_date;
    private String tran_particular;
    private String tran_amt;
    private String lcy_tran_amt;
    private String lcy_tran_crncy;
    private String tran_crncy_code;
    private String tran_code;
    private String value_date;
    private String exception;
    private String pstd_flg;
    private String tran_rmks;
    private String account_id;
    private String acct_type;
    private String remitter_name;
    private String beneficiary_name;
    private String instrmnt_type;
    private String clr_bal_amt;
    private String balance_cur;
    private String un_clr_bal_amt;
    private String avl_bal;
    private String avl_bal_lcy;
    private String eff_avl_bal;
    private String part_tran_type;
    private String evt_inducer;
    private String ref_num;
    private String zone_date;
    private String zone_code;
    private String payee_id;
    private String merchant_categ;
    private String part_tran_srl_num;
    private String tran_category;
    private String instrmnt_alpha;
    private String tran_type;
    private String tran_sub_type;
    private String entry_date;
    private String pstd_date;
    private String vfd_date;
    private String txn_br_id;
    private String rate;
    private String ref_tran_amt;
    private String ref_tran_crncy;
    private String acct_sol_id;
    private String schm_type;
    private String prod_code;
    private String place_holder;
    private String cust_id;
    private String cx_cust_id;
    private String cx_acct_id;
    private String acct_name;
    private String acct_ownership;
    private String acctopendate;
    private String acct_branch_id;
    private String emp_id;
    private String cre_dr_ptran;
    private String pur_acct_num;
    private String payeename;
    private String payeecity;
    private String online_batch;
    private String kyc_stat;
    private String acct_stat;
    private String sanction_amt;
    private String fcnr_flag;
    private String eod;
    private String ip_address;
    private String dc_id;
    private String dcc_id;
    private String device_id;
    private String acct_occp_code;
    private String reservedfield1;
    private String reservedfield2;
    private String reservedfield3;
    private String reservedfield4;
    private String reservedfield5;
    private String channel_desc;
    private String entity_id;
    private String entity_type;
    private String entry_user;
    private String event_sub_type;
    private String eventts;
    private String hdrmkrs;
    private String mode_oprn_code;
    private String msg_name;
    private String source;
    private String tran_date;
    private String bin;
    private String systemcountry;
    private String dev_owner_id;
    private String cust_card_id;
    private String acct_open_date;
    private String sequence_number;
    private String counterpartyaccount;
    private String counterpartyname;
    private String counterpartybank;
    private String counterpartyamount;
    private String counterpartycurrency;
    private String counterpartyamountlcy;
    private String counterpartycurrencylcy;
    private String counterpartyaddress;
    private String counterpartybankcode;
    private String counterpartybic;
    private String counterpartybankaddress;
    private String counterpartycountrycode;
    private String counterpartybusiness;
    private String tellernumber;
    private String sequencenumber;
    private String country_code;
    private String event_id;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getEventsubtype() {
        return eventsubtype;
    }

    public void setEventsubtype(String eventsubtype) {
        this.eventsubtype = eventsubtype;
    }

    public String getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(String host_user_id) {
        this.host_user_id = host_user_id;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getAgent_type() {
        return agent_type;
    }

    public void setAgent_type(String agent_type) {
        this.agent_type = agent_type;
    }

    public String getPstd_user_id() {
        return pstd_user_id;
    }

    public void setPstd_user_id(String pstd_user_id) {
        this.pstd_user_id = pstd_user_id;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getCust_ref_type() {
        return cust_ref_type;
    }

    public void setCust_ref_type(String cust_ref_type) {
        this.cust_ref_type = cust_ref_type;
    }

    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    public String getCx_cif_i_d() {
        return cx_cif_i_d;
    }

    public void setCx_cif_i_d(String cx_cif_i_d) {
        this.cx_cif_i_d = cx_cif_i_d;
    }

    public String getTxn_ref_type() {
        return txn_ref_type;
    }

    public void setTxn_ref_type(String txn_ref_type) {
        this.txn_ref_type = txn_ref_type;
    }

    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getLoan_settlement_date() {
        return loan_settlement_date;
    }

    public void setLoan_settlement_date(String loan_settlement_date) {
        this.loan_settlement_date = loan_settlement_date;
    }

    public String getTran_particular() {
        return tran_particular;
    }

    public void setTran_particular(String tran_particular) {
        this.tran_particular = tran_particular;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public void setTran_amt(String tran_amt) {
        this.tran_amt = tran_amt;
    }

    public String getLcy_tran_amt() {
        return lcy_tran_amt;
    }

    public void setLcy_tran_amt(String lcy_tran_amt) {
        this.lcy_tran_amt = lcy_tran_amt;
    }

    public String getLcy_tran_crncy() {
        return lcy_tran_crncy;
    }

    public void setLcy_tran_crncy(String lcy_tran_crncy) {
        this.lcy_tran_crncy = lcy_tran_crncy;
    }

    public String getTran_crncy_code() {
        return tran_crncy_code;
    }

    public void setTran_crncy_code(String tran_crncy_code) {
        this.tran_crncy_code = tran_crncy_code;
    }

    public String getTran_code() {
        return tran_code;
    }

    public void setTran_code(String tran_code) {
        this.tran_code = tran_code;
    }

    public String getValue_date() {
        return value_date;
    }

    public void setValue_date(String value_date) {
        this.value_date = value_date;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getPstd_flg() {
        return pstd_flg;
    }

    public void setPstd_flg(String pstd_flg) {
        this.pstd_flg = pstd_flg;
    }

    public String getTran_rmks() {
        return tran_rmks;
    }

    public void setTran_rmks(String tran_rmks) {
        this.tran_rmks = tran_rmks;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAcct_type() {
        return acct_type;
    }

    public void setAcct_type(String acct_type) {
        this.acct_type = acct_type;
    }

    public String getRemitter_name() {
        return remitter_name;
    }

    public void setRemitter_name(String remitter_name) {
        this.remitter_name = remitter_name;
    }

    public String getBeneficiary_name() {
        return beneficiary_name;
    }

    public void setBeneficiary_name(String beneficiary_name) {
        this.beneficiary_name = beneficiary_name;
    }

    public String getInstrmnt_type() {
        return instrmnt_type;
    }

    public void setInstrmnt_type(String instrmnt_type) {
        this.instrmnt_type = instrmnt_type;
    }

    public String getClr_bal_amt() {
        return clr_bal_amt;
    }

    public void setClr_bal_amt(String clr_bal_amt) {
        this.clr_bal_amt = clr_bal_amt;
    }

    public String getBalance_cur() {
        return balance_cur;
    }

    public void setBalance_cur(String balance_cur) {
        this.balance_cur = balance_cur;
    }

    public String getUn_clr_bal_amt() {
        return un_clr_bal_amt;
    }

    public void setUn_clr_bal_amt(String un_clr_bal_amt) {
        this.un_clr_bal_amt = un_clr_bal_amt;
    }

    public String getAvl_bal() {
        return avl_bal;
    }

    public void setAvl_bal(String avl_bal) {
        this.avl_bal = avl_bal;
    }

    public String getAvl_bal_lcy() {
        return avl_bal_lcy;
    }

    public void setAvl_bal_lcy(String avl_bal_lcy) {
        this.avl_bal_lcy = avl_bal_lcy;
    }

    public String getEff_avl_bal() {
        return eff_avl_bal;
    }

    public void setEff_avl_bal(String eff_avl_bal) {
        this.eff_avl_bal = eff_avl_bal;
    }

    public String getPart_tran_type() {
        return part_tran_type;
    }

    public void setPart_tran_type(String part_tran_type) {
        this.part_tran_type = part_tran_type;
    }

    public String getEvt_inducer() {
        return evt_inducer;
    }

    public void setEvt_inducer(String evt_inducer) {
        this.evt_inducer = evt_inducer;
    }

    public String getRef_num() {
        return ref_num;
    }

    public void setRef_num(String ref_num) {
        this.ref_num = ref_num;
    }

    public String getZone_date() {
        return zone_date;
    }

    public void setZone_date(String zone_date) {
        this.zone_date = zone_date;
    }

    public String getZone_code() {
        return zone_code;
    }

    public void setZone_code(String zone_code) {
        this.zone_code = zone_code;
    }

    public String getPayee_id() {
        return payee_id;
    }

    public void setPayee_id(String payee_id) {
        this.payee_id = payee_id;
    }

    public String getMerchant_categ() {
        return merchant_categ;
    }

    public void setMerchant_categ(String merchant_categ) {
        this.merchant_categ = merchant_categ;
    }

    public String getPart_tran_srl_num() {
        return part_tran_srl_num;
    }

    public void setPart_tran_srl_num(String part_tran_srl_num) {
        this.part_tran_srl_num = part_tran_srl_num;
    }

    public String getTran_category() {
        return tran_category;
    }

    public void setTran_category(String tran_category) {
        this.tran_category = tran_category;
    }

    public String getInstrmnt_alpha() {
        return instrmnt_alpha;
    }

    public void setInstrmnt_alpha(String instrmnt_alpha) {
        this.instrmnt_alpha = instrmnt_alpha;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_sub_type() {
        return tran_sub_type;
    }

    public void setTran_sub_type(String tran_sub_type) {
        this.tran_sub_type = tran_sub_type;
    }

    public String getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(String entry_date) {
        this.entry_date = entry_date;
    }

    public String getPstd_date() {
        return pstd_date;
    }

    public void setPstd_date(String pstd_date) {
        this.pstd_date = pstd_date;
    }

    public String getVfd_date() {
        return vfd_date;
    }

    public void setVfd_date(String vfd_date) {
        this.vfd_date = vfd_date;
    }

    public String getTxn_br_id() {
        return txn_br_id;
    }

    public void setTxn_br_id(String txn_br_id) {
        this.txn_br_id = txn_br_id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRef_tran_amt() {
        return ref_tran_amt;
    }

    public void setRef_tran_amt(String ref_tran_amt) {
        this.ref_tran_amt = ref_tran_amt;
    }

    public String getRef_tran_crncy() {
        return ref_tran_crncy;
    }

    public void setRef_tran_crncy(String ref_tran_crncy) {
        this.ref_tran_crncy = ref_tran_crncy;
    }

    public String getAcct_sol_id() {
        return acct_sol_id;
    }

    public void setAcct_sol_id(String acct_sol_id) {
        this.acct_sol_id = acct_sol_id;
    }

    public String getSchm_type() {
        return schm_type;
    }

    public void setSchm_type(String schm_type) {
        this.schm_type = schm_type;
    }

    public String getProd_code() {
        return prod_code;
    }

    public void setProd_code(String prod_code) {
        this.prod_code = prod_code;
    }

    public String getPlace_holder() {
        return place_holder;
    }

    public void setPlace_holder(String place_holder) {
        this.place_holder = place_holder;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getCx_cust_id() {
        return cx_cust_id;
    }

    public void setCx_cust_id(String cx_cust_id) {
        this.cx_cust_id = cx_cust_id;
    }

    public String getCx_acct_id() {
        return cx_acct_id;
    }

    public void setCx_acct_id(String cx_acct_id) {
        this.cx_acct_id = cx_acct_id;
    }

    public String getAcct_name() {
        return acct_name;
    }

    public void setAcct_name(String acct_name) {
        this.acct_name = acct_name;
    }

    public String getAcct_ownership() {
        return acct_ownership;
    }

    public void setAcct_ownership(String acct_ownership) {
        this.acct_ownership = acct_ownership;
    }

    public String getAcctopendate() {
        return acctopendate;
    }

    public void setAcctopendate(String acctopendate) {
        this.acctopendate = acctopendate;
    }

    public String getAcct_branch_id() {
        return acct_branch_id;
    }

    public void setAcct_branch_id(String acct_branch_id) {
        this.acct_branch_id = acct_branch_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getCre_dr_ptran() {
        return cre_dr_ptran;
    }

    public void setCre_dr_ptran(String cre_dr_ptran) {
        this.cre_dr_ptran = cre_dr_ptran;
    }

    public String getPur_acct_num() {
        return pur_acct_num;
    }

    public void setPur_acct_num(String pur_acct_num) {
        this.pur_acct_num = pur_acct_num;
    }

    public String getPayeename() {
        return payeename;
    }

    public void setPayeename(String payeename) {
        this.payeename = payeename;
    }

    public String getPayeecity() {
        return payeecity;
    }

    public void setPayeecity(String payeecity) {
        this.payeecity = payeecity;
    }

    public String getOnline_batch() {
        return online_batch;
    }

    public void setOnline_batch(String online_batch) {
        this.online_batch = online_batch;
    }

    public String getKyc_stat() {
        return kyc_stat;
    }

    public void setKyc_stat(String kyc_stat) {
        this.kyc_stat = kyc_stat;
    }

    public String getAcct_stat() {
        return acct_stat;
    }

    public void setAcct_stat(String acct_stat) {
        this.acct_stat = acct_stat;
    }

    public String getSanction_amt() {
        return sanction_amt;
    }

    public void setSanction_amt(String sanction_amt) {
        this.sanction_amt = sanction_amt;
    }

    public String getFcnr_flag() {
        return fcnr_flag;
    }

    public void setFcnr_flag(String fcnr_flag) {
        this.fcnr_flag = fcnr_flag;
    }

    public String getEod() {
        return eod;
    }

    public void setEod(String eod) {
        this.eod = eod;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getDc_id() {
        return dc_id;
    }

    public void setDc_id(String dc_id) {
        this.dc_id = dc_id;
    }

    public String getDcc_id() {
        return dcc_id;
    }

    public void setDcc_id(String dcc_id) {
        this.dcc_id = dcc_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getAcct_occp_code() {
        return acct_occp_code;
    }

    public void setAcct_occp_code(String acct_occp_code) {
        this.acct_occp_code = acct_occp_code;
    }

    public String getReservedfield1() {
        return reservedfield1;
    }

    public void setReservedfield1(String reservedfield1) {
        this.reservedfield1 = reservedfield1;
    }

    public String getReservedfield2() {
        return reservedfield2;
    }

    public void setReservedfield2(String reservedfield2) {
        this.reservedfield2 = reservedfield2;
    }

    public String getReservedfield3() {
        return reservedfield3;
    }

    public void setReservedfield3(String reservedfield3) {
        this.reservedfield3 = reservedfield3;
    }

    public String getReservedfield4() {
        return reservedfield4;
    }

    public void setReservedfield4(String reservedfield4) {
        this.reservedfield4 = reservedfield4;
    }

    public String getReservedfield5() {
        return reservedfield5;
    }

    public void setReservedfield5(String reservedfield5) {
        this.reservedfield5 = reservedfield5;
    }

    public String getChannel_desc() {
        return channel_desc;
    }

    public void setChannel_desc(String channel_desc) {
        this.channel_desc = channel_desc;
    }

    public String getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public String getEntry_user() {
        return entry_user;
    }

    public void setEntry_user(String entry_user) {
        this.entry_user = entry_user;
    }

    public String getEvent_sub_type() {
        return event_sub_type;
    }

    public void setEvent_sub_type(String event_sub_type) {
        this.event_sub_type = event_sub_type;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getHdrmkrs() {
        return hdrmkrs;
    }

    public void setHdrmkrs(String hdrmkrs) {
        this.hdrmkrs = hdrmkrs;
    }

    public String getMode_oprn_code() {
        return mode_oprn_code;
    }

    public void setMode_oprn_code(String mode_oprn_code) {
        this.mode_oprn_code = mode_oprn_code;
    }

    public String getMsg_name() {
        return msg_name;
    }

    public void setMsg_name(String msg_name) {
        this.msg_name = msg_name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getDev_owner_id() {
        return dev_owner_id;
    }

    public void setDev_owner_id(String dev_owner_id) {
        this.dev_owner_id = dev_owner_id;
    }

    public String getCust_card_id() {
        return cust_card_id;
    }

    public void setCust_card_id(String cust_card_id) {
        this.cust_card_id = cust_card_id;
    }

    public String getAcct_open_date() {
        return acct_open_date;
    }

    public void setAcct_open_date(String acct_open_date) {
        this.acct_open_date = acct_open_date;
    }

    public String getSequence_number() {
        return sequence_number;
    }

    public void setSequence_number(String sequence_number) {
        this.sequence_number = sequence_number;
    }

    public String getCounterpartyaccount() {
        return counterpartyaccount;
    }

    public void setCounterpartyaccount(String counterpartyaccount) {
        this.counterpartyaccount = counterpartyaccount;
    }

    public String getCounterpartyname() {
        return counterpartyname;
    }

    public void setCounterpartyname(String counterpartyname) {
        this.counterpartyname = counterpartyname;
    }

    public String getCounterpartybank() {
        return counterpartybank;
    }

    public void setCounterpartybank(String counterpartybank) {
        this.counterpartybank = counterpartybank;
    }

    public String getCounterpartyamount() {
        return counterpartyamount;
    }

    public void setCounterpartyamount(String counterpartyamount) {
        this.counterpartyamount = counterpartyamount;
    }

    public String getCounterpartycurrency() {
        return counterpartycurrency;
    }

    public void setCounterpartycurrency(String counterpartycurrency) {
        this.counterpartycurrency = counterpartycurrency;
    }

    public String getCounterpartyamountlcy() {
        return counterpartyamountlcy;
    }

    public void setCounterpartyamountlcy(String counterpartyamountlcy) {
        this.counterpartyamountlcy = counterpartyamountlcy;
    }

    public String getCounterpartycurrencylcy() {
        return counterpartycurrencylcy;
    }

    public void setCounterpartycurrencylcy(String counterpartycurrencylcy) {
        this.counterpartycurrencylcy = counterpartycurrencylcy;
    }

    public String getCounterpartyaddress() {
        return counterpartyaddress;
    }

    public void setCounterpartyaddress(String counterpartyaddress) {
        this.counterpartyaddress = counterpartyaddress;
    }

    public String getCounterpartybankcode() {
        return counterpartybankcode;
    }

    public void setCounterpartybankcode(String counterpartybankcode) {
        this.counterpartybankcode = counterpartybankcode;
    }

    public String getCounterpartybic() {
        return counterpartybic;
    }

    public void setCounterpartybic(String counterpartybic) {
        this.counterpartybic = counterpartybic;
    }

    public String getCounterpartybankaddress() {
        return counterpartybankaddress;
    }

    public void setCounterpartybankaddress(String counterpartybankaddress) {
        this.counterpartybankaddress = counterpartybankaddress;
    }

    public String getCounterpartycountrycode() {
        return counterpartycountrycode;
    }

    public void setCounterpartycountrycode(String counterpartycountrycode) {
        this.counterpartycountrycode = counterpartycountrycode;
    }

    public String getCounterpartybusiness() {
        return counterpartybusiness;
    }

    public void setCounterpartybusiness(String counterpartybusiness) {
        this.counterpartybusiness = counterpartybusiness;
    }

    public String getTellernumber() {
        return tellernumber;
    }

    public void setTellernumber(String tellernumber) {
        this.tellernumber = tellernumber;
    }

    public String getSequencenumber() {
        return sequencenumber;
    }

    public void setSequencenumber(String sequencenumber) {
        this.sequencenumber = sequencenumber;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
