cxps.noesis.glossary.entity.CUSTOM_FATCA_BOOTSTRAP {
        attributes = [
                        {name = TotalYearlyCredit, type = "decimal:20,2"}
                        {name = TotalYearlyInterestCredit, type = "decimal:20,2"}
                        {name = TotalAccountBalance, type = "decimal:20,2"}
                        {name = CUSTID, type = "string:350", key=true}
                        {name = YEAR, type = "number:10", key=true}
        ]
}
