package clari5.custom.wlalert;

import cxps.apex.utils.CxpsLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.PrintStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip
{
    public static CxpsLogger logger = CxpsLogger.getLogger(UnZip.class);

    public boolean unZipIt(String zipFile, String unzipDir)
            throws IOException
    {
//        System.out.println("UnZip.unZipIt param [zipfile=" + zipFile + ". unzipDir=" + unzipDir + "]");

        File z1 = new File(zipFile);
        if (z1.isDirectory())
        {
  //          System.out.println("Unzip.unzipIt is a directory returning false");
            return false;
        }
        byte[] buffer = new byte[1024];

        File folder = new File(unzipDir);
            if (!folder.exists()) {
            folder.mkdir();
        }
        FileOutputStream fos = null;
        ZipInputStream zip = null;
        try
        {
            zip = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry ze = zip.getNextEntry();
            while (ze != null)
            {
                String fileName = ze.getName();
                File newFile = new File(unzipDir + File.separator + fileName);
                new File(newFile.getParent()).mkdirs();

                fos = new FileOutputStream(newFile);
                int len;
                while ((len = zip.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                ze = zip.getNextEntry();
            }
            logger.info("Unzipping of file done ");
    //        System.out.println("Unzipping of file done ");
        }
        catch (Exception e)
        {
           //String fileName;
            e.printStackTrace();
            return false;
        }
        finally
        {
            if (fos != null) {
                fos.close();
            }
            if (zip != null)
            {
                zip.closeEntry();
                zip.close();
            }
        }
        return true;
    }
}
