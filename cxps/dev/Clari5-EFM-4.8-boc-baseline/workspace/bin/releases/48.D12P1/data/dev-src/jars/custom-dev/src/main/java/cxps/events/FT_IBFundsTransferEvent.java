package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FT_IBFundsTransferEvent extends Event {
    private static final long serialVersionUID = 971136614054831273L;
    private String source;
    private Date sysTime;
    private String keys;
    private String userId;
    private String  custId;
    private String deviceId;
    private String succFailFlg;
    private String errorCode;
    private String errorDesc;
    private String timeSlot;
    private Double txnAmt;
    private String txnCrncy;
    private Double avlBal;
    private String tranType;
    private String addrNetwork;
    private String drAcctId;
    private String bankId;
    private String payeeId;
    private String crAcctId;
    private String crAcctName;
    private String crBankName;
    private String crBankId;
    private String crBranchId;
    private String crBranchName;
    private String tranId;
    private String txnRefNo;
    private String utrNumber;
    private String mmId;
    private String country;
    private String entity1;
    private String entity2;
    private String entity3;
    private String entity4;
    private String entity5;
    private String entity6;
    private String entity7;
    private String entity8;
    private String entity9;
    private String entity10;

    public String getCountry() {
        return this.country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getEntity1() {
        return this.entity1;
    }
    public void setEntity1(String entity1) {
        this.entity1 = entity1;
    }
    public String getEntity2() {
        return this.entity2;
    }
    public void setEntity2(String entity2) {
        this.entity2 = entity2;
    }
    public String getEntity3() {
        return this.entity3;
    }
    public void setEntity3(String entity3) {
        this.entity3 = entity3;
    }
    public String getEntity4() {
        return this.entity4;
    }
    public void setEntity4(String entity4) {
        this.entity4 = entity4;
    }
    public String getEntity5() {
        return this.entity5;
    }
    public void setEntity5(String entity5) {
        this.entity5 = entity5;
    }
    public String getEntity6() {
        return this.entity6;
    }
    public void setEntity6(String entity6) {
        this.entity6 = entity6;
    }
    public String getEntity7() {
        return this.entity7;
    }
    public void setEntity7(String entity7) {
        this.entity7 = entity7;
    }
    public String getEntity8() {
        return this.entity8;
    }
    public void setEntity8(String entity8) {
        this.entity8 = entity8;
    }
    public String getEntity9() {
        return this.entity9;
    }
    public void setEntity9(String entity9) {
        this.entity9 = entity9;
    }
    public String getEntity10() {
        return this.entity10;
    }
    public void setEntity10(String entity10) {
        this.entity10 = entity10;
    }
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    public String getKeys() {
        return keys;
    }
    public void setKeys(String keys) {
        this.keys = keys;
    }
    public Date getSysTime() {
        return sysTime;
    }

    public void setSysTime(String sysTime) {
        if (sysTime == null || "".equals(sysTime.trim())) {
            throw new IllegalArgumentException("Value is required: sysTime");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                this.sysTime = dateFormat1.parse(sysTime);
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide sysTime in the format:dd-MM-yyyy HH:mm:ss.SSS .Invalid Value>field:sysTime>value:" +sysTime);
            }
        }
    }
    public void setEventTSStr(String eventts) {
        if (eventts == null || "".equals(eventts.trim())) {
            throw new IllegalArgumentException("Value is required: eventts");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                setEventTS(dateFormat1.parse(eventts));
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide eventTs in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:eventts>value:" +eventts);
            }
        }
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getCustId() {
        return custId;
    }
    public  void setCustId(String custId) {
        if (custId == null || "".equals(custId.trim())){
            throw new IllegalArgumentException("Value is required: CustId");
        }
        else
            this.custId = custId;
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public String getSuccFailFlg() {
        return succFailFlg;
    }
    public void setSuccFailFlg(String succFailFlg) {
        this.succFailFlg = succFailFlg;
    }
    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    public String getErrorDesc() {
        return errorDesc;
    }
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
    public String getTimeSlot() {
        return timeSlot;
    }
    public void setTimeSlot(String timeSlot) {
        if(timeSlot.trim().startsWith("S")) {
            this.timeSlot = timeSlot;
        } else {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                Date dateForTimeSlot = dateFormat.parse(timeSlot);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateForTimeSlot);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                if((6 <= hour) && (hour < 9)) {
                    this.timeSlot = "S1";
                } else if ((9 <= hour) && (hour < 15)) {
                    this.timeSlot = "S2";
                } else if ((15 <= hour) && (hour < 21)) {
                    this.timeSlot = "S3";
                } else if ((21 <= hour) && (hour < 23)) {
                    this.timeSlot = "S4";
                } else if ((23 == hour) || ((hour >= 0) && (hour < 9))) {
                    this.timeSlot = "S5";
                } else {
                    throw new IllegalArgumentException("Invalid hour value " +hour +" for timeslot");
                }
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide timeSlot or date in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:timeSlot>value:" +timeSlot);
            }
        }
    }
    public Double getTxnAmt() {
        return txnAmt;
    }
    public void setTxnAmt(String txnAmt) {
        if (txnAmt != null && !"".equals(txnAmt.trim())) {
            try {
                this.txnAmt = Double.parseDouble(txnAmt.replaceAll(",",""));

            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException(
                        "Invalid Value>field:txnAmt>value:" + txnAmt);
            }
        }
    }
    public String getTxnCrncy() {
        return txnCrncy;
    }
    public void setTxnCrncy(String txnCrncy) {
        this.txnCrncy = txnCrncy;
    }
    public void setAvlBal(String avlBal) {
        if (avlBal != null && !"".equals(avlBal.trim())) {
            try {
                this.avlBal = Double.parseDouble(avlBal.replaceAll(",",""));

            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException(
                        "Invalid Value>field:avlBal>value:" + avlBal);
            }
        }
    }
    public Double getAvlBal()
    {
        return avlBal;
    }
    public String getTranType() {
        return tranType;
    }
    public void setTranType(String tranType) {
        this.tranType = tranType;
    }
    public String getAddrNetwork() {
        return addrNetwork;
    }
    public void setAddrNetwork(String addrNetwork) {
        this.addrNetwork = addrNetwork;
    }
    public String getDrAcctId() {
        return drAcctId;
    }
    public void setDrAcctId(String drAcctId) {
        this.drAcctId = drAcctId;
    }
    public String getBankId() {
        return bankId;
    }
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }
    public String getPayeeId() {
        return payeeId;
    }
    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }
    public String getCrAcctId() {
        return crAcctId;
    }
    public void setCrAcctId(String crAcctId) {
        this.crAcctId = crAcctId;
    }
    public String getCrAcctName() {
        return crAcctName;
    }
    public void setCrAcctName(String crAcctName) {
        this.crAcctName = crAcctName;
    }
    public String getCrBankName() {
        return crBankName;
    }
    public void setCrBankName(String crBankName) {
        this.crBankName = crBankName;
    }
    public String getCrBankId() {
        return crBankId;
    }
    public void setCrBankId(String crBankId) {
        this.crBankId = crBankId;
    }
    public String getCrBranchId() {
        return crBranchId;
    }
    public void setCrBranchId(String crBranchId) {
        this.crBranchId = crBranchId;
    }
    public String getCrBranchName() {
        return crBranchName;
    }
    public void setCrBranchName(String crBranchName) {
        this.crBranchName = crBranchName;
    }
    public String getTranId() {
        return tranId;
    }
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }
    public String getTxnRefNo() {
        return txnRefNo;
    }
    public void setTxnRefNo(String txnRefNo) {
        this.txnRefNo = txnRefNo;
    }
    public String getMMId() {
        return mmId;
    }
    public void setMMId(String mmId) {
        this.mmId = mmId;
    }
    public String getUtrNumber() {
        return utrNumber;
    }
    public void setUtrNumber(String utrNumber) {
        this.utrNumber = utrNumber;
    }
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setEventType("FT");
        setEventSubType("IBFundTransfer");
        setSource("IB");
        setKeys((String)msgMap.get("keys"));
        setSysTime((String)msgMap.get("sys-time"));
        setUserId((String)msgMap.get("user-id"));
        setCustId((String)msgMap.get("cust-id"));
        setDeviceId((String)msgMap.get("device-id"));
        setSuccFailFlg((String)msgMap.get("succ-fail-flg"));
        setErrorCode((String)msgMap.get("error-code"));
        setErrorDesc((String)msgMap.get("error-desc"));
        setTimeSlot((String)msgMap.get("time-slot"));
        setTxnAmt((String)msgMap.get("txn-amt"));
        setTxnCrncy((String)msgMap.get("txn-crncy"));
        setAvlBal((String)msgMap.get("avl-bal"));
        setTranType((String)msgMap.get("tran-type"));
        setAddrNetwork((String)msgMap.get("addr-network"));
        setDrAcctId((String)msgMap.get("dr-acct-id"));
        setBankId((String)msgMap.get("bank-id"));
        setPayeeId((String)msgMap.get("payee-id"));
        setCrAcctId((String)msgMap.get("cr-acct-id"));
        setCrAcctName((String)msgMap.get("cr-acct-name"));
        setCrBankName((String)msgMap.get("cr-bank-name"));
        setCrBankId((String)msgMap.get("cr-bank-id"));
        setCrBranchId((String)msgMap.get("cr-branch-id"));
        setCrBranchName((String)msgMap.get("cr-branch-name"));
        setTranId((String)msgMap.get("tran-id"));
        setTxnRefNo((String)msgMap.get("txn-ref-no"));
        setMMId((String)msgMap.get("mmid"));
        setUtrNumber((String)msgMap.get("utr-number"));
        setCountry((String)msgMap.get("country"));
        setEntity1((String)msgMap.get("entity1"));
        setEntity2((String)msgMap.get("entity2"));
        setEntity3((String)msgMap.get("entity3"));
        setEntity4((String)msgMap.get("entity4"));
        setEntity5((String)msgMap.get("entity5"));
        setEntity6((String)msgMap.get("entity6"));
        setEntity7((String)msgMap.get("entity7"));
        setEntity8((String)msgMap.get("entity8"));
        setEntity9((String)msgMap.get("entity9"));
        setEntity10((String)msgMap.get("entity10"));
    }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        String hostAcctKey = this.getDrAcctId();
        if(hostAcctKey == null) return null;

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
        String cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, this.getHostId(), hostAcctKey);
        String cxCifId = h.getCxCifIdGivenCxAcctKey(rdbmsSession, cxAcctKey);

        WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, cxAcctKey);
        wi.addParam("acctId", cxAcctKey);
        if(h.isValidCxKey(WorkspaceName.CUSTOMER, cxCifId)) {
            wi.addParam("cxCifID", cxCifId);
        }

        wsInfoSet.add(wi);
       return wsInfoSet;
    }


    public JSONObject getFRJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("event_id",getEventId());
        jsonObject.put("cust_id",getCustId());
        jsonObject.put("event-name",getEventName());
        jsonObject.put("sys_time",getEventTS().getTime());
        jsonObject.put("EventType",getEventType());
        jsonObject.put("EventSubType",getEventSubType());

        return jsonObject;
    }
}
