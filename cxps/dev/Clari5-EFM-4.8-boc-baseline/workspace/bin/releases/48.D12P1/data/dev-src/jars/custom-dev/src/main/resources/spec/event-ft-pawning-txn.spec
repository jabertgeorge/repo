cxps.events.event.ft-pawning-txn {

  table-name : EVENT_ft_PAWNING
  event-mnemonic = FP
  workspaces : {
	   CUSTOMER: cust-id
  	}
  
event-attributes : {

    event-name : {db : true, raw_name: event-name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    host-id : {db : true ,raw_name : host-id ,type : "string:20"}
    channel-id : {db : true ,raw_name : Channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}  
    Cust-id : {db : true ,raw_name : CustomerID ,type : "string:50"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    sys-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}
    txn-date : {db : true ,raw_name : Tran_Date ,type : timestamp}
    tran-date : {db : true ,raw_name : tran_date ,type : timestamp}
    nic : {db : true ,raw_name : NIC ,type : "string:50"}
    br : {db : true ,raw_name : BR ,type : "string:50"}
    txn-amt : {db : true ,raw_name : Tran_amt ,type :"number:11,2"}
    lcy-txn-amt : {db : true ,raw_name : LCY_TRAN_AMT , type : "number:11,2"}
    lcy-txn-Curncy : {db : true ,raw_name : LCY_TRAN_CURR,type : "string:20"}
    acct-id : {db : true ,raw_name : AccountId ,type : "string:20"}
    txn-dr-cr : {db : true ,raw_name : part-tran-type ,type : "string:20"}
    txn-srl-no : {db : true ,raw_name : Part_tran_Srl_Num ,type : "string:20"}
    part-tran-srl-num : {db : true ,raw_name : part_tran_srl_num ,type : "string:20"}
    txn-type : {db : true ,raw_name : tran-type ,type : "string:20"}
    txn-sub-type : {db : true ,raw_name : tran-sub-type ,type : "string:20"}
    txn-id : {db : true ,raw_name : tran_id ,type : "string:20"}
    tran-id : {db : true ,raw_name : tran_id ,type : "string:20"}
    txn-currency : {db : true ,raw_name : Tran_curr ,type : "string:20"}
    system-time : {db : true ,raw_name : Systime ,type : timestamp}
    event-time : {db : true ,raw_name : eventts ,type : timestamp}
    msg-source : {db : true ,raw_name : source ,type : "string:20"} 
    add-entity-id1:{type:"string:100",db:true,raw_name:reservedfield1}
    add-entity-id2:{type:"string:100",db:true,raw_name:reservedfield2}
    add-entity-id3:{type:"string:100",db:true,raw_name:reservedfield3}
    add-entity-id4:{type:"string:100",db:true,raw_name:reservedfield4}
    add-entity-id5:{type:"string:100",db:true,raw_name:reservedfield5}
    
   }
}

