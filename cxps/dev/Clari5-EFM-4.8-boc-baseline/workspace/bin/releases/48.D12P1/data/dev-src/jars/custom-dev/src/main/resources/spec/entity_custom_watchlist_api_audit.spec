clari5.hfdb.entity.watchlist-api-audit {
   attributes:[
    { name: "name", type: "string:350" }
    { name: "dob", type: "string:350" }
    { name: "nationality", type: "string:350" }
    { name: "nic", type: "string:350" }
    { name: "passport", type: "string:350" }
    { name: "max-score", type: "decimal:10,4" }
    { name: "status", type: "string:350" }
    { name: "created-date", type: "date" }
    { name: "entity-id", type: "number:10", key=true }
   ]
}