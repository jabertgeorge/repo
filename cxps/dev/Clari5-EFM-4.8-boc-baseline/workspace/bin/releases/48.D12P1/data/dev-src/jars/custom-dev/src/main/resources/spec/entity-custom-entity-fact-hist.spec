clari5.hfdb.entity {
    custom-entity-fact-hist {
        attributes:[
            { name: entity-fact-hist-id, type: "number:10", key: true }
            { name: cx-key, type: "string:100" }
            { name: fact-name, type: "string:150" }
            { name: date-time, type: timestamp }
            { name: source, type: "string:20"}
            { name: risk-level, type: "string:5" }
            { name: module-id, type: "string:50"}
            { name: score, type: "number:4" }
            { name: evidence, type: clob }
            { name: pc-flag, type: "string:1"}
            { name: migrated-flag, type: "string:1"}
        ]
    }
}

