// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_StaticInfoChangeEventMapper extends EventMapper<NFT_StaticInfoChangeEvent> {

public NFT_StaticInfoChangeEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_StaticInfoChangeEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_StaticInfoChangeEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_StaticInfoChangeEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_StaticInfoChangeEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_StaticInfoChangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_StaticInfoChangeEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getEntityType());
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setString(i++, obj.getSolid());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getInitSubEntityVal());
            preparedStatement.setString(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getChannelId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getChannelType());
            preparedStatement.setString(i++, obj.getRefType());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getRefId());
            preparedStatement.setString(i++, obj.getEntitySubType());
            preparedStatement.setString(i++, obj.getChannelDesc());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getFinalSubEntityVal());
            preparedStatement.setString(i++, obj.getEntityId());
            preparedStatement.setString(i++, obj.getStatus());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [NFT_STATIC_INFO_CHANGE_EVENT_TBL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_StaticInfoChangeEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_StaticInfoChangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("NFT_STATIC_INFO_CHANGE_EVENT_TBL"));
        putList = new ArrayList<>();

        for (NFT_StaticInfoChangeEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ENTITY_TYPE",  obj.getEntityType());
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "SOLID",  obj.getSolid());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "INIT_SUB_ENTITY_VAL",  obj.getInitSubEntityVal());
            p = this.insert(p, "EVENT_TIME",  obj.getEventTime());
            p = this.insert(p, "CHANNEL_ID",  obj.getChannelId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "CHANNEL_TYPE",  obj.getChannelType());
            p = this.insert(p, "REF_TYPE",  obj.getRefType());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "REF_ID",  obj.getRefId());
            p = this.insert(p, "ENTITY_SUB_TYPE",  obj.getEntitySubType());
            p = this.insert(p, "CHANNEL_DESC",  obj.getChannelDesc());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "FINAL_SUB_ENTITY_VAL",  obj.getFinalSubEntityVal());
            p = this.insert(p, "ENTITY_ID",  obj.getEntityId());
            p = this.insert(p, "STATUS",  obj.getStatus());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("NFT_STATIC_INFO_CHANGE_EVENT_TBL"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [NFT_STATIC_INFO_CHANGE_EVENT_TBL]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [NFT_STATIC_INFO_CHANGE_EVENT_TBL]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_StaticInfoChangeEvent obj = new NFT_StaticInfoChangeEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setEntityType(rs.getString("ENTITY_TYPE"));
    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setSolid(rs.getString("SOLID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setInitSubEntityVal(rs.getString("INIT_SUB_ENTITY_VAL"));
    obj.setEventTime(rs.getString("EVENT_TIME"));
    obj.setChannelId(rs.getString("CHANNEL_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setChannelType(rs.getString("CHANNEL_TYPE"));
    obj.setRefType(rs.getString("REF_TYPE"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setRefId(rs.getString("REF_ID"));
    obj.setEntitySubType(rs.getString("ENTITY_SUB_TYPE"));
    obj.setChannelDesc(rs.getString("CHANNEL_DESC"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setFinalSubEntityVal(rs.getString("FINAL_SUB_ENTITY_VAL"));
    obj.setEntityId(rs.getString("ENTITY_ID"));
    obj.setStatus(rs.getString("STATUS"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [NFT_STATIC_INFO_CHANGE_EVENT_TBL]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_StaticInfoChangeEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_StaticInfoChangeEvent> events;
 NFT_StaticInfoChangeEvent obj = new NFT_StaticInfoChangeEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("NFT_STATIC_INFO_CHANGE_EVENT_TBL"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_StaticInfoChangeEvent obj = new NFT_StaticInfoChangeEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setEntityType(getColumnValue(rs, "ENTITY_TYPE"));
            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setSolid(getColumnValue(rs, "SOLID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setInitSubEntityVal(getColumnValue(rs, "INIT_SUB_ENTITY_VAL"));
            obj.setEventTime(getColumnValue(rs, "EVENT_TIME"));
            obj.setChannelId(getColumnValue(rs, "CHANNEL_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setChannelType(getColumnValue(rs, "CHANNEL_TYPE"));
            obj.setRefType(getColumnValue(rs, "REF_TYPE"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setRefId(getColumnValue(rs, "REF_ID"));
            obj.setEntitySubType(getColumnValue(rs, "ENTITY_SUB_TYPE"));
            obj.setChannelDesc(getColumnValue(rs, "CHANNEL_DESC"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setFinalSubEntityVal(getColumnValue(rs, "FINAL_SUB_ENTITY_VAL"));
            obj.setEntityId(getColumnValue(rs, "ENTITY_ID"));
            obj.setStatus(getColumnValue(rs, "STATUS"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [NFT_STATIC_INFO_CHANGE_EVENT_TBL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [NFT_STATIC_INFO_CHANGE_EVENT_TBL]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ENTITY_TYPE\",\"ACCT_ID\",\"SOLID\",\"USER_ID\",\"INIT_SUB_ENTITY_VAL\",\"EVENT_TIME\",\"CHANNEL_ID\",\"CUST_ID\",\"CHANNEL_TYPE\",\"REF_TYPE\",\"TRAN_DATE\",\"REF_ID\",\"ENTITY_SUB_TYPE\",\"CHANNEL_DESC\",\"HOST_USER_ID\",\"HOST_ID\",\"FINAL_SUB_ENTITY_VAL\",\"ENTITY_ID\",\"STATUS\"" +
              " FROM NFT_STATIC_INFO_CHANGE_EVENT_TBL";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ENTITY_TYPE],[ACCT_ID],[SOLID],[USER_ID],[INIT_SUB_ENTITY_VAL],[EVENT_TIME],[CHANNEL_ID],[CUST_ID],[CHANNEL_TYPE],[REF_TYPE],[TRAN_DATE],[REF_ID],[ENTITY_SUB_TYPE],[CHANNEL_DESC],[HOST_USER_ID],[HOST_ID],[FINAL_SUB_ENTITY_VAL],[ENTITY_ID],[STATUS]" +
              " FROM NFT_STATIC_INFO_CHANGE_EVENT_TBL";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ENTITY_TYPE`,`ACCT_ID`,`SOLID`,`USER_ID`,`INIT_SUB_ENTITY_VAL`,`EVENT_TIME`,`CHANNEL_ID`,`CUST_ID`,`CHANNEL_TYPE`,`REF_TYPE`,`TRAN_DATE`,`REF_ID`,`ENTITY_SUB_TYPE`,`CHANNEL_DESC`,`HOST_USER_ID`,`HOST_ID`,`FINAL_SUB_ENTITY_VAL`,`ENTITY_ID`,`STATUS`" +
              " FROM NFT_STATIC_INFO_CHANGE_EVENT_TBL";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO NFT_STATIC_INFO_CHANGE_EVENT_TBL (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ENTITY_TYPE\",\"ACCT_ID\",\"SOLID\",\"USER_ID\",\"INIT_SUB_ENTITY_VAL\",\"EVENT_TIME\",\"CHANNEL_ID\",\"CUST_ID\",\"CHANNEL_TYPE\",\"REF_TYPE\",\"TRAN_DATE\",\"REF_ID\",\"ENTITY_SUB_TYPE\",\"CHANNEL_DESC\",\"HOST_USER_ID\",\"HOST_ID\",\"FINAL_SUB_ENTITY_VAL\",\"ENTITY_ID\",\"STATUS\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ENTITY_TYPE],[ACCT_ID],[SOLID],[USER_ID],[INIT_SUB_ENTITY_VAL],[EVENT_TIME],[CHANNEL_ID],[CUST_ID],[CHANNEL_TYPE],[REF_TYPE],[TRAN_DATE],[REF_ID],[ENTITY_SUB_TYPE],[CHANNEL_DESC],[HOST_USER_ID],[HOST_ID],[FINAL_SUB_ENTITY_VAL],[ENTITY_ID],[STATUS]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ENTITY_TYPE`,`ACCT_ID`,`SOLID`,`USER_ID`,`INIT_SUB_ENTITY_VAL`,`EVENT_TIME`,`CHANNEL_ID`,`CUST_ID`,`CHANNEL_TYPE`,`REF_TYPE`,`TRAN_DATE`,`REF_ID`,`ENTITY_SUB_TYPE`,`CHANNEL_DESC`,`HOST_USER_ID`,`HOST_ID`,`FINAL_SUB_ENTITY_VAL`,`ENTITY_ID`,`STATUS`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

