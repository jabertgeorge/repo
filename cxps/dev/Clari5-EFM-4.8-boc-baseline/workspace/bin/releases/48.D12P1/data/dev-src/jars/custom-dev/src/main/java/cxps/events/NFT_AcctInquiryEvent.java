// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="NFT_ACCT_INQUIRY_EVENT_TBL", Schema="rice")
public class NFT_AcctInquiryEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=100) public String accountName;
       @Field(size=100) public String branchIdDesc;
       @Field(size=100) public String acctSolId;
       @Field(size=100) public String addEntity1;
       @Field(size=100) public String accountId;
       @Field(size=100) public String userId;
       @Field(size=100) public String enquiryChannel;
       @Field(size=100) public String custRefId;
       @Field(size=100) public String branchId;
       @Field(size=100) public String menuId;
       @Field(size=100) public String empId;
       @Field(size=100) public String channelType;
       @Field(size=100) public String custRefType;
       @Field(size=100) public String tranBrId;
       @Field(size=100) public String instrumentType;
       @Field public java.sql.Timestamp enquiryTimeStamp;
       @Field(size=100) public String channelDesc;
       @Field(size=100) public String hostId;
       @Field public Double availableBal;
       @Field(size=100) public String instrumentId;
       @Field(size=100) public String enquiryType;


    @JsonIgnore
    public ITable<NFT_AcctInquiryEvent> t = AEF.getITable(this);

	public NFT_AcctInquiryEvent(){}

    public NFT_AcctInquiryEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AcctInquiry");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));
        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

        setAccountName(json.getString("acct_name"));
        setBranchIdDesc(json.getString("BranchIdDesc"));
        setAcctSolId(json.getString("acct_sol_id"));
        setAddEntity1(json.getString("add_entity1"));
        setAccountId(json.getString("AcctId"));
        setUserId(json.getString("UserId"));
        setEnquiryChannel(json.getString("enquiry_channel"));
        setCustRefId(json.getString("acid"));
        setBranchId(json.getString("BranchId"));
        setMenuId(json.getString("MenuId"));
        setEmpId(json.getString("EmpId"));
        setChannelType(json.getString("channel_type"));
        setCustRefType(json.getString("cust_ref_type"));
        setTranBrId(json.getString("tran_br_id"));
        setInstrumentType(json.getString("instrument_type"));
        setEnquiryTimeStamp(EventHelper.toTimestamp(json.getString("enquiry_time_stamp")));
        setChannelDesc(json.getString("channel_desc"));
        setHostId(json.getString("HostId"));
        setAvailableBal(EventHelper.toDouble(json.getString("avl_bal")));
        setInstrumentId(json.getString("instrument_id"));
        setEnquiryType(json.getString("enquiry_type"));

        setDerivedValues();
    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAccountName(){ return accountName; }
    public String getBranchIdDesc(){ return branchIdDesc; }
    public String getAcctSolId(){ return acctSolId; }
    public String getAddEntity1(){ return addEntity1; }
    public String getAccountId(){ return accountId; }
    public String getUserId(){ return userId; }
    public String getEnquiryChannel(){ return enquiryChannel; }
    public String getCustRefId(){ return custRefId; }
    public String getBranchId(){ return branchId; }
    public String getMenuId(){ return menuId; }
    public String getEmpId(){ return empId; }
    public String getChannelType(){ return channelType; }
    public String getCustRefType(){ return custRefType; }
    public String getTranBrId(){ return tranBrId; }
    public String getInstrumentType(){ return instrumentType; }
    public java.sql.Timestamp getEnquiryTimeStamp(){ return enquiryTimeStamp; }
    public String getChannelDesc(){ return channelDesc; }
    public String getHostId(){ return hostId; }
    public Double getAvailableBal(){ return availableBal; }
    public String getInstrumentId(){ return instrumentId; }
    public String getEnquiryType(){ return enquiryType; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAccountName(String val){ this.accountName = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setAddEntity1(String val){ this.addEntity1 = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setEnquiryChannel(String val){ this.enquiryChannel = val; }
    public void setCustRefId(String val){ this.custRefId = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setMenuId(String val){ this.menuId = val; }
    public void setEmpId(String val){ this.empId = val; }
    public void setChannelType(String val){ this.channelType = val; }
    public void setCustRefType(String val){ this.custRefType = val; }
    public void setTranBrId(String val){ this.tranBrId = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setEnquiryTimeStamp(java.sql.Timestamp val){ this.enquiryTimeStamp = val; }
    public void setChannelDesc(String val){ this.channelDesc = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setAvailableBal(Double val){ this.availableBal = val; }
    public void setInstrumentId(String val){ this.instrumentId = val; }
    public void setEnquiryType(String val){ this.enquiryType = val; }

    /* Custom Getters*/
    public Double getAvl_bal() { return this.availableBal; }

    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_AcctInquiryEvent");
	Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        String hostAcctKey = getCustRefId();
        if (hostAcctKey == null) {
            return wsInfoSet;
        }

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        String cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), hostAcctKey);
        String cxCifId = h.getCxCifIdGivenCxAcctKey(session, cxAcctKey);

        WorkspaceInfo wi = new WorkspaceInfo("Account", cxAcctKey);
        if (h.isValidCxKey(WorkspaceName.CUSTOMER, cxCifId)) {
            wi.addParam("cxCifID", cxCifId);
        }

        wsInfoSet.add(wi);


        if (null == getUserId()) {
            return wsInfoSet;
        }

        String cxUserKey = h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), getUserId());

        if (!Hfdb.isValidRefCode("VIRTUAL_USER", cxUserKey)) {
            return wsInfoSet;
        }

        String cxBranchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(), getBranchId());
        wi = new WorkspaceInfo("User", cxUserKey);

        wsInfoSet.add(wi);
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        json.put("account_name",getAccountName());
        json.put("branch_id_desc",getBranchIdDesc());
        json.put("account_id",getAccountId());
        json.put("user_id",getUserId());
        json.put("menu_id",getMenuId());
        json.put("emp_id",getEmpId());
        json.put("host_id",getHostId());
        json.put("available_bal",getAvailableBal());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AcctInquiry");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AcctInquiry");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
