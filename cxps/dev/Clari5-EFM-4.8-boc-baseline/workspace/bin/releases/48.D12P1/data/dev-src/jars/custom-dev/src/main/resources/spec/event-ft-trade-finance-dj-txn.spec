cxps.events.event.ft-tradefinancedj {

  table-name : EVENT_FT_TRADE_FINANCE
  event-mnemonic = TF
  workspaces : {
  	            ACCOUNT :  cust-id
  }

  event-attributes : {
    event-name : {db : true, raw_name: event-name, type : "string:100"}
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    cust-id : {db : true, raw_name: cust_id, type : "string:100"}
    source:{type:"string:100",db:true,raw_name:source}
    eventts:{type:timestamp,db:true,raw_name:eventts}
    created_on:{type:timestamp,db:true,raw_name:created_on}
    updated_on:{type:timestamp,db:true,raw_name:updated_on}
    host-id:{type:"string:100",db:true,raw_name:host-id}
    system:{type:"string:100",db:true,raw_name:SYSTEM}
    max-score : {db : true ,raw_name : max_score ,type : "string:100"}
    rule-name :{type:"string:100",db:true,raw_name:rule_name}
    dj-id :{type:"string:100",db:true,raw_name:dj_id}
    list-name :{type:"string:100",db:true,raw_name:list_name}
    sanctions-references-list-provider-code:{type:"string:100",db:true,raw_name:sanctions_references_list_provider_code}
    description-code:{type:"string:100",db:true,raw_name:description_code}
    tran-date : {db : true ,raw_name : sys_time ,type : timestamp}
    tran-date : {db : true ,raw_name : tran_date ,type : timestamp}
    score :{type:"string:100",db:true,raw_name:score}
    name:{type:"string:100",db:true,raw_name:name}
    query-value:{type:"string:100",db:true,raw_name:query_value}
    value:{type:"string:100",db:true,raw_name:value}
    tran_id:{type:"string:100",db:true,raw_name:tran_id}
    part-tran-srl-num : {db : true ,raw_name : part_tran_srl_num ,type : "string:100"}
  }
}
