# This file defines the configuration for the RDBMS and Other modules

C3P0_MIN_POOL = ${NOESIS_C3P0_MIN_POOL}
C3P0_MAX_POOL = ${NOESIS_C3P0_MAX_POOL}

clari5 {

  bootstrap-order = [dbcon, rdbms, hfdb, NOESIS, HITPS-NOESIS, risk, newcmsjira, ace, rbi, ACE, CMS, wl, GENIE, SYNCER, MT]

  log-folder = "."

  logger {
    cxps.noesis {
      include "logger-module.conf"
      file = "noesis-clari5.log"
    }
  }

  enable-scn-params: N # flag to enable or disable scnparams, default is N. If scnparam is required, change it to Y

  modules {

    include "dbcon-module.conf"
    include "rdbms.conf"
    include "hfdb-module.conf"
    include "risk-module.conf"
    include "newcmsjira-module.conf"
    include "wl-module.conf"
    include "ace-module.conf"
    include "rbi-module.conf"

    NOESIS {
      class: cxps.apex.ed.services.ProcessEvent
      resource-type: RUNNABLE

      q-name: NOESIS
      num-workers : ${NOESIS_WORKERS}
      bulk-size : 500

      num-ack-dispatcher : 5
      max-ack-holding-buffer-capacity : 50000

      ack-send-memory-limit-in-kb : 2048
      ack-send-batch : 100

	  mq-connection-timeout: 60000
	  mq-socket-timeout: 300000
      ignored-entities : [ ]

      factHandlers: {
        AML: "cxps.eoi.utils.FactSender"
        EFM: "cxps.eoi.utils.FactSender"
        STAFF: "cxps.eoi.utils.FactSender"
        RDE: "cxps.eoi.utils.FactSender"
      }

    }
	MT{
            class:clari5.custom.dev.daemons.MTDaemon
            resource-type: RUNNABLE
            num-workers: 1
            bulk-size: 100
            time-out: 10000
        }

    HITPS-NOESIS {
      class: cxps.apex.ed.services.HiTpsProcessEvent
      resource-type: RUNNABLE

      q-name: HITPS-NOESIS
      num-workers : 20 // hardcoded for high tps
      bulk-size : 500

      num-ack-dispatcher : 5
      max-ack-holding-buffer-capacity : 50000

      ack-send-memory-limit-in-kb : 2048
      ack-send-batch : 100

      compression: false
      ignored-entities : [ ]

      factHandlers: {
        AML: "cxps.eoi.utils.FactSender"
        EFM: "cxps.eoi.utils.FactSender"
        STAFF: "cxps.eoi.utils.FactSender"
        RDE: "cxps.eoi.utils.FactSender"
      }
    }

    GENIE {
      class : clari5.trace.GenieDaemon
      resource-type : RUNNABLE
      num-workers : 5
      time-out : 10000
      q-name : GENIE
      bulk-size : 500
      genieUrl:${GENIE_HOST}/api/predict
    }

    ACE {
      class: clari5.aml.Ace
      resource-type: RUNNABLE
      num-workers: ${ACE_WORKERS}
      bulk-size: 10
      time-out: 10000
      q-name: ACE
      new-factor: ${ACE_NEW_FACTOR}
      min-wait-period-in-sec: 300
      case-closure-handler: "clari5.aml.closure.DefaultCaseClosureHandler"
    }

    CMS {
      class: clari5.aml.cms.newjira.CMSProcessor
      resource-type: RUNNABLE
      num-workers: ${CMS_CASE_CLOSURE_WORKERS}
      bulk-size: 10
      time-out: 10000
      q-name: CMS
    }

    SYNCER {
      class : clari5.aml.cms.newjira.SyncerManager
      resource-type : DAEMON
      max-sleep : 5000
      incident-state : Created
      non-editable-states : [resolved, closed]
      syncer-lock-timeout-sec : 120
      projects : {
        RDE: ${RDE_SYNC_WORKERS}
        EFM: ${EFM_SYNC_WORKERS}
        AML: ${AML_SYNC_WORKERS}
      }
    }

    include "jirapurge-daemon.conf"
  }
}

include "custom-qconsumer-clari5.conf"
include "custom-amlbsf-clari5.conf"
include "custom-noesis-clari5.conf"
