package clari5.custom.util.xmltojson;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;


public class RequestHandler extends Application {

    private Set<Object> singletons = new HashSet<>();

    @Override
    public Set<Class<?>> getClasses() {
        return null;
    }

    public RequestHandler() {
        singletons.add(new XmlToJsonConverter());
 }

    @Override
    public Set<Object> getSingletons() {
        return this.singletons;
    }
}
