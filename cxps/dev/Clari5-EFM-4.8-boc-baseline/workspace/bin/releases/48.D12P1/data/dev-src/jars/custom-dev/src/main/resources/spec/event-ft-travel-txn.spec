cxps.events.event.ft-travel-txn {
   table-name : EVENT_FT_TRAVELTXN
   event-mnemonic: FT
   workspaces : {
	   CUSTOMER: cust-id,
	   ACCOUNT : cx-acct-id
  	}
   event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    host-id : {db : true ,raw_name : host-id ,type : "string:20"}
    card-no : {db : true ,raw_name : CardNumber ,type : "string:20"}
    channel : {db : true ,raw_name : channel ,type : "string:20"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"} 
    txn-amt : {db : true ,raw_name : TRAN_AMT ,type : "number:11,2"}
    lcy-curr : {db : true ,raw_name : LCY_CURR,type : "number:11,2"}
    lcy-amt : {db : true ,raw_name : LCY_AMT,type : "number:11,2"}
    txn-cntry : {db : true ,raw_name : Country_Of_transaction ,type : "string:20"}
    txn-cntry-code : {db : true ,raw_name : CountryCodeTransaction ,type : "string:20"}
    txn-code : {db : true ,raw_name : tran_code ,type : "string:20"}
    txn-status : {db : true ,raw_name : pstd-flg ,type : "string:20"}
    tran_id:{type:"string:100",db:true,raw_name:tran_id}
    source-or-dest-acct-id : {db : true ,raw_name : account-id ,type : "string:20"}
    txn-dr-cr : {db : true ,raw_name : part-tran-type ,type : "string:20"}
    tran-srl-no : {db : true ,raw_name : Part_tran_Srl_Num ,type : "string:20"}
    part-tran-srl-num : {db : true ,raw_name : part_tran_srl_num ,type : "string:20"}
    tran-type : {db : true ,raw_name : tran-type ,type : "string:20"}
    tran-sub-type : {db : true ,raw_name : tran-sub-type ,type : "string:20"}
    txn-curr : {db : true ,raw_name : TRAN_CURR ,type : "string:20"}
    system-time : {db : true ,raw_name : sys-time ,type : timestamp}
    cust-id : {db : true ,raw_name : cust-id ,type : "string:20"}
    online-batch : {db : true ,raw_name : online-batch ,type : "string:20"}
    event-time : {db : true ,raw_name : eventts ,type : "string:20"}
    msg-source : {db : true ,raw_name : source ,type : "string:20"} 
    txn-date : {db : true ,raw_name : Tran_Date ,type : timestamp}
    tran-date : {db : true ,raw_name : tran_date ,type : timestamp}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {raw_name : reservedfield4 ,type : "string:100"}
    add-entity-id5 : {raw_name : reservedfield5 ,type : "string:100"}
    	
   }
}

