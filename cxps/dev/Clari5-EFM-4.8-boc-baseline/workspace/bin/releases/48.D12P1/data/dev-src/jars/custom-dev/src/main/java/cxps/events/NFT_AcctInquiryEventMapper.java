// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AcctInquiryEventMapper extends EventMapper<NFT_AcctInquiryEvent> {

public NFT_AcctInquiryEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AcctInquiryEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AcctInquiryEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AcctInquiryEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AcctInquiryEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AcctInquiryEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AcctInquiryEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAccountName());
            preparedStatement.setString(i++, obj.getBranchIdDesc());
            preparedStatement.setString(i++, obj.getAcctSolId());
            preparedStatement.setString(i++, obj.getAddEntity1());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getEnquiryChannel());
            preparedStatement.setString(i++, obj.getCustRefId());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getMenuId());
            preparedStatement.setString(i++, obj.getEmpId());
            preparedStatement.setString(i++, obj.getChannelType());
            preparedStatement.setString(i++, obj.getCustRefType());
            preparedStatement.setString(i++, obj.getTranBrId());
            preparedStatement.setString(i++, obj.getInstrumentType());
            preparedStatement.setTimestamp(i++, obj.getEnquiryTimeStamp());
            preparedStatement.setString(i++, obj.getChannelDesc());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getAvailableBal());
            preparedStatement.setString(i++, obj.getInstrumentId());
            preparedStatement.setString(i++, obj.getEnquiryType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [NFT_ACCT_INQUIRY_EVENT_TBL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AcctInquiryEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AcctInquiryEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("NFT_ACCT_INQUIRY_EVENT_TBL"));
        putList = new ArrayList<>();

        for (NFT_AcctInquiryEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCOUNT_NAME",  obj.getAccountName());
            p = this.insert(p, "BRANCH_ID_DESC",  obj.getBranchIdDesc());
            p = this.insert(p, "ACCT_SOL_ID",  obj.getAcctSolId());
            p = this.insert(p, "ADD_ENTITY1",  obj.getAddEntity1());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "ENQUIRY_CHANNEL",  obj.getEnquiryChannel());
            p = this.insert(p, "CUST_REF_ID",  obj.getCustRefId());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "MENU_ID",  obj.getMenuId());
            p = this.insert(p, "EMP_ID",  obj.getEmpId());
            p = this.insert(p, "CHANNEL_TYPE",  obj.getChannelType());
            p = this.insert(p, "CUST_REF_TYPE",  obj.getCustRefType());
            p = this.insert(p, "TRAN_BR_ID",  obj.getTranBrId());
            p = this.insert(p, "INSTRUMENT_TYPE",  obj.getInstrumentType());
            p = this.insert(p, "ENQUIRY_TIME_STAMP", String.valueOf(obj.getEnquiryTimeStamp()));
            p = this.insert(p, "CHANNEL_DESC",  obj.getChannelDesc());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "AVAILABLE_BAL", String.valueOf(obj.getAvailableBal()));
            p = this.insert(p, "INSTRUMENT_ID",  obj.getInstrumentId());
            p = this.insert(p, "ENQUIRY_TYPE",  obj.getEnquiryType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("NFT_ACCT_INQUIRY_EVENT_TBL"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [NFT_ACCT_INQUIRY_EVENT_TBL]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [NFT_ACCT_INQUIRY_EVENT_TBL]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AcctInquiryEvent obj = new NFT_AcctInquiryEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAccountName(rs.getString("ACCOUNT_NAME"));
    obj.setBranchIdDesc(rs.getString("BRANCH_ID_DESC"));
    obj.setAcctSolId(rs.getString("ACCT_SOL_ID"));
    obj.setAddEntity1(rs.getString("ADD_ENTITY1"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setEnquiryChannel(rs.getString("ENQUIRY_CHANNEL"));
    obj.setCustRefId(rs.getString("CUST_REF_ID"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setMenuId(rs.getString("MENU_ID"));
    obj.setEmpId(rs.getString("EMP_ID"));
    obj.setChannelType(rs.getString("CHANNEL_TYPE"));
    obj.setCustRefType(rs.getString("CUST_REF_TYPE"));
    obj.setTranBrId(rs.getString("TRAN_BR_ID"));
    obj.setInstrumentType(rs.getString("INSTRUMENT_TYPE"));
    obj.setEnquiryTimeStamp(rs.getTimestamp("ENQUIRY_TIME_STAMP"));
    obj.setChannelDesc(rs.getString("CHANNEL_DESC"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setAvailableBal(rs.getDouble("AVAILABLE_BAL"));
    obj.setInstrumentId(rs.getString("INSTRUMENT_ID"));
    obj.setEnquiryType(rs.getString("ENQUIRY_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [NFT_ACCT_INQUIRY_EVENT_TBL]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AcctInquiryEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AcctInquiryEvent> events;
 NFT_AcctInquiryEvent obj = new NFT_AcctInquiryEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("NFT_ACCT_INQUIRY_EVENT_TBL"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AcctInquiryEvent obj = new NFT_AcctInquiryEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAccountName(getColumnValue(rs, "ACCOUNT_NAME"));
            obj.setBranchIdDesc(getColumnValue(rs, "BRANCH_ID_DESC"));
            obj.setAcctSolId(getColumnValue(rs, "ACCT_SOL_ID"));
            obj.setAddEntity1(getColumnValue(rs, "ADD_ENTITY1"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setEnquiryChannel(getColumnValue(rs, "ENQUIRY_CHANNEL"));
            obj.setCustRefId(getColumnValue(rs, "CUST_REF_ID"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setMenuId(getColumnValue(rs, "MENU_ID"));
            obj.setEmpId(getColumnValue(rs, "EMP_ID"));
            obj.setChannelType(getColumnValue(rs, "CHANNEL_TYPE"));
            obj.setCustRefType(getColumnValue(rs, "CUST_REF_TYPE"));
            obj.setTranBrId(getColumnValue(rs, "TRAN_BR_ID"));
            obj.setInstrumentType(getColumnValue(rs, "INSTRUMENT_TYPE"));
            obj.setEnquiryTimeStamp(EventHelper.toTimestamp(getColumnValue(rs, "ENQUIRY_TIME_STAMP")));
            obj.setChannelDesc(getColumnValue(rs, "CHANNEL_DESC"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setAvailableBal(EventHelper.toDouble(getColumnValue(rs, "AVAILABLE_BAL")));
            obj.setInstrumentId(getColumnValue(rs, "INSTRUMENT_ID"));
            obj.setEnquiryType(getColumnValue(rs, "ENQUIRY_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [NFT_ACCT_INQUIRY_EVENT_TBL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [NFT_ACCT_INQUIRY_EVENT_TBL]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCOUNT_NAME\",\"BRANCH_ID_DESC\",\"ACCT_SOL_ID\",\"ADD_ENTITY1\",\"ACCOUNT_ID\",\"USER_ID\",\"ENQUIRY_CHANNEL\",\"CUST_REF_ID\",\"BRANCH_ID\",\"MENU_ID\",\"EMP_ID\",\"CHANNEL_TYPE\",\"CUST_REF_TYPE\",\"TRAN_BR_ID\",\"INSTRUMENT_TYPE\",\"ENQUIRY_TIME_STAMP\",\"CHANNEL_DESC\",\"HOST_ID\",\"AVAILABLE_BAL\",\"INSTRUMENT_ID\",\"ENQUIRY_TYPE\"" +
              " FROM NFT_ACCT_INQUIRY_EVENT_TBL";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCOUNT_NAME],[BRANCH_ID_DESC],[ACCT_SOL_ID],[ADD_ENTITY1],[ACCOUNT_ID],[USER_ID],[ENQUIRY_CHANNEL],[CUST_REF_ID],[BRANCH_ID],[MENU_ID],[EMP_ID],[CHANNEL_TYPE],[CUST_REF_TYPE],[TRAN_BR_ID],[INSTRUMENT_TYPE],[ENQUIRY_TIME_STAMP],[CHANNEL_DESC],[HOST_ID],[AVAILABLE_BAL],[INSTRUMENT_ID],[ENQUIRY_TYPE]" +
              " FROM NFT_ACCT_INQUIRY_EVENT_TBL";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCOUNT_NAME`,`BRANCH_ID_DESC`,`ACCT_SOL_ID`,`ADD_ENTITY1`,`ACCOUNT_ID`,`USER_ID`,`ENQUIRY_CHANNEL`,`CUST_REF_ID`,`BRANCH_ID`,`MENU_ID`,`EMP_ID`,`CHANNEL_TYPE`,`CUST_REF_TYPE`,`TRAN_BR_ID`,`INSTRUMENT_TYPE`,`ENQUIRY_TIME_STAMP`,`CHANNEL_DESC`,`HOST_ID`,`AVAILABLE_BAL`,`INSTRUMENT_ID`,`ENQUIRY_TYPE`" +
              " FROM NFT_ACCT_INQUIRY_EVENT_TBL";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO NFT_ACCT_INQUIRY_EVENT_TBL (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCOUNT_NAME\",\"BRANCH_ID_DESC\",\"ACCT_SOL_ID\",\"ADD_ENTITY1\",\"ACCOUNT_ID\",\"USER_ID\",\"ENQUIRY_CHANNEL\",\"CUST_REF_ID\",\"BRANCH_ID\",\"MENU_ID\",\"EMP_ID\",\"CHANNEL_TYPE\",\"CUST_REF_TYPE\",\"TRAN_BR_ID\",\"INSTRUMENT_TYPE\",\"ENQUIRY_TIME_STAMP\",\"CHANNEL_DESC\",\"HOST_ID\",\"AVAILABLE_BAL\",\"INSTRUMENT_ID\",\"ENQUIRY_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCOUNT_NAME],[BRANCH_ID_DESC],[ACCT_SOL_ID],[ADD_ENTITY1],[ACCOUNT_ID],[USER_ID],[ENQUIRY_CHANNEL],[CUST_REF_ID],[BRANCH_ID],[MENU_ID],[EMP_ID],[CHANNEL_TYPE],[CUST_REF_TYPE],[TRAN_BR_ID],[INSTRUMENT_TYPE],[ENQUIRY_TIME_STAMP],[CHANNEL_DESC],[HOST_ID],[AVAILABLE_BAL],[INSTRUMENT_ID],[ENQUIRY_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCOUNT_NAME`,`BRANCH_ID_DESC`,`ACCT_SOL_ID`,`ADD_ENTITY1`,`ACCOUNT_ID`,`USER_ID`,`ENQUIRY_CHANNEL`,`CUST_REF_ID`,`BRANCH_ID`,`MENU_ID`,`EMP_ID`,`CHANNEL_TYPE`,`CUST_REF_TYPE`,`TRAN_BR_ID`,`INSTRUMENT_TYPE`,`ENQUIRY_TIME_STAMP`,`CHANNEL_DESC`,`HOST_ID`,`AVAILABLE_BAL`,`INSTRUMENT_ID`,`ENQUIRY_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

