var reqstPage = "";
var tabCount = 5;
var tabTemplate = "<li  class='ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active' style='list-style: none;float: left; position: relative;top: 0; margin: 1px .2em 0 0;border-bottom-width: 0; padding: 0; white-space: nowrap;' aria-controls='#{aria_control}' id='#{tab_id}' style='#{backStyle}' >" +
    "<span style='#{tabImg}' role='presentation'></span>" +
    "<a class='ui-tabs-anchor' style='color:#000000;font-size: 1.2em;overflow:hidden;width:105px;margin-left:-4px;' title='#{titleText}' onClick='#{clckFun}' oncontextmenu='return false;' href='#{href}'>#{label}</a> " +
    "<span class='ui-icon ui-icon-close' style='float:right;margin-top:-2px;margin-right:-1px;' role='presentation'>Remove Tab</span>" +
    "</li>";
var tabCounter = 1;
var tabCaseCount = new Array();
var tabCustCount = new Array();
var tabNicCount = new Array();
var tabAcctCount = new Array();
var tabEmpCount = new Array();
var tabTranCount = new Array();
var tabDealCount = new Array();
var tabPawnCount = new Array();
var tabTradeCount = new Array();
var tabLockerCount = new Array();
var tabLeaseCount = new Array();
var tabBillCount = new Array();

var tabs = null;
var treeId = "";
var custRmlId = "";
var nicId = "";
var dealId="";
var dealCount="";
var pawnId="";
var pawnCount="";
var tradeId="";
var tradeCount="";
var lockerId="";
var lockerCount="";
var leaseId="";
var leaseCount="";
var billId="";
var billCount="";
var cxcifId = "";
var cxacctcid = "";

function init() {
    initTxn();
    initCust();
    initAcct();
    initEmp();
    initCase();
    initNic();
    initdeal();
    initPawn();
    initLocker();
    initTrade();
    initLease();
    initBill();




    // $(document).tooltip();
    $("#tabNavUl").hide();
    $("#canvas").hide();
    tabs = $("#canvas");
    $("#gridTabs").tabs();
    var firstClick = 1;
    $("#refId").val("");
    loadIODetailsFromJira(top.refId, top.cName, top.src);
    $(".clckBut").click(function () {
        var brdVal = "";
        $("#leftForm").validationEngine('validate', {scroll: false});
        if (firstClick == 1) {
            intializeArray();
            treeObj = "";
            cxacctcid = "";
            cxcifId = "";
            nicId="";
            $("#comptDiv").empty();
            if ($("#refId").val() != "") {
                confirmTab($(this).text(), $("#refId").val());
            } else if ($(this).text() != "Staff" && $(this).text() != "NIC") {
                // Any tab other than Staff
                brdVal = "2px solid #14143D";//+getColor($(this).text());
                $("#srchWindow").css('border', brdVal);
                $(this).removeClass("clckBut");
                $(this).addClass("clckButactive");
                $(this).css('line-height', '30px;');
                $("#clSchWd").css('border-right', brdVal);
                $("#clSchWd").css('border-top', brdVal);
                $("#clSchWd").css('border-bottom', brdVal);
                $("#srchWindow").show();
                $("#clSchWd").show();
                firstClick++;
                clearTabs();
                if ($(this).text() == "Case") {
                    loadCase("comptDiv", caseSrch);
                    $("#fromDate").datepicker();
                    $("#toDate").datepicker();
                    $("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd");
                    $("#toDate").datepicker("option", "dateFormat", "yy-mm-dd");
                    $("#fromDate").datepicker("option", "maxDate", new Date());
                    $("#toDate").datepicker("option", "maxDate", new Date());
                }

                if ($(this).text() == "Customer") {
                    loadCase("comptDiv", custSrch);
                }

                if ($(this).text() == "Account") {
                    loadCase("comptDiv", acctSrch);
                }

                /*  No panel
                 if ($(this).text() == "Staff") {
                 loadCase("comptDiv", empSrch);
                 }
                 */

                if ($(this).text() == "Transaction") {
                    loadCase("comptDiv", tranSrch);
                    $("#fromDate").datepicker();
                    $("#toDate").datepicker();
                    $("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd");
                    $("#toDate").datepicker("option", "dateFormat", "yy-mm-dd");
                    $("#fromDate").datepicker("option", "maxDate", new Date());
                    $("#toDate").datepicker("option", "maxDate", new Date());
                }
            }

        }
        else {
            var r = confirm("Do you want to do new search?");
            if (r == true) {
                intializeArray();
                treeObj = "";
                cxacctcid = "";
                cxcifId = "";
                nicId="";
                if ($("#refId").val() == "" && $(this).text() != "Staff" && $(this).text() != "NIC") {
                    $("#srchWindow").show();
                    brdVal = "2px solid #14143D";//+getColor($(this).text());
                    $("#srchWindow").css('border', brdVal);
                    $("div#leftPanel").children().removeClass("clckButactive");
                    $("div#leftPanel").children().addClass("clckBut");
                    $(this).addClass("clckButactive");
                    $("#clSchWd").css('border-right', brdVal);
                    $("#clSchWd").css('border-top', brdVal);
                    $("#clSchWd").css('border-bottom', brdVal);
                    $(this).css('line-height', '30px;');
                    $("#clSchWd").show();
                    $("#comptDiv").empty();
                    clearTabs();
                    confirmTab($(this).text(), $("#refId").val());
                } else {
                    $("#comptDiv").empty();
                    $("#srchWindow").hide();
                    $("#clSchWd").hide();
                    $("div#leftPanel").children().removeClass("clckButactive");
                    $(this).addClass("clckButactive");
                    clearTabs();
                    confirmTab($(this).text(), $("#refId").val());
                }
            }
            firstClick++;
        }
    });

    tabs.delegate("span.ui-icon-close", "click", function () {
        var panelId = $(this).closest("li").remove().attr("aria-controls");
        $("#" + panelId).remove();
        if ($("ul#tabNavUl").children().length == 0) {
            $("#tabNavUl").hide();
            $("#canvas").hide();
        }
        clearArray(panelId);
        //tabs.tabs("refresh");
        $("ul#tabNavUl li:last-child").addClass("ui-tabs-active ui-state-active");
        var actId = $("ul#tabNavUl li:last-child").attr("aria-controls");
        $("div#" + actId).show();
        //tabs.tabs("refresh");
        //var tabSize = tabCaseCount.length + tabAcctCount.length + tabCustCount.length + tabTranCount.length+tabNicCount.length;
        tabSizeexpander();
    });

    if (window.location.href.split("?")[1]) {
        var params = window.location.href.split("?")[1].split("&");
        for (var param of params) {
            if (param.split("=")[1] && param.split("=")[0] === 'refId') {
                let urlRefId = param.split("=")[1];
                document.getElementById("refId").value = urlRefId;
            }

            if (param.split("=")[1] && param.split("=")[0] === 'cName') {
                let urlCName = param.split("=")[1];
                let clickOn = "";
                if (urlCName && urlCName === "Customer") {
                    clickOn = "custDiv";
                } else if (urlCName && urlCName === "Account") {
                    clickOn = "acctDiv";
                } else if (urlCName && urlCName === "Staff") {
                    clickOn = "empDiv";
                }
                if (clickOn)
                    document.getElementById(clickOn).click();
            }
        }
    }
}

var tabPosition = "";
function knowTabId(id, count) {
    var TabID = ($(id).attr("href").split("--")[0]).split("#")[1];
    if (TabID == "Account") {
        accountID = $(id).attr("href").split("--")[1];
        acctCount = count + 1;
        tabPosition = count;
    } else if (TabID == "Staff") {
        employeeId = $(id).attr("href").split("--")[1];
        empCount = count + 1;
        tabPosition = count;
    } else if (TabID == "NIC") {
         nicId = $(id).attr("href").split("--")[1];
         nicCount = count + 1;
         tabPosition = count;
    }
    else if (TabID == "Deal") {
         dealId = $(id).attr("href").split("--")[1];
         dealCount = count + 1;
         tabPosition = count;
    }
    else if (TabID == "Pawn") {
         pawnId = $(id).attr("href").split("--")[1];
         pawnCount = count + 1;
         tabPosition = count;
    }
     else if (TabID == "Trade") {
     tradeId = $(id).attr("href").split("--")[1];
     tradeCount = count + 1;
     tabPosition = count;
    }
    else if (TabID == "FLease") {
     leaseId = $(id).attr("href").split("--")[1];
     leaseCount = count + 1;
     tabPosition = count;
    }
    else if (TabID == "Locker") {
     lockerId = $(id).attr("href").split("--")[1];
     lockerCount = count + 1;
     tabPosition = count;
    }
    else if (TabID == "Bill") {
     billId = $(id).attr("href").split("--")[1];
     billCount = count + 1;
     tabPosition = count;
    }
      else if (TabID == "Customer") {
        customerId = $(id).attr("href").split("--")[1];
        custCount = count + 1;
        tabPosition = count;
    } else if (TabID == "Case") {
        caseId = $(id).attr("href").split("--")[1];
        caseCount = count + 1;
        tabPosition = count;
    } else if (TabID == "Transaction") {
        transactionId = $(id).attr("href").split("--")[1];
        tranCount = count + 1;
        tabPosition = count;
    }
    activateCanvasTabId($(id).attr("href"));
}

function intializeArray() {
    tabCaseCount = new Array();
    tabCustCount = new Array();
    tabNicCount = new Array();
    tabDealCount = new Array();
    tabPawnCount = new Array();
    tabTradeCount = new Array();
    tabLeaseCount = new Array();
    tabBillCount = new Array();
    tabLockerCount = new Array();
    tabAcctCount = new Array();
    tabEmpCount = new Array();
    tabTranCount = new Array();
}

function confirmTab(id, value) {

    if (id == "Case") {

        if (value != "") {
            clearTabs();
            addTab(id, value);
        } else {
            loadCase("comptDiv", caseSrch);
            setSrchCss();
            $("#fromDate").datepicker();
            $("#toDate").datepicker();
            $("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd");
            $("#toDate").datepicker("option", "dateFormat", "yy-mm-dd");
            $("#fromDate").datepicker("option", "maxDate", new Date());
            $("#toDate").datepicker("option", "maxDate", new Date());
        }
    }

    if (id == "Customer") {
        if (value != "") {
            clearTabs();
            addTab(id, value);
        }
        else {
            loadCase("comptDiv", custSrch);
            setSrchCss();
        }
    }

     if (id == "NIC") {
            if (value != "") {
                clearTabs();
                addTab(id, value);
            }
        }

    if (id == "Account") {
        if (value != "") {
            clearTabs();
            addTab(id, value);
        } else {
            loadCase("comptDiv", acctSrch);
            setSrchCss();
        }
    }

    if (id == "Staff") {
        if (value != "") {
            clearTabs();
            addTab(id, value);
        }
        /* No Search Panel
         else {
         loadCase("comptDiv", empSrch);
         setSrchCss();
         }*/
    }

    if (id == "Transaction") {
        if (value != "") {
            clearTabs();
            addTab(id, value);
        } else {
            loadCase("comptDiv", tranSrch);
            setSrchCss();
            $("#fromDate").datepicker();
            $("#toDate").datepicker();
            $("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd");
            $("#toDate").datepicker("option", "dateFormat", "yy-mm-dd");
            $("#fromDate").datepicker("option", "maxDate", new Date());
            $("#toDate").datepicker("option", "maxDate", new Date());
        }
    }

}

function loadIODetailsFromJira(refId, cName, src) {
    if (src == "jira") {
        confirmTab(cName, refId);
    }
}

function clearArray(panelId) {
    if (panelId.split("-")[0] == "Customer") {
        tabCustCount.splice(tabCustCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Deal") {
        tabDealCount.splice(tabDealCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Pawn") {
        tabPawnCount.splice(tabPawnCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Trade") {
        tabTradeCount.splice(tabTradeCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Bill") {
        tabBillCount.splice(tabBillCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "FLease") {
        tabLeaseCount.splice(tabLeaseCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Locker") {
        tabLockerCount.splice(tabLockerCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "NIC") {
         tabNicCount.splice(tabNicCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Account") {
        tabAcctCount.splice(tabAcctCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Staff") {
        tabEmpCount.splice(tabEmpCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Case") {
        tabCaseCount.splice(tabCaseCount.indexOf(panelId, 1));
    }
    if (panelId.split("-")[0] == "Transaction") {
        tabTranCount.splice(tabTranCount.indexOf(panelId, 1));
    }
}

function setSrchCss() {
    if ($("#srchWindow").attr('style').indexOf("block") > 0) {
        toggleClassByID("clSchWd", "clsWindow", "opnWindow");
    } else {
        $("#clSchWd").toggleClass("opnWindow");
        $("#clSchWd").toggleClass("clsWindow");
    }
    toggleClassByID("imgWindow", "clSrchInsideDiv", "clSrchOutsideDiv");
}

function clearTabs() {
    $("#canvas").empty();
    $("#canvas").attr('style', 'display:none');
    $("#canvas").append("<ul id='tabNavUl' class='ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'' style='display: none;' role='tablist'></ul>");
    intializeArray();
}

function getColor(reqFor) {
    if (reqFor == "Customer") {
        return bg = "#C2C2E0";
    }
    if (reqFor == "Lease") {
        return bg = "#C2C2E0";
    }
    if (reqFor == "Deal") {
       return bg = "#C2C2E0";
    }
    if (reqFor == "Pawn") {
       return bg = "#C2C2E0";
    }
    if (reqFor == "Bill") {
        return bg = "#C2C2E0";
    }
     if (reqFor == "Trade") {
        return bg = "#C2C2E0";
    }
    if (reqFor == "Locker") {
        return bg = "#C2C2E0";
    }
    if (reqFor == "NIC") {
        return bg = "#C2C2E0";
    }
    if (reqFor == "Account") {
        return bg = "#8585C2";
    }
    if (reqFor == "Case") {
        return bg = "#5A5A93";
    }
    if (reqFor == "Transaction") {
        return bg = "#56567C";
    }
}

function loadCase(mainId, divText) {
    $("#" + mainId).append(divText);
}

function addTab(reqFor, dataId) {
    reqstPage = reqFor;
    var bg = "";
    var wid = "";
    var label = "";
    if (dataId.length > 0) {
        if (dataId.includes("--DEAL")){
        label = dataId.replace("--DEAL","") || "Tab " + tabCounter;
        reqFor = "Deal";}
        else if (dataId.includes("--TRADE")){
        label = dataId.replace("--TRADE","") || "Tab " + tabCounter;
        reqFor = "Trade";}
        else if (dataId.includes("--PAWN")){
        label = dataId.replace("--PAWN","") || "Tab " + tabCounter;
        reqFor = "Pawn";}
        else if (dataId.includes("--BILL")){
        label = dataId.replace("--BILL","") || "Tab " + tabCounter;
        reqFor = "Bill";}
        else if (dataId.includes("--LOCKER")){
        label = dataId.replace("--LOCKER","") || "Tab " + tabCounter;
        reqFor = "Locker";}
        else if (dataId.includes("--LEASE")){
        label = dataId.replace("--LEASE","") || "Tab " + tabCounter;
        reqFor = "FLease";}

        else{
        label = dataId || "Tab " + tabCounter;
        }
    } else {
        label = reqFor || "Tab " + tabCounter;
    }
    if (label.length >= 19) {
        wid = "18%";
    } else if (label.length >= 9 && label.length <= 19) {
        wid = "16%";
    } else {
        wid = "9%";
    }
    var tabimage = "";
    if (reqFor == "Customer") {
        bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
    }
    if  (reqFor == "Deal") {
        bg = "width:130px;height:24px;font-size:10px;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
        }
    if  (reqFor == "Pawn") {
        bg = "width:130px;height:24px;font-size:10px;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
    }
    if  (reqFor == "Bill") {
         bg = "width:130px;height:24px;font-size:10px;";
             tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
         }
    if  (reqFor == "Trade") {
        bg = "width:130px;height:24px;font-size:10px;";
            tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
        }
    if  (reqFor == "Locker") {
        bg = "width:130px;height:24px;font-size:10px;";
            tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
        }
    if  (reqFor == "FLease") {
        bg = "width:130px;height:24px;font-size:10px;";
            tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
        }
    if (reqFor == "NIC") {
         bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
         tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
        }
    if (reqFor == "Account") {
        bg = "width:130px;height:24px;font-size:10px;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
    }
    if (reqFor == "Staff") {
        bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;color:#000000;margin-top:2px;"
    }
    if (reqFor == "Case") {
        bg = "width:130px;height:24px;font-size:10px;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/case-b-20.png\") no-repeat scroll 0px 0px; height: 23px; padding-left: 16px; margin-right:6px;margin-top:2px;"
    }
    if (reqFor == "Transaction") {
        bg = "width:130px;height:24px;font-size:10px;";
        tabimage = "font-size: 10px; background: url(\"/cdn/efm/common/images/txn-b-20.png\") no-repeat scroll 0px 0px; height: 23px;  padding-left: 16px; margin-right:6px;margin-top:2px;"
    }

    var id = "";
    var currtab = "";
    if (dataId != "") {
        currtab = dataId;
        id = reqFor + "--" + dataId;
    } else {
        currtab = tabCounter;
        id = reqFor + "--" + tabCounter;
    }
    var li = $(tabTemplate.replace(/#\{aria_control\}/g, id).replace(/#\{tab_id\}/g, tabCounter).replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label).replace(/#\{backStyle\}/g, bg).replace(/#\{tabImg\}/g, tabimage).replace(/#\{titleText\}/g, dataId).replace(/#\{clckFun\}/g, "knowTabId(this," + tabCounter + ")"));

    var existId = reqFor + "--" + id.split("_")[id.split("_").length - 1];
    if (reqFor == "Customer") {
        if (dataId != "") {
            if (tabCustCount.indexOf(existId) >= 0 || tabCustCount.indexOf(id) >= 0) {
                if (tabCustCount.indexOf(id) >= 0) {
                    activateOpenTab(id, reqFor);
                } else {
                    activateOpenTab(existId, reqFor);
                }
            } else {
                maintainTabData(tabCustCount, id);
                addTabMenu(li, id);
                treeId = "custmContainer" + tabCounter;
               // loadCase(id, getCustTreeData(tabCounter, id));
               // $("#custMenu" + tabCounter).tabs();
                custLoad(dataId, tabCounter,id);
                activateOpenTab(id, reqFor);
            }
        }
        else {
            maintainTabData(tabCustCount, id);
            addTabMenu(li, id);
            treeId = "custmContainer" + tabCounter;
            loadCase(id, custOutput);
            activateOpenTab(id, reqFor);
        }
    }
   if (reqFor == "NIC") {
            if (dataId != "") {
                if (tabNicCount.indexOf(existId) >= 0 || tabNicCount.indexOf(id) >= 0) {
                    if (tabNicCount.indexOf(id) >= 0) {
                        activateOpenTab(id, reqFor);
                    } else {
                        activateOpenTab(existId, reqFor);
                    }
                } else {
                    maintainTabData(tabNicCount, id);
                    addTabMenu(li, id);
                    treeId = "nicContainer" + tabCounter;
                    loadCase(id, getNicTreeData(tabCounter, id));
                    nicLoad(dataId, tabCounter);
                    activateOpenTab(id, reqFor);
                }
            }
            else {
                maintainTabData(tabNicCount, id);
                addTabMenu(li, id);
                treeId = "nicContainer" + tabCounter;
                loadCase(id, nicOutput);
                activateOpenTab(id, reqFor);
            }
        }
    if (reqFor == "Staff") {
        if (dataId != "") {
            if (tabEmpCount.indexOf(existId) >= 0 || tabEmpCount.indexOf(id) >= 0) {
                if (tabEmpCount.indexOf(id) >= 0) {
                    activateOpenTab(id, reqFor);
                } else {
                    activateOpenTab(existId, reqFor);
                }
            } else {
                maintainTabData(tabEmpCount, id);
                addTabMenu(li, id);
                treeId = "empmContainer" + tabCounter;
                loadCase(id, getEmpTreeData(tabCounter, id));
                $("#empMenu" + tabCounter).tabs();
                empLoad(dataId, tabCounter);
                activateOpenTab(id, reqFor);
            }
        }
        else {
            maintainTabData(tabEmpCount, id);
            addTabMenu(li, id);
            treeId = "empmContainer" + tabCounter;
            loadCase(id, empOutput);
            activateOpenTab(id, reqFor);
        }
    }
    if (reqFor == "Deal") {
         if (dataId != "") {

                if (tabDealCount.indexOf(existId) >= 0 || tabDealCount.indexOf(id) >= 0) {
                                if (tabDealCount.indexOf(id) >= 0) {
                                    activateOpenTab(id, reqFor);
                                } else {
                                    activateOpenTab(existId, reqFor);
                                }
                            } else {
                                maintainTabData(tabDealCount, id);

                                addTabMenu(li, id);
                                treeId = "dealContainer" + tabCounter;
                                loadCase(id, getDealTreeData(tabCounter, id));
                                dealLoad(dataId, tabCounter);
                                activateOpenTab(id, reqFor);
            }
            }
                 else {
                        maintainTabData(tabDealCount, id);
                        addTabMenu(li, id);
                        treeId = "dealContainer" + tabCounter;
                        //loadCase(id, empOutput);
                        activateOpenTab(id, reqFor);
                    }
                }


            if (reqFor == "Pawn") {
     if (dataId != "") {

            if (tabPawnCount.indexOf(existId) >= 0 || tabPawnCount.indexOf(id) >= 0) {
                            if (tabPawnCount.indexOf(id) >= 0) {
                                activateOpenTab(id, reqFor);
                            } else {
                                activateOpenTab(existId, reqFor);
                            }
                        } else {
                            maintainTabData(tabPawnCount, id);

                            addTabMenu(li, id);
                            treeId = "pawnContainer" + tabCounter;
                            loadCase(id, getPawnTreeData(tabCounter, id));
                            pawnLoad(dataId, tabCounter);
                            activateOpenTab(id, reqFor);
        }
        }
             else {
                    maintainTabData(tabPawnCount, id);
                    addTabMenu(li, id);
                    treeId = "pawnContainer" + tabCounter;
                    //loadCase(id, empOutput);
                    activateOpenTab(id, reqFor);
                }
            }

        if (reqFor == "Bill") {
 if (dataId != "") {

        if (tabBillCount.indexOf(existId) >= 0 || tabBillCount.indexOf(id) >= 0) {
                        if (tabBillCount.indexOf(id) >= 0) {
                            activateOpenTab(id, reqFor);
                        } else {
                            activateOpenTab(existId, reqFor);
                        }
                    } else {
                        maintainTabData(tabBillCount, id);

                        addTabMenu(li, id);
                        treeId = "billContainer" + tabCounter;
                        loadCase(id, getBillTreeData(tabCounter, id));
                        billLoad(dataId, tabCounter);
                        activateOpenTab(id, reqFor);
    }
    }
         else {
                maintainTabData(tabBillCount, id);
                addTabMenu(li, id);
                treeId = "billContainer" + tabCounter;
                //loadCase(id, empOutput);
                activateOpenTab(id, reqFor);
            }
        }

    if (reqFor == "FLease") {
     if (dataId != "") {

            if (tabLeaseCount.indexOf(existId) >= 0 || tabLeaseCount.indexOf(id) >= 0) {
                            if (tabLeaseCount.indexOf(id) >= 0) {
                                activateOpenTab(id, reqFor);
                            } else {
                                activateOpenTab(existId, reqFor);
                            }
                        } else {
                            maintainTabData(tabLeaseCount, id);

                            addTabMenu(li, id);
                            treeId = "leaseContainer" + tabCounter;
                            loadCase(id, getLeaseTreeData(tabCounter, id));
                            leaseLoad(dataId, tabCounter);
                            activateOpenTab(id, reqFor);
        }
        }
             else {
                    maintainTabData(tabLeaseCount, id);
                    addTabMenu(li, id);
                    treeId = "leaseContainer" + tabCounter;
                    //loadCase(id, empOutput);
                    activateOpenTab(id, reqFor);
                }
            }

                if (reqFor == "Locker") {
         if (dataId != "") {

                if (tabLockerCount.indexOf(existId) >= 0 || tabLockerCount.indexOf(id) >= 0) {
                                if (tabLockerCount.indexOf(id) >= 0) {
                                    activateOpenTab(id, reqFor);
                                } else {
                                    activateOpenTab(existId, reqFor);
                                }
                            } else {
                                maintainTabData(tabLockerCount, id);

                                addTabMenu(li, id);
                                treeId = "lockerContainer" + tabCounter;
                                loadCase(id, getLockerTreeData(tabCounter, id));
                                lockerLoad(dataId, tabCounter);
                                activateOpenTab(id, reqFor);
            }
            }
                 else {
                        maintainTabData(tabLockerCount, id);
                        addTabMenu(li, id);
                        treeId = "lockerContainer" + tabCounter;
                        //loadCase(id, empOutput);
                        activateOpenTab(id, reqFor);
                    }
                }
         if (reqFor == "Trade") {
         if (dataId != "") {

                if (tabTradeCount.indexOf(existId) >= 0 || tabTradeCount.indexOf(id) >= 0) {
                                if (tabTradeCount.indexOf(id) >= 0) {
                                    activateOpenTab(id, reqFor);
                                } else {
                                    activateOpenTab(existId, reqFor);
                                }
                            } else {
                                maintainTabData(tabTradeCount, id);

                                addTabMenu(li, id);
                                treeId = "tradeContainer" + tabCounter;
                                loadCase(id, getTradeTreeData(tabCounter, id));
                                tradeLoad(dataId, tabCounter);
                                activateOpenTab(id, reqFor);
            }
            }
                 else {
                        maintainTabData(tabTradeCount, id);
                        addTabMenu(li, id);
                        treeId = "tradeContainer" + tabCounter;
                        //loadCase(id, empOutput);
                        activateOpenTab(id, reqFor);
                    }
                }

    if (reqFor == "Account") {

        if (dataId != "") {
            if (tabAcctCount.indexOf(existId) >= 0 || tabAcctCount.indexOf(id) >= 0) {
                if (tabAcctCount.indexOf(id) >= 0) {
                    activateOpenTab(id, reqFor);
                } else {
                    activateOpenTab(existId, reqFor);
                }
            } else {
                maintainTabData(tabAcctCount, id);
                addTabMenu(li, id);
                treeId = "acctContainer" + tabCounter;
                loadCase(id, getAcctTreeData(tabCounter, id));
                $("#acctMenu" + tabCounter).tabs();
                $("#fromDate" + tabCounter).datepicker();
                $("#toDate" + tabCounter).datepicker();
                $("#fromDate" + tabCounter).datepicker("option", "dateFormat", "yy-mm-dd");
                $("#toDate" + tabCounter).datepicker("option", "dateFormat", "yy-mm-dd");
                $("#fromDate" + tabCounter).datepicker("option", "maxDate", new Date());
                $("#toDate" + tabCounter).datepicker("option", "maxDate", new Date());
                acctLoad(dataId, tabCounter);
                activateOpenTab(id, reqFor);
            }
        }
        else {
            maintainTabData(tabAcctCount, id);
            addTabMenu(li, id);
            treeId = "acctContainer" + tabCounter;
            loadCase(id, acctOutput);
            activateOpenTab(id, reqFor);
        }
    }

    if (reqFor == "Case") {
        if (dataId != "") {
            if (tabCaseCount.indexOf(existId) >= 0 || tabCaseCount.indexOf(id) >= 0) {
                if (tabCaseCount.indexOf(id) >= 0) {
                    activateOpenTab(id, reqFor);
                } else {
                    activateOpenTab(existId, reqFor);
                }
            } else {
                maintainTabData(tabCaseCount, id);
                addTabMenu(li, id);
                treeId = "caseContainer" + tabCounter;
                loadCase(id, getCaseTreeData(tabCounter, id));
                $("#caseMenu" + tabCounter).tabs();
                $("#fromDate").datepicker();
                $("#toDate").datepicker();
                $("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd");
                $("#toDate").datepicker("option", "dateFormat", "yy-mm-dd");
                $("#toDate").datepicker("option", "maxDate", new Date());
                caseLoad(dataId, tabCounter, getModuleFromCaseId(dataId));
                activateOpenTab(id, reqFor);
            }
        }
        else {
            maintainTabData(tabCaseCount, id);
            addTabMenu(li, id);
            treeId = "caseContainer" + tabCounter;
            loadCase(id, caseOutput);
            activateOpenTab(id, reqFor);
        }
    }

    if (reqFor == "Transaction") {
        treeId = "tranContainer";
        if (dataId != "") {
            if (tabTranCount.indexOf(existId) >= 0 || tabTranCount.indexOf(id) >= 0) {
                if (tabTranCount.indexOf(id) >= 0) {
                    activateOpenTab(id, reqFor);
                } else {
                    activateOpenTab(existId, reqFor);
                }
            } else {
                maintainTabData(tabTranCount, id);
                addTabMenu(li, id);
                treeId = "tranContainer" + tabCounter;
                loadCase(id, getTranTreeData(tabCounter, id));
                $("#fromDate").datepicker();
                $("#toDate").datepicker();
                $("#fromDate").datepicker("option", "dateFormat", "yy-mm-dd");
                $("#toDate").datepicker("option", "dateFormat", "yy-mm-dd");
                $("#toDate").datepicker("option", "maxDate", new Date());
                tranLoad(dataId, tabCounter, id);
                activateOpenTab(id, reqFor);
            }
        }
        else {
            maintainTabData(tabTranCount, id);
            addTabMenu(li, id);
            treeId = "tranContainer" + tabCounter;
            loadCase(id, tranOutput);
            activateOpenTab(id, reqFor);
        }
    }
    var tabSize = tabCaseCount.length + tabAcctCount.length + tabCustCount.length + tabTranCount.length +tabDealCount.length+tabLeaseCount.length+tabTradeCount.length+tabLockerCount.length+tabBillCount.length+tabPawnCount.length;
    if (tabSize >= 8) {
        tabSizeShinker();
    }
}

function addTabMenu(li, id) {
    for (var t = 0; t < $("ul#tabNavUl li").length; t++) {
        $("ul#tabNavUl li").removeClass("ui-tabs-active");
        $("ul#tabNavUl li").removeClass("ui-state-active");
    }
    tabs.find("#tabNavUl").append(li);
    tabs.append("<div id='" + id + "' style='margin-left: 20px ;width: 100%; height:100%;'></div>");
    tabCounter++;
    $("div#" + id).show();
    $("#tabNavUl").show();
    $("#canvas").attr("style", "display:block;height:100.3%;");
    var index = $("#" + id).index();
}

function activateOpenTab(tabId, reqForm) {
    var index = $("#" + tabId).index();
    tabPosition = index;
    activateCanvasTabIndex(index - 1);
}

function tabSizeShinker() {
    var n = $("#tabNavUl li").length;
    var w = 0;
    var wid = 0;
    var addVal = (n >= 10 && n < 12) ? 4.0 : ((n >= 12 && n < 14) ? 6.0 : ((n >= 14 && n < 16) ? 8.0 : 10.0));
    if (n >= 10) {
        if (n >= 12) {
            w = ((100.0) / (n + addVal));
            wid = 100 - ((n / 4) * n + w);
        } else {
            w = ((100.0) / (n + addVal));
            wid = 100 - ((n / 4) * n + w);
        }
    } else {
        w = (100 / (n + 3));
        wid = 100 - (n + w);
    }
    $("#tabNavUl li").width(w + '%');
    $("#tabNavUl li a").css('width', wid + 'px');
}

function tabSizeexpander() {
    var n = $("#tabNavUl li").length;
    var w = 0;
    var wid = 0;
    var redVal = (n >= 8 && n < 10) ? 3.0 : ((n >= 10 && n < 12) ? 5.0 : ((n >= 12 && n < 14) ? 7.0 : 9.0));
    if (n >= 9) {
        if (n >= 11) {
            w = ((100.0) / (n + redVal));
            wid = 100 - (n * redVal / 2) - 10;
            w += "%";
        } else {
            w = ((100.0) / (n + redVal));
            wid = 100 - (n * redVal);
            w += "%";
        }
    } else {
        wid = "97";
        w = "auto";
    }
    $("#tabNavUl li").width(w);
    $("#tabNavUl li a").css('width', wid + 'px');
}

function maintainTabData(tabArry, id) {
    if (tabArry.length >= 5) {
        var tempArray = tabArry;
        var contId = tabArry.splice(1, 4, tempArray[2], tempArray[3], tempArray[4], id);
        removeTabExceedCount(contId[0]);
        $("#" + id).show();
    }
    else {
        tabArry.push(id);
    }
}

function removeTabExceedCount(id) {
    var lid = "li[aria-controls=" + id + "]";
    $('ul[id=tabNavUl] ' + lid).remove();
    $("#" + id).remove();
    //tabs.tabs("refresh");
}

function toggleSrchWindow() {

    if ($("#imgWindow").attr('class') == "clSrchInsideDiv") {
        $("#srchWindow").animate({width: 'toggle'}, "slow");
        $(".clsWindow").animate({
            left: "-=204"
        }, 530, function () {
            $("#clSchWd").toggleClass("clsWindow");
            $("#clSchWd").toggleClass("opnWindow");
            toggleClassByID("imgWindow", "clSrchOutsideDiv", "clSrchInsideDiv");
            $("#clSchWd").css('left', '0px');
        });
        return false;
    } else {
        $("#srchWindow").animate({width: 'toggle'}, "slow");
        $(".opnWindow").animate({
            left: "+=204"
        }, 610, function () {
            $("#clSchWd").toggleClass("opnWindow");
            $("#clSchWd").toggleClass("clsWindow");
            toggleClassByID("imgWindow", "clSrchInsideDiv", "clSrchOutsideDiv");
            $("#clSchWd").css('left', '0px');
        });
    }

}

function openLinkAnanlysis(linkUrl) {
    var wind;
    wind = window.open(linkUrl, 'Link Ananlysis', 'height=950, width=1200, toolbar=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=yes, modal=yes');
    wind.focus();
}

function goBack(backId) {
    var index = $("#" + backId).index();
    if (index === 1) {
        document.getElementById(backId).style.cursor = 'default';
        document.getElementById(backId).disabled = true;

        $('#' + backId).hover(function () {
            $(this).css("opacity", "1");
        });

    }

    else {
        document.getElementById(backId).style.cursor = 'hand';

        $('#' + backId).hover(function () {
            $(this).css("opacity", "0.9");
        });

        document.getElementById(backId).disabled = false;
        activateCanvasTabIndex(index - 2);
    }

}

function activateCanvasTabIndex(ind) {
    $("ul#tabNavUl li").removeClass("ui-tabs-active");
    $("ul#tabNavUl li").removeClass("ui-state-active");
    var ele = $("#tabNavUl").children()[ind];
    $(ele).addClass("ui-tabs-active");
    var divId = $(ele).find("a").attr("href");
    $("#canvas > div").css("display", "none");
    $(divId).css("display", "block");
}

function activateCanvasTabId(id) {
    $("ul#tabNavUl li").removeClass("ui-tabs-active");
    $("ul#tabNavUl li").removeClass("ui-state-active");
    var panelId = id.split("#")[1];
    var liEle = $("ul#tabNavUl li[aria-controls='" + panelId + "']")[0];
    $(liEle).addClass("ui-tabs-active");
    var divId = $(liEle).find("a").attr("href");
    $("#canvas > div").each(function() {
        if (this.id === divId.split("#")[1])
            $(this).css("display", "block");
        else
            $(this).css("display", "none");
    });

    // $(divId).css("display", "block");
}

function linkClose() {
    $('#linkAnalysis').fadeIn('fast', function () {
        $('#linkAnalysis').animate({'top': '-350px'}, 5, function () {
            $('#linkOverlay').fadeOut('fast');
        });
    });
}

function viewLinkAnanlysis(acctId) {
    $('#linkAnalysis').fadeIn('fast', function () {
        $('#linkOverlay').fadeIn('fast');
        $('#linkAnalysis').animate({'top': '200px'}, 5, function () {
        });
    });
    $("#dataTableLink").remove();
    var linkData = "";
    linkData = "<table id='dataTableLink'><tr><td style='font-size:0.79em; padding:4px 0 4px 0;' >Account ID</td><td style='font-size:0.79em;  padding:4px 0 4px 0;' id='linkId'>" + acctId + "</td></tr><tr><td style='padding-left:0px;' ><a class='abutton submit-w-24 ' href='javascript:loadAnanlysis()'>OK</a></td><td><a class='abutton cancel-w-20' href='javascript:linkClose()' >Cancel</a></td></tr></table>";
    $("#linkAnalysis").append(linkData);
}

function loadAnanlysis() {
    openLinkAnanlysis("la.jsp?userId=" + top.user + "&refId=" + $("#linkId").html());
    $('#linkAnalysis').fadeIn('fast', function () {
        $('#linkAnalysis').animate({'top': '-350px'}, 5, function () {
            $('#linkOverlay').fadeOut('fast');
        });
    });
}

function backAnanlysis() {
    $("#iDiv").show();
    $("#analDiv").hide();
}
