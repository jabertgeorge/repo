<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"

%>
<%@ page
        import="java.sql.*, java.util.*,java.text.SimpleDateFormat,java.sql.ResultSetMetaData,java.beans.Statement,java.io.BufferedReader,java.io.File,java.io.FileInputStream,java.io.FileReader,java.io.IOException,java.io.PrintWriter" %>
<%@ page
        import="java.sql.Connection,java.sql.PreparedStatement,java.sql.ResultSet,java.sql.ResultSetMetaData,java.sql.SQLException,java.util.Iterator,java.util.StringTokenizer" %>
<%@ page
        import="javax.servlet.ServletException,javax.servlet.http.HttpServlet,javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse,javax.servlet.http.HttpSession" %>
<%--@page import="org.apache.xmlbeans.impl.xb.xsdschema.Public" --%>
<%@ page import="clari5.upload.ui.DBConnection" %>
<%@ page import="java.util.List" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
      <script src="/efm/JavaScriptServlet"></script>

</head>
<body>
<div class="row">
    <div class="col-xs-5"></div>
    <div class="col-xs-3 "><h3>Update Table Data</h3></div>
    <div class="col-xs-4"></div>
</div>
<br>

<%
    String table_name = (String) session.getAttribute("myId");
//String primary_key=request.getParameter("id");
    String columnWithHeader[] = request.getParameterValues("id");
%>
<form action="/efm/Update" method="post">
    <input type="hidden" name="table" value="<%= table_name%>">
    <input type="hidden" name="columnWithHeader" value="<%= columnWithHeader%>">
    <%

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ResultSetMetaData MetaData = null;
        int columnCount = 0;
        String ColumnName;
        String query;
        session = request.getSession();


        // Connection con=ConnectionOracleDB.getConnection();
        try {
            List<String> header;
            List<String> values;
            con = DBConnection.getDBConnection();
            ps = con.prepareStatement("select * from " + table_name);
            rs = ps.executeQuery();

            MetaData = rs.getMetaData();
            columnCount = MetaData.getColumnCount();


            ColumnName = MetaData.getColumnName(1);
            String[] columnWithHeaders = new String[columnWithHeader.length - 1];
            for (String s : columnWithHeader) {
                columnWithHeaders = s.split("~");
            }
            header = Arrays.asList(columnWithHeaders[0].split(","));
            values = Arrays.asList(columnWithHeaders[1].split(","));
            String where = " where ";
            for (int i = 0; i < header.size(); i++) {
                if (header.get(i).equals("LCHG_TIME") || header.get(i).equals("RCRE_TIME") || header.get(i).equals("RCRE_USER") || header.get(i).equals("LCHG_USER"))
                    continue;
                if (values.get(i).equals("null")) {
                    where += "\"" + header.get(i) + "\" is null ";
                } else {
                    where += "\"" + header.get(i) + "\" = '" + values.get(i) + "'";
                }
                if (i != (header.size() - 1)) where += " and ";
            }
            if (where.substring(where.length() - 4, where.length()).trim().equals("and"))
                where = where.substring(0, where.length() - 5);

            session.setAttribute("where", where);
            System.out.println("Sql where clause " + where);
            //query="select * from "+table_name+" where \""+ColumnName+"\"='"+primary_key+"'";

            if(rs.next()){
                for(int i=1;i <= columnCount;i++){
                    String dt = rs.getMetaData().getColumnTypeName(i).toLowerCase();
                    System.out.println("updateTableFile.jsp : "+dt+" : "+("timestamp".equals(dt)?rs.getTimestamp(i):rs.getObject(i)));
                }
            }


            query = "select * from " + table_name + " " + where;
            System.out.println("[updateTableFile.jsp] Query is " + query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
    %>
    <table align="center" id="updatetable"><%

        while (rs.next()) {
            System.out.println("[updateTableFile.jsp] Column count " + columnCount);
            for (int i = 1; i <= columnCount; i++) {
    %>
        <tr>
            <td><%=MetaData.getColumnName(i) %>
            </td>

            <%

            if (rs.getString(MetaData.getColumnName(i)) == null) {
            %>
            <td><input type="text" name="<%= MetaData.getColumnName(i) %>" value=''></td>
            <%

            } else if (("timestamp".equals(rs.getMetaData().getColumnTypeName(i).toLowerCase()))) {
            %>
            <td><input type="text" readonly="readonly" name="<%=MetaData.getColumnName(i) %>"
                       value="<%=new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss.SS a").format(rs.getTimestamp(i)) %>">
            </td>
            <%
            } else if ("date".equals(rs.getMetaData().getColumnTypeName(i).toLowerCase()))//||("uploaddate".equals(rs.getMetaData().getColumnTypeName(i).toLowerCase())))
            {
            %>
            <td><input type="text" required title="Must Enter The Value DD-MM-YYYY" pattern="\d{1,2}-\d{1,2}-\d{4}"
                       name="<%= MetaData.getColumnName(i) %>"
                       value="<%= new SimpleDateFormat("dd-MM-yyyy").format(rs.getDate(i)) %>">
            </td>

            <% } else {
            %>
            <td><input type="text" required title="Must Enter The Value" name="<%= MetaData.getColumnName(i) %>"
                       value="<%=rs.getString(MetaData.getColumnName(i))%>">
            </td>
        </tr>
        <%
                    }
                }
            }
        %>

        <%
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (con != null) con.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                try {
                    if (ps != null) ps.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
        %>
    </table>
    <br>
    <div class="row">
        <div class="col-xs-6"></div>
        <div class="col-xs-1"><input type="submit" value="update" class="btn btn-info"
                                     onclick="return confirm('Are you sure you want to Update')"></div>
        <div class="col-xs-5"></div>
    </div>
</form>
</body>
</html>
