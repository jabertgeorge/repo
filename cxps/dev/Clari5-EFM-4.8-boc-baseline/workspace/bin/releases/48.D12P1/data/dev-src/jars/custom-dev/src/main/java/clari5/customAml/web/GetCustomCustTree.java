package clari5.customAml.web;

import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetCustomCustTree {
    CxpsLogger logger = CxpsLogger.getLogger(GetCustomCustTree.class);
    private String custId="";
    private String pawnNumChache="";;

    GetCustomCustTree(String payload){
        this.custId="";
        JSONObject jsonObject = new JSONObject(payload);
        this.custId = jsonObject.getString("custId");
    }

    /*
    * Function to get accountId's,JointCustId's,and EmployeeId's
     */
    public synchronized String getCustTree(){
        JSONObject obj = new JSONObject();
        try {
            logger.debug("getCustree:custId:"+this.custId);
            if(this.custId == null){
                logger.error("No Cust Id provided");
                return null;
            }

            if(this.custId!= null && !(this.custId).startsWith("C_")){
                logger.error("Cust Id dont have proper prifix");
                return null;
            }
            JSONArray actJarray=getAcctIds(this.custId);
            JSONArray jointActJarray=getJointCustIds(actJarray);
            JSONArray empIdJarray=getEmployeeIdsforCustomer(this.custId);
            obj.put("acctIds", actJarray);
            obj.put("jointCustIds", jointActJarray);
            obj.put("empIds", empIdJarray);
            logger.debug("response custTree:"+obj.toString());
            return obj.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray getAcctIds(String custId) throws SQLException{
        JSONArray jarray = new JSONArray();
        ResultSet rs=null;
        String hostId=null;
        try{
            hostId=getCustHostId(custId);
            if(!hostId.equals(null)) getOtherSystemAccounts(hostId,custId,jarray);
            String sql="select acct_id from AML_ACCOUNT_VIEW where cust_id=?";
            execQuery(sql,custId,jarray,"acctIds");
            logger.info("accountList Array:"+jarray);
            return jarray;
        }catch (Exception e){
            e.printStackTrace();
        }
        return jarray;
    }

    public String getCustHostId(String custId) throws SQLException{
        ResultSet rs=null;
        String hostId=null;
        try{
            String sql="select host_id from AML_CUSTOMER_VIEW where cust_id=?";
            hostId=execQuery(sql,custId);
            return hostId;
        }catch (Exception e){
            e.printStackTrace();
        }
        return hostId;
    }

    public JSONArray getJointCustIds(JSONArray acctIds) throws SQLException{
        JSONArray jarray = new JSONArray();
        ResultSet rs=null;
        try {
            StringBuilder sql=new StringBuilder("select cust_id from AML_ACCOUNT_RELATION_VIEW where account_id in(");
            String[] array = new String[acctIds.length()];
            for (int i = 0; i < acctIds.length(); i++) {
                array[i] = (String)acctIds.get(i);
                sql.append("?,");
            }
            logger.debug("preparing query:"+sql.substring(0, sql.length()-1) );
            String str=sql.substring(0, sql.length() - 1).toString()+")";
            execQuery(str,array,jarray,"joint");
            logger.debug("jointList Array:"+jarray);
            return jarray;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray execQuery(String sql,String[] parameter,JSONArray jarray,String sysType){
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs=null;

        try {
            conn=Rdbms.getAppConnection();
            logger.debug("execQuery:Query: "+sql);
            stmt=conn.prepareStatement(sql);
            for (int i = 0; i < parameter.length; i++) {
                stmt.setString(i+1,parameter[i].toString().replace("--PAWN","").replace("--DEAL","").replace("--BILL","").replace("--TRADE","").replace("--LOCKER","").replace("--LEASE",""));
            }
            rs=stmt.executeQuery();
            while(rs.next()){
                jarray.put(rs.getString(1));
            }
            logger.debug("execQuery:Array:"+jarray);
            return jarray;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
        }
        return null;
    }

    public JSONArray getEmployeeIdsforCustomer(String custId) throws SQLException{
        JSONArray jarray = new JSONArray();
        ResultSet rs=null;
        try {
            String sql="select emp_id from EMPLOYEE_MASTER where cust_id=?";
            execQuery(sql,custId,jarray,"empIds");
            logger.debug("empList Array:"+jarray);
            return jarray;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    protected void setOtherSystemAccounts(String sql,String custId,JSONArray jarray,String sysType) throws SQLException{
        ResultSet rs=null;
        try {
            execQuery(sql,custId,jarray,sysType);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void getOtherSystemAccounts(String hostid,String custid,JSONArray jarray){
        logger.debug("in getOtherSystemAccounts: "+hostid);
        StringBuilder sb=new StringBuilder();
        String query="";
        try{
            switch(hostid){
                case "P":
                    query="select Account_ID from Pawning_Master where CustId=? ";
                    setOtherSystemAccounts(query,custid,jarray,"PAWN");
                    break;
                case "X":
                    query="select deal_No from Finacle_DealMaster where CIFID=? ";
                    setOtherSystemAccounts(query,custid,jarray,"DEAL");
                    break;
                case "B":
                    query="select Treasury_Bill_Num from Bills_Master where CIFID=? ";
                    setOtherSystemAccounts(query,custid,jarray,"BILL");
                    break;
                default:
                    query="select FACILTY_NUM from TF_MASTER where cxCifID=? ";
                    setOtherSystemAccounts(query,custid,jarray,"TRADE");
                    query="select Locker_Number from SAFEDEP_MASTER where CORE_CIFID_1=? ";
                    setOtherSystemAccounts(query,custid,jarray,"LOCKER");
                    query="select Lease_Facility_Number from Lease_Master where cxCifID=?";
                    setOtherSystemAccounts(query,custid,jarray,"LEASE");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public JSONArray execQuery(String sql,String parameter,JSONArray jarray,String sysType){
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs=null;

        try {
            conn=Rdbms.getAppConnection();
            logger.debug("execQuery:Query: "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,parameter);
            rs=stmt.executeQuery();
            while(rs.next()){
               //System.out.println("pawnNumChache:"+pawnNumChache+"jarray:"+jarray);
                if(sysType.equals("acctIds") || sysType.equals("joint") || sysType.equals("empIds")){
                    if(!pawnNumChache.equals("") || !pawnNumChache.equals(null)){
                        if(pawnNumChache.equals(rs.getString(1)))
                            logger.debug("duplicate account:"+rs.getString(1));
                        else
                            jarray.put(rs.getString(1));
                    }else
                        jarray.put(rs.getString(1));
                }else{
                    if(sysType.equals("PAWN"))
                        pawnNumChache=rs.getString(1);
                    jarray.put(rs.getString(1)+"--"+sysType);
                }
            }
            logger.debug("execQuery:Array:"+jarray);
            return jarray;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
        }
        return null;
    }

    public String execQuery(String sql,String parameter){
        String resValue=null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs=null;

        try {
            conn=Rdbms.getAppConnection();
            logger.debug("execQuery:Query: "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,parameter);
            rs=stmt.executeQuery();
            while(rs.next()){
                resValue=rs.getString(1);
            }
            logger.debug("execQuery:resValue:"+resValue);
            return resValue;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
        }
        return null;
    }
}