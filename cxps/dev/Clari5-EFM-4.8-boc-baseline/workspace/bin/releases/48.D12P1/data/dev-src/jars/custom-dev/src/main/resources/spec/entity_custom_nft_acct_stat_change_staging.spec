clari5.hfdb.entity.nft-account-stat {
   attributes:[
   { name : event_id , type : "string:100" , key = true}
   { name : server_id, type : "number:3" }
   { name : SYNC_STATUS , type:"string:100"}

{ name = event_name, type = "string:100" }
{ name = eventtype, type = "string:100" }
{ name = eventsubtype, type = "string:100" }
{ name = source, type = "string:20 " }
{ name = msgsource, type = "string:20" }
{ name = host_user_id, type = "string:20" }
{ name = channel, type = "string:20" }
{ name = keys, type = "string:20  " }
{ name = entitytype, type = "string:20" }
{ name = host_id, type = "string:20" }
{ name = status, type = "string:20" }
{ name = eventts, type = "date" }
{ name = sys_time, type = "date" }
{ name = custid, type = "string:20" }
{ name = cx_cust_id, type = "string:20" }
{ name = cx_acct_id, type = "string:20" }
{ name = acid, type = "string:20" }
{ name = init_acct_status, type = "string:20" }
{ name = final_acct_status, type = "string:20" }
{ name = account_id, type = "string:20" }
{ name = acct_name, type = "string:20" }
{ name = systemcountry, type = "string:20" }
{ name = reservedfield1, type = "string:20" }
{ name = reservedfield2, type = "string:20" }
{ name = reservedfield3, type = "string:20" }
{ name = reservedfield4, type = "string:100" }
{ name = reservedfield5, type = "string:100" }

   ]
}
