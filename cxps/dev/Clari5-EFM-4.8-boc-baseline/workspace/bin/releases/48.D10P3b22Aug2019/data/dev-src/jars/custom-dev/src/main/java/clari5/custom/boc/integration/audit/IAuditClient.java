package clari5.custom.boc.integration.audit;

public interface IAuditClient {
	void log(String message);
	void log(LogLevel level, String message);
}
