package clari5.upload.ui;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Arrays;
import java.util.Enumeration;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

/*This performs the
 *  DELETE operation  based on the primary Key */

public class Update extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static CxpsLogger logger = CxpsLogger.getLogger(DeleteServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    @SuppressWarnings("null")
    private void doEither(HttpServletRequest request,
                          HttpServletResponse response) throws IOException {

        HttpSession session;
        session = request.getSession();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        int columnCount = 0;
        String tableName = "";
        String status = "";
        String primaryColName = "";
        String userId = (String) session.getAttribute("userId");
        tableName = request.getParameter("table");
        String where = (String) session.getAttribute("where");
        String audit_key = "";
        System.out.println("Update.java : "+tableName+" where = "+where);
        Enumeration<String> enumeration = request.getParameterNames();
        try {
//            if(checkIfRecordExists(where,tableName)) {
//                response.setContentType("text/html");
//                PrintWriter out = response.getWriter();
//                out.println("<script type=\"text/javascript\">");
//                out.println("alert(\"SIMILAR RECORD EXISTS\");");
//                out.println("window.location.href='UpdateTable?table_name='+'" + tableName + "';");
//                out.println("</script>");
//            } else {
                con = DBConnection.getDBConnection();
                String updatrSql = "";
                updatrSql = "update " + tableName + " set ";
                while (enumeration.hasMoreElements()) {
                    String key = enumeration.nextElement();
                    if (key == null || key.equalsIgnoreCase("columnWithHeader") || key.equalsIgnoreCase("table"))
                        continue;
                    updatrSql += key + "='" + request.getParameter(key) + (enumeration.hasMoreElements() ? "'," : "' ");
                }
                updatrSql += where;
                ps = con.prepareStatement(updatrSql);
                //ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                System.out.println(" Table name [" + tableName + "] query : [" + updatrSql + "]");
                logger.info("update  " + tableName + " \n Query is [" + updatrSql + "]");
                ps = con.prepareStatement(updatrSql);
                ps.executeUpdate();
                con.commit();
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"SUCCESSFULLY UPDATED\");");
                out.println("window.location.href='UpdateTable?table_name='+'" + tableName + "';");
                out.println("</script>");
                status = "Success";
//            }
        } catch (SQLException e) {
            e.printStackTrace();
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            out.println("<script type=\"text/javascript\">");
            out.println("alert(\"Exception while Updating\");");
            out.println("window.location.href='UpdateTable?table_name='+'" + tableName + "';");
            out.println("</script>");
            status = "Failure";
            logger.info("Exception while updating record[" + e.getMessage() + "]Cause [" + e.getCause() + "]");
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
        //status = UploadUiAudit.updateAudit(tableName, userId, status, audit_key, "", "Update");

    }

    private boolean checkIfRecordExists(String where, String tableName) throws SQLException {
        boolean status = false;
        Connection con = null;
        try {
            String query = "select * from " + tableName + " where " + where;
            System.out.println("checkIfRecordExists : "+query);
            con = DBConnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                status = true;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {con.close();}catch (Exception ex){}
        }
        return status;
    }

    @SuppressWarnings("null")
    private void doEither1(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {

        HttpSession session;
        session = request.getSession();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        int columnCount = 0;
        String tableName = "";
        String status = "";
        String primaryColName = "";
        String userId = (String) session.getAttribute("userId");
        tableName = request.getParameter("table");
        String where = (String) session.getAttribute("where");
        String audit_key = "";


        try {
            con = DBConnection.getDBConnection();
            ps = con.prepareStatement("select * from " + tableName + where);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnCount = rsmd.getColumnCount();
            primaryColName = rsmd.getColumnName(1);
            audit_key = request.getParameter(rsmd.getColumnName(1));
//            if (session == null || session.getAttribute("userId")== null){
//                request.getRequestDispatcher("/expiry.jsp").forward(request,response);
//            }
            String updatrSql = "";
            System.out.println(" Table name [" + tableName + "] columnCount [" + columnCount + "]");
            while (rs.next()) {
                switch (columnCount) {
                    case 5:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? , LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, userId);
                        break;


                    case 6:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=?," + rsmd.getColumnName(2) + "=?,LCHG_TIME=CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, userId);
                        break;


                    case 7:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? , LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, userId);
                        break;

                    case 8:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, userId);
                        break;

                    case 9:
                        updatrSql = "update " + tableName + " set \"" + rsmd.getColumnName(1) + "\"=? ,\"" + rsmd.getColumnName(2) + "\"=? ,\"" + rsmd.getColumnName(3) + "\"=? ,\"" + rsmd.getColumnName(4) + "\"=?,\"" + rsmd.getColumnName(5) + "\"=?,LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, userId);
                        break;

                    case 10:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, userId);
                        break;

                    case 11:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?,LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, userId);
                        break;

                    case 12:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, userId);
                        break;

                    case 13:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, userId);
                        break;

                    case 14:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, userId);
                        break;

                    case 15:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?," + rsmd.getColumnName(11) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, request.getParameter(rsmd.getColumnName(11)));
                        ps.setString(12, userId);
                        break;

                    case 16:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?," + rsmd.getColumnName(11) + "=?," + rsmd.getColumnName(12) + "=?,LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, request.getParameter(rsmd.getColumnName(11)));
                        ps.setString(12, request.getParameter(rsmd.getColumnName(12)));
                        ps.setString(13, userId);
                        break;

                    case 17:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?," + rsmd.getColumnName(11) + "=?," + rsmd.getColumnName(12) + "=?," + rsmd.getColumnName(13) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, request.getParameter(rsmd.getColumnName(11)));
                        ps.setString(12, request.getParameter(rsmd.getColumnName(12)));
                        ps.setString(13, request.getParameter(rsmd.getColumnName(13)));
                        ps.setString(14, userId);
                        break;

                    case 18:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?," + rsmd.getColumnName(11) + "=?," + rsmd.getColumnName(12) + "=?," + rsmd.getColumnName(13) + "=?," + rsmd.getColumnName(14) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, request.getParameter(rsmd.getColumnName(11)));
                        ps.setString(12, request.getParameter(rsmd.getColumnName(12)));
                        ps.setString(13, request.getParameter(rsmd.getColumnName(13)));
                        ps.setString(14, request.getParameter(rsmd.getColumnName(14)));
                        ps.setString(15, userId);
                        break;

                    case 19:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?," + rsmd.getColumnName(11) + "=?," + rsmd.getColumnName(12) + "=?," + rsmd.getColumnName(13) + "=?," + rsmd.getColumnName(14) + "=?," + rsmd.getColumnName(15) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, request.getParameter(rsmd.getColumnName(11)));
                        ps.setString(12, request.getParameter(rsmd.getColumnName(12)));
                        ps.setString(13, request.getParameter(rsmd.getColumnName(13)));
                        ps.setString(14, request.getParameter(rsmd.getColumnName(14)));
                        ps.setString(15, request.getParameter(rsmd.getColumnName(15)));
                        ps.setString(16, userId);
                        break;

                    case 20:
                        updatrSql = "update " + tableName + " set " + rsmd.getColumnName(1) + "=? ," + rsmd.getColumnName(2) + "=? ," + rsmd.getColumnName(3) + "=? ," + rsmd.getColumnName(4) + "=?," + rsmd.getColumnName(5) + "=?," + rsmd.getColumnName(6) + "=?," + rsmd.getColumnName(7) + "=?," + rsmd.getColumnName(8) + "=?," + rsmd.getColumnName(9) + "=?," + rsmd.getColumnName(10) + "=?," + rsmd.getColumnName(11) + "=?," + rsmd.getColumnName(12) + "=?," + rsmd.getColumnName(13) + "=?," + rsmd.getColumnName(14) + "=?," + rsmd.getColumnName(15) + "=?," + rsmd.getColumnName(16) + "=?, LCHG_TIME =CURRENT_TIMESTAMP,LCHG_USER=? " + where;
                        ps = con.prepareStatement(updatrSql);
                        ps.setString(1, request.getParameter(rsmd.getColumnName(1)));
                        ps.setString(2, request.getParameter(rsmd.getColumnName(2)));
                        ps.setString(3, request.getParameter(rsmd.getColumnName(3)));
                        ps.setString(4, request.getParameter(rsmd.getColumnName(4)));
                        ps.setString(5, request.getParameter(rsmd.getColumnName(5)));
                        ps.setString(6, request.getParameter(rsmd.getColumnName(6)));
                        ps.setString(7, request.getParameter(rsmd.getColumnName(7)));
                        ps.setString(8, request.getParameter(rsmd.getColumnName(8)));
                        ps.setString(9, request.getParameter(rsmd.getColumnName(9)));
                        ps.setString(10, request.getParameter(rsmd.getColumnName(10)));
                        ps.setString(11, request.getParameter(rsmd.getColumnName(11)));
                        ps.setString(12, request.getParameter(rsmd.getColumnName(12)));
                        ps.setString(13, request.getParameter(rsmd.getColumnName(13)));
                        ps.setString(14, request.getParameter(rsmd.getColumnName(14)));
                        ps.setString(15, request.getParameter(rsmd.getColumnName(15)));
                        ps.setString(16, request.getParameter(rsmd.getColumnName(16)));
                        ps.setString(17, userId);
                        break;
                    default:
                        logger.info("update failed for " + tableName + " \n Query is [" + updatrSql + "]");
                        break;
                }
                break;
            }
            logger.info("update  " + tableName + " \n Query is [" + updatrSql + "]");
            ps.executeUpdate();
            con.commit();
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<script type=\"text/javascript\">");
            out.println("alert(\"SUCCESSFULLY UPDATED\");");
            out.println("window.location.href='UpdateTable?table_name='+'" + tableName + "';");
            out.println("</script>");
            status = "Success";
        } catch (SQLException e) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            out.println("<script type=\"text/javascript\">");
            out.println("alert(\"Exception while Updating\");");
            out.println("window.location.href='UpdateTable?table_name='+'" + tableName + "';");
            out.println("</script>");
            status = "Failure";
            logger.info("Exception while updating record[" + e.getMessage() + "]Cause [" + e.getCause() + "]");
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }
        //status = UploadUiAudit.updateAudit(tableName, userId, status, audit_key, "", "Update");

    }
}
