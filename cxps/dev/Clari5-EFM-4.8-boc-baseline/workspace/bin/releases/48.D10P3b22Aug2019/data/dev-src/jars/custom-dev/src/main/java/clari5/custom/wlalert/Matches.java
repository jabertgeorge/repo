package clari5.custom.wlalert;

import java.util.ArrayList;
import java.util.List;

public class Matches {
    String matchResult;
    String id ;
    int responseCode;
    String remarks;

    public String  getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(String matchResult) {
        this.matchResult = matchResult;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Matches{" +
                "matchResult=" + matchResult +
                ", id='" + id + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
