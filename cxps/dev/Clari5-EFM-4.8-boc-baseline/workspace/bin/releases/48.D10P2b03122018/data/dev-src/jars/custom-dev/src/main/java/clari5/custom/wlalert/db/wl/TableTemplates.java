package clari5.custom.wlalert.db.wl;

import java.util.ArrayList;
import java.util.List;

public class TableTemplates {
    String tableName;
    String listName;
    String dropdownlist;
    private List<String> header = new ArrayList<>();
    private List<String> values = new ArrayList<>();


    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public void setHeader(List<String> header) {
        this.header = header;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }


    public List<String> getHeader() {
        return header;
    }

    public List<String> getValues() {
        return values;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getDropdownlist() {
        return dropdownlist;
    }

    public void setDropdownlist(String dropdownlist) {
        this.dropdownlist = dropdownlist;
    }


    @Override
    public String toString() {
        return "TableTemplates{" +
                "tableName='" + tableName + '\'' +
                ", listName='" + listName + '\'' +
                ", dropdownlist='" + dropdownlist + '\'' +
                ", header=" + header.toString() +
                ", values=" + values .toString()+
                '}';
    }




    public String size() {
        return "TableTemplates{" +
                "tableName=" + tableName +
                "header=" + header.size() +
                ", values=" + values.size() +
                '}';
    }
}
