package clari5.customAml.web;

import java.sql.*;
import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetDealDetails {
    CxpsLogger logger = CxpsLogger.getLogger(GetDealDetails.class);

    private static String dealNum="";
    JSONObject obj = new JSONObject();
    JSONObject obj1 = new JSONObject();

    GetDealDetails(String payload){
        this.dealNum="";
        JSONObject jsonObject = new JSONObject(payload);
        String dealNum = jsonObject.getString("dealNum");
        String[] parts = dealNum.split("--");
        this.dealNum=parts[0];
    }

    public synchronized String getDealDetails(){
        JSONObject obj = new JSONObject();
        String dealDetails=null;
        try {
            logger.debug("getDealDetails:dealNum:"+this.dealNum);
            if ((this.dealNum) == null) {
                logger.error("Trying to fetch trade details. dealNum not found");
                return null;
            }
            dealDetails=getDetails(this.dealNum);
            logger.debug("response dealDetails:"+obj.toString());
            return dealDetails;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dealDetails;
    }

    protected String getDetails(String dealNum){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            String sql="select * from FINACLE_DEALMASTER where deal_No=? ";
            logger.debug("query to get dealDetails : "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,dealNum);
            ResultSet rs = stmt.executeQuery();
            while( rs.next() ){
                obj.put("custId", rs.getString("CIFID")!= null ? rs.getString("CIFID") : "");
                String CustName=getCustName(rs.getString("CIFID")!= null ? rs.getString("CIFID") : "");
                obj.put("deal_Date", rs.getString("deal_Date")!= null ? rs.getString("deal_Date") : "");
                obj.put("Maturity_Date", rs.getString("Maturity_Date")!= null ? rs.getString("Maturity_Date") : "");
                obj.put("deal_Amount_crncy", rs.getString("deal_Amount_crncy")!= null ? rs.getString("deal_Amount_crncy") : "");
                obj.put("Name", CustName!= null ? CustName : "");
                jarray.put(obj);
            }
            obj1.put("dealDetails", jarray);
            logger.debug("getDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting deal details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }

    public String getCustName(String cifId){
        String resValue=null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs=null;

        try {
            conn=Rdbms.getAppConnection();
            String sql="select name from AML_CUSTOMER_VIEW where cust_id=? ";
            logger.debug("execQuery:Query: "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,cifId);
            rs=stmt.executeQuery();
            while(rs.next()){
                resValue=rs.getString(1);
            }
            logger.debug("execQuery:resValue:"+resValue);
            return resValue;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if(rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
        }
        return resValue;
    }
}