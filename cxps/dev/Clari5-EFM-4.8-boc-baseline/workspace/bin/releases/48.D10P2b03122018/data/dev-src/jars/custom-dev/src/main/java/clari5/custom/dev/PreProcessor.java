package clari5.custom.dev;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import cxps.customEventHelper.CustomWsKeyDerivator;
import cxps.events.FT_RemittanceEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PreProcessor {

    static CxpsLogger logger = CxpsLogger.getLogger(PreProcessor.class);

    public static CxConnection getConnection() {
        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("Connection object is NULL");
        return rdbms.getConnection();
    }

    public static String getCustIdForRemmittanceEvent(FT_RemittanceEvent event) {

        /**
         * Removing trailing zero's from REM/BEN account  number
         */

        String custId = null;
        CxConnection connection = getConnection();

        try {
            if (event.system.equalsIgnoreCase("SR")) {
                while (true) {
                    if (event.getRemType().equalsIgnoreCase("I") && event.getBenAcctNo().startsWith("0"))
                        event.setBenAcctNo(event.getBenAcctNo().substring(1));

                    else if (event.getRemType().equalsIgnoreCase("O") && event.getRemAcctNo().startsWith("0"))
                        event.setRemAcctNo(event.getBenAcctNo().substring(1));

                    else break;
                }

                /**
                 * Setting default value for (rem/ben) if value is not coming json to prevent event processing
                 * instead of throwing error.
                 */

                String acctId = CustomWsKeyDerivator.accountKeyForRemmitance(event.getRemType(),
                        (event.getRemAcctNo() != null && !event.getRemAcctNo().isEmpty()) ?
                                event.getRemAcctNo() : "12REMxxxx34",
                        (event.getBenAcctNo() != null && !event.getBenAcctNo().isEmpty()) ?
                                event.getBenAcctNo() : "12BENxxxx34");
                CxKeyHelper h = Hfdb.getCxKeyHelper();
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, event.getHostId(), acctId);
                event.setCxAcctId(accountKey);

                /**
                 * Setting default value for nic if value is not in json to prevent event processing
                 * instead of throwing error
                 */

                event.setNic((event.getNic() != null && !event.getNic().isEmpty()) ? event.getNic() : "12nicxxxx34");
                event.setFcyTranAmt(event.getTranAmt());
                event.setTranAmt(event.getLcyAmt());
                event.setFcyTranCur(event.getTranCurr());
                event.setTranCurr(event.getLcyCurr());
                event.setEventts(event.getTxnDate());
                event.setTranId("SR" + System.nanoTime());
                PreparedStatement ps = null;
                ResultSet rs = null;
                String sql = "select acctCustID from dda where acctID = '" + accountKey + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    custId = rs.getString("acctCustID");
                }
                event.setCustId((custId != null && !custId.isEmpty()) ? custId.substring(4) : "12custxxxx34");
                logger.debug("data in cx-cust-id is -------------------> " + custId + " cx-acct-id---------------> " + event.getCxAcctId());
                return (custId != null && !custId.isEmpty())? custId : "C_"+event.getHostId().toUpperCase()+"12cxCustxxxx34";
            } else {

                /**
                 * Setting default value for (rem/ben) if value is not coming json to prevent event processing
                 * instead of throwing error.
                 */

                String acctId = CustomWsKeyDerivator.accountKeyForRemmitance(event.getRemType(),
                        (event.getRemAcctNo() != null && !event.getRemAcctNo().isEmpty()) ?
                                event.getRemAcctNo() : "12REMxxxx34",
                        (event.getBenAcctNo() != null && !event.getBenAcctNo().isEmpty()) ?
                                event.getBenAcctNo() : "12BENxxxx34");
                CxKeyHelper h = Hfdb.getCxKeyHelper();
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, event.getHostId(), acctId);
                event.setCxAcctId(accountKey);

                /**
                 * Setting default value for nic if value is not in json to prevent event processing
                 * instead of throwing error
                 */

                event.setNic((event.getNic() != null && !event.getNic().isEmpty()) ? event.getNic() : "12nicxxxx34");
                event.setEventts(event.getTxnDate());
                PreparedStatement ps = null;
                ResultSet rs = null;
                String sql = "select acctCustID from dda where acctID = '" + accountKey + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    custId = rs.getString("acctCustID");
                }
                event.setCustId((custId != null && !custId.isEmpty()) ? custId.substring(4) : "12custxxxx34");
                logger.debug("data in cx-cust-id inside else is -------------------> " + custId + " cx-acct-id inside else is ---------------> " + event.getCxAcctId());
                return (custId != null && !custId.isEmpty())? custId : "C_"+event.getHostId().toUpperCase()+"12cxCustxxxx34";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("CustId is null...");
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return (custId != null && !custId.isEmpty())? custId : "C_"+event.getHostId().toUpperCase()+"12cxCustxxxx34";
    }
}
