package clari5.custom.dev.Script;

import clari5.custom.dev.ProcessSwiftFile;
import clari5.tools.util.Hocon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExecuteScript {

    public static Hocon scriptPath;

    static {
        scriptPath = new Hocon();
        scriptPath.loadFromContext("script_execute.conf");
    }

    public static void createDirectory() {
        File convertedPath = new File(scriptPath.getString("path.converted"));
        if (!convertedPath.exists()){
            convertedPath.mkdirs();
        }
    }

    public static Process execute() throws FileNotFoundException {
        //File file = new File(scriptPath.getString("path.script") + "/convert.sh");
        File dir = new File(scriptPath.getString("path.actual"));
        if (!dir.exists())
                throw new FileNotFoundException("path " + dir + "not exists");
        if (dir.listFiles().length > 0) {
            for (File f : dir.listFiles()) {
                if (f.isFile() && System.currentTimeMillis() - f.lastModified() >= 300000) {
                    try {
                        Files.move(f.toPath(), Paths.get(scriptPath.getString("path.script") + "/" + f.getName()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        Process exec = null;
        try {
            exec = Runtime.getRuntime().exec("sh " + scriptPath.getString("path.script") + "/convert.sh");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exec;
    }

    public static void modifyFileName(String path) throws FileNotFoundException {
        File dir = new File(path);
        if (!dir.exists())
            throw new FileNotFoundException("Directory " + path + " does not exists");

        for (File f : dir.listFiles()) {
            if (f.isFile() && System.currentTimeMillis() - f.lastModified() >= 300000) {
                try {
                    String newFileName = f.getName().replaceAll(" ", "");
                    Files.move(f.toPath(), Paths.get(ProcessSwiftFile.filePath.getString("file_path.mtPath") + "/" + f.getName().replaceAll(f.getName(), newFileName) /*+ ".txt"*/));
                    f.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*public static void main(String[] args) {
        Process p = execute();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String path = null;
        if (p != null) {
            path = scriptPath.getString("path.converted") + "/converted";
            if (path != null && !path.isEmpty()) {
                modifyFileName(path);
            } else {
                System.out.println("Please provide correct path inside script_execute.conf file");
            }
        }
    }*/
}
