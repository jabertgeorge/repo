function showAnch(obj)
{
	//alert("called");
	$(obj).find("a.add-w-16-w").show();
}

function hideAnch(obj)
{
	$(obj).find("a.add-w-16-w").hide();
}

function showCAnch(obj)
{
	$(obj).find("a.fav-w-16-w").show();
}

function hideCAnch(obj)
{
	$(obj).find("a.fav-w-16-w").hide();
}

/*
function expand(obj, acctId)
{
	$(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode).find(".acct-w-24-hvr").removeClass("acct-w-24-hvr").addClass("acct-w-24");
	$(obj.parentNode.parentNode).removeClass("acct-w-24").addClass("acct-w-24-hvr");
	$(obj.parentNode.parentNode.parentNode.parentNode.parentNode).find(".ahorizontal").hide();
	$(obj.parentNode.parentNode.parentNode.parentNode.parentNode).find(".dnf").hide();
	$(obj.parentNode.parentNode.parentNode.parentNode).find(".ahorizontal").show();

	// remove all the next objects with classes : 'vertical-line' and 'acct-detail'
	$(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode).nextAll().css("display", "none");

	// add next element i.e. vertical line
	$(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode).next().css("display", "table-cell");

	// add next element i.e. acctount detail box
	$(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode).next().next().css("display", "table-cell");

	if ( acctId == "Account 33" || acctId == "Account 32" || acctId == "Account 29" ) {
	// add next element i.e. acctount detail box
		//$(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode).next().next().css("display", "table-cell");
		$(obj.parentNode.parentNode.parentNode.parentNode).find(".dnf").show();
	}
}
*/

function showDetails(obj, res)
{
	//var res = [{"amt":"10,00,000.00","date":"12-03-2014 14:20:14"}];
	if ( res != undefined ) {
		var dataList = res;
		var grpdt = new Array();
		if ( dataList.length > 0 ) {
			for ( var j=0; j<dataList.length; j++ ) {
				grpdt[j] = new Array(2);
				if ( dataList[j].amt != undefined && dataList[j].amt != null )
					grpdt[j][0] = dataList[j].amt.toString();
				else
					grpdt[j][0] = "";
				if ( dataList[j].date != undefined && dataList[j].date != null )
					grpdt[j][1] = dataList[j].date.toString();
				else
					grpdt[j][1] = "";
			}
		}
		var oTable = $("#detTable").dataTable({"aaData": grpdt,
			"aoColumns": [
				{ "sTitle": "Amount" },
				{ "sTitle": "Date" }
			]
			,"bProcessing": true
        		,"bDestroy": true
		});
	}
	$(obj).overlay({effect: 'apple'});
	$(".dataTableCss").css("width", "100%");
}

function load(acct)
{
	alert("load this account :: "+acct);
}

function searchAcct()
{
	var isTrue = $("#laForm").validationEngine('validate', {scroll: false});
	if (!isTrue)
		return;
	var acctId = document.getElementById("acctTI").value;
	var startD = document.getElementById("startDTI").value;
	var endD = document.getElementById("endDTI").value;
	var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"begin\"}";
	alert("payload :: "+inObjStr);
	var inputObj = JSON.parse(inObjStr);
	// TODO
	//getAcctDetail(inputObj, searchH, true);
	/* remove this */
	var dummyRes = {
			    "account" : "123",
			    "credit" : [
				{
				    "acctId":"C Account 2",
				    "amount":"Rs. 40,00,000.00",
				    "details":[
					{"amt":"10,00,000.00","date":"12-03-2014 14:20:14"},
					{"amt":"20,00,000.00","date":"13-03-2014 12:22:14"},
					{"amt":"10,00,000.00","date":"15-03-2014 09:41:03"}
				    ]
				},
				{
				    "acctId":"C Account 3",
				    "amount":"Rs. 60,00,000.00",
				    "details":[
					{"amt":"10,00,000.00","date":"12-03-2014 14:20:14"},
					{"amt":"30,00,000.00","date":"13-03-2014 12:22:14"},
					{"amt":"20,00,000.00","date":"15-03-2014 09:41:03"}
				    ]
				}
			    ],
			    "debit" : [
				{
				    "acctId":"Account 2",
				    "amount":"Rs. 40,00,000.00",
				    "details":[
					{"amt":"10,00,000.00","date":"12-03-2014 14:20:14"},
					{"amt":"20,00,000.00","date":"13-03-2014 12:22:14"},
					{"amt":"10,00,000.00","date":"15-03-2014 09:41:03"}
				    ]
				},
				{
				    "acctId":"Account 3",
				    "amount":"Rs. 60,00,000.00",
				    "details":[
					{"amt":"10,00,000.00","date":"12-03-2014 14:20:14"},
					{"amt":"30,00,000.00","date":"13-03-2014 12:22:14"},
					{"amt":"20,00,000.00","date":"15-03-2014 09:41:03"}
				    ]
				},
				{
				    "acctId":"Account 4",
				    "amount":"Rs. 2,00,00,000.00",
				    "details":[
					{"amt":"50,00,000.00","date":"12-03-2014 14:20:14"},
					{"amt":"30,00,000.00","date":"13-03-2014 12:22:14"},
					{"amt":"20,00,000.00","date":"15-03-2014 09:41:03"},
					{"amt":"1,00,00,000.00","date":"15-03-2014 09:41:03"},
					{"amt":"1,00,00,000.00","date":"15-03-2014 09:41:03"}
				    ]
				}
			    ]
			};
	searchH(dummyRes);
}

function searchH(res)
{
	//alert("response ::: "+JSON.stringify(res));

	if ( res == undefined ) {
		$("#canDiv").html("");
		alert("internal server error");
		return;
	}

	// close the panel
	$("div#panel").slideUp("slow");
	$("#toggle a").toggle();

	if ( res.account != undefined && res.account != "" ) {
		// paint credit Area first
		var inObj = "<div id='creditArea' >";
		inObj += "<table>";

		if ( res.credit != undefined ) {
			var creJson = res.credit;
			if ( creJson.length > 0 ) {
				var det = "";
				for ( var i=0; i<creJson.length; i++ ) {
					det = creJson[i].details;
					inObj += "<tr>";
					inObj += "<td><div class='abutton cacct-w-24-crtd' onmouseover=\"showCAnch(this)\" onmouseout=\"hideCAnch(this)\" >"+creJson[i].acctId+" <span><a href='#' class='search-w-16-w-la' rel='#detailO' onClick='showDetails(this, "+JSON.stringify(det)+")' title='Details' ><span style='color:#fff;' >.</span></a><a href='#' class='fav-w-16-w' title='Load this account' style='display:none;' onClick=\"load('"+creJson[i].acctId+"')\" ></a></span></div></td>";
					inObj += "<td style='width:50px;' ><div class='bhorizontal' ></div></td>";
					inObj += "</tr>";
				}
			} else {
				inObj += "<tr>";
				inObj += "<td style='text-align:right;' ><div class='abutton dnf-w-16' style='vertical-align:middle; cursor:auto; background-color:#c0c0c0; color:#191919;' >Data not found</div></td>";
				inObj += "<td style='width:50px;' ><div class='bhorizontal' ></div></td>";
				inObj += "</tr>";
			}
		} else {
			alert("Internal server error");
		}

		inObj += "</table>"; // credit table closed
		inObj += "</div>"; // credit div closed

		inObj += "<div class='vertical-line' style='display:table-cell;' ></div>";

		inObj += "<div id='refAcctD' >";
		inObj += "<div style='vertical-align:middle; height:100%; float:none; display:table;' >";
		inObj +=	"<div style='vertical-align:middle; display:table-cell;' >";
		inObj +=		"<div class='horizontal' ></div>";
		inObj += 	"</div>";
		inObj += 	"<div style='vertical-align:middle; display:table-cell;' >";
		inObj += 		"<a class='abutton start-acct-w-24' style='vertical-align:middle;' >"+res.account+"</a>";
		inObj += 	"</div>";
		inObj += 	"<div style='vertical-align:middle; display:table-cell;' >";
		inObj +=		"<div class='horizontal' ></div>";
		inObj += 	"</div>";
		inObj += "</div>";
		inObj += "</div>";

		inObj += "<div class='vertical-line' style='display:table-cell;' ></div>";

		inObj += "<div id='debtArea' >";
		inObj +=	"<table>";

		if ( res.debit != undefined ) {
			var debJson = res.debit;
			if ( debJson.length > 0 ) {
				var det = "";
				for ( var j=0; j<debJson.length; j++ ) {
					det = debJson[j].details;
					inObj += "<tr>";
					inObj += 	"<td style='text-align:right; width:50px;' ><div class='bhorizontal' ></div></td>";
					inObj += 	"<td style='text-align:left;' ><div class='abutton acct-w-24' onmouseover=\"showAnch(this)\" onmouseout=\"hideAnch(this)\" >"+debJson[j].acctId+" <span><a href='#' class='search-w-16-w-la' rel='#detailO' onClick='showDetails(this, "+JSON.stringify(det)+")' title='Details' ><span style='color:#fff;' >.</span></a><a href='#' class='add-w-16-w' title='Expand' style='display:none;' onClick=\"getNext(this, '"+debJson[j].acctId+"')\" ></a></span></div></td>";
					inObj += 	"<td style='text-align:right; width:50px;' ><div class='ahorizontal' style='display:none;' ></div></td>";

					inObj += 	"<td style='text-align:left;' ><div class='abutton dnf-w-16' style='vertical-align:middle; cursor:auto; background-color:#c0c0c0; color:#191919; display:none;' >Data not found</div></td>";
					inObj+= "</tr>";
				}
			} else {
				inObj += "<tr>";
				inObj += 	"<td style='text-align:right; width:50px;' ><div class='ahorizontal' ></div></td>";

				inObj += 	"<td style='text-align:left;' ><div class='abutton dnf-w-16' style='vertical-align:middle; cursor:auto; background-color:#c0c0c0; color:#191919;' >Data not found</div></td>";
				inObj+= "</tr>";

			}
		} else {
			alert("Internal server error");
		}

		inObj += "</table>"; // debt table closed
		inObj += "</div>"; // debt div closed

		$("#canDiv").html(inObj);
	} else {
		$("#canDiv").html("");
	}
	//TODO
	//$("td a[rel]").overlay({effect: 'apple'});
	$(".acct-w-24, .acct-w-24-hvr, .cacct-w-24-crtd").contextmenu({'Load Account':loadAcct}, 'right', 1000);
}

function loadAcct(e,el)
{
	alert(" load account ::: "+$(el).clone().children().remove().end().text());
}

var detDivObj = null;
function getNext(tObj, acctId)
{
	this.detDivObj = tObj;
	var startD = document.getElementById("startDTI").value;
	var endD = document.getElementById("endDTI").value;
	var inObjStr = "{\"acctId\":\""+acctId+"\", \"minDate\":\""+startD+"\", \"maxDate\":\""+endD+"\", \"flag\":\"XYZ\"}";
	alert("payload :: "+inObjStr);
	var inputObj = JSON.parse(inObjStr);
	//TODO
	//getNextLevel(inputObj, nextH, true);
	/* remove this */
	var dummyD = {"account": "123","credit": [{"details": [{"amt": "3000.0","date": "2013-06-27"}],"acctId": "444","amount": "3000.0"},{"details": [{"amt": "3000.0","date": "2013-06-27"},{"amt": "3000.0","date": "2013-06-26"},{"amt": "3000.0","date": "2013-06-27"}],"acctId": "222","amount": "9000.0"},{"details": [{"amt": "3000.0","date": "2013-06-27"},{"amt": "3000.0","date": "2013-06-29"}],"acctId": "333","amount": "6000.0"}]};
	nextH(dummyD);
}

function nextH(resp)
{
	if ( resp == undefined ) {
		return;
	}
	if ( resp.credit == undefined ) {
		return;
	}

	var res = resp.credit;
	alert("response ::: "+JSON.stringify(res));
	var twoLevel = this.detDivObj.parentNode.parentNode;
	var fourLevel = twoLevel.parentNode.parentNode;
	var sixLevel = fourLevel.parentNode.parentNode;
	$(sixLevel).find(".acct-w-24-hvr").removeClass("acct-w-24-hvr").addClass("acct-w-24");

	$(fourLevel.parentNode).find(".dnf-w-16").hide();

	$(twoLevel).removeClass("acct-w-24").addClass("acct-w-24-hvr");
	$(fourLevel.parentNode).find(".ahorizontal").hide();
	$(fourLevel).find(".ahorizontal").show();
	
	if (res.length > 0) {
		var det = "";
		var nLvl = "<div class='vertical-line' style='display:table-cell; height:100%; vertical-align:middle;' ></div>";
		nLvl += "<div class='acct-detail' >";
		nLvl += 	"<table>";
			for ( var i=0; i<res.length; i++ ) {
				det = res[i].details;
				nLvl += "<tr>";
				nLvl += 	"<td style='text-align:right; width:50px;' ><div class='bhorizontal' ></div></td><td style='text-align:left;' ><div class='abutton acct-w-24' style='vertical-align:middle;' onmouseover=\"showAnch(this)\" onmouseout=\"hideAnch(this)\" >"+res[i].acctId+" <span style='vertical-align:middle;' ><a href='#' class='search-w-16-w-la' rel='#detailO' onClick='showDetails(this, "+JSON.stringify(det)+")' title='Details' ><span style='color:#fff;' >.</span></a><a href='#' class='add-w-16-w' title='Expand' style='display:none;' onClick=\"getNext(this, '"+res[i].acctId+"')\" ></a></span></div></td><td style='text-align:right; width:50px;' ><div class='ahorizontal' style='display:none;' ></div></td>";
				nLvl += 	"<td style='text-align:left;' ><div class='abutton dnf-w-16' style='cursor:auto; background-color:#c0c0c0; color:#191919; display:none;' >Data not found</div></td>";
				nLvl += "</tr>";
			}
		nLvl += 	"</table>";
		nLvl += "</div>";
		nLvl += "</div>";
	} else {
		$(this.detDivObj.parentNode.parentNode.parentNode.parentNode).find(".dnf-w-16").show();
		$(this.detDivObj.parentNode).next().show();
	}

	$(sixLevel.parentNode).nextAll().remove();
	$(sixLevel.parentNode.parentNode).append(nLvl);
	$("td a[rel]").overlay({effect: 'apple'});
}

