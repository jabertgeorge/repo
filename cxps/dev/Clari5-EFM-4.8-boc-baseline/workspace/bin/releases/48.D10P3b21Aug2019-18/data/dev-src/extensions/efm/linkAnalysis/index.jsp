<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	response.setHeader("Pragma","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Expires","0");
	response.setDateHeader("Expires",-1);
%>
<html>
	<head>

    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	<!--
	<link href="../common/css/bootstrap/bootstrap.css" rel="stylesheet">
	<link href="../common/css/bootstrap/bootstrap-theme.css" rel="stylesheet">
	<link rel="stylesheet" href="../common/css/jquery/jquery-ui.css">
	<link rel="stylesheet" href="../common/css/jquery/jquery-ui-1.10.4.css">
	<link href="../common/css/jquery/validation-engine/validationEngine.jquery.css" rel="stylesheet">
	<link rel="stylesheet" href="../common/css/jquery/demo_table.css" type="text/css"/>

	<link href="../common/css/jquery/jstree/style.css"  rel="stylesheet"  />
	<link href="../common/css/jquery/jquery.toastmessage.css" rel="stylesheet" />
	<link rel="stylesheet" href="../common/css/jquery/overlay-apple.css" type="text/css"/>
	-->
	<script type="text/javascript" src="../common/js/lib/loadCDN.js"></script>
	<script>
		loadCSS(["ext/css/bootstrap/bootstrap.css", "ext/css/bootstrap/bootstrap-theme.css", "ext/css/jquery/jquery-ui.css", "ext/css/jquery/jquery-ui-1.10.4.css", "ext/css/jquery/validation-engine/validationEngine.jquery.css", "ext/css/jquery/demo_table.css", "ext/css/jquery/jstree/style.css", "ext/css/jquery/jquery.toastmessage.css", "ext/css/jquery/overlay-apple.css"]);
	</script>
	<link rel="stylesheet" href="css/case.css">
	<link rel="stylesheet" href="../common/css/style.css" type="text/css"/>

  <style>
  #dialog label, #dialog input { display:block; }
  #dialog label { margin-top: 0.5em; }
  #dialog input, #dialog textarea { width: 95%; }
  #tabs { margin-top: 1em; }
  #tabs li .ui-icon-close { float: left; margin: 0.4em 0.2em 0 0; cursor: pointer; }
  #add_tab { cursor: pointer; }

  .clckBut{}

  a:hover, a:focus{
	text-decoration:none;
  }

.clckButactive {
      cursor: pointer;
  	  width:143px;
	color: #fff;
	background-color:#14143d !important;
  	  opacity: .7;
  	  margin: 0px 3px 3px 0;
  	  //border-radius: 4px 4px 4px 4px;
  	  border-top-left-radius:4px;
      -webkit-transform: scale(1.03,1.04);
      -webkit-transition-timing-function: ease-out;
      -webkit-transition-duration: 500ms;
      -moz-transform: scale(1.03,1.04);
      -moz-transition-timing-function: ease-out;
      -moz-transition-duration: 500ms;
       position: relative;
      z-index:101;
  	}
.clckButactive:hover{
background-color:#14143d !important;
}

 .schWindow{
  margin:0px 148px;
  width:18%;
  height:100%;
  z-index:90;
  background:#FFFFFF;
  display:none;
  position:absolute;
  text-align: center;
  border-radius: 8px 8px 8px 8px;
  vertical-align: middle;
  }

.schWindBlck{
  margin:0px 148px;
  width:18%;
  height:100%;
  z-index:-2;
  opacity:0.0;
  display:block;
  line-height: 1;
  position: fixed;
}

.clSrchInsideDiv{
background: url("../common/images/bw-b-28-wc.png") no-repeat scroll 0% 0% transparent;
width: 18px;
height: 18px;
margin-top:40px;
display:block;
float:right;
}

.clsWindow{
 margin:0px 148px;
  background:#FFFFFF;
  display:block;
  position:absolute;
  text-align: center;
  width:18px;
 height:15%;
float:left;
z-index:90;
margin-top:235px;
margin-left:375px;
border-radius: 0px 8px 8px 0px;
}

.opnWindow{
border-radius: 0px 8px 8px 0px;
border:1px solid #E0E0E0;
  margin:0px 148px;
  background:#FFFFFF;
  display:block;
  position:absolute;
  text-align: center;
  width:18px;
 height:15%;
float:left;
z-index:90;
margin-top:235px;
margin-left:158px;
}

.clSrchOutSideDiv{
background: url("../common/images/ff-b-28-wc.png") no-repeat scroll 0% 0% transparent;
width: 16px;
height: 18px;
margin-top:40px;
display:block;
float:right;
}

span {
  display: inline-block;
  vertical-align: middle;
  line-height: normal;
}

#openCloseWrap {
    font-size: 12px;
    font-weight: bold;
}

body {
	font: 1.1em Tahoma, sans-serif !important;
     }

.ui-tabs .ui-tabs-nav li a { 
white-space:normal;height: 20px;

} 

.tabs a:hover, .tabs a:focus, .tabs a:active {    
background: #fff;    
 cursor: pointer; 
 }

 .ui-tabs-selected a {  
  background-color: #fff;  
  color: #000;   
  font-weight: bold;   
 padding: 2px 8px 1px;   
  border-bottom: 1px solid #fff;  
 border-top: 3px solid #fabd23;  
 border-left: 1px solid #fabd23;  
 border-right: 1px solid #fabd23;  
 margin-bottom: -1px;   
 overflow: visible;
 } 

.tabMax{
overflow:hidden;width:97px;
}

.goBackCls{
width:80px;
margin:4px 0px 4px 91%;
color: #FFFFFF !important;
}

.jstree-icon{} 
.jstree-themeicon{} 
.jstree-themeicon-custo{}

.formErrorContent {
width: 100%;
background: #ee0101;
position: relative;
color: #fff;
min-width: 160px;
font-size: 11px;
border: 2px solid #ddd;
box-shadow: 0 0 6px #000;
-moz-box-shadow: 0 0 6px #000;
-webkit-box-shadow: 0 0 6px #000;
-o-box-shadow: 0 0 6px #000;
padding: 4px 10px 4px 10px;
border-radius: 6px;
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
-o-border-radius: 6px;
}

.dataTables_wrapper {
  overflow: auto;
}
  .sorting{
      padding-right: 16px;
  }

  </style>

	<script>
		var jqFile = ["ext/jquery/jquery-1.9.1.min.js"];
		loadFromCDN(jqFile, load);
		var jsFiles = ["ext/jquery/jquery-ui.js", "ext/jquery/validation-engine/jquery.validationEngine-en.js", "ext/jquery/validation-engine/jquery.validationEngine.js", "ext/jquery/jstree/jstree.min.js", "ext/jquery/jquery.dataTables.min.js", "ext/jquery/jquery.toastmessage-min.js"];
		function load()
		{
			loadFromCDN(jsFiles, null);
		}
	</script>	

	<script type="text/javascript" src="js/ioService.js"></script>
	<script type="text/javascript" src="../common/js/lib/utils.js" ></script>
	<script type="text/javascript" src="../common/js/lib/cxNetwork.js" ></script>
	<script type="text/javascript" src="js/dataTabUtil.js" ></script>
	<script type="text/javascript" src="js/utils.js" ></script>
	<script type="text/javascript" src="js/case.js" ></script>
	<script type="text/javascript" src="js/customer.js" ></script>
	<script type="text/javascript" src="js/account.js" ></script>
	<script type="text/javascript" src="js/transaction.js" ></script>

    <!--Device and merchant js for demo purpose -->
    <script type="text/javascript" src="js/merchant.js" ></script>
    <script type="text/javascript" src="js/device.js" ></script>


    <script type="text/javascript" src="js/linkAnalysis.js" ></script>


    <script>
      var reqstPage="";
      var tabCount=5;
      var tabTemplate = "<li class='ui-tabs-active ui-state-active tabMax' style='#{backStyle}' ><span style='#{tabImg}' role='presentation'></span><a style='color:#000000;font-size: 1.2em;overflow:hidden;width:105px;margin-left:-4px;' title='#{titleText}' onClick='#{clckFun}' oncontextmenu='return false;' class='' href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' style='float:right;margin-top:-2px;margin-right:-1px;' role='presentation'>Remove Tab</span></li>";
      var tabCounter = 1;
      	var tabCaseCount=new Array();
      	var tabCustCount=new Array();
        var tabAcctCount=new Array();
        var tabTranCount=new Array();
      	var tabs = null;
	var treeId="";
	var custRmlId="";
	var cxcifId="";
	var cxacctcid="";

function init() 
{
	initTxn();
	initCust();
	initAcct();
	initCase();

	$(document).tooltip();
	$("#tabNavUl").hide();
	$("#canvas").hide();
    tabs = $("#canvas").tabs();
     $("#gridTabs").tabs();
    var firstClick=1;
    $("#refId").val("");
     loadIODetailsFromJira(top.refId,top.cName,top.src);
	$(".clckBut").click(function() {
	    var brdVal="";
	    $("#leftForm").validationEngine('validate', {scroll: false});
	    if(firstClick==1){
		intializeArray();
		treeObj="";
		cxacctcid="";
		cxcifId="";
		$("#comptDiv").empty();
	        if($("#refId").val()!=""){
                confirmTab($(this).text(),$("#refId").val());
	        }else{
                brdVal="2px solid #14143D";//+getColor($(this).text());
                $("#srchWindow").css('border',brdVal);
                $(this).removeClass("clckBut");
                $(this).addClass("clckButactive");
                $(this).css('line-height','30px;');
                $("#clSchWd").css('border-right',brdVal);
                $("#clSchWd").css('border-top',brdVal);
                $("#clSchWd").css('border-bottom',brdVal);
                $("#srchWindow").show();
                $("#clSchWd").show();
                firstClick++;
                clearTabs();
                if($(this).text()=="Case"){
                       loadCase("comptDiv",caseSrch);
                       $("#fromDate").datepicker();
                       $("#toDate").datepicker();
                       $("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
                       $("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
		       $("#fromDate").datepicker( "option", "maxDate", new Date());
		       $("#toDate").datepicker( "option", "maxDate", new Date());
                }

                if($(this).text()=="Customer"){
                       loadCase("comptDiv",custSrch);
                }

                if($(this).text()=="Account"){
                       loadCase("comptDiv",acctSrch);
                }

                if($(this).text()=="Transaction"){
                      loadCase("comptDiv",tranSrch);
                      $("#fromDate").datepicker();
                      $("#toDate").datepicker();
                      $("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
                      $("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
   		      $("#fromDate").datepicker( "option", "maxDate", new Date());
		      $("#toDate").datepicker( "option", "maxDate", new Date());
                }


                //code for merchant and device
                if($(this).text()=="Merchant"){
                    loadCase("comptDiv",mrchntSrch);
                }
                if($(this).text()=="Device"){
                    loadCase("comptDiv",dvcSrch);
                }
               // if($(this).text().replace(/ +/g, "")=="LinkAnalysis"){
               //     loadCase("comptDiv",linkSrch);
               // }
                //It checks the text of button
                if($(this).text().replace(/ +/g, "")=="EntityLinkAnalysis"){
                    loadCase("comptDiv",linkSrch);
                }
                if($(this).text().replace(/ +/g, "")=="EventLinkAnalysis"){
                    loadCase("comptDiv",linkSrch);
                }
	        }

	    }
	    else{
         var r=confirm("Do you want to do new search?");
            if (r==true)
              {
		intializeArray();
		treeObj="";
		cxacctcid="";
		cxcifId="";
	        if($("#refId").val()==""){
               $("#srchWindow").show();
               brdVal="2px solid #14143D";//+getColor($(this).text());
               $("#srchWindow").css('border',brdVal);
               $("div#leftPanel").children().removeClass("clckButactive");
               $("div#leftPanel").children().addClass("clckBut");
               $(this).addClass("clckButactive");
                $("#clSchWd").css('border-right',brdVal);
                $("#clSchWd").css('border-top',brdVal);
                $("#clSchWd").css('border-bottom',brdVal);
                $(this).css('line-height','30px;');
		$("#clSchWd").show();
	       $("#comptDiv").empty();
               clearTabs();
               confirmTab($(this).text(),$("#refId").val());
		}else{
	               $("#comptDiv").empty();
		       $("#srchWindow").hide();
			$("#clSchWd").hide();
	               $(this).addClass("clckButactive");
	               clearTabs();
        	       confirmTab($(this).text(),$("#refId").val());
		}
              }
              firstClick++;
	    }
	});

    tabs.delegate( "span.ui-icon-close", "click", function() {
      var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
      $( "#" + panelId ).remove();
      if($("ul#tabNavUl").children().length==0){
       	$("#tabNavUl").hide();
       	$("#canvas").hide();
      }
      clearArray(panelId);
      tabs.tabs("refresh");
      $("ul#tabNavUl li:last-child").addClass("ui-tabs-active ui-state-active");
      var actId=$("ul#tabNavUl li:last-child").attr("aria-controls");
      $("div#"+actId).show();
      tabs.tabs("refresh");
//var tabSize=tabCaseCount.length+tabAcctCount.length+tabCustCount.length+tabTranCount.length;
        var tabSize=tabCaseCount.length+tabAcctCount.length+tabCustCount.length+tabTranCount.length+tabMrchntCount.length+tabDvcCount.length; ///Added for Merchant
		tabSizeexpander();
    });

    tabs.bind( "keyup", function( event ) {
      if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
        var panelId = tabs.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
        $("#"+panelId).remove();
        tabs.tabs( "refresh" );
      }
    });
}

function knowTabId(id,count){
	var TabID=($(id).attr("href").split("--")[0]).split("#")[1];
	if(TabID=="Account"){
		accountID=$(id).attr("href").split("--")[1];
		acctCount=count+1;
	}else if(TabID=="Customer"){
		customerId=$(id).attr("href").split("--")[1];
		custCount=count+1;
	}else if(TabID=="Case"){
		caseId=$(id).attr("href").split("--")[1];
		caseCount=count+1;
	}else if(TabID=="Transaction"){
		transactionId=$(id).attr("href").split("--")[1];
		tranCount=count+1;
	}else if(TabID=="Merchant"){    //Added for merchant
        mrchntId=$(id).attr("href").split("--")[1];
        mrchntCount=count+1;
    }
}

function intializeArray(){
   tabCaseCount=new Array();
   tabCustCount=new Array();
   tabAcctCount=new Array();
   tabTranCount=new Array();
   tabMrchntCount=new Array();  //Added for merchant
   tabDvcCount=new Array();  //Added for device
   tabLinkCount=new Array();  //Added for device
}
function confirmTab(id,value){

	if(id=="Case"){

	    if(value!=""){
		clearTabs();
		addTab(id,value);
	    }else{
		loadCase("comptDiv",caseSrch);
		setSrchCss();
		$("#fromDate").datepicker();
		$("#toDate").datepicker();
		$("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
		$("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
	$("#fromDate").datepicker( "option", "maxDate", new Date());
	$("#toDate").datepicker( "option", "maxDate", new Date());
	    }
	}

	if(id=="Customer"){
	    if(value!=""){
		clearTabs();
		addTab(id,value);
	    }
	    else{
		loadCase("comptDiv",custSrch);
		setSrchCss();
	    }
	}

	if(id=="Account"){
	    if(value!=""){
		clearTabs();
		addTab(id,value);
	    }else{
		loadCase("comptDiv",acctSrch);
		setSrchCss();
	    }
	}

	if(id=="Transaction"){
	    if(value!=""){
		clearTabs();
		addTab(id,value);
	    }else{
		loadCase("comptDiv",tranSrch);
		setSrchCss();
	      $("#fromDate").datepicker();
	      $("#toDate").datepicker();
	      $("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
	      $("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
	$("#fromDate").datepicker( "option", "maxDate", new Date());
	$("#toDate").datepicker( "option", "maxDate", new Date());
	    }
	}
    /*Code for Merchant and device
    * for loading the search field */
    if(id=="Merchant"){
        if(value!=""){
            clearTabs();
            addTab(id,value);
        }
        else{
            loadCase("comptDiv",mrchntSrch);
            setSrchCss();
        }
    }

    if(id=="Device"){
        if(value!=""){
            clearTabs();
            addTab(id,value);
        }
        else{
            loadCase("comptDiv",dvcSrch);
            setSrchCss();
        }
    }

    if(id.replace(/ +/g, "")=="LinkAnalysis"){
        if(value!=""){
            clearTabs();
            addTab(id,value);
        }
        else{
            loadCase("comptDiv",linkSrch);
            setSrchCss();
        }
    }

    if(id.replace(/ +/g, "")=="EntityLinkAnalysis"){
        if(value!=""){
            clearTabs();
            addTab(id,value);
        }
        else{
            loadCase("comptDiv",linkSrch);
            setSrchCss();
        }
    }
}

function loadIODetailsFromJira(refId,cName,src){
	if(src=="jira"){
		confirmTab(cName,refId);
	}
}
function clearArray(panelId){
    if ( panelId.split("-")[0] == "Customer" ) {
	   tabCustCount.splice(tabCustCount.indexOf(panelId,1));
    }
    if ( panelId.split("-")[0] == "Account" ) {
        tabAcctCount.splice(tabAcctCount.indexOf(panelId,1));
    }
    if( panelId.split("-")[0]=="Case"){
        tabCaseCount.splice(tabCaseCount.indexOf(panelId,1));
    }
    if( panelId.split("-")[0]=="Transaction"){
        tabTranCount.splice(tabTranCount.indexOf(panelId,1));
    }
    /*Code for merchant and device and link analysis*/
    if( panelId.split("-")[0]=="Merchant"){
        tabMrchntCount.splice(tabMrchntCount.indexOf(panelId,1));
    }
    if( panelId.split("-")[0]=="Device"){
        tabDvcCount.splice(tabDvcCount.indexOf(panelId,1));
    }
    if( panelId.split("-")[0]=="LinkAnalsis"){
        tabLinkCount.splice(tabLinkCount.indexOf(panelId,1));
    }
}

function setSrchCss(){
   if($("#srchWindow").attr('style').indexOf("block")>0){
	toggleClassByID("clSchWd","clsWindow","opnWindow");
    }else{
    $("#clSchWd").toggleClass("opnWindow");
    $("#clSchWd").toggleClass("clsWindow");
    }
    toggleClassByID("imgWindow","clSrchInsideDiv","clSrchOutsideDiv");
}

function clearTabs(){
	$("#canvas").empty();
	$("#canvas").attr('style','display:none');
	$("#canvas").append("<ul id='tabNavUl' class='ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'' style='display: none;' role='tablist'></ul>");
		intializeArray();
}

function getColor(reqFor){
    if ( reqFor == "Customer" ) {
        return bg = "#C2C2E0";
    }
    if ( reqFor == "Account" ) {
        return bg = "#8585C2";
    }
    if(reqFor=="Case"){
        return bg = "#5A5A93";
    }
    if(reqFor=="Transaction"){
        return bg = "#56567C";
    }
    if(reqFor=="Merchant"){ //Added for merchant
        return bg = "#56567C";
    }
}

function loadCase(mainId,divText){
	$("#"+mainId).append(divText);
}

function addTab(reqFor,dataId)
{
    reqstPage=reqFor;
	var bg="";
	var wid="";
	var label="";
	if(dataId.length>0){
	     label = dataId || "Tab " + tabCounter;
	}else{
	     label = reqFor || "Tab " + tabCounter;
	}
	if(label.length>=19){
		wid="18%";
	}else if(label.length>=9 && label.length<=19){
		wid="16%";
	}else{
		wid="9%";
	}
	var tabimage="";
	if ( reqFor == "Customer" ) {
		bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
		tabimage = "font-size: 10px; background: url(\"../common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
	}
	if ( reqFor == "Account" ) {
		bg = "width:130px;height:24px;font-size:10px;";
		tabimage = "font-size: 10px; background: url(\"../common/images/acct-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;margin-top:2px;"
	}
	if(reqFor=="Case"){
		bg = "width:130px;height:24px;font-size:10px;";
		tabimage = "font-size: 10px; background: url(\"../common/images/case-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;margin-top:2px;"

	}
	if(reqFor=="Transaction"){
		bg = "width:130px;height:24px;font-size:10px;";
		tabimage = "font-size: 10px; background: url(\"../common/images/txn-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;margin-top:2px;"

	}
    //Additional field for merchant
    if ( reqFor == "Merchant" ) {
        bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
        tabimage = "font-size: 10px; background: url(\"../common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
    }
    if ( reqFor == "Device" ) {
        bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
        tabimage = "font-size: 10px; background: url(\"../common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
    }
    //if ( reqFor == "LiinkAnalysis" ) {
    //    bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
    //    tabimage = "font-size: 10px; background: url(\"../common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
    //}
    if ( reqFor == "EntityLinkAnalysis" ) {
        bg = "width:130px;height:24px;font-size:10px;overflow:hidden;";
        tabimage = "font-size: 10px; background: url(\"../common/images/cust-b-20.png\") no-repeat scroll 0px 0px; height: 23px; float: left; padding-left: 16px; margin-right:-6px;color:#000000;margin-top:2px;"
    }

var id="";
if(dataId!=""){
id = reqFor+"--" +dataId;
}else{
id =reqFor+"--" +tabCounter;
}
var li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ).replace( /#\{backStyle\}/g, bg ).replace( /#\{tabImg\}/g, tabimage ).replace( /#\{titleText\}/g,dataId).replace( /#\{clckFun\}/g,"knowTabId(this,"+tabCounter+")"));

    var existId=reqFor+"--"+id.split("_")[id.split("_").length-1];
	if ( reqFor == "Customer" ) {
    if(dataId!=""){
        if(tabCustCount.indexOf(existId)>=0 || tabCustCount.indexOf(id)>=0){
	if(tabCustCount.indexOf(id)>=0){
	  activateOpenTab(id,reqFor);
	}else{
	  activateOpenTab(existId,reqFor);
	}
        }else{
	  maintainTabData(tabCustCount,id);
          addTabMenu(li,id);
	treeId="custmContainer"+tabCounter;
          loadCase(id,getCustTreeData(tabCounter,id));
	$("#custMenu"+tabCounter).tabs();
          custLoad(dataId,tabCounter);
	activateOpenTab(id,reqFor);
        }
    }
    else{
	   maintainTabData(tabCustCount,id);
           addTabMenu(li,id);
	treeId="custmContainer"+tabCounter;
   	   loadCase(id,custOutput);
	activateOpenTab(id,reqFor);
   	   }
	}

	if ( reqFor == "Account" ) {

    if(dataId!=""){
        if(tabAcctCount.indexOf(existId)>=0 || tabAcctCount.indexOf(id)>=0){
	if(tabAcctCount.indexOf(id)>=0){
	  activateOpenTab(id,reqFor);
	}else{
	  activateOpenTab(existId,reqFor);
	}
        }else{
	  maintainTabData(tabAcctCount,id);
	addTabMenu(li,id);
	treeId="acctContainer"+tabCounter;
       loadCase(id,getAcctTreeData(tabCounter,id));
	$("#acctMenu"+tabCounter).tabs();
      $("#fromDate"+tabCounter).datepicker();
      $("#toDate"+tabCounter).datepicker();
      $("#fromDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
      $("#toDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
      $("#fromDate"+tabCounter).datepicker( "option", "maxDate", new Date());
      $("#toDate"+tabCounter).datepicker( "option", "maxDate", new Date());
      acctLoad(dataId,tabCounter);
	activateOpenTab(id,reqFor);
        }
    }
   	 else{
       maintainTabData(tabAcctCount,id);
       addTabMenu(li,id);
	treeId="acctContainer"+tabCounter;
       loadCase(id,acctOutput);
	activateOpenTab(id,reqFor);
   	   }
	}

	if(reqFor=="Case"){
	if(dataId!=""){
        if(tabCaseCount.indexOf(existId)>=0 || tabCaseCount.indexOf(id)>=0){
	if(tabCaseCount.indexOf(id)>=0){
	  activateOpenTab(id,reqFor);
	}else{
	  activateOpenTab(existId,reqFor);
	}
        }else{
	  maintainTabData(tabCaseCount,id);
	  addTabMenu(li,id);
	treeId="caseContainer"+tabCounter;
	  loadCase(id,getCaseTreeData(tabCounter,id));
	$("#caseMenu"+tabCounter).tabs();
      $("#fromDate").datepicker();
      $("#toDate").datepicker();
      $("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
      $("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
      $("#toDate").datepicker( "option", "maxDate", new Date());
	caseLoad(dataId,tabCounter,getModuleFromCaseId(dataId));
	activateOpenTab(id,reqFor);
        }
    }
   	 else{
	        maintainTabData(tabCaseCount,id);
		addTabMenu(li,id);
	treeId="caseContainer"+tabCounter;
		loadCase(id,caseOutput);
	activateOpenTab(id,reqFor);
   	   }
	}

	if(reqFor=="Transaction"){
	treeId="tranContainer";
	if(dataId!=""){
        if(tabTranCount.indexOf(existId)>=0 || tabTranCount.indexOf(id)>=0){
	if(tabTranCount.indexOf(id)>=0){
	  activateOpenTab(id,reqFor);
	}else{
	  activateOpenTab(existId,reqFor);
	}
        }else{
	  maintainTabData(tabTranCount,id);
	addTabMenu(li,id);
	treeId="tranContainer"+tabCounter;
       loadCase(id,getTranTreeData(tabCounter,id));
      $("#fromDate").datepicker();
      $("#toDate").datepicker();
      $("#fromDate").datepicker( "option", "dateFormat","yy-mm-dd");
      $("#toDate").datepicker( "option", "dateFormat","yy-mm-dd");
      $("#toDate").datepicker( "option", "maxDate", new Date());
       tranLoad(dataId,tabCounter,id);
	activateOpenTab(id,reqFor);
        }
    }
   	 else{
	        maintainTabData(tabTranCount,id);
	addTabMenu(li,id);
	treeId="tranContainer"+tabCounter;
       loadCase(id,tranOutput);
	activateOpenTab(id,reqFor);
   	   }
    }

    /*Added for Meerchant tab*/
    if ( reqFor == "Merchant" ) {

        if(dataId!=""){
            if(tabMrchntCount.indexOf(existId)>=0 || tabMrchntCount.indexOf(id)>=0){
                if(tabMrchntCount.indexOf(id)>=0){
                    activateOpenTab(id,reqFor);
                }else{
                    activateOpenTab(existId,reqFor);
                }
            }else{
                maintainTabData(tabMrchntCount,id);
                addTabMenu(li,id);
                treeId="acctContainer"+tabCounter;
                loadCase(id,getAcctTreeData(tabCounter,id));
                $("#acctMenu"+tabCounter).tabs();
                $("#fromDate"+tabCounter).datepicker();
                $("#toDate"+tabCounter).datepicker();
                $("#fromDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
                $("#toDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
                $("#fromDate"+tabCounter).datepicker( "option", "maxDate", new Date());
                $("#toDate"+tabCounter).datepicker( "option", "maxDate", new Date());
                acctLoad(dataId,tabCounter);
                activateOpenTab(id,reqFor);
            }
        }
        else{
            maintainTabData(tabMrchntCount,id);
            addTabMenu(li,id);
            treeId="mrchntContainer"+tabCounter;
            loadCase(id,mrchntOutput);
            activateOpenTab(id,reqFor);
        }
    }

    if ( reqFor == "Device" ) {

        if(dataId!=""){
            if(tabDvcCount.indexOf(existId)>=0 || tabDvcCount.indexOf(id)>=0){
                if(tabDvcCount.indexOf(id)>=0){
                    activateOpenTab(id,reqFor);
                }else{
                    activateOpenTab(existId,reqFor);
                }
            }else{
                maintainTabData(tabDvcCount,id);
                addTabMenu(li,id);
                treeId="acctContainer"+tabCounter;
                loadCase(id,getAcctTreeData(tabCounter,id));
                $("#acctMenu"+tabCounter).tabs();
                $("#fromDate"+tabCounter).datepicker();
                $("#toDate"+tabCounter).datepicker();
                $("#fromDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
                $("#toDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
                $("#fromDate"+tabCounter).datepicker( "option", "maxDate", new Date());
                $("#toDate"+tabCounter).datepicker( "option", "maxDate", new Date());
                acctLoad(dataId,tabCounter);
                activateOpenTab(id,reqFor);
            }
        }
        else{
            maintainTabData(tabDvcCount,id);
            addTabMenu(li,id);
            treeId="dvcContainer"+tabCounter;
            loadCase(id,dvcOutput);
            activateOpenTab(id,reqFor);
        }
    }

    if ( reqFor == "Link Analysis" ) {

        if(dataId!=""){
            if(tabLinkCount.indexOf(existId)>=0 || tabLinkCount.indexOf(id)>=0){
                if(tabLinkCount.indexOf(id)>=0){
                    activateOpenTab(id,reqFor);
                }else{
                    activateOpenTab(existId,reqFor);
                }
            }else{
                maintainTabData(tabLinkCount,id);
                addTabMenu(li,id);
                treeId="acctContainer"+tabCounter;
                loadCase(id,getAcctTreeData(tabCounter,id));
                $("#acctMenu"+tabCounter).tabs();
                $("#fromDate"+tabCounter).datepicker();
                $("#toDate"+tabCounter).datepicker();
                $("#fromDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
                $("#toDate"+tabCounter).datepicker( "option", "dateFormat","yy-mm-dd");
                $("#fromDate"+tabCounter).datepicker( "option", "maxDate", new Date());
                $("#toDate"+tabCounter).datepicker( "option", "maxDate", new Date());
                acctLoad(dataId,tabCounter);
                activateOpenTab(id,reqFor);
            }
        }
        else{
            maintainTabData(tabLinkCount,id);
            addTabMenu(li,id);
            treeId="dvcContainer"+tabCounter;
            loadCase(id,linkOutput);
            activateOpenTab(id,reqFor);
        }
    }

	//var tabSize=tabCaseCount.length+tabAcctCount.length+tabCustCount.length+tabTranCount.length;
    var tabSize=tabCaseCount.length+tabAcctCount.length+tabCustCount.length+tabTranCount.length+tabMrchntCount.length+tabDvcCount.length+tabLinkCount.length; //Added for merchant
	if(tabSize>=8){
		tabSizeShinker();
	}
}

function addTabMenu(li,id){
	for(var t=0;t<$("ul#tabNavUl li").length;t++){
	$("ul#tabNavUl li").removeClass("ui-tabs-active");
	$("ul#tabNavUl li").removeClass("ui-state-active");
	}
	tabs.find("#tabNavUl").append( li );
        tabs.append( "<div id='" + id + "' style='width: 101%; height:100%;'></div>" );
        tabs.tabs("refresh");
        tabCounter++;
        $("div#"+id).show();
        $("#tabNavUl").show();
        $("#canvas").attr("style","display:block;height:100.3%;");
	var index = $("#"+id).index();
}

function activateOpenTab(tabId,reqForm){
	var index = $("#"+tabId).index();
	tabs.tabs({ active: (index-1) });
}

function tabSizeShinker(){
	var n = $("#tabNavUl li").length;
	var w = 0;
	var wid=0;
	var addVal=(n>=10 && n<12)?4.0:((n>=12 && n<14)?6.0:((n>=14 && n<16)?8.0:10.0));
	if(n>=10){
		if(n>=12){
		   w=((100.0)/(n+addVal));
		   wid=100-((n/4)*n+w);
		}else{
		   w=((100.0)/(n+addVal));
		   wid=100-((n/4)*n+w);
		}
	}else{
	w=(100/(n+3));
	wid=100-(n+w);
	}
	$("#tabNavUl li").width(w+'%');
	$("#tabNavUl li a").css('width',wid+'px');
}

function tabSizeexpander(){
	var n = $("#tabNavUl li").length;
	var w=0;
	var wid=0;
	var redVal=(n>=8 && n<10)?3.0:((n>=10 && n<12)?5.0:((n>=12 && n<14)?7.0:9.0));
	if(n>=9){
	   if(n>=11){
	   w=((100.0)/(n+redVal));
	   wid=100-(n*redVal/2)-10;
	   w+="%";
	   }else{
		   w=((100.0)/(n+redVal));
		   wid=100-(n*redVal);
		   w+="%";
		}
	}else{
		wid="97";
		w="130px";
	}
	$("#tabNavUl li").width(w);
	$("#tabNavUl li a").css('width',wid+'px');
}

function maintainTabData(tabArry,id){
	if(tabArry.length>=5){
	    var tempArray=tabArry;
	    var contId=tabArry.splice(1,4,tempArray[2],tempArray[3],tempArray[4],id);
	    removeTabExceedCount(contId[0]);
	    $("#"+id).show();
	}
	else{
	    tabArry.push(id);
	}
}

function removeTabExceedCount(id){
	 var lid= "li[aria-controls="+id+"]";
	 $('ul[id=tabNavUl] '+lid).remove();
	 $("#"+id).remove();
	 tabs.tabs("refresh");
}

function toggleSrchWindow(){

if($("#imgWindow").attr('class')=="clSrchInsideDiv"){
    $("#srchWindow").animate({width: 'toggle'},"slow");
    $( ".clsWindow" ).animate({
        left: "-=204"
    }, 530, function() {
    $("#clSchWd").toggleClass("clsWindow");
    $("#clSchWd").toggleClass("opnWindow");
    toggleClassByID("imgWindow","clSrchOutsideDiv","clSrchInsideDiv");
    $("#clSchWd").css('left','0px');
    });
    return false;
}else{
    $("#srchWindow").animate({width: 'toggle'},"slow");
    $( ".opnWindow" ).animate({
    left: "+=204"
    }, 610, function() {
    $("#clSchWd").toggleClass("opnWindow");
    $("#clSchWd").toggleClass("clsWindow");
    toggleClassByID("imgWindow","clSrchInsideDiv","clSrchOutsideDiv");
    $("#clSchWd").css('left','0px');
    });
}

}

function openLinkAnanlysis(linkUrl){
	var wind;
        wind=window.open(linkUrl,'Link Ananlysis', 'height=950, width=1200, toolbar=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=yes, modal=yes');
	wind.focus();
}

function goBack(backId){
    var index = $("#"+backId).index();
    tabs.tabs({ active: (index-2) });
}

function linkClose(){
$('#linkAnalysis').fadeIn('fast',function(){
      	$('#linkAnalysis').animate({'top':'-350px'},5,function(){
            $('#linkOverlay').fadeOut('fast');
        });
    });
}

function viewLinkAnanlysis(acctId){
	$('#linkAnalysis').fadeIn('fast',function(){
		$('#linkOverlay').fadeIn('fast');
		$('#linkAnalysis').animate({'top':'200px'},5,function(){
		});
        });
	$("#dataTableLink").remove();
	var linkData="";
		linkData="<table id='dataTableLink'><tr><td style='font-size:0.79em; padding:4px 0 4px 0;' >Account ID</td><td style='font-size:0.79em;  padding:4px 0 4px 0;' id='linkId'>"+acctId+"</td></tr><tr><td style='padding-left:0px;' ><a class='abutton submit-w-24 ' href='javascript:loadAnanlysis()'>OK</a></td><td><a class='abutton cancel-w-20' href='javascript:linkClose()' >Cancel</a></td></tr></table>";
	$("#linkAnalysis").append(linkData);
}

function loadAnanlysis(){
openLinkAnanlysis("la.jsp?userId="+top.user+"&refId="+$("#linkId").html());
$('#linkAnalysis').fadeIn('fast',function(){
$('#linkAnalysis').animate({'top':'-350px'},5,function(){
    $('#linkOverlay').fadeOut('fast');
});
    });
}
function backAnanlysis(){
	$("#iDiv").show();
	$("#analDiv").hide();
}
  </script>
	</head>

	<body onload="init()" >
		<div style="width:101%; height:100%;"  id="iDiv">
	<div id='leftPanel' style="width:12%; border:1px solid #808080; border-radius:7px; float:left; height:100%; padding:3px;background:#fcfcfc;" ></br></br></br>
			<form id="leftForm" style="display:none;">
				<div style='font-size:14px;' >
                    <input type="text" class="validate[custom[oneSpcLetterNumber]]" placeholder="Customer/Account ID" value="" style="margin:10px; width:120px;"  id="refId">
                    </input>
                </div>
            </form>
            <a id='custDiv' class='abutton cust-w-def clckBut' style='margin-bottom:6px;display:none;' >Customer</a>
            <a id='acctDiv' class='abutton acct-w-def clckBut' style='margin-bottom:6px;display:none;' >Account</a>
            <a id='merDiv' class='abutton cust-w-def clckBut' style='margin-bottom:6px;display:none;' >Merchant</a>
            <a id='deviceDiv' class='abutton acct-w-def clckBut' style='margin-bottom:6px;display:none;' >Device</a>
        <!--<a id='linkAnalysisDiv' class='abutton acct-w-def clckBut' style='margin-bottom:6px;' >Link Analysis</a>-->
        <a id='linkAnalysisDiv' class='abutton acct-w-def clckBut' style='margin-bottom:6px;' >Entity Link Analysis</a>
            <!--<a id='eventLinkAnalysisDiv' class='abutton acct-w-def clckBut' style='margin-bottom:6px;dislplay:none;' >Event Link Analysis</a>-->
	</div>

<div class="overlay" id="linkOverlay" style="display:none;"></div><div id="linkAnalysis" style="display:none;" class="analTab" ><a class="boxclose" id="linkClose" onclick="linkClose()" href="#" ></a><br/><div id="analLink" style="margin:-16px 0px 0px -10px; border:1px solid #808080; border-radius:0 10px 7px 7px;width:110%;"><div class="heading" ><h3>Confirm to load link analysis</h3></div></div></div>
<div></div>
            <div id='slidePnl'><div id='srchWindow' class='schWindow' ><div id='comptDiv'></div></div><div id='clSchWd' class='clsWindow' style='display:none;float:left;left:0px;'><a href='#' id='imgWindow' class='clSrchInsideDiv' onclick='toggleSrchWindow()' style=''></a></div></div>
			<div style="width:86.7%; border:1px solid #808080; border-radius:7px; margin-left:4px; float:left; height:100%; padding:3px; background:#fcfcfc;" >
			<div id="canvas" style="height:99%;">
				<ul id="tabNavUl" >
				</ul>
			</div>
			</div>
		</div>
<div style="width:100%; height:100%;"  id="analDiv" style="display:none;"><div><a onclick="backAnanlysis()" class="abutton back-w-def goBackCls" >Back</a></div></div>
	</body>
</html>
