package clari5.kyc.custom;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class KycExceptionMapper implements ExceptionMapper<KycException> {

    @Override
    public Response toResponse(KycException exception) {
       // return wrap(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), exception.getReason());
        return wrap(403, exception.getReason());
    }

    private Response wrap(int httpCode, String messsge) {
        return Response.status(httpCode).type(MediaType.APPLICATION_JSON_TYPE).entity(messsge).build();
    }

}
