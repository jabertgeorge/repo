package clari5.kyc.custom.pojo;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

public class CustFatcaUserDetails {

    private String custId;
    private String name;
    private String address;
    private String city;
    private String state;
    private String country;
    private String pcode;
    private String tin;
    private String fatcaDeclaration;
    private String udType;

    public static ArrayList<CustFatcaUserDetails> getUsers(ResultSet rs) {
        ArrayList<CustFatcaUserDetails> custFatcaUserDetailList = new ArrayList<>();

        try {
            while (rs.next()) {
                int index = rs.getRow() - 1;

                CustFatcaUserDetails custFatcaUsersDetails = new CustFatcaUserDetails();
                custFatcaUsersDetails.setCustId(rs.getString("cust_id"));
                custFatcaUsersDetails.setName(rs.getString("name"));
                custFatcaUsersDetails.setAddress(rs.getString("address"));
                custFatcaUsersDetails.setCity(rs.getString("city"));
                custFatcaUsersDetails.setState(rs.getString("state"));
                custFatcaUsersDetails.setCountry(rs.getString("country"));
                custFatcaUsersDetails.setPcode(rs.getString("pcode"));
                custFatcaUsersDetails.setTin(rs.getString("tin"));
                custFatcaUsersDetails.setFatcaDeclaration(rs.getString("fatca_declaration"));
                custFatcaUsersDetails.setUdType(rs.getString("ud_type"));

                custFatcaUserDetailList.add(custFatcaUsersDetails);
            }
            if (custFatcaUserDetailList.size() == 0)
                return null;

            return custFatcaUserDetailList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getFatcaDeclaration() {
        return fatcaDeclaration;
    }

    public void setFatcaDeclaration(String fatcaDeclaration) {
        this.fatcaDeclaration = fatcaDeclaration;
    }

    public String getUdType() {
        return udType;
    }

    public void setUdType(String udType) {
        this.udType = udType;
    }
}