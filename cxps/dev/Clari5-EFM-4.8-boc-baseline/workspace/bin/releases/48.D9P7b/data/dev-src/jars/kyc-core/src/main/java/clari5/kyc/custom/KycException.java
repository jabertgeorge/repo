package clari5.kyc.custom;

public class KycException extends Exception {

    private String reason;

    public KycException(String reason) {
        super(reason);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }


}
