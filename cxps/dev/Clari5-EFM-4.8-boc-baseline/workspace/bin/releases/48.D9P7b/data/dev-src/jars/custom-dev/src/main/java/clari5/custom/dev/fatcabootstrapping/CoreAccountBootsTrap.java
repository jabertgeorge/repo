package clari5.custom.dev.fatcabootstrapping;

import clari5.platform.dbcon.CxConnection;
import cxps.events.FT_AccountTxnEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class CoreAccountBootsTrap {

    public static void updateCustomerForCoreAccount(FT_AccountTxnEvent ft_accountTxnEvent) throws SQLException {

        CxConnection connection = CustomDerivator.getConnection();

        try {
            if (ft_accountTxnEvent.getCxCustId() != null && !ft_accountTxnEvent.getCxCustId().isEmpty()
                    && ft_accountTxnEvent.getInternalTxnCode().isEmpty() &&
                    ft_accountTxnEvent.getInternalTxnCode() != null &&
                    (!ft_accountTxnEvent.getInternalTxnCode().equalsIgnoreCase("97") &&
                            !ft_accountTxnEvent.getInternalTxnCode().equalsIgnoreCase("2") &&
                            !ft_accountTxnEvent.getInternalTxnCode().equalsIgnoreCase("20"))) {

                LocalDateTime yearFromEvent =
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(ft_accountTxnEvent.getTxnDate().getTime()),
                                TimeZone.getDefault().toZoneId());

                System.out.println("year from event is: " + yearFromEvent.getYear());
                List<Double> dataFromTable = getTotalYearlyCreditforCore(ft_accountTxnEvent, yearFromEvent.getYear(), connection);
                if (!dataFromTable.isEmpty() && dataFromTable != null) {
                    double exchangeRate = getExchangeRate(connection);


                        updateTableForCore(convertTotalYearlyCredit(exchangeRate,
                                dataFromTable.get(0), ft_accountTxnEvent),
                                dataFromTable.get(1),
                                ft_accountTxnEvent, yearFromEvent.getYear(), connection);

                    return;
                } else {
                    insert(ft_accountTxnEvent, yearFromEvent.getYear(), connection);
                    return;
                }
            }

            if (ft_accountTxnEvent.getCxCustId() != null && !ft_accountTxnEvent.getCxCustId().isEmpty()
                    && ((ft_accountTxnEvent.getInternalTxnCode().equalsIgnoreCase("97")) ||
                    ft_accountTxnEvent.getInternalTxnCode().equalsIgnoreCase("2"))) {

                if (ft_accountTxnEvent.getAcctType().equalsIgnoreCase("SBA") ||
                        ft_accountTxnEvent.getAcctType().equalsIgnoreCase("CAA")) {
                    LocalDateTime yearFromEvent =
                            LocalDateTime.ofInstant(Instant.ofEpochMilli(ft_accountTxnEvent.getTxnDate().getTime()),
                                    TimeZone.getDefault().toZoneId());

                    List<Double> dataFromTable = getTotalYearlyCreditforCore(ft_accountTxnEvent, yearFromEvent.getYear(), connection);
                    if (!dataFromTable.isEmpty() && dataFromTable != null) {
                        double exchangeRate = getExchangeRate(connection);

                            updateTableForCore(convertTotalYearlyCredit(exchangeRate, dataFromTable.get(0), ft_accountTxnEvent),
                                    convertTotalYearlyInterestCredit(exchangeRate, dataFromTable.get(1), ft_accountTxnEvent),
                                    ft_accountTxnEvent, yearFromEvent.getYear(), connection);

                        return;
                    } else {
                        insert(ft_accountTxnEvent, yearFromEvent.getYear(), connection);
                        return;
                    }
                }
            }

            if (ft_accountTxnEvent.getCxCustId() != null && !ft_accountTxnEvent.getCxCustId().isEmpty()
                    && ((ft_accountTxnEvent.getInternalTxnCode().equalsIgnoreCase("20")))) {

                if (ft_accountTxnEvent.getAcctType().equalsIgnoreCase("TDA")) {
                    LocalDateTime yearFromEvent =
                            LocalDateTime.ofInstant(Instant.ofEpochMilli(ft_accountTxnEvent.getTxnDate().getTime()),
                                    TimeZone.getDefault().toZoneId());

                    List<Double> dataFromTable = getTotalYearlyCreditforCore(ft_accountTxnEvent, yearFromEvent.getYear(), connection);
                    if (!dataFromTable.isEmpty() && dataFromTable != null) {
                        double exchangeRate = getExchangeRate(connection);

                            updateTableForCore(convertTotalYearlyCredit(exchangeRate, dataFromTable.get(0), ft_accountTxnEvent),
                                    convertTotalYearlyInterestCredit(exchangeRate, dataFromTable.get(1), ft_accountTxnEvent),
                                    ft_accountTxnEvent, yearFromEvent.getYear(), connection);

                        return;
                    } else {
                        insert(ft_accountTxnEvent, yearFromEvent.getYear(), connection);
                        return;
                    }
                }
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        } catch (ArithmeticException e) {
            System.out.println("divide by zero error...");
        } catch (SQLException sq){
            sq.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static double convertTotalYearlyCredit(double exchangeRate, double realAmount, FT_AccountTxnEvent ft_accountTxnEvent) throws ArithmeticException{
        double value= 0.0;
            value = (ft_accountTxnEvent.getTxnAmt() / exchangeRate) + realAmount;
            if (String.valueOf(value).equalsIgnoreCase("Infinity") || value == 0.0
                    || String.valueOf(value).isEmpty() || exchangeRate == 0.0 ||
                    String.valueOf(exchangeRate).isEmpty()) {
                value = realAmount + ft_accountTxnEvent.getTxnAmt();
                return value;
            } else {
                return value;
            }
    }

    public static double convertTotalYearlyInterestCredit(double exchangeRate, double realAmount, FT_AccountTxnEvent ft_accountTxnEvent) throws ArithmeticException {
        double value = 0.0;
        value = (ft_accountTxnEvent.getTxnAmt() / exchangeRate) + realAmount;
        if (String.valueOf(value).equalsIgnoreCase("Infinity") || value == 0.0
                || String.valueOf(value).isEmpty() || exchangeRate == 0.0 ||
                String.valueOf(exchangeRate).isEmpty()) {
            value = realAmount + ft_accountTxnEvent.getTxnAmt();
            return value;
        } else {
            return value;
        }
    }

    public static List<Double> getTotalYearlyCreditforCore(FT_AccountTxnEvent ft_accountTxnEvent,
                                                           int year, CxConnection connection) {
        /*CxConnection connection = CustomDerivator.getConnection();*/
        PreparedStatement ps = null;
        ResultSet rs = null;

        String sql = "select totalYearlyCredit, totalYearlyInterestCredit from CUSTOM_FATCA_BOOTSTRAP " +
                "WHERE CUSTID = '"
                + ft_accountTxnEvent.getCxCustId() + "' and YEAR = " + year;

        List<Double> list = new ArrayList<>();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(0, rs.getDouble("totalYearlyCredit"));
                list.add(1, rs.getDouble("totalYearlyInterestCredit"));
            }
        } catch (NullPointerException npe) {
            System.out.println("data not available...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void insert(FT_AccountTxnEvent ft_accountTxnEvent, int year, CxConnection connection) {
        /*CxConnection connection = CustomDerivator.getConnection();*/
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO dbo.CUSTOM_FATCA_BOOTSTRAP (CUSTID, " +
                    "[YEAR], TotalAccountBalance, TotalYearlyCredit, TotalYearlyInterestCredit)" +
                    "VALUES (?, ?, ?, ?, ?);");
            int i = 1;
            ps.setString(i++, ft_accountTxnEvent.getCxCustId());
            ps.setInt(i++, year);
            ps.setDouble(i++, 0.0);
            ps.setDouble(i++, ft_accountTxnEvent.getTxnAmt());
            ps.setDouble(i++, ft_accountTxnEvent.getTxnAmt());
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static double getExchangeRate(CxConnection connection) throws SQLException, NullPointerException {

        /*CxConnection connection = CustomDerivator.getConnection();*/
        PreparedStatement ps = null;
        ResultSet rs = null;

        double exchangeRate = 0.0;
        String sql = "select GBBKXR from dbo.EXCHANGE_RATE where GCPET = 'USD'";
        ps = connection.prepareStatement(sql);
        rs = ps.executeQuery();
        while (rs.next()) {
            exchangeRate = rs.getDouble("GBBKXR");
        }
        return exchangeRate;
    }

    public static void updateTableForCore(double totalYearlyCredit, double totalYearlyInterestCredit, FT_AccountTxnEvent ft_accountTxnEvent, int yearFromEvent,
                                          CxConnection connection) {
        System.out.println("totalYearlyCredit -> " + totalYearlyCredit + " totalYearlyInterestCredit -> " +
                " custid -> " + ft_accountTxnEvent.getCxCustId() + " year -> " + yearFromEvent);
        /*CxConnection connection = CustomDerivator.getConnection();*/
        PreparedStatement ps = null;
        ResultSet rs = null;

        String sql = "UPDATE CUSTOM_FATCA_BOOTSTRAP set totalYearlyCredit = ?, totalYearlyInterestCredit = ?" +
                ", TotalAccountBalance = ? where CUSTID = ? and YEAR = ?";

        try {
            ps = connection.prepareStatement(sql);

            int i = 1;
            ps.setDouble(i++, totalYearlyCredit);
            ps.setDouble(i++, totalYearlyInterestCredit);
            ps.setDouble(i++, 0.0);
            ps.setString(i++, ft_accountTxnEvent.getCxCustId());
            ps.setInt(i++, yearFromEvent);
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
