/* caseTree.js start */
var mstrData = '[{"text":"Info","id":"' + custRmlId + '","icon":"/cdn/efm/common/images/jstree/root.png","type":"demo","state":{"opened":true},"metadata":{"nodeData":{},"editable":false},"children":[{"text":"NIC","id":"nic","parent":"nicRelam","icon":"/cdn/efm/io/images/root.jpg","type":"folder","state":{"opened":true},"metadata":{"nodeData":{},"editable":false}' + nicTab + '},{"text":"CustomerID","id":"customer","parent":"customerRelam","icon":"/cdn/efm/io/images/root.jpg","type":"folder","state":{"opened":true},"metadata":{"nodeData":{},"editable":false}' + customerTab + '},{"text":"AccountID","id":"account","parent":"customerRelam","icon":"/cdn/efm/io/images/root.jpg","type":"folder","state":{"opened":true},"metadata":{"nodeData":{},"editable":false}' + accountTab + '},{"text":"EmployeeID","id":"employee","parent":"employeeRelam","icon":"/cdn/efm/io/images/root.jpg","type":"folder","state":{"opened":true},"metadata":{"nodeData":{},"editable":false}' + employeeTab + '}]}]';
JSON.parse(mstrData);

$(function () {
    $("#" + treeId).jstree({
        "plugins": ["themes", "ui", "types", "unique", "contextmenu"],
        "themes": {
            "name": "apple",
            "url": "themes/apple/style.css",
            "dir": "themes",
            "stripes": "true"
        },
        "ui": {
            "initially_select": ["customerRelam3"]
        },
        "types": {
            "default": {
                "icon": "glyphicon glyphicon-flash"
            },
            "demo": {
                "icon": "/cdn/efm/common/images/jstree/root.png"
            },
            "file": {
                "icon": "/cdn/efm/common/images/jstree/file.png"
            },
            "folder": {
                "icon": "/cdn/efm/io/images/root.jpg"
            },
            "find": {
                "icon": "../common/images/jstree/find.png"
            }
        },
        "core": {
            "html_titles": true,
            "load_open": true,
            "multiple": false,
            "check_callback": true,
            "data": getJSONDoc(mstrData)
        },
        "contextmenu": {
            "select_node": true,
            "show_at_node": true,
            items: function (o) {
                if (o.id != "customer" && o.id != "nic" && o.id != "account" && o.id != "employee" && o.id.indexOf("customerRelam") < 0
                 && o.id.indexOf("nicRelam") < 0 && o.id.indexOf("employeeRelam") < 0) {
                    return {
                        "create": {
                            "label": "View Ananlysis",
                            "action": function (data) {
                                viewLinkAnanlysis(o.id.split("#")[2]);
                            }
                        }
                    };
                } else {
                    return {};
                }
            }
        }
    }).bind("activate_node.jstree", function (e, data) {
        if (data.node.id.split("#")[1] == "C") {
            addTab('Customer', data.node.id.split("#")[2]);
        } else if (data.node.id.split("#")[1] == "A") {
            addTab('Account', data.node.id.split("#")[2]);
        }
         else if (data.node.id.split("#")[1] == "N") {
            addTab('NIC', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "D") {
            addTab('Deal', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "L") {
            addTab('Locker', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "B") {
            addTab('Bill', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "F") {
            addTab('FLease', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "T") {
            addTab('Trade', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "P") {
            addTab('Pawn', data.node.id.split("#")[2]);
        }else if (data.node.id.split("#")[1] == "S") {
            addTab('Staff', data.node.id.split("#")[2]);
        }
    });
});
/* caseTree.js end */
