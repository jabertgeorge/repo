wl {
  class: clari5.aml.wl.Wl
  refresh: 600
  indexInMem: false
  indexDir: ${CX_DATA_DIR}"/wl-lucene-index"
  jobDir: ${CX_DATA_DIR}"/wl-job"
  searchThreadCount: 4
  indexThreadCount: 4
  batchSize: 1000
  machineID: M1
  autoReindexOn: true
  reindexTimestamp: ${CX_DATA_DIR}"/reindex-timestamp.txt"
  reindexCommitSize: 400000
  indexRefreshTime: 1864000
  stopWordsFilePath: ${CX_DATA_DIR}"/wl-stop-words.txt"
  fieldsInfo: {
    #only these fields will be accepted by WL. else it should report error
    name {analyzerName: name}
    country {analyzerName: lowercase}
    country_name {analyzerName: lowercase}
    dob {analyzerName: date}
    tax_id {}
    addr1 {}
    addr2 {}
    city {}
    list {analyze: false}
    entity_type {analyze: false}
    bic {}
    phonetic {}
    passport {}
    nic {}
    id {}
    sourcedescription {}
    birthplace {}
    gender {}
    sanctionsreferences {}
    applicantname {}
    benname {}
    benbank {}
    applicantbank {}
    applicantcountry {}
    bencountry {}
    event_id {}
    vessel_name {}
    vessel_number {}
    Port {}
  }
  defaultWt: 1
  listNameField: list
  #WlListTypeEnabled : [UN,OFAC,ACCUITY,CSV,OFAC,UN]
  WlListTypeEnabled: [ACCUITY, CSV, DJ, OFAC, UN, CSV1, CSV2, CUSTOMER]
  scorer {
    # used for both indexing and searching
    algoMetaInfo {
      simHash {
        nshingle: 3
        nfeature: 40
      }
    }
    enabled: [simHash, charCount, mobile]

    # used only for searching
    # these are default values and will be over-ridden by search-config values.
    extraSearchCount: 0
    individualCutoffs {
      simHash: 0.2
      charCount: 0.5
      mobile: 0.5
    }
  }
  uploader {

    table: [{
      table_name: EVENT_FT_REMITTANCE
      mappings {
        #fieldinWl : #fieldinSource
        id: event_id
        applicantname: rem_name
        benname: ben_name
        benbank: benf_bank
        applicantbank: rem_bank
        applicantcountry: rem_cntry_code
        bencountry: ben_cntry_code
        vessel_name: vessel_name
        vessel_number: vessel_number
        Port: Port
      }
    }
    ]

    accuity {
      mappings {
        #fieldinWl : #fieldinSource
        entity_type: [entityType]
        list: [listCode]
        name: [name]
        dob: [dob]
        addr1: [address1]
        addr2: [address2]
        city: [city]
        country: [country]
        country_name: [countryName]
        bic: [bic]
      }
    }
    csv {
      rowDelim: "\n"
      colDelim: "|"
      mappings {
        #fieldinWl : #fieldinSource
        name: [name]
        dob: [dob]
        addr1: [address]
        city: [city]
        country_name: [country]
        tax_id: [tax_id]
        list: [list_code]
        passport: []
        nic: []
      }
    }
    csv1 {
      rowDelim: "\n"
      colDelim: "|"
      mappings {
        #fieldinWl : #fieldinSource
        id: [id]
        name: [custName]
        passport: [custPassportNo]
        nic: [nationalId]
        list: [list_code]
      }
    }
    csv2 {
      rowDelim: "\n"
      colDelim: "|"
      mappings {
        #fieldinWl : #fieldinSource
        name: [name]
        dob: [dob]
        addr1: [address]
        city: [city]
        country_name: [country]
        tax_id: [tax_id]
        list: [list_code]
      }
    }
    ofac {
      mappings {
        name: [firstName, lastName]
        dob: [dateOfBirth]
        addr1: [address1]
        addr2: [address2, address3]
        country: [country]
        country_name: [country]
        city: [city]
      }
    }
    un {
      mappings {
        name: [FIRST_NAME, SECOND_NAME, THIRD_NAME, FOURTH_NAME]
        dob: [DATE]
        addr1: [STREET]
        addr2: []
        country: [COUNTRY]
        country_name: [COUNTRY]
        city: [CITY]

      }
    }

    dj {
      # specify which version of dj you are using, values can be 'old|new'
      version: old

      mappings {
        # we can index CITIZENSHIP_XXXX, RESIDENT_OF_XXX, COUNTRY_OF_AFFILIATION_XXX, ENHANCED_RISK_COUNTRY_XXX too if put in configuration..
        name: [name]
        applicantname: [name]
        benname: [name]
        country: [country]
        country_name: [country_name]
        dob: [date, dob]
        addr1: [addrline]
        addr2: [addr1]
        city: [city]
        sourcedescription: [sourcedescription]
        birthplace: [birthplace]
        description: [description]
        gender: [gender]
        bic: [bic]
        sanctionsreferences: [sanctionsreferences]
      }
    }
  }

  searcher {
    customer {
      mappings {
        #fieldinCustomer : #fieldInWL
        name: name
        dob: dob
        nationality: country_name
        resAddr: addr1
        resAddrCity: city
        pan: tax_id
        bic: bic
        passport: passport
        nic: nic
        ApplicantName: applicantname
        BenificiaryName: benname
        BenificiaryBank: benbank
        ApplicantBank: applicantbank
        ApplicantCountry: applicantcountry
        BenificiaryCountry: bencountry
        vessel_name: vessel_name
        vessel_number: vessel_number
        Port: Port
      }
    }
    default {
      mappings {
        #fieldinCustomer : #fieldInWL
        name: name
        dob: dob
        nationality: country_name
        resAddr: addr1
        resAddrCity: city
        pan: tax_id
        bic: bic
      }
    }
    beneficiary {
      mappings {
        #fieldinEvent : #fieldInWL
        BEN_NAME: name
        BEN_ADD1: addr1
        BEN_ADD2: addr2
        BEN_CNTRY_CODE: country
        BEN_BIC: bic
      }
    }
    remitter {
      mappings {
        #fieldinEvent : #fieldInWL
        REM_NAME: name
        REM_ADD1: addr1
        REM_ADD2: addr2
        REM_CNTRY_CODE: country
        REM_BIC: bic
      }
    }
  }
  csvBatchSearch {
    batchSize: 5000
    inPath: ${CX_DATA_DIR}"/wl-batch-search-in-path"
    outPath: ${CX_DATA_DIR}"/wl-batch-search-out-path"
    errPath: ${CX_DATA_DIR}"/wl-batch-search-err-path"
    rowDelim: "\n"
    colDelim: "|"
  }
}
