package clari5.kyc.custom.rest.impl;

import clari5.hfdb.AmlCustomerView;
import clari5.hfdb.CrcRisk;
import clari5.hfdb.CrcRiskHist;
import clari5.kyc.CustFatca;
import clari5.kyc.CustFatcaDetails;
import clari5.kyc.CustKycCngHis;
import clari5.kyc.CustMakerTbl;
import clari5.kyc.custom.KycException;
import clari5.kyc.custom.rest.KycServices;
import clari5.kyc.mappers.CustMakerTblMapper;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.*;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.rdbms.Rdbms;
import org.json.JSONArray;
import org.json.JSONObject;
import clari5.kyc.custom.pojo.ChangesJson;
import clari5.kyc.custom.pojo.CustFatcaUserDetails;
import clari5.uac.helpers.ServiceAuth;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import clari5.uac.constants.UserAccessType;
import clari5.uac.exceptions.AuthException;
import clari5.uac.uow.login.LoginInfo;

import javax.ws.rs.core.SecurityContext;
import clari5.platform.logger.CxpsLogger;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class KycServiceImpl implements KycServices {

    @Context
    private UriInfo uriInfo;
    private static CxpsLogger logger = CxpsLogger.getLogger(KycServiceImpl.class);
    private static final String CAPABILITY = "EFM/AML/KYC";
    @Override
    public String initializeKyc(SecurityContext ctx) throws KycException {
        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null) {
            logger.error("Kyc Exception: Unable to acquire session, Please try again after some time");
            throw new KycException("Unable to acquire session, Please try again after some time");
        }

        try (RDBMSSession session = Rdbms.getAppSession()) {

            String userId = ctx.getUserPrincipal().getName();

            LoginInfo login = null;
            try{
                login = ServiceAuth.validateUserContext(ctx, CAPABILITY, uriInfo.getPath(), UserAccessType.CHECKER_ACCESS);
            }catch (Exception e){
                System.out.println("No checker access");
            }


            HashMap delta = new HashMap();
            List deltaCreatedLists = new ArrayList();
            CustMakerTblMapper makerTbl = session.getMapper(CustMakerTblMapper.class);
            List deltaList = makerTbl.getAllRecords();
            int index = 0;

            while (deltaList.size() > index) {
                HashMap deltaDetails = new HashMap();
                CustMakerTbl custMakerTbl = (CustMakerTbl) deltaList.get(index);

                if (custMakerTbl != null) {
                    deltaDetails.put("custId", custMakerTbl.getCustId());
                    deltaDetails.put("updateType", custMakerTbl.getUpdateType());
                    switch (custMakerTbl.getUpdateType()) {
                        case "riskLevel":

                            JSONObject changeJson = new JSONObject(custMakerTbl.getUpdates());
                            deltaDetails.put("newDetails", changeJson.optString("riskLevel"));

                            CrcRisk crcRisk = new CrcRisk(session);
                            crcRisk.setCxKey(custMakerTbl.getCustId());
                            crcRisk = crcRisk.select();

                            if (crcRisk != null) {
                                deltaDetails.put("oldDetails", crcRisk.getRiskLevel());
                            }
                            break;

                        case "fatca":
                            CustFatca custFatca = new CustFatca(session);
                            try {
                                deltaDetails.put("newDetails", custMakerTbl.getUpdates());

                                //old Json
                                CustFatca custFatcaOld = new CustFatca(session);
                                custFatcaOld.setCustId(custMakerTbl.getCustId());
                                custFatcaOld = custFatcaOld.select();

                                if(custFatcaOld!=null && custFatcaOld.getClassification()!=null) {
                                    //list of users
                                    ArrayList<CustFatcaUserDetails> custFatcaUserDetailsOld = selectUserProfiles(custMakerTbl.getCustId(), session);

                                    ChangesJson oldChangesJson = new ChangesJson();
                                    oldChangesJson.setCustId(custMakerTbl.getCustId());
                                    oldChangesJson.setClassification(custFatcaOld.getClassification());
                                    oldChangesJson.setUsPerson(custFatcaOld.getUsPerson());
                                    oldChangesJson.setNpfi(custFatcaOld.getNpfi());
                                    oldChangesJson.setW8Available(custFatcaOld.getW8Available());
                                    oldChangesJson.setExpiryDate(custFatcaOld.getExpiryDate());
                                    oldChangesJson.setAccountRelatedUsers(custFatcaUserDetailsOld);

                                    deltaDetails.put("oldDetails", oldChangesJson);
                                }
                                else
                                {
                                    deltaDetails.put("oldDetails", null);
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage());
                            }
                            break;
                        case "userProfile":
                            deltaDetails.put("newDetails", (new JSONObject(custMakerTbl.getUpdates())).optString("bankRelatedFamily"));

                            AmlCustomerView cust = new AmlCustomerView(session);
                            cust.setCustId(custMakerTbl.getCustId());
                            cust = cust.select();

                            if (cust != null) {
                                deltaDetails.put("oldDetails", cust.getBankRelatedFamily());
                            }
                            break;
                    }
                    if (login != null && login.hasAccess(session.getCxConnection(), CAPABILITY, UserAccessType.CHECKER_ACCESS) && !custMakerTbl.getUserId().equalsIgnoreCase(userId) ) {
                        deltaDetails.put("canVerify", true);
                    } else {
                        deltaDetails.put("canVerify", false);
                    }
                    deltaCreatedLists.add(deltaDetails);
                }
                index++;
            }

            delta.put("delta", deltaCreatedLists);
            return new JSONObject(delta).toString();

        } catch (RdbmsException|SQLException e) {
            logger.error(e.getMessage());
            throw new KycException("Exception while communicating to DB, Please try again after some time.");
        }
    }

    @Override
    public String getCustomerDetails(String custId,SecurityContext ctx) throws KycException,AuthException {

        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");

        LoginInfo login = null;
        try{
            login = ServiceAuth.validateUserContext(ctx, CAPABILITY, uriInfo.getPath(), UserAccessType.MAKER_ACCESS);
        }catch (Exception e){
            login = null;
        }

        try (RDBMSSession session = Rdbms.getAppSession()) {
            HashMap custDetails = new HashMap();


            AmlCustomerView cust = new AmlCustomerView(session);
            cust.setCustId(custId);
            cust = cust.select();

            if (cust != null) {

                custDetails.put("custDetails", cust);

                CustFatca custFatca = new CustFatca(session);
                custFatca.setCustId(custId);
                custFatca = custFatca.select();
                if (custFatca != null) {

                    ArrayList<CustFatcaUserDetails> custFatcaUserDetailsList = selectUserProfiles(custId, session);

                    ChangesJson changesJson = new ChangesJson();
                    changesJson.setCustId(custId);
                    changesJson.setClassification(custFatca.getClassification());
                    changesJson.setUsPerson(custFatca.getUsPerson());
                    changesJson.setNpfi(custFatca.getNpfi());
                    changesJson.setW8Available(custFatca.getW8Available());
                    changesJson.setExpiryDate(custFatca.getExpiryDate());
                    changesJson.setAccountRelatedUsers(custFatcaUserDetailsList);
                    custDetails.put("fatcaDetails", changesJson);
                }

                CrcRisk crcRisk = new CrcRisk(session);
                crcRisk.setCxKey(custId);
                crcRisk = crcRisk.select();

                if (crcRisk != null)
                    custDetails.put("riskLevel", crcRisk.getRiskLevel());

                if(login != null && login.hasAccess(session.getCxConnection(), CAPABILITY, UserAccessType.MAKER_ACCESS))
                    custDetails.put("canEdit", true);
                else
                    custDetails.put("canEdit", false);

                return new JSONObject(custDetails).toString();
            }
            throw new KycException("Invalid customer ID !!");
        } catch (RdbmsException|SQLException e) {
            logger.error(e.getMessage());
            throw new KycException("Exception while communicating to DB, Please try again after some time.");
        }
    }

    @Override
    public String updateDetails(String custDetails, SecurityContext ctx) throws KycException, Exception {
        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");
        try (RDBMSSession session = Rdbms.getAppSession()) {
            String userId = ctx.getUserPrincipal().getName();
            JSONObject changeJson = new JSONObject(custDetails);
            String custId = changeJson.optString("custId");

            int index = 0;

            JSONArray changes = changeJson.getJSONArray("changes");
            while (changes.length() > index) {
                JSONObject update = changes.optJSONObject(index).optJSONObject("updates");
                String updateType = changes.optJSONObject(index).optString("updateType");
                CustMakerTbl custMakerTbl = new CustMakerTbl(session);

                custMakerTbl.setCustId(custId);
                custMakerTbl.setUpdateType(updateType);
                custMakerTbl.setUpdates(update.toString());
                custMakerTbl.setTimestamp(new Timestamp(System.currentTimeMillis()));
                custMakerTbl.setUserId(userId);

                custMakerTbl.from(custDetails);
                if (custMakerTbl.insert() != 1) {
                    throw (new KycException("Update request already present.Please verify update request and try again. !!"));
                }
                index++;
            }


            session.commit();
            return "Success";
        } catch (RdbmsException e) {
            logger.error(e.getMessage());
            throw (new KycException("Failed to process your update request, please try again after some time."));
        }

    }

    @Override
    public String verifyKycUpdate(String kycUpdateDetails, SecurityContext ctx) throws KycException, Exception {
        RDBMS rdbms = (RDBMS) Clari5.getResource("rdbms");

        if (rdbms == null)
            throw new KycException("Unable to acquire session, Please try again after some time");

        try (RDBMSSession session = Rdbms.getAppSession()) {
            String userId = ctx.getUserPrincipal().getName();
            JSONObject jsonObject = new JSONObject(kycUpdateDetails);
            String custId = jsonObject.optString("custId");
            String updateType = jsonObject.optString("updateType");
            String isAccepted = jsonObject.optString("isAccepted");
            String remark = jsonObject.optString("remark");

            CustMakerTbl custMakerTbl = new CustMakerTbl(session);
            custMakerTbl.setCustId(custId);
            custMakerTbl.setUpdateType(updateType);

            custMakerTbl = custMakerTbl.select();

            if (custMakerTbl.getUserId().equalsIgnoreCase(userId)) {
                throw new KycException("User Doesn't have authority to verify.");
            }

            if (isAccepted.equalsIgnoreCase("yes")) {

                switch (updateType) {
                    case "riskLevel":
                        updateCustomerRiskLevel(custId, custMakerTbl.getUpdates(), userId, session);
                        break;

                    case "fatca":
                        updateFatcaDetails(custMakerTbl.getUpdates(), custId, session);
                        break;

                    case "userProfile":
                        updateUserProfile(custId, custMakerTbl.getUpdates(), session);
                        break;
                }
            }
            CustKycCngHis custKycCngHis = new CustKycCngHis(session);

            custKycCngHis.setCreatedDate(custMakerTbl.getTimestamp());
            custKycCngHis.setVerifyDate(new Timestamp(System.currentTimeMillis()));
            custKycCngHis.setCustId(custId);
            custKycCngHis.setUpdateType(updateType);
            custKycCngHis.setUpdates(custMakerTbl.getUpdates());
            custKycCngHis.setCreatedUserId(custMakerTbl.getUserId());
            custKycCngHis.setVerifierUserId(userId);
            custKycCngHis.setIsAccepted(isAccepted);
            custKycCngHis.setRemark(remark);

            if (custKycCngHis.insert() == 1) {
                custMakerTbl.delete();
            }

            session.commit();
            return "Success";
        } catch (RdbmsException e) {
            logger.error(e.getMessage());
            throw new KycException("Failed to verify the changes. Please try again after some time.");
        }
    }


    private String updateFatcaDetails(String fatcaDetails, String custID, RDBMSSession session) throws KycException, Exception {
        if (session != null) {

            CustFatca custFatca = new CustFatca(session);
            try {
                custFatca.from(fatcaDetails);
                custFatca.setCustId(custID);
                if (custFatca.upsert() != 1) {
                    throw new KycException("Failed to verify the changes. Please try again after some time.");
                }

                logger.debug("inserted into cust Facta");
                JSONArray fatcaUserDetails = new JSONObject(fatcaDetails).getJSONArray("accountRelatedUsers");

                if (!deleteUserProfile(custID, session)) {
                    throw new KycException("Failed to verify the changes.Please try again after some time ");
                }
                logger.debug("deleted users");

                int i = 1;
                while (fatcaUserDetails.length() != 0) {
                    CustFatcaDetails fatcaUserDetail = new CustFatcaDetails(session);
                    JSONObject details = (JSONObject) fatcaUserDetails.remove(0);
                    logger.debug("User details = " + details.toString());

                    fatcaUserDetail.from(details.toString());
                    fatcaUserDetail.setCustId(custID);

                    fatcaUserDetail.setDdlId(custID + "_user_" + fatcaUserDetail.getUdType());

                    if (fatcaUserDetail == null && fatcaUserDetail.getUdType() == null) {
                        throw new KycException("Failed to verify the chnages. Please try again after some time. - " + fatcaUserDetail.getUdType());
                    }

                    if (fatcaUserDetail.getUdType().equalsIgnoreCase("Ow")) {
                        fatcaUserDetail.setDdlId(fatcaUserDetail.getDdlId() + "_" + i);
                        i++;
                    }

                    if (fatcaUserDetail.insert() != 1) {
                        throw new KycException("Failed to verify the chnages. Please try again after some time.");
                    }
                    logger.debug("inserted users");
                }
                session.commit();
                return "Success";
            } catch (RdbmsException e) {
                logger.error(e.getMessage());
                throw new KycException("Exception while processing your request. Please contact support staff.");
            }
        }
        throw new KycException("Unable to acquire session, Please try again after some time");
    }

    private String updateCustomerRiskLevel(String custId, String details, String userId, RDBMSSession session) throws KycException, Exception {
        if (session != null) {

            try {
                CrcRisk crcRisk = new CrcRisk(session);
                crcRisk.setCxKey(custId);
                crcRisk = crcRisk.select();

                JSONObject jsonObject = new JSONObject(details);

                if (crcRisk != null) {
                    CrcRiskHist crcRiskHist = new CrcRiskHist(session);
                    crcRiskHist.setCxKey(custId);
                    //crcRiskHist.from(crcRisk.toJson());
                    crcRiskHist.setCreationTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                    crcRiskHist.setFactName(crcRisk.getFactName());
                    crcRiskHist.setDateTime(crcRisk.getDateTime());
                    crcRiskHist.setSource(crcRisk.getSource());
                    crcRiskHist.setRiskLevel(crcRisk.getRiskLevel());
                    crcRiskHist.setModuleId(crcRisk.getModuleId());
                    crcRiskHist.setScore(crcRisk.getScore());
                    crcRiskHist.setEvidence(crcRisk.getEvidence());
                    crcRiskHist.setPcFlag(crcRisk.getPcFlag());
                    crcRiskHist.setMigratedFlag(crcRisk.getMigratedFlag());
                    crcRiskHist.setUserId(crcRisk.getUserId());


                    if (crcRiskHist.insert() == 1) {
                        crcRisk.setDateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                        crcRisk.setSource("user");
                        crcRisk.setUserId("userId");
                        crcRisk.setRiskLevel(jsonObject.optString("riskLevel"));

                        if (crcRisk.update() != 1) {
                            throw (new KycException("Failed to verify the changes. Please try again after some time."));
                        }
                    }
                } else {
                    crcRisk = new CrcRisk();
                    crcRisk.setCxKey(custId);
                    crcRisk.setDateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                    crcRisk.setSource("user");
                    crcRisk.setUserId("userId");
                    crcRisk.setRiskLevel(jsonObject.optString("riskLevel"));
                    if (crcRisk.insert() != 1) {
                        throw (new KycException("Failed to verify the changes. Please try again after some time."));
                    }
                }

                session.commit();
                return "Success";
            } catch (RdbmsException e) {
                logger.error(e.getMessage());
                throw new KycException("Exception while processing your request. Please contact support staff.");
            }
        }
        throw new KycException("Unable to acquire session, Please try again after some time");
    }

    public String updateUserProfile(String custId, String details, RDBMSSession session) throws KycException {
        try {
            AmlCustomerView cust = new AmlCustomerView(session);
            cust.setCustId(custId);
            cust = cust.select();

            if (cust == null)
                throw new KycException("User not found !!");

            JSONObject jsonObject = new JSONObject(details);

            CxConnection con = session.getCxConnection();
            String query = null;

            switch (con.getDbType()) {
                case SQLSERVER:
                    query = "update customer set bankRelatedFamily = ?  where cxCifID = ?";
                    break;
                case ORACLE:
                    query = "update customer set \"bankRelatedFamily\" = ? where \"cxCifID\" = ?";
                    break;
                default:
                    query = "update customer set bankRelatedFamily = ?  where cxCifID = ?";
            }
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, jsonObject.optString("bankRelatedFamily"));
                ps.setString(2, custId);

                ps.executeUpdate();
                con.commit();

                return "Success";

            } catch (Exception e) {
                logger.error(e.getMessage());
                session.getCxConnection().rollback();
                throw new KycException("Exception while processing your request. Please contact support staff.");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new KycException("Exception while processing your request. Please contact support staff.");
        }
    }

    public boolean deleteUserProfile(String custId, RDBMSSession session) throws KycException {
        try{
            if (custId == null)
                throw new KycException("Customer Id can not be null !!");

            String query = null;

            CxConnection con = session.getCxConnection();
            switch (con.getDbType()) {
                case SQLSERVER:
                    query = "delete from CUST_FATCA_DETAILS where cust_id = ?";
                    break;
                case ORACLE:
                    query = "delete from CUST_FATCA_DETAILS where cust_id = ?";
                    break;
                default:
                    query = "delete from CUST_FATCA_DETAILS where cust_id = ?";

            }
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, custId);

                ps.executeUpdate();
                con.commit();

                return true;

            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new KycException("Exception while processing your request. Please contact support staff.");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new KycException("Exception while processing your request. Please contact support staff.");
        }

    }

    public ArrayList<CustFatcaUserDetails> selectUserProfiles(String custId, RDBMSSession session) throws KycException {
        try{
            if (custId == null)
                throw new KycException("Customer Id can not be null !!");

            CxConnection con = session.getCxConnection();
            String query = null;
            ResultSet rs;
            switch (con.getDbType()) {
                case SQLSERVER:
                    query = "select * from CUST_FATCA_DETAILS where cust_id = ?";
                    break;
                case ORACLE:
                    query = "select * from CUST_FATCA_DETAILS where cust_id = ?";
                    break;
                default:
                    query = "select * from CUST_FATCA_DETAILS where cust_id = ?";
            }
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, custId);
                rs = ps.executeQuery();

                ArrayList<CustFatcaUserDetails> custFatcaUserDetails = CustFatcaUserDetails.getUsers(rs);
                rs.close();
                return custFatcaUserDetails;

            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new KycException("Exception while processing your request. Please contact support staff.");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new KycException("Exception while processing your request. Please contact support staff.");
        }

    }
}
