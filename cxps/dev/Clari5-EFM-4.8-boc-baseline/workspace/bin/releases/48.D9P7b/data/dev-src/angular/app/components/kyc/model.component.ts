/**
 * Created by Hemanshu on 16/04/2018.
 */



export class CustomerDetails {
    public custType: string = "";
    public name: string = "";
    public dob: string = "";
    public gender: string = "";
    public address: string = "";
    public NIC: number;
    public customerBranch: string = "";
    public citizenship: string = "";
    public homeTel: number;
    public mobile1: number;
    public email: string = "";
    public riskLevel: string = "";
    public bankRelatedFamily: string = "";
}

export class FatcaDetails {
    public custId: string = "";
    public classification: string = "";     // individual(P) , entity(NP) , financial(FI)
    public usPerson: string = "";           // US , NON-US, recalcitrant
    public npfi: string = "";               // YES, NO
    public w8Available: string = "";        // YES, NO
    public expiryDate: string = "";
    public accountRelatedUsers: Array<AccountRelatedUser> = [];
}

export class AccountRelatedUser {
    public name: string = "";
    public address: string = "";
    public city: string = "";
    public state: string = "";
    public country: string = "";
    public pcode: string = "";
    public tin: string = "";
    public fatcaDeclaration: string = "";
    public udType: string = "";
}


export class UpdatesJson {
    custId: string = "";
    changes: Array<update> = [];
}

export class update{
    updateType: string = "";
    updates: any;
}

export class VerifyRequest {
    custId: string = "";
    updateType: string = "";
    isAccepted: string = "";
    remarks: string = ""
}

/**
 *====================================================================================================================== 
 */

/**
export class CustomerDetails {
    custType: string = "";
    name: string = "";
    dob: string = "";
    gender: string = "";
    address: string = "";
    NIC: number;
    customerBranch: string = "";
    citizenship: string = "";
    homeTel: number;
    mobile1: number;
    email: string = "";
    riskLevel: string = "";
    bankRelatedFamily: string = "";

}

export class FatcaDetails {
    address: string;
    city: string;
    state: string;
    country: string;
    pin: string;
    tin: string;
    declaration: string;
}

export class FatcaNonUSDetails {
    availability: string = "";
    expiryDate: string = "";
}





export class custFatcaDetails {
    custId: string = "";
    classification: string ="";                       //individual , entity , financial
    usPerson: string ="";                             // US , NON-US, recalcitrant
    npfi: string ="";  //YES, NO
    acctHolderAddress: string ="";
    acctHolderCity: string ="";
    acctHolderState: string ="";
    acctHolderCountry: string ="";
    acctHolderPcode: string ="";
    acctHolderTin: string ="";
    ahfd: string ="";
    ownerName: string ="";
    ownerAddress: string ="";
    ownerCity: string ="";
    ownerState: string ="";
    ownerCountry: string ="";
    ownerPcode: string ="";
    ownerTin: string ="";
    ofd: string ="";
    w8Available : string ="";                               //YES, NO
    expiryDate: string ="";
}

export class DiffJson {
    custId: string = "";
    changes: Array<any> =[];
}

export class FatcaDiff{
    updateType : string = "fatca";
    updates : any;
}

export class riskLevelDiff{
    updateType : string = "riskLevel";
    updates : any;
}

export class customerDiff{
    updateType : string = "userProfile";
    updates : any;
}

export class VerifyChanges{
    custId : string= "";
    updateType : string = "";
    isAccepted : string = "";
    remarks : string = ""
}
*/