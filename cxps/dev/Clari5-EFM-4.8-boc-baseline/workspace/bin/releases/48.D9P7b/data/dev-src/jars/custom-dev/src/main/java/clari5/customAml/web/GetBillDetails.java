package clari5.customAml.web;

import java.sql.*;
import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetBillDetails {
    CxpsLogger logger = CxpsLogger.getLogger(GetBillDetails.class);

    private static String billNum="";

    GetBillDetails(String payload){
        this.billNum="";
        JSONObject jsonObject = new JSONObject(payload);
        String bNum = jsonObject.getString("billNum");
        String[] parts = bNum.split("--");
        this.billNum=parts[0];
    }

    public synchronized String getBillDetails(){
        JSONObject obj = new JSONObject();
        String tradeDetails=null;
        try {
            logger.debug("getBillDetails:billNum:"+this.billNum);
            if ((this.billNum) == null) {
                logger.error("Trying to fetch bill details. billNum not found");
                return null;
            }
            tradeDetails=getDetails(this.billNum);
            logger.debug("response Bill:"+obj.toString());
            return tradeDetails;
        }catch (Exception e){
            e.printStackTrace();
        }
        return tradeDetails;
    }

    protected String getDetails(String billNum){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            String sql="select * from BILLS_MASTER where Treasury_Bill_Num=? ";
            logger.debug("query to get billDetails : "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,billNum);
            ResultSet rs = stmt.executeQuery();
            while( rs.next() ){
                obj.put("custId", rs.getString("CIFID")!= null ? rs.getString("CIFID") : "");
                obj.put("Sale_Date", rs.getString("Sale_Date")!= null ? rs.getString("Sale_Date") : "");
                obj.put("Face_Value_Crncy", rs.getString("Face_Value_Crncy")!= null ? rs.getString("Face_Value_Crncy") : "");
                obj.put("Maturity_Date", rs.getString("Maturity_Date")!= null ? rs.getString("Maturity_Date") : "");
                jarray.put(obj);
            }
            obj1.put("billDetails", jarray);
            logger.debug("getDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting bill details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }
}