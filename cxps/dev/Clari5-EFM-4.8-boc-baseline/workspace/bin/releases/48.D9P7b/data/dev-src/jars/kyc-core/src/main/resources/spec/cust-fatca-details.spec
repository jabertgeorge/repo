clari5.kyc.entity.cust-fatca-details{
    attributes = [
        { name : ddl-id, type ="string:100", key=true}
		{ name : cust-id, type ="string:100" }
		{ name : name, type ="string:200" }
		{ name : address,  type = "string:1000" }
		{ name : city,  type = "string:200"}
		{ name : state,  type = "string:200" }
		{ name : country,  type = "string:200" }
		{ name : pcode,  type = "string:50" }
		{ name : tin,  type = "string:100" }
        { name : fatca-declaration,  type = "string:50" }
        { name : ud-type, type ="string:50" }
	]
	
	indexes {
        fatca-detail-idx : [ cust-id ]
    }
  		
}
