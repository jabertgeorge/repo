package clari5.custom.boc.integration.data.bootstrap;

import clari5.custom.boc.integration.audit.AuditManager;
import clari5.custom.boc.integration.audit.LogLevel;
import clari5.custom.boc.integration.builder.MsgMetaData;
import clari5.custom.boc.integration.builder.data.Message;
import clari5.custom.boc.integration.builder.data.VarSource;
import clari5.custom.boc.integration.config.BepCon;
import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TableMap extends HashMap<String, MsgMetaData> {
    public static CxpsLogger logger = CxpsLogger.getLogger(TableMap.class);
    private static final long serialVersionUID = -8775280342165894706L;
    private static TableMap tmap;

    private TableMap() throws Exception {
        Hocon h = new Hocon();
        try {
            h.loadFromContext("tbl-json-mapping.conf");
            Set<String> tableNames = BepCon.getConfig().getTableClassMap().keySet();
            for (String tableName : tableNames) {
                Set<String> keys = h.getKeys(tableName);
                MsgMetaData msgmetas = new MsgMetaData();
                for (String key : keys) {
                    if (!key.startsWith("duplicate")) {
                        Set<String> variables = h.getKeys(tableName + "." + key);
                        Message varSources = new Message(key);
                        for (String v : variables) {
                            VarSource var = new VarSource();
                            var.setName(v);
                            Hocon f = h.get(tableName + "." + key + "." + v);
                            var.setSource(f.getString("Source"));
                            var.setColumn(f.getString("Column"));
                            var.setValue(f.getString("Value"));
                            if (var.getSource().startsWith("db")) {
                                var.setKeys(f.getStringList("Keys"));
                                var.setWhere(f.getStringList("Where"));
                            }
                            varSources.add(var);
                            //AuditManager.log("The source: " + var.toString() + " is added for event, " + key + " for tablename, " + tableName);
                            logger.info("The source: " + var.toString() + " is added for event, " + key + " for tablename, " + tableName);
                        }
                        msgmetas.put(key, varSources);
                    } /*else {
                        Set<String> dSet = h.getKeys(tableName + "." + key);
                        Map<String, List<String>> dupes = new HashMap<String, List<String>>();
                        String eventName = "";
                        for (String d : dSet) {
                            List<String> dupesList = null;
                            if (d.equals("event")) {
                                eventName = h.getString(tableName + "." + key + ".event");
                            } else {
                                dupesList = h.getStringList(tableName + "." + key + "." + d);
                                dupes.put(d, dupesList);
                            }
                        }
                        MessageMappingManager.get().addDuplicate(eventName, dupes);
                         logger.info("Duplicate source is added for event, " + key + " for tablename, " + tableName);

                    }*/
                }
                this.put(tableName, msgmetas);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            throw e1;
        } finally {
        }
    }

    public static synchronized TableMap getTableMap() throws Exception {
        if (tmap == null) {
            tmap = new TableMap();
        }
        return tmap;
    }

    public static void main(String[] args) throws Exception {
    }
}
