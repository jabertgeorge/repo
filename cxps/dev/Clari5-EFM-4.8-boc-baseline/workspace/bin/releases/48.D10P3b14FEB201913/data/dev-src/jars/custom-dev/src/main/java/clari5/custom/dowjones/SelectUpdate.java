package clari5.custom.dowjones;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class SelectUpdate {

    static List<String> fieldList = new ArrayList<>();
    //static Set<String> eventKeySet = new LinkedHashSet<>();
    static Hocon query;
    static {
        query = new Hocon();
        query.loadFromContext("dowjonesquery.conf");
        fieldList.addAll(Arrays.asList(query.getString("dowjones.fieldList").trim().split(",")));
        //eventKeySet.addAll(Arrays.asList(query.getString("dowjones.getKey").trim().split(",")));
    }

    public static Map<String, String> selectdata(CxConnection connection){

        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, String> dataMap = new HashMap<>();

        try {
            int i = 1;
            ps = connection.prepareStatement(query.getString("dowjones.select"));
            ps.setString(i++, query.getString("dowjones.flag"));
            rs = ps.executeQuery();
            while (rs.next()){
                for (String datFieldName : fieldList){
                    dataMap.put(datFieldName, rs.getString(datFieldName) != null ? rs.getString(datFieldName) : "");
                }
            }
            return dataMap;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dataMap;
    }

    public static void updateRemTable(CxConnection connection, Map<String, String> selectMap){
        PreparedStatement ps= null;

        try {
            ps = connection.prepareStatement(query.getString("dowjones.update"));
            int i = 1;
            ps.setString(i++, query.getString("dowjones.updateFlag"));
            ps.setString(i++, selectMap.get(query.getString("dowjones.updateKey")));
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
