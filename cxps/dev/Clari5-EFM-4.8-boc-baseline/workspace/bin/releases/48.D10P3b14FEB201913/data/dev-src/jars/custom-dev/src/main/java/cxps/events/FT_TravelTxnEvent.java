// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_TRAVELTXN", Schema="rice")
public class FT_TravelTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String keys;
       @Field(size=20) public String channel;
       @Field(size=100) public String tranId;
       @Field(size=20) public String eventTime;
       @Field(size=20) public String onlineBatch;
       @Field(size=20) public String tranSubType;
       @Field(size=11) public Double lcyAmt;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=11) public Double lcyCurr;
       @Field(size=20) public String txnStatus;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String cxAcctId;
       @Field(size=11) public Double txnAmt;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=20) public String cardNo;
       @Field(size=20) public String txnCntryCode;
       @Field(size=20) public String txnCntry;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String txnCode;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String eventSubtype;
       @Field(size=20) public String sourceOrDestAcctId;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=20) public String txnCurr;
       @Field(size=20) public String custId;
       @Field(size=20) public String tranSrlNo;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String tranType;
       @Field(size=20) public String systemCountry;


    @JsonIgnore
    public ITable<FT_TravelTxnEvent> t = AEF.getITable(this);

	public FT_TravelTxnEvent(){}

    public FT_TravelTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("TravelTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setMsgSource(json.getString("source"));
            setKeys(json.getString("keys"));
            setChannel(json.getString("channel"));
            setTranId(json.getString("tran_id"));
            setEventTime(json.getString("eventts"));
            setOnlineBatch(json.getString("online-batch"));
            setTranSubType(json.getString("tran-sub-type"));
            setLcyAmt(EventHelper.toDouble(json.getString("LCY_AMT")));
            setSystemTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setLcyCurr(EventHelper.toDouble(json.getString("LCY_CURR")));
            setTxnStatus(json.getString("pstd-flg"));
            setTxnDrCr(json.getString("part-tran-type"));
            setCxAcctId(json.getString("cx-acct-id"));
            setTxnAmt(EventHelper.toDouble(json.getString("TRAN_AMT")));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setCardNo(json.getString("CardNumber"));
            setTxnCntryCode(json.getString("CountryCodeTransaction"));
            setTxnCntry(json.getString("Country_Of_transaction"));
            setAddEntityId2(json.getString("reservedfield2"));
            setTxnCode(json.getString("tran_code"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("Tran_Date")));
            setAddEntityId3(json.getString("reservedfield3"));
            setEventSubtype(json.getString("eventsubtype"));
            setSourceOrDestAcctId(json.getString("account-id"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setTxnCurr(json.getString("TRAN_CURR"));
            setCustId(json.getString("cust-id"));
            setTranSrlNo(json.getString("Part_tran_Srl_Num"));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setTranType(json.getString("tran-type"));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getMsgSource(){ return msgSource; }

    public String getKeys(){ return keys; }

    public String getChannel(){ return channel; }

    public String getTranId(){ return tranId; }

    public String getEventTime(){ return eventTime; }

    public String getOnlineBatch(){ return onlineBatch; }

    public String getTranSubType(){ return tranSubType; }

    public Double getLcyAmt(){ return lcyAmt; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public Double getLcyCurr(){ return lcyCurr; }

    public String getTxnStatus(){ return txnStatus; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getCxAcctId(){ return cxAcctId; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public String getCardNo(){ return cardNo; }

    public String getTxnCntryCode(){ return txnCntryCode; }

    public String getTxnCntry(){ return txnCntry; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getTxnCode(){ return txnCode; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getSourceOrDestAcctId(){ return sourceOrDestAcctId; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getTxnCurr(){ return txnCurr; }

    public String getCustId(){ return custId; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getTranType(){ return tranType; }

    public String getSystemCountry(){ return systemCountry; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setEventTime(String val){ this.eventTime = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setLcyAmt(Double val){ this.lcyAmt = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setLcyCurr(Double val){ this.lcyCurr = val; }
    public void setTxnStatus(String val){ this.txnStatus = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCardNo(String val){ this.cardNo = val; }
    public void setTxnCntryCode(String val){ this.txnCntryCode = val; }
    public void setTxnCntry(String val){ this.txnCntry = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setSourceOrDestAcctId(String val){ this.sourceOrDestAcctId = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setTxnCurr(String val){ this.txnCurr = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_TravelTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.cxAcctId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_TravelTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "TravelTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}