
package clari5.custom.boc.integration.data.bootstrap;

import clari5.custom.boc.integration.builder.data.VarSource;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageMappingManager {
    //Commenting it is not required as of now, Keeping coz it may need in future
   /* private static MessageMappingManager mmm = new MessageMappingManager();
    private Map<String, Map<String, List<String>>> eventDuplicatesColumns = new
            HashMap<String, Map<String, List<String>>>();

    private MessageMappingManager() {
    }

    public void addDuplicate(String event, Map<String, List<String>> duplicates) {
        eventDuplicatesColumns.put(event, duplicates);
    }

    public Map<String, List<String>> getDuplicate(String event) {
        return eventDuplicatesColumns.get(event);
    }

    public static MessageMappingManager get() {
        return mmm;
    }

    public List<JSONObject> evaluateDuplicateForColumns(JSONObject input, JSONObject jrow, String eventName) throws Exception {
        //String eventName = input.getString("event-name");
        String tableName = jrow.getString("tableName");
        Map<String, List<String>> duplicateFieldMap = eventDuplicatesColumns.get(eventName);
        List<JSONObject> dupeJsons = new ArrayList<JSONObject>();
        if (duplicateFieldMap != null) {
            for (Map.Entry<String, List<String>> e : duplicateFieldMap.entrySet()) {
                VarSource vs = TableMap.getTableMap().get(tableName).getVarSource(eventName, e.getKey());
                List<String> columnsToDupe = e.getValue();
                JSONObject o = null;
                for (String s : columnsToDupe) {
                    VarSource dupe = vs.clone();
                    dupe.setColumn(s);
                    o = new JSONObject(input, JSONObject.getNames(input));
                    // At this point only one is expected.
                    List<String> d = dupe.process(jrow);
                    if (d.size() > 1) {
                        throw new Exception("The column is configured to pick one value. If multiple columns "
                                + "should form part of multiple rows, configure accordingly. Else reevaluate keys. "
                                + jrow.toString() + " : " + input.toString());
                    } else {
                        if (d != null && d.size() > 0) {
                            o.put(e.getKey(), d.get(0));
                            dupeJsons.add(o);
                        }
                    }
                }
            }
        }
        return dupeJsons;
    }

    // As of now supporting only duplication based on one column. Will revise this logic later.
    public List<JSONObject> evaluateDuplicateForRows(Map<String, List<String>> cValues, JSONObject original) throws Exception {
        List<JSONObject> rows = new ArrayList<JSONObject>();
        String columnname = "";
        for (String key : cValues.keySet()) {
            columnname = key;
        }
        JSONObject o = null;
        List<String> values = cValues.get(columnname);
        if (values != null) {
            for (String s : values) {
                o = new JSONObject(original, JSONObject.getNames(original));
                o.put(columnname, s);
                rows.add(o);
            }
        }
        return rows;
    }*/
}