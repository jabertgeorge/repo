// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_GoldTxnEventMapper extends EventMapper<FT_GoldTxnEvent> {

public FT_GoldTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_GoldTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_GoldTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_GoldTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_GoldTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_GoldTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_GoldTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getZoneDate());
            preparedStatement.setString(i++, obj.getZoneCode());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getNic());
            preparedStatement.setTimestamp(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setTimestamp(i++, obj.getSystemTime());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getTxnId());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setDouble(i++, obj.getRefAmt());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getTxnCurr());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setDouble(i++, obj.getRefCurncy());
            preparedStatement.setString(i++, obj.getTranSrlNo());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getRemarks());
            preparedStatement.setString(i++, obj.getSystemCountry());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_GOLDTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_GoldTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_GoldTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_GOLDTXN"));
        putList = new ArrayList<>();

        for (FT_GoldTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ZONE_DATE", String.valueOf(obj.getZoneDate()));
            p = this.insert(p, "ZONE_CODE",  obj.getZoneCode());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "NIC",  obj.getNic());
            p = this.insert(p, "EVENT_TIME", String.valueOf(obj.getEventTime()));
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "SYSTEM_TIME", String.valueOf(obj.getSystemTime()));
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "TXN_ID",  obj.getTxnId());
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "REF_AMT", String.valueOf(obj.getRefAmt()));
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "TXN_CURR",  obj.getTxnCurr());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "REF_CURNCY", String.valueOf(obj.getRefCurncy()));
            p = this.insert(p, "TRAN_SRL_NO",  obj.getTranSrlNo());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "REMARKS",  obj.getRemarks());
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_GOLDTXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_GOLDTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_GOLDTXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_GoldTxnEvent obj = new FT_GoldTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setZoneDate(rs.getTimestamp("ZONE_DATE"));
    obj.setZoneCode(rs.getString("ZONE_CODE"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setNic(rs.getString("NIC"));
    obj.setEventTime(rs.getTimestamp("EVENT_TIME"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setSystemTime(rs.getTimestamp("SYSTEM_TIME"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setTxnId(rs.getString("TXN_ID"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setRefAmt(rs.getDouble("REF_AMT"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setTxnCurr(rs.getString("TXN_CURR"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setRefCurncy(rs.getDouble("REF_CURNCY"));
    obj.setTranSrlNo(rs.getString("TRAN_SRL_NO"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setRemarks(rs.getString("REMARKS"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_GOLDTXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_GoldTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_GoldTxnEvent> events;
 FT_GoldTxnEvent obj = new FT_GoldTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_GOLDTXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_GoldTxnEvent obj = new FT_GoldTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setZoneDate(EventHelper.toTimestamp(getColumnValue(rs, "ZONE_DATE")));
            obj.setZoneCode(getColumnValue(rs, "ZONE_CODE"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setNic(getColumnValue(rs, "NIC"));
            obj.setEventTime(EventHelper.toTimestamp(getColumnValue(rs, "EVENT_TIME")));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setSystemTime(EventHelper.toTimestamp(getColumnValue(rs, "SYSTEM_TIME")));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setTxnId(getColumnValue(rs, "TXN_ID"));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setRefAmt(EventHelper.toDouble(getColumnValue(rs, "REF_AMT")));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setTxnCurr(getColumnValue(rs, "TXN_CURR"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setRefCurncy(EventHelper.toDouble(getColumnValue(rs, "REF_CURNCY")));
            obj.setTranSrlNo(getColumnValue(rs, "TRAN_SRL_NO"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setRemarks(getColumnValue(rs, "REMARKS"));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_GOLDTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_GOLDTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ZONE_DATE\",\"ZONE_CODE\",\"MSG_SOURCE\",\"KEYS\",\"CHANNEL\",\"NIC\",\"EVENT_TIME\",\"TRAN_SUB_TYPE\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"CX_ACCT_ID\",\"TXN_AMT\",\"HOST_USER_ID\",\"HOST_ID\",\"ADD_ENTITY_ID2\",\"TXN_ID\",\"TXN_CODE\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"REF_AMT\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"TXN_CURR\",\"CUST_ID\",\"REF_CURNCY\",\"TRAN_SRL_NO\",\"CX_CUST_ID\",\"EVENT_NAME\",\"TRAN_TYPE\",\"REMARKS\",\"SYSTEM_COUNTRY\"" +
              " FROM EVENT_FT_GOLDTXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ZONE_DATE],[ZONE_CODE],[MSG_SOURCE],[KEYS],[CHANNEL],[NIC],[EVENT_TIME],[TRAN_SUB_TYPE],[SYSTEM_TIME],[TXN_DR_CR],[CX_ACCT_ID],[TXN_AMT],[HOST_USER_ID],[HOST_ID],[ADD_ENTITY_ID2],[TXN_ID],[TXN_CODE],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[REF_AMT],[ADD_ENTITY_ID5],[EVENT_TYPE],[TXN_CURR],[CUST_ID],[REF_CURNCY],[TRAN_SRL_NO],[CX_CUST_ID],[EVENT_NAME],[TRAN_TYPE],[REMARKS],[SYSTEM_COUNTRY]" +
              " FROM EVENT_FT_GOLDTXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ZONE_DATE`,`ZONE_CODE`,`MSG_SOURCE`,`KEYS`,`CHANNEL`,`NIC`,`EVENT_TIME`,`TRAN_SUB_TYPE`,`SYSTEM_TIME`,`TXN_DR_CR`,`CX_ACCT_ID`,`TXN_AMT`,`HOST_USER_ID`,`HOST_ID`,`ADD_ENTITY_ID2`,`TXN_ID`,`TXN_CODE`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`REF_AMT`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`TXN_CURR`,`CUST_ID`,`REF_CURNCY`,`TRAN_SRL_NO`,`CX_CUST_ID`,`EVENT_NAME`,`TRAN_TYPE`,`REMARKS`,`SYSTEM_COUNTRY`" +
              " FROM EVENT_FT_GOLDTXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_GOLDTXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ZONE_DATE\",\"ZONE_CODE\",\"MSG_SOURCE\",\"KEYS\",\"CHANNEL\",\"NIC\",\"EVENT_TIME\",\"TRAN_SUB_TYPE\",\"SYSTEM_TIME\",\"TXN_DR_CR\",\"CX_ACCT_ID\",\"TXN_AMT\",\"HOST_USER_ID\",\"HOST_ID\",\"ADD_ENTITY_ID2\",\"TXN_ID\",\"TXN_CODE\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"REF_AMT\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"TXN_CURR\",\"CUST_ID\",\"REF_CURNCY\",\"TRAN_SRL_NO\",\"CX_CUST_ID\",\"EVENT_NAME\",\"TRAN_TYPE\",\"REMARKS\",\"SYSTEM_COUNTRY\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ZONE_DATE],[ZONE_CODE],[MSG_SOURCE],[KEYS],[CHANNEL],[NIC],[EVENT_TIME],[TRAN_SUB_TYPE],[SYSTEM_TIME],[TXN_DR_CR],[CX_ACCT_ID],[TXN_AMT],[HOST_USER_ID],[HOST_ID],[ADD_ENTITY_ID2],[TXN_ID],[TXN_CODE],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[REF_AMT],[ADD_ENTITY_ID5],[EVENT_TYPE],[TXN_CURR],[CUST_ID],[REF_CURNCY],[TRAN_SRL_NO],[CX_CUST_ID],[EVENT_NAME],[TRAN_TYPE],[REMARKS],[SYSTEM_COUNTRY]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ZONE_DATE`,`ZONE_CODE`,`MSG_SOURCE`,`KEYS`,`CHANNEL`,`NIC`,`EVENT_TIME`,`TRAN_SUB_TYPE`,`SYSTEM_TIME`,`TXN_DR_CR`,`CX_ACCT_ID`,`TXN_AMT`,`HOST_USER_ID`,`HOST_ID`,`ADD_ENTITY_ID2`,`TXN_ID`,`TXN_CODE`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`REF_AMT`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`TXN_CURR`,`CUST_ID`,`REF_CURNCY`,`TRAN_SRL_NO`,`CX_CUST_ID`,`EVENT_NAME`,`TRAN_TYPE`,`REMARKS`,`SYSTEM_COUNTRY`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

