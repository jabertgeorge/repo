package clari5.custom.wlalert;

import clari5.custom.wlalert.daemon.RuleFact;
import clari5.custom.wlalert.db.IDBOperation;
import clari5.custom.wlalert.db.WlRecordDetail;
import cxps.apex.utils.CxpsLogger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

/**
 * @author lisa
 */
public class FileProcess {
    public static CxpsLogger logger = CxpsLogger.getLogger(FileProcess.class);

    static FileStructure fileStructure = new FileStructure();


    public void getFile() {

        IDBOperation wlRecordDetail = new WlRecordDetail();

        File[] listOfDirectory = fileStructure.getDirList();

        for (File file : listOfDirectory) {
            if (file.isDirectory()) {

                boolean status = fileunzip(file.getAbsolutePath());


                if (fileStructure.getFileType(file.getName()).equalsIgnoreCase("xml") && status) {
                    wlRecordDetail.insert(readXmlFile());
                    logger.warn("File type obtained is xml");

                } else if (fileStructure.getFileType(file.getName()).equalsIgnoreCase("csv") && status) {
                    wlRecordDetail.insert(readCsvFile());
                    logger.warn("File type obtained is csv");

                }
            }


        }
    }


    public RuleFact readXmlFile() {

        RuleFact ruleFact = null;
        File[] xmlFilesList = new File(fileStructure.getUnzipFilePath()).listFiles();
        XmlParser parser = new XmlParser();
        for (File xmlFile : xmlFilesList) {
            ruleFact = parser.xmlparser(fileStructure.getUnzipFilePath(), xmlFile);
            break;
        }
        logger.warn("Rule Fact for xml File" + ruleFact.toString());
        return ruleFact;

    }

    public RuleFact readCsvFile() {
        RuleFact ruleFact = null;
        CsvParser csvParser = new CsvParser();
        File[] csvFileList = new File(fileStructure.getUnzipFilePath()).listFiles();

        for (File csvFile : csvFileList) {
            ruleFact = csvParser.csvparser(fileStructure.getUnzipFilePath(), csvFile);
        }
        logger.warn("Rule Fact for csv File" + ruleFact.toString());
        return ruleFact;
    }


    public boolean fileunzip(String dir) {
        File dirPath = new File(dir);

       try {
            if (dirPath.list().length <= 0) return false;

            File[] zipFileList = dirPath.listFiles();

            UnZip unZip = new UnZip();
            fileStructure.setUnzipFilePath(dir + "/unzip");

            for (File zipFile : zipFileList) {

                String zipFileName = zipFile.getAbsolutePath();


                unZip.unZipIt(zipFileName, fileStructure.getUnzipFilePath());

                FileUtils.copyFile(zipFile, new File(fileStructure.getIndexingPath() + "/" + zipFile.getName()));
                File archiveFile = new File(fileStructure.getArchiveFilePath());
                logger.info("Archive path " +fileStructure.getArchiveFilePath());
                if (!archiveFile.exists()) archiveFile.mkdirs();
                Files.move(Paths.get(zipFileName), Paths.get(fileStructure.getArchiveFilePath() + zipFile.getName()));
            }
            return true;

        } catch (IOException io) {
            logger.error("Exception  while unzipping the file [" + io.getMessage() + "] Cause [" + io.getCause() + "]");
            io.printStackTrace();
        }

        return false;
    }

    public static void main(String[] args) {
        FileProcess fileProcess = new FileProcess();
        fileProcess.getFile();
    }

}