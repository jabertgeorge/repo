package clari5.upload.ui;

import java.util.List;

import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

/**
 * Created by shishir on 25/5/17.
 */
public class Delete {
    private static CxpsLogger logger = CxpsLogger.getLogger(DeleteServlet.class);
    List<String> headers;
    List<String> values;
    String tableName;

    Delete(List<String> headers, List<String> values, String tableName) {
        this.headers = headers;
        this.values = values;
        this.tableName = tableName;
    }

    public String getDeleteQuery() {
        logger.info("tableName for which recors will be deleted" + tableName);
        String deleteSql = "delete from " + tableName + " where ";
        logger.info("Delete Query ["+deleteSql+"]");
        String where = "";
        int noCol = headers.size();
        for (int i = 0; i < noCol; ++i) {
            String currentHeader = this.headers.get(i).toString();
            String currentValue = this.values.get(i).toString();
            if (currentHeader.equals("LCHG_TIME") || currentHeader.equals("RCRE_TIME")) continue;
            if (StringUtils.isNullOrEmpty(this.values.get(i))) {
                where += "\"" + currentHeader + "\" is null  ";
            } else {
                where += "\"" + currentHeader + "\" = '" + currentValue + "'";
            }
            if (i != noCol - 1) where += " and ";
        }
        deleteSql += " " + where;
        logger.info("Delete Query" + deleteSql);
        return deleteSql;
    }
}
