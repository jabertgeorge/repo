// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_TRFT_TXN", Schema="rice")
public class FT_TrftTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String branchIdDesc;
       @Field(size=20) public String counterPartyAddress;
       @Field(size=20) public String sourceBank;
       @Field(size=20) public String countrPartyAccount;
       @Field(size=20) public String counterPartyAmount;
       @Field(size=20) public String onlineBatch;
       @Field(size=20) public String tranSubType;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String tranId;
       @Field(size=20) public String acctType;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=20) public String txnCode;
       @Field(size=20) public String sourceOrDestAcctId;
       @Field(size=11) public Double refAmt;
       @Field(size=20) public String sequenceNumber;
       @Field(size=20) public String tranCrncyCode;
       @Field(size=20) public String acctName;
       @Field(size=20) public String tranSrlNo;
       @Field(size=20) public String counterPartyBank;
       @Field(size=20) public String counterPartyBankAddress;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=11) public Double avlBal;
       @Field public java.sql.Timestamp tranPostedDt;
       @Field public java.sql.Timestamp txnValueDate;
       @Field(size=20) public String systemCountry;
       @Field(size=20) public String desc;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String schemeCode;
       @Field(size=20) public String keys;
       @Field(size=20) public String counterPartyCurrencyLcy;
       @Field public java.sql.Timestamp eventTime;
       @Field(size=20) public String channelId;
       @Field(size=20) public String counterPartyBusiness;
       @Field(size=20) public String counterPartyBIC;
       @Field(size=20) public String txnStatus;
       @Field(size=20) public String cxAcctId;
       @Field(size=11) public Double txnAmt;
       @Field(size=20) public String counterPartyAmountLcy;
       @Field(size=20) public String counterPartyCountryCode;
       @Field(size=20) public String tellerNumber;
       @Field(size=20) public String counterPartyName;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String addEntityId1;
       @Field(size=20) public String counterPartyCurrency;
       @Field(size=100) public String addEntityId4;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String eventSubtype;
       @Field(size=100) public String addEntityId5;
       @Field(size=100) public String eventType;
       @Field(size=20) public String accountOwnership;
       @Field(size=20) public String custId;
       @Field(size=11) public Double refCurncy;
       @Field public java.sql.Timestamp accountOpendate;
       @Field(size=20) public String counterPartyBankCode;
       @Field(size=20) public String tranType;
       @Field(size=20) public String instrumentType;
       @Field(size=20) public String remarks;


    @JsonIgnore
    public ITable<FT_TrftTxnEvent> t = AEF.getITable(this);

	public FT_TrftTxnEvent(){}

    public FT_TrftTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("TrftTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBranchIdDesc(json.getString("acct-Branch-id"));
            setCounterPartyAddress(json.getString("CounterParty_Address"));
            setSourceBank(json.getString("bank-code"));
            setCountrPartyAccount(json.getString("CounterParty_Account"));
            setCounterPartyAmount(json.getString("CounterParty_Amount"));
            setOnlineBatch(json.getString("online-batch"));
            setTranSubType(json.getString("tran-sub-type"));
            setSystemTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setTxnDrCr(json.getString("part-tran-type"));
            setTranId(json.getString("tran-id"));
            setAcctType(json.getString("Acct-type"));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setTxnCode(json.getString("tran_code"));
            setSourceOrDestAcctId(json.getString("account-id"));
            setRefAmt(EventHelper.toDouble(json.getString("lcy-tran-amt")));
            setSequenceNumber(json.getString("Sequence_number"));
            setTranCrncyCode(json.getString("tran-crncy-code"));
            setAcctName(json.getString("acct-name"));
            setTranSrlNo(json.getString("part_tran_srl_num"));
            setCounterPartyBank(json.getString("CounterParty_Bank"));
            setCounterPartyBankAddress(json.getString("CounterParty_Bank_Address"));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setAvlBal(EventHelper.toDouble(json.getString("avl-bal_LCY")));
            setTranPostedDt(EventHelper.toTimestamp(json.getString("pstd-date")));
            setTxnValueDate(EventHelper.toTimestamp(json.getString("value-date")));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));
            setDesc(json.getString("tran-particular"));
            setMsgSource(json.getString("source"));
            setSchemeCode(json.getString("Prod-code"));
            setKeys(json.getString("keys"));
            setCounterPartyCurrencyLcy(json.getString("CounterParty_Currency_LCY"));
            setEventTime(EventHelper.toTimestamp(json.getString("eventts")));
            setChannelId(json.getString("channel"));
            setCounterPartyBusiness(json.getString("CounterParty_Business"));
            setCounterPartyBIC(json.getString("CounterParty_BIC"));
            setTxnStatus(json.getString("pstd-flg"));
            setCxAcctId(json.getString("cx-acct-id"));
            setTxnAmt(EventHelper.toDouble(json.getString("tran-amt")));
            setCounterPartyAmountLcy(json.getString("CounterParty _Amount_LCY"));
            setCounterPartyCountryCode(json.getString("CounterParty_Country_Code"));
            setTellerNumber(json.getString("Teller_number"));
            setCounterPartyName(json.getString("CounterParty_Name"));
            setAddEntityId2(json.getString("reservedfield2"));
            setAddEntityId1(json.getString("reservedfield1"));
            setCounterPartyCurrency(json.getString("CounterParty_Currency"));
            setAddEntityId4(json.getString("reservedfield4"));
            setTxnDate(EventHelper.toTimestamp(json.getString("tran-date")));
            setAddEntityId3(json.getString("reservedfield3"));
            setEventSubtype(json.getString("eventsubtype"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("eventtype"));
            setAccountOwnership(json.getString("acct-ownership"));
            setCustId(json.getString("cust-id"));
            setRefCurncy(EventHelper.toDouble(json.getString("lcy-tran-crncy")));
            setAccountOpendate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setCounterPartyBankCode(json.getString("CounterParty_Bank_Code"));
            setTranType(json.getString("tran-type"));
            setInstrumentType(json.getString("instrmnt-type"));
            setRemarks(json.getString("tran-rmks"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FTR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getCounterPartyAddress(){ return counterPartyAddress; }

    public String getSourceBank(){ return sourceBank; }

    public String getCountrPartyAccount(){ return countrPartyAccount; }

    public String getCounterPartyAmount(){ return counterPartyAmount; }

    public String getOnlineBatch(){ return onlineBatch; }

    public String getTranSubType(){ return tranSubType; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getTranId(){ return tranId; }

    public String getAcctType(){ return acctType; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public String getTxnCode(){ return txnCode; }

    public String getSourceOrDestAcctId(){ return sourceOrDestAcctId; }

    public Double getRefAmt(){ return refAmt; }

    public String getSequenceNumber(){ return sequenceNumber; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getAcctName(){ return acctName; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public String getCounterPartyBank(){ return counterPartyBank; }

    public String getCounterPartyBankAddress(){ return counterPartyBankAddress; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public Double getAvlBal(){ return avlBal; }

    public java.sql.Timestamp getTranPostedDt(){ return tranPostedDt; }

    public java.sql.Timestamp getTxnValueDate(){ return txnValueDate; }

    public String getSystemCountry(){ return systemCountry; }

    public String getDesc(){ return desc; }

    public String getMsgSource(){ return msgSource; }

    public String getSchemeCode(){ return schemeCode; }

    public String getKeys(){ return keys; }

    public String getCounterPartyCurrencyLcy(){ return counterPartyCurrencyLcy; }

    public java.sql.Timestamp getEventTime(){ return eventTime; }

    public String getChannelId(){ return channelId; }

    public String getCounterPartyBusiness(){ return counterPartyBusiness; }

    public String getCounterPartyBIC(){ return counterPartyBIC; }

    public String getTxnStatus(){ return txnStatus; }

    public String getCxAcctId(){ return cxAcctId; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getCounterPartyAmountLcy(){ return counterPartyAmountLcy; }

    public String getCounterPartyCountryCode(){ return counterPartyCountryCode; }

    public String getTellerNumber(){ return tellerNumber; }

    public String getCounterPartyName(){ return counterPartyName; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getCounterPartyCurrency(){ return counterPartyCurrency; }

    public String getAddEntityId4(){ return addEntityId4; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getAccountOwnership(){ return accountOwnership; }

    public String getCustId(){ return custId; }

    public Double getRefCurncy(){ return refCurncy; }

    public java.sql.Timestamp getAccountOpendate(){ return accountOpendate; }

    public String getCounterPartyBankCode(){ return counterPartyBankCode; }

    public String getTranType(){ return tranType; }

    public String getInstrumentType(){ return instrumentType; }

    public String getRemarks(){ return remarks; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setCounterPartyAddress(String val){ this.counterPartyAddress = val; }
    public void setSourceBank(String val){ this.sourceBank = val; }
    public void setCountrPartyAccount(String val){ this.countrPartyAccount = val; }
    public void setCounterPartyAmount(String val){ this.counterPartyAmount = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setAcctType(String val){ this.acctType = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setSourceOrDestAcctId(String val){ this.sourceOrDestAcctId = val; }
    public void setRefAmt(Double val){ this.refAmt = val; }
    public void setSequenceNumber(String val){ this.sequenceNumber = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setCounterPartyBank(String val){ this.counterPartyBank = val; }
    public void setCounterPartyBankAddress(String val){ this.counterPartyBankAddress = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setTranPostedDt(java.sql.Timestamp val){ this.tranPostedDt = val; }
    public void setTxnValueDate(java.sql.Timestamp val){ this.txnValueDate = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }
    public void setDesc(String val){ this.desc = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setSchemeCode(String val){ this.schemeCode = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setCounterPartyCurrencyLcy(String val){ this.counterPartyCurrencyLcy = val; }
    public void setEventTime(java.sql.Timestamp val){ this.eventTime = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setCounterPartyBusiness(String val){ this.counterPartyBusiness = val; }
    public void setCounterPartyBIC(String val){ this.counterPartyBIC = val; }
    public void setTxnStatus(String val){ this.txnStatus = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setCounterPartyAmountLcy(String val){ this.counterPartyAmountLcy = val; }
    public void setCounterPartyCountryCode(String val){ this.counterPartyCountryCode = val; }
    public void setTellerNumber(String val){ this.tellerNumber = val; }
    public void setCounterPartyName(String val){ this.counterPartyName = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setCounterPartyCurrency(String val){ this.counterPartyCurrency = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setAccountOwnership(String val){ this.accountOwnership = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setRefCurncy(Double val){ this.refCurncy = val; }
    public void setAccountOpendate(java.sql.Timestamp val){ this.accountOpendate = val; }
    public void setCounterPartyBankCode(String val){ this.counterPartyBankCode = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setRemarks(String val){ this.remarks = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_TrftTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_TrftTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "TrftTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}