package clari5.custom.boc.integration.data;

public class NFT_Dedupe extends ITableData {

    private String tableName = "NFT_DEDUPE";
    private String event_type = "NFT_Dedupe";
    private String host_id;
    private String EVENT_ID;
    private String completemailingaddr;
    private String emailid;
    private String pan;
    private String passportnumber;
    private String ucicid;
    private String accountid;

    private String completepermanentaddr;
    private String dob;
    private String permanentaddr1;
    private String permanentaddr2;
    private String officelandlinenumber;
    private String permanentaddr3;
    private String doi;
    private String completemailingaddrchanged;
    private String accountcount;
    private String landlinenumber;
    private String product_code;
    private String mailingaddr1;
    private String mailingaddr2;
    private String mobnumber;
    private String mailingaddr3;
    private String custid;
    private String sys_time;
    private String homelandlinenumber;
    private String completeperaddrchanged;
    private String custname;
    private String handphone;
    private String entity_type;
    private String namechanged;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getEVENT_ID() {
        return EVENT_ID;
    }

    public void setEVENT_ID(String EVENT_ID) {
        this.EVENT_ID = EVENT_ID;
    }

    public String getCompletemailingaddr() {
        return completemailingaddr;
    }

    public void setCompletemailingaddr(String completemailingaddr) {
        this.completemailingaddr = completemailingaddr;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getPassportnumber() {
        return passportnumber;
    }

    public void setPassportnumber(String passportnumber) {
        this.passportnumber = passportnumber;
    }

    public String getUcicid() {
        return ucicid;
    }

    public void setUcicid(String ucicid) {
        this.ucicid = ucicid;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public String getCompletepermanentaddr() {
        return completepermanentaddr;
    }

    public void setCompletepermanentaddr(String completepermanentaddr) {
        this.completepermanentaddr = completepermanentaddr;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPermanentaddr1() {
        return permanentaddr1;
    }

    public void setPermanentaddr1(String permanentaddr1) {
        this.permanentaddr1 = permanentaddr1;
    }

    public String getPermanentaddr2() {
        return permanentaddr2;
    }

    public void setPermanentaddr2(String permanentaddr2) {
        this.permanentaddr2 = permanentaddr2;
    }

    public String getOfficelandlinenumber() {
        return officelandlinenumber;
    }

    public void setOfficelandlinenumber(String officelandlinenumber) {
        this.officelandlinenumber = officelandlinenumber;
    }

    public String getPermanentaddr3() {
        return permanentaddr3;
    }

    public void setPermanentaddr3(String permanentaddr3) {
        this.permanentaddr3 = permanentaddr3;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getCompletemailingaddrchanged() {
        return completemailingaddrchanged;
    }

    public void setCompletemailingaddrchanged(String completemailingaddrchanged) {
        this.completemailingaddrchanged = completemailingaddrchanged;
    }

    public String getAccountcount() {
        return accountcount;
    }

    public void setAccountcount(String accountcount) {
        this.accountcount = accountcount;
    }

    public String getLandlinenumber() {
        return landlinenumber;
    }

    public void setLandlinenumber(String landlinenumber) {
        this.landlinenumber = landlinenumber;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getMailingaddr1() {
        return mailingaddr1;
    }

    public void setMailingaddr1(String mailingaddr1) {
        this.mailingaddr1 = mailingaddr1;
    }

    public String getMailingaddr2() {
        return mailingaddr2;
    }

    public void setMailingaddr2(String mailingaddr2) {
        this.mailingaddr2 = mailingaddr2;
    }

    public String getMobnumber() {
        return mobnumber;
    }

    public void setMobnumber(String mobnumber) {
        this.mobnumber = mobnumber;
    }

    public String getMailingaddr3() {
        return mailingaddr3;
    }

    public void setMailingaddr3(String mailingaddr3) {
        this.mailingaddr3 = mailingaddr3;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getHomelandlinenumber() {
        return homelandlinenumber;
    }

    public void setHomelandlinenumber(String homelandlinenumber) {
        this.homelandlinenumber = homelandlinenumber;
    }

    public String getCompleteperaddrchanged() {
        return completeperaddrchanged;
    }

    public void setCompleteperaddrchanged(String completeperaddrchanged) {
        this.completeperaddrchanged = completeperaddrchanged;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public String getNamechanged() {
        return namechanged;
    }

    public void setNamechanged(String namechanged) {
        this.namechanged = namechanged;
    }
}
