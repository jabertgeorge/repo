package clari5.custom.dev.daemons;

import clari5.custom.dowjones.Controller;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.io.UnsupportedEncodingException;

public class DJDaemon extends CxpsRunnable implements ICxResource {
    @Override
    protected Object getData() throws RuntimeFatalException {
        return 0;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        try {
            Controller.process();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void configure(Hocon h) {
        System.out.println("DJ Configured");
    }
}
