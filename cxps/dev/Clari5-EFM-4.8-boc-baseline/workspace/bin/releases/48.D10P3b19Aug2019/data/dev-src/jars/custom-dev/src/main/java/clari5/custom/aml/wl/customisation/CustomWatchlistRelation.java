package clari5.custom.aml.wl.customisation;

import clari5.aml.wle.MatchedRecord;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import org.json.JSONArray;
import org.json.JSONObject;
import clari5.aml.wl.customisation.IWLResultsCustom;
import cxps.apex.utils.CxpsLogger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;



/**
 * Created by Suryakant on 7/17/19
 */

public class CustomWatchlistRelation implements IWLResultsCustom {

    public Collection<MatchedRecord> populateRelationDetails(Collection<MatchedRecord> matchedRecordList,String rulename) {
        Collection<MatchedRecord> list =  new ArrayList<>();
        for (MatchedRecord object: matchedRecordList) {
            String str = getRelationdetails(object.getDocId(),rulename).toString();
            object.setRelations(str);
            list.add(object);
        }
        return list;
    }

    public JSONArray getRelationdetails(String cust_id,String rulename) {
        JSONArray jsarray = new JSONArray();

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement statement = connection.prepareStatement("SELECT CUSTOMERCIF2,RELATIONSHIP from memo_relationship where CUSTOMERCIF1= ?")) {

                    statement.setString(1, cust_id);

                    try (ResultSet rs = statement.executeQuery()) {

                        while (rs.next()) {

                            JSONObject jsobject = new JSONObject();
                            String cif2 = rs.getString("CUSTOMERCIF2");
                            jsobject.put("customercif2", cif2);
                            jsobject.put("Relationship", rs.getString("RELATIONSHIP"));
                            getdetailscif2(cif2, connection, jsobject);
                            jsarray.put(jsobject);
                        }
                       if( custDetailsinsertion(cust_id,rulename,jsarray,connection)){
                          // System.out.println("Data inserted in the table");
                       }


                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();

        }

        return jsarray;
    }

    public JSONObject getdetailscif2(String custid, CxConnection connection, JSONObject jobject) {

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (PreparedStatement statement = connection.prepareStatement("select custName,custPassportNo,nationalId from CUSTOMER where cxCifID=?")) {

                statement.setString(1, custid);

                try (ResultSet rs = statement.executeQuery()) {
                    while (rs.next()) {
                        jobject.put("Customercif2Name", rs.getString("custName"));
                        jobject.put("passport", rs.getString("custPassportNo"));
                        jobject.put("nic", rs.getString("nationalId"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobject;
    }

    public boolean custDetailsinsertion(String cust_id,String rulename, JSONArray jsarray,CxConnection connection) {

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO WLSEARCH_RESULT(CUST_ID,SEARCH_RESULT,WL_RULE) VALUES (?,?,?)")) {
                statement.setString(1, cust_id);
                statement.setString(2, jsarray.toString());
                statement.setString(3, rulename);
                statement.execute();
                connection.commit();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
