package clari5.customAml.web.la;

/**
 * Created by anshul on 26/8/15.
 */

import clari5.customAml.web.la.mapper.LAMapper;
import clari5.hfdb.AmlCustomerView;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class LAServlet extends HttpServlet {

    private JSONObject responseJson = new JSONObject();
    private static final long serialVersionUID = -2972318723733399669L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGetOrPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGetOrPost(request, response);
    }

    private void doGetOrPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {

            String type = request.getParameter("type");
            String value = request.getParameter("value");

            if (type == null || type.equals("")) {
                responseJson.put("status", "failed");
                responseJson.put("reason", "Type is null or empty");
            }
            if (value == null || value.equals("")) {
                responseJson.put("status", "failed");
                responseJson.put("reason", "value is null or empty");
            }
            JSONObject resultJson = getDetails(type, value);
            response.getWriter().write(resultJson.toString());


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private JSONObject getDetails(String type, String value) {

        JSONObject resultJson = new JSONObject();
        LAMapper mapper;
        List<AmlCustomerView> custids;
        try (RDBMSSession session = Rdbms.getAppSession()) {
            mapper = session.getMapper(LAMapper.class);
            switch (type.toUpperCase()) {
                case "PAN":
                    custids = mapper.getDetailsGivenPan(value);
                    break;
                case "EMAIL":
                    custids = mapper.getDetailsGivenEmail(value);
                    break;
                case "MOB":
                    custids = mapper.getDetailsGivenMobile(value);
                    break;
                default:
                    resultJson.put("status", "failed");
                    resultJson.put("reason", "Only PAN, EMAIL and MOBILE are allowed in category");
                    return resultJson;
            }


            System.out.println("size is " + custids.size());
            if (custids == null || custids.size() == 0) {
                resultJson.put("status", "failed");
                resultJson.put("reason", "No matching customers found for the given " + type + " : " + value);
                return resultJson;
            }

            resultJson.put("name", type.toUpperCase());
            resultJson.put("Acc", value);
            JSONArray acctJsonArray = new JSONArray();

            for (AmlCustomerView custid : custids) {
                getCustDetails(custid.getCustId(), mapper, acctJsonArray);

            }
            resultJson.put("children", acctJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resultJson;

    }

    private void getCustDetails(String custId, LAMapper mapper, JSONArray acctJsonArray) throws JSONException {
        JSONObject custJson = new JSONObject();
        custJson.put("Acc", custId);

        List<AmlCustomerView> customers = mapper.getAllDetailsOfACustomer(custId);
        if (customers == null) {
            return;
        }
        AmlCustomerView customer = customers.get(0);
        JSONObject detailsJson = new JSONObject();
        detailsJson.put("Customer Name", customer.getName());
        detailsJson.put("Date of Birth", customer.getDob());
        detailsJson.put("Phone", customer.getMobile1());
        detailsJson.put("Address", customer.getResAddr());
        JSONArray detailsJsonArray = new JSONArray();
        detailsJsonArray.put(detailsJson);
        custJson.put("Account", detailsJsonArray);
        acctJsonArray.put(custJson);
    }

    @Override
    public void init() throws ServletException {

    }

}
