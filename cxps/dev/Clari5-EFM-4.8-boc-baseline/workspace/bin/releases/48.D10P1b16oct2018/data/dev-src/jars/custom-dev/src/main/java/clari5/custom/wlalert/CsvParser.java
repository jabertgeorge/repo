package clari5.custom.wlalert;

import clari5.custom.wlalert.daemon.RuleFact;
import cxps.apex.utils.CxpsLogger;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

public class CsvParser {
    public static CxpsLogger logger = CxpsLogger.getLogger(CsvParser.class);

    public RuleFact csvparser(String unzipFilePath, File csvName) {
        RuleFact ruleFact = new RuleFact();
        try {
            ArrayList<Integer> headers = new ArrayList<>();

            File file = new File(csvName.getAbsolutePath());

            List<String> lines = Files.readAllLines(file.toPath());
            int i = 0;

            for (String line : lines) {
                String[] array = line.split("\\|");


                    if (i == 0) {
                        headers = getFileHeader(array);
                        i++;
                    } else {
                        ruleFact.addName(array[headers.get(0)]);
                        ruleFact.addPassport(array[headers.get(1)]);
                        ruleFact.addNic(array[headers.get(2)]);
                    }

            }
            ruleFact.setList("bankinternallist");
            FileUtils.deleteDirectory(new File(unzipFilePath));


        } catch (Exception e) {
           logger.warn("Exception while parsing the csv file ["+e.getMessage()+"] Cause ["+e.getCause()+"]");
        }
        return ruleFact;
    }


    private ArrayList<Integer> getFileHeader(String[] headers) {
        ArrayList<Integer> header = new ArrayList<>();
        List<String> csvHeader = Arrays.asList(headers);
        header.add(csvHeader.indexOf("name"));
        header.add(csvHeader.indexOf("passport"));
        header.add(csvHeader.indexOf("nic"));
        return header;
    }
}

