var webpack = require("webpack");
var CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
    entry: {
        "app": "./app/main",
        
    },
    output: {
        path: __dirname,
        filename: "./_dist/bundle.js",
        publicPath: 'http://localhost:4242/watchlist'
    },
    resolve: {
        extensions: ['.js', '.html']
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                use: ["html-loader?interpolate=require&-minimize"]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            minimize: true
        }),
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            __dirname
        )
    ],
    devServer: {
        historyApiFallback: true,
        stats: 'minimal',
        inline: true,
        port: 4242
    }
};

