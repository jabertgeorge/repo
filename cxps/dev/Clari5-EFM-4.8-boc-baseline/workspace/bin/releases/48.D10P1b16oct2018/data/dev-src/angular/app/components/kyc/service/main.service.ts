/**
 * Created by hemanshu on 12/4/2018.
 */

import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {CustomerDetails} from "../model.component";

@Injectable()
export class KYCService {

    constructor(private http: Http) {
    }


    public baseUrlKYC: string = "kyc/rest/kyc";
    private customerId: string = "";
    private custDetails: CustomerDetails = new CustomerDetails();

    getCustomerId() {
        return this.customerId;
    }

    setCustomerId(val: string) {
        this.customerId = val;
    }

    getCustomerDetails() {
        return this.custDetails;
    }

    setCustomerDetails(val: CustomerDetails) {
        this.custDetails = val;
    }


    public extractText(response: any) {
        try {
            return response.text();
        } catch (e) {
            if (!response.ok && response.status === 401) {
                window.location.href = window.location.origin + "/efm";
            }
        }
    }

    public extractJson(response: any) {
        try {
            return response.json();
        } catch (e) {
            // Try redirect code
            if (!response.ok && response.status === 401) {
                window.location.href = window.location.origin + "/efm";
            }
        }
    }

    public handleError(error: any) {
        if (error.status === 401) {
            window.location.href = window.location.origin + "/efm";
        }
        return Observable.throw(error);
    }


    searchKYCDetail(val: string): Observable<any> {
        return this.http.get(this.baseUrlKYC + "/fetchCustomerDetails/" + val)
            .map(this.extractJson)
            .catch(this.handleError);
    }

    getDeltaChanges(): Observable<any> {
        return this.http.get(this.baseUrlKYC + "/getDelta")
            .map(this.extractJson)
            .catch(this.handleError);
    }

    submitChanges(val: any): Observable<any> {
        return this.http.post(this.baseUrlKYC + "/updateDetails", val)
            .map(this.extractJson)
            .catch(this.handleError);
    }


    verifyChanges(val: any): Observable<any> {
        return this.http.post(this.baseUrlKYC + "/verifyChanges", val)
            .map(this.extractJson)
            .catch(this.handleError);
    }

}
