package clari5.custom.dev.event.generator;

import clari5.tools.util.Hocon;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.field.Field;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MTEventGenerator {
    private static final String INWARD = ".INWARD";
    private static final String OUTWARD = ".OUTWARD";

    private static final Hocon messageMap = new Hocon();
    private static final Hocon event = new Hocon();
    private static final Hocon tagListIndex = new Hocon();
    private static final Hocon parserPackage = new Hocon();
    static {
        messageMap.loadFromContext("mt_message.conf");
        event.loadFromContext("ft_remittanceEvent.conf");
        tagListIndex.loadFromContext("mt_tags_index.conf");
        parserPackage.loadFromContext("parser-config.conf");
    }

    protected Hocon getEvent() {
        return event;
    }

    protected Hocon getMessageMap() { return messageMap; }

    protected Hocon getTagListIndex() {
        return tagListIndex;
    }

    public Map<String, String> getEventFromMessage(SwiftMessage message) {
        return message.toMT().isInput() ? getEvent(message, Boolean.TRUE) : getEvent(message, Boolean.FALSE);
    }

    private Map<String, String> getEvent(SwiftMessage message, Boolean isInward) {
        Map<String, String> valueMap  = new HashMap<>();
        try {
            Class mtClass = Class.forName(parserPackage.getString("parser-package." + message.getType()));
            Object mtMessage = null;

            for (Constructor constructor : mtClass.getDeclaredConstructors()) {
                if (constructor.getParameterCount() == 1
                        && constructor.getParameterTypes()[0].getSimpleName().equals("SwiftMessage")) {
                    mtMessage = constructor.newInstance(message);
                }
            }
            for (String key : getMessageMap().get("MESSAGE.MT"+message.getType() + (isInward ? INWARD : OUTWARD)).getKeys()) {
                for (String tag : getMessageMap().get("MESSAGE.MT" + message.getType() + (isInward ? INWARD : OUTWARD)).getStringList(key)) {
                    Object fieldObj = mtClass.getMethod("getField" + tag.toUpperCase()).invoke(mtMessage, new Class[] {});
                    boolean isListField = false;
                    Field field = null;
                    List<Field> listField = null;
                    if (fieldObj instanceof List) {
                        listField = (List<Field>) fieldObj;
                        isListField = true;
                    } else {
                        field = (Field) fieldObj;
                    }
                    if (null != field && !isListField) {
                        List<String> components = new ArrayList<>();
                        for (String s : field.getComponents()){
                            if (s != null){
                                components.add(s);
                            }
                        }
                        if (getTagListIndex().get("Tag.MT" + message.getType() + "." + tag).getInt(key) - 1 < components.size()) {
                            String val = components.get(getTagListIndex().get("Tag.MT" + message.getType() + "." + tag).getInt(key) - 1);
                            valueMap.put(key, null != val ? val : StringUtils.EMPTY);
                        }
                    } else if (isListField && null != listField && listField.size() > 0) {
                        StringBuffer sb = new StringBuffer();
                        for (Field flds : listField) {
                            if (null != flds && flds.getValue().trim().length() > 0) {
                                sb.append(flds.getValue());
                            } else
                                continue;
                        }
                        valueMap.put(key, sb.toString());
                    } else
                        continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valueMap;
    }
}
