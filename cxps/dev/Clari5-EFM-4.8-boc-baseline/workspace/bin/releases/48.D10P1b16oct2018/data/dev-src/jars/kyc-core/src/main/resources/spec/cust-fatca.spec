clari5.kyc.entity.cust-fatca{
    attributes = [
		{ name : cust-id, type ="string:100" , key=true}
		
		{ name : classification, type = "string:50" }
		{ name : us-person, type = "string:50" }
		{ name : npfi, type = "string:100" }
		{ name : w8-available,  type = "string:100" }
		{ name : expiry-date, type : "string:50"}
	]
}
