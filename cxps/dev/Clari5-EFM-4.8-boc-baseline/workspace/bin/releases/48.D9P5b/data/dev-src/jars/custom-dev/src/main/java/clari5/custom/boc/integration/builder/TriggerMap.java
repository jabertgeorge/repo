package clari5.custom.boc.integration.builder;

import clari5.custom.boc.integration.audit.AuditManager;
import clari5.custom.boc.integration.audit.LogLevel;
import clari5.custom.boc.integration.builder.data.Trigger;
import clari5.platform.util.Hocon;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;

import java.util.*;

// As of now only one trigger is supported.
// Actually should have included in the class than inheriting. The class evolved over time due to
// dynamic requirements and due to time limits, could not make the change. This is To do.
public class TriggerMap extends HashMap<String, Map<String, List<Trigger>>> {
    public static CxpsLogger logger = CxpsLogger.getLogger(TriggerMap.class);
    private static final long serialVersionUID = -1528496110305631828L;

    private static TriggerMap triggers = null;
    private Map<String, String> rule = new HashMap<String, String>();

    private TriggerMap() {
        Hocon h = new Hocon();
        try {
            h.loadFromContext("tbl-trigger-mapping.config");
            Set<String> keys = h.getKeys();
            for (String k : keys) {
                Hocon th = h.get(k);
                Set<String> ths = th.getKeys();
                Map<String, List<Trigger>> eventToTriggers = new HashMap<String, List<Trigger>>();
                String eventName;
                for (String kt : ths) {
                    eventName = kt;
                    Hocon triggersH = th.get(kt);
                    Set<String> triggersSet = triggersH.getKeys();
                    List<Trigger> lt = new ArrayList<Trigger>();
                    for (String tr : triggersSet) {
                        if (!tr.equals("rule")) {
                            Trigger t = new Trigger();
                            t.setTrigger(triggersH.getString(tr + ".Trigger"));
                            t.setColumn(triggersH.getString(tr + ".Column"));
                            t.setWhen(triggersH.getString(tr + ".When"));
                            t.setKeys(triggersH.getStringList(tr + ".Keys"));
                            t.setWhere(triggersH.getStringList(tr + ".Where"));
                            t.setValue(triggersH.getString(tr + ".Value"));
                            lt.add(t);
                        }
                    }
                    if (triggersSet != null && triggersSet.size() != 0) {
                        rule.put(eventName, triggersH.getString("rule"));
                    }
                    eventToTriggers.put(eventName, lt);
                }
                this.put(k, eventToTriggers);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            throw e1;
        } finally {
        }
    }

    // To modify and improved.
    public boolean shouldProceed(JSONObject row, String eventName) throws Exception {
        String callerTableName = row.getString("tableName");
        Map<String, List<Trigger>> temporaryTriggersMap = this.get(callerTableName);
        List<Trigger> triggers = null;
        if (temporaryTriggersMap != null) {
            triggers = this.get(callerTableName).get(eventName);
        }
        boolean shouldProceed = true;
        if (triggers != null && triggers.size() != 0) {
            for (Trigger t : triggers) {
                shouldProceed = t.process(row, eventName);
            }
            if (!shouldProceed) {
                logger.debug("Condition not matched for event: " + eventName + " for row: "
                        + "" + row.toString() + ".\nNo event will be generated.");
            }
        } else {
            logger.debug(LogLevel.DEBUG, "Rule is configured for generating all events configured as jsons. "
                    + "Event: " + eventName + " for row: " + row.toString() + " will be processed.");
        }
        return shouldProceed;
    }

    public static TriggerMap getTrigger() {
        if (triggers == null) {
            triggers = new TriggerMap();
        }
        return triggers;
    }
}
