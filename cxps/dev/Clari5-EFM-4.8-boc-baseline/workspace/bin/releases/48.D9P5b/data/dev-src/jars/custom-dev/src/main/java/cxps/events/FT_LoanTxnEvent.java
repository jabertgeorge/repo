// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_LOANTXN", Schema="rice")
public class FT_LoanTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String exception;
       @Field(size=20) public String beneficiaryName;
       @Field public java.sql.Timestamp zoneDate;
       @Field(size=20) public String fcnrFlag;
       @Field(size=20) public String counterpartyBic;
       @Field(size=20) public String bin;
       @Field(size=20) public String channel;
       @Field(size=20) public String onlineBatch;
       @Field(size=20) public String tranSubType;
       @Field(size=20) public String txnRefType;
       @Field public java.sql.Timestamp systemTime;
       @Field(size=20) public String counterpartyBankAddress;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String acctType;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String hostId;
       @Field(size=11) public Double tranAmt;
       @Field(size=20) public String txnCode;
       @Field(size=20) public String directChannelId;
       @Field(size=20) public String counterpartyBank;
       @Field(size=20) public String sequenceNumber;
       @Field(size=20) public String counterpartyBusiness;
       @Field(size=20) public String tranCrncyCode;
       @Field public java.sql.Timestamp tranVerifiedDt;
       @Field(size=20) public String purAcctNum;
       @Field(size=20) public String cxCustId;
       @Field(size=100) public String eventName;
       @Field(size=20) public String payeeId;
       @Field(size=20) public String acctOccpCode;
       @Field(size=20) public String acctStatus;
       @Field(size=20) public String systemCountry;
       @Field(size=20) public String counterpartyCountryCode;
       @Field(size=20) public String entityType;
       @Field(size=20) public String zoneCode;
       @Field(size=20) public String countryCode;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String counterpartyAddress;
       @Field(size=20) public String merchantCateg;
       @Field(size=20) public String instAlpha;
       @Field(size=20) public String msgSource;
       @Field(size=20) public String payerIdTemp;
       @Field(size=20) public String terminalIpAddr;
       @Field(size=20) public String cxAcctId;
       @Field(size=20) public String entryUser;
       @Field(size=20) public String addEntityId2;
       @Field(size=20) public String addEntityId1;
       @Field(size=100) public String addEntityId4;
       @Field(size=20) public String counterpartyCurrencyLcy;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=20) public String addEntityId3;
       @Field(size=100) public String addEntityId5;
       @Field(size=20) public String eventType;
       @Field(size=20) public String accountOwnership;
       @Field(size=20) public String counterpartyAmount;
       @Field(size=11) public Double sanctionAmt;
       @Field(size=11) public Double refCurncy;
       @Field(size=20) public String kycStatus;
       @Field(size=20) public String agentId;
       @Field(size=20) public String counterpartyAmountLcy;
       @Field(size=20) public String tranType;
       @Field(size=20) public String channelDesc;
       @Field(size=20) public String branchIdDesc;
       @Field(size=20) public String creDrPtran;
       @Field(size=20) public String txnRefId;
       @Field(size=20) public String sourceBank;
       @Field(size=20) public String payeeCity;
       @Field(size=20) public String devOwnerId;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String tranId;
       @Field(size=20) public String counterpartyCurrency;
       @Field(size=20) public String entityId;
       @Field(size=20) public String eventSubType;
       @Field(size=20) public String sourceOrDestAcctId;
       @Field(size=11) public Double refAmt;
       @Field(size=20) public String placeHolder;
       @Field(size=20) public String directChannelControllerId;
       @Field(size=20) public String payeeName;
       @Field(size=20) public String channelType;
       @Field(size=20) public String acctName;
       @Field(size=20) public String custRefType;
       @Field(size=20) public String custCardId;
       @Field(size=20) public String counterpartyName;
       @Field(size=20) public String tranSrlNo;
       @Field(size=11) public Double avlBal;
       @Field(size=20) public String evtInducer;
       @Field public java.sql.Timestamp txnValueDate;
       @Field public java.sql.Timestamp tranPostedDt;
       @Field(size=20) public String schemeType;
       @Field(size=20) public String desc;
       @Field(size=20) public String agentType;
       @Field public java.sql.Timestamp tranEntryDt;
       @Field(size=20) public String cxCifId;
       @Field(size=20) public String actionAtHost;
       @Field(size=20) public String schemeCode;
       @Field(size=20) public String counterpartyAccount;
       @Field(size=20) public String keys;
       @Field(size=20) public String custRefId;
       @Field public java.sql.Timestamp eventTime;
       @Field(size=20) public String billId;
       @Field(size=11) public Double rate;
       @Field(size=20) public String txnStatus;
       @Field(size=11) public Double txnAmt;
       @Field(size=11) public Double effAvlBal;
       @Field(size=20) public String tranCategory;
       @Field(size=20) public String tellerNumber;
       @Field(size=20) public Long instrumentId;
       @Field(size=20) public String balanceCur;
       @Field(size=11) public Double ledgerBal;
       @Field(size=20) public String employeeId;
       @Field(size=20) public String acctSolId;
       @Field(size=100) public String eventSubtype;
       @Field(size=20) public String txnAmtCur;
       @Field(size=20) public String refCurrency;
       @Field(size=20) public String counterpartyBankCode;
       @Field(size=20) public String modeOprnCode;
       @Field(size=20) public String msgName;
       @Field(size=20) public String custId;
       @Field public java.sql.Date loanSettlementDate;
       @Field public java.sql.Timestamp accountOpendate;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String hdrmkrs;
       @Field(size=20) public String tranBrId;
       @Field(size=20) public String remitterName;
       @Field(size=20) public String instrumentType;
       @Field(size=11) public Double shadowBal;
       @Field(size=11) public Double refTranAmt;
       @Field(size=20) public Long eod;
       @Field(size=20) public String remarks;


    @JsonIgnore
    public ITable<FT_LoanTxnEvent> t = AEF.getITable(this);

	public FT_LoanTxnEvent(){}

    public FT_LoanTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("LoanTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setException(json.getString("exception"));
            setBeneficiaryName(json.getString("beneficiary_name"));
            setZoneDate(EventHelper.toTimestamp(json.getString("zone_date")));
            setFcnrFlag(json.getString("fcnr_flag"));
            setCounterpartyBic(json.getString("CounterpartyBIC"));
            setBin(json.getString("BIN"));
            setChannel(json.getString("channel"));
            setOnlineBatch(json.getString("online-batch"));
            setTranSubType(json.getString("tran-sub-type"));
            setTxnRefType(json.getString("txn_ref_type"));
            setSystemTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setCounterpartyBankAddress(json.getString("CounterpartyBankAddress"));
            setTxnDrCr(json.getString("part-tran-type"));
            setAcctType(json.getString("Acct-type"));
            setHostUserId(json.getString("host_user_id"));
            setHostId(json.getString("host-id"));
            setTranAmt(EventHelper.toDouble(json.getString("tran-amt")));
            setTxnCode(json.getString("tran_code"));
            setDirectChannelId(json.getString("dc_id"));
            setCounterpartyBank(json.getString("CounterpartyBank"));
            setSequenceNumber(json.getString("SequenceNumber"));
            setCounterpartyBusiness(json.getString("CounterpartyBusiness"));
            setTranCrncyCode(json.getString("tran-crncy-code"));
            setTranVerifiedDt(EventHelper.toTimestamp(json.getString("vfd_date")));
            setPurAcctNum(json.getString("pur_acct_num"));
            setCxCustId(json.getString("cx-cust-id"));
            setEventName(json.getString("event_name"));
            setPayeeId(json.getString("payee_id"));
            setAcctOccpCode(json.getString("acct_occp_code"));
            setAcctStatus(json.getString("Acct_Stat"));
            setSystemCountry(json.getString("SYSTEMCOUNTRY"));
            setCounterpartyCountryCode(json.getString("CounterpartyCountryCode"));
            setEntityType(json.getString("entity_type"));
            setZoneCode(json.getString("zone_code"));
            setCountryCode(json.getString("country_code"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setCounterpartyAddress(json.getString("CounterpartyAddress"));
            setMerchantCateg(json.getString("merchant_categ"));
            setInstAlpha(json.getString("instrmnt_alpha"));
            setMsgSource(json.getString("source"));
            setPayerIdTemp(json.getString("acid"));
            setTerminalIpAddr(json.getString("ip_address"));
            setCxAcctId(json.getString("cx-acct-id"));
            setEntryUser(json.getString("entry_user"));
            setAddEntityId2(json.getString("reservedfield2"));
            setAddEntityId1(json.getString("reservedfield1"));
            setAddEntityId4(json.getString("reservedfield4"));
            setCounterpartyCurrencyLcy(json.getString("CounterpartyCurrencyLCY"));
            setTxnDate(EventHelper.toTimestamp(json.getString("sys_time")));
            setAddEntityId3(json.getString("reservedfield3"));
            setAddEntityId5(json.getString("reservedfield5"));
            setEventType(json.getString("event_type"));
            setAccountOwnership(json.getString("acct-ownership"));
            setCounterpartyAmount(json.getString("CounterpartyAmount"));
            setSanctionAmt(EventHelper.toDouble(json.getString("sanction_amt")));
            setRefCurncy(EventHelper.toDouble(json.getString("lcy-tran-crncy")));
            setKycStatus(json.getString("KYC_Stat"));
            setAgentId(json.getString("pstd_user_id"));
            setCounterpartyAmountLcy(json.getString("CounterpartyAmountLCY"));
            setTranType(json.getString("tran-type"));
            setChannelDesc(json.getString("channel_desc"));
            setBranchIdDesc(json.getString("acct-Branch-id"));
            setCreDrPtran(json.getString("cre_dr_ptran"));
            setTxnRefId(json.getString("tran_id"));
            setSourceBank(json.getString("bank-code"));
            setPayeeCity(json.getString("payeecity"));
            setDevOwnerId(json.getString("dev_owner_id"));
            setDeviceId(json.getString("device_id"));
            setTranId(json.getString("tran-id"));
            setCounterpartyCurrency(json.getString("CounterpartyCurrency"));
            setEntityId(json.getString("entity_id"));
            setEventSubType(json.getString("event_sub_type"));
            setSourceOrDestAcctId(json.getString("account-id"));
            setRefAmt(EventHelper.toDouble(json.getString("lcy-tran-amt")));
            setPlaceHolder(json.getString("place_holder"));
            setDirectChannelControllerId(json.getString("dcc_id"));
            setPayeeName(json.getString("payeename"));
            setChannelType(json.getString("channel_type"));
            setAcctName(json.getString("acct-name"));
            setCustRefType(json.getString("cust_ref_type"));
            setCustCardId(json.getString("cust_card_id"));
            setCounterpartyName(json.getString("CounterpartyName"));
            setTranSrlNo(json.getString("part_tran_srl_num"));
            setAvlBal(EventHelper.toDouble(json.getString("avl-bal_LCY")));
            setEvtInducer(json.getString("evt_inducer"));
            setTxnValueDate(EventHelper.toTimestamp(json.getString("value-date")));
            setTranPostedDt(EventHelper.toTimestamp(json.getString("pstd-date")));
            setSchemeType(json.getString("schm_type"));
            setDesc(json.getString("tran-particular"));
            setAgentType(json.getString("agent_type"));
            setTranEntryDt(EventHelper.toTimestamp(json.getString("entry_date")));
            setCxCifId(json.getString("cx_cif_i_d"));
            setActionAtHost(json.getString("module_id"));
            setSchemeCode(json.getString("Prod-code"));
            setCounterpartyAccount(json.getString("CounterpartyAccount"));
            setKeys(json.getString("keys"));
            setCustRefId(json.getString("acid"));
            setEventTime(EventHelper.toTimestamp(json.getString("eventts")));
            setBillId(json.getString("ref_num"));
            setRate(EventHelper.toDouble(json.getString("rate")));
            setTxnStatus(json.getString("pstd-flg"));
            setTxnAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setEffAvlBal(EventHelper.toDouble(json.getString("eff_avl_bal")));
            setTranCategory(json.getString("tran_category"));
            setTellerNumber(json.getString("TellerNumber"));
            setInstrumentId(EventHelper.toLong(json.getString("instrmnt_type")));
            setBalanceCur(json.getString("balance_cur"));
            setLedgerBal(EventHelper.toDouble(json.getString("clr_bal_amt")));
            setEmployeeId(json.getString("emp_id"));
            setAcctSolId(json.getString("acct_sol_id"));
            setEventSubtype(json.getString("eventsubtype"));
            setTxnAmtCur(json.getString("tran_crncy_code"));
            setRefCurrency(json.getString("ref_tran_crncy"));
            setCounterpartyBankCode(json.getString("CounterpartyBankCode"));
            setModeOprnCode(json.getString("mode_oprn_code"));
            setMsgName(json.getString("msg_name"));
            setCustId(json.getString("cust-id"));
            setLoanSettlementDate(EventHelper.toDate(json.getString("loan_settlement_Date")));
            setAccountOpendate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setTranDate(EventHelper.toTimestamp(json.getString("tran-date")));
            setHdrmkrs(json.getString("hdrmkrs"));
            setTranBrId(json.getString("TXN_BR-ID"));
            setRemitterName(json.getString("remitter_name"));
            setInstrumentType(json.getString("instrmnt-type"));
            setShadowBal(EventHelper.toDouble(json.getString("un_clr_bal_amt")));
            setRefTranAmt(EventHelper.toDouble(json.getString("ref_tran_amt")));
            setEod(EventHelper.toLong(json.getString("eod")));
            setRemarks(json.getString("tran-rmks"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FLO"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getException(){ return exception; }

    public String getBeneficiaryName(){ return beneficiaryName; }

    public java.sql.Timestamp getZoneDate(){ return zoneDate; }

    public String getFcnrFlag(){ return fcnrFlag; }

    public String getCounterpartyBic(){ return counterpartyBic; }

    public String getBin(){ return bin; }

    public String getChannel(){ return channel; }

    public String getOnlineBatch(){ return onlineBatch; }

    public String getTranSubType(){ return tranSubType; }

    public String getTxnRefType(){ return txnRefType; }

    public java.sql.Timestamp getSystemTime(){ return systemTime; }

    public String getCounterpartyBankAddress(){ return counterpartyBankAddress; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getAcctType(){ return acctType; }

    public String getHostUserId(){ return hostUserId; }

    public String getHostId(){ return hostId; }

    public Double getTranAmt(){ return tranAmt; }

    public String getTxnCode(){ return txnCode; }

    public String getDirectChannelId(){ return directChannelId; }

    public String getCounterpartyBank(){ return counterpartyBank; }

    public String getSequenceNumber(){ return sequenceNumber; }

    public String getCounterpartyBusiness(){ return counterpartyBusiness; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public java.sql.Timestamp getTranVerifiedDt(){ return tranVerifiedDt; }

    public String getPurAcctNum(){ return purAcctNum; }

    public String getCxCustId(){ return cxCustId; }

    public String getEventName(){ return eventName; }

    public String getPayeeId(){ return payeeId; }

    public String getAcctOccpCode(){ return acctOccpCode; }

    public String getAcctStatus(){ return acctStatus; }

    public String getSystemCountry(){ return systemCountry; }

    public String getCounterpartyCountryCode(){ return counterpartyCountryCode; }

    public String getEntityType(){ return entityType; }

    public String getZoneCode(){ return zoneCode; }

    public String getCountryCode(){ return countryCode; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getCounterpartyAddress(){ return counterpartyAddress; }

    public String getMerchantCateg(){ return merchantCateg; }

    public String getInstAlpha(){ return instAlpha; }

    public String getMsgSource(){ return msgSource; }

    public String getPayerIdTemp(){ return payerIdTemp; }

    public String getTerminalIpAddr(){ return terminalIpAddr; }

    public String getCxAcctId(){ return cxAcctId; }

    public String getEntryUser(){ return entryUser; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getAddEntityId1(){ return addEntityId1; }

    public String getAddEntityId4(){ return addEntityId4; }

    public String getCounterpartyCurrencyLcy(){ return counterpartyCurrencyLcy; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getAddEntityId5(){ return addEntityId5; }

    public String getEventType(){ return eventType; }

    public String getAccountOwnership(){ return accountOwnership; }

    public String getCounterpartyAmount(){ return counterpartyAmount; }

    public Double getSanctionAmt(){ return sanctionAmt; }

    public Double getRefCurncy(){ return refCurncy; }

    public String getKycStatus(){ return kycStatus; }

    public String getAgentId(){ return agentId; }

    public String getCounterpartyAmountLcy(){ return counterpartyAmountLcy; }

    public String getTranType(){ return tranType; }

    public String getChannelDesc(){ return channelDesc; }

    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getCreDrPtran(){ return creDrPtran; }

    public String getTxnRefId(){ return txnRefId; }

    public String getSourceBank(){ return sourceBank; }

    public String getPayeeCity(){ return payeeCity; }

    public String getDevOwnerId(){ return devOwnerId; }

    public String getDeviceId(){ return deviceId; }

    public String getTranId(){ return tranId; }

    public String getCounterpartyCurrency(){ return counterpartyCurrency; }

    public String getEntityId(){ return entityId; }

    public String getEventSubType(){ return eventSubType; }

    public String getSourceOrDestAcctId(){ return sourceOrDestAcctId; }

    public Double getRefAmt(){ return refAmt; }

    public String getPlaceHolder(){ return placeHolder; }

    public String getDirectChannelControllerId(){ return directChannelControllerId; }

    public String getPayeeName(){ return payeeName; }

    public String getChannelType(){ return channelType; }

    public String getAcctName(){ return acctName; }

    public String getCustRefType(){ return custRefType; }

    public String getCustCardId(){ return custCardId; }

    public String getCounterpartyName(){ return counterpartyName; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public Double getAvlBal(){ return avlBal; }

    public String getEvtInducer(){ return evtInducer; }

    public java.sql.Timestamp getTxnValueDate(){ return txnValueDate; }

    public java.sql.Timestamp getTranPostedDt(){ return tranPostedDt; }

    public String getSchemeType(){ return schemeType; }

    public String getDesc(){ return desc; }

    public String getAgentType(){ return agentType; }

    public java.sql.Timestamp getTranEntryDt(){ return tranEntryDt; }

    public String getCxCifId(){ return cxCifId; }

    public String getActionAtHost(){ return actionAtHost; }

    public String getSchemeCode(){ return schemeCode; }

    public String getCounterpartyAccount(){ return counterpartyAccount; }

    public String getKeys(){ return keys; }

    public String getCustRefId(){ return custRefId; }

    public java.sql.Timestamp getEventTime(){ return eventTime; }

    public String getBillId(){ return billId; }

    public Double getRate(){ return rate; }

    public String getTxnStatus(){ return txnStatus; }

    public Double getTxnAmt(){ return txnAmt; }

    public Double getEffAvlBal(){ return effAvlBal; }

    public String getTranCategory(){ return tranCategory; }

    public String getTellerNumber(){ return tellerNumber; }

    public Long getInstrumentId(){ return instrumentId; }

    public String getBalanceCur(){ return balanceCur; }

    public Double getLedgerBal(){ return ledgerBal; }

    public String getEmployeeId(){ return employeeId; }

    public String getAcctSolId(){ return acctSolId; }

    public String getEventSubtype(){ return eventSubtype; }

    public String getTxnAmtCur(){ return txnAmtCur; }

    public String getRefCurrency(){ return refCurrency; }

    public String getCounterpartyBankCode(){ return counterpartyBankCode; }

    public String getModeOprnCode(){ return modeOprnCode; }

    public String getMsgName(){ return msgName; }

    public String getCustId(){ return custId; }

    public java.sql.Date getLoanSettlementDate(){ return loanSettlementDate; }

    public java.sql.Timestamp getAccountOpendate(){ return accountOpendate; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getHdrmkrs(){ return hdrmkrs; }

    public String getTranBrId(){ return tranBrId; }

    public String getRemitterName(){ return remitterName; }

    public String getInstrumentType(){ return instrumentType; }

    public Double getShadowBal(){ return shadowBal; }

    public Double getRefTranAmt(){ return refTranAmt; }

    public Long getEod(){ return eod; }

    public String getRemarks(){ return remarks; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setException(String val){ this.exception = val; }
    public void setBeneficiaryName(String val){ this.beneficiaryName = val; }
    public void setZoneDate(java.sql.Timestamp val){ this.zoneDate = val; }
    public void setFcnrFlag(String val){ this.fcnrFlag = val; }
    public void setCounterpartyBic(String val){ this.counterpartyBic = val; }
    public void setBin(String val){ this.bin = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setTxnRefType(String val){ this.txnRefType = val; }
    public void setSystemTime(java.sql.Timestamp val){ this.systemTime = val; }
    public void setCounterpartyBankAddress(String val){ this.counterpartyBankAddress = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setAcctType(String val){ this.acctType = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setTxnCode(String val){ this.txnCode = val; }
    public void setDirectChannelId(String val){ this.directChannelId = val; }
    public void setCounterpartyBank(String val){ this.counterpartyBank = val; }
    public void setSequenceNumber(String val){ this.sequenceNumber = val; }
    public void setCounterpartyBusiness(String val){ this.counterpartyBusiness = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranVerifiedDt(java.sql.Timestamp val){ this.tranVerifiedDt = val; }
    public void setPurAcctNum(String val){ this.purAcctNum = val; }
    public void setCxCustId(String val){ this.cxCustId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setAcctOccpCode(String val){ this.acctOccpCode = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setSystemCountry(String val){ this.systemCountry = val; }
    public void setCounterpartyCountryCode(String val){ this.counterpartyCountryCode = val; }
    public void setEntityType(String val){ this.entityType = val; }
    public void setZoneCode(String val){ this.zoneCode = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setCounterpartyAddress(String val){ this.counterpartyAddress = val; }
    public void setMerchantCateg(String val){ this.merchantCateg = val; }
    public void setInstAlpha(String val){ this.instAlpha = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setPayerIdTemp(String val){ this.payerIdTemp = val; }
    public void setTerminalIpAddr(String val){ this.terminalIpAddr = val; }
    public void setCxAcctId(String val){ this.cxAcctId = val; }
    public void setEntryUser(String val){ this.entryUser = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setAddEntityId4(String val){ this.addEntityId4 = val; }
    public void setCounterpartyCurrencyLcy(String val){ this.counterpartyCurrencyLcy = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setAddEntityId5(String val){ this.addEntityId5 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setAccountOwnership(String val){ this.accountOwnership = val; }
    public void setCounterpartyAmount(String val){ this.counterpartyAmount = val; }
    public void setSanctionAmt(Double val){ this.sanctionAmt = val; }
    public void setRefCurncy(Double val){ this.refCurncy = val; }
    public void setKycStatus(String val){ this.kycStatus = val; }
    public void setAgentId(String val){ this.agentId = val; }
    public void setCounterpartyAmountLcy(String val){ this.counterpartyAmountLcy = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setChannelDesc(String val){ this.channelDesc = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setCreDrPtran(String val){ this.creDrPtran = val; }
    public void setTxnRefId(String val){ this.txnRefId = val; }
    public void setSourceBank(String val){ this.sourceBank = val; }
    public void setPayeeCity(String val){ this.payeeCity = val; }
    public void setDevOwnerId(String val){ this.devOwnerId = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setCounterpartyCurrency(String val){ this.counterpartyCurrency = val; }
    public void setEntityId(String val){ this.entityId = val; }
    public void setEventSubType(String val){ this.eventSubType = val; }
    public void setSourceOrDestAcctId(String val){ this.sourceOrDestAcctId = val; }
    public void setRefAmt(Double val){ this.refAmt = val; }
    public void setPlaceHolder(String val){ this.placeHolder = val; }
    public void setDirectChannelControllerId(String val){ this.directChannelControllerId = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setChannelType(String val){ this.channelType = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustRefType(String val){ this.custRefType = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setCounterpartyName(String val){ this.counterpartyName = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setEvtInducer(String val){ this.evtInducer = val; }
    public void setTxnValueDate(java.sql.Timestamp val){ this.txnValueDate = val; }
    public void setTranPostedDt(java.sql.Timestamp val){ this.tranPostedDt = val; }
    public void setSchemeType(String val){ this.schemeType = val; }
    public void setDesc(String val){ this.desc = val; }
    public void setAgentType(String val){ this.agentType = val; }
    public void setTranEntryDt(java.sql.Timestamp val){ this.tranEntryDt = val; }
    public void setCxCifId(String val){ this.cxCifId = val; }
    public void setActionAtHost(String val){ this.actionAtHost = val; }
    public void setSchemeCode(String val){ this.schemeCode = val; }
    public void setCounterpartyAccount(String val){ this.counterpartyAccount = val; }
    public void setKeys(String val){ this.keys = val; }
    public void setCustRefId(String val){ this.custRefId = val; }
    public void setEventTime(java.sql.Timestamp val){ this.eventTime = val; }
    public void setBillId(String val){ this.billId = val; }
    public void setRate(Double val){ this.rate = val; }
    public void setTxnStatus(String val){ this.txnStatus = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setEffAvlBal(Double val){ this.effAvlBal = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setTellerNumber(String val){ this.tellerNumber = val; }
    public void setInstrumentId(Long val){ this.instrumentId = val; }
    public void setBalanceCur(String val){ this.balanceCur = val; }
    public void setLedgerBal(Double val){ this.ledgerBal = val; }
    public void setEmployeeId(String val){ this.employeeId = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setEventSubtype(String val){ this.eventSubtype = val; }
    public void setTxnAmtCur(String val){ this.txnAmtCur = val; }
    public void setRefCurrency(String val){ this.refCurrency = val; }
    public void setCounterpartyBankCode(String val){ this.counterpartyBankCode = val; }
    public void setModeOprnCode(String val){ this.modeOprnCode = val; }
    public void setMsgName(String val){ this.msgName = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setLoanSettlementDate(java.sql.Date val){ this.loanSettlementDate = val; }
    public void setAccountOpendate(java.sql.Timestamp val){ this.accountOpendate = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setHdrmkrs(String val){ this.hdrmkrs = val; }
    public void setTranBrId(String val){ this.tranBrId = val; }
    public void setRemitterName(String val){ this.remitterName = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setShadowBal(Double val){ this.shadowBal = val; }
    public void setRefTranAmt(Double val){ this.refTranAmt = val; }
    public void setEod(Long val){ this.eod = val; }
    public void setRemarks(String val){ this.remarks = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_LoanTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.custRefId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        String transactionKey= h.getCxKeyGivenHostKey(WorkspaceName.TRANSACTION, getHostId(), this.txnRefId);
        wsInfoSet.add(new WorkspaceInfo("Transaction", transactionKey));
        String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.agentId);
        wsInfoSet.add(new WorkspaceInfo("User", userKey));
        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_LoanTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "LoanTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("country_code",getCountryCode());
        json.put("acct_open_date",getAcctOpenDate());
        json.put("dev_owner_id",getDevOwnerId());
        json.put("cust_card_id",getCustCardId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}