/**
 * Created By : Lalit
 * Created On : 5/26/17
 * Organisation: CustomerXPs Software Private Ltd.
 */

import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {SharedService} from "./cc.shared.service";

@Injectable()
export class GenesisService {

    SharedBase = SharedService.baseUrl + '/user/menus/EFM';

    constructor(private http : Http){
    }

    getMenu() : Observable<any>{
        return this.http.get(this.SharedBase)
            .map(resp => resp.json())
            .catch(err => Observable.throw(err));
    }

    loginValidate() : Observable<any>{
        return this.http.get(this.SharedBase)
            .map((resp) => {// Ignore as User is logged In
            }).catch((error: any) => {
            return Observable.throw(error);
            });
    }

    logout() : Observable<any>{
        return this.http.get(SharedService.baseUrl + "/cc-login/logout")
            .map(SharedService.extractText)
            .catch(SharedService.handleError);
    }

    getMyInfo() : Observable<any>{
        return this.http.get(SharedService.baseUrl + "/user/my-info")
            .map(SharedService.extractJson)
            .catch(SharedService.handleError);
    }

    checkUrl(url : string) : Observable<any> {
        return this.http.get(url)
            .catch(SharedService.handleError);
    }
}
