cxps.events.event.ft-gold-txn {
  table-name : EVENT_FT_GOLDTXN
  event-mnemonic: FG
  workspaces : {
    NONCUSTOMER: nic
  }
  event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    nic: {db : true ,raw_name : nic ,type : "string:20"}
    host-id : {db : true ,raw_name : host-id ,type : "string:20"}
    channel : {db : true ,raw_name : channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}  
    cust-id : {db : true ,raw_name : cust_id ,type : "string:20"}
    txn-id : {db : true ,raw_name : tran_id ,type : "string:20"}
    txn-date : {db : true ,raw_name : tran_date ,type : timestamp}
    txn-amt : {db : true ,raw_name : tran_amt ,type : "number:11,2"}
    txn-curr : {db : true ,raw_name : tran_curr ,type : "string:20"}
    Ref-amt : {db : true ,raw_name : LCY_TRAN_AMT,type : "number:11,2"}
    Ref-Curncy : {db : true ,raw_name : LCY_TRAN_CURR,type : "number:11,2"}
    txn-code : {db : true ,raw_name : tran_code ,type : "string:20"}
    remarks : {db : true ,raw_name : tran_rmks ,type : "string:20"}
    txn-dr-cr : {db : true ,raw_name : part_tran_type ,type : "string:20"}
    zone-date : {db : true ,raw_name : zone_date ,type : timestamp}
    zone-code : {db : true ,raw_name : zone_code ,type : "string:20"}
    tran-srl-no : {db : true ,raw_name : part_tran_srl_num ,type : "string:20"}
    tran-type : {db : true ,raw_name : tran_type ,type : "string:20"}
    tran-sub-type : {db : true ,raw_name : tran_sub_type ,type : "string:20"}
    system-time : {db : true ,raw_name : sys_time ,type : timestamp}
    event-time : {db : true ,raw_name : eventts ,type : timestamp}
    msg-source : {db : true ,raw_name : source ,type : "string:20"} 
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}	
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {raw_name : reservedfield4 ,type : "string:100"}
    add-entity-id5 : {raw_name : reservedfield5 ,type : "string:100"}
    }
}
