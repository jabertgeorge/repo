package clari5.custom.boc.integration.data;

public class FT_Remittance extends ITableData {

    private String tableName = "FT_REMITTANCE_TXN";
    private String event_type = "FT_Remittance";

    private String lcy_curr;
    private String agent_name;
    private String year;
    private String channel;
    private String acct_type;
    private String source;
    private String branch;
    private String vessel_number;
    private String rem_bank_branch;
    private String rem_acct_no;
    private String user_type;
    private String ben_name;
    private String reservedfield1;
    private String rem_name;
    private String rem_cntry_code;
    private String agent_code;
    private String ben_cntry_code;
    private String cx_acct_id;
    private String swift_code;
    private String joint_acct_name;
    private String purpose_desc;
    private String swift_message_type;
    private String reservedfield2;
    private String rem_bank_code;
    private String reservedfield3;
    private String reservedfield4;
    private String reservedfield5;
    private String server_id;
    private String vessel_name;
    private String benf_bank;
    private String system;
    private String rem_type;
    private String rem_add3;
    private String rem_add2;
    private String port;
    private String rem_add1;
    private String facility_number;
    private String acct_curr;
    private String event_name;
    private String eventsubtype;
    private String usd_eqv_amt;
    private String ben_city;
    private String eventtype;
    private String cust_id;
    private String core_cust_id;
    private String tran_ref_no;
    private String ben_add2;
    private String temp_ref_no;
    private String ben_add1;
    private String ben_add3;
    private String keys;
    private String systemcountry;
    private String benf_id_no;
    private String type_of_goods;
    private String SYNC_STATUS;
    private String exec_customer;
    private String swift_transaction;
    private String sys_time;
    private String client_acc_no;
    private String acct_no_ws_key;
    private String service_code;
    private String purpose_code;
    private String rembank;
    private String benf_bank_branch;
    private String rem_bic;
    private String channel_type;
    private String rem_cust_id;
    private String acct_name;
    private String lcy_amount;
    private String acct_email;
    private String transaction_code;
    private String bank_code;
    private String cpty_ac_no;
    private String acct_branch;
    private String tran_type;
    private String tran_curr;
    private String host_user_id;
    private String rem_id_no;
    private String ben_cust_id;
    private String ben_acct_no;
    private String account_catagory;
    private String eventts;
    private String host_id;
    private String isrtgs;
    private String trn_date;
    private String event_id;
    private String account_id;
    private String rem_city;
    private String sno;
    private String cx_cust_id;
    private String benf_id_type;
    private String acct_addr;
    private String tran_amt;
    private String core_account_id;
    private String ben_bic;
    private String fcytranamt;
    private String fcytrancur;
    private String tran_id;

    public String getFcytrancur() {
        return fcytrancur;
    }

    public void setFcytrancur(String fcytrancur) {
        this.fcytrancur = fcytrancur;
    }

    public String getFcytranamt() {
        return fcytranamt;
    }

    public void setFcytranamt(String fcytranamt) {
        this.fcytranamt = fcytranamt;
    }



    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }

  //  public String getFcyTranAmt() {
   //     return fcyTranAmt;
   // }

   // public void setFcyTranAmt(String fcyTranAmt) {
   //     this.fcyTranAmt = fcyTranAmt;
  //  }

   // public String getFcyTranCur() {
  //      return fcyTranCur;
   // }

   // public void setFcyTranCur(String fcyTranCur) {
   //     this.fcyTranCur = fcyTranCur;
  //  }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getLcy_curr() {
        return lcy_curr;
    }

    public void setLcy_curr(String lcy_curr) {
        this.lcy_curr = lcy_curr;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAcct_type() {
        return acct_type;
    }

    public void setAcct_type(String acct_type) {
        this.acct_type = acct_type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getVessel_number() {
        return vessel_number;
    }

    public void setVessel_number(String vessel_number) {
        this.vessel_number = vessel_number;
    }

    public String getRem_bank_branch() {
        return rem_bank_branch;
    }

    public void setRem_bank_branch(String rem_bank_branch) {
        this.rem_bank_branch = rem_bank_branch;
    }

    public String getRem_acct_no() {
        return rem_acct_no;
    }

    public void setRem_acct_no(String rem_acct_no) {
        this.rem_acct_no = rem_acct_no;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getBen_name() {
        return ben_name;
    }

    public void setBen_name(String ben_name) {
        this.ben_name = ben_name;
    }

    public String getReservedfield1() {
        return reservedfield1;
    }

    public void setReservedfield1(String reservedfield1) {
        this.reservedfield1 = reservedfield1;
    }

    public String getRem_name() {
        return rem_name;
    }

    public void setRem_name(String rem_name) {
        this.rem_name = rem_name;
    }

    public String getRem_cntry_code() {
        return rem_cntry_code;
    }

    public void setRem_cntry_code(String rem_cntry_code) {
        this.rem_cntry_code = rem_cntry_code;
    }

    public String getAgent_code() {
        return agent_code;
    }

    public void setAgent_code(String agent_code) {
        this.agent_code = agent_code;
    }

    public String getBen_cntry_code() {
        return ben_cntry_code;
    }

    public void setBen_cntry_code(String ben_cntry_code) {
        this.ben_cntry_code = ben_cntry_code;
    }

    public String getCx_acct_id() {
        return cx_acct_id;
    }

    public void setCx_acct_id(String cx_acct_id) {
        this.cx_acct_id = cx_acct_id;
    }

    public String getSwift_code() {
        return swift_code;
    }

    public void setSwift_code(String swift_code) {
        this.swift_code = swift_code;
    }

    public String getJoint_acct_name() {
        return joint_acct_name;
    }

    public void setJoint_acct_name(String joint_acct_name) {
        this.joint_acct_name = joint_acct_name;
    }

    public String getPurpose_desc() {
        return purpose_desc;
    }

    public void setPurpose_desc(String purpose_desc) {
        this.purpose_desc = purpose_desc;
    }

    public String getSwift_message_type() {
        return swift_message_type;
    }

    public void setSwift_message_type(String swift_message_type) {
        this.swift_message_type = swift_message_type;
    }

    public String getReservedfield2() {
        return reservedfield2;
    }

    public void setReservedfield2(String reservedfield2) {
        this.reservedfield2 = reservedfield2;
    }

    public String getRem_bank_code() {
        return rem_bank_code;
    }

    public void setRem_bank_code(String rem_bank_code) {
        this.rem_bank_code = rem_bank_code;
    }

    public String getReservedfield3() {
        return reservedfield3;
    }

    public void setReservedfield3(String reservedfield3) {
        this.reservedfield3 = reservedfield3;
    }

    public String getReservedfield4() {
        return reservedfield4;
    }

    public void setReservedfield4(String reservedfield4) {
        this.reservedfield4 = reservedfield4;
    }

    public String getReservedfield5() {
        return reservedfield5;
    }

    public void setReservedfield5(String reservedfield5) {
        this.reservedfield5 = reservedfield5;
    }

    public String getServer_id() {
        return server_id;
    }

    public void setServer_id(String server_id) {
        this.server_id = server_id;
    }

    public String getVessel_name() {
        return vessel_name;
    }

    public void setVessel_name(String vessel_name) {
        this.vessel_name = vessel_name;
    }

    public String getBenf_bank() {
        return benf_bank;
    }

    public void setBenf_bank(String benf_bank) {
        this.benf_bank = benf_bank;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getRem_type() {
        return rem_type;
    }

    public void setRem_type(String rem_type) {
        this.rem_type = rem_type;
    }

    public String getRem_add3() {
        return rem_add3;
    }

    public void setRem_add3(String rem_add3) {
        this.rem_add3 = rem_add3;
    }

    public String getRem_add2() {
        return rem_add2;
    }

    public void setRem_add2(String rem_add2) {
        this.rem_add2 = rem_add2;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getRem_add1() {
        return rem_add1;
    }

    public void setRem_add1(String rem_add1) {
        this.rem_add1 = rem_add1;
    }

    public String getFacility_number() {
        return facility_number;
    }

    public void setFacility_number(String facility_number) {
        this.facility_number = facility_number;
    }

    public String getAcct_curr() {
        return acct_curr;
    }

    public void setAcct_curr(String acct_curr) {
        this.acct_curr = acct_curr;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEventsubtype() {
        return eventsubtype;
    }

    public void setEventsubtype(String eventsubtype) {
        this.eventsubtype = eventsubtype;
    }

    public String getUsd_eqv_amt() {
        return usd_eqv_amt;
    }

    public void setUsd_eqv_amt(String usd_eqv_amt) {
        this.usd_eqv_amt = usd_eqv_amt;
    }

    public String getBen_city() {
        return ben_city;
    }

    public void setBen_city(String ben_city) {
        this.ben_city = ben_city;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getCore_cust_id() {
        return core_cust_id;
    }

    public void setCore_cust_id(String core_cust_id) {
        this.core_cust_id = core_cust_id;
    }

    public String getTran_ref_no() {
        return tran_ref_no;
    }

    public void setTran_ref_no(String tran_ref_no) {
        this.tran_ref_no = tran_ref_no;
    }

    public String getBen_add2() {
        return ben_add2;
    }

    public void setBen_add2(String ben_add2) {
        this.ben_add2 = ben_add2;
    }

    public String getTemp_ref_no() {
        return temp_ref_no;
    }

    public void setTemp_ref_no(String temp_ref_no) {
        this.temp_ref_no = temp_ref_no;
    }

    public String getBen_add1() {
        return ben_add1;
    }

    public void setBen_add1(String ben_add1) {
        this.ben_add1 = ben_add1;
    }

    public String getBen_add3() {
        return ben_add3;
    }

    public void setBen_add3(String ben_add3) {
        this.ben_add3 = ben_add3;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getBenf_id_no() {
        return benf_id_no;
    }

    public void setBenf_id_no(String benf_id_no) {
        this.benf_id_no = benf_id_no;
    }

    public String getType_of_goods() {
        return type_of_goods;
    }

    public void setType_of_goods(String type_of_goods) {
        this.type_of_goods = type_of_goods;
    }

    public String getSYNC_STATUS() {
        return SYNC_STATUS;
    }

    public void setSYNC_STATUS(String SYNC_STATUS) {
        this.SYNC_STATUS = SYNC_STATUS;
    }

    public String getExec_customer() {
        return exec_customer;
    }

    public void setExec_customer(String exec_customer) {
        this.exec_customer = exec_customer;
    }

    public String getSwift_transaction() {
        return swift_transaction;
    }

    public void setSwift_transaction(String swift_transaction) {
        this.swift_transaction = swift_transaction;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getClient_acc_no() {
        return client_acc_no;
    }

    public void setClient_acc_no(String client_acc_no) {
        this.client_acc_no = client_acc_no;
    }

    public String getAcct_no_ws_key() {
        return acct_no_ws_key;
    }

    public void setAcct_no_ws_key(String acct_no_ws_key) {
        this.acct_no_ws_key = acct_no_ws_key;
    }

    public String getService_code() {
        return service_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    public String getPurpose_code() {
        return purpose_code;
    }

    public void setPurpose_code(String purpose_code) {
        this.purpose_code = purpose_code;
    }

    public String getRembank() {
        return rembank;
    }

    public void setRembank(String rembank) {
        this.rembank = rembank;
    }

    public String getBenf_bank_branch() {
        return benf_bank_branch;
    }

    public void setBenf_bank_branch(String benf_bank_branch) {
        this.benf_bank_branch = benf_bank_branch;
    }

    public String getRem_bic() {
        return rem_bic;
    }

    public void setRem_bic(String rem_bic) {
        this.rem_bic = rem_bic;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getRem_cust_id() {
        return rem_cust_id;
    }

    public void setRem_cust_id(String rem_cust_id) {
        this.rem_cust_id = rem_cust_id;
    }

    public String getAcct_name() {
        return acct_name;
    }

    public void setAcct_name(String acct_name) {
        this.acct_name = acct_name;
    }

    public String getLcy_amount() {
        return lcy_amount;
    }

    public void setLcy_amount(String lcy_amount) {
        this.lcy_amount = lcy_amount;
    }

    public String getAcct_email() {
        return acct_email;
    }

    public void setAcct_email(String acct_email) {
        this.acct_email = acct_email;
    }

    public String getTransaction_code() {
        return transaction_code;
    }

    public void setTransaction_code(String transaction_code) {
        this.transaction_code = transaction_code;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getCpty_ac_no() {
        return cpty_ac_no;
    }

    public void setCpty_ac_no(String cpty_ac_no) {
        this.cpty_ac_no = cpty_ac_no;
    }

    public String getAcct_branch() {
        return acct_branch;
    }

    public void setAcct_branch(String acct_branch) {
        this.acct_branch = acct_branch;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_curr() {
        return tran_curr;
    }

    public void setTran_curr(String tran_curr) {
        this.tran_curr = tran_curr;
    }

    public String getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(String host_user_id) {
        this.host_user_id = host_user_id;
    }

    public String getRem_id_no() {
        return rem_id_no;
    }

    public void setRem_id_no(String rem_id_no) {
        this.rem_id_no = rem_id_no;
    }

    public String getBen_cust_id() {
        return ben_cust_id;
    }

    public void setBen_cust_id(String ben_cust_id) {
        this.ben_cust_id = ben_cust_id;
    }

    public String getBen_acct_no() {
        return ben_acct_no;
    }

    public void setBen_acct_no(String ben_acct_no) {
        this.ben_acct_no = ben_acct_no;
    }

    public String getAccount_catagory() {
        return account_catagory;
    }

    public void setAccount_catagory(String account_catagory) {
        this.account_catagory = account_catagory;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getIsrtgs() {
        return isrtgs;
    }

    public void setIsrtgs(String isrtgs) {
        this.isrtgs = isrtgs;
    }

    public String getTrn_date() {
        return trn_date;
    }

    public void setTrn_date(String trn_date) {
        this.trn_date = trn_date;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getRem_city() {
        return rem_city;
    }

    public void setRem_city(String rem_city) {
        this.rem_city = rem_city;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getCx_cust_id() {
        return cx_cust_id;
    }

    public void setCx_cust_id(String cx_cust_id) {
        this.cx_cust_id = cx_cust_id;
    }

    public String getBenf_id_type() {
        return benf_id_type;
    }

    public void setBenf_id_type(String benf_id_type) {
        this.benf_id_type = benf_id_type;
    }

    public String getAcct_addr() {
        return acct_addr;
    }

    public void setAcct_addr(String acct_addr) {
        this.acct_addr = acct_addr;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public void setTran_amt(String tran_amt) {
        this.tran_amt = tran_amt;
    }

    public String getCore_account_id() {
        return core_account_id;
    }

    public void setCore_account_id(String core_account_id) {
        this.core_account_id = core_account_id;
    }

    public String getBen_bic() {
        return ben_bic;
    }

    public void setBen_bic(String ben_bic) {
        this.ben_bic = ben_bic;
    }

    @Override
    public String getTableName() {
        return tableName;
    }
}
