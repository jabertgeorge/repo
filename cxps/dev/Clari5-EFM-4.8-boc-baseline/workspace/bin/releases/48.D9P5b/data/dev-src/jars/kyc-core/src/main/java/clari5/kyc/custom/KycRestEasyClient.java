package clari5.kyc.custom;


import clari5.kyc.custom.rest.impl.KycServiceImpl;
import clari5.platform.applayer.Clari5;
import clari5.platform.fileq.Clari5Reader;
import clari5.platform.util.Hocon;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

import javax.servlet.ServletException;
import java.io.IOException;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class KycRestEasyClient extends Application {
    private Set<Object> singletons = new HashSet<Object>();

    public KycRestEasyClient() {
        singletons.add(new KycServiceImpl());
        singletons.add(new KycExceptionMapper());
    }



    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
