package clari5.custom.dev;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import cxps.customEventHelper.CustomWsKeyDerivator;
import cxps.events.FT_RemittanceEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PreProcessor {

    static CxpsLogger logger = CxpsLogger.getLogger(PreProcessor.class);

    public static CxConnection getConnection() {
        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("Connection object is NULL");
        return rdbms.getConnection();
    }

    public static String getCustIdForRemmittanceEvent(FT_RemittanceEvent event) {
        try {
            if (event.system.equalsIgnoreCase("SR")) {
                String acctId = CustomWsKeyDerivator.accountKeyForRemmitance(event.getRemType(), event.getRemAcctNo(), event.getBenAcctNo());
                CxKeyHelper h = Hfdb.getCxKeyHelper();
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, event.getHostId(), acctId);
                event.setCxAcctId(accountKey);
                event.setFcyTranAmt(event.getTranAmt());
                event.setTranAmt(event.getLcyAmt());
                CxConnection connection = getConnection();
                PreparedStatement ps = null;
                ResultSet rs = null;
                String custId = null;
                String sql = "select acctCustID from dda where acctID = '" + accountKey + "'";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    custId = rs.getString("acctCustID");
                }
                event.setCustId(custId.substring(4));
                logger.debug("data in cx-cust-id is -------------------> " + custId + " cx-acct-id---------------> " + event.getCxAcctId());
                return null != custId ? custId : "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
