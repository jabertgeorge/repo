// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_RemittanceEventMapper extends EventMapper<FT_RemittanceEvent> {

public FT_RemittanceEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_RemittanceEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_RemittanceEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_RemittanceEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_RemittanceEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_RemittanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_RemittanceEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getUpdatedOn());
            preparedStatement.setString(i++, obj.getRemBankCode());
            preparedStatement.setString(i++, obj.getYear());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getAcctType());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setString(i++, obj.getBenCity());
            preparedStatement.setString(i++, obj.getBranch());
            preparedStatement.setString(i++, obj.getBenCntryCode());
            preparedStatement.setString(i++, obj.getAgentCode());
            preparedStatement.setDouble(i++, obj.getVesselNumber());
            preparedStatement.setDouble(i++, obj.getLcyAmt());
            preparedStatement.setString(i++, obj.getAgentName());
            preparedStatement.setString(i++, obj.getLcyCurr());
            preparedStatement.setString(i++, obj.getRemName());
            preparedStatement.setString(i++, obj.getBenBic());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getPurposDesc());
            preparedStatement.setString(i++, obj.getBenfBank());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getSwiftCode());
            preparedStatement.setString(i++, obj.getJointAcctName());
            preparedStatement.setString(i++, obj.getVesselName());
            preparedStatement.setString(i++, obj.getChannelType());
            preparedStatement.setString(i++, obj.getBenAcctNo());
            preparedStatement.setString(i++, obj.getBenCustId());
            preparedStatement.setString(i++, obj.getAcctNoWsKey());
            preparedStatement.setString(i++, obj.getSystem());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getUserTyp());
            preparedStatement.setTimestamp(i++, obj.getCreatedOn());
            preparedStatement.setString(i++, obj.getRemBnkBranch());
            preparedStatement.setString(i++, obj.getPort());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getAcctCurr());
            preparedStatement.setString(i++, obj.getRemBic());
            preparedStatement.setString(i++, obj.getClientAccNo());
            preparedStatement.setDouble(i++, obj.getCoreCustId());
            preparedStatement.setString(i++, obj.getSystemCountry());
            preparedStatement.setString(i++, obj.getFcyTranCur());
            preparedStatement.setString(i++, obj.getRemAdd1());
            preparedStatement.setString(i++, obj.getRemType());
            preparedStatement.setString(i++, obj.getRemAdd3());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getRemAdd2());
            preparedStatement.setTimestamp(i++, obj.getTrnDate());
            preparedStatement.setString(i++, obj.getSwiftTxn());
            preparedStatement.setString(i++, obj.getRemBank());
            preparedStatement.setString(i++, obj.getTypeOfGoods());
            preparedStatement.setString(i++, obj.getBenAdd3());
            preparedStatement.setDouble(i++, obj.getCoreAcctId());
            preparedStatement.setDouble(i++, obj.getAcctCategory());
            preparedStatement.setString(i++, obj.getTempRefNo());
            preparedStatement.setString(i++, obj.getBenAdd2());
            preparedStatement.setString(i++, obj.getRemAcctNo());
            preparedStatement.setString(i++, obj.getSwiftMsgType());
            preparedStatement.setString(i++, obj.getRemIdNo());
            preparedStatement.setString(i++, obj.getBenAdd1());
            preparedStatement.setDouble(i++, obj.getFcyTranAmt());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setDouble(i++, obj.getFacilityNum());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getTranRefNo());
            preparedStatement.setString(i++, obj.getAcctEmail());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getRemCustId());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAcctBranch());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getTxnType());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getBenName());
            preparedStatement.setString(i++, obj.getCptyAcctNo());
            preparedStatement.setString(i++, obj.getServicCode());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getPurposCod());
            preparedStatement.setString(i++, obj.getIsrtgs());
            preparedStatement.setDouble(i++, obj.getUsdEqvAmt());
            preparedStatement.setString(i++, obj.getBankCode());
            preparedStatement.setString(i++, obj.getRemCntryCode());
            preparedStatement.setString(i++, obj.getBenfBnkBranch());
            preparedStatement.setDouble(i++, obj.getSno());
            preparedStatement.setString(i++, obj.getExecCust());
            preparedStatement.setString(i++, obj.getTranCurr());
            preparedStatement.setString(i++, obj.getBenfIdNo());
            preparedStatement.setString(i++, obj.getAcctAddr());
            preparedStatement.setString(i++, obj.getRemCity());
            preparedStatement.setString(i++, obj.getBenfIdType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_REMITTANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_RemittanceEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_RemittanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_REMITTANCE"));
        putList = new ArrayList<>();

        for (FT_RemittanceEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "UPDATED_ON", String.valueOf(obj.getUpdatedOn()));
            p = this.insert(p, "REM_BANK_CODE",  obj.getRemBankCode());
            p = this.insert(p, "YEAR",  obj.getYear());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "ACCT_TYPE",  obj.getAcctType());
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "BEN_CITY",  obj.getBenCity());
            p = this.insert(p, "BRANCH",  obj.getBranch());
            p = this.insert(p, "BEN_CNTRY_CODE",  obj.getBenCntryCode());
            p = this.insert(p, "AGENT_CODE",  obj.getAgentCode());
            p = this.insert(p, "VESSEL_NUMBER", String.valueOf(obj.getVesselNumber()));
            p = this.insert(p, "LCY_AMT", String.valueOf(obj.getLcyAmt()));
            p = this.insert(p, "AGENT_NAME",  obj.getAgentName());
            p = this.insert(p, "LCY_CURR",  obj.getLcyCurr());
            p = this.insert(p, "REM_NAME",  obj.getRemName());
            p = this.insert(p, "BEN_BIC",  obj.getBenBic());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "PURPOS_DESC",  obj.getPurposDesc());
            p = this.insert(p, "BENF_BANK",  obj.getBenfBank());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "SWIFT_CODE",  obj.getSwiftCode());
            p = this.insert(p, "JOINT_ACCT_NAME",  obj.getJointAcctName());
            p = this.insert(p, "VESSEL_NAME",  obj.getVesselName());
            p = this.insert(p, "CHANNEL_TYPE",  obj.getChannelType());
            p = this.insert(p, "BEN_ACCT_NO",  obj.getBenAcctNo());
            p = this.insert(p, "BEN_CUST_ID",  obj.getBenCustId());
            p = this.insert(p, "ACCT_NO_WS_KEY",  obj.getAcctNoWsKey());
            p = this.insert(p, "SYSTEM",  obj.getSystem());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "USER_TYP",  obj.getUserTyp());
            p = this.insert(p, "CREATED_ON", String.valueOf(obj.getCreatedOn()));
            p = this.insert(p, "REM_BNK_BRANCH",  obj.getRemBnkBranch());
            p = this.insert(p, "PORT",  obj.getPort());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "ACCT_CURR",  obj.getAcctCurr());
            p = this.insert(p, "REM_BIC",  obj.getRemBic());
            p = this.insert(p, "CLIENT_ACC_NO",  obj.getClientAccNo());
            p = this.insert(p, "CORE_CUST_ID", String.valueOf(obj.getCoreCustId()));
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            p = this.insert(p, "FCY_TRAN_CUR",  obj.getFcyTranCur());
            p = this.insert(p, "REM_ADD1",  obj.getRemAdd1());
            p = this.insert(p, "REM_TYPE",  obj.getRemType());
            p = this.insert(p, "REM_ADD3",  obj.getRemAdd3());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "REM_ADD2",  obj.getRemAdd2());
            p = this.insert(p, "TRN_DATE", String.valueOf(obj.getTrnDate()));
            p = this.insert(p, "SWIFT_TXN",  obj.getSwiftTxn());
            p = this.insert(p, "REM_BANK",  obj.getRemBank());
            p = this.insert(p, "TYPE_OF_GOODS",  obj.getTypeOfGoods());
            p = this.insert(p, "BEN_ADD3",  obj.getBenAdd3());
            p = this.insert(p, "CORE_ACCT_ID", String.valueOf(obj.getCoreAcctId()));
            p = this.insert(p, "ACCT_CATEGORY", String.valueOf(obj.getAcctCategory()));
            p = this.insert(p, "TEMP_REF_NO",  obj.getTempRefNo());
            p = this.insert(p, "BEN_ADD2",  obj.getBenAdd2());
            p = this.insert(p, "REM_ACCT_NO",  obj.getRemAcctNo());
            p = this.insert(p, "SWIFT_MSG_TYPE",  obj.getSwiftMsgType());
            p = this.insert(p, "REM_ID_NO",  obj.getRemIdNo());
            p = this.insert(p, "BEN_ADD1",  obj.getBenAdd1());
            p = this.insert(p, "FCY_TRAN_AMT", String.valueOf(obj.getFcyTranAmt()));
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "FACILITY_NUM", String.valueOf(obj.getFacilityNum()));
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "TRAN_REF_NO",  obj.getTranRefNo());
            p = this.insert(p, "ACCT_EMAIL",  obj.getAcctEmail());
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "REM_CUST_ID",  obj.getRemCustId());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ACCT_BRANCH",  obj.getAcctBranch());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "TXN_TYPE",  obj.getTxnType());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "BEN_NAME",  obj.getBenName());
            p = this.insert(p, "CPTY_ACCT_NO",  obj.getCptyAcctNo());
            p = this.insert(p, "SERVIC_CODE",  obj.getServicCode());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "PURPOS_COD",  obj.getPurposCod());
            p = this.insert(p, "ISRTGS",  obj.getIsrtgs());
            p = this.insert(p, "USD_EQV_AMT", String.valueOf(obj.getUsdEqvAmt()));
            p = this.insert(p, "BANK_CODE",  obj.getBankCode());
            p = this.insert(p, "REM_CNTRY_CODE",  obj.getRemCntryCode());
            p = this.insert(p, "BENF_BNK_BRANCH",  obj.getBenfBnkBranch());
            p = this.insert(p, "SNO", String.valueOf(obj.getSno()));
            p = this.insert(p, "EXEC_CUST",  obj.getExecCust());
            p = this.insert(p, "TRAN_CURR",  obj.getTranCurr());
            p = this.insert(p, "BENF_ID_NO",  obj.getBenfIdNo());
            p = this.insert(p, "ACCT_ADDR",  obj.getAcctAddr());
            p = this.insert(p, "REM_CITY",  obj.getRemCity());
            p = this.insert(p, "BENF_ID_TYPE",  obj.getBenfIdType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_REMITTANCE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_REMITTANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_REMITTANCE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_RemittanceEvent obj = new FT_RemittanceEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setUpdatedOn(rs.getTimestamp("UPDATED_ON"));
    obj.setRemBankCode(rs.getString("REM_BANK_CODE"));
    obj.setYear(rs.getString("YEAR"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setAcctType(rs.getString("ACCT_TYPE"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setBenCity(rs.getString("BEN_CITY"));
    obj.setBranch(rs.getString("BRANCH"));
    obj.setBenCntryCode(rs.getString("BEN_CNTRY_CODE"));
    obj.setAgentCode(rs.getString("AGENT_CODE"));
    obj.setVesselNumber(rs.getDouble("VESSEL_NUMBER"));
    obj.setLcyAmt(rs.getDouble("LCY_AMT"));
    obj.setAgentName(rs.getString("AGENT_NAME"));
    obj.setLcyCurr(rs.getString("LCY_CURR"));
    obj.setRemName(rs.getString("REM_NAME"));
    obj.setBenBic(rs.getString("BEN_BIC"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setPurposDesc(rs.getString("PURPOS_DESC"));
    obj.setBenfBank(rs.getString("BENF_BANK"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setSwiftCode(rs.getString("SWIFT_CODE"));
    obj.setJointAcctName(rs.getString("JOINT_ACCT_NAME"));
    obj.setVesselName(rs.getString("VESSEL_NAME"));
    obj.setChannelType(rs.getString("CHANNEL_TYPE"));
    obj.setBenAcctNo(rs.getString("BEN_ACCT_NO"));
    obj.setBenCustId(rs.getString("BEN_CUST_ID"));
    obj.setAcctNoWsKey(rs.getString("ACCT_NO_WS_KEY"));
    obj.setSystem(rs.getString("SYSTEM"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setUserTyp(rs.getString("USER_TYP"));
    obj.setCreatedOn(rs.getTimestamp("CREATED_ON"));
    obj.setRemBnkBranch(rs.getString("REM_BNK_BRANCH"));
    obj.setPort(rs.getString("PORT"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setAcctCurr(rs.getString("ACCT_CURR"));
    obj.setRemBic(rs.getString("REM_BIC"));
    obj.setClientAccNo(rs.getString("CLIENT_ACC_NO"));
    obj.setCoreCustId(rs.getDouble("CORE_CUST_ID"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));
    obj.setFcyTranCur(rs.getString("FCY_TRAN_CUR"));
    obj.setRemAdd1(rs.getString("REM_ADD1"));
    obj.setRemType(rs.getString("REM_TYPE"));
    obj.setRemAdd3(rs.getString("REM_ADD3"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setRemAdd2(rs.getString("REM_ADD2"));
    obj.setTrnDate(rs.getTimestamp("TRN_DATE"));
    obj.setSwiftTxn(rs.getString("SWIFT_TXN"));
    obj.setRemBank(rs.getString("REM_BANK"));
    obj.setTypeOfGoods(rs.getString("TYPE_OF_GOODS"));
    obj.setBenAdd3(rs.getString("BEN_ADD3"));
    obj.setCoreAcctId(rs.getDouble("CORE_ACCT_ID"));
    obj.setAcctCategory(rs.getDouble("ACCT_CATEGORY"));
    obj.setTempRefNo(rs.getString("TEMP_REF_NO"));
    obj.setBenAdd2(rs.getString("BEN_ADD2"));
    obj.setRemAcctNo(rs.getString("REM_ACCT_NO"));
    obj.setSwiftMsgType(rs.getString("SWIFT_MSG_TYPE"));
    obj.setRemIdNo(rs.getString("REM_ID_NO"));
    obj.setBenAdd1(rs.getString("BEN_ADD1"));
    obj.setFcyTranAmt(rs.getDouble("FCY_TRAN_AMT"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setFacilityNum(rs.getDouble("FACILITY_NUM"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setTranRefNo(rs.getString("TRAN_REF_NO"));
    obj.setAcctEmail(rs.getString("ACCT_EMAIL"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setRemCustId(rs.getString("REM_CUST_ID"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAcctBranch(rs.getString("ACCT_BRANCH"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setTxnType(rs.getString("TXN_TYPE"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setBenName(rs.getString("BEN_NAME"));
    obj.setCptyAcctNo(rs.getString("CPTY_ACCT_NO"));
    obj.setServicCode(rs.getString("SERVIC_CODE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setPurposCod(rs.getString("PURPOS_COD"));
    obj.setIsrtgs(rs.getString("ISRTGS"));
    obj.setUsdEqvAmt(rs.getDouble("USD_EQV_AMT"));
    obj.setBankCode(rs.getString("BANK_CODE"));
    obj.setRemCntryCode(rs.getString("REM_CNTRY_CODE"));
    obj.setBenfBnkBranch(rs.getString("BENF_BNK_BRANCH"));
    obj.setSno(rs.getDouble("SNO"));
    obj.setExecCust(rs.getString("EXEC_CUST"));
    obj.setTranCurr(rs.getString("TRAN_CURR"));
    obj.setBenfIdNo(rs.getString("BENF_ID_NO"));
    obj.setAcctAddr(rs.getString("ACCT_ADDR"));
    obj.setRemCity(rs.getString("REM_CITY"));
    obj.setBenfIdType(rs.getString("BENF_ID_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_REMITTANCE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_RemittanceEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_RemittanceEvent> events;
 FT_RemittanceEvent obj = new FT_RemittanceEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_REMITTANCE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_RemittanceEvent obj = new FT_RemittanceEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setUpdatedOn(EventHelper.toTimestamp(getColumnValue(rs, "UPDATED_ON")));
            obj.setRemBankCode(getColumnValue(rs, "REM_BANK_CODE"));
            obj.setYear(getColumnValue(rs, "YEAR"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setAcctType(getColumnValue(rs, "ACCT_TYPE"));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setBenCity(getColumnValue(rs, "BEN_CITY"));
            obj.setBranch(getColumnValue(rs, "BRANCH"));
            obj.setBenCntryCode(getColumnValue(rs, "BEN_CNTRY_CODE"));
            obj.setAgentCode(getColumnValue(rs, "AGENT_CODE"));
            obj.setVesselNumber(EventHelper.toDouble(getColumnValue(rs, "VESSEL_NUMBER")));
            obj.setLcyAmt(EventHelper.toDouble(getColumnValue(rs, "LCY_AMT")));
            obj.setAgentName(getColumnValue(rs, "AGENT_NAME"));
            obj.setLcyCurr(getColumnValue(rs, "LCY_CURR"));
            obj.setRemName(getColumnValue(rs, "REM_NAME"));
            obj.setBenBic(getColumnValue(rs, "BEN_BIC"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setPurposDesc(getColumnValue(rs, "PURPOS_DESC"));
            obj.setBenfBank(getColumnValue(rs, "BENF_BANK"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setSwiftCode(getColumnValue(rs, "SWIFT_CODE"));
            obj.setJointAcctName(getColumnValue(rs, "JOINT_ACCT_NAME"));
            obj.setVesselName(getColumnValue(rs, "VESSEL_NAME"));
            obj.setChannelType(getColumnValue(rs, "CHANNEL_TYPE"));
            obj.setBenAcctNo(getColumnValue(rs, "BEN_ACCT_NO"));
            obj.setBenCustId(getColumnValue(rs, "BEN_CUST_ID"));
            obj.setAcctNoWsKey(getColumnValue(rs, "ACCT_NO_WS_KEY"));
            obj.setSystem(getColumnValue(rs, "SYSTEM"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setUserTyp(getColumnValue(rs, "USER_TYP"));
            obj.setCreatedOn(EventHelper.toTimestamp(getColumnValue(rs, "CREATED_ON")));
            obj.setRemBnkBranch(getColumnValue(rs, "REM_BNK_BRANCH"));
            obj.setPort(getColumnValue(rs, "PORT"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setAcctCurr(getColumnValue(rs, "ACCT_CURR"));
            obj.setRemBic(getColumnValue(rs, "REM_BIC"));
            obj.setClientAccNo(getColumnValue(rs, "CLIENT_ACC_NO"));
            obj.setCoreCustId(EventHelper.toDouble(getColumnValue(rs, "CORE_CUST_ID")));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));
            obj.setFcyTranCur(getColumnValue(rs, "FCY_TRAN_CUR"));
            obj.setRemAdd1(getColumnValue(rs, "REM_ADD1"));
            obj.setRemType(getColumnValue(rs, "REM_TYPE"));
            obj.setRemAdd3(getColumnValue(rs, "REM_ADD3"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setRemAdd2(getColumnValue(rs, "REM_ADD2"));
            obj.setTrnDate(EventHelper.toTimestamp(getColumnValue(rs, "TRN_DATE")));
            obj.setSwiftTxn(getColumnValue(rs, "SWIFT_TXN"));
            obj.setRemBank(getColumnValue(rs, "REM_BANK"));
            obj.setTypeOfGoods(getColumnValue(rs, "TYPE_OF_GOODS"));
            obj.setBenAdd3(getColumnValue(rs, "BEN_ADD3"));
            obj.setCoreAcctId(EventHelper.toDouble(getColumnValue(rs, "CORE_ACCT_ID")));
            obj.setAcctCategory(EventHelper.toDouble(getColumnValue(rs, "ACCT_CATEGORY")));
            obj.setTempRefNo(getColumnValue(rs, "TEMP_REF_NO"));
            obj.setBenAdd2(getColumnValue(rs, "BEN_ADD2"));
            obj.setRemAcctNo(getColumnValue(rs, "REM_ACCT_NO"));
            obj.setSwiftMsgType(getColumnValue(rs, "SWIFT_MSG_TYPE"));
            obj.setRemIdNo(getColumnValue(rs, "REM_ID_NO"));
            obj.setBenAdd1(getColumnValue(rs, "BEN_ADD1"));
            obj.setFcyTranAmt(EventHelper.toDouble(getColumnValue(rs, "FCY_TRAN_AMT")));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setFacilityNum(EventHelper.toDouble(getColumnValue(rs, "FACILITY_NUM")));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setTranRefNo(getColumnValue(rs, "TRAN_REF_NO"));
            obj.setAcctEmail(getColumnValue(rs, "ACCT_EMAIL"));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setRemCustId(getColumnValue(rs, "REM_CUST_ID"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAcctBranch(getColumnValue(rs, "ACCT_BRANCH"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setTxnType(getColumnValue(rs, "TXN_TYPE"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setBenName(getColumnValue(rs, "BEN_NAME"));
            obj.setCptyAcctNo(getColumnValue(rs, "CPTY_ACCT_NO"));
            obj.setServicCode(getColumnValue(rs, "SERVIC_CODE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setPurposCod(getColumnValue(rs, "PURPOS_COD"));
            obj.setIsrtgs(getColumnValue(rs, "ISRTGS"));
            obj.setUsdEqvAmt(EventHelper.toDouble(getColumnValue(rs, "USD_EQV_AMT")));
            obj.setBankCode(getColumnValue(rs, "BANK_CODE"));
            obj.setRemCntryCode(getColumnValue(rs, "REM_CNTRY_CODE"));
            obj.setBenfBnkBranch(getColumnValue(rs, "BENF_BNK_BRANCH"));
            obj.setSno(EventHelper.toDouble(getColumnValue(rs, "SNO")));
            obj.setExecCust(getColumnValue(rs, "EXEC_CUST"));
            obj.setTranCurr(getColumnValue(rs, "TRAN_CURR"));
            obj.setBenfIdNo(getColumnValue(rs, "BENF_ID_NO"));
            obj.setAcctAddr(getColumnValue(rs, "ACCT_ADDR"));
            obj.setRemCity(getColumnValue(rs, "REM_CITY"));
            obj.setBenfIdType(getColumnValue(rs, "BENF_ID_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_REMITTANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_REMITTANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"UPDATED_ON\",\"REM_BANK_CODE\",\"YEAR\",\"CHANNEL\",\"ACCT_TYPE\",\"SOURCE\",\"BEN_CITY\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"AGENT_CODE\",\"VESSEL_NUMBER\",\"LCY_AMT\",\"AGENT_NAME\",\"LCY_CURR\",\"REM_NAME\",\"BEN_BIC\",\"HOST_USER_ID\",\"HOST_ID\",\"TRAN_AMT\",\"TXN_CODE\",\"PURPOS_DESC\",\"BENF_BANK\",\"ACCOUNT_ID\",\"SWIFT_CODE\",\"JOINT_ACCT_NAME\",\"VESSEL_NAME\",\"CHANNEL_TYPE\",\"BEN_ACCT_NO\",\"BEN_CUST_ID\",\"ACCT_NO_WS_KEY\",\"SYSTEM\",\"CX_CUST_ID\",\"USER_TYP\",\"CREATED_ON\",\"REM_BNK_BRANCH\",\"PORT\",\"EVENT_NAME\",\"ACCT_CURR\",\"REM_BIC\",\"CLIENT_ACC_NO\",\"CORE_CUST_ID\",\"SYSTEM_COUNTRY\",\"FCY_TRAN_CUR\",\"REM_ADD1\",\"REM_TYPE\",\"REM_ADD3\",\"KEYS\",\"REM_ADD2\",\"TRN_DATE\",\"SWIFT_TXN\",\"REM_BANK\",\"TYPE_OF_GOODS\",\"BEN_ADD3\",\"CORE_ACCT_ID\",\"ACCT_CATEGORY\",\"TEMP_REF_NO\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"SWIFT_MSG_TYPE\",\"REM_ID_NO\",\"BEN_ADD1\",\"FCY_TRAN_AMT\",\"CX_ACCT_ID\",\"FACILITY_NUM\",\"ACCT_NAME\",\"TRAN_REF_NO\",\"ACCT_EMAIL\",\"ADD_ENTITY_ID2\",\"REM_CUST_ID\",\"ADD_ENTITY_ID1\",\"ACCT_BRANCH\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"TXN_TYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"BEN_NAME\",\"CPTY_ACCT_NO\",\"SERVIC_CODE\",\"CUST_ID\",\"EVENTTS\",\"PURPOS_COD\",\"ISRTGS\",\"USD_EQV_AMT\",\"BANK_CODE\",\"REM_CNTRY_CODE\",\"BENF_BNK_BRANCH\",\"SNO\",\"EXEC_CUST\",\"TRAN_CURR\",\"BENF_ID_NO\",\"ACCT_ADDR\",\"REM_CITY\",\"BENF_ID_TYPE\"" +
              " FROM EVENT_FT_REMITTANCE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [UPDATED_ON],[REM_BANK_CODE],[YEAR],[CHANNEL],[ACCT_TYPE],[SOURCE],[BEN_CITY],[BRANCH],[BEN_CNTRY_CODE],[AGENT_CODE],[VESSEL_NUMBER],[LCY_AMT],[AGENT_NAME],[LCY_CURR],[REM_NAME],[BEN_BIC],[HOST_USER_ID],[HOST_ID],[TRAN_AMT],[TXN_CODE],[PURPOS_DESC],[BENF_BANK],[ACCOUNT_ID],[SWIFT_CODE],[JOINT_ACCT_NAME],[VESSEL_NAME],[CHANNEL_TYPE],[BEN_ACCT_NO],[BEN_CUST_ID],[ACCT_NO_WS_KEY],[SYSTEM],[CX_CUST_ID],[USER_TYP],[CREATED_ON],[REM_BNK_BRANCH],[PORT],[EVENT_NAME],[ACCT_CURR],[REM_BIC],[CLIENT_ACC_NO],[CORE_CUST_ID],[SYSTEM_COUNTRY],[FCY_TRAN_CUR],[REM_ADD1],[REM_TYPE],[REM_ADD3],[KEYS],[REM_ADD2],[TRN_DATE],[SWIFT_TXN],[REM_BANK],[TYPE_OF_GOODS],[BEN_ADD3],[CORE_ACCT_ID],[ACCT_CATEGORY],[TEMP_REF_NO],[BEN_ADD2],[REM_ACCT_NO],[SWIFT_MSG_TYPE],[REM_ID_NO],[BEN_ADD1],[FCY_TRAN_AMT],[CX_ACCT_ID],[FACILITY_NUM],[ACCT_NAME],[TRAN_REF_NO],[ACCT_EMAIL],[ADD_ENTITY_ID2],[REM_CUST_ID],[ADD_ENTITY_ID1],[ACCT_BRANCH],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[TXN_TYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[BEN_NAME],[CPTY_ACCT_NO],[SERVIC_CODE],[CUST_ID],[EVENTTS],[PURPOS_COD],[ISRTGS],[USD_EQV_AMT],[BANK_CODE],[REM_CNTRY_CODE],[BENF_BNK_BRANCH],[SNO],[EXEC_CUST],[TRAN_CURR],[BENF_ID_NO],[ACCT_ADDR],[REM_CITY],[BENF_ID_TYPE]" +
              " FROM EVENT_FT_REMITTANCE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`UPDATED_ON`,`REM_BANK_CODE`,`YEAR`,`CHANNEL`,`ACCT_TYPE`,`SOURCE`,`BEN_CITY`,`BRANCH`,`BEN_CNTRY_CODE`,`AGENT_CODE`,`VESSEL_NUMBER`,`LCY_AMT`,`AGENT_NAME`,`LCY_CURR`,`REM_NAME`,`BEN_BIC`,`HOST_USER_ID`,`HOST_ID`,`TRAN_AMT`,`TXN_CODE`,`PURPOS_DESC`,`BENF_BANK`,`ACCOUNT_ID`,`SWIFT_CODE`,`JOINT_ACCT_NAME`,`VESSEL_NAME`,`CHANNEL_TYPE`,`BEN_ACCT_NO`,`BEN_CUST_ID`,`ACCT_NO_WS_KEY`,`SYSTEM`,`CX_CUST_ID`,`USER_TYP`,`CREATED_ON`,`REM_BNK_BRANCH`,`PORT`,`EVENT_NAME`,`ACCT_CURR`,`REM_BIC`,`CLIENT_ACC_NO`,`CORE_CUST_ID`,`SYSTEM_COUNTRY`,`FCY_TRAN_CUR`,`REM_ADD1`,`REM_TYPE`,`REM_ADD3`,`KEYS`,`REM_ADD2`,`TRN_DATE`,`SWIFT_TXN`,`REM_BANK`,`TYPE_OF_GOODS`,`BEN_ADD3`,`CORE_ACCT_ID`,`ACCT_CATEGORY`,`TEMP_REF_NO`,`BEN_ADD2`,`REM_ACCT_NO`,`SWIFT_MSG_TYPE`,`REM_ID_NO`,`BEN_ADD1`,`FCY_TRAN_AMT`,`CX_ACCT_ID`,`FACILITY_NUM`,`ACCT_NAME`,`TRAN_REF_NO`,`ACCT_EMAIL`,`ADD_ENTITY_ID2`,`REM_CUST_ID`,`ADD_ENTITY_ID1`,`ACCT_BRANCH`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`TXN_TYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`BEN_NAME`,`CPTY_ACCT_NO`,`SERVIC_CODE`,`CUST_ID`,`EVENTTS`,`PURPOS_COD`,`ISRTGS`,`USD_EQV_AMT`,`BANK_CODE`,`REM_CNTRY_CODE`,`BENF_BNK_BRANCH`,`SNO`,`EXEC_CUST`,`TRAN_CURR`,`BENF_ID_NO`,`ACCT_ADDR`,`REM_CITY`,`BENF_ID_TYPE`" +
              " FROM EVENT_FT_REMITTANCE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_REMITTANCE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"UPDATED_ON\",\"REM_BANK_CODE\",\"YEAR\",\"CHANNEL\",\"ACCT_TYPE\",\"SOURCE\",\"BEN_CITY\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"AGENT_CODE\",\"VESSEL_NUMBER\",\"LCY_AMT\",\"AGENT_NAME\",\"LCY_CURR\",\"REM_NAME\",\"BEN_BIC\",\"HOST_USER_ID\",\"HOST_ID\",\"TRAN_AMT\",\"TXN_CODE\",\"PURPOS_DESC\",\"BENF_BANK\",\"ACCOUNT_ID\",\"SWIFT_CODE\",\"JOINT_ACCT_NAME\",\"VESSEL_NAME\",\"CHANNEL_TYPE\",\"BEN_ACCT_NO\",\"BEN_CUST_ID\",\"ACCT_NO_WS_KEY\",\"SYSTEM\",\"CX_CUST_ID\",\"USER_TYP\",\"CREATED_ON\",\"REM_BNK_BRANCH\",\"PORT\",\"EVENT_NAME\",\"ACCT_CURR\",\"REM_BIC\",\"CLIENT_ACC_NO\",\"CORE_CUST_ID\",\"SYSTEM_COUNTRY\",\"FCY_TRAN_CUR\",\"REM_ADD1\",\"REM_TYPE\",\"REM_ADD3\",\"KEYS\",\"REM_ADD2\",\"TRN_DATE\",\"SWIFT_TXN\",\"REM_BANK\",\"TYPE_OF_GOODS\",\"BEN_ADD3\",\"CORE_ACCT_ID\",\"ACCT_CATEGORY\",\"TEMP_REF_NO\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"SWIFT_MSG_TYPE\",\"REM_ID_NO\",\"BEN_ADD1\",\"FCY_TRAN_AMT\",\"CX_ACCT_ID\",\"FACILITY_NUM\",\"ACCT_NAME\",\"TRAN_REF_NO\",\"ACCT_EMAIL\",\"ADD_ENTITY_ID2\",\"REM_CUST_ID\",\"ADD_ENTITY_ID1\",\"ACCT_BRANCH\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"TXN_TYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"BEN_NAME\",\"CPTY_ACCT_NO\",\"SERVIC_CODE\",\"CUST_ID\",\"EVENTTS\",\"PURPOS_COD\",\"ISRTGS\",\"USD_EQV_AMT\",\"BANK_CODE\",\"REM_CNTRY_CODE\",\"BENF_BNK_BRANCH\",\"SNO\",\"EXEC_CUST\",\"TRAN_CURR\",\"BENF_ID_NO\",\"ACCT_ADDR\",\"REM_CITY\",\"BENF_ID_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[UPDATED_ON],[REM_BANK_CODE],[YEAR],[CHANNEL],[ACCT_TYPE],[SOURCE],[BEN_CITY],[BRANCH],[BEN_CNTRY_CODE],[AGENT_CODE],[VESSEL_NUMBER],[LCY_AMT],[AGENT_NAME],[LCY_CURR],[REM_NAME],[BEN_BIC],[HOST_USER_ID],[HOST_ID],[TRAN_AMT],[TXN_CODE],[PURPOS_DESC],[BENF_BANK],[ACCOUNT_ID],[SWIFT_CODE],[JOINT_ACCT_NAME],[VESSEL_NAME],[CHANNEL_TYPE],[BEN_ACCT_NO],[BEN_CUST_ID],[ACCT_NO_WS_KEY],[SYSTEM],[CX_CUST_ID],[USER_TYP],[CREATED_ON],[REM_BNK_BRANCH],[PORT],[EVENT_NAME],[ACCT_CURR],[REM_BIC],[CLIENT_ACC_NO],[CORE_CUST_ID],[SYSTEM_COUNTRY],[FCY_TRAN_CUR],[REM_ADD1],[REM_TYPE],[REM_ADD3],[KEYS],[REM_ADD2],[TRN_DATE],[SWIFT_TXN],[REM_BANK],[TYPE_OF_GOODS],[BEN_ADD3],[CORE_ACCT_ID],[ACCT_CATEGORY],[TEMP_REF_NO],[BEN_ADD2],[REM_ACCT_NO],[SWIFT_MSG_TYPE],[REM_ID_NO],[BEN_ADD1],[FCY_TRAN_AMT],[CX_ACCT_ID],[FACILITY_NUM],[ACCT_NAME],[TRAN_REF_NO],[ACCT_EMAIL],[ADD_ENTITY_ID2],[REM_CUST_ID],[ADD_ENTITY_ID1],[ACCT_BRANCH],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[TXN_TYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[BEN_NAME],[CPTY_ACCT_NO],[SERVIC_CODE],[CUST_ID],[EVENTTS],[PURPOS_COD],[ISRTGS],[USD_EQV_AMT],[BANK_CODE],[REM_CNTRY_CODE],[BENF_BNK_BRANCH],[SNO],[EXEC_CUST],[TRAN_CURR],[BENF_ID_NO],[ACCT_ADDR],[REM_CITY],[BENF_ID_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`UPDATED_ON`,`REM_BANK_CODE`,`YEAR`,`CHANNEL`,`ACCT_TYPE`,`SOURCE`,`BEN_CITY`,`BRANCH`,`BEN_CNTRY_CODE`,`AGENT_CODE`,`VESSEL_NUMBER`,`LCY_AMT`,`AGENT_NAME`,`LCY_CURR`,`REM_NAME`,`BEN_BIC`,`HOST_USER_ID`,`HOST_ID`,`TRAN_AMT`,`TXN_CODE`,`PURPOS_DESC`,`BENF_BANK`,`ACCOUNT_ID`,`SWIFT_CODE`,`JOINT_ACCT_NAME`,`VESSEL_NAME`,`CHANNEL_TYPE`,`BEN_ACCT_NO`,`BEN_CUST_ID`,`ACCT_NO_WS_KEY`,`SYSTEM`,`CX_CUST_ID`,`USER_TYP`,`CREATED_ON`,`REM_BNK_BRANCH`,`PORT`,`EVENT_NAME`,`ACCT_CURR`,`REM_BIC`,`CLIENT_ACC_NO`,`CORE_CUST_ID`,`SYSTEM_COUNTRY`,`FCY_TRAN_CUR`,`REM_ADD1`,`REM_TYPE`,`REM_ADD3`,`KEYS`,`REM_ADD2`,`TRN_DATE`,`SWIFT_TXN`,`REM_BANK`,`TYPE_OF_GOODS`,`BEN_ADD3`,`CORE_ACCT_ID`,`ACCT_CATEGORY`,`TEMP_REF_NO`,`BEN_ADD2`,`REM_ACCT_NO`,`SWIFT_MSG_TYPE`,`REM_ID_NO`,`BEN_ADD1`,`FCY_TRAN_AMT`,`CX_ACCT_ID`,`FACILITY_NUM`,`ACCT_NAME`,`TRAN_REF_NO`,`ACCT_EMAIL`,`ADD_ENTITY_ID2`,`REM_CUST_ID`,`ADD_ENTITY_ID1`,`ACCT_BRANCH`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`TXN_TYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`BEN_NAME`,`CPTY_ACCT_NO`,`SERVIC_CODE`,`CUST_ID`,`EVENTTS`,`PURPOS_COD`,`ISRTGS`,`USD_EQV_AMT`,`BANK_CODE`,`REM_CNTRY_CODE`,`BENF_BNK_BRANCH`,`SNO`,`EXEC_CUST`,`TRAN_CURR`,`BENF_ID_NO`,`ACCT_ADDR`,`REM_CITY`,`BENF_ID_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

