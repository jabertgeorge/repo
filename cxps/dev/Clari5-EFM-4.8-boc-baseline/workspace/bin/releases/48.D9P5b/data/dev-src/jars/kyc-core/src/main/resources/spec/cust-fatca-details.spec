clari5.kyc.entity.cust-fatca-details{
    attributes = [
        { name : ddl-id, type ="string:50", key=true}
		{ name : cust-id, type ="string:50" }
		{ name : name, type ="string:100" }
		{ name : address,  type = "string:200" }
		{ name : city,  type = "string:100"}
		{ name : state,  type = "string:100" }
		{ name : country,  type = "string:100" }
		{ name : pcode,  type = "string:50" }
		{ name : tin,  type = "string:100" }
        { name : fatca-declaration,  type = "string:30" }
        { name : ud-type, type ="string:30" }
	]
}
