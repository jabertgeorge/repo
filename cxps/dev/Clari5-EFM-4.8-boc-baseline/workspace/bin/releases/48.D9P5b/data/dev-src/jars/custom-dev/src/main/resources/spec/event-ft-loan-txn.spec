cxps.events.event.ft-loan-txn {
   table-name : EVENT_FT_LOANTXN
   event-mnemonic: FLO
   workspaces : {
	   ACCOUNT : cust-ref-id,
	   USER : agent-id
	   CUSTOMER: cust-id
	   TRANSACTION: txn-ref-id
	   PAYMENTCARD:  cust-card-id
  	}
   event-attributes : {

    event-name : {db : true, raw_name: event_name, type : "string:100"}	
    event-type : {db : true, raw_name: eventtype, type : "string:100"}
    event-subtype : {db : true, raw_name: eventsubtype, type : "string:100"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    host-id : {db : true ,raw_name : host-id ,type : "string:20"}
    channel-type : {db : true ,raw_name : channel_type ,type : "string:20"}
    channel : {db : true ,raw_name : channel ,type : "string:20"}
    keys : {db : true ,raw_name : keys ,type : "string:20"}  
    agent-type : {db : true ,raw_name : agent_type ,type : "string:20"}
    agent-id : {db : true ,raw_name : pstd_user_id ,type : "string:20"}
    source-bank : {db : true ,raw_name : bank-code ,type : "string:20"}
    cust-ref-type : {db : true ,raw_name : cust_ref_type ,type : "string:20"}
    cust-ref-id : {db : true ,raw_name : acid ,type : "string:20"}
    cx-cif-id : {db : true ,raw_name : cx_cif_i_d ,type : "string:20"}
    txn-ref-type : {db : true ,raw_name : txn_ref_type ,type : "string:20"}
    txn-ref-id : {db : true ,raw_name : tran_id ,type : "string:20"}
    txn-date : {db : true ,raw_name : sys_time ,type : timestamp}
    action-at-host : {db : true ,raw_name : module_id ,type : "string:20"}
    loan-settlement-Date : {db : true ,raw_name : loan_settlement_Date ,type : date}
    desc : {db : true ,raw_name : tran-particular ,type : "string:20"}
    txn-amt : {db : true ,raw_name : tran_amt ,type : "number:11,2"}
    Ref-amt : {db : true ,raw_name : lcy-tran-amt,type : "number:11,2" }
    Ref-Curncy : {db : true ,raw_name : lcy-tran-crncy,type : "number:11,2" }
    txn-amt-cur : {db : true ,raw_name : tran_crncy_code ,type : "string:20"}
    txn-code : {db : true ,raw_name : tran_code ,type : "string:20"}
    tran-crncy-code : {db : true ,raw_name : tran-crncy-code ,type : "string:20"}
    txn-value-date : {db : true ,raw_name : value-date ,type : timestamp}
    exception : {db : true ,raw_name : exception ,type : "string:20"}
    txn-status : {db : true ,raw_name : pstd-flg ,type : "string:20"}
    remarks : {db : true ,raw_name : tran-rmks ,type : "string:20"}
    source-or-dest-acct-id : {db : true ,raw_name : account-id ,type : "string:20" }
    acct-type : {db : true ,raw_name : Acct-type ,type : "string:20" }
    remitter-name : {db : true ,raw_name : remitter_name ,type : "string:20"}
    beneficiary-name : {db : true ,raw_name : beneficiary_name ,type : "string:20"}
    instrument-type : {db : true ,raw_name : instrmnt-type ,type : "string:20"}
    instrument-id : {db : true ,raw_name : instrmnt_type ,type : "number:20"}
    ledger-bal : {db : true ,raw_name :clr_bal_amt ,type : "number:11,2"}
    balance-cur : {db : true ,raw_name : balance_cur ,type : "string:20"}
    shadow-bal : {db : true ,raw_name : un_clr_bal_amt ,type : "number:11,2"}
    avl-bal : {db : true ,raw_name : avl-bal ,type : "number:11,2"}
    avl-bal : {db : true ,raw_name : avl-bal_LCY ,type : "number:11,2"}
    eff-avl-bal : {db : true ,raw_name : eff_avl_bal ,type : "number:11,2"}
    txn-dr-cr : {db : true ,raw_name : part-tran-type ,type : "string:20"}
    evt-inducer : {db : true ,raw_name : evt_inducer ,type : "string:20"}
    bill-id : {db : true ,raw_name : ref_num ,type : "string:20"}
    zone-date : {db : true ,raw_name : zone_date ,type : timestamp}
    zone-code : {db : true ,raw_name : zone_code ,type : "string:20"}
    payee-id : {db : true ,raw_name : payee_id ,type : "string:20"}
    merchant-categ : {db : true ,raw_name : merchant_categ ,type : "string:20"}
    tran-srl-no : {db : true ,raw_name : part_tran_srl_num ,type : "string:20" }
    tran-category : {db : true ,raw_name : tran_category ,type : "string:20"}
    payer-id-temp : {db : true ,raw_name : acid ,type : "string:20"}
    inst-alpha : {db : true ,raw_name : instrmnt_alpha ,type : "string:20"}
    tran-type : {db : true ,raw_name : tran-type ,type : "string:20" }
    tran-sub-type : {db : true ,raw_name : tran-sub-type ,type : "string:20"}
    tran-entry-dt : {db : true ,raw_name : entry_date ,type : timestamp}
    tran-posted-dt : {db : true ,raw_name : pstd-date ,type : timestamp}
    tran-verified-dt : {db : true ,raw_name : vfd_date ,type : timestamp}
    tran-br-id : {db : true ,raw_name : TXN_BR-ID ,type : "string:20"}
    tran-id : {db : true ,raw_name : tran-id ,type : "string:20"}
    rate : {db : true ,raw_name : rate ,type : "number:11,2"}
    tran-amt : {db : true ,raw_name : tran-amt ,type : "number:11,2"}
    ref-tran-amt : {db : true ,raw_name : ref_tran_amt ,type : "number:11,2"}
    ref-currency : {db : true ,raw_name : ref_tran_crncy ,type : "string:20"}
    acct-sol-id : {db : true ,raw_name : acct_sol_id ,type : "string:20"}
    system-time : {db : true ,raw_name : sys-time ,type : timestamp}
    scheme-type : {db : true ,raw_name : schm_type ,type : "string:20"}
    scheme-code : {db : true ,raw_name : Prod-code ,type : "string:20"}
    place-holder : {db : true ,raw_name : place_holder ,type : "string:20"}
    cust-id : {db : true ,raw_name : cust-id ,type : "string:20"}
    cx-cust-id : {db : true ,raw_name : cx-cust-id ,type : "string:20"}
    cx-acct-id : {db : true ,raw_name : cx-acct-id ,type : "string:20"}
    acct-name : {db : true ,raw_name : acct-name ,type : "string:20"}
    account-ownership : {db : true ,raw_name : acct-ownership ,type : "string:20"}
    account-opendate : {db : true ,raw_name : acctopendate ,type : timestamp}
    branch-id-desc : {db : true ,raw_name : acct-Branch-id ,type : "string:20"}
    employee-id : {db : true ,raw_name : emp_id ,type : "string:20"}
    cre-dr-ptran : {db : true ,raw_name : cre_dr_ptran ,type : "string:20"}
    pur-acct-num : {db : true ,raw_name : pur_acct_num ,type : "string:20"}
    payee-name : {db : true ,raw_name : payeename ,type : "string:20"}
    payee-city : {db : true ,raw_name : payeecity ,type : "string:20"}
    online-batch : {db : true ,raw_name : online-batch ,type : "string:20"}
    kyc-status : {db : true ,raw_name : KYC_Stat ,type : "string:20"}
    acct-status : {db : true ,raw_name : Acct_Stat ,type : "string:20"}
    sanction-amt : {db : true ,raw_name : sanction_amt ,type : "number:11,2"}
    fcnr-flag : {db : true ,raw_name : fcnr_flag ,type : "string:20"}
    eod : {db : true ,raw_name : eod ,type : "number:20"}
    terminal-ip-addr : {db : true ,raw_name : ip_address ,type : "string:20"}
    direct-channel-id : {db : true ,raw_name : dc_id ,type : "string:20"}
    direct-channel-controller-id : {db : true ,raw_name : dcc_id ,type : "string:20"}
    device-id : {db : true ,raw_name : device_id ,type : "string:20"}
    acct-occp-code : {raw_name : acct_occp_code ,type : "string:20"}
    add-entity-id1 : {raw_name : reservedfield1 ,type : "string:20"}
    add-entity-id2 : {raw_name : reservedfield2 ,type : "string:20"}
    add-entity-id3 : {raw_name : reservedfield3 ,type : "string:20"}
    add-entity-id4 : {type : "string:100",raw_name : reservedfield4}
    add-entity-id5 : {type : "string:100",raw_name : reservedfield5}
    channel-desc : {db : true ,raw_name : channel_desc ,type : "string:20"}
    entity-id : {db : true ,raw_name : entity_id ,type : "string:20"}
    entity-type : {db : true ,raw_name : entity_type ,type : "string:20"}
    entry-user : {db : true ,raw_name : entry_user ,type : "string:20"}
    event-sub-type : {db : true ,raw_name : event_sub_type ,type : "string:20"}
    event-time : {db : true ,raw_name : eventts ,type :  timestamp}
    event-type : {db : true ,raw_name : event_type ,type : "string:20"}
    hdrmkrs : {db : true ,raw_name : hdrmkrs ,type : "string:20"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:20"}
    mode-oprn-code : {db : true ,raw_name : mode_oprn_code ,type : "string:20"}
    msg-name : {db : true ,raw_name : msg_name ,type : "string:20"}
    msg-source : {db : true ,raw_name : source ,type : "string:20"} 
    tran-date : {db : true ,raw_name : tran-date ,type : timestamp}
    bin : {db : true ,raw_name : BIN ,type : "string:20"}
    system-country : {db : true ,raw_name : SYSTEMCOUNTRY ,type : "string:20"}	
    country-code : {db : true ,raw_name : country_code ,fr: true, type : "string:20"}
    dev-owner-id : {db : true ,raw_name : dev_owner_id ,fr: true, type : "string:20"}
    cust-card-id : {db : true ,raw_name : cust_card_id ,fr: true, type : "string:20"}
    acct-open-date : {db : true ,raw_name : acct_open_date ,fr: true, type : timestamp}
    sequence-number : {db : true ,raw_name : sequence_number ,type : "string:20" }
    counterparty-account : {db : true ,raw_name : CounterpartyAccount ,type : "string:20" }
    counterparty-name : {db : true ,raw_name : CounterpartyName ,type : "string:20" }
    counterparty-bank : {db : true ,raw_name : CounterpartyBank ,type : "string:20" }
    counterparty-amount : {db : true ,raw_name : CounterpartyAmount ,type : "string:20" }
    counterparty-currency : {db : true ,raw_name : CounterpartyCurrency ,type : "string:20" }
    counterparty-amount-lcy : {db : true ,raw_name : CounterpartyAmountLCY ,type : "string:20" }
    counterparty-currency-lcy : {db : true ,raw_name : CounterpartyCurrencyLCY ,type : "string:20" }
    counterparty-address : {db : true ,raw_name : CounterpartyAddress ,type : "string:20" }
    counterparty-bank-code : {db : true ,raw_name : CounterpartyBankCode ,type : "string:20" }
    counterparty-bic : {db : true ,raw_name : CounterpartyBIC ,type : "string:20" }
    counterparty-bank-address : {db : true ,raw_name : CounterpartyBankAddress ,type : "string:20" }
    counterparty-country-code : {db : true ,raw_name : CounterpartyCountryCode ,type : "string:20" }
    counterparty-business : {db : true ,raw_name : CounterpartyBusiness ,type : "string:20" }
    teller-number : {db : true ,raw_name : TellerNumber ,type : "string:20" }
    sequence-number : {db : true ,raw_name : SequenceNumber ,type : "string:20" }    
    
   }
}

