package clari5.custom.wlalert.daemon;

import clari5.custom.wlalert.FileProcess;
import clari5.custom.wlalert.db.WlRecordDetail;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;


public class FetchData extends CxpsRunnable implements ICxResource {
    @Override
    protected Object getData() throws RuntimeFatalException {
      FileProcess fileProcess =new FileProcess();
      fileProcess.getFile();
        return fileProcess ;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
    }

    @Override
    public void configure(Hocon h) {

    }

    @Override
    public void release() {

    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void refresh() {

    }

    @Override
    public ConfigType getType() {
        return null;
    }

    @Override
    public void configure(Hocon h, Hocon location) {

    }

    @Override
    public void refresh(Hocon conf, Hocon location) {

    }
}
