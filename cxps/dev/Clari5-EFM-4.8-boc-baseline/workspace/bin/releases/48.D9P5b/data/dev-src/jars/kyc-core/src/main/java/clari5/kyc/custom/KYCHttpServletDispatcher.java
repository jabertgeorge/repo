package clari5.kyc.custom;

import clari5.platform.applayer.Clari5;
import clari5.platform.fileq.Clari5Reader;
import clari5.platform.util.Hocon;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Created by abhijeet .
 */
public class KYCHttpServletDispatcher extends HttpServletDispatcher {

    @Override
    public void init() throws ServletException {
        super.init();

        System.out.println("Initializing KYC app");
        Hocon h = new Hocon();
        h.loadFromContext("kyc-clari5.conf");
        Clari5.bootstrap("KYC", "kyc-clari5.conf");

        h.loadFromContext("rdbms.conf");
        Clari5.bootstrap("KYC-RDBMS", "rdbms.conf");

        System.out.println("KYC application initialized");
    }

    @Override
    public void destroy() {
        System.out.println("Releasing KYC resources");
        Clari5.release();
        System.out.println("KYC release completed");
        super.destroy();
    }
}
