package clari5.custom.dev.fatcabootstrapping;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.Hocon;
import cxps.events.FT_AccountTxnEvent;
import cxps.events.FT_RemittanceEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class CustomDerivator {

    public static String checkInstance(Object object){

        if (object instanceof FT_AccountTxnEvent){
            try {
                CoreAccountBootsTrap.updateCustomerForCoreAccount((FT_AccountTxnEvent) object);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (object instanceof FT_RemittanceEvent){

        }

        return null;
    }

    public static CxConnection getConnection() {
        //Clari5.batchBootstrap("test", "efm-clari5.conf");

        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
    }
}
