package clari5.kyc.custom.pojo;

import clari5.kyc.CustFatcaDetails;
import java.util.List;

public class ChangesJson {

    private String custId;
    private String classification;
    private String usPerson;
    private String npfi;
    private String w8Available;
    private String expiryDate;
    private List<CustFatcaUserDetails> accountRelatedUsers;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getUsPerson() {
        return usPerson;
    }

    public void setUsPerson(String usPerson) {
        this.usPerson = usPerson;
    }

    public String getNpfi() {
        return npfi;
    }

    public void setNpfi(String npfi) {
        this.npfi = npfi;
    }

    public String getW8Available() {
        return w8Available;
    }

    public void setW8Available(String w8Available) {
        this.w8Available = w8Available;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public List<CustFatcaUserDetails> getAccountRelatedUsers() {
        return accountRelatedUsers;
    }

    public void setAccountRelatedUsers(List<CustFatcaUserDetails> custFatcaDetails) {
        this.accountRelatedUsers = custFatcaDetails;
    }
}