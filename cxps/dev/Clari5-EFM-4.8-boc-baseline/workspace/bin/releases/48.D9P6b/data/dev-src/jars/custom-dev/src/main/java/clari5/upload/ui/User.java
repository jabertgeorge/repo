package clari5.upload.ui;

import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
    private static CxpsLogger logger = CxpsLogger.getLogger(User.class);

    static Connection con = null;
    static PreparedStatement psmt = null;
    static ResultSet resultSet = null;

    public static String getUserName(String sessionId) {
        String userName = "NA";
        try {

            con = DBConnection.getDBConnection();
            String query = "select user_id from cl5_live_session_tbl where  session_key=?";
            psmt = con.prepareStatement(query);
            psmt.setString(1, sessionId);
            resultSet = psmt.executeQuery();

            while (resultSet.next()) {
                userName = resultSet.getString(1);
            }


        } catch (SQLException sqe) {
            logger.error("Unable to perform db db operations " + sqe.getMessage() + "Cause " + sqe.getCause());

        } catch (Exception e) {
            logger.error("Exception " + e.getMessage() + " Cause " + e.getCause());
        } finally {
            try {
                if (con != null) {
                    resultSet.close();
                    psmt.close();
                    con.close();
                }
            } catch (SQLException sqe) {
                logger.error("Failed to close connection " + sqe.getMessage() + " Cause " + sqe.getCause());
            }
        }

        logger.info("User name is " + userName);
        return userName;

    }
}

