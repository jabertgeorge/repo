package clari5.custom.boc.integration.audit;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import clari5.custom.boc.integration.exceptions.ConfigurationException;

import clari5.custom.boc.integration.config.BepCon;

public class AuditManager {
	static List<IAuditClient> auditors;
	static {
		try {
			BepCon config;
			try {
				config = BepCon.getConfig();
				String logPath = config.getProperty("logPath", null);
				LogLevel logLevel = LogLevel.getLogLevel(config.getProperty("logLevel", "3"));
				FileAudit logger = null;
				if(logLevel.compareTo(LogLevel.OFF) != 0) {
					logger = FileAudit.getLogger(logLevel, logPath);
				}
				
				if(logger != null) {
					auditors = new ArrayList<IAuditClient>();
					auditors.add(logger);
				}
			} catch (IllegalArgumentException iae) {
				throw new ConfigurationException("Invalid log level specified in the configuration file.\n"+iae.getMessage());
			} catch (NullPointerException ne) {
				throw new ConfigurationException("Log level specified is not specified in the configuration file\n"+ne.getMessage());
			} catch (Exception e) {
				throw new ConfigurationException("Exception caught while loading configuration. \n"+e.getMessage());
			}
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	public static void log(String message) {
		// Logs the message to all the clients subscribed.
		for(IAuditClient client : auditors) {
			client.log(LogLevel.ALL, message);
		}
	}
	public static void log(LogLevel logLevel, String message) {
		// Logs the message to all the clients subscribed.
		for(IAuditClient client : auditors) {
			client.log(logLevel, message);
		}
	}
	public static void log(Exception e) {
		StringWriter exStackSw = new StringWriter();
		PrintWriter exStackPw = new PrintWriter(exStackSw);
		e.printStackTrace(exStackPw);
		AuditManager.log(exStackSw.toString());
	}
}