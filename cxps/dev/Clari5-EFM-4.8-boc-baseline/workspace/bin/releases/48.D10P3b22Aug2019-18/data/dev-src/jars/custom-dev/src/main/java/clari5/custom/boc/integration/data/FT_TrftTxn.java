package clari5.custom.boc.integration.data;

public class FT_TrftTxn extends ITableData {

    private String tableName = "FT_TRFT_TXN";
    private String event_type = "FT_TrftTxn";

    private String event_name;
    private String eventtype;
    private String eventsubtype;
    private String host_user_id;
    private String source;
    private String eventts;
    private String keys;
    private String systemcountry;
    private String host_id;
    private String channel;
    private String account_id;
    private String acct_type;
    private String prod_code;
    private String cust_id;
    private String cx_cust_id;
    private String cx_acct_id;
    private String acct_name;
    private String acct_branch_id;
    private String acct_ownership;
    private String acctopendate;
    private String avl_bal;
    private String avl_bal_lcy;
    private String tran_type;
    private String tran_sub_type;
    private String part_tran_type;
    private String tran_date;
    private String pstd_date;
    private String tran_id;
    private String part_tran_srl_num;
    private String tran_code;
    private String value_date;
    private String tran_amt;
    private String tran_crncy_code;
    private String lcy_tran_amt;
    private String lcy_tran_crncy;
    private String tran_particular;
    private String tran_rmks;
    private String instrmnt_type;
    private String sys_time;
    private String bank_code;
    private String pstd_flg;
    private String online_batch;
    private String counterparty_account;
    private String counterparty_name;
    private String counterparty_bank;
    private String counterparty_amount;
    private String counterparty_currency;
    private String counterparty_amount_lcy;
    private String counterparty_currency_lcy;
    private String counterparty_address;
    private String counterparty_bank_code;
    private String counterparty_bic;
    private String counterparty_bank_address;
    private String counterparty_country_code;
    private String counterparty_business;
    private String teller_number;
    private String sequence_number;
    private String reservedfield1;
    private String reservedfield2;
    private String reservedfield3;
    private String reservedfield4;
    private String reservedfield5;
    private String event_id;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getEventsubtype() {
        return eventsubtype;
    }

    public void setEventsubtype(String eventsubtype) {
        this.eventsubtype = eventsubtype;
    }

    public String getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(String host_user_id) {
        this.host_user_id = host_user_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getEventts() {
        return eventts;
    }

    public void setEventts(String eventts) {
        this.eventts = eventts;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getSystemcountry() {
        return systemcountry;
    }

    public void setSystemcountry(String systemcountry) {
        this.systemcountry = systemcountry;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAcct_type() {
        return acct_type;
    }

    public void setAcct_type(String acct_type) {
        this.acct_type = acct_type;
    }

    public String getProd_code() {
        return prod_code;
    }

    public void setProd_code(String prod_code) {
        this.prod_code = prod_code;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getCx_cust_id() {
        return cx_cust_id;
    }

    public void setCx_cust_id(String cx_cust_id) {
        this.cx_cust_id = cx_cust_id;
    }

    public String getCx_acct_id() {
        return cx_acct_id;
    }

    public void setCx_acct_id(String cx_acct_id) {
        this.cx_acct_id = cx_acct_id;
    }

    public String getAcct_name() {
        return acct_name;
    }

    public void setAcct_name(String acct_name) {
        this.acct_name = acct_name;
    }

    public String getAcct_branch_id() {
        return acct_branch_id;
    }

    public void setAcct_branch_id(String acct_branch_id) {
        this.acct_branch_id = acct_branch_id;
    }

    public String getAcct_ownership() {
        return acct_ownership;
    }

    public void setAcct_ownership(String acct_ownership) {
        this.acct_ownership = acct_ownership;
    }

    public String getAcctopendate() {
        return acctopendate;
    }

    public void setAcctopendate(String acctopendate) {
        this.acctopendate = acctopendate;
    }

    public String getAvl_bal() {
        return avl_bal;
    }

    public void setAvl_bal(String avl_bal) {
        this.avl_bal = avl_bal;
    }

    public String getAvl_bal_lcy() {
        return avl_bal_lcy;
    }

    public void setAvl_bal_lcy(String avl_bal_lcy) {
        this.avl_bal_lcy = avl_bal_lcy;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_sub_type() {
        return tran_sub_type;
    }

    public void setTran_sub_type(String tran_sub_type) {
        this.tran_sub_type = tran_sub_type;
    }

    public String getPart_tran_type() {
        return part_tran_type;
    }

    public void setPart_tran_type(String part_tran_type) {
        this.part_tran_type = part_tran_type;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getPstd_date() {
        return pstd_date;
    }

    public void setPstd_date(String pstd_date) {
        this.pstd_date = pstd_date;
    }

    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }

    public String getPart_tran_srl_num() {
        return part_tran_srl_num;
    }

    public void setPart_tran_srl_num(String part_tran_srl_num) {
        this.part_tran_srl_num = part_tran_srl_num;
    }

    public String getTran_code() {
        return tran_code;
    }

    public void setTran_code(String tran_code) {
        this.tran_code = tran_code;
    }

    public String getValue_date() {
        return value_date;
    }

    public void setValue_date(String value_date) {
        this.value_date = value_date;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public void setTran_amt(String tran_amt) {
        this.tran_amt = tran_amt;
    }

    public String getTran_crncy_code() {
        return tran_crncy_code;
    }

    public void setTran_crncy_code(String tran_crncy_code) {
        this.tran_crncy_code = tran_crncy_code;
    }

    public String getLcy_tran_amt() {
        return lcy_tran_amt;
    }

    public void setLcy_tran_amt(String lcy_tran_amt) {
        this.lcy_tran_amt = lcy_tran_amt;
    }

    public String getLcy_tran_crncy() {
        return lcy_tran_crncy;
    }

    public void setLcy_tran_crncy(String lcy_tran_crncy) {
        this.lcy_tran_crncy = lcy_tran_crncy;
    }

    public String getTran_particular() {
        return tran_particular;
    }

    public void setTran_particular(String tran_particular) {
        this.tran_particular = tran_particular;
    }

    public String getTran_rmks() {
        return tran_rmks;
    }

    public void setTran_rmks(String tran_rmks) {
        this.tran_rmks = tran_rmks;
    }

    public String getInstrmnt_type() {
        return instrmnt_type;
    }

    public void setInstrmnt_type(String instrmnt_type) {
        this.instrmnt_type = instrmnt_type;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getPstd_flg() {
        return pstd_flg;
    }

    public void setPstd_flg(String pstd_flg) {
        this.pstd_flg = pstd_flg;
    }

    public String getOnline_batch() {
        return online_batch;
    }

    public void setOnline_batch(String online_batch) {
        this.online_batch = online_batch;
    }

    public String getCounterparty_account() {
        return counterparty_account;
    }

    public void setCounterparty_account(String counterparty_account) {
        this.counterparty_account = counterparty_account;
    }

    public String getCounterparty_name() {
        return counterparty_name;
    }

    public void setCounterparty_name(String counterparty_name) {
        this.counterparty_name = counterparty_name;
    }

    public String getCounterparty_bank() {
        return counterparty_bank;
    }

    public void setCounterparty_bank(String counterparty_bank) {
        this.counterparty_bank = counterparty_bank;
    }

    public String getCounterparty_amount() {
        return counterparty_amount;
    }

    public void setCounterparty_amount(String counterparty_amount) {
        this.counterparty_amount = counterparty_amount;
    }

    public String getCounterparty_currency() {
        return counterparty_currency;
    }

    public void setCounterparty_currency(String counterparty_currency) {
        this.counterparty_currency = counterparty_currency;
    }

    public String getCounterparty_amount_lcy() {
        return counterparty_amount_lcy;
    }

    public void setCounterparty_amount_lcy(String counterparty_amount_lcy) {
        this.counterparty_amount_lcy = counterparty_amount_lcy;
    }

    public String getCounterparty_currency_lcy() {
        return counterparty_currency_lcy;
    }

    public void setCounterparty_currency_lcy(String counterparty_currency_lcy) {
        this.counterparty_currency_lcy = counterparty_currency_lcy;
    }

    public String getCounterparty_address() {
        return counterparty_address;
    }

    public void setCounterparty_address(String counterparty_address) {
        this.counterparty_address = counterparty_address;
    }

    public String getCounterparty_bank_code() {
        return counterparty_bank_code;
    }

    public void setCounterparty_bank_code(String counterparty_bank_code) {
        this.counterparty_bank_code = counterparty_bank_code;
    }

    public String getCounterparty_bic() {
        return counterparty_bic;
    }

    public void setCounterparty_bic(String counterparty_bic) {
        this.counterparty_bic = counterparty_bic;
    }

    public String getCounterparty_bank_address() {
        return counterparty_bank_address;
    }

    public void setCounterparty_bank_address(String counterparty_bank_address) {
        this.counterparty_bank_address = counterparty_bank_address;
    }

    public String getCounterparty_country_code() {
        return counterparty_country_code;
    }

    public void setCounterparty_country_code(String counterparty_country_code) {
        this.counterparty_country_code = counterparty_country_code;
    }

    public String getCounterparty_business() {
        return counterparty_business;
    }

    public void setCounterparty_business(String counterparty_business) {
        this.counterparty_business = counterparty_business;
    }

    public String getTeller_number() {
        return teller_number;
    }

    public void setTeller_number(String teller_number) {
        this.teller_number = teller_number;
    }

    public String getSequence_number() {
        return sequence_number;
    }

    public void setSequence_number(String sequence_number) {
        this.sequence_number = sequence_number;
    }

    public String getReservedfield1() {
        return reservedfield1;
    }

    public void setReservedfield1(String reservedfield1) {
        this.reservedfield1 = reservedfield1;
    }

    public String getReservedfield2() {
        return reservedfield2;
    }

    public void setReservedfield2(String reservedfield2) {
        this.reservedfield2 = reservedfield2;
    }

    public String getReservedfield3() {
        return reservedfield3;
    }

    public void setReservedfield3(String reservedfield3) {
        this.reservedfield3 = reservedfield3;
    }

    public String getReservedfield4() {
        return reservedfield4;
    }

    public void setReservedfield4(String reservedfield4) {
        this.reservedfield4 = reservedfield4;
    }

    public String getReservedfield5() {
        return reservedfield5;
    }

    public void setReservedfield5(String reservedfield5) {
        this.reservedfield5 = reservedfield5;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
