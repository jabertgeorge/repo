package clari5.custom.dev;

import clari5.aml.web.wl.search.WlQuery;
import clari5.aml.web.wl.search.WlRecordDetails;
import clari5.aml.web.wl.search.WlResult;
import clari5.aml.web.wl.search.WlSearcher;
import clari5.aml.wle.MatchedRecord;
import clari5.aml.wle.MatchedResults;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxRest;
import clari5.platform.util.Hocon;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * @author manoj kumar shah
 */

public class WatchListFileMatcher {

    private static final CxpsLogger logger = CxpsLogger.getLogger(WatchListFileMatcher.class);
    static List<String> descriptionList = new ArrayList<>();
    static List<String> sanctionReferenceList = new ArrayList<>();
    static Hocon dmsSequenceReader;

    static {
        dmsSequenceReader = new Hocon();
        dmsSequenceReader.loadFromContext("dms_sequence.conf");
        String[] splitDescription = dmsSequenceReader.getString("sequence.descriptionCode").split(",");
        String[] splitSanctionReferences = dmsSequenceReader.getString("sequence.sanctionsreferences_ListProviderCode").split(",");

        descriptionList.addAll(Arrays.asList(splitDescription));
        sanctionReferenceList.addAll(Arrays.asList(splitSanctionReferences));
    }

    private String url = "";
    private JSONObject obj;

    private String name;
    private String dob;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    private String nationality;
    private String nic;
    private String passport;

    public WatchListFileMatcher(String url, JSONObject obj) {
        this.url = url;
        this.obj = obj;
    }

    public JSONArray createSequence(JSONArray jsonArray) {

        JSONArray jsArray = null;
        try {
            if (jsonArray == null) return null;

            String descriptionSequence = "";

            for (int i = 0; i < jsonArray.length(); i++) {

                if (((JSONObject) jsonArray.get(i)).has("details") && ((JSONObject) jsonArray.get(i)).has("listName")
                        && ((JSONObject) jsonArray.get(i)).getString("listName").equalsIgnoreCase("dj")) {

                    JSONObject jsonObject = new JSONObject(((JSONObject) jsonArray.get(i)).getString("details"));

                    if (jsonObject.has("dob"))
                        ((JSONObject) jsonArray.get(i)).put("dob", jsonObject.getString("dob"));
                    else ((JSONObject) jsonArray.get(i)).put("dob", "");

                    if (jsonObject.has("addr1"))
                        ((JSONObject) jsonArray.get(i)).put("addr1", jsonObject.getString("addr1"));
                    else ((JSONObject) jsonArray.get(i)).put("addr1", "");

                    if (jsonObject.has("passport"))
                        ((JSONObject) jsonArray.get(i)).put("identityNumber", jsonObject.getString("passport"));
                    else ((JSONObject) jsonArray.get(i)).put("identityNumber", "");

                    if (jsonObject.has("name"))
                        ((JSONObject) jsonArray.get(i)).put("sn", jsonObject.getString("name"));
                    else ((JSONObject) jsonArray.get(i)).put("sn", "");

                    if (jsonObject.has("country"))
                        ((JSONObject) jsonArray.get(i)).put("country", jsonObject.getString("country"));
                    else ((JSONObject) jsonArray.get(i)).put("country", "");

                    /*if (((JSONObject) jsonArray.get(i)).has("fields")) {
                        jsArray = ((JSONObject) jsonArray.get(i)).getJSONArray("fields");
                        for (int j = 0; j < jsArray.length(); j++) {
                            if (((JSONObject) jsArray.get(j)).has("name") && ((JSONObject) jsArray.get(j)).getString("name").equalsIgnoreCase("name")) {
                                ((JSONObject) jsonArray.get(i)).put("sn", ((JSONObject) jsArray.get(j)).getString("value"));
                            } else {
                                ((JSONObject) jsonArray.get(i)).put("sn", "");
                            }
                        }
                    }*/


                    if (jsonObject.has("sanctionsreferences_ListProviderCode")) {

                        List<String> srlpList = new ArrayList<>(Arrays.asList(jsonObject.
                                getString("sanctionsreferences_ListProviderCode").replaceAll(",", "").trim().split("\\|")));

                        for (int j = 0; j < sanctionReferenceList.size(); j++) {
                            if (srlpList.contains(sanctionReferenceList.get(j))) {
                                descriptionSequence += sanctionReferenceList.get(j).trim() + "|";
                            } else continue;
                        }
                    }

                    if (jsonObject.has("descriptionCode")) {

                        List<String> descriptionCode = new ArrayList<>(Arrays.asList(jsonObject.
                                getString("descriptionCode").replaceAll(", ", "").trim().split("\\|")));

                        for (int j = 0; j < descriptionList.size(); j++) {
                            if (descriptionCode.contains(descriptionList.get(j))) {
                                descriptionSequence += descriptionList.get(j).trim() + "|";
                            } else continue;
                        }
                    }

                    ((JSONObject) jsonArray.get(i)).put("listName", descriptionSequence == "" ?
                            ((JSONObject) jsonArray.get(i)).getString("listName") : descriptionSequence);

                    descriptionSequence = "";

                } else if (((JSONObject) jsonArray.get(i)).has("details") && ((JSONObject) jsonArray.get(i)).has("listName")
                        && ((JSONObject) jsonArray.get(i)).getString("listName").equalsIgnoreCase("custom")){

                    JSONObject jsonObject = new JSONObject(((JSONObject) jsonArray.get(i)).getString("details"));

                    if (jsonObject.has("dateofbirth"))
                        ((JSONObject) jsonArray.get(i)).put("dob", jsonObject.getString("dateofbirth"));
                    else ((JSONObject) jsonArray.get(i)).put("dob", "");

                    if (jsonObject.has("address1"))
                        ((JSONObject) jsonArray.get(i)).put("address1", jsonObject.getString("address1"));
                    else ((JSONObject) jsonArray.get(i)).put("address1", "");

                    if (jsonObject.has("identitynumber1"))
                        ((JSONObject) jsonArray.get(i)).put("identityNumber", jsonObject.getString("identitynumber1"));
                    else ((JSONObject) jsonArray.get(i)).put("identityNumber", "");

                    if (jsonObject.has("entityname"))
                        ((JSONObject) jsonArray.get(i)).put("sn", jsonObject.getString("entityname"));
                    else ((JSONObject) jsonArray.get(i)).put("sn", "");

                    if (jsonObject.has("country"))
                        ((JSONObject) jsonArray.get(i)).put("country", jsonObject.getString("country"));
                    else ((JSONObject) jsonArray.get(i)).put("country", "");

                    /*if (((JSONObject) jsonArray.get(i)).has("fields")) {
                        jsArray = ((JSONObject) jsonArray.get(i)).getJSONArray("fields");
                        for (int j = 0; j < jsArray.length(); j++) {
                            if (((JSONObject) jsArray.get(j)).has("name") && ((JSONObject) jsArray.get(j)).getString("name").equalsIgnoreCase("name")) {
                                ((JSONObject) jsonArray.get(i)).put("sn", ((JSONObject) jsArray.get(j)).getString("value"));
                            } else {
                                ((JSONObject) jsonArray.get(i)).put("sn", "");
                            }
                        }
                    }*/
                } else continue;
            }
            return jsonArray;
        } catch (NullPointerException e) {
            System.out.println("Either Description or Sanction references data is null. Please " +
                    "check the response...");
        } catch (ArrayIndexOutOfBoundsException aio) {
            System.out.println("index overflow... " + aio.getMessage());
        }
        return jsonArray;
    }


    public String getMatchScore() throws UnsupportedEncodingException {
        String temp = "Negative";
        //url=System.getenv("DN")+"/efm/wlsearch?q="+ URLEncoder.encode(obj.toString());
        url = url + "?q=" + URLEncoder.encode(obj.toString(), "UTF-8");
        //getHttpResponse(obj,url);

        String status = "";
        String s = getHttpResponse(obj, url);
        JSONObject jsonObject = null;
        if (s == null || s.trim().equals("")) {
            return "{}";
        }
        jsonObject = new JSONObject(s);
        System.out.println(s);

        double score = 0;
        double maxscore = 0;
        try {
            score = jsonObject.getJSONObject("matchedResults").getDouble("maxScore");
            maxscore = score * 100;
            jsonObject.getJSONObject("matchedResults").put("maxScore", maxscore);
        } catch (Exception e) {
            System.out.println("Below is a handled exception.");
            e.printStackTrace();
        }
        status = jsonObject.getJSONObject("status").get("status").toString();
        double maxNameScore = 0;

        try {
            if (status.equalsIgnoreCase("success")) {

                JSONArray jsonArray = null;
                jsonArray = jsonObject.getJSONObject("matchedResults").getJSONArray("matchedRecords");
                jsonArray = createSequence(jsonArray);

                for (int i = 0; i < jsonArray.length(); i++) {
                    String fid = ((JSONObject) jsonArray.get(i)).getString("id");
                    JSONArray fieldArray = ((JSONObject) jsonArray.get(i)).getJSONArray("fields");
                    for (int j = 0; j < fieldArray.length(); j++) {
                        String fname = ((JSONObject) fieldArray.get(j)).getString("name");
                        double fscore = ((JSONObject) fieldArray.get(j)).getDouble("score");

                        if (("passport".equalsIgnoreCase(fname) && fscore == 1.0) || ("nic".equalsIgnoreCase(fname) && fscore == 1.0)) {
                            if (fid != null && fid.toUpperCase().startsWith("CUSTOM")) {
                                temp = "Critical";
                                break;
                            } else {
                                temp = "Positive";
                            }
                        } else if ("name".equalsIgnoreCase(fname) && maxNameScore < fscore) {
                            maxNameScore = fscore;
                        }
                    }
                    if ("Critical".equalsIgnoreCase(temp))
                        break;
                }
                if (!temp.equalsIgnoreCase("Critical")) {
                    if (maxNameScore > 0.79) {
                        temp = "Positive";
                    } else if (!"Positive".equalsIgnoreCase(temp)) {
                        temp = "Negative";
                    }
                }

            }
            saveWatchListAudit(score, temp);
            jsonObject.put("prodstatus", jsonObject.get("status"));
            jsonObject.put("status", temp);
            return jsonObject.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        jsonObject.put("prodstatus", jsonObject.get("status"));
        jsonObject.put("status", temp);
        return jsonObject.toString();
    }


    public String getHttpResponse(JSONObject obj, String url) {
        /*HttpResponse resp = null;
        String response = "";
        String instanceId = System.getenv("INSTANCEID");
        String appSecret = System.getenv("APPSECRET");
        try {
            System.out.println("WL_ONBOARDING_CUST : url = " + url);
            //resp= CxRest.get(url).header("accept", "application/json").header("Content-Type", "application/json").header("mode", "PROG").queryString("msg", obj.toString()).basicAuth(instanceId, appSecret).asString();
            resp = CxRest.get(url).header("accept", "application/json")
                    .header("Content-Type", "application/json")
                    .header("mode", "PROG").queryString("msg", obj.toString())
                    .basicAuth(instanceId, appSecret).asString();
            response = (String) resp.getBody();
            System.out.println("WL_ONBOARDING_CUST : Response = " + response + " : " + resp.getStatus());
        } catch (UnirestException e) {
            e.printStackTrace();
            System.out.println("WL_ONBOARDING_CUST : WatchList not reachable" + e);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("WL_ONBOARDING_CUST : Exception in parsing" + e);
        }
        return response;*/
        String response = "";
        try {
            WlQuery query = new WlQuery();
            query.from(obj.toString());

            logger.info("Query object created from json : "+ obj.toString());

            WlSearcher searcher = new WlSearcher();
            WlResult result = searcher.search(query);
            MatchedResults matched = result.getMatchedResults();
            Collection<MatchedRecord> recordList = matched.getMatchedRecords();
            for(MatchedRecord record : recordList) {
                WlRecordDetails wlDetails = new WlRecordDetails() ;
                String outputDetails = wlDetails.getRecordDetails(record.getDocId());

                record.setDetails(outputDetails);

            }

            response = result.toJson();
            logger.info("Response for online DMS fetched: " + response);
        } catch (Exception e) {
            logger.error("unable to fetch response from WL for URL: ---> " + obj.toString());
            e.getCause();
            e.printStackTrace();
        }
        return response;
    }

    public boolean saveWatchListAudit(double score, String status) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO " + tableName() + " (entity_id,created_date,name,nationality,dob,nic,passport,max_score,status) VALUES (NEXT VALUE FOR WATCHLIST_API_AUDIT_seq,GETDATE(),?, ?, ?, ?, ?,?,?);");
            int i = 1;
            ps.setString(i++, name);
            ps.setString(i++, nationality);
            ps.setString(i++, dob);
            ps.setString(i++, nic);
            ps.setString(i++, passport);
            ps.setString(i++, score + "");
            ps.setString(i++, status);
            ps.executeUpdate();
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static CxConnection getConnection() {
        //Clari5.batchBootstrap("test", "efm-clari5.conf");

        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
    }

    public String tableName() {
        return "WATCHLIST_API_AUDIT";
    }

    public static void main(String[] ar) {
        //WatchListFileMatcher watchListFileMatcher=new WatchListFileMatcher("");
        //System.out.print(watchListFileMatcher.getMatchScore());

    }
}
