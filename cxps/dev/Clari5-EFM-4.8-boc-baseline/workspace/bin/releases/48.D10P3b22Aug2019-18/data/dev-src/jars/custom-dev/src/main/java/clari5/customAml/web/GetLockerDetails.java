package clari5.customAml.web;

import java.sql.*;
import clari5.rdbms.Rdbms;
import clari5.hfdb.mappers.HfdbMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.Arrays;
import cxps.apex.utils.CxpsLogger;

public class GetLockerDetails {
    CxpsLogger logger = CxpsLogger.getLogger(GetLockerDetails.class);

    private static String lockerNum="";
    JSONObject obj = new JSONObject();
    JSONObject obj1 = new JSONObject();

    GetLockerDetails(String payload){
        this.lockerNum="";
        JSONObject jsonObject = new JSONObject(payload);
        String facNum = jsonObject.getString("lockerNum");
        String[] parts = facNum.split("--");
        this.lockerNum=parts[0];
    }

    public synchronized String getLockerDetails(String custId){
        JSONObject obj = new JSONObject();
        String lockerDetails=null;
        try {
            logger.debug("getLockerDetails:lockerNum:"+this.lockerNum);
            if ((this.lockerNum) == null) {
                logger.error("Trying to fetch trade details. lockerNum not found");
                return null;
            }
            lockerDetails=getDetails(this.lockerNum,custId);
            logger.debug("response custEmpTree:"+obj.toString());
            return lockerDetails;
        }catch (Exception e){
            e.printStackTrace();
        }
        return lockerDetails;
    }

    protected String getDetails(String lockerNum,String custId){
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        PreparedStatement stmt = null;
        Statement stmtNic = null;
        String nic = null;
        try{
            conn=Rdbms.getAppConnection();
            String sqlNic = "select BranchCode from SAFEDEP_MASTER where CORE_CIFID_1='"+custId+"'";
            //System.out.println(sqlNic);
            stmtNic = conn.createStatement();
            ResultSet nrc = stmtNic.executeQuery(sqlNic);
            while(nrc.next()){
                nic = nrc.getString("BranchCode");
            }
            String sql="select * from SAFEDEP_MASTER where Locker_Number=? and BranchCode =?";
            logger.debug("query to get lockerDetails : "+sql);
            stmt=conn.prepareStatement(sql);
            stmt.setString(1,lockerNum);
            stmt.setString(2,nic);
           // System.out.println(sql);
            ResultSet rs = stmt.executeQuery();
            while( rs.next() ){
                obj.put("custId", rs.getString("CORE_CIFID_1")!= null ? rs.getString("CORE_CIFID_1") : "");
                obj.put("locker_num", rs.getString("Locker_Number")!= null ? rs.getString("Locker_Number") : "");
                obj.put("open_date", rs.getString("Locker_Opened_Date")!= null ? rs.getString("Locker_Opened_Date") : "");
                obj.put("close_date", rs.getString("Closed_Date")!= null ? rs.getString("Closed_Date") : "");
                obj.put("status",rs.getString("Status")!= null ? rs.getString("Status") : "");
                obj.put("BranchCode",rs.getString("BranchCode")!= null ? rs.getString("BranchCode") : "");
                jarray.put(obj);
            }
            obj1.put("lockerDetails", jarray);
            logger.debug("getDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting locker details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }
}