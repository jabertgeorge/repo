package clari5.kyc.custom.rest;



import clari5.kyc.custom.KycException;
import clari5.uac.exceptions.AuthException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.HashMap;
import java.util.List;

@Path("/kyc")
public interface KycServices {
    @GET
    @Path("/getDelta")
    String initializeKyc(@Context SecurityContext ctx) throws KycException;

    @GET
    @Path("/fetchCustomerDetails/{custId}")
    String getCustomerDetails(@PathParam("custId") String custId, @Context SecurityContext ctx) throws KycException, AuthException;

    @POST
    @Path("/updateDetails")
    String updateDetails(String custDetails,@Context SecurityContext ctx) throws KycException ,Exception;

    @POST
    @Path("/verifyChanges")
    String verifyKycUpdate(String kycChangeDetails,@Context SecurityContext ctx) throws KycException ,Exception;
}
