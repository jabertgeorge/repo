// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_TravelTxnEventMapper extends EventMapper<FT_TravelTxnEvent> {

public FT_TravelTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_TravelTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_TravelTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_TravelTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_TravelTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_TravelTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_TravelTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setDouble(i++, obj.getLcyAmt());
            preparedStatement.setTimestamp(i++, obj.getSystemTime());
            preparedStatement.setDouble(i++, obj.getLcyCurr());
            preparedStatement.setString(i++, obj.getTxnStatus());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getCardNo());
            preparedStatement.setString(i++, obj.getTxnCntryCode());
            preparedStatement.setString(i++, obj.getTxnCntry());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getTxnCode());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setTimestamp(i++, obj.getTxnDate());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getSourceOrDestAcctId());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getTxnCurr());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getTranSrlNo());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getSystemCountry());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_TRAVELTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_TravelTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_TravelTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRAVELTXN"));
        putList = new ArrayList<>();

        for (FT_TravelTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "EVENT_TIME",  obj.getEventTime());
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "LCY_AMT", String.valueOf(obj.getLcyAmt()));
            p = this.insert(p, "SYSTEM_TIME", String.valueOf(obj.getSystemTime()));
            p = this.insert(p, "LCY_CURR", String.valueOf(obj.getLcyCurr()));
            p = this.insert(p, "TXN_STATUS",  obj.getTxnStatus());
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CARD_NO",  obj.getCardNo());
            p = this.insert(p, "TXN_CNTRY_CODE",  obj.getTxnCntryCode());
            p = this.insert(p, "TXN_CNTRY",  obj.getTxnCntry());
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "TXN_CODE",  obj.getTxnCode());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "TXN_DATE", String.valueOf(obj.getTxnDate()));
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "SOURCE_OR_DEST_ACCT_ID",  obj.getSourceOrDestAcctId());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "TXN_CURR",  obj.getTxnCurr());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "TRAN_SRL_NO",  obj.getTranSrlNo());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_TRAVELTXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_TRAVELTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_TRAVELTXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_TravelTxnEvent obj = new FT_TravelTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setEventTime(rs.getString("EVENT_TIME"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setLcyAmt(rs.getDouble("LCY_AMT"));
    obj.setSystemTime(rs.getTimestamp("SYSTEM_TIME"));
    obj.setLcyCurr(rs.getDouble("LCY_CURR"));
    obj.setTxnStatus(rs.getString("TXN_STATUS"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCardNo(rs.getString("CARD_NO"));
    obj.setTxnCntryCode(rs.getString("TXN_CNTRY_CODE"));
    obj.setTxnCntry(rs.getString("TXN_CNTRY"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setTxnCode(rs.getString("TXN_CODE"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setTxnDate(rs.getTimestamp("TXN_DATE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setSourceOrDestAcctId(rs.getString("SOURCE_OR_DEST_ACCT_ID"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setTxnCurr(rs.getString("TXN_CURR"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setTranSrlNo(rs.getString("TRAN_SRL_NO"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_TRAVELTXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_TravelTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_TravelTxnEvent> events;
 FT_TravelTxnEvent obj = new FT_TravelTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRAVELTXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_TravelTxnEvent obj = new FT_TravelTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setEventTime(getColumnValue(rs, "EVENT_TIME"));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setLcyAmt(EventHelper.toDouble(getColumnValue(rs, "LCY_AMT")));
            obj.setSystemTime(EventHelper.toTimestamp(getColumnValue(rs, "SYSTEM_TIME")));
            obj.setLcyCurr(EventHelper.toDouble(getColumnValue(rs, "LCY_CURR")));
            obj.setTxnStatus(getColumnValue(rs, "TXN_STATUS"));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCardNo(getColumnValue(rs, "CARD_NO"));
            obj.setTxnCntryCode(getColumnValue(rs, "TXN_CNTRY_CODE"));
            obj.setTxnCntry(getColumnValue(rs, "TXN_CNTRY"));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setTxnCode(getColumnValue(rs, "TXN_CODE"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setTxnDate(EventHelper.toTimestamp(getColumnValue(rs, "TXN_DATE")));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setSourceOrDestAcctId(getColumnValue(rs, "SOURCE_OR_DEST_ACCT_ID"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setTxnCurr(getColumnValue(rs, "TXN_CURR"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setTranSrlNo(getColumnValue(rs, "TRAN_SRL_NO"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRAVELTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRAVELTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"MSG_SOURCE\",\"KEYS\",\"CHANNEL\",\"TRAN_ID\",\"EVENT_TIME\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"LCY_AMT\",\"SYSTEM_TIME\",\"LCY_CURR\",\"TXN_STATUS\",\"TXN_DR_CR\",\"CX_ACCT_ID\",\"TXN_AMT\",\"HOST_USER_ID\",\"HOST_ID\",\"CARD_NO\",\"TXN_CNTRY_CODE\",\"TXN_CNTRY\",\"ADD_ENTITY_ID2\",\"TXN_CODE\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"SOURCE_OR_DEST_ACCT_ID\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"TXN_CURR\",\"CUST_ID\",\"TRAN_SRL_NO\",\"CX_CUST_ID\",\"EVENT_NAME\",\"TRAN_TYPE\",\"SYSTEM_COUNTRY\"" +
              " FROM EVENT_FT_TRAVELTXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [MSG_SOURCE],[KEYS],[CHANNEL],[TRAN_ID],[EVENT_TIME],[ONLINE_BATCH],[TRAN_SUB_TYPE],[LCY_AMT],[SYSTEM_TIME],[LCY_CURR],[TXN_STATUS],[TXN_DR_CR],[CX_ACCT_ID],[TXN_AMT],[HOST_USER_ID],[HOST_ID],[CARD_NO],[TXN_CNTRY_CODE],[TXN_CNTRY],[ADD_ENTITY_ID2],[TXN_CODE],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[SOURCE_OR_DEST_ACCT_ID],[ADD_ENTITY_ID5],[EVENT_TYPE],[TXN_CURR],[CUST_ID],[TRAN_SRL_NO],[CX_CUST_ID],[EVENT_NAME],[TRAN_TYPE],[SYSTEM_COUNTRY]" +
              " FROM EVENT_FT_TRAVELTXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`MSG_SOURCE`,`KEYS`,`CHANNEL`,`TRAN_ID`,`EVENT_TIME`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`LCY_AMT`,`SYSTEM_TIME`,`LCY_CURR`,`TXN_STATUS`,`TXN_DR_CR`,`CX_ACCT_ID`,`TXN_AMT`,`HOST_USER_ID`,`HOST_ID`,`CARD_NO`,`TXN_CNTRY_CODE`,`TXN_CNTRY`,`ADD_ENTITY_ID2`,`TXN_CODE`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`SOURCE_OR_DEST_ACCT_ID`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`TXN_CURR`,`CUST_ID`,`TRAN_SRL_NO`,`CX_CUST_ID`,`EVENT_NAME`,`TRAN_TYPE`,`SYSTEM_COUNTRY`" +
              " FROM EVENT_FT_TRAVELTXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_TRAVELTXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"MSG_SOURCE\",\"KEYS\",\"CHANNEL\",\"TRAN_ID\",\"EVENT_TIME\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"LCY_AMT\",\"SYSTEM_TIME\",\"LCY_CURR\",\"TXN_STATUS\",\"TXN_DR_CR\",\"CX_ACCT_ID\",\"TXN_AMT\",\"HOST_USER_ID\",\"HOST_ID\",\"CARD_NO\",\"TXN_CNTRY_CODE\",\"TXN_CNTRY\",\"ADD_ENTITY_ID2\",\"TXN_CODE\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"TXN_DATE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"SOURCE_OR_DEST_ACCT_ID\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"TXN_CURR\",\"CUST_ID\",\"TRAN_SRL_NO\",\"CX_CUST_ID\",\"EVENT_NAME\",\"TRAN_TYPE\",\"SYSTEM_COUNTRY\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[MSG_SOURCE],[KEYS],[CHANNEL],[TRAN_ID],[EVENT_TIME],[ONLINE_BATCH],[TRAN_SUB_TYPE],[LCY_AMT],[SYSTEM_TIME],[LCY_CURR],[TXN_STATUS],[TXN_DR_CR],[CX_ACCT_ID],[TXN_AMT],[HOST_USER_ID],[HOST_ID],[CARD_NO],[TXN_CNTRY_CODE],[TXN_CNTRY],[ADD_ENTITY_ID2],[TXN_CODE],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[TXN_DATE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[SOURCE_OR_DEST_ACCT_ID],[ADD_ENTITY_ID5],[EVENT_TYPE],[TXN_CURR],[CUST_ID],[TRAN_SRL_NO],[CX_CUST_ID],[EVENT_NAME],[TRAN_TYPE],[SYSTEM_COUNTRY]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`MSG_SOURCE`,`KEYS`,`CHANNEL`,`TRAN_ID`,`EVENT_TIME`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`LCY_AMT`,`SYSTEM_TIME`,`LCY_CURR`,`TXN_STATUS`,`TXN_DR_CR`,`CX_ACCT_ID`,`TXN_AMT`,`HOST_USER_ID`,`HOST_ID`,`CARD_NO`,`TXN_CNTRY_CODE`,`TXN_CNTRY`,`ADD_ENTITY_ID2`,`TXN_CODE`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`TXN_DATE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`SOURCE_OR_DEST_ACCT_ID`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`TXN_CURR`,`CUST_ID`,`TRAN_SRL_NO`,`CX_CUST_ID`,`EVENT_NAME`,`TRAN_TYPE`,`SYSTEM_COUNTRY`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

