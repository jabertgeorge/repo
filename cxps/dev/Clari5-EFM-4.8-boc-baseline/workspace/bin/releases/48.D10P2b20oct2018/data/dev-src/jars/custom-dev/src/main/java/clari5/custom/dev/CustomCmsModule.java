package clari5.custom.dev;

//import clari5.aml.cms.newjira.CustomField;

import clari5.aml.cms.newjira.JiraInstance;
import clari5.platform.util.Hocon;
import cxps.apex.exceptions.RuntimeFatalException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by yashaswi on 25/9/17.
 */
public class CustomCmsModule {
    private String projectCode;
    private JiraInstance instance;
    private String userGroup;
    private Map<String, CustomField> customFields = new HashMap<String, CustomField>();
    private Map<String, String> departments = new HashMap<String, String>();
    private static Map<String, CustomCmsModule> moduleMap = new HashMap<>();

    public static CustomCmsModule getInstance(String moduleId) {
        if (moduleMap.get(moduleId) == null) {
            CustomCmsModule module = new CustomCmsModule(moduleId);
            moduleMap.put(moduleId, module);
        }
        return moduleMap.get(moduleId);
    }

    private CustomCmsModule(String moduleId) {
        this.load(moduleId);
    }

    private void load(String moduleId) {
        Hocon appCfg = new Hocon();
        appCfg.loadFromContext("newcmsjira.conf");


        Hocon cmsJiraCfg = appCfg.get("clari5.aml.cms.newjira.cmsjira");
        if (cmsJiraCfg == null) {
            throw new RuntimeFatalException.ResourceConfigError("CMSJIRA", "Unable to load CMS JIRA Config");
        }
        try {
            this.from(cmsJiraCfg.get(moduleId));
        } catch (Exception e) {
            e.printStackTrace(); //TODO remove if unnecessary
            throw new RuntimeFatalException.ResourceConfigError("CMSJIRA", "Unable to load config for [" + moduleId + "]");
        }
    }

    public void from(Hocon h) throws Exception {
        Hocon customFieldsHocon = h.get("customFields");
        if (customFieldsHocon != null) {
            Set fieldNames = customFieldsHocon.getKeys();
            Iterator var4 = fieldNames.iterator();

            while (var4.hasNext()) {
                String fieldName = (String) var4.next();
                this.customFields.put(fieldName, new CustomField(customFieldsHocon.get(fieldName)));
            }
        }
    }

    public String getUserGroup() {
        return this.userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public JiraInstance getInstance() {
        return this.instance;
    }

    public void setInstance(JiraInstance instance) {
        this.instance = instance;
    }

    Map<String, CustomField> getCustomFields() {
        return this.customFields;
    }

    void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectCode() {
        return this.projectCode;
    }

    public Map<String, String> getDepartments() {
        return this.departments;
    }
}
