package clari5.custom.boc.integration.exceptions;

public class ConfigurationException  extends Exception {
	private static final long serialVersionUID = -7718700911069114338L;
	public ConfigurationException(String message) {
		super(message);
	}
}