package clari5.custom.dev.daemons;

import clari5.custom.dev.ProcessSwiftFile;
import clari5.platform.applayer.CxpsDaemon;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.cmq.CxqDaemon;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.fileq.Clari5Payload;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class MTDaemon extends CxpsRunnable implements ICxResource{
    @Override
    protected Object getData() throws RuntimeFatalException {
        return 0;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        try {
            ProcessSwiftFile.process();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void configure(Hocon h) {

        System.out.println("MT Configured");
    }
}
