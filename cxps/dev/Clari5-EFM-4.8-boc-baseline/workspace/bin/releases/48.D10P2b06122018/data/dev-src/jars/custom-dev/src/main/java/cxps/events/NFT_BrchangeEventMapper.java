// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_BrchangeEventMapper extends EventMapper<NFT_BrchangeEvent> {

public NFT_BrchangeEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_BrchangeEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_BrchangeEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_BrchangeEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_BrchangeEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_BrchangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_BrchangeEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getFinalBranchId());
            preparedStatement.setString(i++, obj.getSystemCountry());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_BRCHANGE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_BrchangeEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_BrchangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_BRCHANGE"));
        putList = new ArrayList<>();

        for (NFT_BrchangeEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "FINAL_BRANCH_ID",  obj.getFinalBranchId());
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_BRCHANGE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_BRCHANGE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_BRCHANGE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_BrchangeEvent obj = new NFT_BrchangeEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setFinalBranchId(rs.getString("FINAL_BRANCH_ID"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_BRCHANGE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_BrchangeEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_BrchangeEvent> events;
 NFT_BrchangeEvent obj = new NFT_BrchangeEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_BRCHANGE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_BrchangeEvent obj = new NFT_BrchangeEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setFinalBranchId(getColumnValue(rs, "FINAL_BRANCH_ID"));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_BRCHANGE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_BRCHANGE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"MSG_SOURCE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"KEYS\",\"CHANNEL\",\"BRANCH_ID\",\"CUST_ID\",\"EVENTTS\",\"CX_CUST_ID\",\"EVENT_NAME\",\"CX_ACCT_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"FINAL_BRANCH_ID\",\"SYSTEM_COUNTRY\"" +
              " FROM EVENT_NFT_BRCHANGE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[MSG_SOURCE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[KEYS],[CHANNEL],[BRANCH_ID],[CUST_ID],[EVENTTS],[CX_CUST_ID],[EVENT_NAME],[CX_ACCT_ID],[HOST_USER_ID],[HOST_ID],[FINAL_BRANCH_ID],[SYSTEM_COUNTRY]" +
              " FROM EVENT_NFT_BRCHANGE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`MSG_SOURCE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`KEYS`,`CHANNEL`,`BRANCH_ID`,`CUST_ID`,`EVENTTS`,`CX_CUST_ID`,`EVENT_NAME`,`CX_ACCT_ID`,`HOST_USER_ID`,`HOST_ID`,`FINAL_BRANCH_ID`,`SYSTEM_COUNTRY`" +
              " FROM EVENT_NFT_BRCHANGE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_BRCHANGE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"MSG_SOURCE\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"KEYS\",\"CHANNEL\",\"BRANCH_ID\",\"CUST_ID\",\"EVENTTS\",\"CX_CUST_ID\",\"EVENT_NAME\",\"CX_ACCT_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"FINAL_BRANCH_ID\",\"SYSTEM_COUNTRY\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[MSG_SOURCE],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[ADD_ENTITY_ID5],[EVENT_TYPE],[KEYS],[CHANNEL],[BRANCH_ID],[CUST_ID],[EVENTTS],[CX_CUST_ID],[EVENT_NAME],[CX_ACCT_ID],[HOST_USER_ID],[HOST_ID],[FINAL_BRANCH_ID],[SYSTEM_COUNTRY]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`MSG_SOURCE`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`KEYS`,`CHANNEL`,`BRANCH_ID`,`CUST_ID`,`EVENTTS`,`CX_CUST_ID`,`EVENT_NAME`,`CX_ACCT_ID`,`HOST_USER_ID`,`HOST_ID`,`FINAL_BRANCH_ID`,`SYSTEM_COUNTRY`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

