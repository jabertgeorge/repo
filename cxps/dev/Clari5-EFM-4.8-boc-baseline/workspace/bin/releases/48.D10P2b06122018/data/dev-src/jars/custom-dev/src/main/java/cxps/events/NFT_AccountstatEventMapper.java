// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AccountstatEventMapper extends EventMapper<NFT_AccountstatEvent> {

public NFT_AccountstatEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AccountstatEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AccountstatEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AccountstatEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AccountstatEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AccountstatEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AccountstatEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getEntityType());
            preparedStatement.setString(i++, obj.getMsgSource());
            preparedStatement.setString(i++, obj.getKeys());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getCustRefId());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setTimestamp(i++, obj.getEventTime());
            preparedStatement.setString(i++, obj.getCxAcctId());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getAddEntityId2());
            preparedStatement.setString(i++, obj.getAddEntityId1());
            preparedStatement.setString(i++, obj.getAddEntityId4());
            preparedStatement.setString(i++, obj.getAddEntityId3());
            preparedStatement.setString(i++, obj.getEventSubtype());
            preparedStatement.setString(i++, obj.getSourceOrDestAcctId());
            preparedStatement.setString(i++, obj.getAddEntityId5());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getInitAcctStatus());
            preparedStatement.setString(i++, obj.getCxCustId());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getFinalAcctStatus());
            preparedStatement.setString(i++, obj.getStatus());
            preparedStatement.setString(i++, obj.getSystemCountry());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_ACCOUNTSTAT]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AccountstatEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AccountstatEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCOUNTSTAT"));
        putList = new ArrayList<>();

        for (NFT_AccountstatEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ENTITY_TYPE",  obj.getEntityType());
            p = this.insert(p, "MSG_SOURCE",  obj.getMsgSource());
            p = this.insert(p, "KEYS",  obj.getKeys());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "CUST_REF_ID",  obj.getCustRefId());
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "EVENT_TIME", String.valueOf(obj.getEventTime()));
            p = this.insert(p, "CX_ACCT_ID",  obj.getCxAcctId());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "ADD_ENTITY_ID2",  obj.getAddEntityId2());
            p = this.insert(p, "ADD_ENTITY_ID1",  obj.getAddEntityId1());
            p = this.insert(p, "ADD_ENTITY_ID4",  obj.getAddEntityId4());
            p = this.insert(p, "ADD_ENTITY_ID3",  obj.getAddEntityId3());
            p = this.insert(p, "EVENT_SUBTYPE",  obj.getEventSubtype());
            p = this.insert(p, "SOURCE_OR_DEST_ACCT_ID",  obj.getSourceOrDestAcctId());
            p = this.insert(p, "ADD_ENTITY_ID5",  obj.getAddEntityId5());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "INIT_ACCT_STATUS",  obj.getInitAcctStatus());
            p = this.insert(p, "CX_CUST_ID",  obj.getCxCustId());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "FINAL_ACCT_STATUS",  obj.getFinalAcctStatus());
            p = this.insert(p, "STATUS",  obj.getStatus());
            p = this.insert(p, "SYSTEM_COUNTRY",  obj.getSystemCountry());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_ACCOUNTSTAT"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_ACCOUNTSTAT]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_ACCOUNTSTAT]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AccountstatEvent obj = new NFT_AccountstatEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setEntityType(rs.getString("ENTITY_TYPE"));
    obj.setMsgSource(rs.getString("MSG_SOURCE"));
    obj.setKeys(rs.getString("KEYS"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setCustRefId(rs.getString("CUST_REF_ID"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setEventTime(rs.getTimestamp("EVENT_TIME"));
    obj.setCxAcctId(rs.getString("CX_ACCT_ID"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setAddEntityId2(rs.getString("ADD_ENTITY_ID2"));
    obj.setAddEntityId1(rs.getString("ADD_ENTITY_ID1"));
    obj.setAddEntityId4(rs.getString("ADD_ENTITY_ID4"));
    obj.setAddEntityId3(rs.getString("ADD_ENTITY_ID3"));
    obj.setEventSubtype(rs.getString("EVENT_SUBTYPE"));
    obj.setSourceOrDestAcctId(rs.getString("SOURCE_OR_DEST_ACCT_ID"));
    obj.setAddEntityId5(rs.getString("ADD_ENTITY_ID5"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setInitAcctStatus(rs.getString("INIT_ACCT_STATUS"));
    obj.setCxCustId(rs.getString("CX_CUST_ID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setFinalAcctStatus(rs.getString("FINAL_ACCT_STATUS"));
    obj.setStatus(rs.getString("STATUS"));
    obj.setSystemCountry(rs.getString("SYSTEM_COUNTRY"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_ACCOUNTSTAT]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AccountstatEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AccountstatEvent> events;
 NFT_AccountstatEvent obj = new NFT_AccountstatEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCOUNTSTAT"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AccountstatEvent obj = new NFT_AccountstatEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setEntityType(getColumnValue(rs, "ENTITY_TYPE"));
            obj.setMsgSource(getColumnValue(rs, "MSG_SOURCE"));
            obj.setKeys(getColumnValue(rs, "KEYS"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setCustRefId(getColumnValue(rs, "CUST_REF_ID"));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setEventTime(EventHelper.toTimestamp(getColumnValue(rs, "EVENT_TIME")));
            obj.setCxAcctId(getColumnValue(rs, "CX_ACCT_ID"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setAddEntityId2(getColumnValue(rs, "ADD_ENTITY_ID2"));
            obj.setAddEntityId1(getColumnValue(rs, "ADD_ENTITY_ID1"));
            obj.setAddEntityId4(getColumnValue(rs, "ADD_ENTITY_ID4"));
            obj.setAddEntityId3(getColumnValue(rs, "ADD_ENTITY_ID3"));
            obj.setEventSubtype(getColumnValue(rs, "EVENT_SUBTYPE"));
            obj.setSourceOrDestAcctId(getColumnValue(rs, "SOURCE_OR_DEST_ACCT_ID"));
            obj.setAddEntityId5(getColumnValue(rs, "ADD_ENTITY_ID5"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setInitAcctStatus(getColumnValue(rs, "INIT_ACCT_STATUS"));
            obj.setCxCustId(getColumnValue(rs, "CX_CUST_ID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setFinalAcctStatus(getColumnValue(rs, "FINAL_ACCT_STATUS"));
            obj.setStatus(getColumnValue(rs, "STATUS"));
            obj.setSystemCountry(getColumnValue(rs, "SYSTEM_COUNTRY"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCOUNTSTAT]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCOUNTSTAT]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ENTITY_TYPE\",\"MSG_SOURCE\",\"KEYS\",\"CHANNEL\",\"CUST_REF_ID\",\"SOURCE\",\"EVENT_TIME\",\"CX_ACCT_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"SOURCE_OR_DEST_ACCT_ID\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"CUST_ID\",\"ACCT_NAME\",\"INIT_ACCT_STATUS\",\"CX_CUST_ID\",\"EVENT_NAME\",\"FINAL_ACCT_STATUS\",\"STATUS\",\"SYSTEM_COUNTRY\"" +
              " FROM EVENT_NFT_ACCOUNTSTAT";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ENTITY_TYPE],[MSG_SOURCE],[KEYS],[CHANNEL],[CUST_REF_ID],[SOURCE],[EVENT_TIME],[CX_ACCT_ID],[HOST_USER_ID],[HOST_ID],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[SOURCE_OR_DEST_ACCT_ID],[ADD_ENTITY_ID5],[EVENT_TYPE],[CUST_ID],[ACCT_NAME],[INIT_ACCT_STATUS],[CX_CUST_ID],[EVENT_NAME],[FINAL_ACCT_STATUS],[STATUS],[SYSTEM_COUNTRY]" +
              " FROM EVENT_NFT_ACCOUNTSTAT";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ENTITY_TYPE`,`MSG_SOURCE`,`KEYS`,`CHANNEL`,`CUST_REF_ID`,`SOURCE`,`EVENT_TIME`,`CX_ACCT_ID`,`HOST_USER_ID`,`HOST_ID`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`SOURCE_OR_DEST_ACCT_ID`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`CUST_ID`,`ACCT_NAME`,`INIT_ACCT_STATUS`,`CX_CUST_ID`,`EVENT_NAME`,`FINAL_ACCT_STATUS`,`STATUS`,`SYSTEM_COUNTRY`" +
              " FROM EVENT_NFT_ACCOUNTSTAT";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_ACCOUNTSTAT (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ENTITY_TYPE\",\"MSG_SOURCE\",\"KEYS\",\"CHANNEL\",\"CUST_REF_ID\",\"SOURCE\",\"EVENT_TIME\",\"CX_ACCT_ID\",\"HOST_USER_ID\",\"HOST_ID\",\"ADD_ENTITY_ID2\",\"ADD_ENTITY_ID1\",\"ADD_ENTITY_ID4\",\"ADD_ENTITY_ID3\",\"EVENT_SUBTYPE\",\"SOURCE_OR_DEST_ACCT_ID\",\"ADD_ENTITY_ID5\",\"EVENT_TYPE\",\"CUST_ID\",\"ACCT_NAME\",\"INIT_ACCT_STATUS\",\"CX_CUST_ID\",\"EVENT_NAME\",\"FINAL_ACCT_STATUS\",\"STATUS\",\"SYSTEM_COUNTRY\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ENTITY_TYPE],[MSG_SOURCE],[KEYS],[CHANNEL],[CUST_REF_ID],[SOURCE],[EVENT_TIME],[CX_ACCT_ID],[HOST_USER_ID],[HOST_ID],[ADD_ENTITY_ID2],[ADD_ENTITY_ID1],[ADD_ENTITY_ID4],[ADD_ENTITY_ID3],[EVENT_SUBTYPE],[SOURCE_OR_DEST_ACCT_ID],[ADD_ENTITY_ID5],[EVENT_TYPE],[CUST_ID],[ACCT_NAME],[INIT_ACCT_STATUS],[CX_CUST_ID],[EVENT_NAME],[FINAL_ACCT_STATUS],[STATUS],[SYSTEM_COUNTRY]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ENTITY_TYPE`,`MSG_SOURCE`,`KEYS`,`CHANNEL`,`CUST_REF_ID`,`SOURCE`,`EVENT_TIME`,`CX_ACCT_ID`,`HOST_USER_ID`,`HOST_ID`,`ADD_ENTITY_ID2`,`ADD_ENTITY_ID1`,`ADD_ENTITY_ID4`,`ADD_ENTITY_ID3`,`EVENT_SUBTYPE`,`SOURCE_OR_DEST_ACCT_ID`,`ADD_ENTITY_ID5`,`EVENT_TYPE`,`CUST_ID`,`ACCT_NAME`,`INIT_ACCT_STATUS`,`CX_CUST_ID`,`EVENT_NAME`,`FINAL_ACCT_STATUS`,`STATUS`,`SYSTEM_COUNTRY`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

