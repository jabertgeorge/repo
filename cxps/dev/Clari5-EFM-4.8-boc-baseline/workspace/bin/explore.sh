#!/usr/bin/env bash
# Author: Lovish

# The explore utility can be used to check any definition in current environment

unalias __expl explore 2>/dev/null
unset __expl explore 

__expl() {
	local _input=$1

	echo ${_input} | grep -q "[.]sh$"
	[ $? -eq 0 ] \
		&& echo "Haha, you can't pass script as Argument here. Not yet anyways." \
		&& return 1

	export | grep -w "${_input}=" 2>/dev/null
	[ $? -eq 0 ] \
		&& return 0

	compgen -c $_input | grep -qw "$_input"
	[ $? -ne 0 ] \
		&& echo "No Definition found" \
		&& return 1

	alias | grep -w "${_input}=" 2>/dev/null
	[ $? -eq 0 ] \
		&& return 0

	declare -f $_input 2>/dev/null
	[ $? -eq 0 ] \
		&& return 0

	local _choice
	read -p "Check man/help (m/h) [Default : Help ] : " _choice < /dev/stdin
	case ${_choice} in

		"m"|"M")
				man ${_input}
				return 0
				;;
		"h"|"H")
				;;
		*)
				;;
	esac
	
	( ${_input} --help || ${_input} -help || ${_input} -h ) 2>/dev/null
	[ $? -eq 0 ] \
		&& return 0 \
		|| echo "No Help Found"

	local _path=`which ${_input}` 2>/dev/null
	[ ! -z "${_path}" ] \
		&& echo "Available at ${_path}" \
		&& return 0
	
	# How did you reach here?
	echo 'Wow, you really found something I cannot help you with!!!'
	return 1
}

explore() {
	
	if [ $# -eq 0 ]; then
		while read line
		do
			case $line in
				"-"*)
					line=""
					;;
				*)
					;;
			esac

			[ -z "$line" ] \
				&& echo -e "Provide a (variable/alias/function/command) to be explored.\n" \
				&& continue
		
echo "-----------------------
 $line definition
-----------------------"
			__expl "$line"
			echo
		done < /dev/stdin
	else
		for arg in $*
		do
			case $arg in
				"-"*)
					echo -e "Provide a (variable/alias/function/command) to be explored."
					;;
				*)
					__expl "$arg"
					;;
			esac
		done
	fi
}
