:

#Assume that PATH is already set
. cxpsvars.sh

# Execute depsetup.sh
depsetup.sh ${CXPS_WORKSPACE}/bin/deps.cfg ${CXPS_DEPLOYMENT}

# Add the path for WORKSPACE/bin
PATH=${CXPS_WORKSPACE}/bin/scripts:${CXPS_WORKSPACE}/bin:${CXPS_BIN}:${CXPS_LIB}:$PATH

chmod +x ${CXPS_WORKSPACE}/bin/*.sh
chmod +x ${CXPS_WORKSPACE}/bin/scripts/*.sh

# Setup the path for various dependencies
. path.sh

# Setup short cut variables
export w=$CXPS_WORKSPACE
export d=$CXPS_DEPLOYMENT

if [ ! -f $CXPS_WORKSPACE/bin/.executedPermissions.flg ] ; then
    echo "Setting execute permission to all shell scripts ..."
    find $CXPS_WORKSPACE -name '*.sh' -exec chmod +x {} \;
    touch $CXPS_WORKSPACE/bin/.executedPermissions.flg
fi

shrink

# Export customization ID
export CL5_CUSTOM_ID="boc"

# Explore utility sourcing
. ${CXPS_WORKSPACE}/bin/explore.sh

# ----------------------------
# Set the release environment
# ----------------------------
unset srel 2>/dev/null
srel() {

    local rel=$1

    if ( [[ -z "$rel" ]] || [ ! -e ${w}/bin/releases/$rel ] ) ; then
        echo "
        ----------------------------------------
         Select a product release to work with
        ----------------------------------------" | cut -c9- >&2
        OLD_PS3=$PS3
        PS3="choice : "
        select rel in $( cd ${w}/bin/releases && ls )
        do
            [ -z "${rel}" ] && continue
            [ ! -e ${w}/bin/releases/${rel}/deps.cfg ] && continue
            break
        done
        PS3=${OLD_PS3}
    fi

    echo "Selected release [$rel]"

    export CL5_REL_NAME=$( echo ${rel} | tr -d '/' )
    export relV=${w}/bin/releases/${CL5_REL_NAME}/deps.cfg
    export CL5_WS_REP=${w}/rep/${CL5_REL_NAME}
    export MISSING_ASSEMBLY_FILE=${CL5_WS_REP}/.assemble.missing
    export I="${CL5_WS_REP}"

    echo "${CL5_REL_NAME}" > ${w}/bin/.rselected

    export DEP_AREA="$(dirname ${CXPS_DEPLOYMENT})/${CL5_REL_NAME}"
    if [ -e "${DEP_AREA}" ]; then
        rm -fr ${CXPS_DEPLOYMENT}
        ln -fs ${DEP_AREA} ${CXPS_DEPLOYMENT}
        echo "${CL5_REL_NAME}" > ${DEP_AREA}/../.deployed
    fi
}

# Set default to SNAPSHOT
if [ -z "${CL5_REL_NAME}" ]; then
    [ -f ${w}/bin/.rselected ] && CL5_REL_NAME=$( cat ${w}/bin/.rselected )
    [ -z "${CL5_REL_NAME}" ] && CL5_REL_NAME=SNAPSHOT
fi
srel ${CL5_REL_NAME}

export DEP_BASE=${d}

unset rel 2>/dev/null
rel() {

    # Return the release name
    [[ "$1" = "?" ]] && echo ${CL5_REL_NAME} && return 0

    # Execute assemble
    ${w}/bin/scripts/assemble.sh ${relV}
    touch ${MISSING_ASSEMBLY_FILE}

    export CX_TOOL_BIN=${w}/bin/tools/bin
    [ -e ${CX_TOOL_BIN} ] \
        && export PATH=${CX_TOOL_BIN}:${PATH}

    [ -e ${CX_TOOL_BIN}/.envpath ] \
        && chmod +x ${CX_TOOL_BIN}/.envpath \
        && . ${CX_TOOL_BIN}/.envpath

    return 0
}

alias ins='cd $I'

# Reset the environment
resetenv() {
    . ${w}/bin/scripts/configure.sh --reset
}

unalias rt 2>/dev/null
unset rt
rt() {
    echo "Starting tomcat as stand-alone app"
    export JAVA_OPTS="-Xms512m -Xmx2048m -XX:HeapDumpPath=/tmp/heap.bin -XX:+HeapDumpOnOutOfMemoryError -DCXPS_DEPLOYMENT=${CXPS_DEPLOYMENT}"
    ( cd ${CXPS_DEPLOYMENT}/tomcat/logs && rm -fr * )
    ( cd ${CXPS_DEPLOYMENT}/tomcat/bin && java -cp "*:../lib/*" org.apache.catalina.startup.Bootstrap )
}

alias dep="cd ${CXPS_DEPLOYMENT}"

alias ct="${CXPS_WORKSPACE}/bin/scripts/cxpsctl.sh status"
alias st="tomcat; ${CXPS_WORKSPACE}/bin/scripts/cxpsctl.sh start && tail -f catalina.out"
alias St="${CXPS_WORKSPACE}/bin/scripts/cxpsctl.sh stop"
alias kt="${CXPS_WORKSPACE}/bin/scripts/cxpsctl.sh kill"
alias Kt="${CXPS_WORKSPACE}/bin/scripts/cxpsctl.sh killA"

# Load utilities and environment
. ${CXPS_WORKSPACE}/bin/utilities.sh
. ${CXPS_WORKSPACE}/bin/scripts/configure.sh

unalias ws 2>/dev/null
unset ws 2>/dev/null
ws() {
    PATH=${CXPS_WORKSPACE}/bin/scripts:${CXPS_BIN}:${CXPS_LIB}:$PATH
    . path.sh
    shrink

    export CX_TOOL_BIN=${CXPS_WORKSPACE}/bin/tools/bin
    [ -e ${CX_TOOL_BIN} ] \
        && export PATH=${CX_TOOL_BIN}:${PATH}

    [ -e ${CX_TOOL_BIN}/.envpath ] \
        && chmod +x ${CX_TOOL_BIN}/.envpath \
        && . ${CX_TOOL_BIN}/.envpath

    cd ${CXPS_WORKSPACE}
}

alias ib='fbuild.sh -i'
# Execute ws to set everything up
ws

# export sq
unset sq 2>/dev/null
unalias sq 2>/dev/null
sq() {

    local _psd=${DB_PMSEED}
    [ -z "${_psd}" ] && _psd="c_xps123"

    echo "Connecting to [${DB_TYPE}] with [${DB_USER}@${DB_SID}/${_psd}]"
    case ${DB_TYPE} in
        "oracle")
            sqlplus ${DB_USER}/${_psd}@${DB_SID}
            ;;
        "sqlserver")
            sqlcmd -S ${DB_SID} -U ${DB_USER} -P ${_psd} -d ${DB_USER}
            ;;
        "mysql")
            mysql -u${DB_USER} -p${_psd} $DB_USER
            ;;
    esac
}

# export es
unset es 2>/dev/null
unalias es 2>/dev/null
es() {
    
    local dir=${d}/platform/instances/elasticsearch/5.2.0/elasticsearch-5.2.0
    [ ! -e ${dir} ] && echo "Elastic search is not deployed" && return 0

    [ -z "$1" ] && cd ${dir} && return 0
    case $1 in
        "start"|"st") 
            export ES_JAVA_OPTS="-Xms128m -Xmx256m"
            printf "Starting up elastic search ... "
            ( unset JAVA_OPTS >/dev/null 2>&1 && cd ${dir}/bin && ./start )
            echo "done" ;;
        "stop"|"St"|"kt") 
            printf "Shutting down elastic search ... "
            ( cd ${dir}/bin && ./stop )
            echo "done" ;;
        "status"|"ct"|"?")
            ( cd ${dir}/bin && ./status ) \
                && echo "Elastic search is running" \
                || echo "Elastic search is down"
            ;;
        *)  echo "Operation not supported"
            return 1
            ;;
    esac
    return 0
}

# export kibana
unset kibana 2>/dev/null
unalias kibana 2>/dev/null
kibana() {
    
    local dir=${d}/platform/instances/kibana/5.2.0/kibana-5.2.0
    [ ! -e ${dir} ] && echo "Kibana is not deployed" && return 0

    [ -z "$1" ] && cd ${dir} && return 0
    case $1 in
        "start"|"st") 
            printf "Starting up kibana ... "
            ( cd ${dir}/bin && ./start )
            echo "done" ;;
        "stop"|"St"|"kt") 
            printf "Shutting down kibana ... "
            ( cd ${dir}/bin && ./stop )
            echo "done" ;;
        "status"|"?"|"ct")
            ( cd ${dir}/bin && ./status ) \
                && echo "Kibana is running" \
                || echo "Kibana is down"
            ;;
        *)  echo "Operation not supported"
            return 1
            ;;
    esac
    return 0
}

unalias src 2>/dev/null
alias src='bld.sh --src && cd ${w}/work'
