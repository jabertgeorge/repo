: 
# Database configurations

[ -z "${DB_TYPE}" ] && DB_TYPE=sqlserver
[[ "${DB_TYPE}" = "oracleXE" ]] && DB_TYPE=oracle

# Database type
export DB_TYPE
export SECURITY_TYPE=${DB_TYPE}

# Database connection
case ${DB_TYPE} in
	"mysql")
        export DB_IP="localhost"
        export DB_PORT="3306"
        export DB_SID="cxpsadm_${USER}_48dev"
        export DB_DIALECT="org.hibernate.dialect.MySQLDialect"
		;;
    "oracle")
		# lan
        export DB_IP="192.168.5.37"
        export DB_PORT="1521"
        export DB_SID="cxps12c"
	
	export JASPER_DN="sanat.customerxps.com:11080/jasperserver-pro"
		# azure
        #export DB_IP="52.172.8.206"
        #export DB_SID="oracle12c"

		# local
        # export DB_IP="127.0.0.1"
        # export DB_SID="xe"

        export DB_DIALECT="org.hibernate.dialect.Oracle10gDialect"
        ;;
    "sqlserver")
		# lan
        export DB_IP="192.168.5.76"
        export DB_PORT="1433"
        export DB_SID="CXPSWINSQL"

		# azure
        #export DB_IP="52.172.40.121" #azure
        #export DB_SID="dbsid" #azure

        # local
        #export DB_IP="127.0.0.1"
        #export DB_SID="$(hostname -s)"

        export DB_DIALECT="org.hibernate.dialect.SQLServer2012Dialect"
        ;;
esac

#[ -e ${w}/bin/overrides/db.sh ] \
   # && echo "Overriding DB values using [${w}/bin/overrides/db.sh]" \
    #&& chmod +x ${w}/bin/overrides/db.sh \
    #&& . ${w}/bin/overrides/db.sh \

# Database users
#[ -z "${DB_USER_OVERRIDDEN}" ] && export DB_USER="jiraadm_${USER}_48dev"
[ -z "${DB_USER_OVERRIDDEN}" ] && export DB_USER="cxpsadm_${USER}_48dev"
export SEC_DB_USER=${DB_USER}
export SECURITY_USER=${DB_USER}

# Security schema for cas
export SECURITY_IP=${DB_IP}
export SECURITY_PORT=${DB_PORT}
export SECURITY_SID=${DB_SID}

# Password management
export RICE_PMSEED="c_xps123"
export RICE_PMTYPE="SIMPLE"
export SECURITY_PMSEED="c_xps123"
export SECURITY_PMTYPE="SIMPLE"
export DB_PMTYPE=$RICE_PMTYPE
export DB_PMSEED=$RICE_PMSEED
export CC_PMTYPE=$RICE_PMTYPE
export CC_PMSEED=$RICE_PMSEED
export WL_PMSEED=$RICE_PMSEED
export WL_PMTYPE=$RICE_PMTYPE
export WLDATA_PMSEED=$RICE_PMSEED
export WLDATA_PMTYPE=$RICE_PMTYPE

# Other remaining env params
export DN="http://$(hostname -f):5000"
export LOCAL_DN="http://$(hostname -f):5000"
export CX_DATA_DIR="${DEP_BASE}/data"
export BASE_Q_LOCATION="${CX_DATA_DIR}/CMQ"
export KENDEPLOY_PATH="${DEP_BASE}/KEN"
export CXPS_JIRA_HOME="${HOME}/atlassian/jira"
export CMQ_ARCHIVE_DIR="${CX_DATA_DIR}/CMQ_ARCHIVE"
export LOG_Q_LOCATION="${CX_DATA_DIR}/APPLOG"
export CAS_IDLE_TIMEOUT="300000"
export AJP_PORT="9090"

# Jira credentials
export JIRA_USER_ID="cxpsaml"
export JIRA_PASSWORD="Y3hwc2FtbA=="

export JIRA_DN="http://yashaswi.customerxps.com:8093"
export JASPER_DN="http://sanat.customerxps.com:11080/jasperserver-pro"
export JIRA_FORMATTER="clari5.custom.dev.CustomCmsFormatter"
export MONETARY_VALUE="customfield_12101"
export STR_HANDLER="clari5.str.CustomSTRHandler"
		# azure
# Values for JIRA CLIENT auth
export INSTANCEID="clari5"
export APPID="clari5"
export APPSECRET="clari5"
export APP_HOST="`hostname -f`"

# JAVA_OPTS set to override JAVA_OPTS from meta conf
export JAVA_OPTS="-Xmx2g"

[ -e ${w}/overrides.sh ] \
    && chmod +x ${w}/overrides.sh \
    && . ${w}/overrides.sh

