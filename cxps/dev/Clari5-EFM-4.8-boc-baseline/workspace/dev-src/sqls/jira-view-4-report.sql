CREATE  VIEW JIRA_VIEW ("PROJECT_NAME", "ALERT_ID", "ACCOUNTID", "SCENARIOID", "SCORE", "SUMMARY", "DISPLAY_NAME", "USER_NAME", "PRIORITY", "ISSUESTATUS", "ISSUETYPE", "ASSIGNEE", "REPORTER", "RESOLUTION", "CREATED", "UPDATED", "RESOLUTIONDATE", "RESOLUTIONACTION", "CATEGORY", "AREA_OF_FRAUD", "CASE_DETAILS", "ACTION_TAKEN")
        AS
WITH a1 AS
  (SELECT
    (SELECT UPPER(PNAME) FROM jiraadm.dbo.project WHERE id = j.project
    ) project_name,
    (SELECT ISNULL(ORIGINALKEY, '')+'-'+CONVERT(VARCHAR(10),(isnull(j.issuenum, '')) )FROM jiraadm.dbo.project WHERE id = j.project

    ) alert_id,
    (SELECT SUBSTRING(stringvalue, 0, CHARINDEX('|', stringvalue))
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='10214'
    ) accountId,
    (SELECT REPLACE(stringvalue,'R_F_','')
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='10215'
    ) solid,
    (SELECT stringvalue
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='10211'
    ) scenarioId,
    (SELECT stringvalue
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='10212'
    ) score,
    j."SUMMARY",
    UPPER(c.DISPLAY_NAME) DISPLAY_NAME,
    UPPER(c.USER_NAME) USER_NAME,
    --j."PRIORITY",
    (SELECT CASE stringvalue WHEN 'L1' THEN 'High' WHEN 'L2' THEN 'High' WHEN 'L3' THEN 'Medium' WHEN 'L4' THEN 'Low' END
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='10610'
    ) PRIORITY,
    j."ISSUESTATUS",
    j.ISSUETYPE,
    UPPER(j.ASSIGNEE) ASSIGNEE,
    UPPER(j.REPORTER) REPORTER,
    j."RESOLUTION",
    j.CREATED,
    j.UPDATED,
    j."RESOLUTIONDATE",
    (SELECT  customvalue
    FROM jiraadm.dbo.customfieldoption
    WHERE ID =
      (SELECT stringvalue
      FROM jiraadm.dbo.customfieldvalue
      WHERE issue     =J.id
      AND customfield ='11206')
    ) RESOLUTIONACTION,
    (SELECT customvalue
    FROM jiraadm.dbo.customfieldoption
    WHERE ID =
      (SELECT stringvalue
      FROM jiraadm.dbo.customfieldvalue
      WHERE issue     =J.id
      AND customfield ='11201')
    ) CATEGORY,
    (SELECT customvalue
    FROM jiraadm.dbo.customfieldoption
    WHERE ID =
      (SELECT stringvalue
      FROM jiraadm.dbo.customfieldvalue
      WHERE issue     =J.id
      AND customfield ='11202')
    ) AREA_OF_FRAUD,
    (SELECT stringvalue
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='11203'
    ) CASE_DETAILS,
    (SELECT stringvalue
    FROM jiraadm.dbo.customfieldvalue
    WHERE issue     =J.id
    AND customfield ='10404'
    ) ACTION_TAKEN
  FROM "jiraadm.dbo.JIRAISSUE" j,
    cwd_user c
  WHERE (j.ISSUETYPE    = '1'
  OR j.ISSUETYPE        = '5')
  AND upper(j.ASSIGNEE) = upper(c.USER_NAME)
  )
SELECT a1.project_name,
  a1.alert_id,
  a1.accountId,
  a1.scenarioId,
  a1.score,
  a1.summary,
  a1.DISPLAY_NAME,
  a1.USER_NAME,
  a1.PRIORITY,
  a1.ISSUESTATUS,
  a1.ISSUETYPE,
  a1.ASSIGNEE,
  a1.REPORTER,
  a1.RESOLUTION,
a1.CREATED,
    a1.UPDATED,
  a1.RESOLUTIONDATE,
  COALESCE(a1.RESOLUTIONACTION,'untouched'),
  a1.CATEGORY,
  a1.AREA_OF_FRAUD,
  a1.CASE_DETAILS,
  a1.ACTION_TAKEN
FROM jiraadm.dbo.a1;

