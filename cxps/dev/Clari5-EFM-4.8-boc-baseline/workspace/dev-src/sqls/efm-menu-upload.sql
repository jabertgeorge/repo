INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_SITE_CAPABILITY
(help, [external], name_space, menu_name, root, site_id, icon, allowed_access_type, display_name, url)
VALUES(NULL, 'Y', 'EFM/CONFIG', 'INSERT', 'EFM', 'CC', NULL, '|NO_ACCESS|ALL_ACCESS|', 'UPLOAD', '/efm/upload/InsertPage.html');

select * from dbo.CL5_SITE_CAPABILITY;
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_SITE_CAPABILITY
(help, [external], name_space, menu_name, root, site_id, icon, allowed_access_type, display_name, url)
VALUES(NULL, 'Y', 'EFM/CONFIG', 'UPDATE', 'EFM', 'CC', NULL, '|NO_ACCESS|ALL_ACCESS|', 'UPLOAD', '/efm/upload/UpdatePage.html');

select * from dbo.CL5_SITE_CAPABILITY;
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_SITE_CAPABILITY
(help, [external], name_space, menu_name, root, site_id, icon, allowed_access_type, display_name, url)
VALUES(NULL, 'Y', 'EFM/CONFIG', 'DELETE', 'EFM', 'CC', NULL, '|NO_ACCESS|ALL_ACCESS|', 'UPLOAD', '/efm/upload/DeletePage.html');


INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/INSERT', 'NO_ACCESS', 'NO_ACCESS_ROLE', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/INSERT', 'ALL_ACCESS', 'SUPERADMIN', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/INSERT', 'ALL_ACCESS', 'SUPERUSER', NULL);

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/UPDATE', 'NO_ACCESS', 'NO_ACCESS_ROLE', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/UPDATE', 'ALL_ACCESS', 'SUPERADMIN', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/UPDATE', 'ALL_ACCESS', 'SUPERUSER', NULL);

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/DELETE', 'NO_ACCESS', 'NO_ACCESS_ROLE', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/DELETE', 'ALL_ACCESS', 'SUPERADMIN', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/CONFIG/DELETE', 'ALL_ACCESS', 'SUPERUSER', NULL);
