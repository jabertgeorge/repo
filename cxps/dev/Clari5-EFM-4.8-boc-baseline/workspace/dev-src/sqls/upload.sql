
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_SITE_CAPABILITY
(help, [external], name_space, menu_name, root, site_id, icon, allowed_access_type, display_name, url)
VALUES(NULL, 'Y', 'EFM/UPLOAD', 'INSERT', 'EFM', 'CC', NULL, '|NO_ACCESS|ALL_ACCESS|', 'UPLOAD', '/efm/upload/InsertPage.html');

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_SITE_CAPABILITY
(help, [external], name_space, menu_name, root, site_id, icon, allowed_access_type, display_name, url)
VALUES(NULL, 'Y', 'EFM/UPLOAD', 'UPDATE', 'EFM', 'CC', NULL, '|NO_ACCESS|ALL_ACCESS|', 'UPLOAD', '/efm/upload/UpdatePage.html');

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_SITE_CAPABILITY
(help, [external], name_space, menu_name, root, site_id, icon, allowed_access_type, display_name, url)
VALUES(NULL, 'Y', 'EFM/UPLOAD', 'DELETE', 'EFM', 'CC', NULL, '|NO_ACCESS|ALL_ACCESS|', 'UPLOAD', '/efm/upload/DeletePage.html');

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/INSERT', 'NO_ACCESS', 'NO_ACCESS_ROLE', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/INSERT', 'ALL_ACCESS', 'SUPERADMIN', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/INSERT', 'ALL_ACCESS', 'SUPERUSER', NULL);

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/UPDATE', 'NO_ACCESS', 'NO_ACCESS_ROLE', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/UPDATE', 'ALL_ACCESS', 'SUPERADMIN', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/UPDATE', 'ALL_ACCESS', 'SUPERUSER', NULL);

INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/DELETE', 'NO_ACCESS', 'NO_ACCESS_ROLE', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/DELETE', 'ALL_ACCESS', 'SUPERADMIN', NULL);
INSERT INTO cxpsadm_sanat_48dev.dbo.CL5_ROLE_CAPABILITY
(capability, access_type, role_id, delta_access_type)
VALUES('EFM/UPLOAD/DELETE', 'ALL_ACCESS', 'SUPERUSER', NULL);
