package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import org.json.JSONException;
import org.json.JSONObject;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class NFT_IBPayeeRegEvent extends Event {
    private static final long serialVersionUID = -5059568925663084244L;
    private String source;
    private String keys;
    private Date sysTime;
    private String userId;
    private String custId;
    private String deviceId;
    private String addrNetwork;
    private String succFailFlg;
    private String errorCode;
    private String errorDesc;
    private String timeSlot;
    private String payeeType;
    private String payeeId;
    private String payeeNickName;
    private String payeeAccountType;
    private String payeeAccountId;
    private String payeeAccountName;
    private String payeeBankName;
    private String payeeBankId;
    private String payeeBranchName;
    private String payeeBranchId;
    private String payeeStatus;
    private Date txnTs;
    private String postMfaFlag;
    private String mfaType;
    private String onOffFlg;
    private String payeeRefNo;
    private String payeeifscCode;
    private String payeeMmid;
    private String country;
    private String entity1;
    private String entity2;
    private String entity3;
    private String entity4;
    private String entity5;
    private String entity6;
    private String entity7;
    private String entity8;
    private String entity9;
    private String entity10;

    public String getCountry() {
        return this.country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getEntity1() {
        return this.entity1;
    }
    public void setEntity1(String entity1) {
        this.entity1 = entity1;
    }
    public String getEntity2() {
        return this.entity2;
    }
    public void setEntity2(String entity2) {
        this.entity2 = entity2;
    }
    public String getEntity3() {
        return this.entity3;
    }
    public void setEntity3(String entity3) {
        this.entity3 = entity3;
    }
    public String getEntity4() {
        return this.entity4;
    }
    public void setEntity4(String entity4) {
        this.entity4 = entity4;
    }
    public String getEntity5() {
        return this.entity5;
    }
    public void setEntity5(String entity5) {
        this.entity5 = entity5;
    }
    public String getEntity6() {
        return this.entity6;
    }
    public void setEntity6(String entity6) {
        this.entity6 = entity6;
    }
    public String getEntity7() {
        return this.entity7;
    }
    public void setEntity7(String entity7) {
        this.entity7 = entity7;
    }
    public String getEntity8() {
        return this.entity8;
    }
    public void setEntity8(String entity8) {
        this.entity8 = entity8;
    }
    public String getEntity9() {
        return this.entity9;
    }
    public void setEntity9(String entity9) {
        this.entity9 = entity9;
    }
    public String getEntity10() {
        return this.entity10;
    }
    public void setEntity10(String entity10) {
        this.entity10 = entity10;
    }
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    public String getKeys() {
        return keys;
    }
    public void setKeys(String keys) {
        this.keys = keys;
    }
    public Date getSysTime() {
        return sysTime;
    }
    public void setSysTime(Date sysTime) {
        this.sysTime = sysTime;
    }
    public void setSysTime(String sysTime) {
        if (sysTime == null || "".equals(sysTime.trim())) {
            throw new IllegalArgumentException("Value is required: sysTime");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                this.sysTime = dateFormat1.parse(sysTime);
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide sysTime in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:sysTime>value:" +sysTime);
            }
        }
    }
    public void setEventTSStr(String eventts) {
        if (eventts == null || "".equals(eventts.trim())) {
            throw new IllegalArgumentException("Value is required: eventts");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                setEventTS(dateFormat1.parse(eventts));
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide eventTs in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:eventts>value:" +eventts);
            }
        }
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getCustId() {
        return custId;
    }
    public void setCustId(String custId) {
        if (custId == null || "".equals(custId.trim())){
            throw new IllegalArgumentException("Value is required: CustId");
        }
        else
            this.custId = custId;
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public String getAddrNetwork() {
        return addrNetwork;
    }
    public void setAddrNetwork(String addrNetwork) {
        this.addrNetwork = addrNetwork;
    }
    public String getSuccFailFlg() {
        return succFailFlg;
    }
    public void setSuccFailFlg(String succFailFlg) {
        this.succFailFlg = succFailFlg;
    }
    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    public String getErrorDesc() {
        return errorDesc;
    }
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
    public String getTimeSlot() {
        return timeSlot;
    }
    public void setTimeSlot(String timeSlot) {
        if(timeSlot.trim().startsWith("S")) {
            this.timeSlot = timeSlot;
        } else {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                Date dateForTimeSlot = dateFormat.parse(timeSlot);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateForTimeSlot);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                if((6 <= hour) && (hour < 9)) {
                    this.timeSlot = "S1";
                } else if ((9 <= hour) && (hour < 15)) {
                    this.timeSlot = "S2";
                } else if ((15 <= hour) && (hour < 21)) {
                    this.timeSlot = "S3";
                } else if ((21 <= hour) && (hour < 23)) {
                    this.timeSlot = "S4";
                } else if ((23 == hour) || ((hour >= 0) && (hour < 9))) {
                    this.timeSlot = "S5";
                } else {
                    throw new IllegalArgumentException("Invalid hour value " +hour +" for timeslot");
                }
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide timeSlot or time in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:timeSlot>value:" +timeSlot);
            }
        }
    }
    public String getPayeeType() {
        return payeeType;
    }
    public void setPayeeType(String payeeType) {
        this.payeeType = payeeType;
    }
    public String getPayeeId() {
        return payeeId;
    }
    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }
    public String getPayeeNickName() {
        return payeeNickName;
    }
    public void setPayeeNickName(String payeeNickName) {
        this.payeeNickName = payeeNickName;
    }
    public String getPayeeAccountType() {
        return payeeAccountType;
    }
    public void setPayeeAccountType(String payeeAaccountType) {
        this.payeeAccountType = payeeAaccountType;
    }
    public String getPayeeAccountId() {
        return payeeAccountId;
    }
    public void setPayeeAccountId(String payeeAccountId) {
        this.payeeAccountId = payeeAccountId;
    }
    public String getPayeeAccountName() {
        return payeeAccountName;
    }
    public void setPayeeAccountName(String payeeAccountName) {
        this.payeeAccountName = payeeAccountName;
    }
    public String getPayeeBankName() {
        return payeeBankName;
    }
    public void setPayeeBankName(String payeeBankName) {
        this.payeeBankName = payeeBankName;
    }
    public String getPayeeBankId() {
        return payeeBankId;
    }
    public void setPayeeBankId(String payeeBankId) {
        this.payeeBankId = payeeBankId;
    }
    public String getPayeeBranchName() {
        return payeeBranchName;
    }
    public void setPayeeBranchName(String payeeBranchName) {
        this.payeeBranchName = payeeBranchName;
    }
    public String getPayeeBranchId() {
        return payeeBranchId;
    }
    public void setPayeeBranchId(String payeeBranchId) {
        this.payeeBranchId = payeeBranchId;
    }
    public String getPayeeStatus() {
        return payeeStatus;
    }
    public void setPayeeStatus(String payeeStatus) {
        this.payeeStatus = payeeStatus;
    }
    public Date getTxnTs() {
        return txnTs;
    }
    public void setTxnTs(String txnTs) {
        if (txnTs == null || "".equals(txnTs.trim())) {
            throw new IllegalArgumentException("Value is required: txnts");
        } else {
            try {
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                this.txnTs = dateFormat1.parse(txnTs);
            } catch (ParseException ex) {
                throw new IllegalArgumentException("Provide txnTs in the format:dd-MM-yyyy HH:mm:ss.SSS. Invalid Value>field:TxnTs>value:" +txnTs);
            }
        }
    }
    public void setTxnTs(Date txnTs) {
        this.txnTs = txnTs;
    }
    public String getPostMfaFlag() {
        return postMfaFlag;
    }
    public void setPostMfaFlag(String postMfaFlag) {
        this.postMfaFlag = postMfaFlag;
    }
    public String getMfaType() {
        return mfaType;
    }
    public void setMfaType(String mfaType) {
        this.mfaType = mfaType;
    }
    public String getOnOffFlg() {
        return onOffFlg;
    }
    public void setOnOffFlg(String onOffFlg) {
        this.onOffFlg = onOffFlg;
    }
    public String getPayeeRefNo() {
        return payeeRefNo;
    }
    public void setPayeeRefNo(String payeeRefNo) {
        this.payeeRefNo = payeeRefNo;
    }
    public String getPayeeifscCode() {
        return payeeifscCode;
    }
    public void setPayeeifscCode(String payeeifscCode) {
        this.payeeifscCode = payeeifscCode;
    }
    public String getPayeeMmid() {
        return payeeMmid;
    }
    public void setPayeeMmid(String payeeMmid) {
        this.payeeMmid = payeeMmid;
    }
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setEventType("NFT");
        setEventSubType("IBPayeeReg");
        setSource((String)msgMap.get("source"));
        setKeys((String)msgMap.get("keys"));
        setSysTime((String) msgMap.get("sys_time"));
        setUserId((String) msgMap.get("user-id"));
        setCustId((String) msgMap.get("cust-id"));
        setDeviceId((String) msgMap.get("device-id"));
        setAddrNetwork((String)msgMap.get("addr-network"));
        setSuccFailFlg((String) msgMap.get("succ-fail-flg"));
        setErrorCode((String)msgMap.get("error-code"));
        setErrorDesc((String) msgMap.get("error-desc"));
        setTimeSlot((String) msgMap.get("time-slot"));
        setPayeeType((String) msgMap.get("payee-type"));
        setPayeeId((String) msgMap.get("payee-id"));
        setPayeeNickName((String) msgMap.get("payee-nickname"));
        setPayeeAccountType((String) msgMap.get("payee-account-type"));
        setPayeeAccountId((String) msgMap.get("payee-account-id"));
        setPayeeAccountName((String) msgMap.get("payee-account-name"));
        setPayeeBankName((String) msgMap.get("payee-bank-name"));
        setPayeeBankId((String) msgMap.get("payee-bank-id"));
        setPayeeBranchName((String) msgMap.get("payee-branch-name"));
        setPayeeBranchId((String) msgMap.get("payee-branch-id"));
        setPayeeStatus((String) msgMap.get("payee-status"));
        setTxnTs((String)msgMap.get("txn-ts"));
        setPostMfaFlag((String)msgMap.get("post-mfa-flag"));
        setMfaType((String)msgMap.get("mfa-type"));
        setOnOffFlg((String)msgMap.get("on-off-flag"));
        setPayeeRefNo((String) msgMap.get("payee-ref-no"));
        setPayeeifscCode((String) msgMap.get("payee-ifsc-code"));
        setPayeeMmid((String)msgMap.get("payee-mmid"));
        setCountry((String)msgMap.get("country"));
        setEntity1((String)msgMap.get("entity1"));
        setEntity2((String)msgMap.get("entity2"));
        setEntity3((String)msgMap.get("entity3"));
        setEntity4((String)msgMap.get("entity4"));
        setEntity5((String)msgMap.get("entity5"));
        setEntity6((String)msgMap.get("entity6"));
        setEntity7((String)msgMap.get("entity7"));
        setEntity8((String)msgMap.get("entity8"));
        setEntity9((String)msgMap.get("entity9"));
        setEntity10((String)msgMap.get("entity10"));
    }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_IBPayeeRegEvent", new Object[0]);
        HashSet wsInfoSet = new HashSet();
        String hostAcctKey = this.getPayeeAccountId();
        if(hostAcctKey == null || hostAcctKey.trim().length() == 0) {
            cxLog.ferrexit("Nil-HostAcctKey", new Object[0]);
        }

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        String cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, this.getHostId(), hostAcctKey.trim());
        if(!CxKeyHelper.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
            cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey, new Object[0]);
        }

        WorkspaceInfo wi = new WorkspaceInfo("Account", cxAcctKey);
        wi.addParam("acctId", cxAcctKey);

        wsInfoSet.add(wi);
        return wsInfoSet;

    }

    public JSONObject getFRJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("event_id",getEventId());
        jsonObject.put("cust_id",getCustId());
        jsonObject.put("event-name",getEventName());
        jsonObject.put("sys_time",getEventTS().getTime());
        jsonObject.put("EventType",getEventType());
        jsonObject.put("EventSubType",getEventSubType());

        return jsonObject;
    }
}

