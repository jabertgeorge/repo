package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import cxps.noesis.core.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by anshul on 12/10/15.
 */
public class TR_AccountFundFlowEvent extends Event {

    private static final long serialVersionUID = -837072257960922351L;
    private String tradingAccountId;
    private String accountType;
    private String clientId;
    private String accountOwnerShipFlag;
    private Date acctOpenDate;
    private Double availableBalance;
    private Double marginBalanceAmount;
    private Double nonMarginBalanceAmount;

    private String transactionType; // C or D
    private Date tranDate;
    private String transactionSubType;
    private String tranId;
    private Double transactionAmount;
    private String currencyCode;
    private String tranParticular;
    private String bankCode;
    private String bankAccountId;
    private String clientOccupationCode;
    private String remisierCode;
    private Date sysDateTime;
    private String tranLocationPin;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");


    @Override
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setTradingAccountId((String) msgMap.get("TA_account-id"));
        setAccountType((String) msgMap.get("acct-type"));
        setClientId((String) msgMap.get("client-id"));
        setAccountOwnerShipFlag((String) msgMap.get("acct-ownership"));
        setAcctOpenDate((String) msgMap.get("acctopendate"));
        setAvailableBalance((String) msgMap.get("avl-bal"));
        setMarginBalanceAmount((String) msgMap.get("margin-bal-amt"));
        setNonMarginBalanceAmount((String) msgMap.get("non-margin-bal-amt"));
        setTransactionType((String) msgMap.get("tran-type"));
        setTransactionSubType((String) msgMap.get("tran-sub-type"));
        setTranDate((String) msgMap.get("tran-date"));
        setTranId((String) msgMap.get("tran-id"));
        setTranLocationPin((String) msgMap.get("tran-location-pin"));
        setTransactionAmount((String) msgMap.get("tran-amt"));
        setCurrencyCode((String) msgMap.get("tran-crncy-code"));
        setTranParticular((String) msgMap.get("tran-particular"));
        setBankCode((String) msgMap.get("bank-code"));
        setBankAccountId((String) msgMap.get("Bank_Account-id"));
        setClientOccupationCode((String) msgMap.get("client-occp-code"));
        setRemisierCode((String) msgMap.get("remesier-code"));
        setSysDateTime((String) msgMap.get("sys-datetime"));

    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTradingAccountId() {
        return tradingAccountId;
    }

    public void setTradingAccountId(String tradingAccountId) {
        this.tradingAccountId = tradingAccountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccountOwnerShipFlag() {
        return accountOwnerShipFlag;
    }

    public void setAccountOwnerShipFlag(String accountOwnerShipFlag) {
        this.accountOwnerShipFlag = accountOwnerShipFlag;
    }

    public Date getAcctOpenDate() {
        return acctOpenDate;
    }

    public void setAcctOpenDate(String acctOpenDate) {
        try {
            this.acctOpenDate = dateFormat1.parse(acctOpenDate);
        } catch (ParseException ex) {
            try {
                this.acctOpenDate = dateFormat2.parse(acctOpenDate);
            } catch (ParseException ex1) {
                try {
                    this.acctOpenDate = dateFormat.parse(acctOpenDate);
                } catch (ParseException e) {
                    throw new IllegalArgumentException("Invalid Value>field:acctOpenDate>value:" + acctOpenDate);
                }
            }
        }
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        try {
            double availBal = Double.parseDouble(availableBalance);
            this.availableBalance = availBal;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:availableBalance>value:" + availableBalance);
        }
    }

    public Double getMarginBalanceAmount() {
        return marginBalanceAmount;
    }

    public void setMarginBalanceAmount(String marginBalanceAmount) {
        try {
            double marginBalanceAmt = Double.parseDouble(marginBalanceAmount);
            this.marginBalanceAmount = marginBalanceAmt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:marginBalanceAmount>value:" + marginBalanceAmount);
        }
    }

    public Double getNonMarginBalanceAmount() {
        return nonMarginBalanceAmount;
    }

    public void setNonMarginBalanceAmount(String nonMarginBalanceAmount) {
        try {
            double nonMarginBalanceAmt = Double.parseDouble(nonMarginBalanceAmount);
            this.nonMarginBalanceAmount = nonMarginBalanceAmt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:nonMarginBalanceAmount>value:" + nonMarginBalanceAmount);
        }
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionSubType() {
        return transactionSubType;
    }

    public void setTransactionSubType(String transactionSubType) {
        this.transactionSubType = transactionSubType;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {

        try {
            this.tranDate = dateFormat1.parse(tranDate);
        } catch (ParseException ex) {
            try {
                this.tranDate = dateFormat2.parse(tranDate);
            } catch (ParseException ex1) {
                try {
                    this.tranDate = dateFormat.parse(tranDate);
                } catch (ParseException e) {
                    throw new IllegalArgumentException("Invalid Value>field:tranDate>value:" + tranDate);
                }
            }
        }
    }
    

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        try {
            double txnAm = Double.parseDouble(transactionAmount);
            this.transactionAmount = txnAm;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:transactionAmount>value:" + transactionAmount);
        }
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTranParticular() {
        return tranParticular;
    }

    public void setTranParticular(String tranParticular) {
        this.tranParticular = tranParticular;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(String bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getClientOccupationCode() {
        return clientOccupationCode;
    }

    public void setClientOccupationCode(String clientOccupationCode) {
        this.clientOccupationCode = clientOccupationCode;
    }

    public String getRemisierCode() {
        return remisierCode;
    }

    public void setRemisierCode(String remisierCode) {
        this.remisierCode = remisierCode;
    }

    public Date getSysDateTime() {
        return sysDateTime;
    }

    public void setSysDateTime(String sysDateTime) {
        if (StringUtils.isNullOrEmpty(sysDateTime)) {
            throw new IllegalArgumentException("Value is required: sysDateTime");
        } else {
            try {
                this.sysDateTime = dateFormat1.parse(sysDateTime);
            } catch (ParseException ex) {
                try {
                    this.sysDateTime = dateFormat2.parse(sysDateTime);
                } catch (ParseException ex1) {
                    try {
                        this.sysDateTime = dateFormat.parse(sysDateTime);
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("Invalid Value>field:sysDateTime>value:" + sysDateTime);
                    }
                }
            }
        }
    }

	public String getTranLocationPin() {
		return tranLocationPin;
	}

	public void setTranLocationPin(String tranLocationPin) {
		this.tranLocationPin = tranLocationPin;
	}
    @SuppressWarnings("unchecked")
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = (ICXLog) CXLog.fenter("derive.FT_AccountTxnEvent", new Object[0]);
        HashSet wsInfoSet = new HashSet();
        String hostAcctKey = this.getTradingAccountId();
        if(hostAcctKey == null || hostAcctKey.trim().length() == 0) {
            cxLog.ferrexit("Nil-HostAcctKey", new Object[0]);
        }

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        String cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, this.getHostId(), hostAcctKey.trim());
        if(!CxKeyHelper.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
            cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey, new Object[0]);
        }

        WorkspaceInfo wi = new WorkspaceInfo("Account", cxAcctKey);
        wi.addParam("acctId", cxAcctKey);

        wsInfoSet.add(wi);
       return wsInfoSet;
    }
}
