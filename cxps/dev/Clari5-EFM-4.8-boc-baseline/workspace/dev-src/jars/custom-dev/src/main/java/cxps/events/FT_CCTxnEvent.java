package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import cxps.noesis.constants.Constants;
import cxps.noesis.core.Event;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by nidhi on 18/10/16.
 */
public class FT_CCTxnEvent extends Event {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String source;
    private String keys;
    private int msgType;
    private int posEntryMode;
    private String swipeType;
    private String cardNo;
    private String custId;
    private int procCode;
    private String merchantId;
    private String merchantName;
    private String mcc;
    private String terminalId;
    private String acqInstIdCode;
    private String respCode;
    private String txnCntryCode;
    private String acqCntryCode;
    private String cardTypeFlg;
    private String txnCrncyCode;
    private String channel;
    private String interbankcard;
    private String bin;
    private String acctNo;
    private Double centerCurrency;
    private Double amtCenter;
    private Double txnAmt;
    private String txnReferenceNo;
    private Double avblBalance;
    private Double otbCrLlimit;
    private Double otbCashLimit;
    private String acqInstAddr;
    private String acqInstCity;
    private String addrNetwork;
    private String threedSecureFlg;
    private String issuerFlg;
    private String contryregion;
    private String subregion;


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        if (StringUtils.isNullOrEmpty(msgType)) {
            throw new IllegalArgumentException("Value is required: 'msg-type'");
        } else {
            try {
                int val = Integer.parseInt(msgType);
                this.msgType = val;
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid Value>field:msg-type>value:" + msgType + " , must be integer");
            }
        }
    }

    public int getPosEntryMode() {
        return posEntryMode;
    }

    public void setPosEntryMode(String posEntryMode) {
        if (StringUtils.isNullOrEmpty(posEntryMode)) {
            throw new IllegalArgumentException("Value is required: 'pos-entry-mode'");
        }
        else{
            try {
                int val = Integer.parseInt(posEntryMode);
                this.posEntryMode = val;
            }
            catch(NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid Value>field:pos-entry-mode>value:" + posEntryMode + " , must be integer");

            }
        }
    }

    public String getSwipeType() {
        return swipeType;
    }

    public void setSwipeType(String swipeType) {
        if (StringUtils.isNullOrEmpty(swipeType)) {
            throw new IllegalArgumentException("Value is required: 'swipe-type'");
        }
        else {
            this.swipeType = swipeType;
        }
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        if (StringUtils.isNullOrEmpty(cardNo)) {
            throw new IllegalArgumentException("Value is required: 'card-no'");
        }
        else {
            this.cardNo = cardNo;
        }
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public int getProcCode() {
        return procCode;
    }

    public void setProcCode(String procCode) {
        if (StringUtils.isNullOrEmpty(procCode)) {
            this.procCode = 0;
        }else {
            try{
                int val = Integer.parseInt(procCode);
                this.procCode = val;
            }
            catch(NumberFormatException ex){
                throw new IllegalArgumentException("Invalid Value>field:proc-code>value:" + procCode + " , must be integer");
            }
        }
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        if (StringUtils.isNullOrEmpty(mcc)) {
            throw new IllegalArgumentException("Value is required: 'mcc'");
        }
        else {
            this.mcc = mcc;
        }
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        if (StringUtils.isNullOrEmpty(terminalId)) {
            throw new IllegalArgumentException("Value is required: 'terminal-id'");
        }
        else{
            this.terminalId = terminalId;
        }
    }

    public String getAcqInstIdCode() {
        return acqInstIdCode;
    }

    public void setAcqInstIdCode(String acqInstIdCode) {
        this.acqInstIdCode = acqInstIdCode;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        if (StringUtils.isNullOrEmpty(respCode)) {
            throw new IllegalArgumentException("Value is required: 'resp-code'");
        }
        else {
            this.respCode = respCode;
        }
    }

    public String getTxnCntryCode() {
        return txnCntryCode;
    }

    public void setTxnCntryCode(String txnCntryCode) {
        if (StringUtils.isNullOrEmpty(txnCntryCode)) {
            throw new IllegalArgumentException("Value is required: 'txn-cntry-code'");
        }
        else {
            this.txnCntryCode = txnCntryCode;
        }
    }

    public String getAcqCntryCode() {
        return acqCntryCode;
    }

    public void setAcqCntryCode(String acqCntryCode) {
        if (StringUtils.isNullOrEmpty(acqCntryCode)) {
            throw new IllegalArgumentException("Value is required: 'acq-cntry-code'");
        }
        else {
            this.acqCntryCode = acqCntryCode;
        }
    }

    public String getCardTypeFlg() {
        return cardTypeFlg;
    }

    public void setCardTypeFlg(String cardTypeFlg) {

        if (StringUtils.isNullOrEmpty(cardTypeFlg)) {
            throw new IllegalArgumentException("Value is required: 'card-type-flg'");
        }
        else {
            this.cardTypeFlg = cardTypeFlg;
        }
    }

    public String getTxnCrncyCode() {
        return txnCrncyCode;
    }

    public void setTxnCrncyCode(String txnCrncyCode) {
        this.txnCrncyCode = txnCrncyCode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        if (StringUtils.isNullOrEmpty(channel)) {
            throw new IllegalArgumentException("Value is required: 'channel'");
        }
        else {
            this.channel = channel;
        }
    }

    public String getInterbankcard() {
        return interbankcard;
    }

    public void setInterbankcard(String interbankcard) {
        this.interbankcard = interbankcard;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public Double getCenterCurrency() {
        return centerCurrency;
    }

    public void setCenterCurrency(String centerCurrency) {
        try
        {
            double centCurr = Double.parseDouble(centerCurrency);
            this.centerCurrency = centCurr;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:centerCurrency>value:" + centerCurrency);
        }
    }

    public Double getAmtCenter() {
        return amtCenter;
    }

    public void setAmtCenter(String amtCenter) {
        try
        {
            double amtCent = Double.parseDouble(amtCenter);
            this.amtCenter = amtCent;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:amt-Center>value:" + amtCenter);
        }

    }



    public Double getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(Double txnAmt) {
        if (txnAmt==null || txnAmt==' ') {
            throw new IllegalArgumentException("Value is required: txn-amt");
        } else {
            try {
                //double txnAm = Double.parseDouble(String.valueOf(txnAmt));
                this.txnAmt = txnAmt;
                System.out.println("-----------------txn-ftcc"+this.txnAmt);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid Value>field:txn-amt>value:" + txnAmt);
            }
        }
    }

    public String getTxnReferenceNo() {
        return txnReferenceNo;
    }

    public void setTxnReferenceNo(String txnReferenceNo) {
        this.txnReferenceNo = txnReferenceNo;
    }

    public Double getAvblBalance() {
        return avblBalance;
    }

    public void setAvblBalance(String avblBalance) {
        if (StringUtils.isNullOrEmpty(avblBalance)){
            this.avblBalance = 0.0;
        }else{
            try
            {
                double avblbal = Double.parseDouble(avblBalance);
                this.avblBalance = Double.valueOf(avblbal);
                System.out.println("---------aval-ftcc"+this.avblBalance);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid Value>field:avbl-balance>value:" + avblBalance);
            }
        }
    }

    public Double getOtbCrLlimit() {
        return otbCrLlimit;
    }

    public void setOtbCrLlimit(String otbCrLlimit) {
        if (StringUtils.isNullOrEmpty(otbCrLlimit)) {
            this.otbCrLlimit = 0.0;
        }
        else{
            try
            {
                this.otbCrLlimit = Double.valueOf(otbCrLlimit);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid Value>field:otb-cr-limit>value:" + otbCrLlimit + " , must be double");
            }
        }
    }

    public Double getOtbCashLimit() {
        return otbCashLimit;
    }

    public void setOtbCashLimit(String otbCashLimit) {
        if (StringUtils.isNullOrEmpty(otbCashLimit)){
            this.otbCashLimit = 0.0;
        } else {
            try{
                double val = Double.parseDouble(otbCashLimit);
                this.otbCashLimit = val;
            }
            catch(NumberFormatException ex){
                throw new IllegalArgumentException("Invalid Value>field:otb-cash-limit>value:" + otbCashLimit + " , must be double");
            }
        }
    }

    public String getAcqInstAddr() {
        return acqInstAddr;
    }

    public void setAcqInstAddr(String acqInstAddr) {
        this.acqInstAddr = acqInstAddr;
    }

    public String getAcqInstCity() {
        return acqInstCity;
    }

    public void setAcqInstCity(String acqInstCity) {
        this.acqInstCity = acqInstCity;
    }

    public String getAddrNetwork() {
        return addrNetwork;
    }

    public void setAddrNetwork(String addrNetwork) {
        this.addrNetwork = addrNetwork;
    }

    public String getThreedSecureFlg() {
        return threedSecureFlg;
    }

    public void setThreedSecureFlg(String threedSecureFlg) {
        if (StringUtils.isNullOrEmpty(threedSecureFlg)) {
            throw new IllegalArgumentException("Value is required: '3d-secure-flg'");
        }
        else {
            this.threedSecureFlg = threedSecureFlg;
        }
    }

    public String getIssuerFlg() {
        return issuerFlg;
    }

    public void setIssuerFlg(String issuerFlg) {

        if (StringUtils.isNullOrEmpty(issuerFlg)) {
            throw new IllegalArgumentException("Value is required: 'issuer-flag'");
        }else {
            this.issuerFlg = issuerFlg;
        }

    }

    public String getContryregion() {
        return contryregion;
    }

    public void setContryregion(String contryregion) {
        if (StringUtils.isNullOrEmpty(contryregion)) {
            throw new IllegalArgumentException("Value is required: 'country-region'");
        } else {
            this.contryregion = contryregion;
        }
    }
    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        if (StringUtils.isNullOrEmpty(subregion)) {
            throw new IllegalArgumentException("Value is required: 'sub-region'");
        } else {
            this.subregion = subregion;
        }
    }

    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setEventType("ft");

        setEventSubType("cc_txn");
        setEventName("ft_cc_txn");
        setHostId("F");
        setSource ((String) msgMap.get("source"));
        setKeys((String) msgMap.get("keys"));
        setMsgType((String) msgMap.get("msg-type"));
        setPosEntryMode((String) msgMap.get("pos-entry-mode"));
        setSwipeType((String) msgMap.get("swipe-type"));
        setCardNo((String) msgMap.get("card-no"));
        setCustId((String) msgMap.get("cust-id"));
        setCustId((String) msgMap.get("cust_id"));
        setProcCode((String) msgMap.get("proc-code"));
        setMerchantId((String) msgMap.get("merchant-id"));
        setMerchantName((String) msgMap.get("merchant-name"));
        setMcc((String) msgMap.get("mcc"));
        setTerminalId((String) msgMap.get("terminal-id"));
        setAcqInstIdCode((String) msgMap.get("acq-inst-id-code"));
        setRespCode((String) msgMap.get("resp-code"));
        setTxnCntryCode((String) msgMap.get("txn-cntry-code"));
        setAcqCntryCode((String) msgMap.get("acq-cntry-code"));
        setCardTypeFlg((String) msgMap.get("card-type-flg"));
        setTxnCrncyCode((String) msgMap.get("txn-crncy-code"));
        setChannel((String) msgMap.get("channel"));
        setInterbankcard((String) msgMap.get("interbankcard"));
        setBin((String) msgMap.get("BIN"));
        setAcctNo((String) msgMap.get("acct-no"));
        setTxnAmt(Double.parseDouble(msgMap.get("txn-amt") + ""));
        setTxnReferenceNo((String) msgMap.get("txn-reference-no"));
        setAvblBalance((String) msgMap.get("avbl-balance"));
        setOtbCrLlimit((String) msgMap.get("otb-cr-limit"));
        setOtbCashLimit((String) msgMap.get("otb-cash-limit"));
        setAcqInstAddr((String) msgMap.get("acq-inst-addr"));
        setAcqInstCity((String) msgMap.get("acq-inst-city"));
        setAddrNetwork((String) msgMap.get("addr-network"));
        setThreedSecureFlg((String) msgMap.get("3d-secure-flg"));
        setIssuerFlg((String) msgMap.get("issuer-flag"));
        setContryregion((String) msgMap.get("country-region"));
        setSubregion((String) msgMap.get("sub-region"));


    }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        String hostCustKey = this.getCustId();

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
        String cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, this.getHostId(), hostCustKey);

        WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_CUST, cxCifId);
        wi.addParam("cxCifID", cxCifId);

        wsInfoSet.add(wi);
      return wsInfoSet;

    }

    public JSONObject getFRJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("EventType",getEventType());
        jsonObject.put("txn_type","CC");
        jsonObject.put("EventSubType",getEventSubType());
        jsonObject.put("cust-id",getCustId());
        jsonObject.put("event-id",getEventId());
        jsonObject.put("sys_time",getEventTS().getTime());
        jsonObject.put("event-name",getEventName());
        jsonObject.put("host-id",getHostId());
        jsonObject.put("source",getSource());
        jsonObject.put("keys",getKeys());
        jsonObject.put("msg-type",getMsgType());
        jsonObject.put("pos-entry-mode",getPosEntryMode());
        jsonObject.put("swipe-type",getSwipeType());
        jsonObject.put("card_no",getCardNo());
        jsonObject.put("cust-id",getCustId());
        jsonObject.put("proc-code",getProcCode());
        jsonObject.put("merchant-id",getMerchantId());
        jsonObject.put("merchant_name",getMerchantName());
        jsonObject.put("mcc",getMcc());
        jsonObject.put("terminal-id",getTerminalId());
        jsonObject.put("acq-inst-id-code",getAcqInstIdCode());
        jsonObject.put("resp-code",getRespCode());
        jsonObject.put("country_code",getTxnCntryCode());
        jsonObject.put("acq-cntry-code",getAcqCntryCode());
        jsonObject.put("card-type-flg",getCardTypeFlg());
        jsonObject.put("currency",getTxnCrncyCode());
        jsonObject.put("channel",getChannel());
        jsonObject.put("centercurrency",getCenterCurrency());
        jsonObject.put("amt_center",getAmtCenter());
        jsonObject.put("BIN",getBin());
        jsonObject.put("acct-no",getAcctNo());
        jsonObject.put("txn_amount",getTxnAmt());
        jsonObject.put("txn-reference-no",getTxnReferenceNo());
        jsonObject.put("avbl-balance",getAvblBalance());
        jsonObject.put("otb-cr-limit",getOtbCrLlimit());
        jsonObject.put("otb-cash-limit",getOtbCashLimit());
        jsonObject.put("acq-inst-addr",getAcqInstAddr());
        jsonObject.put("acq-inst-city",getAcqInstCity());
        jsonObject.put("addr-network",getAddrNetwork());
        jsonObject.put("3d-secure-flg",getThreedSecureFlg());
        jsonObject.put("issuer-flag",getIssuerFlg());
        jsonObject.put("country-region",getContryregion());
        jsonObject.put("sub-region",getSubregion());
        return jsonObject;
    }
}


