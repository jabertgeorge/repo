package cxps.events;

import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Author: aditya.
 * Date: 19/10/15
 * TODO: fromMap(), toJson()
 */
public class TR_SecurityPositionChangeEvent extends cxps.noesis.core.Event {

    private String clientId;
    private String instrumentId;
    private Double concentrationPercent;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");

    public TR_SecurityPositionChangeEvent(){}

    public TR_SecurityPositionChangeEvent(String clientId, String instrumentId){
        this.clientId = clientId;
        this.instrumentId = instrumentId;
    }

    static long counter = 0;
    private static String generateEventId() {
    	if(counter > 100000) counter=0;
    	return "S"+Long.toString((System.currentTimeMillis()+counter++));
    }

    public String toJson(){
        JSONObject eventJson = new JSONObject();
        JSONObject msgJson = new JSONObject();

        try {

            eventJson.put("msgName", "EVENT");
            eventJson.put("msgSource", "CORE");
            eventJson.put("EventType", "TR");
            eventJson.put("EventSubType","SecurityPositionChange");

            //Long microseconds = System.currentTimeMillis();
            //String sysTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.").format(microseconds)+String.format("%06d", microseconds%1000);
            //msgJson.put("sys_time",sysTime);
            msgJson.put("sys_time",sdf.format(new Date()));
            msgJson.put("event_id", generateEventId());
            msgJson.put("HostId","F");
            msgJson.put("event-name","tr_securitypositionchange");
            msgJson.put("clientId", clientId);
            msgJson.put("instrumentId", instrumentId);
            msgJson.put("concentrationPercent", Double.toString(concentrationPercent));

            eventJson.put("msgBody",msgJson.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventJson.toString();
    }

    @Override
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setClientId((String) msgMap.get("clientId"));
        setInstrumentId((String) msgMap.get("instrumentId"));
        setConcentrationPercent((String) msgMap.get("concentrationPercent"));


        }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        return null;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public Double getConcentrationPercent() {
        return concentrationPercent;
    }

    public void setConcentrationPercent(Double concentrationPercent) {
        this.concentrationPercent = concentrationPercent;
    }

    public void setConcentrationPercent(String concentrationPercent) {
        try {
            double concentrationPcnt = Double.parseDouble(concentrationPercent);
            this.concentrationPercent=concentrationPcnt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:concentrationPercent>value:" + concentrationPercent);
        }
    }
}
