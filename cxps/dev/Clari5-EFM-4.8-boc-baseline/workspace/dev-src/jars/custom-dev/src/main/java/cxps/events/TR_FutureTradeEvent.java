package cxps.events;

import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.utils.StringUtils;
import cxps.noesis.core.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by anshul on 12/10/15.
 */
public class TR_FutureTradeEvent extends Event {

    private String tradingAccountId;
    private String accountType;
    private String clientId;
    private Double availableBalance;
    private Double marginBalanceAmount;
    private Double nonMarginBalanceAmount;
    private String transactionType; // BUY or SELL
    private String instrumentId;
    private String instrumentDesc;
    private Integer instrumentQuantity;
    private Double instrumentRate;
    private Double brokeragePercentage;
    private String tradeType; //  CASH or MARGIN
    private String exchange;
    private Double portFolioValue;
    private Date sysDateTime;
    private String remisierCode;
    private String orderType; // BOOK/CANCEL/CONFIRM
    private Date expiryDate;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
    private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SS");





    @Override
    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setTradingAccountId((String) msgMap.get("TA_account-id"));
        setAccountType((String) msgMap.get("acct-type"));
        setClientId((String) msgMap.get("client-id"));
        setAvailableBalance((String) msgMap.get("avl-bal"));
        setMarginBalanceAmount((String) msgMap.get("margin-bal-amt"));
        setNonMarginBalanceAmount((String) msgMap.get("non-margin-bal-amt"));
        setInstrumentId((String) msgMap.get("instrument_id"));
        setInstrumentDesc((String) msgMap.get("instrument_desc"));
        setInstrumentQuantity((String) msgMap.get("instrument_qty"));
        setInstrumentRate((String) msgMap.get("instrument_rate"));
        setBrokeragePercentage((String) msgMap.get("brokerage_pcnt"));
        setTradeType((String) msgMap.get("Trade Type"));
        setExchange((String) msgMap.get("Exchange"));
        setPortFolioValue((String) msgMap.get("Prior_PortFolio_value"));
        setSysDateTime((String) msgMap.get("sys-datetime"));
        setRemisierCode((String) msgMap.get("remisier-code"));
        setOrderType((String) msgMap.get("order-type"));
        setExpiryDate((String) msgMap.get("expiry-date"));
        setTransactionType((String)msgMap.get("tran-type"));
        setMarginBalanceAmount();
    }


    private void setMarginBalanceAmount() {
		// TODO Auto-generated method stub
    	this.marginBalanceAmount = this.instrumentQuantity*this.instrumentRate;
	}

	public String getTradingAccountId() {
        return tradingAccountId;
    }

    public void setTradingAccountId(String tradingAccountId) {
        this.tradingAccountId = tradingAccountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        try {
            double availBal = Double.parseDouble(availableBalance);
            this.availableBalance = availBal;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:availableBalance>value:" + availableBalance);
        }
    }

    public Double getMarginBalanceAmount() {
        return marginBalanceAmount;
    }

    public void setMarginBalanceAmount(String marginBalanceAmount) {
        try {
            double marginBalanceAmt = Double.parseDouble(marginBalanceAmount);
            this.marginBalanceAmount = marginBalanceAmt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:marginBalanceAmount>value:" + marginBalanceAmount);
        }
    }

    public Double getNonMarginBalanceAmount() {
        return nonMarginBalanceAmount;
    }

    public void setNonMarginBalanceAmount(String nonMarginBalanceAmount) {
        try {
            double nonMarginBalanceAmt = Double.parseDouble(nonMarginBalanceAmount);
            this.nonMarginBalanceAmount = nonMarginBalanceAmt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:nonMarginBalanceAmount>value:" + nonMarginBalanceAmount);
        }
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getInstrumentDesc() {
        return instrumentDesc;
    }

    public void setInstrumentDesc(String instrumentDesc) {
        this.instrumentDesc = instrumentDesc;
    }

    public Integer getInstrumentQuantity() {
        return instrumentQuantity;
    }

    public void setInstrumentQuantity(String instrumentQuantity) {
    	try {
    		this.instrumentQuantity = Integer.valueOf(instrumentQuantity);
    	} catch (NumberFormatException ex) {
    		throw new IllegalArgumentException("Invalid Value>field:instrument_qty>value:" + instrumentQuantity);
    	}
    }

    public Double getInstrumentRate() {
        return instrumentRate;
    }

    public void setInstrumentRate(String instrumentRate) {
        try {
            double instRate = Double.parseDouble(instrumentRate);
            this.instrumentRate = instRate;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:instrumentRate>value:" + instrumentRate);
        }
    }

    public Double getBrokeragePercentage() {
        return brokeragePercentage;
    }

    public void setBrokeragePercentage(String brokeragePercentage) {
        try {
            double brokeragePcnt = Double.parseDouble(brokeragePercentage);
            this.brokeragePercentage =  brokeragePcnt;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:brokeragePercentage>value:" + brokeragePercentage);
        }
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public Double getPortFolioValue() {
        return portFolioValue;
    }

    public void setPortFolioValue(String portFolioValue) {
        try {
            double portFolioVal = Double.parseDouble(portFolioValue);
            this.portFolioValue = portFolioVal;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid Value>field:portFolioValue>value:" + portFolioValue);
        }
    }

    public Date getSysDateTime() {
        return sysDateTime;
    }

    public void setSysDateTime(String sysDateTime) {
        if (StringUtils.isNullOrEmpty(sysDateTime)) {
            throw new IllegalArgumentException("Value is required: sysDateTime");
        } else {
            try {
                this.sysDateTime = dateFormat1.parse(sysDateTime);
            } catch (ParseException ex) {
                try {
                    this.sysDateTime = dateFormat2.parse(sysDateTime);
                } catch (ParseException ex1) {
                    try {
                        this.sysDateTime = dateFormat.parse(sysDateTime);
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("Invalid Value>field:sysDateTime>value:" + sysDateTime);
                    }
                }
            }
        }
    }

    public String getRemisierCode() {
        return remisierCode;
    }

    public void setRemisierCode(String remisierCode) {
        this.remisierCode = remisierCode;
    }


	public String getOrderType() {
		return orderType;
	}


	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

    public Date getExpiryDate() {
        return expiryDate;
    }


    public void setExpiryDate(String expiryDate) {
        if (StringUtils.isNullOrEmpty(expiryDate)) {
            throw new IllegalArgumentException("Value is required: sysDateTime");
        } else {
            try {
                this.expiryDate = dateFormat1.parse(expiryDate);
            } catch (ParseException ex) {
                try {
                    this.expiryDate = dateFormat2.parse(expiryDate);
                } catch (ParseException ex1) {
                    try {
                        this.expiryDate = dateFormat.parse(expiryDate);
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("Invalid Value>field:expiryDate>value:" + expiryDate);
                    }
                }
            }
        }
    }
    @Override
    @SuppressWarnings("unchecked")
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession rdbmsSession) {
        ICXLog cxLog = CXLog.fenter("derive.FT_AccountTxnEvent", new Object[0]);
        HashSet wsInfoSet = new HashSet();
        String hostAcctKey = this.getTradingAccountId();
        if(hostAcctKey == null || hostAcctKey.trim().length() == 0) {
            cxLog.ferrexit("Nil-HostAcctKey", new Object[0]);
        }

        CxKeyHelper h = Hfdb.getCxKeyHelper();
        String cxAcctKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, this.getHostId(), hostAcctKey.trim());
        if(!CxKeyHelper.isValidCxKey(WorkspaceName.ACCOUNT, cxAcctKey)) {
            cxLog.ferrexit("Invalid-CxKey-" + cxAcctKey, new Object[0]);
        }

        WorkspaceInfo wi = new WorkspaceInfo("Account", cxAcctKey);
        wi.addParam("acctId", cxAcctKey);

        wsInfoSet.add(wi);
       return wsInfoSet;
    }


}
