:

main() {
    # Get basedir
    basedir=$( cd $(pwd) && cd $(dirname $0) && pwd )
    workD=${basedir}/converted
    oldD=${basedir}/archive/"$(date +'%d-%m-%Y')"

    mkdir -p ${workD}
    mkdir -p ${oldD}

    for f in ${basedir}/*
    do
        [ ! -f "${f}" ] && continue
	( echo "${f}" | grep -qw 'convert.sh$' ) && continue
        #[[ "${f}" = "${basedir}/convert.sh" ]] && continue
        file=$( basename "$f" )
        sed -e 's[ 	]*-begin-g' -e 's-end-\ng' "$f" | tr -d '\r' > "${workD}/${file}"
	mv "${basedir}/${file}" ${oldD}
    done
}

# Main method
main $*
