select rem.temp_ref_no,rem.[system],rem.ben_acct_no,rem.cust_id,rem.eventts,rem.cx_cust_id,cu.primarySOL 
from dbo.customer cu,
(select a.temp_ref_no,a.[system],a.ben_acct_no,a.cust_id,a.eventts,a.cx_cust_id 
from dbo.EVENT_FT_REMITTANCE a
where 
a.eventts>='2018/2/2' and a.eventts<='2018/2/2' and a.rem_type='I' and
(a.ben_add1 is null or trim(a.ben_add1) = '' OR
 a.ben_add2 is null or trim(a.ben_add2) = '' OR
 a.benf_bank is null or trim(a.benf_bank)='' or 
 a.ben_name is null or trim(a.ben_name)='' or 
 a.benf_id_no is null or trim(a.benf_id_no)='' or 
 a.ben_acct_no is null or trim(a.ben_acct_no)='' or 
 a.rem_bic is null or trim(a.rem_bic) = ''
)
) rem
where rem.cx_cust_id=cu.cxCifID;
