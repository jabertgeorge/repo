select l.cxCifID cxCifID,l.CustName custName,l.COREAccountID loanAccountId,
       l.MaturityDate maturityDate,cu.primarySOL homeBranch 
from dbo.LOAN_MASTER l,dbo.CUSTOMER cu
where l.cxCifID=cu.cxCifID and 
l.cxCifID in (select c.cx_key from dbo.CRC_RISK c where c.risk_level='L1');
