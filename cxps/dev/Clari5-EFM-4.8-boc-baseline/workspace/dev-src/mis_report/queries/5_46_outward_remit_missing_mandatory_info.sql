select rem.temp_ref_no,rem.[system],rem.rem_acct_no,rem.rem_cust_id,rem.eventts,rem.cx_cust_id,cu.primarySOL 
from dbo.customer cu,
(select a.temp_ref_no,a.[system],a.rem_acct_no,a.rem_cust_id,a.eventts,a.cx_cust_id 
from dbo.EVENT_FT_REMITTANCE a
where 
a.eventts>='2018/2/2' and a.eventts<='2018/2/2' and a.rem_type='O' and
(a.rem_add1 is null or trim(a.rem_add1) = '' OR
 a.rem_add2 is null or trim(a.rem_add2) = '' OR
 a.benf_bank is null or trim(a.benf_bank)='' or 
 a.ben_name is null or trim(a.ben_name)='' or 
 a.rem_name is null or trim(a.rem_name)='' or 
 a.rem_id_no is null or trim(a.rem_id_no)='' or 
 a.rem_acct_no is null or trim(a.rem_acct_no)='' or 
 a.ben_bic is null or trim(a.ben_bic) = ''
)
) rem
where rem.cx_cust_id=cu.cxCifID;
