select d.hostAcctId hostAcctId,d.acctCustID acctCustId,d.acctBrID acctBrId,c.primarySOL custBrId,c.custName custName from dbo.DDA d,dbo.CUSTOMER c where d.acctCustID=c.cxCifID and d.acctCustID in (
select d.acctCustID from dbo.DDA d where d.acctCustID is NOT NULL and TRIM(d.acctCustID) !='' and d.acctCustID like 'C_F_%' group by d.acctCustID having count(*) > 2
) order by acctCustID;
