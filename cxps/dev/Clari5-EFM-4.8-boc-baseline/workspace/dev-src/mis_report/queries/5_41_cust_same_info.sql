select cxCifID,custName, matchType, homeBranch from (
select cxCifID,custName,'Mobile' matchType,primarySOL homeBranch from dbo.customer where custMobile1 in (
select custMobile1 from dbo.CUSTOMER where custMobile1 is not NULL and custMobile1!='' group by custMobile1 having count(*) > 1)
union
select cxCifID,custName,'Address' matchType,primarySOL homeBranch from dbo.customer where upper(custMailingAddr) in (
select upper(custMailingAddr) custMailingAddr from dbo.CUSTOMER where custMailingAddr is not NULL and custMailingAddr!='' group by custMailingAddr having count(*) > 1)
union
select cxCifID,custName,'Email' matchType,primarySOL homeBranch from dbo.customer where UPPER(custEmail1) in (
select upper(custEmail1) custEmail1 from dbo.CUSTOMER where custEmail1 is not NULL and custEmail1!='' group by custEmail1 having count(*) > 1)
union
select cxCifID,custName,'Name' matchType,primarySOL homeBranch from dbo.customer where upper(custName) in (
select upper(custName) custName from dbo.CUSTOMER where custName is not NULL and custName != '' group by custName having count(*) > 1)
union
select cxCifID,custName,'Telephone' matchType,primarySOL homeBranch from dbo.customer where custHomeTel in (
select custHomeTel from dbo.CUSTOMER where custHomeTel is not NULL and custHomeTel!='' group by custHomeTel having count(*) > 1)
) as a order by matchType;
