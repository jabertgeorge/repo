CREATE TABLE CREDIT_CARD_MASTER (
	customerID varchar(10),
	accountId varchar(10) NOT NULL,
	creditCardNumber varchar(16),
	creditCardLimit numeric(10,0),
	interestRate numeric(10,0),
	nicRegistration varchar(10),
	annulFee varchar(10),
	productType varchar(10),
	productDescription varchar(10),
	bankRelatedParty varchar(10),
	acctOpenDate datetime2,
	renewalFee varchar(10),
	customerName varchar(10),
	homeBranch varchar(10)
) 
