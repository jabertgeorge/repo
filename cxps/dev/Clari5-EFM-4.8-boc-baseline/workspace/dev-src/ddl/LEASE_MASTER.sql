CREATE TABLE LEASE_MASTER (
	leaseFacilityNumber varchar(100),
	stakeHolderId varchar(100) NOT NULL,
	coreCifId varchar(100),
	leaseCreationDate datetime2,
	leaseFacilityAmt numeric(10,2),
	leaseAmount numeric(10,2),
	customerContribution numeric(10,2),
	nicBr varchar(100),
	interestRate numeric(10,2),
	productCode varchar(100),
	productCodeDesc varchar(100),
	bankRelatedFamily varchar(100),
	customerName varchar(100),
	leasingBranchId varchar(100),
	status varchar(100),
	LeaseMaturityDate datetime2 NOT NULL
)
