package https;

import clari5.platform.util.Hocon;

import java.io.File;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Properties;

class MyAuthenticator extends Authenticator
{
    Hocon authenticator;

    private final PasswordAuthentication authentication;

    public MyAuthenticator() {
        authenticator = new Hocon(new File("conf/userPassAuth.conf"));
        String userName = authenticator.getString("auth.username");
        String password = authenticator.getString("auth.password");
        if (userName == null || password == null) {
            authentication = null;
        } else {
            authentication = new PasswordAuthentication(userName, password.toCharArray());
        }
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return authentication;
    }
}


class MyAuthenticatorWL extends Authenticator
{
    Hocon authenticatorWL;
    private final PasswordAuthentication authentication;

    public MyAuthenticatorWL() {
        authenticatorWL = new Hocon(new File("conf/userPassAuthWL.conf"));
        String userName = authenticatorWL.getString("authwl.username");
        String password = authenticatorWL.getString("authwl.password");
        if (userName == null || password == null) {
            authentication = null;
        } else {
            authentication = new PasswordAuthentication(userName, password.toCharArray());
        }
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return authentication;
    }
}

class MyAuthenticatorTC extends Authenticator
{
    Hocon authenticatorTC;
    private final PasswordAuthentication authentication;

    public MyAuthenticatorTC() {
        authenticatorTC = new Hocon(new File("conf/userPassAuthTC.conf"));
        String userName = authenticatorTC.getString("authtc.username");
        String password = authenticatorTC.getString("authtc.password");
        if (userName == null || password == null) {
            authentication = null;
        } else {
            authentication = new PasswordAuthentication(userName, password.toCharArray());
        }
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return authentication;
    }
}