package https;

import clari5.platform.util.Hocon;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HttpsFetcher {
    static Hocon urlReader;

    static {
        urlReader = new Hocon();
        urlReader.loadFromContext("urlReader.conf");
    }

    public static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static String getPrevDate(String dateFormat) {

        /*long time = System.currentTimeMillis() - 1;
        DateFormat df = new SimpleDateFormat(dateFormat);
        String date = df.format(time);
        System.out.println("date for previous is:"+date);
        return date;*/

        DateFormat dformat = new SimpleDateFormat(dateFormat);
        System.out.println("previous date is: " + dformat.format(yesterday()));
        return dformat.format(yesterday());
    }

    public static void fetchDataFromServer() {

        System.out.println("__ENTERING CluemasterData::fetchDataFromServer()");
        HttpsURLConnection http = null;
        InputStream is = null;
        OutputStream outputStream = null;
        try {
            String[] splitUrl = urlReader.getString("url_path.url").split("\\|");
            String originalUrl = splitUrl[0] + getPrevDate(splitUrl[1]) + splitUrl[2];
            URL u = new URL(originalUrl);
            http = (HttpsURLConnection) u.openConnection();
            Authenticator.setDefault(new MyAuthenticator());
            http.setAllowUserInteraction(true);
            http.setRequestMethod("GET");
            http.connect();

            int contentLength = http.getContentLength();
            System.out.println("File contentLength = " + contentLength + " bytes");

            is = http.getInputStream();
            String[] urlSplit = urlReader.getString("url_path.urlOS").split("\\|");
            String writeURL = urlSplit[0] + getPrevDate(urlSplit[1]) + urlSplit[2];
            System.out.println("write url is: " + writeURL);
            outputStream = new FileOutputStream(writeURL);
            byte[] buffer = new byte[2048];
            int length;
            int downloaded = 0;


            while ((length = is.read(buffer)) != -1) {
                // Writing data
                outputStream.write(buffer, 0, length);
                downloaded += length;
                //System.out.println("Downlad Status: " + (downloaded * 100) / (contentLength * 1.0) + "%");
            }



            /*BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line + "\n");
            }
            System.out.println(stringBuilder.toString());*/
        } catch (FileNotFoundException f) {
            System.out.println("File Not Present...");
        } catch (Exception he) {
            he.printStackTrace();
        } finally {
            try {
                http.disconnect();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                outputStream.close();
            } catch (Exception e) {
            }
        }
    }

    public static void fetchDataFromServerWL() {

        System.out.println("__ENTERING ::fetchDataFromServerWL()");
        HttpsURLConnection http = null;
        InputStream is = null;
        OutputStream outputStream = null;
        try {
            String[] splitUrl = urlReader.getString("url_path.urlWL").split("\\|");
            String originalUrl = splitUrl[0] + getPrevDate(splitUrl[1]) + splitUrl[2];
            System.out.println("URL for WL is: " + originalUrl);
            URL u = new URL(originalUrl);
            http = (HttpsURLConnection) u.openConnection();
            Authenticator.setDefault(new MyAuthenticatorWL());
            http.setAllowUserInteraction(true);
            http.setRequestMethod("GET");
            http.connect();

            int contentLength = http.getContentLength();
            System.out.println("File contentLength = " + contentLength + " bytes");

            is = http.getInputStream();
            String[] urlSplit = urlReader.getString("url_path.urlWLOS").split("\\|");
            String writeURL = urlSplit[0] + getPrevDate(urlSplit[1]) + urlSplit[2];
            System.out.println("write url is: " + writeURL);
            outputStream = new FileOutputStream(writeURL);
            byte[] buffer = new byte[2048];
            int length;
            int downloaded = 0;

            while ((length = is.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
                downloaded += length;
            }
        } catch (FileNotFoundException f) {
            System.out.println("File Not Present...");
        } catch (Exception he) {
            he.printStackTrace();
        } finally {
            try {
                http.disconnect();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                outputStream.close();
            } catch (Exception e) {
            }
        }
    }

    public static void fetchDataFromServerTC() {

        System.out.println("__ENTERING::fetchDataFromServerTC()");
        HttpsURLConnection http = null;
        InputStream is = null;
        OutputStream outputStream = null;
        try {
            String[] splitUrl = urlReader.getString("url_path.urlTC").split("\\|");
            String originalUrl = splitUrl[0] + getPrevDate(splitUrl[1]) + splitUrl[2];
            URL u = new URL(originalUrl);
            http = (HttpsURLConnection) u.openConnection();
            Authenticator.setDefault(new MyAuthenticatorTC());
            http.setAllowUserInteraction(true);
            http.setRequestMethod("GET");
            http.connect();

            int contentLength = http.getContentLength();
            System.out.println("TC File contentLength = " + contentLength + " bytes");

            is = http.getInputStream();
            String[] urlSplit = urlReader.getString("url_path.urlTCOS").split("\\|");
            String writeURL = urlSplit[0] + getPrevDate(urlSplit[1]) + urlSplit[2];
            System.out.println("write url is: " + writeURL);
            outputStream = new FileOutputStream(writeURL);
            byte[] buffer = new byte[2048];
            int length;
            int downloaded = 0;

            while ((length = is.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
                downloaded += length;
            }
        } catch (FileNotFoundException f) {
            System.out.println("File Not Present...");
        } catch (Exception he) {
            he.printStackTrace();
        } finally {
            try {
                http.disconnect();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                outputStream.close();
            } catch (Exception e) {
            }
        }
    }
}
