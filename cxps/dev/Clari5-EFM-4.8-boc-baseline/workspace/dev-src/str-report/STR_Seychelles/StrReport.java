// -- GENERATED CODE --
package clari5.report.db;

import clari5.platform.util.Hocon;

import java.util.regex.Pattern;
import java.util.Map;
import java.util.List;
import java.util.HashMap;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.Timestamp;
import java.io.UnsupportedEncodingException;

import cxps.apex.utils.StringUtils;
import clari5.platform.rdbms.*;
import clari5.report.db.mappers.StrReportMapper;

public class StrReport extends StrReportKey {

    protected String accountId;
    protected String noTransactionToBeReported;
    protected String transactionId;
    protected String relatedAcct;
    protected String additionalRemarks;
    protected String volumeOfTransactions;
    protected String beneficiaryDetails;
    protected String factName;
    protected String riskLevel;
    protected String caseEntityId;
    protected String entityIdType;
    protected String incidentScore;
    protected String entityName;
    protected String incidentClosedDate;
    protected String entityId;
    protected String caseEntityName;
    protected String caseDisplayKey;
    protected Integer noOfStr;
    protected Long lastFiledStr;
    protected String relationshipBeginDate;
    protected String custIdNum;
    protected String custFax;
    protected String professionType;
    protected String custType;
    protected String custGender;
    protected String custMobile;
    protected String custEmail;
    protected String custMailingAddr;
    protected String custResAddr;
    protected String custResAddrCity;
    protected String custResAddrState;
    protected String custResAddrCountry;
    protected String custDOB;
    protected String custName;
    protected String createdOn;
    protected String reportRelatedTo;
    protected String accountType;
    protected String personExecutingTxnAcctholderName;
    protected String personExecutingTxnAcctholderDateOfBirth;
    protected String personExecutingTxnAcctholderIdNumber;
    protected String personExecutingTxnAcctholderPassportNumber;
    protected String personExecutingTxnAcctholderNationality;
    protected String businessentityPlaceOfIncorporation;
    protected String businessentityDateOfIncorporation;
    protected String businessentityBusinessActivity;
    protected String aCompanyDirectorsDateOfBirth;
    protected String aCompanyDirectorsIdNumber;
    protected String aCompanyDirectorsPassportNumber;
    protected String aCompanyDirectorsNationality;
    protected String aCompanyDirectorsOccupation;
    protected String bCompanyDirectorsDateOfBirth;
    protected String bCompanyDirectorsIdNumber;
    protected String bCompanyDirectorsPassportNumber;
    protected String bCompanyDirectorsNationality;
    protected String bCompanyDirectorsOccupation;
    protected String cCompanyDirectorsDateOfBirth;
    protected String cCompanyDirectorsIdNumber;
    protected String cCompanyDirectorsPassportNumber;
    protected String cCompanyDirectorsNationality;
    protected String cCompanyDirectorsOccupation;
    protected String aBeneficialOwnersName;
    protected String aBeneficialOwnersDateOfBirth;
    protected String aBeneficialOwnersIdNumber;
    protected String aBeneficialOwnersPassportNumber;
    protected String aBeneficialOwnersNationality;
    protected String aBeneficialOwnersOccupation;
    protected String aBeneficialOwnersDateofappointment;
    protected String aBeneficialOwnersDateofresignation;
    protected String bBeneficialOwnersName;
    protected String bBeneficialOwnersDateOfBirth;
    protected String bBeneficialOwnersIdNumber;
    protected String bBeneficialOwnersPassportNumber;
    protected String bBeneficialOwnersNationality;
    protected String bBeneficialOwnersOccupation;
    protected String bBeneficialOwnersDateofappointment;
    protected String bBeneficialOwnersDateofresignation;
    protected String informationAssociatedPersonsCompaniesEtc;
    protected String whyTheTxnWasReportedSuspicious;
    protected String designation;
    protected String date;

	public StrReport(){
	    super();
	    clear();
	}

    public StrReport(RDBMSSession session) {
        super(session);
        clear();
    }

	public void clear(){
	    super.clear();
        this.accountId = null;
        this.noTransactionToBeReported = null;
        this.transactionId = null;
        this.relatedAcct = null;
        this.additionalRemarks = null;
        this.volumeOfTransactions = null;
        this.beneficiaryDetails = null;
        this.factName = null;
        this.riskLevel = null;
        this.caseEntityId = null;
        this.entityIdType = null;
        this.incidentScore = null;
        this.entityName = null;
        this.incidentClosedDate = null;
        this.entityId = null;
        this.caseEntityName = null;
        this.caseDisplayKey = null;
        this.noOfStr = null;
        this.lastFiledStr = null;
        this.relationshipBeginDate = null;
        this.custIdNum = null;
        this.custFax = null;
        this.professionType = null;
        this.custType = null;
        this.custGender = null;
        this.custMobile = null;
        this.custEmail = null;
        this.custMailingAddr = null;
        this.custResAddr = null;
        this.custResAddrCity = null;
        this.custResAddrState = null;
        this.custResAddrCountry = null;
        this.custDOB = null;
        this.custName = null;
        this.createdOn = null;
        this.reportRelatedTo = null;
        this.accountType = null;
        this.personExecutingTxnAcctholderName = null;
        this.personExecutingTxnAcctholderDateOfBirth = null;
        this.personExecutingTxnAcctholderIdNumber = null;
        this.personExecutingTxnAcctholderPassportNumber = null;
        this.personExecutingTxnAcctholderNationality = null;
        this.businessentityPlaceOfIncorporation = null;
        this.businessentityDateOfIncorporation = null;
        this.businessentityBusinessActivity = null;
        this.aCompanyDirectorsDateOfBirth = null;
        this.aCompanyDirectorsIdNumber = null;
        this.aCompanyDirectorsPassportNumber = null;
        this.aCompanyDirectorsNationality = null;
        this.aCompanyDirectorsOccupation = null;
        this.bCompanyDirectorsDateOfBirth = null;
        this.bCompanyDirectorsIdNumber = null;
        this.bCompanyDirectorsPassportNumber = null;
        this.bCompanyDirectorsNationality = null;
        this.bCompanyDirectorsOccupation = null;
        this.cCompanyDirectorsDateOfBirth = null;
        this.cCompanyDirectorsIdNumber = null;
        this.cCompanyDirectorsPassportNumber = null;
        this.cCompanyDirectorsNationality = null;
        this.cCompanyDirectorsOccupation = null;
        this.aBeneficialOwnersName = null;
        this.aBeneficialOwnersDateOfBirth = null;
        this.aBeneficialOwnersIdNumber = null;
        this.aBeneficialOwnersPassportNumber = null;
        this.aBeneficialOwnersNationality = null;
        this.aBeneficialOwnersOccupation = null;
        this.aBeneficialOwnersDateofappointment = null;
        this.aBeneficialOwnersDateofresignation = null;
        this.bBeneficialOwnersName = null;
        this.bBeneficialOwnersDateOfBirth = null;
        this.bBeneficialOwnersIdNumber = null;
        this.bBeneficialOwnersPassportNumber = null;
        this.bBeneficialOwnersNationality = null;
        this.bBeneficialOwnersOccupation = null;
        this.bBeneficialOwnersDateofappointment = null;
        this.bBeneficialOwnersDateofresignation = null;
        this.informationAssociatedPersonsCompaniesEtc = null;
        this.whyTheTxnWasReportedSuspicious = null;
        this.designation = null;
        this.date = null;

	}

    public int upsert() throws RdbmsException {
        try {
            return insert();
        }
        catch(RdbmsException.RecordAlreadyExists ignore){
            return update();
        }
    }

    public int insert(RDBMSSession session) throws RdbmsException {
        setMapper(session);
        return insert();
    }

    public int insert() throws RdbmsException {
        assert(mapper != null);

        try {

            return mapper.insert(this);
        }
        catch(Exception ex){
            session.convertException(this.getClass().getName() + ":" + this.getKey(), ex);
        }
        return -1;
    }

    public int update(RDBMSSession session) throws RdbmsException {
        setMapper(session);
        int updatenum=update();
        if(updatenum != 1)
            throw new RdbmsException.StaleObjectException("RVN Mismatch Error [" + this.getClass().getName() + "]");
        else
            return updatenum;
    }

    public int update() throws RdbmsException {
        assert(mapper != null);

        try {
            int count = mapper.update(this);if(count != 1) throw new RdbmsException.StaleObjectException("RVN Mismatch Error [" + this.getClass().getName() + "]");
			return count;
        }
		catch(RdbmsException ex){
			throw ex;
		}
        catch(Exception ex){
            session.convertException(this.getClass().getName(), ex);
        }
        return -1;
    }

    public String getAccountId(){ return accountId; }
    public String getNoTransactionToBeReported(){ return noTransactionToBeReported; }
    public String getTransactionId(){ return transactionId; }
    public String getRelatedAcct(){ return relatedAcct; }
    public String getAdditionalRemarks(){ return additionalRemarks; }
    public String getVolumeOfTransactions(){ return volumeOfTransactions; }
    public String getBeneficiaryDetails(){ return beneficiaryDetails; }
    public String getFactName(){ return factName; }
    public String getRiskLevel(){ return riskLevel; }
    public String getCaseEntityId(){ return caseEntityId; }
    public String getEntityIdType(){ return entityIdType; }
    public String getIncidentScore(){ return incidentScore; }
    public String getEntityName(){ return entityName; }
    public String getIncidentClosedDate(){ return incidentClosedDate; }
    public String getEntityId(){ return entityId; }
    public String getCaseEntityName(){ return caseEntityName; }
    public String getCaseDisplayKey(){ return caseDisplayKey; }
    public Integer getNoOfStr(){ return noOfStr; }
    public Long getLastFiledStr(){ return lastFiledStr; }
    public String getRelationshipBeginDate(){ return relationshipBeginDate; }
    public String getCustIdNum(){ return custIdNum; }
    public String getCustFax(){ return custFax; }
    public String getProfessionType(){ return professionType; }
    public String getCustType(){ return custType; }
    public String getCustGender(){ return custGender; }
    public String getCustMobile(){ return custMobile; }
    public String getCustEmail(){ return custEmail; }
    public String getCustMailingAddr(){ return custMailingAddr; }
    public String getCustResAddr(){ return custResAddr; }
    public String getCustResAddrCity(){ return custResAddrCity; }
    public String getCustResAddrState(){ return custResAddrState; }
    public String getCustResAddrCountry(){ return custResAddrCountry; }
    public String getCustDOB(){ return custDOB; }
    public String getCustName(){ return custName; }
    public String getCreatedOn(){ return createdOn; }
    public String getReportRelatedTo(){ return reportRelatedTo; }
    public String getAccountType(){ return accountType; }
    public String getPersonExecutingTxnAcctholderName(){ return personExecutingTxnAcctholderName; }
    public String getPersonExecutingTxnAcctholderDateOfBirth(){ return personExecutingTxnAcctholderDateOfBirth; }
    public String getPersonExecutingTxnAcctholderIdNumber(){ return personExecutingTxnAcctholderIdNumber; }
    public String getPersonExecutingTxnAcctholderPassportNumber(){ return personExecutingTxnAcctholderPassportNumber; }
    public String getPersonExecutingTxnAcctholderNationality(){ return personExecutingTxnAcctholderNationality; }
    public String getBusinessentityPlaceOfIncorporation(){ return businessentityPlaceOfIncorporation; }
    public String getBusinessentityDateOfIncorporation(){ return businessentityDateOfIncorporation; }
    public String getBusinessentityBusinessActivity(){ return businessentityBusinessActivity; }
    public String getACompanyDirectorsDateOfBirth(){ return aCompanyDirectorsDateOfBirth; }
    public String getACompanyDirectorsIdNumber(){ return aCompanyDirectorsIdNumber; }
    public String getACompanyDirectorsPassportNumber(){ return aCompanyDirectorsPassportNumber; }
    public String getACompanyDirectorsNationality(){ return aCompanyDirectorsNationality; }
    public String getACompanyDirectorsOccupation(){ return aCompanyDirectorsOccupation; }
    public String getBCompanyDirectorsDateOfBirth(){ return bCompanyDirectorsDateOfBirth; }
    public String getBCompanyDirectorsIdNumber(){ return bCompanyDirectorsIdNumber; }
    public String getBCompanyDirectorsPassportNumber(){ return bCompanyDirectorsPassportNumber; }
    public String getBCompanyDirectorsNationality(){ return bCompanyDirectorsNationality; }
    public String getBCompanyDirectorsOccupation(){ return bCompanyDirectorsOccupation; }
    public String getCCompanyDirectorsDateOfBirth(){ return cCompanyDirectorsDateOfBirth; }
    public String getCCompanyDirectorsIdNumber(){ return cCompanyDirectorsIdNumber; }
    public String getCCompanyDirectorsPassportNumber(){ return cCompanyDirectorsPassportNumber; }
    public String getCCompanyDirectorsNationality(){ return cCompanyDirectorsNationality; }
    public String getCCompanyDirectorsOccupation(){ return cCompanyDirectorsOccupation; }
    public String getABeneficialOwnersName(){ return aBeneficialOwnersName; }
    public String getABeneficialOwnersDateOfBirth(){ return aBeneficialOwnersDateOfBirth; }
    public String getABeneficialOwnersIdNumber(){ return aBeneficialOwnersIdNumber; }
    public String getABeneficialOwnersPassportNumber(){ return aBeneficialOwnersPassportNumber; }
    public String getABeneficialOwnersNationality(){ return aBeneficialOwnersNationality; }
    public String getABeneficialOwnersOccupation(){ return aBeneficialOwnersOccupation; }
    public String getABeneficialOwnersDateofappointment(){ return aBeneficialOwnersDateofappointment; }
    public String getABeneficialOwnersDateofresignation(){ return aBeneficialOwnersDateofresignation; }
    public String getBBeneficialOwnersName(){ return bBeneficialOwnersName; }
    public String getBBeneficialOwnersDateOfBirth(){ return bBeneficialOwnersDateOfBirth; }
    public String getBBeneficialOwnersIdNumber(){ return bBeneficialOwnersIdNumber; }
    public String getBBeneficialOwnersPassportNumber(){ return bBeneficialOwnersPassportNumber; }
    public String getBBeneficialOwnersNationality(){ return bBeneficialOwnersNationality; }
    public String getBBeneficialOwnersOccupation(){ return bBeneficialOwnersOccupation; }
    public String getBBeneficialOwnersDateofappointment(){ return bBeneficialOwnersDateofappointment; }
    public String getBBeneficialOwnersDateofresignation(){ return bBeneficialOwnersDateofresignation; }
    public String getInformationAssociatedPersonsCompaniesEtc(){ return informationAssociatedPersonsCompaniesEtc; }
    public String getWhyTheTxnWasReportedSuspicious(){ return whyTheTxnWasReportedSuspicious; }
    public String getDesignation(){ return designation; }
    public String getDate(){ return date; }

    public void setAccountId(String v){ if(v != null) this.accountId = v; }
    public void setNoTransactionToBeReported(String v){ if(v != null) this.noTransactionToBeReported = v; }
    public void setTransactionId(String v){ if(v != null) this.transactionId = v; }
    public void setRelatedAcct(String v){ if(v != null) this.relatedAcct = v; }
    public void setAdditionalRemarks(String v){ if(v != null) this.additionalRemarks = v; }
    public void setVolumeOfTransactions(String v){ if(v != null) this.volumeOfTransactions = v; }
    public void setBeneficiaryDetails(String v){ if(v != null) this.beneficiaryDetails = v; }
    public void setFactName(String v){ if(v != null) this.factName = v; }
    public void setRiskLevel(String v){ if(v != null) this.riskLevel = v; }
    public void setCaseEntityId(String v){ if(v != null) this.caseEntityId = v; }
    public void setEntityIdType(String v){ if(v != null) this.entityIdType = v; }
    public void setIncidentScore(String v){ if(v != null) this.incidentScore = v; }
    public void setEntityName(String v){ if(v != null) this.entityName = v; }
    public void setIncidentClosedDate(String v){ if(v != null) this.incidentClosedDate = v; }
    public void setEntityId(String v){ if(v != null) this.entityId = v; }
    public void setCaseEntityName(String v){ if(v != null) this.caseEntityName = v; }
    public void setCaseDisplayKey(String v){ if(v != null) this.caseDisplayKey = v; }
    public void setNoOfStr(Integer v){ if(v != null) this.noOfStr = v; }
    public void setLastFiledStr(Long v){ if(v != null) this.lastFiledStr = v; }
    public void setRelationshipBeginDate(String v){ if(v != null) this.relationshipBeginDate = v; }
    public void setCustIdNum(String v){ if(v != null) this.custIdNum = v; }
    public void setCustFax(String v){ if(v != null) this.custFax = v; }
    public void setProfessionType(String v){ if(v != null) this.professionType = v; }
    public void setCustType(String v){ if(v != null) this.custType = v; }
    public void setCustGender(String v){ if(v != null) this.custGender = v; }
    public void setCustMobile(String v){ if(v != null) this.custMobile = v; }
    public void setCustEmail(String v){ if(v != null) this.custEmail = v; }
    public void setCustMailingAddr(String v){ if(v != null) this.custMailingAddr = v; }
    public void setCustResAddr(String v){ if(v != null) this.custResAddr = v; }
    public void setCustResAddrCity(String v){ if(v != null) this.custResAddrCity = v; }
    public void setCustResAddrState(String v){ if(v != null) this.custResAddrState = v; }
    public void setCustResAddrCountry(String v){ if(v != null) this.custResAddrCountry = v; }
    public void setCustDOB(String v){ if(v != null) this.custDOB = v; }
    public void setCustName(String v){ if(v != null) this.custName = v; }
    public void setCreatedOn(String v){ if(v != null) this.createdOn = v; }
    public void setReportRelatedTo(String v){ if(v != null) this.reportRelatedTo = v; }
    public void setAccountType(String v){ if(v != null) this.accountType = v; }
    public void setPersonExecutingTxnAcctholderName(String v){ if(v != null) this.personExecutingTxnAcctholderName = v; }
    public void setPersonExecutingTxnAcctholderDateOfBirth(String v){ if(v != null) this.personExecutingTxnAcctholderDateOfBirth = v; }
    public void setPersonExecutingTxnAcctholderIdNumber(String v){ if(v != null) this.personExecutingTxnAcctholderIdNumber = v; }
    public void setPersonExecutingTxnAcctholderPassportNumber(String v){ if(v != null) this.personExecutingTxnAcctholderPassportNumber = v; }
    public void setPersonExecutingTxnAcctholderNationality(String v){ if(v != null) this.personExecutingTxnAcctholderNationality = v; }
    public void setBusinessentityPlaceOfIncorporation(String v){ if(v != null) this.businessentityPlaceOfIncorporation = v; }
    public void setBusinessentityDateOfIncorporation(String v){ if(v != null) this.businessentityDateOfIncorporation = v; }
    public void setBusinessentityBusinessActivity(String v){ if(v != null) this.businessentityBusinessActivity = v; }
    public void setACompanyDirectorsDateOfBirth(String v){ if(v != null) this.aCompanyDirectorsDateOfBirth = v; }
    public void setACompanyDirectorsIdNumber(String v){ if(v != null) this.aCompanyDirectorsIdNumber = v; }
    public void setACompanyDirectorsPassportNumber(String v){ if(v != null) this.aCompanyDirectorsPassportNumber = v; }
    public void setACompanyDirectorsNationality(String v){ if(v != null) this.aCompanyDirectorsNationality = v; }
    public void setACompanyDirectorsOccupation(String v){ if(v != null) this.aCompanyDirectorsOccupation = v; }
    public void setBCompanyDirectorsDateOfBirth(String v){ if(v != null) this.bCompanyDirectorsDateOfBirth = v; }
    public void setBCompanyDirectorsIdNumber(String v){ if(v != null) this.bCompanyDirectorsIdNumber = v; }
    public void setBCompanyDirectorsPassportNumber(String v){ if(v != null) this.bCompanyDirectorsPassportNumber = v; }
    public void setBCompanyDirectorsNationality(String v){ if(v != null) this.bCompanyDirectorsNationality = v; }
    public void setBCompanyDirectorsOccupation(String v){ if(v != null) this.bCompanyDirectorsOccupation = v; }
    public void setCCompanyDirectorsDateOfBirth(String v){ if(v != null) this.cCompanyDirectorsDateOfBirth = v; }
    public void setCCompanyDirectorsIdNumber(String v){ if(v != null) this.cCompanyDirectorsIdNumber = v; }
    public void setCCompanyDirectorsPassportNumber(String v){ if(v != null) this.cCompanyDirectorsPassportNumber = v; }
    public void setCCompanyDirectorsNationality(String v){ if(v != null) this.cCompanyDirectorsNationality = v; }
    public void setCCompanyDirectorsOccupation(String v){ if(v != null) this.cCompanyDirectorsOccupation = v; }
    public void setABeneficialOwnersName(String v){ if(v != null) this.aBeneficialOwnersName = v; }
    public void setABeneficialOwnersDateOfBirth(String v){ if(v != null) this.aBeneficialOwnersDateOfBirth = v; }
    public void setABeneficialOwnersIdNumber(String v){ if(v != null) this.aBeneficialOwnersIdNumber = v; }
    public void setABeneficialOwnersPassportNumber(String v){ if(v != null) this.aBeneficialOwnersPassportNumber = v; }
    public void setABeneficialOwnersNationality(String v){ if(v != null) this.aBeneficialOwnersNationality = v; }
    public void setABeneficialOwnersOccupation(String v){ if(v != null) this.aBeneficialOwnersOccupation = v; }
    public void setABeneficialOwnersDateofappointment(String v){ if(v != null) this.aBeneficialOwnersDateofappointment = v; }
    public void setABeneficialOwnersDateofresignation(String v){ if(v != null) this.aBeneficialOwnersDateofresignation = v; }
    public void setBBeneficialOwnersName(String v){ if(v != null) this.bBeneficialOwnersName = v; }
    public void setBBeneficialOwnersDateOfBirth(String v){ if(v != null) this.bBeneficialOwnersDateOfBirth = v; }
    public void setBBeneficialOwnersIdNumber(String v){ if(v != null) this.bBeneficialOwnersIdNumber = v; }
    public void setBBeneficialOwnersPassportNumber(String v){ if(v != null) this.bBeneficialOwnersPassportNumber = v; }
    public void setBBeneficialOwnersNationality(String v){ if(v != null) this.bBeneficialOwnersNationality = v; }
    public void setBBeneficialOwnersOccupation(String v){ if(v != null) this.bBeneficialOwnersOccupation = v; }
    public void setBBeneficialOwnersDateofappointment(String v){ if(v != null) this.bBeneficialOwnersDateofappointment = v; }
    public void setBBeneficialOwnersDateofresignation(String v){ if(v != null) this.bBeneficialOwnersDateofresignation = v; }
    public void setInformationAssociatedPersonsCompaniesEtc(String v){ if(v != null) this.informationAssociatedPersonsCompaniesEtc = v; }
    public void setWhyTheTxnWasReportedSuspicious(String v){ if(v != null) this.whyTheTxnWasReportedSuspicious = v; }
    public void setDesignation(String v){ if(v != null) this.designation = v; }
    public void setDate(String v){ if(v != null) this.date = v; }




    public Map<String,Object> getMap(){
        Map<String,Object> map = super.getMap();
        map.put("accountId", accountId);
        map.put("noTransactionToBeReported", noTransactionToBeReported);
        map.put("transactionId", transactionId);
        map.put("relatedAcct", relatedAcct);
        map.put("additionalRemarks", additionalRemarks);
        map.put("volumeOfTransactions", volumeOfTransactions);
        map.put("beneficiaryDetails", beneficiaryDetails);
        map.put("factName", factName);
        map.put("riskLevel", riskLevel);
        map.put("caseEntityId", caseEntityId);
        map.put("entityIdType", entityIdType);
        map.put("incidentScore", incidentScore);
        map.put("entityName", entityName);
        map.put("incidentClosedDate", incidentClosedDate);
        map.put("entityId", entityId);
        map.put("caseEntityName", caseEntityName);
        map.put("caseDisplayKey", caseDisplayKey);
        map.put("noOfStr", noOfStr);
        map.put("lastFiledStr", lastFiledStr);
        map.put("relationshipBeginDate", relationshipBeginDate);
        map.put("custIdNum", custIdNum);
        map.put("custFax", custFax);
        map.put("professionType", professionType);
        map.put("custType", custType);
        map.put("custGender", custGender);
        map.put("custMobile", custMobile);
        map.put("custEmail", custEmail);
        map.put("custMailingAddr", custMailingAddr);
        map.put("custResAddr", custResAddr);
        map.put("custResAddrCity", custResAddrCity);
        map.put("custResAddrState", custResAddrState);
        map.put("custResAddrCountry", custResAddrCountry);
        map.put("custDOB", custDOB);
        map.put("custName", custName);
        map.put("createdOn", createdOn);
        map.put("reportRelatedTo", reportRelatedTo);
        map.put("accountType", accountType);
        map.put("personExecutingTxnAcctholderName", personExecutingTxnAcctholderName);
        map.put("personExecutingTxnAcctholderDateOfBirth", personExecutingTxnAcctholderDateOfBirth);
        map.put("personExecutingTxnAcctholderIdNumber", personExecutingTxnAcctholderIdNumber);
        map.put("personExecutingTxnAcctholderPassportNumber", personExecutingTxnAcctholderPassportNumber);
        map.put("personExecutingTxnAcctholderNationality", personExecutingTxnAcctholderNationality);
        map.put("businessentityPlaceOfIncorporation", businessentityPlaceOfIncorporation);
        map.put("businessentityDateOfIncorporation", businessentityDateOfIncorporation);
        map.put("businessentityBusinessActivity", businessentityBusinessActivity);
        map.put("aCompanyDirectorsDateOfBirth", aCompanyDirectorsDateOfBirth);
        map.put("aCompanyDirectorsIdNumber", aCompanyDirectorsIdNumber);
        map.put("aCompanyDirectorsPassportNumber", aCompanyDirectorsPassportNumber);
        map.put("aCompanyDirectorsNationality", aCompanyDirectorsNationality);
        map.put("aCompanyDirectorsOccupation", aCompanyDirectorsOccupation);
        map.put("bCompanyDirectorsDateOfBirth", bCompanyDirectorsDateOfBirth);
        map.put("bCompanyDirectorsIdNumber", bCompanyDirectorsIdNumber);
        map.put("bCompanyDirectorsPassportNumber", bCompanyDirectorsPassportNumber);
        map.put("bCompanyDirectorsNationality", bCompanyDirectorsNationality);
        map.put("bCompanyDirectorsOccupation", bCompanyDirectorsOccupation);
        map.put("cCompanyDirectorsDateOfBirth", cCompanyDirectorsDateOfBirth);
        map.put("cCompanyDirectorsIdNumber", cCompanyDirectorsIdNumber);
        map.put("cCompanyDirectorsPassportNumber", cCompanyDirectorsPassportNumber);
        map.put("cCompanyDirectorsNationality", cCompanyDirectorsNationality);
        map.put("cCompanyDirectorsOccupation", cCompanyDirectorsOccupation);
        map.put("aBeneficialOwnersName", aBeneficialOwnersName);
        map.put("aBeneficialOwnersDateOfBirth", aBeneficialOwnersDateOfBirth);
        map.put("aBeneficialOwnersIdNumber", aBeneficialOwnersIdNumber);
        map.put("aBeneficialOwnersPassportNumber", aBeneficialOwnersPassportNumber);
        map.put("aBeneficialOwnersNationality", aBeneficialOwnersNationality);
        map.put("aBeneficialOwnersOccupation", aBeneficialOwnersOccupation);
        map.put("aBeneficialOwnersDateofappointment", aBeneficialOwnersDateofappointment);
        map.put("aBeneficialOwnersDateofresignation", aBeneficialOwnersDateofresignation);
        map.put("bBeneficialOwnersName", bBeneficialOwnersName);
        map.put("bBeneficialOwnersDateOfBirth", bBeneficialOwnersDateOfBirth);
        map.put("bBeneficialOwnersIdNumber", bBeneficialOwnersIdNumber);
        map.put("bBeneficialOwnersPassportNumber", bBeneficialOwnersPassportNumber);
        map.put("bBeneficialOwnersNationality", bBeneficialOwnersNationality);
        map.put("bBeneficialOwnersOccupation", bBeneficialOwnersOccupation);
        map.put("bBeneficialOwnersDateofappointment", bBeneficialOwnersDateofappointment);
        map.put("bBeneficialOwnersDateofresignation", bBeneficialOwnersDateofresignation);
        map.put("informationAssociatedPersonsCompaniesEtc", informationAssociatedPersonsCompaniesEtc);
        map.put("whyTheTxnWasReportedSuspicious", whyTheTxnWasReportedSuspicious);
        map.put("designation", designation);
        map.put("date", date);

        return map;
    }

    public void from(Map<String,Object> map) throws RdbmsException {
        super.from(map);
        Object obj;
        if((obj = map.get("accountId")) != null){
            if(obj instanceof String) setAccountId((String)obj);
        }
        if((obj = map.get("noTransactionToBeReported")) != null){
            if(obj instanceof String) setNoTransactionToBeReported((String)obj);
        }
        if((obj = map.get("transactionId")) != null){
            if(obj instanceof String) setTransactionId((String)obj);
        }
        if((obj = map.get("relatedAcct")) != null){
            if(obj instanceof String) setRelatedAcct((String)obj);
        }
        if((obj = map.get("additionalRemarks")) != null){
            if(obj instanceof String) setAdditionalRemarks((String)obj);
        }
        if((obj = map.get("volumeOfTransactions")) != null){
            if(obj instanceof String) setVolumeOfTransactions((String)obj);
        }
        if((obj = map.get("beneficiaryDetails")) != null){
            if(obj instanceof String) setBeneficiaryDetails((String)obj);
        }
        if((obj = map.get("factName")) != null){
            if(obj instanceof String) setFactName((String)obj);
        }
        if((obj = map.get("riskLevel")) != null){
            if(obj instanceof String) setRiskLevel((String)obj);
        }
        if((obj = map.get("caseEntityId")) != null){
            if(obj instanceof String) setCaseEntityId((String)obj);
        }
        if((obj = map.get("entityIdType")) != null){
            if(obj instanceof String) setEntityIdType((String)obj);
        }
        if((obj = map.get("incidentScore")) != null){
            if(obj instanceof String) setIncidentScore((String)obj);
        }
        if((obj = map.get("entityName")) != null){
            if(obj instanceof String) setEntityName((String)obj);
        }
        if((obj = map.get("incidentClosedDate")) != null){
            if(obj instanceof String) setIncidentClosedDate((String)obj);
        }
        if((obj = map.get("entityId")) != null){
            if(obj instanceof String) setEntityId((String)obj);
        }
        if((obj = map.get("caseEntityName")) != null){
            if(obj instanceof String) setCaseEntityName((String)obj);
        }
        if((obj = map.get("caseDisplayKey")) != null){
            if(obj instanceof String) setCaseDisplayKey((String)obj);
        }
        if((obj = map.get("noOfStr")) != null){
            if(obj instanceof Integer) setNoOfStr((Integer)obj);
            else if(obj instanceof String) setNoOfStr((String)obj);
        }
        if((obj = map.get("lastFiledStr")) != null){
            if(obj instanceof Long) setLastFiledStr((Long)obj);
            else if(obj instanceof String) setLastFiledStr((String)obj);
        }
        if((obj = map.get("relationshipBeginDate")) != null){
            if(obj instanceof String) setRelationshipBeginDate((String)obj);
        }
        if((obj = map.get("custIdNum")) != null){
            if(obj instanceof String) setCustIdNum((String)obj);
        }
        if((obj = map.get("custFax")) != null){
            if(obj instanceof String) setCustFax((String)obj);
        }
        if((obj = map.get("professionType")) != null){
            if(obj instanceof String) setProfessionType((String)obj);
        }
        if((obj = map.get("custType")) != null){
            if(obj instanceof String) setCustType((String)obj);
        }
        if((obj = map.get("custGender")) != null){
            if(obj instanceof String) setCustGender((String)obj);
        }
        if((obj = map.get("custMobile")) != null){
            if(obj instanceof String) setCustMobile((String)obj);
        }
        if((obj = map.get("custEmail")) != null){
            if(obj instanceof String) setCustEmail((String)obj);
        }
        if((obj = map.get("custMailingAddr")) != null){
            if(obj instanceof String) setCustMailingAddr((String)obj);
        }
        if((obj = map.get("custResAddr")) != null){
            if(obj instanceof String) setCustResAddr((String)obj);
        }
        if((obj = map.get("custResAddrCity")) != null){
            if(obj instanceof String) setCustResAddrCity((String)obj);
        }
        if((obj = map.get("custResAddrState")) != null){
            if(obj instanceof String) setCustResAddrState((String)obj);
        }
        if((obj = map.get("custResAddrCountry")) != null){
            if(obj instanceof String) setCustResAddrCountry((String)obj);
        }
        if((obj = map.get("custDOB")) != null){
            if(obj instanceof String) setCustDOB((String)obj);
        }
        if((obj = map.get("custName")) != null){
            if(obj instanceof String) setCustName((String)obj);
        }
        if((obj = map.get("createdOn")) != null){
            if(obj instanceof String) setCreatedOn((String)obj);
        }
        if((obj = map.get("reportRelatedTo")) != null){
            if(obj instanceof String) setReportRelatedTo((String)obj);
        }
        if((obj = map.get("accountType")) != null){
            if(obj instanceof String) setAccountType((String)obj);
        }
        if((obj = map.get("personExecutingTxnAcctholderName")) != null){
            if(obj instanceof String) setPersonExecutingTxnAcctholderName((String)obj);
        }
        if((obj = map.get("personExecutingTxnAcctholderDateOfBirth")) != null){
            if(obj instanceof String) setPersonExecutingTxnAcctholderDateOfBirth((String)obj);
        }
        if((obj = map.get("personExecutingTxnAcctholderIdNumber")) != null){
            if(obj instanceof String) setPersonExecutingTxnAcctholderIdNumber((String)obj);
        }
        if((obj = map.get("personExecutingTxnAcctholderPassportNumber")) != null){
            if(obj instanceof String) setPersonExecutingTxnAcctholderPassportNumber((String)obj);
        }
        if((obj = map.get("personExecutingTxnAcctholderNationality")) != null){
            if(obj instanceof String) setPersonExecutingTxnAcctholderNationality((String)obj);
        }
        if((obj = map.get("businessentityPlaceOfIncorporation")) != null){
            if(obj instanceof String) setBusinessentityPlaceOfIncorporation((String)obj);
        }
        if((obj = map.get("businessentityDateOfIncorporation")) != null){
            if(obj instanceof String) setBusinessentityDateOfIncorporation((String)obj);
        }
        if((obj = map.get("businessentityBusinessActivity")) != null){
            if(obj instanceof String) setBusinessentityBusinessActivity((String)obj);
        }
        if((obj = map.get("aCompanyDirectorsDateOfBirth")) != null){
            if(obj instanceof String) setACompanyDirectorsDateOfBirth((String)obj);
        }
        if((obj = map.get("aCompanyDirectorsIdNumber")) != null){
            if(obj instanceof String) setACompanyDirectorsIdNumber((String)obj);
        }
        if((obj = map.get("aCompanyDirectorsPassportNumber")) != null){
            if(obj instanceof String) setACompanyDirectorsPassportNumber((String)obj);
        }
        if((obj = map.get("aCompanyDirectorsNationality")) != null){
            if(obj instanceof String) setACompanyDirectorsNationality((String)obj);
        }
        if((obj = map.get("aCompanyDirectorsOccupation")) != null){
            if(obj instanceof String) setACompanyDirectorsOccupation((String)obj);
        }
        if((obj = map.get("bCompanyDirectorsDateOfBirth")) != null){
            if(obj instanceof String) setBCompanyDirectorsDateOfBirth((String)obj);
        }
        if((obj = map.get("bCompanyDirectorsIdNumber")) != null){
            if(obj instanceof String) setBCompanyDirectorsIdNumber((String)obj);
        }
        if((obj = map.get("bCompanyDirectorsPassportNumber")) != null){
            if(obj instanceof String) setBCompanyDirectorsPassportNumber((String)obj);
        }
        if((obj = map.get("bCompanyDirectorsNationality")) != null){
            if(obj instanceof String) setBCompanyDirectorsNationality((String)obj);
        }
        if((obj = map.get("bCompanyDirectorsOccupation")) != null){
            if(obj instanceof String) setBCompanyDirectorsOccupation((String)obj);
        }
        if((obj = map.get("cCompanyDirectorsDateOfBirth")) != null){
            if(obj instanceof String) setCCompanyDirectorsDateOfBirth((String)obj);
        }
        if((obj = map.get("cCompanyDirectorsIdNumber")) != null){
            if(obj instanceof String) setCCompanyDirectorsIdNumber((String)obj);
        }
        if((obj = map.get("cCompanyDirectorsPassportNumber")) != null){
            if(obj instanceof String) setCCompanyDirectorsPassportNumber((String)obj);
        }
        if((obj = map.get("cCompanyDirectorsNationality")) != null){
            if(obj instanceof String) setCCompanyDirectorsNationality((String)obj);
        }
        if((obj = map.get("cCompanyDirectorsOccupation")) != null){
            if(obj instanceof String) setCCompanyDirectorsOccupation((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersName")) != null){
            if(obj instanceof String) setABeneficialOwnersName((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersDateOfBirth")) != null){
            if(obj instanceof String) setABeneficialOwnersDateOfBirth((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersIdNumber")) != null){
            if(obj instanceof String) setABeneficialOwnersIdNumber((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersPassportNumber")) != null){
            if(obj instanceof String) setABeneficialOwnersPassportNumber((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersNationality")) != null){
            if(obj instanceof String) setABeneficialOwnersNationality((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersOccupation")) != null){
            if(obj instanceof String) setABeneficialOwnersOccupation((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersDateofappointment")) != null){
            if(obj instanceof String) setABeneficialOwnersDateofappointment((String)obj);
        }
        if((obj = map.get("aBeneficialOwnersDateofresignation")) != null){
            if(obj instanceof String) setABeneficialOwnersDateofresignation((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersName")) != null){
            if(obj instanceof String) setBBeneficialOwnersName((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersDateOfBirth")) != null){
            if(obj instanceof String) setBBeneficialOwnersDateOfBirth((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersIdNumber")) != null){
            if(obj instanceof String) setBBeneficialOwnersIdNumber((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersPassportNumber")) != null){
            if(obj instanceof String) setBBeneficialOwnersPassportNumber((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersNationality")) != null){
            if(obj instanceof String) setBBeneficialOwnersNationality((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersOccupation")) != null){
            if(obj instanceof String) setBBeneficialOwnersOccupation((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersDateofappointment")) != null){
            if(obj instanceof String) setBBeneficialOwnersDateofappointment((String)obj);
        }
        if((obj = map.get("bBeneficialOwnersDateofresignation")) != null){
            if(obj instanceof String) setBBeneficialOwnersDateofresignation((String)obj);
        }
        if((obj = map.get("informationAssociatedPersonsCompaniesEtc")) != null){
            if(obj instanceof String) setInformationAssociatedPersonsCompaniesEtc((String)obj);
        }
        if((obj = map.get("whyTheTxnWasReportedSuspicious")) != null){
            if(obj instanceof String) setWhyTheTxnWasReportedSuspicious((String)obj);
        }
        if((obj = map.get("designation")) != null){
            if(obj instanceof String) setDesignation((String)obj);
        }
        if((obj = map.get("date")) != null){
            if(obj instanceof String) setDate((String)obj);
        }

    }

	public void setNoOfStr(String noOfStr) throws NumberFormatException{ if(noOfStr != null) this.noOfStr = Integer.parseInt(noOfStr); }
	public void setLastFiledStr(String lastFiledStr) throws NumberFormatException{ if(lastFiledStr != null) this.lastFiledStr = Long.parseLong(lastFiledStr); }


	public String toString(){
		StringBuilder sb = new StringBuilder("{");
	    sb.append("\"className\":\"StrReport\",");
		if(getStrId() != null) sb.append("\"strId\":").append("\""+getStrId()+"\"").append(",");
		if(getAccountId() != null) sb.append("\"accountId\":").append("\""+getAccountId()+"\"").append(",");
		if(getNoTransactionToBeReported() != null) sb.append("\"noTransactionToBeReported\":").append("\""+getNoTransactionToBeReported()+"\"").append(",");
		if(getTransactionId() != null) sb.append("\"transactionId\":").append("\""+getTransactionId()+"\"").append(",");
		if(getRelatedAcct() != null) sb.append("\"relatedAcct\":").append("\""+getRelatedAcct()+"\"").append(",");
		if(getAdditionalRemarks() != null) sb.append("\"additionalRemarks\":").append("\""+getAdditionalRemarks()+"\"").append(",");
		if(getVolumeOfTransactions() != null) sb.append("\"volumeOfTransactions\":").append("\""+getVolumeOfTransactions()+"\"").append(",");
		if(getBeneficiaryDetails() != null) sb.append("\"beneficiaryDetails\":").append("\""+getBeneficiaryDetails()+"\"").append(",");
		if(getFactName() != null) sb.append("\"factName\":").append("\""+getFactName()+"\"").append(",");
		if(getRiskLevel() != null) sb.append("\"riskLevel\":").append("\""+getRiskLevel()+"\"").append(",");
		if(getCaseEntityId() != null) sb.append("\"caseEntityId\":").append("\""+getCaseEntityId()+"\"").append(",");
		if(getEntityIdType() != null) sb.append("\"entityIdType\":").append("\""+getEntityIdType()+"\"").append(",");
		if(getIncidentScore() != null) sb.append("\"incidentScore\":").append("\""+getIncidentScore()+"\"").append(",");
		if(getEntityName() != null) sb.append("\"entityName\":").append("\""+getEntityName()+"\"").append(",");
		if(getIncidentClosedDate() != null) sb.append("\"incidentClosedDate\":").append("\""+getIncidentClosedDate()+"\"").append(",");
		if(getEntityId() != null) sb.append("\"entityId\":").append("\""+getEntityId()+"\"").append(",");
		if(getCaseEntityName() != null) sb.append("\"caseEntityName\":").append("\""+getCaseEntityName()+"\"").append(",");
		if(getCaseDisplayKey() != null) sb.append("\"caseDisplayKey\":").append("\""+getCaseDisplayKey()+"\"").append(",");
		if(getNoOfStr() != null) sb.append("\"noOfStr\":").append("\""+getNoOfStr()+"\"").append(",");
		if(getLastFiledStr() != null) sb.append("\"lastFiledStr\":").append("\""+getLastFiledStr()+"\"").append(",");
		if(getRelationshipBeginDate() != null) sb.append("\"relationshipBeginDate\":").append("\""+getRelationshipBeginDate()+"\"").append(",");
		if(getCustIdNum() != null) sb.append("\"custIdNum\":").append("\""+getCustIdNum()+"\"").append(",");
		if(getCustFax() != null) sb.append("\"custFax\":").append("\""+getCustFax()+"\"").append(",");
		if(getProfessionType() != null) sb.append("\"professionType\":").append("\""+getProfessionType()+"\"").append(",");
		if(getCustType() != null) sb.append("\"custType\":").append("\""+getCustType()+"\"").append(",");
		if(getCustGender() != null) sb.append("\"custGender\":").append("\""+getCustGender()+"\"").append(",");
		if(getCustMobile() != null) sb.append("\"custMobile\":").append("\""+getCustMobile()+"\"").append(",");
		if(getCustEmail() != null) sb.append("\"custEmail\":").append("\""+getCustEmail()+"\"").append(",");
		if(getCustMailingAddr() != null) sb.append("\"custMailingAddr\":").append("\""+getCustMailingAddr()+"\"").append(",");
		if(getCustResAddr() != null) sb.append("\"custResAddr\":").append("\""+getCustResAddr()+"\"").append(",");
		if(getCustResAddrCity() != null) sb.append("\"custResAddrCity\":").append("\""+getCustResAddrCity()+"\"").append(",");
		if(getCustResAddrState() != null) sb.append("\"custResAddrState\":").append("\""+getCustResAddrState()+"\"").append(",");
		if(getCustResAddrCountry() != null) sb.append("\"custResAddrCountry\":").append("\""+getCustResAddrCountry()+"\"").append(",");
		if(getCustDOB() != null) sb.append("\"custDOB\":").append("\""+getCustDOB()+"\"").append(",");
		if(getCustName() != null) sb.append("\"custName\":").append("\""+getCustName()+"\"").append(",");
		if(getCreatedOn() != null) sb.append("\"createdOn\":").append("\""+getCreatedOn()+"\"").append(",");
		if(getReportRelatedTo() != null) sb.append("\"reportRelatedTo\":").append("\""+getReportRelatedTo()+"\"").append(",");
		if(getAccountType() != null) sb.append("\"accountType\":").append("\""+getAccountType()+"\"").append(",");
		if(getPersonExecutingTxnAcctholderName() != null) sb.append("\"personExecutingTxnAcctholderName\":").append("\""+getPersonExecutingTxnAcctholderName()+"\"").append(",");
		if(getPersonExecutingTxnAcctholderDateOfBirth() != null) sb.append("\"personExecutingTxnAcctholderDateOfBirth\":").append("\""+getPersonExecutingTxnAcctholderDateOfBirth()+"\"").append(",");
		if(getPersonExecutingTxnAcctholderIdNumber() != null) sb.append("\"personExecutingTxnAcctholderIdNumber\":").append("\""+getPersonExecutingTxnAcctholderIdNumber()+"\"").append(",");
		if(getPersonExecutingTxnAcctholderPassportNumber() != null) sb.append("\"personExecutingTxnAcctholderPassportNumber\":").append("\""+getPersonExecutingTxnAcctholderPassportNumber()+"\"").append(",");
		if(getPersonExecutingTxnAcctholderNationality() != null) sb.append("\"personExecutingTxnAcctholderNationality\":").append("\""+getPersonExecutingTxnAcctholderNationality()+"\"").append(",");
		if(getBusinessentityPlaceOfIncorporation() != null) sb.append("\"businessentityPlaceOfIncorporation\":").append("\""+getBusinessentityPlaceOfIncorporation()+"\"").append(",");
		if(getBusinessentityDateOfIncorporation() != null) sb.append("\"businessentityDateOfIncorporation\":").append("\""+getBusinessentityDateOfIncorporation()+"\"").append(",");
		if(getBusinessentityBusinessActivity() != null) sb.append("\"businessentityBusinessActivity\":").append("\""+getBusinessentityBusinessActivity()+"\"").append(",");
		if(getACompanyDirectorsDateOfBirth() != null) sb.append("\"aCompanyDirectorsDateOfBirth\":").append("\""+getACompanyDirectorsDateOfBirth()+"\"").append(",");
		if(getACompanyDirectorsIdNumber() != null) sb.append("\"aCompanyDirectorsIdNumber\":").append("\""+getACompanyDirectorsIdNumber()+"\"").append(",");
		if(getACompanyDirectorsPassportNumber() != null) sb.append("\"aCompanyDirectorsPassportNumber\":").append("\""+getACompanyDirectorsPassportNumber()+"\"").append(",");
		if(getACompanyDirectorsNationality() != null) sb.append("\"aCompanyDirectorsNationality\":").append("\""+getACompanyDirectorsNationality()+"\"").append(",");
		if(getACompanyDirectorsOccupation() != null) sb.append("\"aCompanyDirectorsOccupation\":").append("\""+getACompanyDirectorsOccupation()+"\"").append(",");
		if(getBCompanyDirectorsDateOfBirth() != null) sb.append("\"bCompanyDirectorsDateOfBirth\":").append("\""+getBCompanyDirectorsDateOfBirth()+"\"").append(",");
		if(getBCompanyDirectorsIdNumber() != null) sb.append("\"bCompanyDirectorsIdNumber\":").append("\""+getBCompanyDirectorsIdNumber()+"\"").append(",");
		if(getBCompanyDirectorsPassportNumber() != null) sb.append("\"bCompanyDirectorsPassportNumber\":").append("\""+getBCompanyDirectorsPassportNumber()+"\"").append(",");
		if(getBCompanyDirectorsNationality() != null) sb.append("\"bCompanyDirectorsNationality\":").append("\""+getBCompanyDirectorsNationality()+"\"").append(",");
		if(getBCompanyDirectorsOccupation() != null) sb.append("\"bCompanyDirectorsOccupation\":").append("\""+getBCompanyDirectorsOccupation()+"\"").append(",");
		if(getCCompanyDirectorsDateOfBirth() != null) sb.append("\"cCompanyDirectorsDateOfBirth\":").append("\""+getCCompanyDirectorsDateOfBirth()+"\"").append(",");
		if(getCCompanyDirectorsIdNumber() != null) sb.append("\"cCompanyDirectorsIdNumber\":").append("\""+getCCompanyDirectorsIdNumber()+"\"").append(",");
		if(getCCompanyDirectorsPassportNumber() != null) sb.append("\"cCompanyDirectorsPassportNumber\":").append("\""+getCCompanyDirectorsPassportNumber()+"\"").append(",");
		if(getCCompanyDirectorsNationality() != null) sb.append("\"cCompanyDirectorsNationality\":").append("\""+getCCompanyDirectorsNationality()+"\"").append(",");
		if(getCCompanyDirectorsOccupation() != null) sb.append("\"cCompanyDirectorsOccupation\":").append("\""+getCCompanyDirectorsOccupation()+"\"").append(",");
		if(getABeneficialOwnersName() != null) sb.append("\"aBeneficialOwnersName\":").append("\""+getABeneficialOwnersName()+"\"").append(",");
		if(getABeneficialOwnersDateOfBirth() != null) sb.append("\"aBeneficialOwnersDateOfBirth\":").append("\""+getABeneficialOwnersDateOfBirth()+"\"").append(",");
		if(getABeneficialOwnersIdNumber() != null) sb.append("\"aBeneficialOwnersIdNumber\":").append("\""+getABeneficialOwnersIdNumber()+"\"").append(",");
		if(getABeneficialOwnersPassportNumber() != null) sb.append("\"aBeneficialOwnersPassportNumber\":").append("\""+getABeneficialOwnersPassportNumber()+"\"").append(",");
		if(getABeneficialOwnersNationality() != null) sb.append("\"aBeneficialOwnersNationality\":").append("\""+getABeneficialOwnersNationality()+"\"").append(",");
		if(getABeneficialOwnersOccupation() != null) sb.append("\"aBeneficialOwnersOccupation\":").append("\""+getABeneficialOwnersOccupation()+"\"").append(",");
		if(getABeneficialOwnersDateofappointment() != null) sb.append("\"aBeneficialOwnersDateofappointment\":").append("\""+getABeneficialOwnersDateofappointment()+"\"").append(",");
		if(getABeneficialOwnersDateofresignation() != null) sb.append("\"aBeneficialOwnersDateofresignation\":").append("\""+getABeneficialOwnersDateofresignation()+"\"").append(",");
		if(getBBeneficialOwnersName() != null) sb.append("\"bBeneficialOwnersName\":").append("\""+getBBeneficialOwnersName()+"\"").append(",");
		if(getBBeneficialOwnersDateOfBirth() != null) sb.append("\"bBeneficialOwnersDateOfBirth\":").append("\""+getBBeneficialOwnersDateOfBirth()+"\"").append(",");
		if(getBBeneficialOwnersIdNumber() != null) sb.append("\"bBeneficialOwnersIdNumber\":").append("\""+getBBeneficialOwnersIdNumber()+"\"").append(",");
		if(getBBeneficialOwnersPassportNumber() != null) sb.append("\"bBeneficialOwnersPassportNumber\":").append("\""+getBBeneficialOwnersPassportNumber()+"\"").append(",");
		if(getBBeneficialOwnersNationality() != null) sb.append("\"bBeneficialOwnersNationality\":").append("\""+getBBeneficialOwnersNationality()+"\"").append(",");
		if(getBBeneficialOwnersOccupation() != null) sb.append("\"bBeneficialOwnersOccupation\":").append("\""+getBBeneficialOwnersOccupation()+"\"").append(",");
		if(getBBeneficialOwnersDateofappointment() != null) sb.append("\"bBeneficialOwnersDateofappointment\":").append("\""+getBBeneficialOwnersDateofappointment()+"\"").append(",");
		if(getBBeneficialOwnersDateofresignation() != null) sb.append("\"bBeneficialOwnersDateofresignation\":").append("\""+getBBeneficialOwnersDateofresignation()+"\"").append(",");
		if(getInformationAssociatedPersonsCompaniesEtc() != null) sb.append("\"informationAssociatedPersonsCompaniesEtc\":").append("\""+getInformationAssociatedPersonsCompaniesEtc()+"\"").append(",");
		if(getWhyTheTxnWasReportedSuspicious() != null) sb.append("\"whyTheTxnWasReportedSuspicious\":").append("\""+getWhyTheTxnWasReportedSuspicious()+"\"").append(",");
		if(getDesignation() != null) sb.append("\"designation\":").append("\""+getDesignation()+"\"").append(",");
		if(getDate() != null) sb.append("\"date\":").append("\""+getDate()+"\"").append(",");

	    sb.append("}");
    	return sb.toString().replaceAll(Pattern.quote(",}"), "}").replaceAll(Pattern.quote(",]"), "]");
    }

    public String toXml(){
        StringBuilder sb = new StringBuilder();
        sb.append("<record name='").append("str-report").append("'>\n");
        sb.append("\t<attributes>\n");
        sb.append("\t\t<attribute name='").append("str-id").append("'>").append(getStrId()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("account-id").append("'>").append(getAccountId()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("no-transaction-to-be-reported").append("'>").append(getNoTransactionToBeReported()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("transaction-id").append("'>").append(getTransactionId()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("related-acct").append("'>").append(getRelatedAcct()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("additional-remarks").append("'>").append(getAdditionalRemarks()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("volume-of-transactions").append("'>").append(getVolumeOfTransactions()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("beneficiary-details").append("'>").append(getBeneficiaryDetails()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("fact-name").append("'>").append(getFactName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("risk-level").append("'>").append(getRiskLevel()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("case-entity-id").append("'>").append(getCaseEntityId()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("entity-id-type").append("'>").append(getEntityIdType()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("incident-score").append("'>").append(getIncidentScore()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("entity-name").append("'>").append(getEntityName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("incident-closed-date").append("'>").append(getIncidentClosedDate()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("entity-id").append("'>").append(getEntityId()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("case-entity-name").append("'>").append(getCaseEntityName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("case-display-key").append("'>").append(getCaseDisplayKey()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("no-of-str").append("'>").append(getNoOfStr()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("last-filed-str").append("'>").append(getLastFiledStr()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("relationship-begin-date").append("'>").append(getRelationshipBeginDate()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-id-num").append("'>").append(getCustIdNum()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-fax").append("'>").append(getCustFax()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("profession-type").append("'>").append(getProfessionType()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-type").append("'>").append(getCustType()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-gender").append("'>").append(getCustGender()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-mobile").append("'>").append(getCustMobile()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-email").append("'>").append(getCustEmail()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-mailing-addr").append("'>").append(getCustMailingAddr()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-res-addr").append("'>").append(getCustResAddr()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-res-addr-city").append("'>").append(getCustResAddrCity()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-res-addr-state").append("'>").append(getCustResAddrState()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-res-addr-country").append("'>").append(getCustResAddrCountry()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-d-o-b").append("'>").append(getCustDOB()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("cust-name").append("'>").append(getCustName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("createdOn").append("'>").append(getCreatedOn()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("report-related-to").append("'>").append(getReportRelatedTo()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("account-type").append("'>").append(getAccountType()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("person-executing-txn-acctholder-name").append("'>").append(getPersonExecutingTxnAcctholderName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("person-executing-txn-acctholder-date-of-birth").append("'>").append(getPersonExecutingTxnAcctholderDateOfBirth()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("person-executing-txn-acctholder-id-number").append("'>").append(getPersonExecutingTxnAcctholderIdNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("person-executing-txn-acctholder-passport-number").append("'>").append(getPersonExecutingTxnAcctholderPassportNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("person-executing-txn-acctholder-nationality").append("'>").append(getPersonExecutingTxnAcctholderNationality()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("businessentity-place-of-incorporation").append("'>").append(getBusinessentityPlaceOfIncorporation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("businessentity-date-of-incorporation").append("'>").append(getBusinessentityDateOfIncorporation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("businessentity-business-activity").append("'>").append(getBusinessentityBusinessActivity()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-company-directors-date-of-birth").append("'>").append(getACompanyDirectorsDateOfBirth()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-company-directors-id-number").append("'>").append(getACompanyDirectorsIdNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-company-directors-passport-number").append("'>").append(getACompanyDirectorsPassportNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-company-directors-nationality").append("'>").append(getACompanyDirectorsNationality()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-company-directors-occupation").append("'>").append(getACompanyDirectorsOccupation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-company-directors-date-of-birth").append("'>").append(getBCompanyDirectorsDateOfBirth()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-company-directors-id-number").append("'>").append(getBCompanyDirectorsIdNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-company-directors-passport-number").append("'>").append(getBCompanyDirectorsPassportNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-company-directors-nationality").append("'>").append(getBCompanyDirectorsNationality()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-company-directors-occupation").append("'>").append(getBCompanyDirectorsOccupation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("c-company-directors-date-of-birth").append("'>").append(getCCompanyDirectorsDateOfBirth()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("c-company-directors-id-number").append("'>").append(getCCompanyDirectorsIdNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("c-company-directors-passport-number").append("'>").append(getCCompanyDirectorsPassportNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("c-company-directors-nationality").append("'>").append(getCCompanyDirectorsNationality()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("c-company-directors-occupation").append("'>").append(getCCompanyDirectorsOccupation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-name").append("'>").append(getABeneficialOwnersName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-date-of-birth").append("'>").append(getABeneficialOwnersDateOfBirth()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-id-number").append("'>").append(getABeneficialOwnersIdNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-passport-number").append("'>").append(getABeneficialOwnersPassportNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-nationality").append("'>").append(getABeneficialOwnersNationality()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-occupation").append("'>").append(getABeneficialOwnersOccupation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-dateofappointment").append("'>").append(getABeneficialOwnersDateofappointment()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("a-beneficial-owners-dateofresignation").append("'>").append(getABeneficialOwnersDateofresignation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-name").append("'>").append(getBBeneficialOwnersName()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-date-of-birth").append("'>").append(getBBeneficialOwnersDateOfBirth()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-id-number").append("'>").append(getBBeneficialOwnersIdNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-passport-number").append("'>").append(getBBeneficialOwnersPassportNumber()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-nationality").append("'>").append(getBBeneficialOwnersNationality()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-occupation").append("'>").append(getBBeneficialOwnersOccupation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-dateofappointment").append("'>").append(getBBeneficialOwnersDateofappointment()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("b-beneficial-owners-dateofresignation").append("'>").append(getBBeneficialOwnersDateofresignation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("information-associated-persons-companies-etc").append("'>").append(getInformationAssociatedPersonsCompaniesEtc()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("why-the-txn-was-reported-suspicious").append("'>").append(getWhyTheTxnWasReportedSuspicious()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("designation").append("'>").append(getDesignation()).append("</attribute>\n");
        sb.append("\t\t<attribute name='").append("date").append("'>").append(getDate()).append("</attribute>\n");

        sb.append("\t</attributes>\n");
        sb.append("</record>");
        return sb.toString().replaceAll(Pattern.quote(",<"), "<");
    }

    public Hocon toHocon() throws Exception{
        return new Hocon(toString());
    }

    public void from(String jsonStr) throws Exception {
        jsonStr = jsonStr.trim();
        if(jsonStr.charAt(0) == '{'){
            jsonStr = jsonStr.substring(1,jsonStr.length()-1);
        }
        Hocon h = new Hocon(jsonStr);
        from(h);
    }

    public void from(Hocon h){
        Hocon t = h.get("StrReport");
        if(t != null) h = t;
        String str;
		StringBuilder sb = new StringBuilder("Illegal Arguments: ");
		boolean error = false;
        try { if((str = h.getString("strId", h.getString("str_id", null))) != null) setStrId(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" strId"); }
        try { if((str = h.getString("accountId", h.getString("account_id", null))) != null) setAccountId(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" accountId"); }
        try { if((str = h.getString("noTransactionToBeReported", h.getString("no_transaction_to_be_reported", null))) != null) setNoTransactionToBeReported(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" noTransactionToBeReported"); }
        try { if((str = h.getString("transactionId", h.getString("transaction_id", null))) != null) setTransactionId(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" transactionId"); }
        try { if((str = h.getString("relatedAcct", h.getString("related_acct", null))) != null) setRelatedAcct(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" relatedAcct"); }
        try { if((str = h.getString("additionalRemarks", h.getString("additional_remarks", null))) != null) setAdditionalRemarks(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" additionalRemarks"); }
        try { if((str = h.getString("volumeOfTransactions", h.getString("volume_of_transactions", null))) != null) setVolumeOfTransactions(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" volumeOfTransactions"); }
        try { if((str = h.getString("beneficiaryDetails", h.getString("beneficiary_details", null))) != null) setBeneficiaryDetails(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" beneficiaryDetails"); }
        try { if((str = h.getString("factName", h.getString("fact_name", null))) != null) setFactName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" factName"); }
        try { if((str = h.getString("riskLevel", h.getString("risk_level", null))) != null) setRiskLevel(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" riskLevel"); }
        try { if((str = h.getString("caseEntityId", h.getString("case_entity_id", null))) != null) setCaseEntityId(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" caseEntityId"); }
        try { if((str = h.getString("entityIdType", h.getString("entity_id_type", null))) != null) setEntityIdType(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" entityIdType"); }
        try { if((str = h.getString("incidentScore", h.getString("incident_score", null))) != null) setIncidentScore(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" incidentScore"); }
        try { if((str = h.getString("entityName", h.getString("entity_name", null))) != null) setEntityName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" entityName"); }
        try { if((str = h.getString("incidentClosedDate", h.getString("incident_closed_date", null))) != null) setIncidentClosedDate(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" incidentClosedDate"); }
        try { if((str = h.getString("entityId", h.getString("entity_id", null))) != null) setEntityId(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" entityId"); }
        try { if((str = h.getString("caseEntityName", h.getString("case_entity_name", null))) != null) setCaseEntityName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" caseEntityName"); }
        try { if((str = h.getString("caseDisplayKey", h.getString("case_display_key", null))) != null) setCaseDisplayKey(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" caseDisplayKey"); }
        try { if((str = h.getString("noOfStr", h.getString("no_of_str", null))) != null) setNoOfStr(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" noOfStr"); }
        try { if((str = h.getString("lastFiledStr", h.getString("last_filed_str", null))) != null) setLastFiledStr(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" lastFiledStr"); }
        try { if((str = h.getString("relationshipBeginDate", h.getString("relationship_begin_date", null))) != null) setRelationshipBeginDate(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" relationshipBeginDate"); }
        try { if((str = h.getString("custIdNum", h.getString("cust_id_num", null))) != null) setCustIdNum(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custIdNum"); }
        try { if((str = h.getString("custFax", h.getString("cust_fax", null))) != null) setCustFax(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custFax"); }
        try { if((str = h.getString("professionType", h.getString("profession_type", null))) != null) setProfessionType(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" professionType"); }
        try { if((str = h.getString("custType", h.getString("cust_type", null))) != null) setCustType(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custType"); }
        try { if((str = h.getString("custGender", h.getString("cust_gender", null))) != null) setCustGender(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custGender"); }
        try { if((str = h.getString("custMobile", h.getString("cust_mobile", null))) != null) setCustMobile(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custMobile"); }
        try { if((str = h.getString("custEmail", h.getString("cust_email", null))) != null) setCustEmail(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custEmail"); }
        try { if((str = h.getString("custMailingAddr", h.getString("cust_mailing_addr", null))) != null) setCustMailingAddr(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custMailingAddr"); }
        try { if((str = h.getString("custResAddr", h.getString("cust_res_addr", null))) != null) setCustResAddr(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custResAddr"); }
        try { if((str = h.getString("custResAddrCity", h.getString("cust_res_addr_city", null))) != null) setCustResAddrCity(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custResAddrCity"); }
        try { if((str = h.getString("custResAddrState", h.getString("cust_res_addr_state", null))) != null) setCustResAddrState(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custResAddrState"); }
        try { if((str = h.getString("custResAddrCountry", h.getString("cust_res_addr_country", null))) != null) setCustResAddrCountry(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custResAddrCountry"); }
        try { if((str = h.getString("custDOB", h.getString("cust_d_o_b", null))) != null) setCustDOB(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custDOB"); }
        try { if((str = h.getString("custName", h.getString("cust_name", null))) != null) setCustName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" custName"); }
        try { if((str = h.getString("createdOn", h.getString("createdOn", null))) != null) setCreatedOn(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" createdOn"); }
        try { if((str = h.getString("reportRelatedTo", h.getString("report_related_to", null))) != null) setReportRelatedTo(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" reportRelatedTo"); }
        try { if((str = h.getString("accountType", h.getString("account_type", null))) != null) setAccountType(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" accountType"); }
        try { if((str = h.getString("personExecutingTxnAcctholderName", h.getString("person_executing_txn_acctholder_name", null))) != null) setPersonExecutingTxnAcctholderName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" personExecutingTxnAcctholderName"); }
        try { if((str = h.getString("personExecutingTxnAcctholderDateOfBirth", h.getString("person_executing_txn_acctholder_date_of_birth", null))) != null) setPersonExecutingTxnAcctholderDateOfBirth(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" personExecutingTxnAcctholderDateOfBirth"); }
        try { if((str = h.getString("personExecutingTxnAcctholderIdNumber", h.getString("person_executing_txn_acctholder_id_number", null))) != null) setPersonExecutingTxnAcctholderIdNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" personExecutingTxnAcctholderIdNumber"); }
        try { if((str = h.getString("personExecutingTxnAcctholderPassportNumber", h.getString("person_executing_txn_acctholder_passport_number", null))) != null) setPersonExecutingTxnAcctholderPassportNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" personExecutingTxnAcctholderPassportNumber"); }
        try { if((str = h.getString("personExecutingTxnAcctholderNationality", h.getString("person_executing_txn_acctholder_nationality", null))) != null) setPersonExecutingTxnAcctholderNationality(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" personExecutingTxnAcctholderNationality"); }
        try { if((str = h.getString("businessentityPlaceOfIncorporation", h.getString("businessentity_place_of_incorporation", null))) != null) setBusinessentityPlaceOfIncorporation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" businessentityPlaceOfIncorporation"); }
        try { if((str = h.getString("businessentityDateOfIncorporation", h.getString("businessentity_date_of_incorporation", null))) != null) setBusinessentityDateOfIncorporation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" businessentityDateOfIncorporation"); }
        try { if((str = h.getString("businessentityBusinessActivity", h.getString("businessentity_business_activity", null))) != null) setBusinessentityBusinessActivity(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" businessentityBusinessActivity"); }
        try { if((str = h.getString("aCompanyDirectorsDateOfBirth", h.getString("a_company_directors_date_of_birth", null))) != null) setACompanyDirectorsDateOfBirth(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aCompanyDirectorsDateOfBirth"); }
        try { if((str = h.getString("aCompanyDirectorsIdNumber", h.getString("a_company_directors_id_number", null))) != null) setACompanyDirectorsIdNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aCompanyDirectorsIdNumber"); }
        try { if((str = h.getString("aCompanyDirectorsPassportNumber", h.getString("a_company_directors_passport_number", null))) != null) setACompanyDirectorsPassportNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aCompanyDirectorsPassportNumber"); }
        try { if((str = h.getString("aCompanyDirectorsNationality", h.getString("a_company_directors_nationality", null))) != null) setACompanyDirectorsNationality(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aCompanyDirectorsNationality"); }
        try { if((str = h.getString("aCompanyDirectorsOccupation", h.getString("a_company_directors_occupation", null))) != null) setACompanyDirectorsOccupation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aCompanyDirectorsOccupation"); }
        try { if((str = h.getString("bCompanyDirectorsDateOfBirth", h.getString("b_company_directors_date_of_birth", null))) != null) setBCompanyDirectorsDateOfBirth(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bCompanyDirectorsDateOfBirth"); }
        try { if((str = h.getString("bCompanyDirectorsIdNumber", h.getString("b_company_directors_id_number", null))) != null) setBCompanyDirectorsIdNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bCompanyDirectorsIdNumber"); }
        try { if((str = h.getString("bCompanyDirectorsPassportNumber", h.getString("b_company_directors_passport_number", null))) != null) setBCompanyDirectorsPassportNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bCompanyDirectorsPassportNumber"); }
        try { if((str = h.getString("bCompanyDirectorsNationality", h.getString("b_company_directors_nationality", null))) != null) setBCompanyDirectorsNationality(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bCompanyDirectorsNationality"); }
        try { if((str = h.getString("bCompanyDirectorsOccupation", h.getString("b_company_directors_occupation", null))) != null) setBCompanyDirectorsOccupation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bCompanyDirectorsOccupation"); }
        try { if((str = h.getString("cCompanyDirectorsDateOfBirth", h.getString("c_company_directors_date_of_birth", null))) != null) setCCompanyDirectorsDateOfBirth(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" cCompanyDirectorsDateOfBirth"); }
        try { if((str = h.getString("cCompanyDirectorsIdNumber", h.getString("c_company_directors_id_number", null))) != null) setCCompanyDirectorsIdNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" cCompanyDirectorsIdNumber"); }
        try { if((str = h.getString("cCompanyDirectorsPassportNumber", h.getString("c_company_directors_passport_number", null))) != null) setCCompanyDirectorsPassportNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" cCompanyDirectorsPassportNumber"); }
        try { if((str = h.getString("cCompanyDirectorsNationality", h.getString("c_company_directors_nationality", null))) != null) setCCompanyDirectorsNationality(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" cCompanyDirectorsNationality"); }
        try { if((str = h.getString("cCompanyDirectorsOccupation", h.getString("c_company_directors_occupation", null))) != null) setCCompanyDirectorsOccupation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" cCompanyDirectorsOccupation"); }
        try { if((str = h.getString("aBeneficialOwnersName", h.getString("a_beneficial_owners_name", null))) != null) setABeneficialOwnersName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersName"); }
        try { if((str = h.getString("aBeneficialOwnersDateOfBirth", h.getString("a_beneficial_owners_date_of_birth", null))) != null) setABeneficialOwnersDateOfBirth(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersDateOfBirth"); }
        try { if((str = h.getString("aBeneficialOwnersIdNumber", h.getString("a_beneficial_owners_id_number", null))) != null) setABeneficialOwnersIdNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersIdNumber"); }
        try { if((str = h.getString("aBeneficialOwnersPassportNumber", h.getString("a_beneficial_owners_passport_number", null))) != null) setABeneficialOwnersPassportNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersPassportNumber"); }
        try { if((str = h.getString("aBeneficialOwnersNationality", h.getString("a_beneficial_owners_nationality", null))) != null) setABeneficialOwnersNationality(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersNationality"); }
        try { if((str = h.getString("aBeneficialOwnersOccupation", h.getString("a_beneficial_owners_occupation", null))) != null) setABeneficialOwnersOccupation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersOccupation"); }
        try { if((str = h.getString("aBeneficialOwnersDateofappointment", h.getString("a_beneficial_owners_dateofappointment", null))) != null) setABeneficialOwnersDateofappointment(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersDateofappointment"); }
        try { if((str = h.getString("aBeneficialOwnersDateofresignation", h.getString("a_beneficial_owners_dateofresignation", null))) != null) setABeneficialOwnersDateofresignation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" aBeneficialOwnersDateofresignation"); }
        try { if((str = h.getString("bBeneficialOwnersName", h.getString("b_beneficial_owners_name", null))) != null) setBBeneficialOwnersName(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersName"); }
        try { if((str = h.getString("bBeneficialOwnersDateOfBirth", h.getString("b_beneficial_owners_date_of_birth", null))) != null) setBBeneficialOwnersDateOfBirth(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersDateOfBirth"); }
        try { if((str = h.getString("bBeneficialOwnersIdNumber", h.getString("b_beneficial_owners_id_number", null))) != null) setBBeneficialOwnersIdNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersIdNumber"); }
        try { if((str = h.getString("bBeneficialOwnersPassportNumber", h.getString("b_beneficial_owners_passport_number", null))) != null) setBBeneficialOwnersPassportNumber(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersPassportNumber"); }
        try { if((str = h.getString("bBeneficialOwnersNationality", h.getString("b_beneficial_owners_nationality", null))) != null) setBBeneficialOwnersNationality(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersNationality"); }
        try { if((str = h.getString("bBeneficialOwnersOccupation", h.getString("b_beneficial_owners_occupation", null))) != null) setBBeneficialOwnersOccupation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersOccupation"); }
        try { if((str = h.getString("bBeneficialOwnersDateofappointment", h.getString("b_beneficial_owners_dateofappointment", null))) != null) setBBeneficialOwnersDateofappointment(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersDateofappointment"); }
        try { if((str = h.getString("bBeneficialOwnersDateofresignation", h.getString("b_beneficial_owners_dateofresignation", null))) != null) setBBeneficialOwnersDateofresignation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" bBeneficialOwnersDateofresignation"); }
        try { if((str = h.getString("informationAssociatedPersonsCompaniesEtc", h.getString("information_associated_persons_companies_etc", null))) != null) setInformationAssociatedPersonsCompaniesEtc(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" informationAssociatedPersonsCompaniesEtc"); }
        try { if((str = h.getString("whyTheTxnWasReportedSuspicious", h.getString("why_the_txn_was_reported_suspicious", null))) != null) setWhyTheTxnWasReportedSuspicious(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" whyTheTxnWasReportedSuspicious"); }
        try { if((str = h.getString("designation", h.getString("designation", null))) != null) setDesignation(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" designation"); }
        try { if((str = h.getString("date", h.getString("date", null))) != null) setDate(str); }
        catch(IllegalArgumentException ex){ error = true; sb.append(" date"); }

        if(error) throw new IllegalArgumentException(sb.toString());
	}

	public String toJson(){
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"strId\": ");
		sb.append((getStrId() == null) ? "\"\"," : "\""+getStrId()+"\",");
		sb.append("\"accountId\": ");
		sb.append((getAccountId() == null) ? "\"\"," : "\""+getAccountId()+"\",");
		sb.append("\"noTransactionToBeReported\": ");
		sb.append((getNoTransactionToBeReported() == null) ? "\"\"," : "\""+getNoTransactionToBeReported()+"\",");
		sb.append("\"transactionId\": ");
		sb.append((getTransactionId() == null) ? "\"\"," : "\""+getTransactionId()+"\",");
		sb.append("\"relatedAcct\": ");
		sb.append((getRelatedAcct() == null) ? "\"\"," : "\""+getRelatedAcct()+"\",");
		sb.append("\"additionalRemarks\": ");
		sb.append((getAdditionalRemarks() == null) ? "\"\"," : "\""+getAdditionalRemarks()+"\",");
		sb.append("\"volumeOfTransactions\": ");
		sb.append((getVolumeOfTransactions() == null) ? "\"\"," : "\""+getVolumeOfTransactions()+"\",");
		sb.append("\"beneficiaryDetails\": ");
		sb.append((getBeneficiaryDetails() == null) ? "\"\"," : "\""+getBeneficiaryDetails()+"\",");
		sb.append("\"factName\": ");
		sb.append((getFactName() == null) ? "\"\"," : "\""+getFactName()+"\",");
		sb.append("\"riskLevel\": ");
		sb.append((getRiskLevel() == null) ? "\"\"," : "\""+getRiskLevel()+"\",");
		sb.append("\"caseEntityId\": ");
		sb.append((getCaseEntityId() == null) ? "\"\"," : "\""+getCaseEntityId()+"\",");
		sb.append("\"entityIdType\": ");
		sb.append((getEntityIdType() == null) ? "\"\"," : "\""+getEntityIdType()+"\",");
		sb.append("\"incidentScore\": ");
		sb.append((getIncidentScore() == null) ? "\"\"," : "\""+getIncidentScore()+"\",");
		sb.append("\"entityName\": ");
		sb.append((getEntityName() == null) ? "\"\"," : "\""+getEntityName()+"\",");
		sb.append("\"incidentClosedDate\": ");
		sb.append((getIncidentClosedDate() == null) ? "\"\"," : "\""+getIncidentClosedDate()+"\",");
		sb.append("\"entityId\": ");
		sb.append((getEntityId() == null) ? "\"\"," : "\""+getEntityId()+"\",");
		sb.append("\"caseEntityName\": ");
		sb.append((getCaseEntityName() == null) ? "\"\"," : "\""+getCaseEntityName()+"\",");
		sb.append("\"caseDisplayKey\": ");
		sb.append((getCaseDisplayKey() == null) ? "\"\"," : "\""+getCaseDisplayKey()+"\",");
		sb.append("\"noOfStr\": ");
		sb.append((getNoOfStr() == null) ? "\"\"," : "\""+getNoOfStr()+"\",");
		sb.append("\"lastFiledStr\": ");
		sb.append((getLastFiledStr() == null) ? "\"\"," : "\""+getLastFiledStr()+"\",");
		sb.append("\"relationshipBeginDate\": ");
		sb.append((getRelationshipBeginDate() == null) ? "\"\"," : "\""+getRelationshipBeginDate()+"\",");
		sb.append("\"custIdNum\": ");
		sb.append((getCustIdNum() == null) ? "\"\"," : "\""+getCustIdNum()+"\",");
		sb.append("\"custFax\": ");
		sb.append((getCustFax() == null) ? "\"\"," : "\""+getCustFax()+"\",");
		sb.append("\"professionType\": ");
		sb.append((getProfessionType() == null) ? "\"\"," : "\""+getProfessionType()+"\",");
		sb.append("\"custType\": ");
		sb.append((getCustType() == null) ? "\"\"," : "\""+getCustType()+"\",");
		sb.append("\"custGender\": ");
		sb.append((getCustGender() == null) ? "\"\"," : "\""+getCustGender()+"\",");
		sb.append("\"custMobile\": ");
		sb.append((getCustMobile() == null) ? "\"\"," : "\""+getCustMobile()+"\",");
		sb.append("\"custEmail\": ");
		sb.append((getCustEmail() == null) ? "\"\"," : "\""+getCustEmail()+"\",");
		sb.append("\"custMailingAddr\": ");
		sb.append((getCustMailingAddr() == null) ? "\"\"," : "\""+getCustMailingAddr()+"\",");
		sb.append("\"custResAddr\": ");
		sb.append((getCustResAddr() == null) ? "\"\"," : "\""+getCustResAddr()+"\",");
		sb.append("\"custResAddrCity\": ");
		sb.append((getCustResAddrCity() == null) ? "\"\"," : "\""+getCustResAddrCity()+"\",");
		sb.append("\"custResAddrState\": ");
		sb.append((getCustResAddrState() == null) ? "\"\"," : "\""+getCustResAddrState()+"\",");
		sb.append("\"custResAddrCountry\": ");
		sb.append((getCustResAddrCountry() == null) ? "\"\"," : "\""+getCustResAddrCountry()+"\",");
		sb.append("\"custDOB\": ");
		sb.append((getCustDOB() == null) ? "\"\"," : "\""+getCustDOB()+"\",");
		sb.append("\"custName\": ");
		sb.append((getCustName() == null) ? "\"\"," : "\""+getCustName()+"\",");
		sb.append("\"createdOn\": ");
		sb.append((getCreatedOn() == null) ? "\"\"," : "\""+getCreatedOn()+"\",");
		sb.append("\"reportRelatedTo\": ");
		sb.append((getReportRelatedTo() == null) ? "\"\"," : "\""+getReportRelatedTo()+"\",");
		sb.append("\"accountType\": ");
		sb.append((getAccountType() == null) ? "\"\"," : "\""+getAccountType()+"\",");
		sb.append("\"personExecutingTxnAcctholderName\": ");
		sb.append((getPersonExecutingTxnAcctholderName() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderName()+"\",");
		sb.append("\"personExecutingTxnAcctholderDateOfBirth\": ");
		sb.append((getPersonExecutingTxnAcctholderDateOfBirth() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderDateOfBirth()+"\",");
		sb.append("\"personExecutingTxnAcctholderIdNumber\": ");
		sb.append((getPersonExecutingTxnAcctholderIdNumber() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderIdNumber()+"\",");
		sb.append("\"personExecutingTxnAcctholderPassportNumber\": ");
		sb.append((getPersonExecutingTxnAcctholderPassportNumber() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderPassportNumber()+"\",");
		sb.append("\"personExecutingTxnAcctholderNationality\": ");
		sb.append((getPersonExecutingTxnAcctholderNationality() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderNationality()+"\",");
		sb.append("\"businessentityPlaceOfIncorporation\": ");
		sb.append((getBusinessentityPlaceOfIncorporation() == null) ? "\"\"," : "\""+getBusinessentityPlaceOfIncorporation()+"\",");
		sb.append("\"businessentityDateOfIncorporation\": ");
		sb.append((getBusinessentityDateOfIncorporation() == null) ? "\"\"," : "\""+getBusinessentityDateOfIncorporation()+"\",");
		sb.append("\"businessentityBusinessActivity\": ");
		sb.append((getBusinessentityBusinessActivity() == null) ? "\"\"," : "\""+getBusinessentityBusinessActivity()+"\",");
		sb.append("\"aCompanyDirectorsDateOfBirth\": ");
		sb.append((getACompanyDirectorsDateOfBirth() == null) ? "\"\"," : "\""+getACompanyDirectorsDateOfBirth()+"\",");
		sb.append("\"aCompanyDirectorsIdNumber\": ");
		sb.append((getACompanyDirectorsIdNumber() == null) ? "\"\"," : "\""+getACompanyDirectorsIdNumber()+"\",");
		sb.append("\"aCompanyDirectorsPassportNumber\": ");
		sb.append((getACompanyDirectorsPassportNumber() == null) ? "\"\"," : "\""+getACompanyDirectorsPassportNumber()+"\",");
		sb.append("\"aCompanyDirectorsNationality\": ");
		sb.append((getACompanyDirectorsNationality() == null) ? "\"\"," : "\""+getACompanyDirectorsNationality()+"\",");
		sb.append("\"aCompanyDirectorsOccupation\": ");
		sb.append((getACompanyDirectorsOccupation() == null) ? "\"\"," : "\""+getACompanyDirectorsOccupation()+"\",");
		sb.append("\"bCompanyDirectorsDateOfBirth\": ");
		sb.append((getBCompanyDirectorsDateOfBirth() == null) ? "\"\"," : "\""+getBCompanyDirectorsDateOfBirth()+"\",");
		sb.append("\"bCompanyDirectorsIdNumber\": ");
		sb.append((getBCompanyDirectorsIdNumber() == null) ? "\"\"," : "\""+getBCompanyDirectorsIdNumber()+"\",");
		sb.append("\"bCompanyDirectorsPassportNumber\": ");
		sb.append((getBCompanyDirectorsPassportNumber() == null) ? "\"\"," : "\""+getBCompanyDirectorsPassportNumber()+"\",");
		sb.append("\"bCompanyDirectorsNationality\": ");
		sb.append((getBCompanyDirectorsNationality() == null) ? "\"\"," : "\""+getBCompanyDirectorsNationality()+"\",");
		sb.append("\"bCompanyDirectorsOccupation\": ");
		sb.append((getBCompanyDirectorsOccupation() == null) ? "\"\"," : "\""+getBCompanyDirectorsOccupation()+"\",");
		sb.append("\"cCompanyDirectorsDateOfBirth\": ");
		sb.append((getCCompanyDirectorsDateOfBirth() == null) ? "\"\"," : "\""+getCCompanyDirectorsDateOfBirth()+"\",");
		sb.append("\"cCompanyDirectorsIdNumber\": ");
		sb.append((getCCompanyDirectorsIdNumber() == null) ? "\"\"," : "\""+getCCompanyDirectorsIdNumber()+"\",");
		sb.append("\"cCompanyDirectorsPassportNumber\": ");
		sb.append((getCCompanyDirectorsPassportNumber() == null) ? "\"\"," : "\""+getCCompanyDirectorsPassportNumber()+"\",");
		sb.append("\"cCompanyDirectorsNationality\": ");
		sb.append((getCCompanyDirectorsNationality() == null) ? "\"\"," : "\""+getCCompanyDirectorsNationality()+"\",");
		sb.append("\"cCompanyDirectorsOccupation\": ");
		sb.append((getCCompanyDirectorsOccupation() == null) ? "\"\"," : "\""+getCCompanyDirectorsOccupation()+"\",");
		sb.append("\"aBeneficialOwnersName\": ");
		sb.append((getABeneficialOwnersName() == null) ? "\"\"," : "\""+getABeneficialOwnersName()+"\",");
		sb.append("\"aBeneficialOwnersDateOfBirth\": ");
		sb.append((getABeneficialOwnersDateOfBirth() == null) ? "\"\"," : "\""+getABeneficialOwnersDateOfBirth()+"\",");
		sb.append("\"aBeneficialOwnersIdNumber\": ");
		sb.append((getABeneficialOwnersIdNumber() == null) ? "\"\"," : "\""+getABeneficialOwnersIdNumber()+"\",");
		sb.append("\"aBeneficialOwnersPassportNumber\": ");
		sb.append((getABeneficialOwnersPassportNumber() == null) ? "\"\"," : "\""+getABeneficialOwnersPassportNumber()+"\",");
		sb.append("\"aBeneficialOwnersNationality\": ");
		sb.append((getABeneficialOwnersNationality() == null) ? "\"\"," : "\""+getABeneficialOwnersNationality()+"\",");
		sb.append("\"aBeneficialOwnersOccupation\": ");
		sb.append((getABeneficialOwnersOccupation() == null) ? "\"\"," : "\""+getABeneficialOwnersOccupation()+"\",");
		sb.append("\"aBeneficialOwnersDateofappointment\": ");
		sb.append((getABeneficialOwnersDateofappointment() == null) ? "\"\"," : "\""+getABeneficialOwnersDateofappointment()+"\",");
		sb.append("\"aBeneficialOwnersDateofresignation\": ");
		sb.append((getABeneficialOwnersDateofresignation() == null) ? "\"\"," : "\""+getABeneficialOwnersDateofresignation()+"\",");
		sb.append("\"bBeneficialOwnersName\": ");
		sb.append((getBBeneficialOwnersName() == null) ? "\"\"," : "\""+getBBeneficialOwnersName()+"\",");
		sb.append("\"bBeneficialOwnersDateOfBirth\": ");
		sb.append((getBBeneficialOwnersDateOfBirth() == null) ? "\"\"," : "\""+getBBeneficialOwnersDateOfBirth()+"\",");
		sb.append("\"bBeneficialOwnersIdNumber\": ");
		sb.append((getBBeneficialOwnersIdNumber() == null) ? "\"\"," : "\""+getBBeneficialOwnersIdNumber()+"\",");
		sb.append("\"bBeneficialOwnersPassportNumber\": ");
		sb.append((getBBeneficialOwnersPassportNumber() == null) ? "\"\"," : "\""+getBBeneficialOwnersPassportNumber()+"\",");
		sb.append("\"bBeneficialOwnersNationality\": ");
		sb.append((getBBeneficialOwnersNationality() == null) ? "\"\"," : "\""+getBBeneficialOwnersNationality()+"\",");
		sb.append("\"bBeneficialOwnersOccupation\": ");
		sb.append((getBBeneficialOwnersOccupation() == null) ? "\"\"," : "\""+getBBeneficialOwnersOccupation()+"\",");
		sb.append("\"bBeneficialOwnersDateofappointment\": ");
		sb.append((getBBeneficialOwnersDateofappointment() == null) ? "\"\"," : "\""+getBBeneficialOwnersDateofappointment()+"\",");
		sb.append("\"bBeneficialOwnersDateofresignation\": ");
		sb.append((getBBeneficialOwnersDateofresignation() == null) ? "\"\"," : "\""+getBBeneficialOwnersDateofresignation()+"\",");
		sb.append("\"informationAssociatedPersonsCompaniesEtc\": ");
		sb.append((getInformationAssociatedPersonsCompaniesEtc() == null) ? "\"\"," : "\""+getInformationAssociatedPersonsCompaniesEtc()+"\",");
		sb.append("\"whyTheTxnWasReportedSuspicious\": ");
		sb.append((getWhyTheTxnWasReportedSuspicious() == null) ? "\"\"," : "\""+getWhyTheTxnWasReportedSuspicious()+"\",");
		sb.append("\"designation\": ");
		sb.append((getDesignation() == null) ? "\"\"," : "\""+getDesignation()+"\",");
		sb.append("\"date\": ");
		sb.append((getDate() == null) ? "\"\"," : "\""+getDate()+"\",");

	    sb.append("}");
    	return sb.toString().replaceAll(Pattern.quote(",}"), "}").replaceAll(Pattern.quote(",]"), "]");
	}
	public String toJsonSnakeCase(){
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"str_id\": ");
		sb.append((getStrId() == null) ? "\"\"," : "\""+getStrId()+"\",");
		sb.append("\"account_id\": ");
		sb.append((getAccountId() == null) ? "\"\"," : "\""+getAccountId()+"\",");
		sb.append("\"no_transaction_to_be_reported\": ");
		sb.append((getNoTransactionToBeReported() == null) ? "\"\"," : "\""+getNoTransactionToBeReported()+"\",");
		sb.append("\"transaction_id\": ");
		sb.append((getTransactionId() == null) ? "\"\"," : "\""+getTransactionId()+"\",");
		sb.append("\"related_acct\": ");
		sb.append((getRelatedAcct() == null) ? "\"\"," : "\""+getRelatedAcct()+"\",");
		sb.append("\"additional_remarks\": ");
		sb.append((getAdditionalRemarks() == null) ? "\"\"," : "\""+getAdditionalRemarks()+"\",");
		sb.append("\"volume_of_transactions\": ");
		sb.append((getVolumeOfTransactions() == null) ? "\"\"," : "\""+getVolumeOfTransactions()+"\",");
		sb.append("\"beneficiary_details\": ");
		sb.append((getBeneficiaryDetails() == null) ? "\"\"," : "\""+getBeneficiaryDetails()+"\",");
		sb.append("\"fact_name\": ");
		sb.append((getFactName() == null) ? "\"\"," : "\""+getFactName()+"\",");
		sb.append("\"risk_level\": ");
		sb.append((getRiskLevel() == null) ? "\"\"," : "\""+getRiskLevel()+"\",");
		sb.append("\"case_entity_id\": ");
		sb.append((getCaseEntityId() == null) ? "\"\"," : "\""+getCaseEntityId()+"\",");
		sb.append("\"entity_id_type\": ");
		sb.append((getEntityIdType() == null) ? "\"\"," : "\""+getEntityIdType()+"\",");
		sb.append("\"incident_score\": ");
		sb.append((getIncidentScore() == null) ? "\"\"," : "\""+getIncidentScore()+"\",");
		sb.append("\"entity_name\": ");
		sb.append((getEntityName() == null) ? "\"\"," : "\""+getEntityName()+"\",");
		sb.append("\"incident_closed_date\": ");
		sb.append((getIncidentClosedDate() == null) ? "\"\"," : "\""+getIncidentClosedDate()+"\",");
		sb.append("\"entity_id\": ");
		sb.append((getEntityId() == null) ? "\"\"," : "\""+getEntityId()+"\",");
		sb.append("\"case_entity_name\": ");
		sb.append((getCaseEntityName() == null) ? "\"\"," : "\""+getCaseEntityName()+"\",");
		sb.append("\"case_display_key\": ");
		sb.append((getCaseDisplayKey() == null) ? "\"\"," : "\""+getCaseDisplayKey()+"\",");
		sb.append("\"no_of_str\": ");
		sb.append((getNoOfStr() == null) ? "\"\"," : "\""+getNoOfStr()+"\",");
		sb.append("\"last_filed_str\": ");
		sb.append((getLastFiledStr() == null) ? "\"\"," : "\""+getLastFiledStr()+"\",");
		sb.append("\"relationship_begin_date\": ");
		sb.append((getRelationshipBeginDate() == null) ? "\"\"," : "\""+getRelationshipBeginDate()+"\",");
		sb.append("\"cust_id_num\": ");
		sb.append((getCustIdNum() == null) ? "\"\"," : "\""+getCustIdNum()+"\",");
		sb.append("\"cust_fax\": ");
		sb.append((getCustFax() == null) ? "\"\"," : "\""+getCustFax()+"\",");
		sb.append("\"profession_type\": ");
		sb.append((getProfessionType() == null) ? "\"\"," : "\""+getProfessionType()+"\",");
		sb.append("\"cust_type\": ");
		sb.append((getCustType() == null) ? "\"\"," : "\""+getCustType()+"\",");
		sb.append("\"cust_gender\": ");
		sb.append((getCustGender() == null) ? "\"\"," : "\""+getCustGender()+"\",");
		sb.append("\"cust_mobile\": ");
		sb.append((getCustMobile() == null) ? "\"\"," : "\""+getCustMobile()+"\",");
		sb.append("\"cust_email\": ");
		sb.append((getCustEmail() == null) ? "\"\"," : "\""+getCustEmail()+"\",");
		sb.append("\"cust_mailing_addr\": ");
		sb.append((getCustMailingAddr() == null) ? "\"\"," : "\""+getCustMailingAddr()+"\",");
		sb.append("\"cust_res_addr\": ");
		sb.append((getCustResAddr() == null) ? "\"\"," : "\""+getCustResAddr()+"\",");
		sb.append("\"cust_res_addr_city\": ");
		sb.append((getCustResAddrCity() == null) ? "\"\"," : "\""+getCustResAddrCity()+"\",");
		sb.append("\"cust_res_addr_state\": ");
		sb.append((getCustResAddrState() == null) ? "\"\"," : "\""+getCustResAddrState()+"\",");
		sb.append("\"cust_res_addr_country\": ");
		sb.append((getCustResAddrCountry() == null) ? "\"\"," : "\""+getCustResAddrCountry()+"\",");
		sb.append("\"cust_d_o_b\": ");
		sb.append((getCustDOB() == null) ? "\"\"," : "\""+getCustDOB()+"\",");
		sb.append("\"cust_name\": ");
		sb.append((getCustName() == null) ? "\"\"," : "\""+getCustName()+"\",");
		sb.append("\"createdOn\": ");
		sb.append((getCreatedOn() == null) ? "\"\"," : "\""+getCreatedOn()+"\",");
		sb.append("\"report_related_to\": ");
		sb.append((getReportRelatedTo() == null) ? "\"\"," : "\""+getReportRelatedTo()+"\",");
		sb.append("\"account_type\": ");
		sb.append((getAccountType() == null) ? "\"\"," : "\""+getAccountType()+"\",");
		sb.append("\"person_executing_txn_acctholder_name\": ");
		sb.append((getPersonExecutingTxnAcctholderName() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderName()+"\",");
		sb.append("\"person_executing_txn_acctholder_date_of_birth\": ");
		sb.append((getPersonExecutingTxnAcctholderDateOfBirth() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderDateOfBirth()+"\",");
		sb.append("\"person_executing_txn_acctholder_id_number\": ");
		sb.append((getPersonExecutingTxnAcctholderIdNumber() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderIdNumber()+"\",");
		sb.append("\"person_executing_txn_acctholder_passport_number\": ");
		sb.append((getPersonExecutingTxnAcctholderPassportNumber() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderPassportNumber()+"\",");
		sb.append("\"person_executing_txn_acctholder_nationality\": ");
		sb.append((getPersonExecutingTxnAcctholderNationality() == null) ? "\"\"," : "\""+getPersonExecutingTxnAcctholderNationality()+"\",");
		sb.append("\"businessentity_place_of_incorporation\": ");
		sb.append((getBusinessentityPlaceOfIncorporation() == null) ? "\"\"," : "\""+getBusinessentityPlaceOfIncorporation()+"\",");
		sb.append("\"businessentity_date_of_incorporation\": ");
		sb.append((getBusinessentityDateOfIncorporation() == null) ? "\"\"," : "\""+getBusinessentityDateOfIncorporation()+"\",");
		sb.append("\"businessentity_business_activity\": ");
		sb.append((getBusinessentityBusinessActivity() == null) ? "\"\"," : "\""+getBusinessentityBusinessActivity()+"\",");
		sb.append("\"a_company_directors_date_of_birth\": ");
		sb.append((getACompanyDirectorsDateOfBirth() == null) ? "\"\"," : "\""+getACompanyDirectorsDateOfBirth()+"\",");
		sb.append("\"a_company_directors_id_number\": ");
		sb.append((getACompanyDirectorsIdNumber() == null) ? "\"\"," : "\""+getACompanyDirectorsIdNumber()+"\",");
		sb.append("\"a_company_directors_passport_number\": ");
		sb.append((getACompanyDirectorsPassportNumber() == null) ? "\"\"," : "\""+getACompanyDirectorsPassportNumber()+"\",");
		sb.append("\"a_company_directors_nationality\": ");
		sb.append((getACompanyDirectorsNationality() == null) ? "\"\"," : "\""+getACompanyDirectorsNationality()+"\",");
		sb.append("\"a_company_directors_occupation\": ");
		sb.append((getACompanyDirectorsOccupation() == null) ? "\"\"," : "\""+getACompanyDirectorsOccupation()+"\",");
		sb.append("\"b_company_directors_date_of_birth\": ");
		sb.append((getBCompanyDirectorsDateOfBirth() == null) ? "\"\"," : "\""+getBCompanyDirectorsDateOfBirth()+"\",");
		sb.append("\"b_company_directors_id_number\": ");
		sb.append((getBCompanyDirectorsIdNumber() == null) ? "\"\"," : "\""+getBCompanyDirectorsIdNumber()+"\",");
		sb.append("\"b_company_directors_passport_number\": ");
		sb.append((getBCompanyDirectorsPassportNumber() == null) ? "\"\"," : "\""+getBCompanyDirectorsPassportNumber()+"\",");
		sb.append("\"b_company_directors_nationality\": ");
		sb.append((getBCompanyDirectorsNationality() == null) ? "\"\"," : "\""+getBCompanyDirectorsNationality()+"\",");
		sb.append("\"b_company_directors_occupation\": ");
		sb.append((getBCompanyDirectorsOccupation() == null) ? "\"\"," : "\""+getBCompanyDirectorsOccupation()+"\",");
		sb.append("\"c_company_directors_date_of_birth\": ");
		sb.append((getCCompanyDirectorsDateOfBirth() == null) ? "\"\"," : "\""+getCCompanyDirectorsDateOfBirth()+"\",");
		sb.append("\"c_company_directors_id_number\": ");
		sb.append((getCCompanyDirectorsIdNumber() == null) ? "\"\"," : "\""+getCCompanyDirectorsIdNumber()+"\",");
		sb.append("\"c_company_directors_passport_number\": ");
		sb.append((getCCompanyDirectorsPassportNumber() == null) ? "\"\"," : "\""+getCCompanyDirectorsPassportNumber()+"\",");
		sb.append("\"c_company_directors_nationality\": ");
		sb.append((getCCompanyDirectorsNationality() == null) ? "\"\"," : "\""+getCCompanyDirectorsNationality()+"\",");
		sb.append("\"c_company_directors_occupation\": ");
		sb.append((getCCompanyDirectorsOccupation() == null) ? "\"\"," : "\""+getCCompanyDirectorsOccupation()+"\",");
		sb.append("\"a_beneficial_owners_name\": ");
		sb.append((getABeneficialOwnersName() == null) ? "\"\"," : "\""+getABeneficialOwnersName()+"\",");
		sb.append("\"a_beneficial_owners_date_of_birth\": ");
		sb.append((getABeneficialOwnersDateOfBirth() == null) ? "\"\"," : "\""+getABeneficialOwnersDateOfBirth()+"\",");
		sb.append("\"a_beneficial_owners_id_number\": ");
		sb.append((getABeneficialOwnersIdNumber() == null) ? "\"\"," : "\""+getABeneficialOwnersIdNumber()+"\",");
		sb.append("\"a_beneficial_owners_passport_number\": ");
		sb.append((getABeneficialOwnersPassportNumber() == null) ? "\"\"," : "\""+getABeneficialOwnersPassportNumber()+"\",");
		sb.append("\"a_beneficial_owners_nationality\": ");
		sb.append((getABeneficialOwnersNationality() == null) ? "\"\"," : "\""+getABeneficialOwnersNationality()+"\",");
		sb.append("\"a_beneficial_owners_occupation\": ");
		sb.append((getABeneficialOwnersOccupation() == null) ? "\"\"," : "\""+getABeneficialOwnersOccupation()+"\",");
		sb.append("\"a_beneficial_owners_dateofappointment\": ");
		sb.append((getABeneficialOwnersDateofappointment() == null) ? "\"\"," : "\""+getABeneficialOwnersDateofappointment()+"\",");
		sb.append("\"a_beneficial_owners_dateofresignation\": ");
		sb.append((getABeneficialOwnersDateofresignation() == null) ? "\"\"," : "\""+getABeneficialOwnersDateofresignation()+"\",");
		sb.append("\"b_beneficial_owners_name\": ");
		sb.append((getBBeneficialOwnersName() == null) ? "\"\"," : "\""+getBBeneficialOwnersName()+"\",");
		sb.append("\"b_beneficial_owners_date_of_birth\": ");
		sb.append((getBBeneficialOwnersDateOfBirth() == null) ? "\"\"," : "\""+getBBeneficialOwnersDateOfBirth()+"\",");
		sb.append("\"b_beneficial_owners_id_number\": ");
		sb.append((getBBeneficialOwnersIdNumber() == null) ? "\"\"," : "\""+getBBeneficialOwnersIdNumber()+"\",");
		sb.append("\"b_beneficial_owners_passport_number\": ");
		sb.append((getBBeneficialOwnersPassportNumber() == null) ? "\"\"," : "\""+getBBeneficialOwnersPassportNumber()+"\",");
		sb.append("\"b_beneficial_owners_nationality\": ");
		sb.append((getBBeneficialOwnersNationality() == null) ? "\"\"," : "\""+getBBeneficialOwnersNationality()+"\",");
		sb.append("\"b_beneficial_owners_occupation\": ");
		sb.append((getBBeneficialOwnersOccupation() == null) ? "\"\"," : "\""+getBBeneficialOwnersOccupation()+"\",");
		sb.append("\"b_beneficial_owners_dateofappointment\": ");
		sb.append((getBBeneficialOwnersDateofappointment() == null) ? "\"\"," : "\""+getBBeneficialOwnersDateofappointment()+"\",");
		sb.append("\"b_beneficial_owners_dateofresignation\": ");
		sb.append((getBBeneficialOwnersDateofresignation() == null) ? "\"\"," : "\""+getBBeneficialOwnersDateofresignation()+"\",");
		sb.append("\"information_associated_persons_companies_etc\": ");
		sb.append((getInformationAssociatedPersonsCompaniesEtc() == null) ? "\"\"," : "\""+getInformationAssociatedPersonsCompaniesEtc()+"\",");
		sb.append("\"why_the_txn_was_reported_suspicious\": ");
		sb.append((getWhyTheTxnWasReportedSuspicious() == null) ? "\"\"," : "\""+getWhyTheTxnWasReportedSuspicious()+"\",");
		sb.append("\"designation\": ");
		sb.append((getDesignation() == null) ? "\"\"," : "\""+getDesignation()+"\",");
		sb.append("\"date\": ");
		sb.append((getDate() == null) ? "\"\"," : "\""+getDate()+"\",");

	    sb.append("}");
    	return sb.toString().replaceAll(Pattern.quote(",}"), "}").replaceAll(Pattern.quote(",]"), "]");
	}

    // TODO: This needs to be done
    public void setDerivedAttributes(){

    }
}