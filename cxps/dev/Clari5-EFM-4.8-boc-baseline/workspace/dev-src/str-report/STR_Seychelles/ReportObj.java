package clari5.aml.reports.rbi.jasperUtil.str.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import clari5.report.db.StrReport;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shashank on 5/2/18.
 */

public class ReportObj {
    private String strId = null;
    private String accountId = null;
    private String noTransactionToBeReported = null;
    private String transactionId = null;
    private String relatedAcct = null;
    private String additionalRemarks = null;
    private String background = null;
    private String eventName = null;
    private String eventId = null;
    private String factName = null;
    private String riskLevel = null;
    private String caseEntityId = null;
    private String entityIdType = null;
    private String incidentScore = null;
    private String entityName = null;
    private String incidentClosedDate = null;
    private String entityId = null;
    private String caseEntityName = null;
    private String caseDisplayKey = null;
    private Integer noOfStr = null;
    private Long lastFiledStr = null;
    private String custIdNum = null;
    private String custFax = null;
    private String professionType = null;
    private String natureOfBusiness = null;
    private String custType = null;
    private String custGender = null;
    private String custMobile = null;
    private String custEmail = null;
    private String custMailingAddr = null;
    private String custResAddr = null;
    private String custResAddrCity = null;
    private String custResAddrState = null;
    private String custResAddrCountry = null;
    private String custDOB = null;
    private String custName = null;
    private String createdOn = null;
    private String beneficiaryDetails = null;
    private String volumeOfTransactions = null;
    private String relationshipBeginDate = null;
    private String personExecutingTxnAcctholderName = null;
    private String personExecutingTxnAcctholderDateOfBirth = null;
    private String personExecutingTxnAcctholderIdNumber = null;
    private String personExecutingTxnAcctholderPassportNumber = null;
    private String personExecutingTxnAcctholderNationality = null;
    private String businessentityPlaceOfIncorporation = null;
    private String businessentityDateOfIncorporation = null;
    private String businessentityBusinessActivity = null;
    private String aCompanyDirectorsDateOfBirth=null;
    private String aCompanyDirectorsIdNumber = null;
    private String aCompanyDirectorsPassportNumber = null;
    private String aCompanyDirectorsNationality = null;
    private String aCompanyDirectorsOccupation = null;
    private String bCompanyDirectorsDateOfBirth = null;
    private String bCompanyDirectorsIdNumber = null;
    private String bCompanyDirectorsPassportNumber = null;
    private String bCompanyDirectorsNationality = null;
    private String bCompanyDirectorsOccupation = null;
    private String cCompanyDirectorsDateOfBirth = null;
    private String cCompanyDirectorsIdNumber = null;
    private String cCompanyDirectorsPassportNumber = null;
    private String cCompanyDirectorsNationality = null;
    private String cCompanyDirectorsOccupation = null;
    private String aBeneficialOwnersName = null;
    private String aBeneficialOwnersDateOfBirth = null;
    private String aBeneficialOwnersIdNumber = null;
    private String aBeneficialOwnersPassportNumber = null;
    private String aBeneficialOwnersNationality = null;
    private String aBeneficialOwnersOccupation = null;
    private String aBeneficialOwnersDateofappointment = null;
    private String aBeneficialOwnersDateofresignation = null;
    private String bBeneficialOwnersName = null;
    private String bBeneficialOwnersDateOfBirth = null;
    private String bBeneficialOwnersIdNumber = null;
    private String bBeneficialOwnersPassportNumber = null;
    private String bBeneficialOwnersNationality = null;
    private String bBeneficialOwnersOccupation = null;
    private String bBeneficialOwnersDateofappointment = null;
    private String bBeneficialOwnersDateofresignation = null;
    private String informationAssociatedPersonsCompaniesEtc = null;
    private String whyTheTxnWasReportedSuspicious = null;
   // private String signatureOfOfficial = null;
    private String designation = null;
    private String date = null;
    private String reportRelatedTo = null;
    private String accountType = null;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getNoTransactionToBeReported() {
        return noTransactionToBeReported;
    }

    public void setNoTransactionToBeReported(String noTransactionToBeReported) {
        this.noTransactionToBeReported = noTransactionToBeReported;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRelatedAcct() {
        return relatedAcct;
    }

    public void setRelatedAcct(String relatedAcct) {
        this.relatedAcct = relatedAcct;
    }

    public String getAdditionalRemarks() {
        return additionalRemarks;
    }

    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getFactName() {
        return factName;
    }

    public void setFactName(String factName) {
        this.factName = factName;
    }

    public String getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }

    public String getCaseEntityId() {
        return caseEntityId;
    }

    public void setCaseEntityId(String caseEntityId) {
        this.caseEntityId = caseEntityId;
    }

    public String getEntityIdType() {
        return entityIdType;
    }

    public void setEntityIdType(String entityIdType) {
        this.entityIdType = entityIdType;
    }

    public String getIncidentScore() {
        return incidentScore;
    }

    public void setIncidentScore(String incidentScore) {
        this.incidentScore = incidentScore;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getIncidentClosedDate() {
        return incidentClosedDate;
    }

    public void setIncidentClosedDate(String incidentClosedDate) {
        this.incidentClosedDate = incidentClosedDate;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getCaseEntityName() {
        return caseEntityName;
    }

    public void setCaseEntityName(String caseEntityName) {
        this.caseEntityName = caseEntityName;
    }

    public String getCaseDisplayKey() {
        return caseDisplayKey;
    }

    public void setCaseDisplayKey(String caseDisplayKey) {
        this.caseDisplayKey = caseDisplayKey;
    }

    public Integer getNoOfStr() {
        return noOfStr;
    }

    public void setNoOfStr(Integer noOfStr) {
        this.noOfStr = noOfStr;
    }

    public Long getLastFiledStr() {
        return lastFiledStr;
    }

    public void setLastFiledStr(Long lastFiledStr) {
        this.lastFiledStr = lastFiledStr;
    }

    public String getCustIdNum() {
        return custIdNum;
    }

    public void setCustIdNum(String custIdNum) {
        this.custIdNum = custIdNum;
    }

    public String getCustFax() {
        return custFax;
    }

    public void setCustFax(String custFax) {
        this.custFax = custFax;
    }

    public String getProfessionType() {
        return professionType;
    }

    public void setProfessionType(String professionType) {
        this.professionType = professionType;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getCustGender() {
        return custGender;
    }

    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMailingAddr() {
        return custMailingAddr;
    }

    public void setCustMailingAddr(String custMailingAddr) {
        this.custMailingAddr = custMailingAddr;
    }

    public String getCustResAddr() {
        return custResAddr;
    }

    public void setCustResAddr(String custResAddr) {
        this.custResAddr = custResAddr;
    }

    public String getCustResAddrCity() {
        return custResAddrCity;
    }

    public void setCustResAddrCity(String custResAddrCity) {
        this.custResAddrCity = custResAddrCity;
    }

    public String getCustResAddrState() {
        return custResAddrState;
    }

    public void setCustResAddrState(String custResAddrState) {
        this.custResAddrState = custResAddrState;
    }

    public String getCustResAddrCountry() {
        return custResAddrCountry;
    }

    public void setCustResAddrCountry(String custResAddrCountry) {
        this.custResAddrCountry = custResAddrCountry;
    }

    public String getCustDOB() {
        return custDOB;
    }

    public void setCustDOB(String custDOB) {
        this.custDOB = custDOB;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getBeneficiaryDetails() {
        return beneficiaryDetails;
    }

    public void setBeneficiaryDetails(String beneficiaryDetails) {
        this.beneficiaryDetails = beneficiaryDetails;
    }

    public String getVolumeOfTransactions() {
        return volumeOfTransactions;
    }

    public void setVolumeOfTransactions(String volumeOfTransactions) {
        this.volumeOfTransactions = volumeOfTransactions;
    }

    public String getRelationshipBeginDate() {
        return relationshipBeginDate;
    }

    public void setRelationshipBeginDate(String relationshipBeginDate) {
        this.relationshipBeginDate = relationshipBeginDate;
    }

   public String getPersonExecutingTxnAcctholderName() {
        return personExecutingTxnAcctholderName;
    }

    public void setPersonExecutingTxnAcctholderName(String personExecutingTxnAcctholderName) {
        this.personExecutingTxnAcctholderName = personExecutingTxnAcctholderName;
    }

    public String getPersonExecutingTxnAcctholderDateOfBirth() {
        return personExecutingTxnAcctholderDateOfBirth;
    }

    public void setPersonExecutingTxnAcctholderDateOfBirth(String personExecutingTxnAcctholderDateOfBirth) {
        this.personExecutingTxnAcctholderDateOfBirth = personExecutingTxnAcctholderDateOfBirth;
    }

    public String getPersonExecutingTxnAcctholderIdNumber() {
        return personExecutingTxnAcctholderIdNumber;
    }

    public void setPersonExecutingTxnAcctholderIdNumber(String personExecutingTxnAcctholderIdNumber) {
        this.personExecutingTxnAcctholderIdNumber = personExecutingTxnAcctholderIdNumber;
    }

    public String getPersonExecutingTxnAcctholderPassportNumber() {
        return personExecutingTxnAcctholderPassportNumber;
    }

    public void setPersonExecutingTxnAcctholderPassportNumber(String personExecutingTxnAcctholderPassportNumber) {
        this.personExecutingTxnAcctholderPassportNumber = personExecutingTxnAcctholderPassportNumber;
    }

    public String getPersonExecutingTxnAcctholderNationality() {
        return personExecutingTxnAcctholderNationality;
    }

    public void setPersonExecutingTxnAcctholderNationality(String personExecutingTxnAcctholderNationality) {
        this.personExecutingTxnAcctholderNationality = personExecutingTxnAcctholderNationality;
    }

    public String getBusinessentityPlaceOfIncorporation() {
        return businessentityPlaceOfIncorporation;
    }

    public void setBusinessentityPlaceOfIncorporation(String businessentityPlaceOfIncorporation) {
        this.businessentityPlaceOfIncorporation = businessentityPlaceOfIncorporation;
    }

    public String getBusinessentityDateOfIncorporation() {
        return businessentityDateOfIncorporation;
    }

    public void setBusinessentityDateOfIncorporation(String businessentityDateOfIncorporation) {
        this.businessentityDateOfIncorporation = businessentityDateOfIncorporation;
    }

    public String getBusinessentityBusinessActivity() {
        return businessentityBusinessActivity;
    }

    public void setBusinessentityBusinessActivity(String businessentityBusinessActivity) {
        this.businessentityBusinessActivity = businessentityBusinessActivity;
    }

    public String getACompanyDirectorsDateOfBirth() {
        return aCompanyDirectorsDateOfBirth;
    }

    public void setACompanyDirectorsDateOfBirth(String aCompanyDirectorsDateOfBirth) {
        this.aCompanyDirectorsDateOfBirth = aCompanyDirectorsDateOfBirth;
    }

    public String getACompanyDirectorsIdNumber() {
        return aCompanyDirectorsIdNumber;
    }

    public void setACompanyDirectorsIdNumber(String aCompanyDirectorsIdNumber) {
        this.aCompanyDirectorsIdNumber = aCompanyDirectorsIdNumber;
    }

    public String getACompanyDirectorsPassportNumber() {
        return aCompanyDirectorsPassportNumber;
    }

    public void setACompanyDirectorsPassportNumber(String aCompanyDirectorsPassportNumber) {
        this.aCompanyDirectorsPassportNumber = aCompanyDirectorsPassportNumber;
    }

    public String getACompanyDirectorsNationality() {
        return aCompanyDirectorsNationality;
    }

    public void setACompanyDirectorsNationality(String aCompanyDirectorsNationality) {
        this.aCompanyDirectorsNationality = aCompanyDirectorsNationality;
    }

    public String getACompanyDirectorsOccupation() {
        return aCompanyDirectorsOccupation;
    }

    public void setACompanyDirectorsOccupation(String aCompanyDirectorsOccupation) {
        this.aCompanyDirectorsOccupation = aCompanyDirectorsOccupation;
    }

    public String getBCompanyDirectorsDateOfBirth() {
        return bCompanyDirectorsDateOfBirth;
    }

    public void setBCompanyDirectorsDateOfBirth(String bCompanyDirectorsDateOfBirth) {
        this.bCompanyDirectorsDateOfBirth = bCompanyDirectorsDateOfBirth;
    }

    public String getBCompanyDirectorsIdNumber() {
        return bCompanyDirectorsIdNumber;
    }

    public void setBCompanyDirectorsIdNumber(String bCompanyDirectorsIdNumber) {
        this.bCompanyDirectorsIdNumber = bCompanyDirectorsIdNumber;
    }

    public String getBCompanyDirectorsPassportNumber() {
        return bCompanyDirectorsPassportNumber;
    }

    public void setBCompanyDirectorsPassportNumber(String bCompanyDirectorsPassportNumber) {
        this.bCompanyDirectorsPassportNumber = bCompanyDirectorsPassportNumber;
    }

    public String getBCompanyDirectorsNationality() {
        return bCompanyDirectorsNationality;
    }

    public void setBCompanyDirectorsNationality(String bCompanyDirectorsNationality) {
        this.bCompanyDirectorsNationality = bCompanyDirectorsNationality;
    }

    public String getBCompanyDirectorsOccupation() {
        return bCompanyDirectorsOccupation;
    }

    public void setBCompanyDirectorsOccupation(String bCompanyDirectorsOccupation) {
        this.bCompanyDirectorsOccupation = bCompanyDirectorsOccupation;
    }

    public String getCCompanyDirectorsDateOfBirth() {
        return cCompanyDirectorsDateOfBirth;
    }

    public void setCCompanyDirectorsDateOfBirth(String cCompanyDirectorsDateOfBirth) {
        this.cCompanyDirectorsDateOfBirth = cCompanyDirectorsDateOfBirth;

    }

    public String getCCompanyDirectorsIdNumber() {
        return cCompanyDirectorsIdNumber;
    }

    public void setCCompanyDirectorsIdNumber(String cCompanyDirectorsIdNumber) {
        this.cCompanyDirectorsIdNumber = cCompanyDirectorsIdNumber;
    }

    public String getCCompanyDirectorsPassportNumber() {
        return cCompanyDirectorsPassportNumber;
    }

    public void setCCompanyDirectorsPassportNumber(String cCompanyDirectorsPassportNumber) {
        this.cCompanyDirectorsPassportNumber = cCompanyDirectorsPassportNumber;
    }

    public String getCCompanyDirectorsNationality() {
        return cCompanyDirectorsNationality;
    }

    public void setCCompanyDirectorsNationality(String cCompanyDirectorsNationality) {
        this.cCompanyDirectorsNationality = cCompanyDirectorsNationality;
    }

    public String getCCompanyDirectorsOccupation() {
        return cCompanyDirectorsOccupation;
    }

    public void setCCompanyDirectorsOccupation(String cCompanyDirectorsOccupation) {
        this.cCompanyDirectorsOccupation = cCompanyDirectorsOccupation;
    }

    public String getABeneficialOwnersName() {
        return aBeneficialOwnersName;
    }

    public void setABeneficialOwnersName(String aBeneficialOwnersName) {
        this.aBeneficialOwnersName = aBeneficialOwnersName;
    }

    public String getABeneficialOwnersDateOfBirth() {
        return aBeneficialOwnersDateOfBirth;
    }

    public void setABeneficialOwnersDateOfBirth(String aBeneficialOwnersDateOfBirth) {
        this.aBeneficialOwnersDateOfBirth = aBeneficialOwnersDateOfBirth;
    }

    public String getABeneficialOwnersIdNumber() {
        return aBeneficialOwnersIdNumber;
    }

    public void setABeneficialOwnersIdNumber(String aBeneficialOwnersIdNumber) {
        this.aBeneficialOwnersIdNumber = aBeneficialOwnersIdNumber;
    }

    public String getABeneficialOwnersPassportNumber() {
        return aBeneficialOwnersPassportNumber;
    }

    public void setABeneficialOwnersPassportNumber(String aBeneficialOwnersPassportNumber) {
        this.aBeneficialOwnersPassportNumber = aBeneficialOwnersPassportNumber;
    }

    public String getABeneficialOwnersNationality() {
        return aBeneficialOwnersNationality;
    }

    public void setABeneficialOwnersNationality(String aBeneficialOwnersNationality) {
        this.aBeneficialOwnersNationality = aBeneficialOwnersNationality;
    }

    public String getABeneficialOwnersOccupation() {
        return aBeneficialOwnersOccupation;
    }

    public void setABeneficialOwnersOccupation(String aBeneficialOwnersOccupation) {
        this.aBeneficialOwnersOccupation = aBeneficialOwnersOccupation;
    }

    public String getABeneficialOwnersDateofappointment() {
        return aBeneficialOwnersDateofappointment;
    }

    public void setABeneficialOwnersDateofappointment(String aBeneficialOwnersDateofappointment) {
        this.aBeneficialOwnersDateofappointment = aBeneficialOwnersDateofappointment;
    }

    public String getABeneficialOwnersDateofresignation() {
        return aBeneficialOwnersDateofresignation;
    }

    public void setABeneficialOwnersDateofresignation(String aBeneficialOwnersDateofresignation) {
        this.aBeneficialOwnersDateofresignation = aBeneficialOwnersDateofresignation;
    }

    public String getBBeneficialOwnersName() {
        return bBeneficialOwnersName;
    }

    public void setBBeneficialOwnersName(String bBeneficialOwnersName) {
        this.bBeneficialOwnersName = bBeneficialOwnersName;
    }

    public String getBBeneficialOwnersDateOfBirth() {
        return bBeneficialOwnersDateOfBirth;
    }

    public void setBBeneficialOwnersDateOfBirth(String bBeneficialOwnersDateOfBirth) {
        this.bBeneficialOwnersDateOfBirth = bBeneficialOwnersDateOfBirth;
    }

    public String getBBeneficialOwnersIdNumber() {
        return bBeneficialOwnersIdNumber;
    }

    public void setBBeneficialOwnersIdNumber(String bBeneficialOwnersIdNumber) {
        this.bBeneficialOwnersIdNumber = bBeneficialOwnersIdNumber;
    }

    public String getBBeneficialOwnersPassportNumber() {
        return bBeneficialOwnersPassportNumber;
    }

    public void setBBeneficialOwnersPassportNumber(String bBeneficialOwnersPassportNumber) {
        this.bBeneficialOwnersPassportNumber = bBeneficialOwnersPassportNumber;
    }

    public String getBBeneficialOwnersNationality() {
        return bBeneficialOwnersNationality;
    }

    public void setBBeneficialOwnersNationality(String bBeneficialOwnersNationality) {
        this.bBeneficialOwnersNationality = bBeneficialOwnersNationality;
    }

    public String getBBeneficialOwnersOccupation() {
        return bBeneficialOwnersOccupation;
    }

    public void setBBeneficialOwnersOccupation(String bBeneficialOwnersOccupation) {
        this.bBeneficialOwnersOccupation = bBeneficialOwnersOccupation;
    }

    public String getBBeneficialOwnersDateofappointment() {
        return bBeneficialOwnersDateofappointment;
    }

    public void setBBeneficialOwnersDateofappointment(String bBeneficialOwnersDateofappointment) {
        this.bBeneficialOwnersDateofappointment = bBeneficialOwnersDateofappointment;
    }

    public String getBBeneficialOwnersDateofresignation() {
        return bBeneficialOwnersDateofresignation;
    }

    public void setBBeneficialOwnersDateofresignation(String bBeneficialOwnersDateofresignation) {
        this.bBeneficialOwnersDateofresignation = bBeneficialOwnersDateofresignation;
    }


    public String getInformationAssociatedPersonsCompaniesEtc() {
        return informationAssociatedPersonsCompaniesEtc;
    }

    public void setInformationAssociatedPersonsCompaniesEtc(String informationAssociatedPersonsCompaniesEtc) {
        this.informationAssociatedPersonsCompaniesEtc = informationAssociatedPersonsCompaniesEtc;
    }

    public String getWhyTheTxnWasReportedSuspicious() {
        return whyTheTxnWasReportedSuspicious;
    }

    public void setWhyTheTxnWasReportedSuspicious(String whyTheTxnWasReportedSuspicious) {
        this.whyTheTxnWasReportedSuspicious = whyTheTxnWasReportedSuspicious;
    }

  //  public String getSignatureOfOfficial() {
    //    return signatureOfOfficial;
   // }

    //public void setSignatureOfOfficial(String signatureOfOfficial) {
      //  this.signatureOfOfficial = signatureOfOfficial;
   // }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReportRelatedTo() { return reportRelatedTo; }

    public void setReportRelatedTo(String reportRelatedTo) { this.reportRelatedTo = reportRelatedTo;  }

    public String getAccountType() { return accountType;  }

    public void setAccountType(String accountType) {this.accountType = accountType; }




    @JsonIgnore
    public static ReportObj mapStrReportToReportObj(StrReport report) {
        ReportObj obj = new ReportObj();
        obj.setStrId(report.getStrId());
        obj.setAccountId(report.getAccountId());
        obj.setNoTransactionToBeReported(report.getNoTransactionToBeReported());
        obj.setTransactionId(report.getTransactionId());
        obj.setRelatedAcct(report.getRelatedAcct());
        obj.setAdditionalRemarks(report.getAdditionalRemarks());
        obj.setFactName(report.getFactName());
        obj.setRiskLevel(report.getRiskLevel());
        obj.setCaseEntityId(report.getCaseEntityId());
        obj.setEntityIdType(report.getEntityIdType());
        obj.setIncidentScore(report.getIncidentScore());
        obj.setEntityName(report.getEntityName());
        obj.setIncidentClosedDate(report.getIncidentClosedDate());
        obj.setEntityId(report.getEntityId());
        obj.setCaseEntityName(report.getCaseEntityName());
        obj.setCaseDisplayKey(report.getCaseDisplayKey());
        obj.setNoOfStr(report.getNoOfStr());
        obj.setLastFiledStr(report.getLastFiledStr());
        obj.setCustIdNum(report.getCustIdNum());
        obj.setCustFax(report.getCustFax());
        obj.setProfessionType(report.getProfessionType());
        obj.setCustType(report.getCustType());
        obj.setCustGender(report.getCustGender());
        obj.setCustMobile(report.getCustMobile());
        obj.setCustEmail(report.getCustEmail());
        obj.setCustMailingAddr(report.getCustMailingAddr());
        obj.setCustResAddr(report.getCustResAddr());
        obj.setCustResAddrCity(report.getCustResAddrCity());
        obj.setCustResAddrState(report.getCustResAddrState());
        obj.setCustResAddrCountry(report.getCustResAddrCountry());
        obj.setCustDOB(report.getCustDOB());
        obj.setCustName(report.getCustName());
        obj.setCreatedOn(report.getCreatedOn());
        obj.setBeneficiaryDetails(report.getBeneficiaryDetails());
        obj.setVolumeOfTransactions(report.getVolumeOfTransactions());
        obj.setRelationshipBeginDate(report.getRelationshipBeginDate());
        obj.setPersonExecutingTxnAcctholderName(report.getPersonExecutingTxnAcctholderName());
        obj.setPersonExecutingTxnAcctholderDateOfBirth(report.getPersonExecutingTxnAcctholderDateOfBirth());
        obj.setPersonExecutingTxnAcctholderIdNumber(report.getPersonExecutingTxnAcctholderIdNumber());
        obj.setPersonExecutingTxnAcctholderPassportNumber(report.getPersonExecutingTxnAcctholderPassportNumber());
        obj.setPersonExecutingTxnAcctholderNationality(report.getPersonExecutingTxnAcctholderNationality());
        obj.setBusinessentityPlaceOfIncorporation(report.getBusinessentityPlaceOfIncorporation());
        obj.setBusinessentityDateOfIncorporation(report.getBusinessentityDateOfIncorporation());
        obj.setBusinessentityBusinessActivity(report.getBusinessentityBusinessActivity());
        obj.setACompanyDirectorsDateOfBirth(report.getACompanyDirectorsDateOfBirth());
        obj.setACompanyDirectorsIdNumber(report.getACompanyDirectorsIdNumber());
        obj.setACompanyDirectorsPassportNumber(report.getACompanyDirectorsPassportNumber());
        obj.setACompanyDirectorsNationality(report.getACompanyDirectorsNationality());
        obj.setACompanyDirectorsOccupation(report.getACompanyDirectorsOccupation());
        obj.setBCompanyDirectorsDateOfBirth(report.getBCompanyDirectorsDateOfBirth());
        obj.setBCompanyDirectorsIdNumber(report.getBCompanyDirectorsIdNumber());
        obj.setBCompanyDirectorsPassportNumber(report.getBCompanyDirectorsPassportNumber());
        obj.setBCompanyDirectorsNationality(report.getBCompanyDirectorsNationality());
        obj.setBCompanyDirectorsOccupation(report.getBCompanyDirectorsOccupation());
        obj.setCCompanyDirectorsDateOfBirth(report.getCCompanyDirectorsDateOfBirth());
        obj.setCCompanyDirectorsIdNumber(report.getCCompanyDirectorsIdNumber());
        obj.setCCompanyDirectorsPassportNumber(report.getCCompanyDirectorsPassportNumber());
        obj.setCCompanyDirectorsNationality(report.getCCompanyDirectorsNationality());
        obj.setCCompanyDirectorsOccupation(report.getCCompanyDirectorsOccupation());
        obj.setABeneficialOwnersName(report.getABeneficialOwnersName());
        obj.setABeneficialOwnersDateOfBirth(report.getABeneficialOwnersDateOfBirth());
        obj.setABeneficialOwnersIdNumber(report.getABeneficialOwnersIdNumber());

        obj.setABeneficialOwnersPassportNumber(report.getABeneficialOwnersPassportNumber());
        obj.setABeneficialOwnersNationality(report.getABeneficialOwnersNationality());
        obj.setABeneficialOwnersOccupation(report.getABeneficialOwnersOccupation());
        obj.setABeneficialOwnersDateofappointment(report.getABeneficialOwnersDateofappointment());
        obj.setABeneficialOwnersDateofresignation(report.getABeneficialOwnersDateofresignation());
        obj.setBBeneficialOwnersName(report.getBBeneficialOwnersName());
        obj.setBBeneficialOwnersDateOfBirth(report.getBBeneficialOwnersDateOfBirth());
        obj.setBBeneficialOwnersIdNumber(report.getBBeneficialOwnersIdNumber());
        obj.setBBeneficialOwnersPassportNumber(report.getBBeneficialOwnersPassportNumber());
        obj.setBBeneficialOwnersNationality(report.getBBeneficialOwnersNationality());
        obj.setBBeneficialOwnersOccupation(report.getBBeneficialOwnersOccupation());
        obj.setBBeneficialOwnersDateofappointment(report.getBBeneficialOwnersDateofappointment());
        obj.setBBeneficialOwnersDateofresignation(report.getBBeneficialOwnersDateofresignation());

        obj.setInformationAssociatedPersonsCompaniesEtc(report.getInformationAssociatedPersonsCompaniesEtc());
        obj.setWhyTheTxnWasReportedSuspicious(report.getWhyTheTxnWasReportedSuspicious());
        //obj.setSignatureOfOfficial(report.getSignatureOfOfficial());
        obj.setDesignation(report.getDesignation());
        obj.setDate(report.getDate());
        obj.setReportRelatedTo(report.getReportRelatedTo());
        obj.setAccountType(report.getAccountType());
        //setDateFields(obj,report);
          return obj;
    }


    /*private static  void setDateFields(ReportObj obj, StrReport report){
        Date date =report.getACompanyDirectorsDateOfBirth();

        if(date == null)obj.setACompanyDirectorsDateOfBirth("");
        else obj.setACompanyDirectorsDateOfBirth(date);

        date = report.getBCompanyDirectorsDateOfBirth();

        if (date == null) obj.setBCompanyDirectorsDateOfBirth("");
        else obj.setBCompanyDirectorsDateOfBirth(date);

        date = report.getCCompanyDirectorsDateOfBirth();
        if (date == null)obj.setCCompanyDirectorsDateOfBirth("");
        else obj.setCCompanyDirectorsDateOfBirth(date);

        date = report.getABeneficialOwnersDateOfBirth();
        if (date == null)obj.setABeneficialOwnersDateOfBirth("");
        else obj.setABeneficialOwnersDateOfBirth(date);

        date = report.getABeneficialOwnersDateofappointment();
        if (date == null)obj.setABeneficialOwnersDateofappointment("");
        else obj.setABeneficialOwnersDateofappointment(date);

        date = report.getABeneficialOwnersDateofresignation();
        if (date== null)obj.setABeneficialOwnersDateofresignation("");
        else obj.setABeneficialOwnersDateofresignation(date);

        date = report.getBBeneficialOwnersDateOfBirth();

        if (date == null)obj.setBBeneficialOwnersDateOfBirth("");
        else obj.setBBeneficialOwnersDateOfBirth(date);

        date = report.getBBeneficialOwnersDateofappointment();
        if (date == null)obj.setBBeneficialOwnersDateofappointment("");
        else obj.setBBeneficialOwnersDateofappointment(date);

        date = report.getBBeneficialOwnersDateofresignation();

        if (date == null)obj.setBBeneficialOwnersDateofresignation("");
        else obj.setBBeneficialOwnersDateofresignation(date);

        date = report.getDate();
        if (date== null)obj.setDate("");
        else obj.setDate(date);
    }
*/
    @JsonIgnore
    public static void mapReportObjToStrReport(ReportObj obj, StrReport report) {

        if (obj.getAccountId() != null)
            report.setAccountId(obj.getAccountId());
        if (obj.getNoTransactionToBeReported() != null)
            report.setNoTransactionToBeReported(obj.getNoTransactionToBeReported());
        if (obj.getTransactionId() != null)
            report.setTransactionId(obj.getTransactionId());
        if (obj.getRelatedAcct() != null)
            report.setRelatedAcct(obj.getRelatedAcct());
        if (obj.getAdditionalRemarks() != null)
            report.setAdditionalRemarks(obj.getAdditionalRemarks());
        if (obj.getFactName() != null)
            report.setFactName(obj.getFactName());
        if (obj.getRiskLevel() != null)
            report.setRiskLevel(obj.getRiskLevel());
        if (obj.getCaseEntityId() != null)
            report.setCaseEntityId(obj.getCaseEntityId());
        if (obj.getEntityIdType() != null)
            report.setEntityIdType(obj.getEntityIdType());
        if (obj.getIncidentScore() != null)
            report.setIncidentScore(obj.getIncidentScore());
        if (obj.getEntityName() != null)
            report.setEntityName(obj.getEntityName());
        if (obj.getIncidentClosedDate() != null)
            report.setIncidentClosedDate(obj.getIncidentClosedDate());
        if (obj.getEntityId() != null)
            report.setEntityId(obj.getEntityId());
        if (obj.getCaseEntityName() != null)
            report.setCaseEntityName(obj.getCaseEntityName());
        if (obj.getCaseDisplayKey() != null)
            report.setCaseDisplayKey(obj.getCaseDisplayKey());
        if (obj.getNoOfStr() != null)
            report.setNoOfStr(obj.getNoOfStr());
        if (obj.getLastFiledStr() != null)
            report.setLastFiledStr(obj.getLastFiledStr());
        if (obj.getCustIdNum() != null)
            report.setCustIdNum(obj.getCustIdNum());
        if (obj.getCustFax() != null)
            report.setCustFax(obj.getCustFax());
        if (obj.getProfessionType() != null)
            report.setProfessionType(obj.getProfessionType());
        if (obj.getCustType() != null)
            report.setCustType(obj.getCustType());
        if (obj.getCustGender() != null)
            report.setCustGender(obj.getCustGender());
        if (obj.getCustMobile() != null)
            report.setCustMobile(obj.getCustMobile());
        if (obj.getCustEmail() != null)
            report.setCustEmail(obj.getCustEmail());
        if (obj.getCustMailingAddr() != null)
            report.setCustMailingAddr(obj.getCustMailingAddr());
        if (obj.getCustResAddr() != null)
            report.setCustResAddr(obj.getCustResAddr());
        if (obj.getCustResAddrCity() != null)
            report.setCustResAddrCity(obj.getCustResAddrCity());
        if (obj.getCustResAddrState() != null)
            report.setCustResAddrState(obj.getCustResAddrState());
        if (obj.getCustResAddrCountry() != null)
            report.setCustResAddrCountry(obj.getCustResAddrCountry());
        if (obj.getCustDOB() != null)
            report.setCustDOB(obj.getCustDOB());
        if (obj.getCustName() != null)
            report.setCustName(obj.getCustName());
        if (obj.getCreatedOn() != null)
            report.setCreatedOn(obj.getCreatedOn());
        if(obj.getVolumeOfTransactions() != null)
            report.setVolumeOfTransactions(obj.getVolumeOfTransactions());
        if(obj.getBeneficiaryDetails() != null)
            report.setBeneficiaryDetails(obj.getBeneficiaryDetails());
        if(obj.getRelationshipBeginDate() != null)
            report.setRelationshipBeginDate(obj.getRelationshipBeginDate());
        if(obj.getPersonExecutingTxnAcctholderName() != null)
            report.setPersonExecutingTxnAcctholderName(obj.getPersonExecutingTxnAcctholderName());
        if(obj.getPersonExecutingTxnAcctholderDateOfBirth() != null)
            report.setPersonExecutingTxnAcctholderDateOfBirth(obj.getPersonExecutingTxnAcctholderDateOfBirth());
        if(obj.getPersonExecutingTxnAcctholderIdNumber() != null)
            report.setPersonExecutingTxnAcctholderIdNumber(obj.getPersonExecutingTxnAcctholderIdNumber());
        if(obj.getPersonExecutingTxnAcctholderPassportNumber() != null)
            report.setPersonExecutingTxnAcctholderPassportNumber(obj.getPersonExecutingTxnAcctholderPassportNumber());
        if(obj.getPersonExecutingTxnAcctholderNationality() != null)
            report.setPersonExecutingTxnAcctholderNationality(obj.getPersonExecutingTxnAcctholderNationality());
        if(obj.getBusinessentityPlaceOfIncorporation() != null)
            report.setBusinessentityPlaceOfIncorporation(obj.getBusinessentityPlaceOfIncorporation());
        if(obj.getBusinessentityDateOfIncorporation() != null)
            report.setBusinessentityDateOfIncorporation(obj.getBusinessentityDateOfIncorporation());
        if(obj.getBusinessentityBusinessActivity() != null)
            report.setBusinessentityBusinessActivity(obj.getBusinessentityBusinessActivity());
        if(obj.getACompanyDirectorsDateOfBirth() != null)
            report.setACompanyDirectorsDateOfBirth(obj.getACompanyDirectorsDateOfBirth());
        if(obj.getACompanyDirectorsIdNumber() != null)
            report.setACompanyDirectorsIdNumber(obj.getACompanyDirectorsIdNumber());
        if(obj.getACompanyDirectorsPassportNumber() != null)
            report.setACompanyDirectorsPassportNumber(obj.getACompanyDirectorsPassportNumber());
        if(obj.getACompanyDirectorsNationality() != null)
            report.setACompanyDirectorsNationality(obj.getACompanyDirectorsNationality());
        if(obj.getACompanyDirectorsOccupation() != null)
            report.setACompanyDirectorsOccupation(obj.getACompanyDirectorsOccupation());
        if(obj.getBCompanyDirectorsDateOfBirth() != null)
            report.setBCompanyDirectorsDateOfBirth(obj.getBCompanyDirectorsDateOfBirth());
        if(obj.getBCompanyDirectorsIdNumber() != null)
            report.setBCompanyDirectorsIdNumber(obj.getBCompanyDirectorsIdNumber());
        if(obj.getBCompanyDirectorsPassportNumber() != null)
            report.setBCompanyDirectorsPassportNumber(obj.getBCompanyDirectorsPassportNumber());
        if(obj.getBCompanyDirectorsNationality() != null)
            report.setBCompanyDirectorsNationality(obj.getBCompanyDirectorsNationality());
        if(obj.getBCompanyDirectorsOccupation() != null)
            report.setBCompanyDirectorsOccupation(obj.getBCompanyDirectorsOccupation());
        if(obj.getCCompanyDirectorsDateOfBirth() != null)
            report.setCCompanyDirectorsDateOfBirth(obj.getCCompanyDirectorsDateOfBirth());
        if(obj.getCCompanyDirectorsIdNumber() != null)
            report.setCCompanyDirectorsIdNumber(obj.getCCompanyDirectorsIdNumber());
        if(obj.getCCompanyDirectorsPassportNumber() != null)
            report.setCCompanyDirectorsPassportNumber(obj.getCCompanyDirectorsPassportNumber());
        if(obj.getCCompanyDirectorsNationality() != null)
            report.setCCompanyDirectorsNationality(obj.getCCompanyDirectorsNationality());
        if(obj.getCCompanyDirectorsOccupation() != null)
            report.setCCompanyDirectorsOccupation(obj.getCCompanyDirectorsOccupation());
        if(obj.getABeneficialOwnersName() != null)
            report.setABeneficialOwnersName(obj.getABeneficialOwnersName());
        if(obj.getABeneficialOwnersDateOfBirth() != null)
            report.setABeneficialOwnersDateOfBirth(obj.getABeneficialOwnersDateOfBirth());
        if(obj.getABeneficialOwnersIdNumber() != null)
            report.setABeneficialOwnersIdNumber(obj.getABeneficialOwnersIdNumber());
        if(obj.getABeneficialOwnersPassportNumber() != null)
            report.setABeneficialOwnersPassportNumber(obj.getABeneficialOwnersPassportNumber());
        if(obj.getABeneficialOwnersNationality() != null)
            report.setABeneficialOwnersNationality(obj.getABeneficialOwnersNationality());
        if(obj.getABeneficialOwnersOccupation() != null)
            report.setABeneficialOwnersOccupation(obj.getABeneficialOwnersOccupation());
        if(obj.getABeneficialOwnersDateofappointment() != null)
            report.setABeneficialOwnersDateofappointment(obj.getABeneficialOwnersDateofappointment());
        if(obj.getABeneficialOwnersDateofresignation() != null)
            report.setABeneficialOwnersDateofresignation(obj.getABeneficialOwnersDateofresignation());
        if(obj.getBBeneficialOwnersName() != null)
            report.setBBeneficialOwnersName(obj.getBBeneficialOwnersName());
        if(obj.getBBeneficialOwnersDateOfBirth() != null)
            report.setBBeneficialOwnersDateOfBirth(obj.getBBeneficialOwnersDateOfBirth());
        if(obj.getBBeneficialOwnersIdNumber() != null)
            report.setBBeneficialOwnersIdNumber(obj.getBBeneficialOwnersIdNumber());
        if(obj.getBBeneficialOwnersPassportNumber() != null)
            report.setBBeneficialOwnersPassportNumber(obj.getBBeneficialOwnersPassportNumber());
        if(obj.getBBeneficialOwnersNationality() != null)
            report.setBBeneficialOwnersNationality(obj.getBBeneficialOwnersNationality());
        if(obj.getBBeneficialOwnersOccupation() != null)
            report.setBBeneficialOwnersOccupation(obj.getBBeneficialOwnersOccupation());
        if(obj.getBBeneficialOwnersDateofappointment() != null)
            report.setBBeneficialOwnersDateofappointment(obj.getBBeneficialOwnersDateofappointment());
        if(obj.getBBeneficialOwnersDateofresignation() != null)
            report.setBBeneficialOwnersDateofresignation(obj.getBBeneficialOwnersDateofresignation());
        if(obj.getInformationAssociatedPersonsCompaniesEtc() != null)
            report.setInformationAssociatedPersonsCompaniesEtc(obj.getInformationAssociatedPersonsCompaniesEtc());
        if(obj.getWhyTheTxnWasReportedSuspicious() != null)
            report.setWhyTheTxnWasReportedSuspicious(obj.getWhyTheTxnWasReportedSuspicious());
      //  if(obj.getSignatureOfOfficial() != null)
        //    report.setSignatureOfOfficial(obj.getSignatureOfOfficial());
        if(obj.getDesignation() != null)
            report.setDesignation(obj.getDesignation());
        if(obj.getDate() != null)
            report.setDate(obj.getDate());
        if(obj.getReportRelatedTo() !=null)
            report.setReportRelatedTo(obj.getReportRelatedTo());
        if(obj.getAccountType() != null)
            report.setAccountType(obj.getAccountType());


    }
}
