clari5.report.db.entity.str-report {
	attributes = [
        { name = str-id, column = strId, type = "string:256", key = true }
        { name = account-id, column = accountId, type = "string:32" }
        { name = no-transaction-to-be-reported, column = noTransactionToBeReported, type = "string:32" }
        { name = transaction-id, column = transactionId, type = "string:64" }
        { name = related-acct, column = relatedAcct, type: "string:32" }
        { name = additional-remarks, column = additionalRemarks, type: "string:4000" }

        { name = volume-of-transactions, column = volumeOfTransactions, type = "string:64" }
        { name = beneficiary-details, column = beneficiaryDetails, type = "string:4000" }
        { name = fact-name, column = factName, type = "string:64" }
        { name = risk-level, column = riskLevel, type = "string:8" }
        { name = case-entity-id, column = caseEntityId, type = "string:64" }
        { name = entity-id-type, column = entityIdType, type = "string:64" }
        { name = incident-score, column = incidentScore, type = "string:8" }
        { name = entity-name, column = entityName, type = "string:64" }
        { name = incident-closed-date, column = incidentClosedDate, type = "string:64" }
        { name = entity-id, column = entityId, type = "string:64" }
        { name = case-entity-name, column = caseEntityName, type = "string:64" }
        { name = case-display-key, column = caseDisplayKey, type = "string:500" }
        { name = no-of-str, column = noOfStr, type = "number:8" }
        { name = last-filed-str, column = lastFiledStr, type = "number:32" }
        { name = relationship-begin-date, column = relationshipBeginDate, type = "string:32" }

        { name = cust-id-num, column = custIdNum, type = "string:64" }
        { name = cust-fax, column = custFax, type = "string:32" }
        { name = profession-type, column = professionType, type = "string:64" }
        #{ name = nature-of-business, column = natureOfBusiness, type = "string:64" }
        { name = cust-type, column = custType, type = "string:64" }
        { name = cust-gender, column = custGender, type = "string:16" }
        { name = cust-mobile, column = custMobile, type = "string:32" }
        { name = cust-email, column = custEmail, type = "string:64" }
        { name = cust-mailing-addr, column = custMailingAddr, type = "string:4000" }
        { name = cust-res-addr, column = custResAddr, type = "string:4000" }
        { name = cust-res-addr-city, column = custResAddrCity, type = "string:256" }
        { name = cust-res-addr-state, column = custResAddrState, type = "string:256" }
        { name = cust-res-addr-country, column = custResAddrCountry, type = "string:256" }
        #{ name = cust-nationality, column = nationality, type = "string:256" }
        { name = cust-d-o-b, column = custDOB, type = "string:64" }
        { name = cust-name, column = custName, type = "string:256" }
        { name = createdOn, column = createdOn, type = "string:64" }

        #Added for STR_SEYCHELLES
	{ name = report-related-to, column = reportRelatedTo, type = "string:400" }
	{ name = account-type, column = accountType, type = "string:400" }

	{ name = person-executing-txn-acctholder-name, column = personExecutingTxnAcctholderName, type = "string:400" }
	{ name = person-executing-txn-acctholder-date-of-birth, column = personExecutingTxnAcctholderDateOfBirth,type = "string:64" }
	{ name = person-executing-txn-acctholder-id-number, column = personExecutingTxnAcctholderIdNumber, type ="string:400" }
	{ name = person-executing-txn-acctholder-passport-number, column = personExecutingTxnAcctholderPassportNumber, type= "string:400" }
	{ name = person-executing-txn-acctholder-nationality, column = personExecutingTxnAcctholderNationality,type = "string:400" }
	#identificationofbusinessentity
	{ name = businessentity-place-of-incorporation, column = businessentityPlaceOfIncorporation, type = "string:400" }
	{ name = businessentity-date-of-incorporation, column = businessentityDateOfIncorporation, type = "string:64"}
	{ name = businessentity-business-activity, column = businessentityBusinessActivity, type = "string:400"}
	#identificationofcompanydirectors
	{ name = a-company-directors-date-of-birth, column = aCompanyDirectorsDateOfBirth, type = "string:64"}
	{ name = a-company-directors-id-number, column = aCompanyDirectorsIdNumber, type ="string:400" }
	{ name = a-company-directors-passport-number, column = aCompanyDirectorsPassportNumber, type = "string:400" }
	{ name = a-company-directors-nationality, column = aCompanyDirectorsNationality,type = "string:400" }
	{ name = a-company-directors-occupation, column = aCompanyDirectorsOccupation,type = "string:400" }
	#b
	{ name = b-company-directors-date-of-birth, column = bCompanyDirectorsDateOfBirth, type = "string:64"}
	{ name = b-company-directors-id-number, column = bCompanyDirectorsIdNumber, type ="string:400" }
	{ name = b-company-directors-passport-number, column = bCompanyDirectorsPassportNumber, type = "string:400" }
	{ name = b-company-directors-nationality, column = bCompanyDirectorsNationality,type = "string:400" }
	{ name = b-company-directors-occupation, column = bCompanyDirectorsOccupation,type = "string:400" }
	#c
	{ name = c-company-directors-date-of-birth, column = cCompanyDirectorsDateOfBirth, type = "string:64"}
	{ name = c-company-directors-id-number, column = cCompanyDirectorsIdNumber, type ="string:400" }
	{ name = c-company-directors-passport-number, column = cCompanyDirectorsPassportNumber, type = "string:400" }
	{ name = c-company-directors-nationality, column = cCompanyDirectorsNationality,type = "string:400" }
	{ name = c-company-directors-occupation, column = cCompanyDirectorsOccupation,type = "string:400" }

	#identificationofbeneficialowners
	{ name = a-beneficial-owners-name, column = aBeneficialOwnersName, type = "string:400" }
	{ name = a-beneficial-owners-date-of-birth, column = aBeneficialOwnersDateOfBirth, type = "string:64"}
	{ name = a-beneficial-owners-id-number, column = aBeneficialOwnersIdNumber, type ="string:400" }
	{ name = a-beneficial-owners-passport-number, column = aBeneficialOwnersPassportNumber, type = "string:400" }
	{ name = a-beneficial-owners-nationality, column = aBeneficialOwnersNationality,type = "string:400" }
	{ name = a-beneficial-owners-occupation, column = aBeneficialOwnersOccupation,type = "string:400" }
	{ name = a-beneficial-owners-dateofappointment, column = aBeneficialOwnersDateofappointment,type = "string:64" }
	{ name = a-beneficial-owners-dateofresignation, column = aBeneficialOwnersDateofresignation,type = "string:64" }
	#b
	{ name = b-beneficial-owners-name, column = bBeneficialOwnersName, type = "string:400" }
	{ name = b-beneficial-owners-date-of-birth, column = bBeneficialOwnersDateOfBirth, type = "string:64"}
	{ name = b-beneficial-owners-id-number, column = bBeneficialOwnersIdNumber, type ="string:400" }
	{ name = b-beneficial-owners-passport-number, column = bBeneficialOwnersPassportNumber, type = "string:400" }
	{ name = b-beneficial-owners-nationality, column = bBeneficialOwnersNationality,type = "string:400" }
	{ name = b-beneficial-owners-occupation, column = bBeneficialOwnersOccupation,type = "string:400" }
	{ name = b-beneficial-owners-dateofappointment, column = bBeneficialOwnersDateofappointment,type = "string:64" }
	{ name = b-beneficial-owners-dateofresignation, column = bBeneficialOwnersDateofresignation,type = "string:64" }
	
	{ name = information-associated-persons-companies-etc, column = informationAssociatedPersonsCompaniesEtc, type = "string:400" }
	{ name = why-the-txn-was-reported-suspicious, column = whyTheTxnWasReportedSuspicious, type = "string:400" }
	{ name =designation, column =designation ,type = "string:400" }
	{name =date ,column=date,type="string:64"}

	]
}
