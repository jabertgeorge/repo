package clari5.aml.reports.rbi.jasperUtil.str.core;

import clari5.aml.reports.rbi.jasperUtil.str.mappers.CustomStrReportMapper;
import clari5.aml.reports.rbi.jasperUtil.str.model.FormData;
import clari5.aml.reports.rbi.jasperUtil.str.model.ReportObj;
import clari5.aml.reports.rbi.jasperUtil.str.model.STROverview;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.report.db.StrReport;
import clari5.report.db.StrReportKey;
import clari5.report.db.mappers.StrReportMapper;
import com.utils.form.core.FormMetaInfoManager;
import com.utils.form.model.FormMetaInfo;
import org.apache.ibatis.session.RowBounds;
import java.io.IOException;
import java.util.*;

/**
 * Created by shashank on 4/24/18.
 */

public class DefaultSTRManager extends STRManager {

    @Override
    public Map<String, LinkedHashSet<FormMetaInfo>> fetchSTRForm(RDBMSSession session, String strId) throws IOException, RdbmsException,
            IllegalAccessException, ClassNotFoundException, InstantiationException {
        StrReportKey key = new StrReportKey(session);
        key.setStrId(strId);
        StrReport report = key.select();
        ReportObj obj = ReportObj.mapStrReportToReportObj(report);
        CxJson json = CxJson.parse(CxJson.obj2json(obj));
        LinkedHashSet<FormMetaInfo> metaInfoSet = fetchReportConfig(session);
        metaInfoSet.forEach(metaInfo -> metaInfo.setValue(json.getString(metaInfo.getTag(), "")));
        return (FormMetaInfoManager.getInstance((new Hocon())).fetchForm(metaInfoSet));
    }

    @Override
    public int updateSTRData(RDBMSSession session, FormData formData) throws RdbmsException, IOException, IllegalAccessException,
            ClassNotFoundException, InstantiationException {
        CxJson json = FormMetaInfoManager.getInstance((new Hocon())).extractFormPostSubmit(formData.getData());
        ReportObj obj = CxJson.json2obj(json.toJson(), ReportObj.class);
        StrReportKey key = new StrReportKey(session);
        key.setStrId(obj.getStrId());
        StrReport report = key.select();
        ReportObj.mapReportObjToStrReport(obj, report);
        return report.update(session);
    }

    @Override
    public long getReportCount(RDBMSSession session) {
        CustomStrReportMapper reportMapper = session.getMapper(CustomStrReportMapper.class);
        return reportMapper.getReportCount();
    }

    @Override
    public List<STROverview> fetchSTRList(RDBMSSession session, int pageIndex, int rowsPerPage) {
        StrReportMapper mapper = session.getMapper(StrReportMapper.class);
        int rowsOffSet = ((pageIndex - 1) * rowsPerPage);
        List<StrReport> reports = mapper.getAllRecords(new RowBounds(rowsOffSet, rowsPerPage));
        List<STROverview> strList = new LinkedList<>();
        for (StrReport report : reports) {
            STROverview overview = new STROverview();
            overview.setStrId(report.getStrId());
            overview.setTransactionId(report.getTransactionId());
            overview.setAccountId(report.getAccountId());
            //overview.setDescription(report.getSummaryOfSuspicion());
            overview.setCreatedOn(report.getCreatedOn().toString());
            strList.add(overview);
        }
        return strList;
    }
}
