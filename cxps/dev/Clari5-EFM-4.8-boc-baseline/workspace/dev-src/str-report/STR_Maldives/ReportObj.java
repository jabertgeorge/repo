package clari5.aml.reports.rbi.jasperUtil.str.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import clari5.report.db.StrReport;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.zip.DeflaterOutputStream;

/**
 * Created by shashank on 5/2/18.
 */

public class ReportObj {
    private String strId = null;
    private String accountId = null;
    private String noTransactionToBeReported = null;
    private String transactionId = null;
    private String relatedAcct = null;
    private String additionalRemarks = null;
    private String background = null;
    private String eventName = null;
    private String eventId = null;
    private String factName = null;
    private String riskLevel = null;
    private String caseEntityId = null;
    private String entityIdType = null;
    private String incidentScore = null;
    private String entityName = null;
    private String incidentClosedDate = null;
    private String entityId = null;
    private String caseEntityName = null;
    private String caseDisplayKey = null;
    private Integer noOfStr = null;
    private String lastFiledStr = null;
    private String custIdNum = null;
    private String custFax = null;
    private String professionType = null;
    private String natureOfBusiness = null;
    private String custType = null;
    private String custGender = null;
    private String custMobile = null;
    private String custEmail = null;
    private String custMailingAddr = null;
    private String custResAddr = null;
    private String custResAddrCity = null;
    private String custResAddrState = null;
    private String custResAddrCountry = null;
    private String custDOB = null;
    private String custName = null;
    private String createdOn = null;
    private String beneficiaryDetails = null;
    private String volumeOfTransactions = null;
    private String relationshipBeginDate = null;
    private String personConductingTransactionName = null;
    private String personConductingTransactionIdno = null;
    private String personConductingTransactionIdType = null;
    private String personConductingTransactionAddress = null;
    private String idIssuer = null;
    private String idIssuedDate = null;
    private String idExpiryDate = null;
    private String capacityInWhichTxnisBeingConducted = null;
    private String onBehalf=null;
    private String transactionRecipient = null;
    private String transactionRecipientOthers = null;
    private String transactionRecipientIdIssuer = null;
    private String transactionRecipientIssuedDate = null;
    private String transactionRecipientExpiryDate = null;
    private String isTheSuspectedPartyAffiliatedWithReportingEntityinAnyway = null;
    private String ifYesSpecifyRelationship = null;
    private String relationshipStatusAfterReporting =null;
    private String buissnessAffiliationRemarks = null;
    private String typeOfReport = null;
    private String previousStrReferenceNumber = null;
    private String previousStrReferenceRemarks = null;
    private String numberOfDocumentsAttached = null;
    private String pleaseProvideList = null;
    private String suspectTypeReportedIndividual=null;
    private String suspectTypeReportedLegalEntity= null;



    private String previousStrReferenceDate = null;
    private String typesOfTransactions = null;
    private String typesOfTransactionsOthers = null;
    private String amountUnknown = null;
    private String reasonForSuspicion = null;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getNoTransactionToBeReported() {
        return noTransactionToBeReported;
    }

    public void setNoTransactionToBeReported(String noTransactionToBeReported) {
        this.noTransactionToBeReported = noTransactionToBeReported;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRelatedAcct() {
        return relatedAcct;
    }

    public void setRelatedAcct(String relatedAcct) {
        this.relatedAcct = relatedAcct;
    }

    public String getAdditionalRemarks() {
        return additionalRemarks;
    }

    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getFactName() {
        return factName;
    }

    public void setFactName(String factName) {
        this.factName = factName;
    }

    public String getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }

    public String getCaseEntityId() {
        return caseEntityId;
    }

    public void setCaseEntityId(String caseEntityId) {
        this.caseEntityId = caseEntityId;
    }

    public String getEntityIdType() {
        return entityIdType;
    }

    public void setEntityIdType(String entityIdType) {
        this.entityIdType = entityIdType;
    }

    public String getIncidentScore() {
        return incidentScore;
    }

    public void setIncidentScore(String incidentScore) {
        this.incidentScore = incidentScore;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getIncidentClosedDate() {
        return incidentClosedDate;
    }

    public void setIncidentClosedDate(String incidentClosedDate) {
        this.incidentClosedDate = incidentClosedDate;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getCaseEntityName() {
        return caseEntityName;
    }

    public void setCaseEntityName(String caseEntityName) {
        this.caseEntityName = caseEntityName;
    }

    public String getCaseDisplayKey() {
        return caseDisplayKey;
    }

    public void setCaseDisplayKey(String caseDisplayKey) {
        this.caseDisplayKey = caseDisplayKey;
    }

    public Integer getNoOfStr() {
        return noOfStr;
    }

    public void setNoOfStr(Integer noOfStr) {
        this.noOfStr = noOfStr;
    }

    public String getLastFiledStr() {
        return lastFiledStr;
    }

    public void setLastFiledStr(String lastFiledStr) {
        this.lastFiledStr = lastFiledStr;
    }

    public String getCustIdNum() {
        return custIdNum;
    }

    public void setCustIdNum(String custIdNum) {
        this.custIdNum = custIdNum;
    }

    public String getCustFax() {
        return custFax;
    }

    public void setCustFax(String custFax) {
        this.custFax = custFax;
    }

    public String getProfessionType() {
        return professionType;
    }

    public void setProfessionType(String professionType) {
        this.professionType = professionType;
    }

    public String getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(String natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getCustGender() {
        return custGender;
    }

    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMailingAddr() {
        return custMailingAddr;
    }

    public void setCustMailingAddr(String custMailingAddr) {
        this.custMailingAddr = custMailingAddr;
    }

    public String getCustResAddr() {
        return custResAddr;
    }

    public void setCustResAddr(String custResAddr) {
        this.custResAddr = custResAddr;
    }

    public String getCustResAddrCity() {
        return custResAddrCity;
    }

    public void setCustResAddrCity(String custResAddrCity) {
        this.custResAddrCity = custResAddrCity;
    }

    public String getCustResAddrState() {
        return custResAddrState;
    }

    public void setCustResAddrState(String custResAddrState) {
        this.custResAddrState = custResAddrState;
    }

    public String getCustResAddrCountry() {
        return custResAddrCountry;
    }

    public void setCustResAddrCountry(String custResAddrCountry) {
        this.custResAddrCountry = custResAddrCountry;
    }

    public String getCustDOB() {
        return custDOB;
    }

    public void setCustDOB(String custDOB) {
        this.custDOB = custDOB;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getBeneficiaryDetails() {
        return beneficiaryDetails;
    }

    public void setBeneficiaryDetails(String beneficiaryDetails) {
        this.beneficiaryDetails = beneficiaryDetails;
    }

    public String getVolumeOfTransactions() {
        return volumeOfTransactions;
    }

    public void setVolumeOfTransactions(String volumeOfTransactions) {
        this.volumeOfTransactions = volumeOfTransactions;
    }

    public String getRelationshipBeginDate() {
        return relationshipBeginDate;
    }

    public void setRelationshipBeginDate(String relationshipBeginDate) {
        this.relationshipBeginDate = relationshipBeginDate;
    }
    public String getPersonConductingTransactionName() {
        return personConductingTransactionName;
    }

    public void setPersonConductingTransactionName(String personConductingTransactionName) {
        this.personConductingTransactionName = personConductingTransactionName;
    }

    public String getPersonConductingTransactionIdno() {
        return personConductingTransactionIdno;
    }

    public void setPersonConductingTransactionIdno(String personConductingTransactionIdno) {
        this.personConductingTransactionIdno = personConductingTransactionIdno;
    }

    public String getPersonConductingTransactionIdType() {
        return personConductingTransactionIdType;
    }

    public void setPersonConductingTransactionIdType(String personConductingTransactionIdType) {
        this.personConductingTransactionIdType = personConductingTransactionIdType;
    }

    public String getPersonConductingTransactionAddress() {
        return personConductingTransactionAddress;
    }

    public void setPersonConductingTransactionAddress(String personConductingTransactionAddress) {
        this.personConductingTransactionAddress = personConductingTransactionAddress;
    }

    public String getIdIssuer() {
        return idIssuer;
    }

    public void setIdIssuer(String idIssuer) {
        this.idIssuer = idIssuer;
    }

    public String getIdIssuedDate() {
        return idIssuedDate;
    }

    public void setIdIssuedDate(String idIssuedDate) {
        this.idIssuedDate = idIssuedDate;
    }

    public String getIdExpiryDate() {
        return idExpiryDate;
    }

    public void setIdExpiryDate(String idExpiryDate) {
        this.idExpiryDate = idExpiryDate;
    }

    public String getCapacityInWhichTxnisBeingConducted() {
        return capacityInWhichTxnisBeingConducted;
    }

    public void setCapacityInWhichTxnisBeingConducted(String capacityInWhichTxnisBeingConducted) {
        this.capacityInWhichTxnisBeingConducted = capacityInWhichTxnisBeingConducted;
    }

    public String getOnBehalf() {
        return onBehalf;
    }

    public void setOnBehalf(String onBehalf) {
        this.onBehalf = onBehalf;
    }

    public String getTransactionRecipient() {
        return transactionRecipient;
    }

    public void setTransactionRecipient(String transactionRecipient) {
        this.transactionRecipient = transactionRecipient;
    }

    public String getTransactionRecipientOthers() {
        return transactionRecipientOthers;
    }

    public void setTransactionRecipientOthers(String transactionRecipientOthers) {
        this.transactionRecipientOthers = transactionRecipientOthers;
    }

    public String getTransactionRecipientIdIssuer() {
        return transactionRecipientIdIssuer;
    }

    public void setTransactionRecipientIdIssuer(String transactionRecipientIdIssuer) {
        this.transactionRecipientIdIssuer = transactionRecipientIdIssuer;
    }

    public String getTransactionRecipientIssuedDate() {
        return transactionRecipientIssuedDate;
    }

    public void setTransactionRecipientIssuedDate(String transactionRecipientIssuedDate) {
        this.transactionRecipientIssuedDate = transactionRecipientIssuedDate;
    }

    public String getTransactionRecipientExpiryDate() {
        return transactionRecipientExpiryDate;
    }

    public void setTransactionRecipientExpiryDate(String transactionRecipientExpiryDate) {
        this.transactionRecipientExpiryDate = transactionRecipientExpiryDate;
    }

    public String getIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway() {
        return isTheSuspectedPartyAffiliatedWithReportingEntityinAnyway;
    }

    public void setIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway(String isTheSuspectedPartyAffiliatedWithReportingEntityinAnyway) {
        this.isTheSuspectedPartyAffiliatedWithReportingEntityinAnyway = isTheSuspectedPartyAffiliatedWithReportingEntityinAnyway;
    }

    public String getIfYesSpecifyRelationship() {
        return ifYesSpecifyRelationship;
    }

    public void setIfYesSpecifyRelationship(String ifYesSpecifyRelationship) {
        this.ifYesSpecifyRelationship = ifYesSpecifyRelationship;
    }

   public String getRelationshipStatusAfterReporting() {
        return relationshipStatusAfterReporting;
    }

    public void setRelationshipStatusAfterReporting(String relationshipStatusAfterReporting) {
        this.relationshipStatusAfterReporting = relationshipStatusAfterReporting;
    }

    public String getBuissnessAffiliationRemarks() {
        return buissnessAffiliationRemarks;
    }

    public void setBuissnessAffiliationRemarks(String buissnessAffiliationRemarks) {
        this.buissnessAffiliationRemarks = buissnessAffiliationRemarks;
    }
    public String getTypeOfReport() {
        return typeOfReport;
    }

    public void setTypeOfReport(String typeOfReport) {
        this.typeOfReport = typeOfReport;
    }

    public String getPreviousStrReferenceNumber() {
        return previousStrReferenceNumber;
    }

    public void setPreviousStrReferenceNumber(String previousStrReferenceNumber) {
        this.previousStrReferenceNumber = previousStrReferenceNumber;
    }

    public String getPreviousStrReferenceRemarks() {
        return previousStrReferenceRemarks;
    }

    public void setPreviousStrReferenceRemarks(String previousStrReferenceRemarks) {
        this.previousStrReferenceRemarks = previousStrReferenceRemarks;
    }

    public String getNumberOfDocumentsAttached() {
        return numberOfDocumentsAttached;
    }

    public void setNumberOfDocumentsAttached(String numberOfDocumentsAttached) {
        this.numberOfDocumentsAttached = numberOfDocumentsAttached;
    }

    public String getPleaseProvideList() {
        return pleaseProvideList;
    }

    public void setPleaseProvideList(String pleaseProvideList) {
        this.pleaseProvideList = pleaseProvideList;
    }

    public String getSuspectTypeReportedIndividual() {  return suspectTypeReportedIndividual;  }

    public void setSuspectTypeReportedIndividual(String suspectTypeReportedIndividual) { this.suspectTypeReportedIndividual = suspectTypeReportedIndividual; }

    public String getSuspectTypeReportedLegalEntity() {  return suspectTypeReportedLegalEntity; }

    public void setSuspectTypeReportedLegalEntity(String suspectTypeReportedLegalEntity) { this.suspectTypeReportedLegalEntity = suspectTypeReportedLegalEntity; }

    public String getPreviousStrReferenceDate() {
        return previousStrReferenceDate;
    }

    public void setPreviousStrReferenceDate(String previousStrReferenceDate) {
        this.previousStrReferenceDate = previousStrReferenceDate;
    }

    public String getTypesOfTransactions() {
        return typesOfTransactions;
    }

    public void setTypesOfTransactions(String typesOfTransactions) {
        this.typesOfTransactions = typesOfTransactions;
    }

    public String getTypesOfTransactionsOthers() {
        return typesOfTransactionsOthers;
    }

    public void setTypesOfTransactionsOthers(String typesOfTransactionsOthers) {
        this.typesOfTransactionsOthers = typesOfTransactionsOthers;
    }

    public String getAmountUnknown() {
        return amountUnknown;
    }

    public void setAmountUnknown(String amountUnknown) {
        this.amountUnknown = amountUnknown;
    }

    public String getReasonForSuspicion() {
        return reasonForSuspicion;
    }

    public void setReasonForSuspicion(String reasonForSuspicion) {
        this.reasonForSuspicion = reasonForSuspicion;
    }



    @JsonIgnore
    public static ReportObj mapStrReportToReportObj(StrReport report) {
        ReportObj obj = new ReportObj();
        obj.setStrId(report.getStrId());
        obj.setAccountId(report.getAccountId());
        obj.setNoTransactionToBeReported(report.getNoTransactionToBeReported());
        obj.setTransactionId(report.getTransactionId());
        obj.setRelatedAcct(report.getRelatedAcct());
        obj.setAdditionalRemarks(report.getAdditionalRemarks());
        obj.setFactName(report.getFactName());
        obj.setRiskLevel(report.getRiskLevel());
        obj.setCaseEntityId(report.getCaseEntityId());
        obj.setEntityIdType(report.getEntityIdType());
        obj.setIncidentScore(report.getIncidentScore());
        obj.setEntityName(report.getEntityName());
        obj.setIncidentClosedDate(report.getIncidentClosedDate());
        obj.setEntityId(report.getEntityId());
        obj.setCaseEntityName(report.getCaseEntityName());
        obj.setCaseDisplayKey(report.getCaseDisplayKey());
        obj.setNoOfStr(report.getNoOfStr());
        obj.setLastFiledStr(report.getLastFiledStr());
        obj.setCustIdNum(report.getCustIdNum());
        obj.setCustFax(report.getCustFax());
        obj.setProfessionType(report.getProfessionType());
        obj.setNatureOfBusiness(report.getNatureOfBusiness());
        obj.setCustType(report.getCustType());
        obj.setCustGender(report.getCustGender());
        obj.setCustMobile(report.getCustMobile());
        obj.setCustEmail(report.getCustEmail());
        obj.setCustMailingAddr(report.getCustMailingAddr());
        obj.setCustResAddr(report.getCustResAddr());
        obj.setCustResAddrCity(report.getCustResAddrCity());
        obj.setCustResAddrState(report.getCustResAddrState());
        obj.setCustResAddrCountry(report.getCustResAddrCountry());
        obj.setCustDOB(report.getCustDOB());
        obj.setCustName(report.getCustName());
        obj.setCreatedOn(report.getCreatedOn());
        obj.setBeneficiaryDetails(report.getBeneficiaryDetails());
        obj.setVolumeOfTransactions(report.getVolumeOfTransactions());
        obj.setPersonConductingTransactionName(report.getPersonConductingTransactionName());
        obj.setPersonConductingTransactionIdno(report.getPersonConductingTransactionIdno());
        obj.setPersonConductingTransactionIdType(report.getPersonConductingTransactionIdType());
        obj.setPersonConductingTransactionAddress(report.getPersonConductingTransactionAddress());
        obj.setIdIssuer(report.getIdIssuer());
        obj.setIdIssuedDate(report.getIdIssuedDate());
        obj.setIdExpiryDate(report.getIdExpiryDate());
        obj.setCapacityInWhichTxnisBeingConducted(report.getCapacityInWhichTxnisBeingConducted());
        obj.setOnBehalf(report.getOnBehalf());
        obj.setTransactionRecipient(report.getTransactionRecipient());
        obj.setTransactionRecipientOthers(report.getTransactionRecipientOthers());
        obj.setTransactionRecipientIdIssuer(report.getTransactionRecipientIdIssuer());
        obj.setTransactionRecipientIssuedDate(report.getTransactionRecipientIssuedDate());
        obj.setTransactionRecipientExpiryDate(report.getTransactionRecipientExpiryDate());
        obj.setIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway(report.getIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway());
        obj.setIfYesSpecifyRelationship(report.getIfYesSpecifyRelationship());
        obj.setRelationshipStatusAfterReporting(report.getRelationshipStatusAfterReporting());
        obj.setBuissnessAffiliationRemarks(report.getBuissnessAffiliationRemarks());
        obj.setRelationshipBeginDate(report.getRelationshipBeginDate());
        obj.setTypeOfReport(report.getTypeOfReport());
        obj.setPreviousStrReferenceNumber(report.getPreviousStrReferenceNumber());
        obj.setPreviousStrReferenceRemarks(report.getPreviousStrReferenceRemarks());
        obj.setNumberOfDocumentsAttached(report.getNumberOfDocumentsAttached());
        obj.setPleaseProvideList(report.getPleaseProvideList());
        obj.setSuspectTypeReportedIndividual(report.getSuspectTypeReportedIndividual());
        obj.setSuspectTypeReportedLegalEntity(report.getSuspectTypeReportedLegalEntity());
        obj.setPreviousStrReferenceDate(report.getPreviousStrReferenceDate());
        obj.setTypesOfTransactions(report.getTypesOfTransactions());
        obj.setAmountUnknown(report.getAmountUnknown());
        obj.setReasonForSuspicion(report.getReasonForSuspicion());
        return obj;
    }

    @JsonIgnore
    public static void mapReportObjToStrReport(ReportObj obj, StrReport report) {

        if (obj.getAccountId() != null)
            report.setAccountId(obj.getAccountId());
        if (obj.getNoTransactionToBeReported() != null)
            report.setNoTransactionToBeReported(obj.getNoTransactionToBeReported());
        if (obj.getTransactionId() != null)
            report.setTransactionId(obj.getTransactionId());
        if (obj.getRelatedAcct() != null)
            report.setRelatedAcct(obj.getRelatedAcct());
        if (obj.getAdditionalRemarks() != null)
            report.setAdditionalRemarks(obj.getAdditionalRemarks());
        if (obj.getFactName() != null)
            report.setFactName(obj.getFactName());
        if (obj.getRiskLevel() != null)
            report.setRiskLevel(obj.getRiskLevel());
        if (obj.getCaseEntityId() != null)
            report.setCaseEntityId(obj.getCaseEntityId());
        if (obj.getEntityIdType() != null)
            report.setEntityIdType(obj.getEntityIdType());
        if (obj.getIncidentScore() != null)
            report.setIncidentScore(obj.getIncidentScore());
        if (obj.getEntityName() != null)
            report.setEntityName(obj.getEntityName());
        if (obj.getIncidentClosedDate() != null)
            report.setIncidentClosedDate(obj.getIncidentClosedDate());
        if (obj.getEntityId() != null)
            report.setEntityId(obj.getEntityId());
        if (obj.getCaseEntityName() != null)
            report.setCaseEntityName(obj.getCaseEntityName());
        if (obj.getCaseDisplayKey() != null)
            report.setCaseDisplayKey(obj.getCaseDisplayKey());
        if (obj.getNoOfStr() != null)
            report.setNoOfStr(obj.getNoOfStr());
        if (obj.getLastFiledStr() != null)
            report.setLastFiledStr(obj.getLastFiledStr());
        if (obj.getCustIdNum() != null)
            report.setCustIdNum(obj.getCustIdNum());
        if (obj.getCustFax() != null)
            report.setCustFax(obj.getCustFax());
        if (obj.getProfessionType() != null)
            report.setProfessionType(obj.getProfessionType());
        if (obj.getNatureOfBusiness() != null)
            report.setNatureOfBusiness(obj.getNatureOfBusiness());
        if (obj.getCustType() != null)
            report.setCustType(obj.getCustType());
        if (obj.getCustGender() != null)
            report.setCustGender(obj.getCustGender());
        if (obj.getCustMobile() != null)
            report.setCustMobile(obj.getCustMobile());
        if (obj.getCustEmail() != null)
            report.setCustEmail(obj.getCustEmail());
        if (obj.getCustMailingAddr() != null)
            report.setCustMailingAddr(obj.getCustMailingAddr());
        if (obj.getCustResAddr() != null)
            report.setCustResAddr(obj.getCustResAddr());
        if (obj.getCustResAddrCity() != null)
            report.setCustResAddrCity(obj.getCustResAddrCity());
        if (obj.getCustResAddrState() != null)
            report.setCustResAddrState(obj.getCustResAddrState());
        if (obj.getCustResAddrCountry() != null)
            report.setCustResAddrCountry(obj.getCustResAddrCountry());
        if (obj.getCustDOB() != null)
            report.setCustDOB(obj.getCustDOB());
        if (obj.getCustName() != null)
            report.setCustName(obj.getCustName());
        if (obj.getCreatedOn() != null)
            report.setCreatedOn(obj.getCreatedOn());
        if(obj.getVolumeOfTransactions() != null)
            report.setVolumeOfTransactions(obj.getVolumeOfTransactions());
        if(obj.getBeneficiaryDetails() != null)
            report.setBeneficiaryDetails(obj.getBeneficiaryDetails());
        if(obj.getPersonConductingTransactionName() != null )
            report.setPersonConductingTransactionName(obj.getPersonConductingTransactionName());
        if(obj.getPersonConductingTransactionIdno() != null )
            report.setPersonConductingTransactionIdno(obj.getPersonConductingTransactionIdno());
        if(obj.getPersonConductingTransactionIdType() != null )
            report.setPersonConductingTransactionIdType(obj.getPersonConductingTransactionIdType());
        if(obj.getPersonConductingTransactionAddress() != null )
            report.setPersonConductingTransactionAddress(obj.getPersonConductingTransactionAddress());
        if(obj.getIdIssuer() != null )
            report.setIdIssuer(obj.getIdIssuer());
        if(obj.getIdIssuedDate() != null)
            report.setIdIssuedDate(obj.getIdIssuedDate());
        if(obj.getIdExpiryDate() != null)
            report.setIdExpiryDate(obj.getIdExpiryDate());
        if(obj.getCapacityInWhichTxnisBeingConducted() != null)
            report.setCapacityInWhichTxnisBeingConducted(obj.getCapacityInWhichTxnisBeingConducted());
        if (obj.getOnBehalf() != null)
            report.setOnBehalf(obj.getOnBehalf());
        if (obj.getTransactionRecipient() != null)
            report.setTransactionRecipient(obj.getTransactionRecipient());
        if(obj.getTransactionRecipientOthers() != null)
            report.setTransactionRecipientOthers(obj.getTransactionRecipientOthers());
        if(obj.getTransactionRecipientIdIssuer() != null)
            report.setTransactionRecipientIdIssuer(obj.getTransactionRecipientIdIssuer());
        if(obj.getTransactionRecipientIssuedDate() != null )
            report.setTransactionRecipientIssuedDate(obj.getTransactionRecipientIssuedDate());
        if(obj.getTransactionRecipientExpiryDate() != null )
            report.setTransactionRecipientExpiryDate(obj.getTransactionRecipientExpiryDate());
        if(obj.getIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway() != null )
            report.setIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway(obj.getIsTheSuspectedPartyAffiliatedWithReportingEntityinAnyway());
        if(obj.getIfYesSpecifyRelationship() != null )
            report.setIfYesSpecifyRelationship(obj.getIfYesSpecifyRelationship());
	if(obj.getIfYesSpecifyRelationship() != null )
            report.setIfYesSpecifyRelationship(obj.getIfYesSpecifyRelationship());
        if(obj.getRelationshipStatusAfterReporting() != null)
            report.setRelationshipStatusAfterReporting(obj.getRelationshipStatusAfterReporting());
        if(obj.getRelationshipBeginDate() != null)
            report.setRelationshipBeginDate(obj.getRelationshipBeginDate());
        if(obj.getTypeOfReport() != null )
            report.setTypeOfReport(obj.getTypeOfReport());
        if(obj.getPreviousStrReferenceNumber() != null )
            report.setPreviousStrReferenceNumber(obj.getPreviousStrReferenceNumber());
        if(obj.getPreviousStrReferenceRemarks() != null )
            report.setPreviousStrReferenceRemarks(obj.getPreviousStrReferenceRemarks());
        if(obj.getNumberOfDocumentsAttached() != null )
            report.setNumberOfDocumentsAttached(obj.getNumberOfDocumentsAttached());
        if(obj.getPleaseProvideList() != null )
            report.setPleaseProvideList(obj.getPleaseProvideList());
        if(obj.getSuspectTypeReportedIndividual()!= null)
            report.setSuspectTypeReportedIndividual(obj.getSuspectTypeReportedIndividual());
        if(obj.getSuspectTypeReportedLegalEntity()!= null)
            report.setSuspectTypeReportedLegalEntity(obj.getSuspectTypeReportedLegalEntity());
        if(obj.getPreviousStrReferenceDate() != null )
            report.setPreviousStrReferenceDate(obj.getPreviousStrReferenceDate());
        if(obj.getTypesOfTransactions() != null )
            report.setTypesOfTransactions(obj.getTypesOfTransactions());
        if(obj.getAmountUnknown() != null )
            report.setAmountUnknown(obj.getAmountUnknown());
        if(obj.getReasonForSuspicion() != null )
            report.setReasonForSuspicion(obj.getReasonForSuspicion());
        }
}
