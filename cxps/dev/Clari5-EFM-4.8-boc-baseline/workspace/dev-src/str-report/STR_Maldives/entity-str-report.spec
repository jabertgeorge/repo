clari5.report.db.entity.str-report {
	attributes = [
        { name = str-id, column = strId, type = "string:256", key = true }
        { name = account-id, column = accountId, type = "string:32" }
        { name = no-transaction-to-be-reported, column = noTransactionToBeReported, type = "string:32" }
        { name = transaction-id, column = transactionId, type = "string:64" }
        { name = related-acct, column = relatedAcct, type: "string:32" }
        { name = additional-remarks, column = additionalRemarks, type: "string:4000" }

        { name = volume-of-transactions, column = volumeOfTransactions, type = "string:64" }
        { name = beneficiary-details, column = beneficiaryDetails, type = "string:4000" }
        { name = fact-name, column = factName, type = "string:64" }
        { name = risk-level, column = riskLevel, type = "string:8" }
        { name = case-entity-id, column = caseEntityId, type = "string:64" }
        { name = entity-id-type, column = entityIdType, type = "string:64" }
        { name = incident-score, column = incidentScore, type = "string:8" }
        { name = entity-name, column = entityName, type = "string:64" }
        { name = incident-closed-date, column = incidentClosedDate, type = "string:256" }
        { name = entity-id, column = entityId, type = "string:64" }
        { name = case-entity-name, column = caseEntityName, type = "string:64" }
        { name = case-display-key, column = caseDisplayKey, type = "string:500" }
        { name = no-of-str, column = noOfStr, type = "number:8" }
        { name = last-filed-str, column = lastFiledStr, type = "string:20" }
        { name = relationship-begin-date, column = relationshipBeginDate, type = "string:32" }

        { name = cust-id-num, column = custIdNum, type = "string:64" }
        { name = cust-fax, column = custFax, type = "string:32" }
        { name = profession-type, column = professionType, type = "string:64" }
        { name = nature-of-business, column = natureOfBusiness, type = "string:64" }
        { name = cust-type, column = custType, type = "string:64" }
        { name = cust-gender, column = custGender, type = "string:16" }
        { name = cust-mobile, column = custMobile, type = "string:32" }
        { name = cust-email, column = custEmail, type = "string:64" }
        { name = cust-mailing-addr, column = custMailingAddr, type = "string:4000" }
        { name = cust-res-addr, column = custResAddr, type = "string:4000" }
        { name = cust-res-addr-city, column = custResAddrCity, type = "string:256" }
        { name = cust-res-addr-state, column = custResAddrState, type = "string:256" }
        { name = cust-res-addr-country, column = custResAddrCountry, type = "string:256" }
        { name = cust-nationality, column = nationality, type = "string:256" }
        { name = cust-d-o-b, column = custDOB, type = "string:256" }
        { name = cust-name, column = custName, type = "string:256" }
        { name = createdOn, column = createdOn, type = "string:256" }


        #extra fields added for manual editing of STR Report for MALDIVES
	{ name = type-of-report, column =typeOfReport, type = "string:256" }
        { name = previous-str-reference-number, column =previousStrReferenceNumber, type = "string:256" }
        { name = previous-str-reference-date, column =previousStrReferenceDate, type = "string:256" }
        { name = previous-str-reference-remarks, column =previousStrReferenceRemarks, type = "string:256" }
        { name = number-of-documents-attached, column =numberOfDocumentsAttached, type = "string:256" }
        { name = please-provide-list, column =pleaseProvideList, type = "string:256" }
        { name = suspect-type-reported-individual, column =suspectTypeReportedIndividual, type = "string:256" }
        { name = suspect-type-reported-legal-entity, column =suspectTypeReportedLegalEntity, type = "string:256" }
        { name = entity-type, column =entityType, type = "string:256" }
        { name = types-of-transactions, column =typesOfTransactions, type = "string:256" }
        { name = amount-unknown, column =amountUnknown, type = "string:256" }
        { name = reason-for-suspicion, column =reasonForSuspicion, type = "string:3000" }

        { name = person-conducting-transaction-name, column =personConductingTransactionName, type = "string:256" }
        { name = person-conducting-transaction-idno, column =  personConductingTransactionIdno, type = "string:256"  }
        { name = person-conducting-transaction-idType, column = personConductIdType, type = "string:256"}
        { name = person-conducting-transaction-address, column = personConductingTransactionAddress, type = "string:256"}
        { name = id-issuer, column = idIssuer, type = "string:256" }
        { name = id-issued-date, column = idIssuedDate, type = "string:256" }
        { name = id-expiry-date, column = idExpiryDate, type = "string:256" }
        { name = capacity-in-which-txnis-being-conducted, column = capacityInWhichTxnisBeingConducted, type = "string:256" }
        { name = on-behalf, column = onBehalf, type = "string:256" }
	{ name = transaction-recipient, column = transactionRecipient, type = "string:256" }
        { name = transaction-recipient-others, column = transactionRecipientOthers, type = "string:256" }
        { name = transaction-recipient-id-issuer, column = transactionRecipientIdIssuer, type = "string:256" }
        { name = transaction-recipient-issued-date, column = transactionRecipientIssuedDate, type = "string:256" }
        { name = transaction-recipient-expiry-date, column = transactionRecipientExpiryDate, type = "string:256" }
        { name = is-the-suspected-party-affiliated-with-reporting-entityin-anyway, column = isTheSuspectedPartyAffiliatedWithReportingEntityinAnyway, type ="string:256" }
        { name = if-yes-specify-relationship, column = ifYesSpecifyRelationship, type = "string:256" }
        { name = relationship-status-after-reporting, column = relationshipStatusAfterReporting, type = "string:256" }
        { name = buissness-affiliation-remarks, column = buissnessAffiliationRemarks, type = "string:256" }
        




        

	]
}
