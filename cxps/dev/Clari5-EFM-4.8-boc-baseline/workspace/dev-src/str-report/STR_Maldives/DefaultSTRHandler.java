package clari5.aml.cms.newjira;

import clari5.aml.commons.*;
import clari5.hfdb.*;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.rdbms.RdbmsException;
import clari5.report.db.StrReport;
import cxps.noesis.utils.AmlCaseEvent;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashank on 8/21/18
 */

public class DefaultSTRHandler implements ISTRHandler {

    public String getCustId(RDBMSSession session, AmlCaseEvent amlCaseEvent) {
        CmsSuspicionDetails cms = amlCaseEvent.getCmsSuspicionDetails();

        String acctId = cms.getAccountsDetails().get(0).getAccountId();
        List<String> accIds = new ArrayList<>();
        accIds.add(acctId);
        List<AmlAccountView> accountViews = Hfdb.getAccounts(session, accIds);
        AmlAccountView accountView = accountViews.get(0);

        assert accountView != null;
        return accountView.getCustId();
    }

    public String getAccountId(AmlCaseEvent amlCaseEvent) {
        return amlCaseEvent.getCmsSuspicionDetails().getAccountsDetails().get(0).getAccountId();
    }

    @Override
    public StrReport populateStrReport(RDBMSSession session, CmsLuw cmsLuw, AmlCaseEvent amlCaseEvent, String json) throws RdbmsException {
        CaseEvent caseEvent = cmsLuw.caseEvent;
        CmsSuspicionDetails cms = amlCaseEvent.getCmsSuspicionDetails();

        String acctId = getAccountId(amlCaseEvent);
        List<String> accIds = new ArrayList<>();
        accIds.add(acctId);
        List<AmlAccountView> accountViews = Hfdb.getAccounts(session, accIds);
        AmlAccountView accountView = accountViews.get(0);

        assert accountView != null;
        String custID = getCustId(session, amlCaseEvent);

        AmlCustomerViewKey key = new AmlCustomerViewKey(session);
        key.setCustId(custID.trim());
        AmlCustomerView customerView = key.select();

        CmsSuspicionDetailsGroundsOfSuspicion cmsSuspicionDetailsGroundsOfSuspicion = cms.getGroundsOfSuspicion();
        StrReport strReport = new StrReport(session);

        CmsSuspicionDetailsAccount cmsSuspicionDetailsAccounts = cms.getAccountsDetails().get(0);

        if (cmsSuspicionDetailsAccounts != null && cmsSuspicionDetailsAccounts.getTransactions() != null && cmsSuspicionDetailsAccounts.getTransactions().size() > 0) {
            CmsSuspicionDetailsAccountTransaction tran = cmsSuspicionDetailsAccounts.getTransactions().get(0);

            strReport.setNoTransactionToBeReported(cmsSuspicionDetailsAccounts.getNoTransactionToBeReported());
            strReport.setTransactionId(tran.getTransactionId());
            strReport.setRelatedAcct(tran.getRelatedAcct());
            strReport.setAdditionalRemarks(tran.getAdditionalRemarks());

        }

        //strReport.setStrId(UUID.randomUUID().toString().replace("-", ""));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-YYYY.HHmm");
        String date = dateFormat.format(new Date(amlCaseEvent.getIncident_closedDate()));
        strReport.setStrId("STR-" + date.toUpperCase() + "-" + acctId);
        strReport.setAccountId(acctId);

        if(cmsSuspicionDetailsGroundsOfSuspicion != null){
            strReport.setRelationshipBeginDate(cmsSuspicionDetailsGroundsOfSuspicion.getRelationshipBeginDate());
            strReport.setBeneficiaryDetails(cmsSuspicionDetailsGroundsOfSuspicion.getBeneficiaryDetails());
            strReport.setVolumeOfTransactions(cmsSuspicionDetailsGroundsOfSuspicion.getVolumeOfTransactions());
        }

        strReport.setFactName(amlCaseEvent.getFactname());
        strReport.setRiskLevel(amlCaseEvent.getRiskLevel());
        strReport.setCaseEntityId(amlCaseEvent.getCaseEntityId());
        strReport.setEntityIdType(amlCaseEvent.getEntityIdType());
        strReport.setIncidentScore(amlCaseEvent.getIncidentScore());
        strReport.setEntityName(amlCaseEvent.getEntityName());
        strReport.setIncidentClosedDate((new Date(amlCaseEvent.getIncident_closedDate())).toString());
        strReport.setEntityId(amlCaseEvent.getEntityId());
        strReport.setCaseEntityName(amlCaseEvent.getCaseEntityName());
        strReport.setCaseDisplayKey(amlCaseEvent.getCaseDisplayKey());
        strReport.setNoOfStr(amlCaseEvent.getNoOfStr());
        strReport.setLastFiledStr(amlCaseEvent.getLastFiledStr().toString());

        strReport.setCustIdNum(customerView.getIdNum());
        strReport.setCustFax(customerView.getFax());
        strReport.setProfessionType(customerView.getProfessionType());
        strReport.setNatureOfBusiness(customerView.getNatureOfBusiness());
        strReport.setCustType(customerView.getCustType());
        strReport.setCustGender(customerView.getGender());
        strReport.setCustMobile(customerView.getMobile1());
        strReport.setCustEmail(customerView.getEmail());
        strReport.setCustMailingAddr(customerView.getMailingAddr());
        strReport.setCustResAddr(customerView.getResAddr()  + "," + customerView.getNationality());
        strReport.setCustResAddrCity(customerView.getResAddrCity());
        strReport.setCustResAddrState(customerView.getResAddrState());
        strReport.setCustResAddrCountry(customerView.getResAddrCountry());
        strReport.setCustDOB(customerView.getDob().toString());
        strReport.setCustName(customerView.getName());
        strReport.setCreatedOn((new Timestamp(System.currentTimeMillis())).toString());
        strReport.setCustNationality(customerView.getNationality());

/*        strReport.setAcctPassNo(customerView.getCustPassportNo());
        //TODO business Type
        strReport.setAcctBussinessType("");
        strReport.setAcctOccupation(customerView.getOccupation());
        //TODO employer name durationRelationShip mapping
        strReport.setAcctEmployerName("");
        strReport.setAcctTelephone(customerView.getMobile1());
        strReport.setAcctDurationRelationshipCust("");

        strReport.setAccountType(accountView.getAcctCurrType());
        strReport.setOpeningDate(accountView.getOpenedDate());

        AmlBranchViewKey amlBranchViewKey = new AmlBranchViewKey(session);
        amlBranchViewKey.setBranchId(accountView.getBranchId());

        AmlBranchView branchView = amlBranchViewKey.select();
        if(branchView != null){
            strReport.setBranchName(branchView.getName());
            strReport.setBranchAddress(branchView.getAddress());
        }
        strReport.setAcctStatus(accountView.getStatus());
        strReport.setCurrBalance(accountView.getBalance());
        strReport.setCurrencyCode(accountView.getCurrency());*/
        return strReport;
    }
}
