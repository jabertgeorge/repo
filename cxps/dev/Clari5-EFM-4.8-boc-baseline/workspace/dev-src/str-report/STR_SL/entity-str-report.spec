clari5.report.db.entity.str-report {
	attributes = [
        { name = str-id, column = strId, type = "string:256", key = true }
        { name = account-id, column = accountId, type = "string:32" }
        { name = no-transaction-to-be-reported, column = noTransactionToBeReported, type = "string:32" }
        { name = transaction-id, column = transactionId, type = "string:64" }
        { name = related-acct, column = relatedAcct, type: "string:32" }
        { name = additional-remarks, column = additionalRemarks, type: "string:4000" }

        { name = volume-of-transactions, column = volumeOfTransactions, type = "string:64" }
        { name = beneficiary-details, column = beneficiaryDetails, type = "string:4000" }
        { name = fact-name, column = factName, type = "string:64" }
        { name = risk-level, column = riskLevel, type = "string:8" }
        { name = case-entity-id, column = caseEntityId, type = "string:64" }
        { name = entity-id-type, column = entityIdType, type = "string:64" }
        { name = incident-score, column = incidentScore, type = "string:8" }
        { name = entity-name, column = entityName, type = "string:64" }
        { name = incident-closed-date, column = incidentClosedDate, type = "date" }
        { name = entity-id, column = entityId, type = "string:64" }
        { name = case-entity-name, column = caseEntityName, type = "string:64" }
        { name = case-display-key, column = caseDisplayKey, type = "string:500" }
        { name = no-of-str, column = noOfStr, type = "number:8" }
        { name = last-filed-str, column = lastFiledStr, type = "number:32" }
        { name = relationship-begin-date, column = relationshipBeginDate, type = "string:32" }

        { name = cust-id-num, column = custIdNum, type = "string:64" }
        { name = cust-fax, column = custFax, type = "string:32" }
        { name = profession-type, column = professionType, type = "string:64" }
        #{ name = nature-of-business, column = natureOfBusiness, type = "string:64" }
        { name = cust-type, column = custType, type = "string:64" }
        { name = cust-gender, column = custGender, type = "string:16" }
        { name = cust-mobile, column = custMobile, type = "string:32" }
        { name = cust-email, column = custEmail, type = "string:64" }
        { name = cust-mailing-addr, column = custMailingAddr, type = "string:4000" }
        { name = cust-res-addr, column = custResAddr, type = "string:4000" }
        { name = cust-res-addr-city, column = custResAddrCity, type = "string:256" }
        { name = cust-res-addr-state, column = custResAddrState, type = "string:256" }
        { name = cust-res-addr-country, column = custResAddrCountry, type = "string:256" }
        #{ name = cust-nationality, column = nationality, type = "string:256" }
        { name = cust-d-o-b, column = custDOB, type = "date" }
        { name = cust-name, column = custName, type = "string:256" }
        { name = createdOn, column = createdOn, type = timestamp }

        #extra fields added for manual editing of STR Report
        { name = acct-pass-no, column = acctPassportNo, type = "string:256" }
        { name = acct-bussiness-type, column = acctBusinessType, type = "string:256" }
        { name = acct-occupation, column = acctOccupation, type = "string:256" }
        { name = acct-telephone, column = acctTelephone, type = "string:256" }
        { name = acct-last-review-date, column = acctLastReviewDate, type = "string:256" }
        { name = acct-duration-relationship-cust, column = acctDurationRelationshipCust, type = "string:256" }
        { name = acct-employer-name, column = acctEmployerName, type = "string:256" }

        { name = person-name, column = personName, type = "string:256" }
        { name = person-residential-addr, column = personResidentialAddress, type = "string:256" }
        { name = person-gender, column = personGender, type = "string:256" }
        { name = person-pass-no, column = personPassportNo, type = "string:256" }
        { name = person-bussiness-type, column = personBusinessType, type = "string:256" }
        { name = person-occupation, column = personOccupation, type = "string:256" }
        { name = person-telephone, column = personTelephone, type = "string:256" }
        { name = person-country-nationality, column = personCountryNationality, type = "string:256" }
        { name = person-employer-name, column = personEmployerName, type = "string:256" }

        { name = opening-date, column = openingDate, type = "date" }
        { name = account-type, column = accountType, type = "string:256" }
        { name = branch-name, column = branchName, type = "string:256" }
        { name = branch-address, column = branchAddress, type = "string:256" }
        { name = acct-status, column = acctStatus, type = "string:256" }
        { name = curr-balance, column = currBalance, type = "number:14,2" }
        { name = currency-code, column = currencyCode, type = "string:256" }

        { name = is-replacement, column = isReplacement, type = "string:256" }
        { name = previous-reference-no, column = previousReferenceNo, type = "string:256" }

        { name = grounds-of-suspicion, column = groundsOfSuspicion, type = "string:256" }
        #{ name = grounds-of-suspicion-details, column = groundsOfSuspicionDetails, type = "string:4000" }
        { name = summary-of-suspicion, column = summaryOfSuspicion, type = "string:4000" }

        { name = name-of-reporting-officer, column = nameOfReportingOfficer, type = "string:256" }
        { name = Designation, column = designation, type = "string:256" }
        { name = Address, column = address, type = "string:256" }
        { name = contact-no, column = contactNo, type = "string:256" }
        { name = name-of-compliance-officer, column = nameOfComplianceOfficer, type = "string:256" }


	]
}
