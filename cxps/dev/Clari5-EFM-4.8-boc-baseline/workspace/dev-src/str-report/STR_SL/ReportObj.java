package clari5.aml.reports.rbi.jasperUtil.str.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import clari5.report.db.StrReport;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by shashank on 5/2/18.
 */

public class ReportObj {
    private String strId = null;
    private String accountId = null;
    private String noTransactionToBeReported = null;
    private String transactionId = null;
    private String relatedAcct = null;
    private String additionalRemarks = null;
    private String background = null;
    private String eventName = null;
    private String eventId = null;
    private String factName = null;
    private String riskLevel = null;
    private String caseEntityId = null;
    private String entityIdType = null;
    private String incidentScore = null;
    private String entityName = null;
    private Date incidentClosedDate = null;
    private String entityId = null;
    private String caseEntityName = null;
    private String caseDisplayKey = null;
    private Integer noOfStr = null;
    private Long lastFiledStr = null;
    private String custIdNum = null;
    private String custFax = null;
    private String professionType = null;
    private String natureOfBusiness = null;
    private String custType = null;
    private String custGender = null;
    private String custMobile = null;
    private String custEmail = null;
    private String custMailingAddr = null;
    private String custResAddr = null;
    private String custResAddrCity = null;
    private String custResAddrState = null;
    private String custResAddrCountry = null;
    private Date custDOB = null;
    private String custName = null;
    private Timestamp createdOn = null;
    private String beneficiaryDetails = null;
    private String volumeOfTransactions = null;
    private String relationshipBeginDate = null;
    private String acctPassportNo = null;
    private String acctBussinessType = null;
    private String acctOccupation = null;
    private String acctTelephone = null;
    private String acctLastReviewDate = null;
    private String acctDurationRelationshipCust = null;
    private String acctEmployerName = null;
    private String personName;
    private String personResidentialAddr;
    private String personGender;
    private String personPassNo = null;
    private String personBussinessType = null;
    private String personOccupation = null;
    private String personTelephone = null;
    private String personCountryNationality = null;
    private String personEmployerName = null;
    private Date openingDate = null;
    private String accountType = null;
    private String branchName = null;
    private String branchAddress = null;
    private String acctStatus = null;
    private Double currBalance = null;
    private String currencyCode = null;
    private String isReplacement = null;
    private String previousReferenceNo = null;
    private String groundsOfSuspicion = null;
    //private String groundsOfSuspicionDetails = null;
    private String summaryOfSuspicion = null;
    private String nameOfReportingOfficer = null;
    private String designation = null;
    private String address = null;
    private String contactNo = null;
    private String nameOfComplianceOfficer = null;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getNoTransactionToBeReported() {
        return noTransactionToBeReported;
    }

    public void setNoTransactionToBeReported(String noTransactionToBeReported) {
        this.noTransactionToBeReported = noTransactionToBeReported;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRelatedAcct() {
        return relatedAcct;
    }

    public void setRelatedAcct(String relatedAcct) {
        this.relatedAcct = relatedAcct;
    }

    public String getAdditionalRemarks() {
        return additionalRemarks;
    }

    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getFactName() {
        return factName;
    }

    public void setFactName(String factName) {
        this.factName = factName;
    }

    public String getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }

    public String getCaseEntityId() {
        return caseEntityId;
    }

    public void setCaseEntityId(String caseEntityId) {
        this.caseEntityId = caseEntityId;
    }

    public String getEntityIdType() {
        return entityIdType;
    }

    public void setEntityIdType(String entityIdType) {
        this.entityIdType = entityIdType;
    }

    public String getIncidentScore() {
        return incidentScore;
    }

    public void setIncidentScore(String incidentScore) {
        this.incidentScore = incidentScore;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Date getIncidentClosedDate() {
        return incidentClosedDate;
    }

    public void setIncidentClosedDate(Date incidentClosedDate) {
        this.incidentClosedDate = incidentClosedDate;
    }

    public void setIncidentClosedDate(String incidentClosedDate) {
        if (incidentClosedDate != null && !incidentClosedDate.isEmpty())
            this.incidentClosedDate = Date.valueOf(incidentClosedDate);
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getCaseEntityName() {
        return caseEntityName;
    }

    public void setCaseEntityName(String caseEntityName) {
        this.caseEntityName = caseEntityName;
    }

    public String getCaseDisplayKey() {
        return caseDisplayKey;
    }

    public void setCaseDisplayKey(String caseDisplayKey) {
        this.caseDisplayKey = caseDisplayKey;
    }

    public Integer getNoOfStr() {
        return noOfStr;
    }

    public void setNoOfStr(Integer noOfStr) {
        this.noOfStr = noOfStr;
    }

    public Long getLastFiledStr() {
        return lastFiledStr;
    }

    public void setLastFiledStr(Long lastFiledStr) {
        this.lastFiledStr = lastFiledStr;
    }

    public String getCustIdNum() {
        return custIdNum;
    }

    public void setCustIdNum(String custIdNum) {
        this.custIdNum = custIdNum;
    }

    public String getCustFax() {
        return custFax;
    }

    public void setCustFax(String custFax) {
        this.custFax = custFax;
    }

    public String getProfessionType() {
        return professionType;
    }

    public void setProfessionType(String professionType) {
        this.professionType = professionType;
    }

   // public String getNatureOfBusiness() {
     //   return natureOfBusiness;
  //  }

  //  public void setNatureOfBusiness(String natureOfBusiness) {
    //    this.natureOfBusiness = natureOfBusiness;
   // }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getCustGender() {
        return custGender;
    }

    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMailingAddr() {
        return custMailingAddr;
    }

    public void setCustMailingAddr(String custMailingAddr) {
        this.custMailingAddr = custMailingAddr;
    }

    public String getCustResAddr() {
        return custResAddr;
    }

    public void setCustResAddr(String custResAddr) {
        this.custResAddr = custResAddr;
    }

    public String getCustResAddrCity() {
        return custResAddrCity;
    }

    public void setCustResAddrCity(String custResAddrCity) {
        this.custResAddrCity = custResAddrCity;
    }

    public String getCustResAddrState() {
        return custResAddrState;
    }

    public void setCustResAddrState(String custResAddrState) {
        this.custResAddrState = custResAddrState;
    }

    public String getCustResAddrCountry() {
        return custResAddrCountry;
    }

    public void setCustResAddrCountry(String custResAddrCountry) {
        this.custResAddrCountry = custResAddrCountry;
    }

    public Date getCustDOB() {
        return custDOB;
    }

    public void setCustDOB(Date custDOB) {
        this.custDOB = custDOB;
    }

    public void setCustDOB(String custDOB) {
        if (custDOB != null && !custDOB.isEmpty())
            this.custDOB = Date.valueOf(custDOB);
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public void setCreatedOn(String createdOn) {
        if(createdOn != null && !createdOn.isEmpty())
            this.createdOn = Timestamp.valueOf(createdOn);
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getAcctPassportNo() {
        return acctPassportNo;
    }

    public void setAcctPassportNo(String acctPassportNo) {
        this.acctPassportNo = acctPassportNo;
    }

    public String getBeneficiaryDetails() {
        return beneficiaryDetails;
    }

    public void setBeneficiaryDetails(String beneficiaryDetails) {
        this.beneficiaryDetails = beneficiaryDetails;
    }

    public String getVolumeOfTransactions() {
        return volumeOfTransactions;
    }

    public void setVolumeOfTransactions(String volumeOfTransactions) {
        this.volumeOfTransactions = volumeOfTransactions;
    }

    public String getAcctBussinessType() {
        return acctBussinessType;
    }

    public void setAcctBussinessType(String acctBussinessType) {
        this.acctBussinessType = acctBussinessType;
    }

    public String getAcctOccupation() {
        return acctOccupation;
    }

    public void setAcctOccupation(String acctOccupation) {
        this.acctOccupation = acctOccupation;
    }

    public String getAcctTelephone() {
        return acctTelephone;
    }

    public void setAcctTelephone(String acctTelephone) {
        this.acctTelephone = acctTelephone;
    }

    public String getAcctLastReviewDate() {
        return acctLastReviewDate;
    }

    public void setAcctLastReviewDate(String acctLastReviewDate) {
        this.acctLastReviewDate = acctLastReviewDate;
    }

    public String getAcctDurationRelationshipCust() {
        return acctDurationRelationshipCust;
    }

    public void setAcctDurationRelationshipCust(String acctDurationRelationshipCust) {
        this.acctDurationRelationshipCust = acctDurationRelationshipCust;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonResidentialAddr() {
        return personResidentialAddr;
    }

    public void setPersonResidentialAddr(String personResidentialAddr) {
        this.personResidentialAddr = personResidentialAddr;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }

    public String getAcctEmployerName() {
        return acctEmployerName;
    }

    public void setAcctEmployerName(String acctEmployerName) {
        this.acctEmployerName = acctEmployerName;
    }

    public String getPersonEmployerName() {
        return personEmployerName;
    }

    public void setPersonEmployerName(String personEmployerName) {
        this.personEmployerName = personEmployerName;
    }

    public String getPersonPassNo() {
        return personPassNo;
    }

    public void setPersonPassNo(String personPassNo) {
        this.personPassNo = personPassNo;
    }

    public String getPersonBussinessType() {
        return personBussinessType;
    }

    public void setPersonBussinessType(String personBussinessType) {
        this.personBussinessType = personBussinessType;
    }

    public String getPersonOccupation() {
        return personOccupation;
    }

    public void setPersonOccupation(String personOccupation) {
        this.personOccupation = personOccupation;
    }



    public String getPersonTelephone() {
        return personTelephone;
    }

    public void setPersonTelephone(String personTelephone) {
        this.personTelephone = personTelephone;
    }


    public String getPersonCountryNationality() {
        return personCountryNationality;
    }

    public void setPersonCountryNationality(String personCountryNationality) {
        this.personCountryNationality = personCountryNationality;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public void setOpeningDate(String openingDate) {
        if (openingDate != null && !openingDate.isEmpty())
            this.openingDate = Date.valueOf(openingDate);
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getAcctStatus() {
        return acctStatus;
    }

    public void setAcctStatus(String acctStatus) {
        this.acctStatus = acctStatus;
    }

    public Double getCurrBalance() {
        return currBalance;
    }

    public void setCurrBalance(Double currBalance) {
        this.currBalance = currBalance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getRelationshipBeginDate() {
        return relationshipBeginDate;
    }

    public void setRelationshipBeginDate(String relationshipBeginDate) {
        this.relationshipBeginDate = relationshipBeginDate;
    }

    public String getIsReplacement() {
        return isReplacement;
    }

    public void setIsReplacement(String isReplacement) {
        this.isReplacement = isReplacement;
    }

    public String getPreviousReferenceNo() {
        return previousReferenceNo;
    }

    public void setPreviousReferenceNo(String previousReferenceNo) {
        this.previousReferenceNo = previousReferenceNo;
    }

    public String getGroundsOfSuspicion() {
        return groundsOfSuspicion;
    }

    public void setGroundsOfSuspicion(String groundsOfSuspicion) {
        this.groundsOfSuspicion = groundsOfSuspicion;
    }

   /* public String getGroundsOfSuspicionDetails() {
        return groundsOfSuspicionDetails;
    }

    public void setGroundsOfSuspicionDetails(String groundsOfSuspicionDetails) {
        this.groundsOfSuspicionDetails = groundsOfSuspicionDetails;
    }*/

    public String getSummaryOfSuspicion() {
        return summaryOfSuspicion;
    }

    public void setSummaryOfSuspicion(String summaryOfSuspicion) {
        this.summaryOfSuspicion = summaryOfSuspicion;
    }

    public String getNameOfReportingOfficer() {
        return nameOfReportingOfficer;
    }

    public void setNameOfReportingOfficer(String nameOfReportingOfficer) {
        this.nameOfReportingOfficer = nameOfReportingOfficer;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getNameOfComplianceOfficer() {
        return nameOfComplianceOfficer;
    }

    public void setNameOfComplianceOfficer(String nameOfComplianceOfficer) {
        this.nameOfComplianceOfficer = nameOfComplianceOfficer;
    }

    @JsonIgnore
    public static ReportObj mapStrReportToReportObj(StrReport report) {
        ReportObj obj = new ReportObj();
        obj.setStrId(report.getStrId());
        obj.setAccountId(report.getAccountId());
        obj.setNoTransactionToBeReported(report.getNoTransactionToBeReported());
        obj.setTransactionId(report.getTransactionId());
        obj.setRelatedAcct(report.getRelatedAcct());
        obj.setAdditionalRemarks(report.getAdditionalRemarks());
        obj.setFactName(report.getFactName());
        obj.setRiskLevel(report.getRiskLevel());
        obj.setCaseEntityId(report.getCaseEntityId());
        obj.setEntityIdType(report.getEntityIdType());
        obj.setIncidentScore(report.getIncidentScore());
        obj.setEntityName(report.getEntityName());
        obj.setIncidentClosedDate(report.getIncidentClosedDate());
        obj.setEntityId(report.getEntityId());
        obj.setCaseEntityName(report.getCaseEntityName());
        obj.setCaseDisplayKey(report.getCaseDisplayKey());
        obj.setNoOfStr(report.getNoOfStr());
        obj.setLastFiledStr(report.getLastFiledStr());
        obj.setCustIdNum(report.getCustIdNum());
        obj.setCustFax(report.getCustFax());
        obj.setProfessionType(report.getProfessionType());
       // obj.setNatureOfBusiness(report.getNatureOfBusiness());
        obj.setCustType(report.getCustType());
        obj.setCustGender(report.getCustGender());
        obj.setCustMobile(report.getCustMobile());
        obj.setCustEmail(report.getCustEmail());
        obj.setCustMailingAddr(report.getCustMailingAddr());
        obj.setCustResAddr(report.getCustResAddr());
        obj.setCustResAddrCity(report.getCustResAddrCity());
        obj.setCustResAddrState(report.getCustResAddrState());
        obj.setCustResAddrCountry(report.getCustResAddrCountry());
        obj.setCustDOB(report.getCustDOB());
        obj.setCustName(report.getCustName());
        obj.setCreatedOn(report.getCreatedOn());
        obj.setAcctPassportNo(report.getAcctPassNo());
        obj.setBeneficiaryDetails(report.getBeneficiaryDetails());
        obj.setVolumeOfTransactions(report.getVolumeOfTransactions());
        obj.setAcctBussinessType(report.getAcctBussinessType());
        obj.setAcctOccupation(report.getAcctOccupation());
        obj.setAcctTelephone(report.getAcctTelephone());
        obj.setAcctLastReviewDate(report.getAcctLastReviewDate());
        obj.setAcctDurationRelationshipCust(report.getAcctDurationRelationshipCust());
        obj.setAcctEmployerName(report.getAcctEmployerName());
        obj.setPersonName(report.getPersonName());
        obj.setPersonResidentialAddr(report.getPersonResidentialAddr());
        obj.setPersonGender(report.getPersonGender());
        obj.setPersonPassNo(report.getPersonPassNo());
        obj.setPersonBussinessType(report.getPersonBussinessType());
        obj.setPersonOccupation(report.getPersonOccupation());
        obj.setPersonTelephone(report.getPersonTelephone());
        obj.setPersonCountryNationality(report.getPersonCountryNationality());
        obj.setPersonEmployerName(report.getPersonEmployerName());
        obj.setOpeningDate(report.getOpeningDate());
        obj.setAccountType(report.getAccountType());
        obj.setBranchName(report.getBranchName());
        obj.setBranchAddress(report.getBranchAddress());
        obj.setAcctStatus(report.getAcctStatus());
        obj.setCurrBalance(report.getCurrBalance());
        obj.setCurrencyCode(report.getCurrencyCode());
        obj.setRelationshipBeginDate(report.getRelationshipBeginDate());
        obj.setIsReplacement(report.getIsReplacement());
        obj.setPreviousReferenceNo(report.getPreviousReferenceNo());
        obj.setGroundsOfSuspicion(report.getGroundsOfSuspicion());
        //obj.setGroundsOfSuspicionDetails(report.getGroundsOfSuspicionDetails());
        obj.setSummaryOfSuspicion(report.getSummaryOfSuspicion());
        obj.setNameOfReportingOfficer(report.getNameOfReportingOfficer());
        obj.setDesignation(report.getDesignation());
        obj.setAddress(report.getAddress());
        obj.setContactNo(report.getContactNo());
        obj.setNameOfComplianceOfficer(report.getNameOfComplianceOfficer());
        return obj;
    }

    @JsonIgnore
    public static void mapReportObjToStrReport(ReportObj obj, StrReport report) {

        if (obj.getAccountId() != null)
            report.setAccountId(obj.getAccountId());
        if (obj.getNoTransactionToBeReported() != null)
            report.setNoTransactionToBeReported(obj.getNoTransactionToBeReported());
        if (obj.getTransactionId() != null)
            report.setTransactionId(obj.getTransactionId());
        if (obj.getRelatedAcct() != null)
            report.setRelatedAcct(obj.getRelatedAcct());
        if (obj.getAdditionalRemarks() != null)
            report.setAdditionalRemarks(obj.getAdditionalRemarks());
        if (obj.getFactName() != null)
            report.setFactName(obj.getFactName());
        if (obj.getRiskLevel() != null)
            report.setRiskLevel(obj.getRiskLevel());
        if (obj.getCaseEntityId() != null)
            report.setCaseEntityId(obj.getCaseEntityId());
        if (obj.getEntityIdType() != null)
            report.setEntityIdType(obj.getEntityIdType());
        if (obj.getIncidentScore() != null)
            report.setIncidentScore(obj.getIncidentScore());
        if (obj.getEntityName() != null)
            report.setEntityName(obj.getEntityName());
        if (obj.getIncidentClosedDate() != null)
            report.setIncidentClosedDate(obj.getIncidentClosedDate());
        if (obj.getEntityId() != null)
            report.setEntityId(obj.getEntityId());
        if (obj.getCaseEntityName() != null)
            report.setCaseEntityName(obj.getCaseEntityName());
        if (obj.getCaseDisplayKey() != null)
            report.setCaseDisplayKey(obj.getCaseDisplayKey());
        if (obj.getNoOfStr() != null)
            report.setNoOfStr(obj.getNoOfStr());
        if (obj.getLastFiledStr() != null)
            report.setLastFiledStr(obj.getLastFiledStr());
        if (obj.getCustIdNum() != null)
            report.setCustIdNum(obj.getCustIdNum());
        if (obj.getCustFax() != null)
            report.setCustFax(obj.getCustFax());
        if (obj.getProfessionType() != null)
            report.setProfessionType(obj.getProfessionType());
       // if (obj.getNatureOfBusiness() != null)
           // report.setNatureOfBusiness(obj.getNatureOfBusiness());
        if (obj.getCustType() != null)
            report.setCustType(obj.getCustType());
        if (obj.getCustGender() != null)
            report.setCustGender(obj.getCustGender());
        if (obj.getCustMobile() != null)
            report.setCustMobile(obj.getCustMobile());
        if (obj.getCustEmail() != null)
            report.setCustEmail(obj.getCustEmail());
        if (obj.getCustMailingAddr() != null)
            report.setCustMailingAddr(obj.getCustMailingAddr());
        if (obj.getCustResAddr() != null)
            report.setCustResAddr(obj.getCustResAddr());
        if (obj.getCustResAddrCity() != null)
            report.setCustResAddrCity(obj.getCustResAddrCity());
        if (obj.getCustResAddrState() != null)
            report.setCustResAddrState(obj.getCustResAddrState());
        if (obj.getCustResAddrCountry() != null)
            report.setCustResAddrCountry(obj.getCustResAddrCountry());
        if (obj.getCustDOB() != null)
            report.setCustDOB(obj.getCustDOB());
        if (obj.getCustName() != null)
            report.setCustName(obj.getCustName());
        if (obj.getCreatedOn() != null)
            report.setCreatedOn(obj.getCreatedOn());
        if(obj.getAcctPassportNo() != null)
            report.setAcctPassNo(obj.getAcctPassportNo());
        if(obj.getVolumeOfTransactions() != null)
            report.setVolumeOfTransactions(obj.getVolumeOfTransactions());
        if(obj.getBeneficiaryDetails() != null)
            report.setBeneficiaryDetails(obj.getBeneficiaryDetails());
        if(obj.getAcctBussinessType() != null )
            report.setAcctBussinessType(obj.getAcctBussinessType());
        if(obj.getAcctOccupation() != null )
            report.setAcctOccupation(obj.getAcctOccupation());
        if(obj.getAcctTelephone() != null )
            report.setAcctTelephone(obj.getAcctTelephone());
        if(obj.getAcctLastReviewDate() != null )
            report.setAcctLastReviewDate(obj.getAcctLastReviewDate());
        if(obj.getAcctDurationRelationshipCust() != null )
            report.setAcctDurationRelationshipCust(obj.getAcctDurationRelationshipCust());
        if(obj.getAcctEmployerName() != null)
            report.setAcctEmployerName(obj.getAcctEmployerName());
        if(obj.getPersonName() != null)
            report.setPersonName(obj.getPersonName());
        if(obj.getPersonResidentialAddr() != null)
            report.setPersonResidentialAddr(obj.getPersonResidentialAddr());
        if(obj.getPersonGender() != null)
            report.setPersonGender(obj.getPersonGender());
        if(obj.getPersonPassNo() != null )
            report.setPersonPassNo(obj.getPersonPassNo());
        if(obj.getPersonBussinessType() != null )
            report.setPersonBussinessType(obj.getPersonBussinessType());
        if(obj.getPersonOccupation() != null )
            report.setPersonOccupation(obj.getPersonOccupation());
        if(obj.getPersonTelephone() != null )
            report.setPersonTelephone(obj.getPersonTelephone());
        if(obj.getPersonCountryNationality() != null)
            report.setPersonCountryNationality(obj.getPersonCountryNationality());
        if(obj.getPersonEmployerName() != null)
            report.setPersonEmployerName(obj.getPersonEmployerName());
        if(obj.getOpeningDate() != null )
            report.setOpeningDate(obj.getOpeningDate());
        if(obj.getAccountType() != null )
            report.setAccountType(obj.getAccountType());
        if(obj.getBranchName() != null )
            report.setBranchName(obj.getBranchName());
        if(obj.getRelationshipBeginDate() != null)
            report.setRelationshipBeginDate(obj.getRelationshipBeginDate());
        if(obj.getIsReplacement() != null)
            report.setIsReplacement(obj.getIsReplacement());
        if(obj.getPreviousReferenceNo() != null)
            report.setPreviousReferenceNo(obj.getPreviousReferenceNo());
        if (obj.getGroundsOfSuspicion() != null)
            report.setGroundsOfSuspicion(obj.getGroundsOfSuspicion());
        /*if (obj.getGroundsOfSuspicionDetails() != null)
            report.setGroundsOfSuspicionDetails(obj.getGroundsOfSuspicionDetails());*/
        if(obj.getSummaryOfSuspicion() != null)
            report.setSummaryOfSuspicion(obj.getSummaryOfSuspicion());
        if(obj.getNameOfReportingOfficer() != null)
            report.setNameOfReportingOfficer(obj.getNameOfReportingOfficer());
        if(obj.getDesignation() != null)
            report.setDesignation(obj.getDesignation());
        if(obj.getAddress() != null)
            report.setAddress(obj.getAddress());
        if(obj.getContactNo() != null)
            report.setContactNo(obj.getContactNo());
        if(obj.getNameOfComplianceOfficer() != null)
            report.setNameOfComplianceOfficer(obj.getNameOfComplianceOfficer());

    }
}
