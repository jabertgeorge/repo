CREATE FUNCTION dbo.TRANDATEFROMEVENT (@input VARCHAR(250))
RETURNS TABLE
AS
RETURN(
select tran_crncy_code, fcy_tran_amt, txn_date, counter_party_account from dbo.EVENT_FT_ACCOUNT_TXN WHERE event_id = @input
UNION
select tran_curr, fcy_tran_amt, trn_date, ben_acct_no from dbo.EVENT_FT_REMITTANCE WHERE event_id = @input
UNION
select txn_currency, txn_amt, txn_date, ''counter_party from dbo.EVENT_FT_CC WHERE event_id = @input
UNION
select txn_curr, txn_amt, txn_date, ''counter_party from dbo.EVENT_FT_GOLDTXN WHERE event_id = @input
UNION
select txn_currency, txn_amt, txn_date, ''counter_party from dbo.EVENT_FT_LEASE WHERE event_id = @input
UNION
select txn_amt_cur, txn_amt, txn_date, counterparty_account from dbo.EVENT_FT_LOANTXN WHERE event_id = @input
UNION
select txn_currency, txn_amt, txn_date, ''counter_party from dbo.EVENT_FT_PAWNING WHERE event_id = @input
UNION
select txn_curr, txn_amt, txn_date, ''counter_party from dbo.EVENT_FT_TRAVELTXN WHERE event_id = @input
UNION
select txn_code, txn_amt, txn_date, countr_party_account from dbo.EVENT_FT_TRB_TXN WHERE event_id = @input
UNION
select txn_code, txn_amt, txn_date, countr_party_account from dbo.EVENT_FT_TRFT_TXN WHERE event_id = @input
);
