#!/usr/bin/env bash
# Author: Lovish

# Main function to allow setting up of all the values
main() {
    
    # If called in default mode, export environment only
    __env_load
    [ $# -lt 1 ] && return 0

    [ -z "${CL5_WS_REP}" ] && err "Error: Release not selected yet" && return 1

    local envfile=${w}/bin/env/product-env-params.sh
    local xenvfile=${w}/bin/env/extra-env-params.sh

    # Execute operations based on the selection
    case $1 in
        "--try"|"--check") __env_validate_conf ;;
        "--put"|"--create") __env_setup_env "${envfile}" "${xenvfile}" ;;
        "--reset") __env_reset "${envfile}" "${xenvfile}" ;;
        *) err "Error: Invalid selection" && return 1 ;;
    esac
}

# Reset environment
__env_reset() {
    
    [ -z "$1" ] || [ -z "$2" ] && err "Error: Incorrect usage" && return 1

    local envfile=${1}
    local xenvfile=${2}
    local files=""

    [ -e ${envfile} ] && files="${envfile}"
    [ -e ${xenvfile} ] && files="${files} ${xenvfile}"
    
    if [ ! -z "${files}" ]; then
        vals=$(grep -h '^[ 	]*export[ 	]*' ${envfile} ${xenvfile} \
            | sed 's/export[ 	]*\([a-zA-Z0-9_]*\)=.*/\1/g' )
        unset ${vals} 2>/dev/null

        rm -f ${envfile} ${xenvfile}
    fi

    # Load the basic environment
    __env_load
}

# Check for the strutural validation of metaconf
__env_validate_conf() {

    local _workDir=${d}/tmp

    # Validation check for the metaconfs of all the apps
    header "Structural validation of metaconf"
    if [ -e ${CL5_WS_REP}/meta ]; then
        printf "Validating for core product ... "        
        for file in ${CL5_WS_REP}/meta/*-meta.conf
        do
            [ ! -e ${file} ] && continue
            mkdir -p ${_workDir}
            ( cd ${_workDir} && execsh appenv --try ${file} )
            [ $? -ne 0 ] && err "error in $(basename ${file})" && rm -fr ${_workDir} && return 1
        done
        success "done"
    fi
    if [ -e ${CL5_WS_REP}-dev/meta ]; then
        printf "Validating for [${CL5_CUSTOM_ID}] dependencies ... "        
        for file in ${CL5_WS_REP}-dev/meta/*-meta.conf
        do
            [ ! -e ${file} ] && continue
            mkdir -p ${_workDir}
            ( cd ${_workDir} && execsh appenv --try ${file} )
            [ $? -ne 0 ] && err "error in $(basename ${file})" && rm -fr ${_workDir} && return 1
        done
        success "done"
    fi

    rm -fr ${_workDir}
}

# Setup the environment
__env_setup_env() {

    [ -z "$1" ] || [ -z "$2" ] && err "Error: Incorrect usage" && return 1

    local envfile=${1}
    local xenvfile=${2}
    local _workDir=${d}/tmp
    local tmpfile=${_workDir}/.envfile

    local _tmp

    # Set up the base environment
    __env_load

    # Always create a fresh file
    __env_create_envfile ${envfile} ${xenvfile}

    # Create env file
    header "Settings up environment"
    if [ -e ${CL5_WS_REP}/meta ]; then
        for file in ${CL5_WS_REP}/meta/*-meta.conf
        do
            [ ! -e ${file} ] && continue
            {
                printf "\n#---------------------------------------------------------------------"
                printf "\n# Adding the environment for [$(basename $file)]"
                printf "\n#---------------------------------------------------------------------\n"
            } >> ${envfile}
            mkdir -p ${_workDir}
            rm -f ${tmpfile}
            ( cd ${_workDir} && execsh appenv --put --append ${file} ${tmpfile} )
            [ $? -ne 0 ] && err "error in $(basename ${file})" && rm -fr ${_workDir} && return 1
            while read line
            do
                _tmp=$( echo $line | sed 's/export[ 	]*\([a-zA-Z0-9_]*\)=.*/\1/g' )
                echo ${CX_ENV_VARS[@]} | tr ' ' '\012' | grep -xq ${_tmp} 
                [ $? -eq 0 ] && ( echo "# Value for [${_tmp}] already set" >> ${envfile} ) \
                    || ( echo $line >> ${envfile} )
            done < ${tmpfile}
            . ${envfile}
        done
        success "Successfully created environment for core product"
    fi
    if [ -e ${CL5_WS_REP}-dev/meta ]; then
        for file in ${CL5_WS_REP}-dev/meta/*-meta.conf
        do
            [ ! -e ${file} ] && continue
            {
                printf "\n#---------------------------------------------------------------------"
                printf "\n# Adding the environment for [$(basename $file)]"
                printf "\n#---------------------------------------------------------------------\n"
            } >> ${xenvfile}
            mkdir -p ${_workDir}
            rm -f ${tmpfile}
            ( cd ${_workDir} && execsh appenv --put --append ${file} ${tmpfile} )
            [ $? -ne 0 ] && err "error in $(basename ${file})" && rm -fr ${_workDir} && return 1
            while read line
            do
                _tmp=$( echo $line | sed 's/export[ 	]*\([a-zA-Z0-9_]*\)=.*/\1/g' )
                echo ${CX_ENV_VARS[@]} | tr ' ' '\012' | grep -xq ${_tmp} 
                [ $? -eq 0 ] && ( echo "# Value for [${_tmp}] already set" >> ${xenvfile} ) \
                    || ( echo $line >> ${xenvfile} )
            done < ${tmpfile}
            . ${envfile}
        done
        success "Successfully created environment for custom dependencies"
    fi

    rm -fr ${_workDir}
}

# Create a basic template for envfile
__env_create_envfile() {
    
    local _main=$1
    local _xfile=$2

    local _basedir=$( dirname ${_main} )
    mkdir -p ${_basedir}

    echo '#!/usr/bin/env bash' | tee ${_main} ${_xfile} >/dev/null
    printf "
    |# -------------------------------------------------------
    |# Generated on $(date)
    |# -------------------------------------------------------
    |
    |xtra_params=\${w}/bin/env/$(basename ${_xfile})
    |[ -e \${xtra_params} ] \\
    |   && chmod +x \${xtra_params} \\
    |   && . \${xtra_params}" | cut -c6- >> ${_main}

    chmod +x ${_main} ${_xfile}
}

# Source the env
__env_load() {

    local _cwd=`pwd`
    local _file=${w}/bin/env/main-env.sh
    local _token _idx

    # Load the file in environment
    chmod +x ${_file} && . ${_file}
    unset CX_ENV_VARS
    for _token in $( grep 'export ' ${_file} | grep -v '^[ 	]*#' | cut -d'=' -f1 | sed 's/export[ 	]*\(.*\)[ 	]*$/\1/g' | sort -u )
    do
        CX_ENV_VARS=( $( echo ${CX_ENV_VARS[@]} ) ${_token} )
    done

    # Execute product env
    _file=${w}/bin/env/product-env-params.sh
    [ -e ${_file} ] \
        && chmod +x ${_file} \
        && . ${_file}

    export CX_ENV_VARS
}

unset header 2>/dev/null
header() {

    local _message="`echo $*`"
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "
${_tag}
  ${_message}
${_tag}
"
}

unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

# Executes script if exists
execsh() {
    SCRIPT=`which $1 2>/dev/null`
    if [ ! -z "${SCRIPT}" ]; then
        eval $*
        return $?
    fi
}

# Check if build has failed
check() {
    local _status=$?
    if [ ${_status} -ne 0 ]; then
        echo "ERROR: DEPLOYMENT FAILED" >&2
        exit ${_status}
    fi
}

# -----------------
# Main method call
# -----------------
main $*
