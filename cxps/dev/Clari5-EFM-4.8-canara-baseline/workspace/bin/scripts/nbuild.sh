#!/usr/bin/env bash
#------------------------------------------------------------
# Author    : Lovish
# Purpose   : Unified script to run build for any workspace,
#             provided a defined structure is followed.
#------------------------------------------------------------

# Define global variables
export REMOVE=/bin/rm
export DBDIR=db

# Descriptor for stdout
exec 3>&1

# Define a main function
main(){

    # Add timer
	local startTime=`date +%s`
    unset JAVA_OPTS

    # Verify if the args are OK (Sets CLEAN ROOTDIR PUBLISH PACK INSTALLBASE)
    _verify $*
    [ $? -ne 0 ] && return 1

	header "Logging in ${ROOTDIR}/buildall.txt"
	$REMOVE -f ${ROOTDIR}/buildall.txt
	exec > >(tee -a ${ROOTDIR}/buildall.txt) 2>&1

    [ -z "$CLEAN" ] || $REMOVE -fr ${INSTALLBASE}
	mkdir -p $INSTALLBASE

    # Set allProj as global array
	cd ${ROOTDIR}
	export allProj=( `grep ^include settings.gradle  | tr "'" "\012" | grep -v -e '^include' -e ',' -e '^[      ]*$'` )

    # Clean phase
	$CLEAN

    # Precompile Phase
    precompile

    # Compile Phase
    compile

    # Installer Creation Phase
	creinstallation

	header "Total time taken: `calcTime ${startTime} $(date +%s)`"
}

# Define a usage
_usage() {
    echo "Usage : `basename $0` [ -i | -h ]"
}

# All argument checks
_verify() {

    CWD=`pwd`

    # Locate the gradle rootProject
    cd $CWD
    currD=.
    found=1
    while [ $found -ne 0 ]
    do
        [ -e ${currD}/settings.gradle ] && found=0 && break
        currD=$( cd $currD && cd .. && pwd )
        [ "${currD}" = "/" ] && break
    done
    [ $found -eq 1 ] && err "ERROR: Unable to locate gradle rootDir" && return 1

    ROOTDIR=$( cd ${currD} && pwd )
    INSTALLBASE=${w}/dev-src/installation

    ANGULAR=0
    CLEAN="clean"
    while [ ! -z "$*" ]
    do
        case $1 in
            "--no-angular")
                ANGULAR=1
                shift 1
                ;;
            "-h")
                _usage
                exit 0
                ;;
            "-i")
                CLEAN=""
                shift 1
                ;;
            *)
                err "Error: Incorrect Usage"
                _usage
                return 1
                ;;
        esac
    done
    export CLEAN ROOTDIR INSTALLBASE ANGULAR
}

# Cleanup phase
clean(){

    header "Cleanup Phase"
	gradle clean 
	# gradle --parallel clean 
    exec_script ${ROOTDIR}/bin/cxbuild/clean.sh
    check
}

# Preprocessing phase
precompile() {

    header "Pre-compilation Phase"
    execsh codegen settings.gradle
    execsh mxmlbuild.sh settings.gradle
    exec_script ${ROOTDIR}/bin/cxbuild/precompile.sh
    check
}

# Compilation phase
compile() {
    
    header "Compilation Phase"

    if ([ ${ANGULAR} -eq 0 ] && [ -d ${ROOTDIR}/angular/app ]); then
        echo "Building angular app"
        
        # Install node modules locally
        [ -z "${CLEAN}" ] || ( cd ${ROOTDIR}/angular && execsh npm install )
        check
        
        # Build app
        ( cd ${ROOTDIR}/angular && execsh npm run build )
        check
    fi

	dbinst=`echo ${allProj[@]} | tr ' ' '\012' | grep '^db' | sed 's/$/:install/'`
	jarinst=`echo ${allProj[@]} | tr ' ' '\012' | grep '^jars' | sed 's/$/:install/'`
	appinst=`echo ${allProj[@]} | tr ' ' '\012' | grep '^apps' | sed 's/^\(.*\)$/\1:install \1:creApp/'`
	daemoninst=`echo ${allProj[@]} | tr ' ' '\012' | grep '^daemons' | sed 's/$/:creWar/'`
	warinst=`echo ${allProj[@]} | tr ' ' '\012' | grep '^wars' | sed 's/$/:creWar/'`
	toolinst=`echo ${allProj[@]} | tr ' ' '\012' | grep '^tools' | sed 's/$/:creTool/'`

	debug "gradle $dbinst $jarinst $appinst $daemoninst $warinst $toolinst"
	gradle $dbinst $jarinst $appinst $daemoninst $warinst $toolinst
	check
    exec_script ${ROOTDIR}/bin/cxbuild/compile.sh
    check
}

# Installer creation phase
creinstallation() {

    local _temp

    header "Installer creation Phase"
	cd $ROOTDIR

    #if ([ ${ANGULAR} -eq 0 ] && [ -e ${ROOTDIR}/angular/app ]) ;then
    #    printf "Installing angular app ... "

    #    SRC=${ROOTDIR}/angular/app
    #    DEST=${INSTALLBASE}/rep/ui/${CX_MODULE}

    #    # Make sure it is always in clean mode
    #    rm -fr ${DEST}
    #    mkdir -p ${DEST}

    #    for file in $( cd ${SRC} && find . -type f )
    #    do
    #        ( echo ${file} | grep -q -e 'js$' -e 'js.map' ) && continue

    #        _temp=$(dirname $file )
    #        mkdir -p ${DEST}/${_temp}
    #        cp ${SRC}/${file} ${DEST}/${file}
    #    done
    #    echo "DONE"
    #fi

    if [ -e ${ROOTDIR}/bin/deps.cfg ]; then
        echo "Executing depsetup.sh using deps.cfg [scope=runtime]"
        depsetup.sh runtime
    fi

    if [ -e ${ROOTDIR}/${DBDIR} ]; then
        echo "Copying ${DBDIR}"
        ${REMOVE} -fr ${INSTALLBASE}/rep/${DBDIR}
        mkdir -p ${INSTALLBASE}/rep/${DBDIR}
        for d in ${ROOTDIR}/${DBDIR}/*
        do
            [ ! -e $d ] && continue
            _temp=`basename $d`
            mkdir -p ${INSTALLBASE}/rep/${DBDIR}/${_temp}
            if [ -d ${d}/dsm ]; then
                ln -fs ${d}/dsm ${INSTALLBASE}/rep/${DBDIR}/${_temp}
                [ ! -e ${ROOTDIR}/KEN ] && continue
                f="${w}/rep/${CL5_REL_NAME}/rep/db/${_temp}.tar"
                [ ! -e "${f}" ] && continue

                if grep -qiwr PATCH_DUMP ${f}; then
                    create_kenspec ${ROOTDIR} ${INSTALLBASE}/rep/${DBDIR}/${_temp}/dsm/seed
                    check
                fi
            fi
            ( cd ${INSTALLBASE}/rep/${DBDIR} && tar chf ${_temp}.tar ${_temp} && rm -fr ${_temp} )
        done
    fi

    # Add custom cdn for merge to happen
    if [ -e ${ROOTDIR}/cdn-efm ]; then
        printf "Adding custom cdn to the product ... "

        # Extract the cdn version
        _temp="$( dirname ${relV} )/deps.cfg"
        _temp=$( grep '[ 	]*cdn-efm[ 	]*:' ${_temp} | grep -v '^[ 	]*#' | cut -f3 -d':' | tr -d ' ')

        if [ -z "${_temp}" ]; then
            _temp="$( dirname ${relV} )/versions.properties"
            _temp=$( cat ${_temp} | grep '^[ 	]*efm[ 	]*=' | cut -f2 -d'=' | tr -d ' ')
        fi

        archiveName="cdn-efm-${_temp}"
        mkdir -p ${INSTALLBASE}/deps/others/cdn-efm/${_temp}
        cp -r ${ROOTDIR}/cdn-efm ${INSTALLBASE}/deps/others/cdn-efm/${_temp}/${archiveName}
        (
            cd ${INSTALLBASE}/deps/others/cdn-efm/${_temp} \
                && tar czf ${archiveName}.tgz ${archiveName} \
                && rm -fr ${INSTALLBASE}/deps/others/cdn-efm/${_temp}/${archiveName}
        )
        [ $? -ne 0 ] && echo "ERROR" && return 1 || echo "DONE"
    fi

    if [ -e ${ROOTDIR}/scripts ]; then
        printf "Creating scripts ... "
        $REMOVE -fr ${INSTALLBASE}/scripts
        cp -r ${ROOTDIR}/scripts ${INSTALLBASE}/scripts
        echo "DONE"
    fi

    exec_script ${ROOTDIR}/bin/cxbuild/creinstallation.sh
    check
}

# Creates the spec from KEN to DSM compliant conf
create_kenspec() {

    local _kenD=${1}/KEN
    local _destD=${2}/ken-seed.conf

    [ ! -d ${_kenD} ] && echo "KEN directory [${_kenD}] not found. SKIPPING" && return 0

    printf "Generating ken data ... "
    ( ! ( cd ${1} && execsh kendep ${_kenD} ${_destD} >/dev/null 2>&1 ) ) && err "ERROR" && return 1
    echo "DONE"
}

# ------------------
# Utility functions
# ------------------

# DEBUG utility
debug() {
	[ -z "$DEBUG" ] || echo "$*" >&2
}

# Executes script if exists
execsh() {
    SCRIPT=`which $1 2>/dev/null`
    if [ ! -z "${SCRIPT}" ]; then
        debug "Executing $SCRIPT"
        eval $*
    fi
}

# Execute a script
exec_script() {
    
    local SCRIPT=$1
    shift 1
    local ARGS=$*
    
    if [ -f ${SCRIPT} ]; then
        chmod +x ${SCRIPT}
        ${SCRIPT} ${ARGS}
    fi
}

unset header 2>/dev/null
header() {
    {
        printf '\033[03;32m'
        msg $*
        printf '\033[00m'
    } >&2
}

unset err 2>/dev/null
err() {
    {
        printf '\033[03;31m'
        msg $*
        printf '\033[00m'
    } >&2
}

# Prints a header message
msg() {

    local _message=`echo $*`
    local _length=${#_message}
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "
${_tag}
  ${_message}
${_tag}
"

}

# Check if build has failed
check() {
    local _status=$?
    if [ ${_status} -ne 0 ]; then
        err "ERROR: BUILD FAILED" >&2
        exit ${_status}
    fi
}

# Utility for calculating time
calcTime() {
    local _start=$1
    local _end=$2

    local _diff=$(( $_end - $_start ))

    local _secs=$_diff
    local _mins=$(( $_secs / 60 ))
    local _mins_sec=$(( $_secs % 60 ))
    local _hrs=$(( $_mins / 60 ))
    local _hrs_mins=$(( $_mins % 60 ))

    [ $_hrs -ne 0 ] && echo "$_hrs hours, $_hrs_mins minutes, $_mins_sec seconds" && return 0
    [ $_mins -ne 0 ] && echo "$_mins minutes, $_mins_sec seconds" && return 0
    echo "$_secs seconds"
}

# --------------
# Call for main
# --------------
main $*
