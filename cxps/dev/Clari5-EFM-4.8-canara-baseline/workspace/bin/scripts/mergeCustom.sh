#!/usr/bin/env bash
# Author: Lovish

( [ -z "${I}" ] || [ ! -d ${I} ] ) && err "Error: Assemble has not happened yet" && exit 1

# Merges the customization over main product
main() {
    
    # Get basedir
    local basedir=$( cd $(pwd) && cd $(dirname $0) && pwd )

    local version=$( ${basedir}/assemble.sh --rversion )
    local workDir=$1
    [ -z "${workDir}" ] && workDir=${w}/dev-src

    echo "Executing merge operation in [${workDir}]"
    unset JAVA_OPTS

    # If work directory not present, merge cannot be correctly performed
    [ ! -d ${workDir} ] && err 'Error: Unable to locate build' && return 1

    # Merge intallation over product (including db)
    merge_installation || return 1

    # Merge jars
    merge_libs ${workDir} || return 1

    # Merge configuration files
    merge_conf ${workDir} || return 1

    # Merge wars (extensions)
    merge_wars ${workDir} || return 1

    rm -f ${MISSING_ASSEMBLY_FILE}
}

# Merge the installation directory over the main product
merge_installation() {
    
    local _srcDir=${w}/dev-src/installation
    local _destDir=${I}

    local file name dir ext

    # If no installation directory exists, just return safely
    [ ! -d ${_srcDir} ] && return 0

    header "Merging the customization over project"
    # Copy all the files over product, merging archives (tar + tgz)
    for file in $( cd ${_srcDir} && find . -type f && find . -type l )
    do
        dir=$( dirname $file )   
        name=$( basename $file )
        ext=$( echo ${name##*.} | tr -d ' ' )

        [ ! -e ${_destDir}/${dir} ] && mkdir -p ${_destDir}/${dir}
        if ( $( echo ${ext} | grep -qxe 'tar' -e 'tgz' ) && [ -e ${_destDir}/${file} ] )
        then
            printf "Merging archive [${file}] ... "
            # Unpack archive of product
            ( cd ${_destDir}/${dir} && tar xf ${name} )

            # Unpack customization over the main, and repack
            ( cd ${_destDir}/${dir} && tar xf ${_srcDir}/${file} )
            
            name=$(basename ${name} .${ext} )
            rm -f ${_destDir}/${dir}/${name}.${ext}
            [[ "${ext}" = "tar" ]] \
                && ( cd ${_destDir}/${dir} && tar chf ${name}.${ext} ${name} ) \
                || ( cd ${_destDir}/${dir} && tar chzf ${name}.${ext} ${name} )
            success "DONE"

            rm -fr ${_destDir}/${dir}/${name}
        else
            printf "Merging [${file}] ... "
            ( ! cp ${_srcDir}/${file} ${_destDir}/${file} 2>${d}/.work ) \
                && err "ERROR" && return 1
            success "DONE"
        fi
    done

    [ $? -eq 0 ] || err "Warning: Issues while copying customization over the product" 
}

# Merge libraries from customization to product (custom prefixed jars are handled)
merge_libs() {

    local _workDir=${1}
    local _destDir=${I}

    local _srcDir=${_workDir}/jars

    local file name dir projdir projects proj jar to_copy version
    
    header "Merging the custom libraries"
    echo "Operation directive :: Merges the jars starting with custom to bundles defined in it's build.gradle"

    # Identify jars which are custom
    for jar in $( cd ${_srcDir} && find . -name 'custom-*.jar' )
    do
        to_copy=1

        dir=$( dirname ${jar} )
        name=$( basename ${jar} )

        # TODO handle transitive dependencies

        projdir=$( cd ${_srcDir}/${dir} && cd ../../ && pwd )
        proj=$( basename ${projdir} )
        
        # Ignore if project not in settings.gradle
        ( isProjectIncluded ${_workDir} ${proj} ) || continue

        echo "Copying jar [${name}]"

        [ ! -e ${projdir}/build.gradle ] \
            && err "Warning: [${proj}] not a project directory. SKIPPED" && continue

        # Get version, using gradle utility
        version=$( cd ${projdir} && gradle -q revision 2>/dev/null )
        [ $? -ne 0 ] && err "Warning: [${proj}] not properly setup. SKIPPED" && continue

        version=$( echo ${version} | tr -d ' ' )
        [ -z "${version}" ] \
            && err "Warning: Unable to fetch revision for [${proj}]. SKIPPED" && continue

        projects=( $( cd ${projdir} && gradle -q bundles 2>/dev/null ) )
        [ $? -ne 0 ] && err "Warning: [${proj}] not properly setup. SKIPPED" && continue
        [ -z "$( echo ${projects[@]} | tr -d ' ')" ] \
            && err "Warning: [${proj}] did not define wars. SKIPPED" && continue

        mkdir -p ${_destDir}/rep/ilib
        for proj in ${projects[@]}
        do
            printf " - Adding entry in [${proj}] ... "
            file=$( (cd ${_destDir}/meta && ls) | grep -ix "${proj}-ijar.list" )
            [ -z "${file}" ] && warn "SKIPPED" && continue

            proj=$( basename $file -ijar.list )
            ( cat ${_destDir}/meta/${file} | grep -qw ${name} ) && warn "SKIPPED" && continue

            # Mark for copy and add in project file
            to_copy=0 && echo "${name}|${version}" >> ${_destDir}/meta/${file}

            success "DONE"
        done

        file="${_destDir}/rep/ilib/$( basename ${jar} )"
        if [ -e ${file} ]; then
            diff -q ${file} ${_srcDir}/${jar} >/dev/null || to_copy=0
        fi

        [ ${to_copy} -eq 0 ] && cp ${_srcDir}/${jar} ${_destDir}/rep/ilib
    done

    return 0
}

# Merge configuration files
merge_conf() {

    local _workDir=${1}
    local _destDir=${I}

    local file name conf cname package projdir projects proj dir cprojects

    header "Merging the custom configurations"
    echo "Operation directive :: Merges the config files inside .../src/main/conf to project files including their names"

    for file in $( cd ${_workDir}/jars && find . -path '*/src/main/conf/*.conf' )
    do
        proj=${file:2}
        proj=${proj%%/*}

        # Ignore if project not in settings.gradle
        ( isProjectIncluded ${_workDir} ${proj} ) || continue

        projdir=$( cd ${_workDir}/jars/${proj} && pwd )
        [ ! -e ${projdir}/build.gradle ] \
            && err "Warning: [${proj}] not a project directory. SKIPPED" && continue

        projects=( $( cd ${projdir} && gradle -q bundles 2>/dev/null ) )
        [ $? -ne 0 ] && err "Warning: [${proj}] not properly setup. SKIPPED" && continue

        name=$(basename $file)
        for conf in $( grep -wl "${name}" ${_destDir}/rep/conf/* 2>/dev/null )
        do
            cprojects=${projects}
            cname=$(basename ${conf})

            for package in $( grep -wl ${cname} ${_destDir}/meta/*-conf.list )
            do
                pname=$( basename ${package} -conf.list )
                cprojects=( $(echo ${cprojects[@]} | tr ' ' '\012' | grep -v ${pname} ) )

                printf "Copying [${name}] in [${pname}] ... "
                ( grep -qw ${name} ${package} ) \
                    && warn "SKIPPED" && continue \
                    || echo ${name} >> ${package}
                success "DONE"
            done

            for package in ${cprojects[@]}
            do
                pname=${package}
                package=${_destDir}/meta/${pname}-conf.list

                printf "Copying [${name}] in [${pname}] ... "
                ( grep -qw ${name} ${package} ) \
                    && warn "SKIPPED" && continue \
                    || echo ${name} >> ${package}
                success "DONE"
            done
        done

        cp ${_workDir}/jars/$file ${_destDir}/rep/conf
    done

    return 0
}

merge_wars() {

    local _workDir=${1}
    local _destDir=${I}

    local _errFile=$( mktemp )

    local file name dir dest

    header "Merging the war extensions"
    echo "Operation directive :: Merges the wars (existing) with custom extensions"

    for dir in ${_workDir}/extensions/*
    do
        [ ! -e ${dir} ] && continue
        name=$( basename ${dir} )
        file=${_destDir}/rep/apps/${name}.war
        
        printf "Merging war extension [${name}] ... "
        [ ! -e ${file} ] && warn "SKIPPED" && continue

        ( 
            # Go where the product war is present
            cd $( dirname ${file} ) || exit 1

            # Create directory to control what to repack
            mkdir ${name} || exit 1

            # Go inside and unpack the paroduct war
            cd ${name} || exit 1
            jar xf ${file} || exit 1

            # Merge the extension over the product war
            cp -rL ${dir} ../ || exit 1

            # Update the war
            rm -f ${file} || exit 1
            jar cf ${file} * || exit 1

        ) 2> ${_errFile}

        [ $? -ne 0 ] && err "FAILED" && cat ${_errFile} \
            || success "DONE"

        # Make sure the directory is gone
        rm -fr "$( dirname ${file} )/${name}"

    done
    rm -f ${_errFile}

    return 0
}

# Load all the jar projects
isProjectIncluded() {

    local _workDir=$1
    local _proj=$2

    cat ${_workDir}/settings.gradle \
        | grep -v -e '^[ 	]*$' -e '^[ 	]*#' \
        | sed 's/include//g' | tr ',' '\012' \
        | tr -d "' " | tr -d '"' \
        | grep -qx "jars:${_proj}"
}

header() {

    local _message="`echo $*`"
    local _length=${#_message}
    local i=0
    local _tag=`i=0; while [ $i -lt $((_length+4 )) ]; do printf "-"; i=$((i+1)); done`

echo -e '\033[01;36m' 2>/dev/null
cat << EOF
${_tag}
  ${_message}
${_tag}
EOF
echo -en '\033[00m' 2>/dev/null

}

# Executes script if exists
execsh() {
    SCRIPT=`which $1 2>/dev/null`
    if [ ! -z "${SCRIPT}" ]; then
        eval $*
    fi
}

# Prints error messages
unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

unset warn 2>/dev/null
warn() {
	printf '\033[00;32m'
	echo $*
	printf '\033[00m'
}

# -----------------
# Main method call
# -----------------
main $*
[ $? -ne 0 ] && err "ERROR: Merging failed" && exit 1 || exit 0
