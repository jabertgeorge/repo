#!/usr/bin/env bash

main() {

    local clean=""
    [ "$1" = "clean" ] && clean=clean && shift 1

    local mode=""
    [ "$1" = "-i" ] && mode="-i" && shift 1

    # Execute build
    ( ${CXPS_WORKSPACE}/bin/scripts/bld.sh $mode $* ) || return 1

    # Validating the correctness of metaconf (product + customization)
    ( cd ${CXPS_WORKSPACE}/bin/scripts && ./configure.sh --try ) || return 1

    if [[ "${clean}" = "clean" ]]; then
        if [[ "${mode}" != "-i" ]]; then
            # Execute DB code
            ${CXPS_WORKSPACE}/bin/scripts/dbexec.sh || return 1
        fi
    fi
}

_header() {

    local _message="`echo $*`"
    local _length=${#_message}
    local i=0
    local _tag=`i=0; while [ $i -lt $((_length+4 )) ]; do printf "-"; i=$((i+1)); done`

echo -e '\033[01;36m' 2>/dev/null
cat << EOF
${_tag}
  ${_message}
${_tag}
EOF
echo -en '\033[00m' 2>/dev/null

}

calcTime() {
    local _start=$1
    local _end=$2

    local _diff=$(( $_end - $_start ))

    local _secs=$_diff
    local _mins=$(( $_secs / 60 ))
    local _mins_sec=$(( $_secs % 60 ))
    local _hrs=$(( $_mins / 60 ))
    local _hrs_mins=$(( $_mins % 60 ))

    [ $_hrs -ne 0 ] && echo "$_hrs hours, $_hrs_mins minutes, $_mins_sec seconds" && return 0
    [ $_mins -ne 0 ] && echo "$_mins minutes, $_mins_sec seconds" && return 0
    echo "$_secs seconds"
}

# -----------
# Main block
# -----------

sTime=`date +%s`
main $*
status=$?
[ $status -ne 0 ] && echo "Full Build Failed"
_header "Total time taken: `calcTime ${sTime} $(date +%s)`"
exit $status
