#!/usr/bin/env bash
# Author : Abu

# Get INSTALLD for extraction
INSTALLATIONDIR=${w}/dev-src/installation
[ -z "${INSTALLATIONDIR}" ] && err "Error: full build has not happened yet" && exit 1

# Merges the customization over main product
main() {
    
    local archiveD="${CL5_CUSTOM_ID}-plugins-${CL5_REL_NAME}"

    if [[ "$1" = "--all" ]]; then

        rm -fr ${w}/../${archiveD}

        local pluginD=$( cd ${w}/../ && mkdir ${archiveD} && cd ${archiveD} && pwd )

        jar_plugin ${pluginD}
        db_plugin ${pluginD}
        general_plugin ${pluginD}
        war_plugin ${pluginD}

        if [ "$(ls ${pluginD} | wc -c)" -ne 0 ]; then
            ( cd ${w}/.. && tar czf ${w}/${archiveD}.tgz ${archiveD} )
        fi
        rm -fr ${pluginD}

        return
    fi

    # Merge intallation over product (including db)
    while :
    do
        header "Choose the type of plugin :"
        echo "(1)   WAR Plugin"
        echo "(2)   JAR Plugin"
        echo "(3)   DB Plugin"
        echo "(4)   CDN Plugin"
        echo "(5)   General Plugin"
        echo
        echo "(q)   Exit"

        echo "Choice:"
        read -sn1 choice
        case $choice in 
            1)
                war_plugin ${w}
                ;;
            2) 
                jar_plugin ${w}
                ;;
            3)
                db_plugin ${w}
                ;;
            4)
                cdn_plugin ${w}
                ;;
            5)
                general_plugin ${w}
                ;;
            "q")
                echo "Exiting ... done"
                return
                ;;
        esac
    done
}

war_plugin() {

    local _workDir=${w}/work
    [ ! -z "$_workDir" ] && rm -rf "$_workDir"

    local _destDir=${1}
    local _errFile=$( mktemp )
    local _repD=${INSTALLATIONDIR}/rep
    local _depsD=${INSTALLATIONDIR}/deps
    local _metaD=${INSTALLATIONDIR}/meta

    header "Creating war plugin"

    for _file in ${_repD}/apps/*
    do
        [ ! -e ${_file} ] && continue

        mkdir -p ${_workDir}/meta
        mkdir -p ${_workDir}/rep/apps
        mkdir -p ${_workDir}/rep/elib
        mkdir -p ${_workDir}/rep/ilib
        mkdir -p ${_workDir}/rep/db

        name=$( basename ${_file} )
        appName=$( basename ${name} .war )
        revision=$( cat ${_metaD}/${appName}.revision )

        ( 
            # Go to the war dir
            mkdir -p ${_workDir}/${appName}
            cd ${_workDir}/${appName}

            # Unpack the war
            jar xf ${_file} || return 1
            
            # create libs directory
            mkdir -p WEB-INF/lib

            # Copy ilibs
            while read jar
            do
                jar=$( echo $jar | cut -d'|' -f1 )
                ln -fs ${_repD}/ilib/${jar} WEB-INF/lib/${jar}
            done < ${_metaD}/${appName}-ijar.list
            touch ${_workDir}/meta/${appName}-ijar.list

            # Copy elibs
            while read jar
            do
                ln -fs ${_repD}/elib/${jar} WEB-INF/lib/${jar}
            done < ${_metaD}/${appName}-ejar.list
            touch ${_workDir}/meta/${appName}-ejar.list

            echo ${revision} > ${_workDir}/revision
            ln -fs ${_metaD}/${appName}.revision ${_workDir}/meta
            ln -fs ${_metaD}/${appName}-meta.conf ${_workDir}/meta

            printf "Creating war plugin [${name}] ... "
            jar cf ${_workDir}/rep/apps/${name} * && rm -fr ${_workDir}/${appName}
            [ ${status} -ne 0 ] && err "FAILED" && cat ${_errFile} || success "DONE"

        ) 2> ${_errFile}

		[ -d "${_depsD}" ] && ln -fs "${_depsD}" ${_workDir}
		[ -d "${_repD}/conf" ] && ln -fs "${_repD}/conf" ${_workDir}/rep

        status=$?

        if [ $status -eq 0 ]; then
            # Package war plugin in workspace
            archive_name="${revision}-${appName}.tgz"

            ( cd ${_workDir}  && tar -czhf ${_destDir}/${archive_name} * )
            echo "War plugin created in ${_destDir}/${archive_name}"
        fi

        rm -rf ${_workDir}
    done
    [ -z "${status}" ] && warn "No WAR plugins found!"

    return ${status:-0}
}

# Create custom jar plugin
jar_plugin() {

    local _srcDir=${I}/rep/ilib
    local _destDir=${1}
    local copies=0

    local name dir projdir proj jar version
    
    msg "Creating jar plugin"
    [ ! -d ${_srcDir} ] && err "Jar Plugin cannot be created. Nothing found!" && return 0

    # Identify jars which are custom
    for jar in ${_srcDir}/custom-*.jar
    do
        [ ! -e ${jar} ] && continue

        name="custom-lib-plugin.jar"

        version=$( unzip -p ${jar} META-INF/MANIFEST.MF | grep svn-version | cut -d':' -f2 | tr -d ' 	' )
        [ -z "${version}" ] \
            && err "Warning: Unable to fetch revision for [${proj}]. SKIPPED" && continue

        printf "Creating JAR Plugin [${version}-${name}] ... "
        cp ${jar} ${_destDir}/${version}-${name}
        success "DONE"
        copies=$(( copies + 1 ))
    done
    [ ${copies} -eq 0 ] && warn "No libraries found!"

    return 0
}

# Create General plugin
general_plugin() {

    local _srcDir=${I}/rep/conf
    local _destDir=${1}
    local _extention=${w}/bin/releases/${CL5_REL_NAME}/data/dev-src/extensions
    local copies=0

    local name
    
    msg "Creating general plugin"

    local _workDir=${w}/work
    local _genDir=${_workDir}/general
    local revision=$( svnversion -c ${w} | cut -d ':' -f2 | tr -d 'S' )
    [ ! -z "$_workDir" ] && rm -rf "$_workDir"
    mkdir -p ${_genDir}

    # Identify confs which are custom
    for conf in ${_srcDir}/custom-*.conf
    do
        [ ! -e ${conf} ] && continue

        name=$( basename ${conf} )

        ln -fs ${conf} ${_genDir}/${name}
        copies=$(( copies + 1 ))
    done

    if [ -e ${_extention} ]; then
        for dir in ${_extention}/*
        do
            [ ! -e ${dir} ] && continue

            name=$( basename ${dir} )
            ln -fs ${dir} ${_genDir}/${name}
            copies=$(( copies + 1 ))
        done
    fi

    if [ ${copies} -eq 0 ]
    then 
        warn "Nothing found for General Plugin!"
    else
        printf "Creating general plugin ... "
        archive_name="${revision}-general.tgz"
        ( cd ${_workDir}  && tar -czhf ${_destDir}/${archive_name} * )
        success "DONE"
    fi

    rm -rf ${_workDir}

    return 0
}

# Create DB plugin
db_plugin() {

    local _workD="${w}/work"
    local _srcD=${INSTALLATIONDIR}/rep/db
    local _destD=${I}/rep/db

    local _pluginD=${1}
    local _errFile=$( mktemp )
    local revision=$( svnversion -c ${_destDir} | cut -d ':' -f2 | tr -d 'S' )
    local copies=0

    header "Creating db plugin"
    for file in ${_srcD}/*
    do
        [ ! -e ${file} ] && continue
        archive=$( basename ${file} )
        name=$( basename ${archive} .tar )

        printf "Creating DB Plugin [${name}-plugin] ... "

        rm -fr ${_workD}
        mkdir -p ${_workD}
        mkdir -p ${_workD}/old
        ( cd ${_workD}/old && tar xf ${_destD}/${archive} )
        mkdir -p ${_workD}/new
        ( cd ${_workD}/new && tar xf ${file} )
        [ -e ${_workD}/old/${name}/dsm/main/gen/ken-seed.conf ] \
            && cp ${_workD}/old/${name}/dsm/main/gen/ken-seed.conf ${_workD}/new/${name}/dsm/main/gen/ken-seed.conf

        mkdir -p $( cd ${_workD}/old/${name} && find . -type d | sed "s.*${_workD}/merged/${name}-plugin/&g" )

        mkdir -p ${_workD}/merged/${name}-plugin/dsm/main/gen
        touch ${_workD}/merged/${name}-plugin/dsm/main/gen/merged.conf
        execsh dsm --merge ${_workD}/old ${_workD}/new ${_workD}/merged/${name}-plugin/dsm/main/gen/merged.conf
        ( cd ${_workD}/merged && tar cf ${_pluginD}/${revision}-${name}-plugin.tar ${name}-plugin ) || return 1
        rm -fr ${_workD}

        success "DONE"

        copies=$(( copies + 1 ))
    done
    [ ${copies} -eq 0 ] && warn "No DB schemas found!"

    return 0
}

# Create custom CDN plugin
cdn_plugin() {

    local _srcDir=${I}/deps/others/cdn-efm/${CL5_REL_NAME}
    local _destDir=${1}
    local revision=$( svnversion -c ${_destDir} | cut -d ':' -f2 | tr -d 'S' )
    local copies=0

    local name
    
    # Identify cdn which is custom
    header "Creating CDN plugin in ${_destDir}"
    for cdn in ${_srcDir}/*
    do
        [ ! -e ${cdn} ] && continue

        name=$( basename ${cdn} )

        printf "Creating CDN Plugin [${name}] ... "
        cp ${cdn} ${_destDir}/${revision}-${name}
        success "DONE"
        copies=$(( copies + 1 ))
    done
    [ ${copies} -eq 0 ] && warn "No CDN found!"

    return 0
}

# Executes script if exists
execsh() {
    SCRIPT=`which $1`
    if [ ! -z "${SCRIPT}" ]; then
        eval $*
        return $?
    fi
}

header() {

    local _message="`echo $*`"
    local _length=${#_message}
    local i=0
    local _tag=`i=0; while [ $i -lt $((_length+4 )) ]; do printf "-"; i=$((i+1)); done`

echo -e '\033[01;36m' 2>/dev/null
cat << EOF
${_tag}
  ${_message}
${_tag}
EOF
echo -en '\033[00m' 2>/dev/null

}

# Prints a header message
msg() {

    local _message=`echo $*`
    local _length=${#_message}
    local _tag=`echo xx${_message}xx | sed 's/./-/g'`

printf "\033[03;32m
${_tag}
  ${_message}
${_tag}\033[00m
"

}
# Prints error messages
unset err 2>/dev/null
err() {
	printf '\033[01;31m'
	echo $*
	printf '\033[00m'
}

unset success 2>/dev/null
success() {
	printf '\033[01;32m'
	echo $*
	printf '\033[00m'
}

unset warn 2>/dev/null
warn() {
	printf '\033[00;32m'
	echo $*
	printf '\033[00m'
}

# -----------------
# Main method call
# -----------------
main $*
[ $? -ne 0 ] && err "ERROR: Plugin creation failed" && exit 1 || exit 0
