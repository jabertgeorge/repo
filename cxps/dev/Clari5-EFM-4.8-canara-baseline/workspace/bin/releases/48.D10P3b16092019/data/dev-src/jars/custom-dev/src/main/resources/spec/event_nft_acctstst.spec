cxps.events.event.nft_acct_stat{
table-name : EVENT_NFT_ACCTSTAT
  event-mnemonic: NFA
  workspaces : {
    ACCOUNT : account_id,
    }
  event-attributes : {
        host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        channel: {db : true ,raw_name : channel ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        account_id: {db : true ,raw_name : branch_id ,type : "string:20"}
        acct_name: {db : true ,raw_name : acct_name ,type : "string:50"}
        schm_type: {db : true ,raw_name : schm_type ,type : "string:20"}
        schm_code: {db : true ,raw_name : schm_code ,type : "string:20"}
        intial_acct_status: {db : true ,raw_name : acct_status ,type : "string:20"}
        final_acct_status: {db : true ,raw_name : acct_status ,type : "string:20"}
        avl_bal: {db : true ,raw_name : avl_bal ,type :  "number:11,2"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        acct_open_date: {db : true ,raw_name : sys_time ,type : timestamp}
        }
}
