// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_AccountTxnEventMapper extends EventMapper<FT_AccountTxnEvent> {

public FT_AccountTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_AccountTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_AccountTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_AccountTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_AccountTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_AccountTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_AccountTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getContraCustid());
            preparedStatement.setString(i++, obj.getTxnRefId());
            preparedStatement.setString(i++, obj.getOriginIpAddr());
            preparedStatement.setString(i++, obj.getPstdUserId());
            preparedStatement.setString(i++, obj.getAcctSolCity());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setTimestamp(i++, obj.getValueDate());
            preparedStatement.setString(i++, obj.getMnemonicCode());
            preparedStatement.setDouble(i++, obj.getPrevBal());
            preparedStatement.setString(i++, obj.getOnlineBatch());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getTxnDrCr());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getEntityId());
            preparedStatement.setString(i++, obj.getEventSubType());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getTranCode());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setString(i++, obj.getTranSrlNo());
            preparedStatement.setString(i++, obj.getContraGlFlag());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getDpCode());
            preparedStatement.setString(i++, obj.getAcctOwnership());
            preparedStatement.setString(i++, obj.getSchemeType());
            preparedStatement.setString(i++, obj.getTxnBrCity());
            preparedStatement.setString(i++, obj.getPstdFlg());
            preparedStatement.setString(i++, obj.getContraAcctId());
            preparedStatement.setString(i++, obj.getAcctStatus());
            preparedStatement.setString(i++, obj.getEntityType());
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getGlFlag());
            preparedStatement.setString(i++, obj.getIfscCode());
            preparedStatement.setString(i++, obj.getSchemeCode());
            preparedStatement.setString(i++, obj.getPenalInterestFlag());
            preparedStatement.setTimestamp(i++, obj.getInstrmntDate());
            preparedStatement.setString(i++, obj.getTdLiquidationFlag());
            preparedStatement.setDouble(i++, obj.getClrBalAmt());
            preparedStatement.setDouble(i++, obj.getRate());
            preparedStatement.setTimestamp(i++, obj.getAcctClosedDate());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getTranCategory());
            preparedStatement.setString(i++, obj.getInstrmntNum());
            preparedStatement.setString(i++, obj.getEntryUser());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getAcctSolId());
            preparedStatement.setString(i++, obj.getReconId());
            preparedStatement.setString(i++, obj.getEventType());
            preparedStatement.setString(i++, obj.getRefCurrency());
            preparedStatement.setString(i++, obj.getContraBankCode());
            preparedStatement.setString(i++, obj.getContraSolId());
            preparedStatement.setTimestamp(i++, obj.getLastTranDate());
            preparedStatement.setString(i++, obj.getTranParticular());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setTimestamp(i++, obj.getAcctMaturityDate());
            preparedStatement.setString(i++, obj.getTxnBrId());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getInstrumentType());
            preparedStatement.setDouble(i++, obj.getRefTranAmt());
            preparedStatement.setTimestamp(i++, obj.getReActivationDate());
            preparedStatement.setString(i++, obj.getStaffFlag());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_AccountTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_AccountTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_ACCOUNT_TXN"));
        putList = new ArrayList<>();

        for (FT_AccountTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "CONTRA_CUSTID",  obj.getContraCustid());
            p = this.insert(p, "TXN_REF_ID",  obj.getTxnRefId());
            p = this.insert(p, "ORIGIN_IP_ADDR",  obj.getOriginIpAddr());
            p = this.insert(p, "PSTD_USER_ID",  obj.getPstdUserId());
            p = this.insert(p, "ACCT_SOL_CITY",  obj.getAcctSolCity());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "VALUE_DATE", String.valueOf(obj.getValueDate()));
            p = this.insert(p, "MNEMONIC_CODE",  obj.getMnemonicCode());
            p = this.insert(p, "PREV_BAL", String.valueOf(obj.getPrevBal()));
            p = this.insert(p, "ONLINE_BATCH",  obj.getOnlineBatch());
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "TXN_DR_CR",  obj.getTxnDrCr());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "ENTITY_ID",  obj.getEntityId());
            p = this.insert(p, "EVENT_SUB_TYPE",  obj.getEventSubType());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_CODE",  obj.getTranCode());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "TRAN_SRL_NO",  obj.getTranSrlNo());
            p = this.insert(p, "CONTRA_GL_FLAG",  obj.getContraGlFlag());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "DP_CODE",  obj.getDpCode());
            p = this.insert(p, "ACCT_OWNERSHIP",  obj.getAcctOwnership());
            p = this.insert(p, "SCHEME_TYPE",  obj.getSchemeType());
            p = this.insert(p, "TXN_BR_CITY",  obj.getTxnBrCity());
            p = this.insert(p, "PSTD_FLG",  obj.getPstdFlg());
            p = this.insert(p, "CONTRA_ACCT_ID",  obj.getContraAcctId());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            p = this.insert(p, "ENTITY_TYPE",  obj.getEntityType());
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "GL_FLAG",  obj.getGlFlag());
            p = this.insert(p, "IFSC_CODE",  obj.getIfscCode());
            p = this.insert(p, "SCHEME_CODE",  obj.getSchemeCode());
            p = this.insert(p, "PENAL_INTEREST_FLAG",  obj.getPenalInterestFlag());
            p = this.insert(p, "INSTRMNT_DATE", String.valueOf(obj.getInstrmntDate()));
            p = this.insert(p, "TD_LIQUIDATION_FLAG",  obj.getTdLiquidationFlag());
            p = this.insert(p, "CLR_BAL_AMT", String.valueOf(obj.getClrBalAmt()));
            p = this.insert(p, "RATE", String.valueOf(obj.getRate()));
            p = this.insert(p, "ACCT_CLOSED_DATE", String.valueOf(obj.getAcctClosedDate()));
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "TRAN_CATEGORY",  obj.getTranCategory());
            p = this.insert(p, "INSTRMNT_NUM",  obj.getInstrmntNum());
            p = this.insert(p, "ENTRY_USER",  obj.getEntryUser());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "ACCT_SOL_ID",  obj.getAcctSolId());
            p = this.insert(p, "RECON_ID",  obj.getReconId());
            p = this.insert(p, "EVENT_TYPE",  obj.getEventType());
            p = this.insert(p, "REF_CURRENCY",  obj.getRefCurrency());
            p = this.insert(p, "CONTRA_BANK_CODE",  obj.getContraBankCode());
            p = this.insert(p, "CONTRA_SOL_ID",  obj.getContraSolId());
            p = this.insert(p, "LAST_TRAN_DATE", String.valueOf(obj.getLastTranDate()));
            p = this.insert(p, "TRAN_PARTICULAR",  obj.getTranParticular());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ACCT_MATURITY_DATE", String.valueOf(obj.getAcctMaturityDate()));
            p = this.insert(p, "TXN_BR_ID",  obj.getTxnBrId());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "INSTRUMENT_TYPE",  obj.getInstrumentType());
            p = this.insert(p, "REF_TRAN_AMT", String.valueOf(obj.getRefTranAmt()));
            p = this.insert(p, "RE_ACTIVATION_DATE", String.valueOf(obj.getReActivationDate()));
            p = this.insert(p, "STAFF_FLAG",  obj.getStaffFlag());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_ACCOUNT_TXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_AccountTxnEvent obj = new FT_AccountTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setContraCustid(rs.getString("CONTRA_CUSTID"));
    obj.setTxnRefId(rs.getString("TXN_REF_ID"));
    obj.setOriginIpAddr(rs.getString("ORIGIN_IP_ADDR"));
    obj.setPstdUserId(rs.getString("PSTD_USER_ID"));
    obj.setAcctSolCity(rs.getString("ACCT_SOL_CITY"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setValueDate(rs.getTimestamp("VALUE_DATE"));
    obj.setMnemonicCode(rs.getString("MNEMONIC_CODE"));
    obj.setPrevBal(rs.getDouble("PREV_BAL"));
    obj.setOnlineBatch(rs.getString("ONLINE_BATCH"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setTxnDrCr(rs.getString("TXN_DR_CR"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setEntityId(rs.getString("ENTITY_ID"));
    obj.setEventSubType(rs.getString("EVENT_SUB_TYPE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranCode(rs.getString("TRAN_CODE"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setTranSrlNo(rs.getString("TRAN_SRL_NO"));
    obj.setContraGlFlag(rs.getString("CONTRA_GL_FLAG"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setDpCode(rs.getString("DP_CODE"));
    obj.setAcctOwnership(rs.getString("ACCT_OWNERSHIP"));
    obj.setSchemeType(rs.getString("SCHEME_TYPE"));
    obj.setTxnBrCity(rs.getString("TXN_BR_CITY"));
    obj.setPstdFlg(rs.getString("PSTD_FLG"));
    obj.setContraAcctId(rs.getString("CONTRA_ACCT_ID"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));
    obj.setEntityType(rs.getString("ENTITY_TYPE"));
    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setGlFlag(rs.getString("GL_FLAG"));
    obj.setIfscCode(rs.getString("IFSC_CODE"));
    obj.setSchemeCode(rs.getString("SCHEME_CODE"));
    obj.setPenalInterestFlag(rs.getString("PENAL_INTEREST_FLAG"));
    obj.setInstrmntDate(rs.getTimestamp("INSTRMNT_DATE"));
    obj.setTdLiquidationFlag(rs.getString("TD_LIQUIDATION_FLAG"));
    obj.setClrBalAmt(rs.getDouble("CLR_BAL_AMT"));
    obj.setRate(rs.getDouble("RATE"));
    obj.setAcctClosedDate(rs.getTimestamp("ACCT_CLOSED_DATE"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setTranCategory(rs.getString("TRAN_CATEGORY"));
    obj.setInstrmntNum(rs.getString("INSTRMNT_NUM"));
    obj.setEntryUser(rs.getString("ENTRY_USER"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setAcctSolId(rs.getString("ACCT_SOL_ID"));
    obj.setReconId(rs.getString("RECON_ID"));
    obj.setEventType(rs.getString("EVENT_TYPE"));
    obj.setRefCurrency(rs.getString("REF_CURRENCY"));
    obj.setContraBankCode(rs.getString("CONTRA_BANK_CODE"));
    obj.setContraSolId(rs.getString("CONTRA_SOL_ID"));
    obj.setLastTranDate(rs.getTimestamp("LAST_TRAN_DATE"));
    obj.setTranParticular(rs.getString("TRAN_PARTICULAR"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setAcctMaturityDate(rs.getTimestamp("ACCT_MATURITY_DATE"));
    obj.setTxnBrId(rs.getString("TXN_BR_ID"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setInstrumentType(rs.getString("INSTRUMENT_TYPE"));
    obj.setRefTranAmt(rs.getDouble("REF_TRAN_AMT"));
    obj.setReActivationDate(rs.getTimestamp("RE_ACTIVATION_DATE"));
    obj.setStaffFlag(rs.getString("STAFF_FLAG"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_AccountTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_AccountTxnEvent> events;
 FT_AccountTxnEvent obj = new FT_AccountTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_ACCOUNT_TXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_AccountTxnEvent obj = new FT_AccountTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setContraCustid(getColumnValue(rs, "CONTRA_CUSTID"));
            obj.setTxnRefId(getColumnValue(rs, "TXN_REF_ID"));
            obj.setOriginIpAddr(getColumnValue(rs, "ORIGIN_IP_ADDR"));
            obj.setPstdUserId(getColumnValue(rs, "PSTD_USER_ID"));
            obj.setAcctSolCity(getColumnValue(rs, "ACCT_SOL_CITY"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setValueDate(EventHelper.toTimestamp(getColumnValue(rs, "VALUE_DATE")));
            obj.setMnemonicCode(getColumnValue(rs, "MNEMONIC_CODE"));
            obj.setPrevBal(EventHelper.toDouble(getColumnValue(rs, "PREV_BAL")));
            obj.setOnlineBatch(getColumnValue(rs, "ONLINE_BATCH"));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setTxnDrCr(getColumnValue(rs, "TXN_DR_CR"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setEntityId(getColumnValue(rs, "ENTITY_ID"));
            obj.setEventSubType(getColumnValue(rs, "EVENT_SUB_TYPE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranCode(getColumnValue(rs, "TRAN_CODE"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setTranSrlNo(getColumnValue(rs, "TRAN_SRL_NO"));
            obj.setContraGlFlag(getColumnValue(rs, "CONTRA_GL_FLAG"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setDpCode(getColumnValue(rs, "DP_CODE"));
            obj.setAcctOwnership(getColumnValue(rs, "ACCT_OWNERSHIP"));
            obj.setSchemeType(getColumnValue(rs, "SCHEME_TYPE"));
            obj.setTxnBrCity(getColumnValue(rs, "TXN_BR_CITY"));
            obj.setPstdFlg(getColumnValue(rs, "PSTD_FLG"));
            obj.setContraAcctId(getColumnValue(rs, "CONTRA_ACCT_ID"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));
            obj.setEntityType(getColumnValue(rs, "ENTITY_TYPE"));
            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setGlFlag(getColumnValue(rs, "GL_FLAG"));
            obj.setIfscCode(getColumnValue(rs, "IFSC_CODE"));
            obj.setSchemeCode(getColumnValue(rs, "SCHEME_CODE"));
            obj.setPenalInterestFlag(getColumnValue(rs, "PENAL_INTEREST_FLAG"));
            obj.setInstrmntDate(EventHelper.toTimestamp(getColumnValue(rs, "INSTRMNT_DATE")));
            obj.setTdLiquidationFlag(getColumnValue(rs, "TD_LIQUIDATION_FLAG"));
            obj.setClrBalAmt(EventHelper.toDouble(getColumnValue(rs, "CLR_BAL_AMT")));
            obj.setRate(EventHelper.toDouble(getColumnValue(rs, "RATE")));
            obj.setAcctClosedDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_CLOSED_DATE")));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setTranCategory(getColumnValue(rs, "TRAN_CATEGORY"));
            obj.setInstrmntNum(getColumnValue(rs, "INSTRMNT_NUM"));
            obj.setEntryUser(getColumnValue(rs, "ENTRY_USER"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setAcctSolId(getColumnValue(rs, "ACCT_SOL_ID"));
            obj.setReconId(getColumnValue(rs, "RECON_ID"));
            obj.setEventType(getColumnValue(rs, "EVENT_TYPE"));
            obj.setRefCurrency(getColumnValue(rs, "REF_CURRENCY"));
            obj.setContraBankCode(getColumnValue(rs, "CONTRA_BANK_CODE"));
            obj.setContraSolId(getColumnValue(rs, "CONTRA_SOL_ID"));
            obj.setLastTranDate(EventHelper.toTimestamp(getColumnValue(rs, "LAST_TRAN_DATE")));
            obj.setTranParticular(getColumnValue(rs, "TRAN_PARTICULAR"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setAcctMaturityDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_MATURITY_DATE")));
            obj.setTxnBrId(getColumnValue(rs, "TXN_BR_ID"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setInstrumentType(getColumnValue(rs, "INSTRUMENT_TYPE"));
            obj.setRefTranAmt(EventHelper.toDouble(getColumnValue(rs, "REF_TRAN_AMT")));
            obj.setReActivationDate(EventHelper.toTimestamp(getColumnValue(rs, "RE_ACTIVATION_DATE")));
            obj.setStaffFlag(getColumnValue(rs, "STAFF_FLAG"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_ACCOUNT_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"CONTRA_CUSTID\",\"TXN_REF_ID\",\"ORIGIN_IP_ADDR\",\"PSTD_USER_ID\",\"ACCT_SOL_CITY\",\"CHANNEL\",\"VALUE_DATE\",\"MNEMONIC_CODE\",\"PREV_BAL\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"DEVICE_ID\",\"TXN_DR_CR\",\"HOST_ID\",\"ENTITY_ID\",\"EVENT_SUB_TYPE\",\"ACCOUNT_ID\",\"SYS_TIME\",\"BRANCH_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_CODE\",\"ACCT_NAME\",\"CUST_CARD_ID\",\"TRAN_SRL_NO\",\"CONTRA_GL_FLAG\",\"EVENT_NAME\",\"AVL_BAL\",\"DP_CODE\",\"ACCT_OWNERSHIP\",\"SCHEME_TYPE\",\"TXN_BR_CITY\",\"PSTD_FLG\",\"CONTRA_ACCT_ID\",\"ACCT_STATUS\",\"ENTITY_TYPE\",\"ACCT_OPEN_DATE\",\"GL_FLAG\",\"IFSC_CODE\",\"SCHEME_CODE\",\"PENAL_INTEREST_FLAG\",\"INSTRMNT_DATE\",\"TD_LIQUIDATION_FLAG\",\"CLR_BAL_AMT\",\"RATE\",\"ACCT_CLOSED_DATE\",\"TXN_AMT\",\"TRAN_CATEGORY\",\"INSTRMNT_NUM\",\"ENTRY_USER\",\"TRAN_DATE\",\"ACCT_SOL_ID\",\"RECON_ID\",\"EVENT_TYPE\",\"REF_CURRENCY\",\"CONTRA_BANK_CODE\",\"CONTRA_SOL_ID\",\"LAST_TRAN_DATE\",\"TRAN_PARTICULAR\",\"CUST_ID\",\"ACCT_MATURITY_DATE\",\"TXN_BR_ID\",\"TRAN_TYPE\",\"INSTRUMENT_TYPE\",\"REF_TRAN_AMT\",\"RE_ACTIVATION_DATE\",\"STAFF_FLAG\"" +
              " FROM EVENT_FT_ACCOUNT_TXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [CONTRA_CUSTID],[TXN_REF_ID],[ORIGIN_IP_ADDR],[PSTD_USER_ID],[ACCT_SOL_CITY],[CHANNEL],[VALUE_DATE],[MNEMONIC_CODE],[PREV_BAL],[ONLINE_BATCH],[TRAN_SUB_TYPE],[DEVICE_ID],[TXN_DR_CR],[HOST_ID],[ENTITY_ID],[EVENT_SUB_TYPE],[ACCOUNT_ID],[SYS_TIME],[BRANCH_ID],[TRAN_CRNCY_CODE],[TRAN_CODE],[ACCT_NAME],[CUST_CARD_ID],[TRAN_SRL_NO],[CONTRA_GL_FLAG],[EVENT_NAME],[AVL_BAL],[DP_CODE],[ACCT_OWNERSHIP],[SCHEME_TYPE],[TXN_BR_CITY],[PSTD_FLG],[CONTRA_ACCT_ID],[ACCT_STATUS],[ENTITY_TYPE],[ACCT_OPEN_DATE],[GL_FLAG],[IFSC_CODE],[SCHEME_CODE],[PENAL_INTEREST_FLAG],[INSTRMNT_DATE],[TD_LIQUIDATION_FLAG],[CLR_BAL_AMT],[RATE],[ACCT_CLOSED_DATE],[TXN_AMT],[TRAN_CATEGORY],[INSTRMNT_NUM],[ENTRY_USER],[TRAN_DATE],[ACCT_SOL_ID],[RECON_ID],[EVENT_TYPE],[REF_CURRENCY],[CONTRA_BANK_CODE],[CONTRA_SOL_ID],[LAST_TRAN_DATE],[TRAN_PARTICULAR],[CUST_ID],[ACCT_MATURITY_DATE],[TXN_BR_ID],[TRAN_TYPE],[INSTRUMENT_TYPE],[REF_TRAN_AMT],[RE_ACTIVATION_DATE],[STAFF_FLAG]" +
              " FROM EVENT_FT_ACCOUNT_TXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`CONTRA_CUSTID`,`TXN_REF_ID`,`ORIGIN_IP_ADDR`,`PSTD_USER_ID`,`ACCT_SOL_CITY`,`CHANNEL`,`VALUE_DATE`,`MNEMONIC_CODE`,`PREV_BAL`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`DEVICE_ID`,`TXN_DR_CR`,`HOST_ID`,`ENTITY_ID`,`EVENT_SUB_TYPE`,`ACCOUNT_ID`,`SYS_TIME`,`BRANCH_ID`,`TRAN_CRNCY_CODE`,`TRAN_CODE`,`ACCT_NAME`,`CUST_CARD_ID`,`TRAN_SRL_NO`,`CONTRA_GL_FLAG`,`EVENT_NAME`,`AVL_BAL`,`DP_CODE`,`ACCT_OWNERSHIP`,`SCHEME_TYPE`,`TXN_BR_CITY`,`PSTD_FLG`,`CONTRA_ACCT_ID`,`ACCT_STATUS`,`ENTITY_TYPE`,`ACCT_OPEN_DATE`,`GL_FLAG`,`IFSC_CODE`,`SCHEME_CODE`,`PENAL_INTEREST_FLAG`,`INSTRMNT_DATE`,`TD_LIQUIDATION_FLAG`,`CLR_BAL_AMT`,`RATE`,`ACCT_CLOSED_DATE`,`TXN_AMT`,`TRAN_CATEGORY`,`INSTRMNT_NUM`,`ENTRY_USER`,`TRAN_DATE`,`ACCT_SOL_ID`,`RECON_ID`,`EVENT_TYPE`,`REF_CURRENCY`,`CONTRA_BANK_CODE`,`CONTRA_SOL_ID`,`LAST_TRAN_DATE`,`TRAN_PARTICULAR`,`CUST_ID`,`ACCT_MATURITY_DATE`,`TXN_BR_ID`,`TRAN_TYPE`,`INSTRUMENT_TYPE`,`REF_TRAN_AMT`,`RE_ACTIVATION_DATE`,`STAFF_FLAG`" +
              " FROM EVENT_FT_ACCOUNT_TXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_ACCOUNT_TXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"CONTRA_CUSTID\",\"TXN_REF_ID\",\"ORIGIN_IP_ADDR\",\"PSTD_USER_ID\",\"ACCT_SOL_CITY\",\"CHANNEL\",\"VALUE_DATE\",\"MNEMONIC_CODE\",\"PREV_BAL\",\"ONLINE_BATCH\",\"TRAN_SUB_TYPE\",\"DEVICE_ID\",\"TXN_DR_CR\",\"HOST_ID\",\"ENTITY_ID\",\"EVENT_SUB_TYPE\",\"ACCOUNT_ID\",\"SYS_TIME\",\"BRANCH_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_CODE\",\"ACCT_NAME\",\"CUST_CARD_ID\",\"TRAN_SRL_NO\",\"CONTRA_GL_FLAG\",\"EVENT_NAME\",\"AVL_BAL\",\"DP_CODE\",\"ACCT_OWNERSHIP\",\"SCHEME_TYPE\",\"TXN_BR_CITY\",\"PSTD_FLG\",\"CONTRA_ACCT_ID\",\"ACCT_STATUS\",\"ENTITY_TYPE\",\"ACCT_OPEN_DATE\",\"GL_FLAG\",\"IFSC_CODE\",\"SCHEME_CODE\",\"PENAL_INTEREST_FLAG\",\"INSTRMNT_DATE\",\"TD_LIQUIDATION_FLAG\",\"CLR_BAL_AMT\",\"RATE\",\"ACCT_CLOSED_DATE\",\"TXN_AMT\",\"TRAN_CATEGORY\",\"INSTRMNT_NUM\",\"ENTRY_USER\",\"TRAN_DATE\",\"ACCT_SOL_ID\",\"RECON_ID\",\"EVENT_TYPE\",\"REF_CURRENCY\",\"CONTRA_BANK_CODE\",\"CONTRA_SOL_ID\",\"LAST_TRAN_DATE\",\"TRAN_PARTICULAR\",\"CUST_ID\",\"ACCT_MATURITY_DATE\",\"TXN_BR_ID\",\"TRAN_TYPE\",\"INSTRUMENT_TYPE\",\"REF_TRAN_AMT\",\"RE_ACTIVATION_DATE\",\"STAFF_FLAG\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[CONTRA_CUSTID],[TXN_REF_ID],[ORIGIN_IP_ADDR],[PSTD_USER_ID],[ACCT_SOL_CITY],[CHANNEL],[VALUE_DATE],[MNEMONIC_CODE],[PREV_BAL],[ONLINE_BATCH],[TRAN_SUB_TYPE],[DEVICE_ID],[TXN_DR_CR],[HOST_ID],[ENTITY_ID],[EVENT_SUB_TYPE],[ACCOUNT_ID],[SYS_TIME],[BRANCH_ID],[TRAN_CRNCY_CODE],[TRAN_CODE],[ACCT_NAME],[CUST_CARD_ID],[TRAN_SRL_NO],[CONTRA_GL_FLAG],[EVENT_NAME],[AVL_BAL],[DP_CODE],[ACCT_OWNERSHIP],[SCHEME_TYPE],[TXN_BR_CITY],[PSTD_FLG],[CONTRA_ACCT_ID],[ACCT_STATUS],[ENTITY_TYPE],[ACCT_OPEN_DATE],[GL_FLAG],[IFSC_CODE],[SCHEME_CODE],[PENAL_INTEREST_FLAG],[INSTRMNT_DATE],[TD_LIQUIDATION_FLAG],[CLR_BAL_AMT],[RATE],[ACCT_CLOSED_DATE],[TXN_AMT],[TRAN_CATEGORY],[INSTRMNT_NUM],[ENTRY_USER],[TRAN_DATE],[ACCT_SOL_ID],[RECON_ID],[EVENT_TYPE],[REF_CURRENCY],[CONTRA_BANK_CODE],[CONTRA_SOL_ID],[LAST_TRAN_DATE],[TRAN_PARTICULAR],[CUST_ID],[ACCT_MATURITY_DATE],[TXN_BR_ID],[TRAN_TYPE],[INSTRUMENT_TYPE],[REF_TRAN_AMT],[RE_ACTIVATION_DATE],[STAFF_FLAG]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`CONTRA_CUSTID`,`TXN_REF_ID`,`ORIGIN_IP_ADDR`,`PSTD_USER_ID`,`ACCT_SOL_CITY`,`CHANNEL`,`VALUE_DATE`,`MNEMONIC_CODE`,`PREV_BAL`,`ONLINE_BATCH`,`TRAN_SUB_TYPE`,`DEVICE_ID`,`TXN_DR_CR`,`HOST_ID`,`ENTITY_ID`,`EVENT_SUB_TYPE`,`ACCOUNT_ID`,`SYS_TIME`,`BRANCH_ID`,`TRAN_CRNCY_CODE`,`TRAN_CODE`,`ACCT_NAME`,`CUST_CARD_ID`,`TRAN_SRL_NO`,`CONTRA_GL_FLAG`,`EVENT_NAME`,`AVL_BAL`,`DP_CODE`,`ACCT_OWNERSHIP`,`SCHEME_TYPE`,`TXN_BR_CITY`,`PSTD_FLG`,`CONTRA_ACCT_ID`,`ACCT_STATUS`,`ENTITY_TYPE`,`ACCT_OPEN_DATE`,`GL_FLAG`,`IFSC_CODE`,`SCHEME_CODE`,`PENAL_INTEREST_FLAG`,`INSTRMNT_DATE`,`TD_LIQUIDATION_FLAG`,`CLR_BAL_AMT`,`RATE`,`ACCT_CLOSED_DATE`,`TXN_AMT`,`TRAN_CATEGORY`,`INSTRMNT_NUM`,`ENTRY_USER`,`TRAN_DATE`,`ACCT_SOL_ID`,`RECON_ID`,`EVENT_TYPE`,`REF_CURRENCY`,`CONTRA_BANK_CODE`,`CONTRA_SOL_ID`,`LAST_TRAN_DATE`,`TRAN_PARTICULAR`,`CUST_ID`,`ACCT_MATURITY_DATE`,`TXN_BR_ID`,`TRAN_TYPE`,`INSTRUMENT_TYPE`,`REF_TRAN_AMT`,`RE_ACTIVATION_DATE`,`STAFF_FLAG`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

