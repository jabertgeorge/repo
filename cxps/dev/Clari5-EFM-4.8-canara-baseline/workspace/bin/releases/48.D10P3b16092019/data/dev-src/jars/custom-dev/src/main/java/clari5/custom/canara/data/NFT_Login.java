package clari5.custom.canara.data;

public class NFT_Login extends ITableData {

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "NFT_LOGIN";
    private String event_type = "NFT_Login";

    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String CHANNEL ;
    private String USER_ID ;
    private String CUST_ID ;
    private String USER_TYPE ;
    private String DEVICE_ID ;
    private String IP_ADDRESS ;
    private String IP_COUNTRY ;
    private String IP_CITY ;
    private String ERROR_CODE ;
    private String ERROR_DESC ;
    private String SUCC_FAIL_FLG ;
    private String LAST_LOGIN_IP ;
    private String LAST_LOGIN_TS ;
    private String CUST_NAME ;
    private String APP_ID ;
    private String COUNTRY_CODE ;
    private String ACCOUNT_ID ;
    private String MAC_ID ;
    private String MOBILE_NO ;
    private String TRAN_DATE ;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getUSER_TYPE() {
        return USER_TYPE;
    }

    public void setUSER_TYPE(String USER_TYPE) {
        this.USER_TYPE = USER_TYPE;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
    }

    public String getIP_COUNTRY() {
        return IP_COUNTRY;
    }

    public void setIP_COUNTRY(String IP_COUNTRY) {
        this.IP_COUNTRY = IP_COUNTRY;
    }

    public String getIP_CITY() {
        return IP_CITY;
    }

    public void setIP_CITY(String IP_CITY) {
        this.IP_CITY = IP_CITY;
    }

    public String getERROR_CODE() {
        return ERROR_CODE;
    }

    public void setERROR_CODE(String ERROR_CODE) {
        this.ERROR_CODE = ERROR_CODE;
    }

    public String getERROR_DESC() {
        return ERROR_DESC;
    }

    public void setERROR_DESC(String ERROR_DESC) {
        this.ERROR_DESC = ERROR_DESC;
    }

    public String getSUCC_FAIL_FLG() {
        return SUCC_FAIL_FLG;
    }

    public void setSUCC_FAIL_FLG(String SUCC_FAIL_FLG) {
        this.SUCC_FAIL_FLG = SUCC_FAIL_FLG;
    }

    public String getLAST_LOGIN_IP() {
        return LAST_LOGIN_IP;
    }

    public void setLAST_LOGIN_IP(String LAST_LOGIN_IP) {
        this.LAST_LOGIN_IP = LAST_LOGIN_IP;
    }

    public String getLAST_LOGIN_TS() {
        return LAST_LOGIN_TS;
    }

    public void setLAST_LOGIN_TS(String LAST_LOGIN_TS) {
        this.LAST_LOGIN_TS = LAST_LOGIN_TS;
    }

    public String getCUST_NAME() {
        return CUST_NAME;
    }

    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    public String getAPP_ID() {
        return APP_ID;
    }

    public void setAPP_ID(String APP_ID) {
        this.APP_ID = APP_ID;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getMAC_ID() {
        return MAC_ID;
    }

    public void setMAC_ID(String MAC_ID) {
        this.MAC_ID = MAC_ID;
    }

    public String getMOBILE_NO() {
        return MOBILE_NO;
    }

    public void setMOBILE_NO(String MOBILE_NO) {
        this.MOBILE_NO = MOBILE_NO;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

}

