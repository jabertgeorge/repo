package clari5.custom.canara.builder.data;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cxps.apex.utils.CxpsLogger;

public class Message extends ArrayList<VarSource> {
    private static final long serialVersionUID = 4630893256177899900L;
    public static CxpsLogger logger = CxpsLogger.getLogger(Message.class);
    private String eventName;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Message(String eventName) {
        this.setEventName(eventName);
    }

    public Map<String, String> process(JSONObject jrow) throws Exception {
        Map<String, String> msg = new HashMap<String, String>();
        for (VarSource vs : this) {
            String value= vs.process(jrow);
                    if (!"".equals(value) && value!=null) {
                        msg.put(vs.getName(),value);
                    } else {
                        msg.put(vs.getName(), "");
                    }
        }


        return msg;
    }
}
