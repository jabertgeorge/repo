cxps.events.event.nft_limchange{
table-name : EVENT_NFT_LIMCHANGE
  event-mnemonic: NLM
  workspaces : {
    CUSTOMER : cust_id
    }
  event-attributes : {
    	host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        user_id: {db : true ,raw_name : user_id ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        old_limit: {db : true ,raw_name : old_limit ,type : "string:20"}
    	new_limit: {db : true ,raw_name : new_limit ,type : "string:20"}
    	card_number: {db : true ,raw_name : card_number ,type : "string:50"}
    	card_type: {db : true ,raw_name : card_type ,type : "string:20"}
        tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
        lim_change_date: {db : true ,raw_name : lim_change_date ,type : timestamp}

    }
}
