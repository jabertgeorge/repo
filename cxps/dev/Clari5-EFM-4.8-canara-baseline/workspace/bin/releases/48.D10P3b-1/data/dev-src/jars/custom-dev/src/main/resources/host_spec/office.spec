# Generated Code
cxps.noesis.glossary.entity.Office {
	db-name = OFFICE
	generate = false
	db_column_quoted = true
	
	tablespace = CXPS_USERS
	attributes = [ 
		{ name = last-modified-time, column = LAST_MODIFIED_TIME, type = timestamp }
		{ name = office-mgr-dir-tel, column = OfficeMgrDirTel, type = "string:20" }
		{ name = Office-category, column = OfficeCategory, type = "string:50", key=false }
		{ name = branch-groups, column = branchGroups, type = "string:200", key=false }
		{ name = office-fin-counslr-emp-iD, column = OfficeFinCounslrEmpID, type = "string:20", key=false }
		{ name = office-inv-advsr-emp-i-d, column = OfficeInvAdvsrEmpID, type = "string:20", key=false }
		{ name = office-country, column = OfficeCountry, type = "string:25", key=false }
		{ name = office-loan-advsr-emp-i-d, column = OfficeLoanAdvsrEmpID, type = "string:20", key=false }
		{ name = office-s-m-id, column = OfficeSMId, type = "string:50", key=false }
		{ name = del-flag, column = delFlag, type = char}
		{ name = office-type, column = OfficeType, type = "string:50", key=false }
		{ name = office-close-dt, column = OfficeCloseDt, type = "date", key=false }
		{ name = office-open-time, column = OfficeOpenTime, type = "date", key=false }
		{ name = office-mgr-id, column = OfficeMgrId, type = "string:50", key=false }
		{ name = Office-city, column = OfficeCity, type = "string:20", key=false }
		{ name = office-zip-code, column = OfficeZipCode, type = "string:10", key=false }
		{ name = version-no, column = VERSION_NO, type = "number:10" }
		{ name = fax, column = fax, type = "string:30", key=false }
		{ name = office-b-o-m-id, column = OfficeBOMId, type = "string:50", key=false }
		{ name = email-id, column = EmailId, type = "string:50", key=false }
		{ name = office-bus-begin-time, column = OfficeBusBeginTime, type = timestamp }
		{ name = office-bus-end-time, column = OfficeBusEndTime, type = timestamp }
		{ name = office-home-crncy-code, column = OfficeHomeCrncyCode, type = "string:3", key=false }
		{ name = office-parent-id, column = OfficeParentId, type = "string:50", key=false }
		{ name = office-profile, column = OfficeProfile, type = "string:50", key=false }
		{ name = office-close-time, column = OfficeCloseTime, type = timestamp }
		{ name = host-id, column = hostId, type = "string:1", key=false }
		{ name = bank-code, column = BankCode, type = "string:10", key=false }
		{ name = office-address, column = OfficeAddress, type = "string:250", key=false }
		{ name = office-id, column = OfficeId, type = "string:20", key=true }
		{ name = office-r-m-id, column = OfficeRMId, type = "string:50", key=false }
		{ name = mobile-no, column = mobileNo, type = "string:20", key=false }		
		{ name = office-open-dt, column = OfficeOpenDt, type = "date", key=false }
		{ name = del-flg, column = delFlg, type = "string:1", key=false }
		{ name = office-state, column = OfficeState, type = "string:20", key=false }
		{ name = office-telephone, column = OfficeTelephone, type = "string:50", key=false }
		{ name = branch-code, column = BranchCode, type = "string:10", key=false }
		{ name = host-branch-code, column = hostBranchCode, type = "string:10", key=false }
		{ name = queue-enabled, column = queueEnabled, type = "string:1", key=false }
		{ name = office-name, column = OfficeName, type = "string:152", key=false }
		{ name = b-r-a-n-c-h-d-e-s-c, column = BRANCH_DESC, type = "string:500", key=false }
		{ name = r-e-g-i-o-n-d-e-s-c, column = REGION_DESC, type = "string:500", key=false }
		{ name = branch-desc, column = branch_desc, type = "string:300", key=false }
		{ name = region-desc, column = region_desc, type = "string:300", key=false }
	]
}
