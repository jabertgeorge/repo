package clari5.custom.dev;

import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;
import clari5.rda.RDA;
import clari5.platform.util.ECClient;

public class SendEvent {

    public String sendDecideEvent(JSONObject jsonObject)
    {

        try {
            System.out.println("Json value for Decide is "+jsonObject.toString());
            String response =  RDA.process(jsonObject.toString());
            //System.out.println("The respone is " +response);
            return  response;
        }
        catch (Exception e){
            System.out.println("Excpetion");
        }
        return null;
    }

    public String sendEnqueueEvent(JSONObject jsonObject)
    {

        //System.out.println("Json Value for Enqueue "+jsonObject.toString());
        ECClient.configure(null);
        boolean a = ECClient.enqueue("HOST", System.nanoTime() + "" + Thread.currentThread().getId(), jsonObject.toString());
        if(a == true)
        {
           // System.out.println(" the enqueue status is "+a);
            return "SUCESS" ;
        }
        return  null;
    }
}
