/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clari5.custom.dev;

import org.json.JSONObject;
import clari5.custom.dev.SaxPojo;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 *
 * @author lakshmisai
 */
public class FormatJson {

    public JSONObject getJsonObject(SaxPojo saxPojo) {

        JSONObject eventJson = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        eventJson.put("eventtype", "ft");
        eventJson.put("eventsubtype", "cardtxn");
        eventJson.put("event-name", "ft_cardtxn");

        jsonObject.put("host_id", "F");
        jsonObject.put("sys_time", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
        jsonObject.put("event_id", "ft_cardtxn"+System.nanoTime());
        jsonObject.put("channel", saxPojo.getChannel());
        jsonObject.put("user_id", saxPojo.getUserId());
        jsonObject.put("user_type", saxPojo.getUserType());
        jsonObject.put("cust_id", saxPojo.getCustId());
        jsonObject.put("device_id", saxPojo.getDeviceId());
        jsonObject.put("ip_address", saxPojo.getIpAddress());
        jsonObject.put("ip_country", saxPojo.getIpCountry());
        jsonObject.put("ip_city", saxPojo.getIpCity());
        jsonObject.put("cust_name", saxPojo.getCustName());
        jsonObject.put("succ_fail_flg", saxPojo.getSuccFailFlg());
        jsonObject.put("error_code", saxPojo.getErrorCode());
        jsonObject.put("error_desc", saxPojo.getErrorDesc());
        jsonObject.put("dr_account_id", saxPojo.getDrAccountId());
        jsonObject.put("txn_amt", saxPojo.getTxnAmt());
        jsonObject.put("avl_bal", saxPojo.getAvlBal());
        jsonObject.put("cr_account_id", saxPojo.getCrAccountId());
        jsonObject.put("cr_ifsc_code", saxPojo.getCrIFSCCode());
        jsonObject.put("payee_id", saxPojo.getPayeeId());
        jsonObject.put("payee_name", saxPojo.getPayeeName());
        jsonObject.put("tran_type", saxPojo.getTranType());
        jsonObject.put("entry_mode", saxPojo.getEntryMode());
        jsonObject.put("branch_id", saxPojo.getBranchId());
        jsonObject.put("acct_ownership", saxPojo.getAcctOwnership());
        if(saxPojo.getAcctOpenDate().equalsIgnoreCase("NA")) {
            jsonObject.put("acct_open_date", "01-01-2000");
        }
        jsonObject.put("part_tran_type", saxPojo.getPartTranType());
        jsonObject.put("country_code", saxPojo.getCountryCode());
        jsonObject.put("cust_card_id", saxPojo.getCustCardId());
        jsonObject.put("bin", saxPojo.getBin());
        jsonObject.put("mcc_code", saxPojo.getMccCode());
        jsonObject.put("terminal_id", saxPojo.getTerminalId());
        jsonObject.put("merchant_id", saxPojo.getMerchantId());
        jsonObject.put("cust_mob_no", saxPojo.getCustMobNo());
        jsonObject.put("txn_secured_flag", saxPojo.getTxnSecuredFlag());
        jsonObject.put("product_code", saxPojo.getProductCode());
        jsonObject.put("dev_owner_id", saxPojo.getDevOwnerId());
        jsonObject.put("atm_dom_limit", saxPojo.getAtmDomLimit());
        jsonObject.put("atm_intr_limit", saxPojo.getAtmDIntrLimit());
        jsonObject.put("pos_ecom_dom_limit", saxPojo.getPosEcomDomLimit());
        jsonObject.put("pos_ecom_intr_limit", saxPojo.getPosEcomIntrLimit());
        jsonObject.put("tran_date", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
        jsonObject.put("mask_card_no", saxPojo.getMaskCardNo());
        jsonObject.put("state_code", saxPojo.getStateCode());
        jsonObject.put("chip_pin_flg", saxPojo.getChipPinFlag());
       /* jsonObject.put("addEntity1", "");
        jsonObject.put("addEntity2", "");
        jsonObject.put("addEntity3", "");
        jsonObject.put("addEntity4", "");
        jsonObject.put("addEntity5", "");*/
        jsonObject.put("pos_entry_mode", saxPojo.getPosEntryMode());
        jsonObject.put("tran_particular", saxPojo.getTranParticular());

        eventJson.put("msgBody", jsonObject.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

        return eventJson;

    }
}
