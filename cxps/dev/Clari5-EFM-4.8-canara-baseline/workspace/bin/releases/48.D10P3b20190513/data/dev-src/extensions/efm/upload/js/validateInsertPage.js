function ValidateTable() {
        var Table = document.getElementById("table_name");
        if ( Table.value == "") {
              $("#tableWarning").modal("show");
            $("#uploadModal").modal("hide");
            return false;
        }else  $("#uploadModal").modal("show");
}
function ValidateFile(){

var File = document.getElementById("fileName");
        if ( File.value == "") {
         $("#fileWarning").modal("show");

            return false;
        }
        return true;

}


$(document).ready(function(){
      	myAjaxCall = function(data, callback){
        $.ajax({
            url: window.location.origin + "/efm/TableDetails",
            cache: false,
            method: "POST",
            data : data,
            type : "text/json",
            success: function (data) {
                try {
                    console.log("Response Received");

                    if( callback ){
                    callback(JSON.parse(data)); 
                  }
                }
                catch(err) {
                   console.log("JSON not in proper format");
                }    
            },
            error: function (a,b,c) {
                console.log("Error in ajax call");
            }
        });    
      }

     //This is called when table name is selected  
      $("#table_name").change(function(){      
        var tableName=$("#table_name").val();
        var payload= {
          "tableName" : tableName
      };
        myAjaxCall(payload,insertCallBack);
     });
   

    });    
    function insertCallBack(resp){
      var html="";
      html +="<table class='table table-bordered table-hover id='Desctable'>";
      html +="<tr>";
      html +="<th class='bg-info'> Column Name </th>";
      html +="<th class='bg-info'> Column Type </th>";
      html +="<th class='bg-info'> Column Size </th>";
      html +="</tr>";
      
      for(var index in resp){
        html +="<tr>";
        html +="<td>"+resp[index].coulmnName +"</td>";
        html +="<td>"+resp[index].columnType +"</td>";
        html +="<td>"+resp[index].columnSize +"</td>";
        html +="</tr>";
      }
      
      $("#display").html(html);
    }
