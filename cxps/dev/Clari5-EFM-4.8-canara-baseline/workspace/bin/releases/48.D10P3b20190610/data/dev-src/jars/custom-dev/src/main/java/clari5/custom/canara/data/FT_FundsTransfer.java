package clari5.custom.canara.data;

public class FT_FundsTransfer extends ITableData{

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "FT_FUNDSTRANSFER";
    private String event_type = "FT_FundsTransfer";

    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String CHANNEL ;
    private String USER_ID ;
    private String USER_TYPE ;
    private String CUST_ID ;
    private String BENIFICARY_ID ;
    private String BENIFICARY_NAME ;
    private String DEVICE_ID ;
    private String IP_ADDRESS ;
    private String IP_COUNTRY ;
    private String IP_CITY ;
    private String CUST_NAME ;
    private String SUCC_FAIL_FLG ;
    private String ERROR_CODE ;
    private String ERROR_DESC ;
    private String DR_ACCOUNT_ID ;
    private String TXN_AMT ;
    private String AVL_BAL ;
    private String CR_ACCOUNT_ID ;
    private String CR_IFSC_CODE ;
    private String PAYEE_ID ;
    private String PAYEE_NAME ;
    private String TXN_TYPE ;
    private String ACCOUNT_OWNERSHIP ;
    private String ACCT_OPEN_DATE ;
    private String AVG_TXN_AMT ;
    private String PART_TRAN_TYPE ;
    private String DAILY_LIMIT ;
    private String MCC_CODE ;
    private String TRAN_DATE ;
    private String MOBILE_NO ;
    private String COUNTRY_CODE ;
    private String TRAN_MODE ;
    private String MERCHANT_ID ;
    private String EFF_AVL_BAL ;
    private String NET_BANKING_TYPE ;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSER_TYPE() {
        return USER_TYPE;
    }

    public void setUSER_TYPE(String USER_TYPE) {
        this.USER_TYPE = USER_TYPE;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getBENIFICARY_ID() {
        return BENIFICARY_ID;
    }

    public void setBENIFICARY_ID(String BENIFICARY_ID) {
        this.BENIFICARY_ID = BENIFICARY_ID;
    }

    public String getBENIFICARY_NAME() {
        return BENIFICARY_NAME;
    }

    public void setBENIFICARY_NAME(String BENIFICARY_NAME) {
        this.BENIFICARY_NAME = BENIFICARY_NAME;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
    }

    public String getIP_COUNTRY() {
        return IP_COUNTRY;
    }

    public void setIP_COUNTRY(String IP_COUNTRY) {
        this.IP_COUNTRY = IP_COUNTRY;
    }

    public String getIP_CITY() {
        return IP_CITY;
    }

    public void setIP_CITY(String IP_CITY) {
        this.IP_CITY = IP_CITY;
    }

    public String getCUST_NAME() {
        return CUST_NAME;
    }

    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    public String getSUCC_FAIL_FLG() {
        return SUCC_FAIL_FLG;
    }

    public void setSUCC_FAIL_FLG(String SUCC_FAIL_FLG) {
        this.SUCC_FAIL_FLG = SUCC_FAIL_FLG;
    }

    public String getERROR_CODE() {
        return ERROR_CODE;
    }

    public void setERROR_CODE(String ERROR_CODE) {
        this.ERROR_CODE = ERROR_CODE;
    }

    public String getERROR_DESC() {
        return ERROR_DESC;
    }

    public void setERROR_DESC(String ERROR_DESC) {
        this.ERROR_DESC = ERROR_DESC;
    }

    public String getDR_ACCOUNT_ID() {
        return DR_ACCOUNT_ID;
    }

    public void setDR_ACCOUNT_ID(String DR_ACCOUNT_ID) {
        this.DR_ACCOUNT_ID = DR_ACCOUNT_ID;
    }

    public String getTXN_AMT() {
        return TXN_AMT;
    }

    public void setTXN_AMT(String TXN_AMT) {
        this.TXN_AMT = TXN_AMT;
    }

    public String getAVL_BAL() {
        return AVL_BAL;
    }

    public void setAVL_BAL(String AVL_BAL) {
        this.AVL_BAL = AVL_BAL;
    }

    public String getCR_ACCOUNT_ID() {
        return CR_ACCOUNT_ID;
    }

    public void setCR_ACCOUNT_ID(String CR_ACCOUNT_ID) {
        this.CR_ACCOUNT_ID = CR_ACCOUNT_ID;
    }

    public String getCR_IFSC_CODE() {
        return CR_IFSC_CODE;
    }

    public void setCR_IFSC_CODE(String CR_IFSC_CODE) {
        this.CR_IFSC_CODE = CR_IFSC_CODE;
    }

    public String getPAYEE_ID() {
        return PAYEE_ID;
    }

    public void setPAYEE_ID(String PAYEE_ID) {
        this.PAYEE_ID = PAYEE_ID;
    }

    public String getPAYEE_NAME() {
        return PAYEE_NAME;
    }

    public void setPAYEE_NAME(String PAYEE_NAME) {
        this.PAYEE_NAME = PAYEE_NAME;
    }

    public String getTXN_TYPE() {
        return TXN_TYPE;
    }

    public void setTXN_TYPE(String TXN_TYPE) {
        this.TXN_TYPE = TXN_TYPE;
    }

    public String getACCOUNT_OWNERSHIP() {
        return ACCOUNT_OWNERSHIP;
    }

    public void setACCOUNT_OWNERSHIP(String ACCOUNT_OWNERSHIP) {
        this.ACCOUNT_OWNERSHIP = ACCOUNT_OWNERSHIP;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getAVG_TXN_AMT() {
        return AVG_TXN_AMT;
    }

    public void setAVG_TXN_AMT(String AVG_TXN_AMT) {
        this.AVG_TXN_AMT = AVG_TXN_AMT;
    }

    public String getPART_TRAN_TYPE() {
        return PART_TRAN_TYPE;
    }

    public void setPART_TRAN_TYPE(String PART_TRAN_TYPE) {
        this.PART_TRAN_TYPE = PART_TRAN_TYPE;
    }

    public String getDAILY_LIMIT() {
        return DAILY_LIMIT;
    }

    public void setDAILY_LIMIT(String DAILY_LIMIT) {
        this.DAILY_LIMIT = DAILY_LIMIT;
    }

    public String getMCC_CODE() {
        return MCC_CODE;
    }

    public void setMCC_CODE(String MCC_CODE) {
        this.MCC_CODE = MCC_CODE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getMOBILE_NO() {
        return MOBILE_NO;
    }

    public void setMOBILE_NO(String MOBILE_NO) {
        this.MOBILE_NO = MOBILE_NO;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getTRAN_MODE() {
        return TRAN_MODE;
    }

    public void setTRAN_MODE(String TRAN_MODE) {
        this.TRAN_MODE = TRAN_MODE;
    }

    public String getMERCHANT_ID() {
        return MERCHANT_ID;
    }

    public void setMERCHANT_ID(String MERCHANT_ID) {
        this.MERCHANT_ID = MERCHANT_ID;
    }

    public String getEFF_AVL_BAL() {
        return EFF_AVL_BAL;
    }

    public void setEFF_AVL_BAL(String EFF_AVL_BAL) {
        this.EFF_AVL_BAL = EFF_AVL_BAL;
    }

    public String getNET_BANKING_TYPE() {
        return NET_BANKING_TYPE;
    }

    public void setNET_BANKING_TYPE(String NET_BANKING_TYPE) {
        this.NET_BANKING_TYPE = NET_BANKING_TYPE;
    }



}

