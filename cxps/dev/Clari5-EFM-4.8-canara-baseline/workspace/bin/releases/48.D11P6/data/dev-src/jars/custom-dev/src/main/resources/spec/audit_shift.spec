cxps.noesis.glossary.entity.audit_shift{
	db-name = AUDIT_SHIFT
	generate = false
	db_column_quoted = true
	tablespace = CXPS_USERS
	attributes = [
		{ name = OPERATION, column = OPERATION, type = "string:50"}
		{ name = LOGIN_USER,   column = LOGIN_USER,    type = "string:50" }
		{ name = SHIFT_USER,   column = SHIFT_USER,    type = "string:50" }
		{ name = CREATED_ON,   column = CREATED_ON,    type : "string:50" }

		         ]
}
