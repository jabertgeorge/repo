package clari5.custom.dev.service;

import clari5.custom.dev.service.struct.DerivatorResult;
import clari5.custom.dev.service.util.EventServiceUtility;
import clari5.platform.applayer.Clari5;
import clari5.platform.fileq.Clari5Payload;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.logger.ICXLog;
import clari5.platform.util.CxJson;
import clari5.trace.HostEventDirector;
import clari5.trace.hedstructs.DerivedEvent;
import clari5.trace.wsrel.WsRel;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.shared.IWSEvent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Author: Shashank Devisetty
 * Organization: CustomerXPs
 * Created On: 10/04/2019
 * Reviewed By:
 */

@Api(value = "/event/add")
@WebServlet("/event/add")
public class HEDEventService extends HttpServlet {
    private static final CxpsLogger logger = CxpsLogger.getLogger(HEDEventService.class);
    static {
        logger.debug("HED Event Service Loaded successfully");
    }

    @ApiOperation(
            value = "Add event to HED",
            produces = "Workspace Keys, Transformed Event and Noesis Payload in JSON format",
            consumes = "Raw event as input in plain text format",
            response = String.class
    )
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String payload = EventServiceUtility.extractPayload(request);
        String type = request.getParameter("event-type");
        try {
            if (type != null && !type.trim().isEmpty() && type.trim().toLowerCase().equals("list"))
                EventServiceUtility.writeResponse(response, addBulkPayload(payload));
            else
                EventServiceUtility.writeResponse(response, addPayload(EventServiceUtility.convert(payload)));
        } catch (Exception e) {
            logger.error("Exception occurred while adding payload [" + payload + "] to HED: " + e.getMessage(), e);
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        logger.error("HED Event Service does not accept GET calls. Only accepts POST call with a payload message which returns derived events along with the workspace keys");
    }

    @SuppressWarnings("unchecked")
    private String addBulkPayload(String payload) throws IOException {
        List<String> messages = (ArrayList) CxJson.json2obj(payload, ArrayList.class);
        List<String> resultList = new ArrayList<>();
        for (String message : messages) resultList.add(addPayload(EventServiceUtility.convert(message)));
        return CxJson.obj2json(resultList);
    }

    private String addPayload(Clari5Payload payload) {
        if (payload == null) return null;
        DerivatorResult result = new DerivatorResult();
        HostEventDirector director = (HostEventDirector) Clari5.getResource("HED");
        List<WsRel> wsRels = new ArrayList<>();
        Set<String> uniqueEventIds = new HashSet<>();
        Map<String, List<IWSEvent>> eventTypes = new HashMap<>();
        Map<String, Map<String, Set<WorkspaceInfo>>> eventTraceMap = new HashMap<>();
        String bid = payload.getInternalId() + "_" + System.currentTimeMillis();
        ICXLog cxlog = CXLog.requestStart(bid);
        cxlog.fenter("API Request start");
        List<DerivedEvent> childEvents = director.deriveEvents(payload, cxlog, uniqueEventIds, wsRels, eventTypes, eventTraceMap);
        director.saveToDB(cxlog, uniqueEventIds, wsRels, eventTypes, eventTraceMap);
        result.setClari5Payloads(director.extractChildren(bid, childEvents));
        List<DerivatorResult.DerivatorObj> derivatorObjList = new LinkedList<>();
        wsRels.forEach(wsRel -> {
            DerivatorResult.DerivatorObj obj = new DerivatorResult.DerivatorObj();
            obj.setWsKey(wsRel.wsKey);
            obj.setEventId(wsRel.eventId);
            obj.setEventName(wsRel.eventName);
            derivatorObjList.add(obj);
        });
        result.setDerivatorObjList(derivatorObjList);
        List<IWSEvent> list = new ArrayList<>();
        eventTypes.forEach((key, value) -> list.addAll(value));
        result.setEventList(list);
        return CxJson.obj2json(result);
    }
}
