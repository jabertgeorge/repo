package clari5.custom.canara.data;


public class FT_Remittance extends ITableData {

    private String tableName = "FT_REMITTANCE";
    private String event_type = "FT_Remittance";
    private String ID;
    private String REM_ADD1;
    private String REM_TYPE;
    private String ACCT_OPEN_DATE;
    private String REM_ADD3;
    private String REM_ADD2;
    private String PURPOSE_CODE;
    private String USD_AMOUNT;
    private String BEN_CITY;
    private String BEN_CNTRY_CODE;
    private String BEN_ADD3;
    private String PURPOSE_DESC;
    private String BEN_ADD2;
    private String REM_ACCT_NO;
    private String INR_AMT;
    private String REM_NAME;
    private String BEN_ADD1;
    private String ACCOUNT_CATAGORY;
    private String BEN_BIC;
    private String HOST_ID;
    private String TRAN_REF_NO;
    private String TRAN_AMT;
    private String REM_CUST_ID;
    private String IMP_EXP_ADVANCE;
    private String BEN_NAME;
    private String SYSTEM_TYPE;
    private String SYS_TIME;
    private String BRANCH_ID;
    private String BEN_ACCT_NO;
    private String BEN_CUST_ID;
    private String REM_CNTRY_CODE;
    private String TRAN_DATE;
    private String SYSTEM;
    private String TRAN_YEAR;
    private String TRAN_CURR;
    private String REM_CITY;
    private String REM_BIC;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getREM_ADD1() {
        return REM_ADD1;
    }

    public void setREM_ADD1(String REM_ADD1) {
        this.REM_ADD1 = REM_ADD1;
    }

    public String getREM_TYPE() {
        return REM_TYPE;
    }

    public void setREM_TYPE(String REM_TYPE) {
        this.REM_TYPE = REM_TYPE;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getREM_ADD3() {
        return REM_ADD3;
    }

    public void setREM_ADD3(String REM_ADD3) {
        this.REM_ADD3 = REM_ADD3;
    }

    public String getREM_ADD2() {
        return REM_ADD2;
    }

    public void setREM_ADD2(String REM_ADD2) {
        this.REM_ADD2 = REM_ADD2;
    }

    public String getPURPOSE_CODE() {
        return PURPOSE_CODE;
    }

    public void setPURPOSE_CODE(String PURPOSE_CODE) {
        this.PURPOSE_CODE = PURPOSE_CODE;
    }

    public String getUSD_AMOUNT() {
        return USD_AMOUNT;
    }

    public void setUSD_AMOUNT(String USD_AMOUNT) {
        this.USD_AMOUNT = USD_AMOUNT;
    }

    public String getBEN_CITY() {
        return BEN_CITY;
    }

    public void setBEN_CITY(String BEN_CITY) {
        this.BEN_CITY = BEN_CITY;
    }

    public String getBEN_CNTRY_CODE() {
        return BEN_CNTRY_CODE;
    }

    public void setBEN_CNTRY_CODE(String BEN_CNTRY_CODE) {
        this.BEN_CNTRY_CODE = BEN_CNTRY_CODE;
    }

    public String getBEN_ADD3() {
        return BEN_ADD3;
    }

    public void setBEN_ADD3(String BEN_ADD3) {
        this.BEN_ADD3 = BEN_ADD3;
    }

    public String getPURPOSE_DESC() {
        return PURPOSE_DESC;
    }

    public void setPURPOSE_DESC(String PURPOSE_DESC) {
        this.PURPOSE_DESC = PURPOSE_DESC;
    }

    public String getBEN_ADD2() {
        return BEN_ADD2;
    }

    public void setBEN_ADD2(String BEN_ADD2) {
        this.BEN_ADD2 = BEN_ADD2;
    }

    public String getREM_ACCT_NO() {
        return REM_ACCT_NO;
    }

    public void setREM_ACCT_NO(String REM_ACCT_NO) {
        this.REM_ACCT_NO = REM_ACCT_NO;
    }

    public String getINR_AMT() {
        return INR_AMT;
    }

    public void setINR_AMT(String INR_AMT) {
        this.INR_AMT = INR_AMT;
    }

    public String getREM_NAME() {
        return REM_NAME;
    }

    public void setREM_NAME(String REM_NAME) {
        this.REM_NAME = REM_NAME;
    }

    public String getBEN_ADD1() {
        return BEN_ADD1;
    }

    public void setBEN_ADD1(String BEN_ADD1) {
        this.BEN_ADD1 = BEN_ADD1;
    }

    public String getACCOUNT_CATAGORY() {
        return ACCOUNT_CATAGORY;
    }

    public void setACCOUNT_CATAGORY(String ACCOUNT_CATAGORY) {
        this.ACCOUNT_CATAGORY = ACCOUNT_CATAGORY;
    }

    public String getBEN_BIC() {
        return BEN_BIC;
    }

    public void setBEN_BIC(String BEN_BIC) {
        this.BEN_BIC = BEN_BIC;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getTRAN_REF_NO() {
        return TRAN_REF_NO;
    }

    public void setTRAN_REF_NO(String TRAN_REF_NO) {
        this.TRAN_REF_NO = TRAN_REF_NO;
    }

    public String getTRAN_AMT() {
        return TRAN_AMT;
    }

    public void setTRAN_AMT(String TRAN_AMT) {
        this.TRAN_AMT = TRAN_AMT;
    }

    public String getREM_CUST_ID() {
        return REM_CUST_ID;
    }

    public void setREM_CUST_ID(String REM_CUST_ID) {
        this.REM_CUST_ID = REM_CUST_ID;
    }

    public String getIMP_EXP_ADVANCE() {
        return IMP_EXP_ADVANCE;
    }

    public void setIMP_EXP_ADVANCE(String IMP_EXP_ADVANCE) {
        this.IMP_EXP_ADVANCE = IMP_EXP_ADVANCE;
    }

    public String getBEN_NAME() {
        return BEN_NAME;
    }

    public void setBEN_NAME(String BEN_NAME) {
        this.BEN_NAME = BEN_NAME;
    }

    public String getSYSTEM_TYPE() {
        return SYSTEM_TYPE;
    }

    public void setSYSTEM_TYPE(String SYSTEM_TYPE) {
        this.SYSTEM_TYPE = SYSTEM_TYPE;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getBEN_ACCT_NO() {
        return BEN_ACCT_NO;
    }

    public void setBEN_ACCT_NO(String BEN_ACCT_NO) {
        this.BEN_ACCT_NO = BEN_ACCT_NO;
    }

    public String getBEN_CUST_ID() {
        return BEN_CUST_ID;
    }

    public void setBEN_CUST_ID(String BEN_CUST_ID) {
        this.BEN_CUST_ID = BEN_CUST_ID;
    }

    public String getREM_CNTRY_CODE() {
        return REM_CNTRY_CODE;
    }

    public void setREM_CNTRY_CODE(String REM_CNTRY_CODE) {
        this.REM_CNTRY_CODE = REM_CNTRY_CODE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getSYSTEM() {
        return SYSTEM;
    }

    public void setSYSTEM(String SYSTEM) {
        this.SYSTEM = SYSTEM;
    }

    public String getTRAN_YEAR() {
        return TRAN_YEAR;
    }

    public void setTRAN_YEAR(String TRAN_YEAR) {
        this.TRAN_YEAR = TRAN_YEAR;
    }

    public String getTRAN_CURR() {
        return TRAN_CURR;
    }

    public void setTRAN_CURR(String TRAN_CURR) {
        this.TRAN_CURR = TRAN_CURR;
    }

    public String getREM_CITY() {
        return REM_CITY;
    }

    public void setREM_CITY(String REM_CITY) {
        this.REM_CITY = REM_CITY;
    }

    public String getREM_BIC() {
        return REM_BIC;
    }

    public void setREM_BIC(String REM_BIC) {
        this.REM_BIC = REM_BIC;
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }
}