package clari5.custom.shiftuser.rest;

/*created by vignesh
 * on 15/11/19
 */

import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.Rdbms;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Path("user")
public class ResponseUser {
    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(ResponseUser.class);

    static String operation= null;
    static String currentuser=null;

    @POST
    @Path("/addUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String addUser(String userDetails) {
        //insert to table dont forget to check  if already exists.
        cxpsLogger.info("Userdetails : [ " + userDetails + " ]");
        System.out.println("--userDetails->-" + userDetails);
        int i = 0;
        JSONObject jobj = new JSONObject(userDetails);
        currentuser = jobj.has("username")  ? jobj.getString("username") : "cannot able to fetch Current user";
        String usr = jobj.getString("usr");
        String shift = jobj.getString("sh");
        //String project = jobj.getString("project");
        JSONArray project =jobj.getJSONArray("project");
        int j = project.length();
        List<String> paths = new ArrayList<String>();
        for(int k=0;k<j;k++){
            paths.add((String) project.get(k));
        }
        String vals =  String.join(",", paths);
        String query = "INSERT INTO CMS_USER_SHIFTS (SHIFT,PROJECT,USERNAME,HLMARKFLAG) VALUES (?,?,?,?)";
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, shift);
            //ps.setString(2, project);
            ps.setString(2, vals);
            ps.setString(3, usr);
            ps.setString(4, "N");
            i = ps.executeUpdate();
            cxpsLogger.info("inserted: [ " + i + "]");
            System.out.println("Records inserted" + i);
            con.commit();
            if(i>0){
                operation= "ADDING_USER";
                auditing(currentuser,operation,usr);
            }
            return "true";
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User already exists");
        }
        return "false";
    }


    @POST
    @Path("/updateUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String updateUser(String userhmark) {
        //Update user with only Holiday Marking for the user
        JSONObject jobj = new JSONObject(userhmark);
        System.out.println("--Holidaymark ->-" + userhmark);
        currentuser = jobj.has("username")  ? jobj.getString("username") : "cannot able to fetch Current user";
        String hmark = jobj.getString("hmark");
        String usr = jobj.getString("usr");
        int i = 0;
        String sql = "UPDATE CMS_USER_SHIFTS SET HLMARKFLAG =? WHERE USERNAME = ?";
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, hmark);
            ps.setString(2, usr);
            i = ps.executeUpdate();
            con.commit();
            cxpsLogger.info("inserted: [ " + i + "]");
            System.out.println("inserted: [ " + i + "]");
            if(i>0){
                operation= "MARKING_HOLIDAY";
                auditing(currentuser,operation,usr);
            }
            return "true";
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User has already marked as" + i);
        }
        return "false";
    }

    @POST
    @Path("/deleteUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteUser(String deluser) {
        //Remove the user from the table with id or username
        JSONObject deljson = new JSONObject(deluser);
        currentuser = deljson.has("username")  ? deljson.getString("username") : "cannot able to fetch Current user";
        String del = deljson.getString("usr");
        cxpsLogger.info("Deleting User: [" + del + "]");
        int i = 0;
        try {
            String sql = "DELETE FROM CMS_USER_SHIFTS WHERE USERNAME=?";
            try (Connection con = Rdbms.getConnection();
                 PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1, del);
                i = ps.executeUpdate();
                con.commit();
                cxpsLogger.info("inserted: [ " + i + "]");
                System.out.println("inserted: [ " + i + "]");
                if(i>0){
                    operation= "DELETING";
                    auditing(currentuser,operation,del);
                }
                return "True";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User cannot delete ");
        }
        return "False";
    }

    @GET
    @Path("/getCl5User")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getCl5User() {
        JSONArray userjson = new JSONArray();
        String user = null;
        try {
            //String sql = "select distinct user_id from cl5_user_tbl where role_id in ('Analyst','Admin') and disabled='N'";
            String sql="select distinct user_id from CL5_USER_TBL";
            try (Connection con = Rdbms.getConnection();
                 PreparedStatement ps = con.prepareStatement(sql);
                 ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    user = rs.getString("user_id");
                    userjson.put(user);
                    cxpsLogger.info("USER_ID : [" + user + "]");
                }
                return userjson;
            }
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- Not getting user");
        }
        return userjson;
    }

    @GET
    @Path("/getShiftUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getShiftUser() {

        String shift = "";
        String username = "";
        String hlmrflg = "";
        String project = "";

        JSONObject shiftjsonobject = new JSONObject();

        try {
            String sql = "SELECT  SHIFT,PROJECT,USERNAME,HLMARKFLAG FROM CMS_USER_SHIFTS ORDER BY SHIFT";
            try (Connection con = Rdbms.getConnection();
                 PreparedStatement ps = con.prepareStatement(sql);
                 ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    shift = rs.getString("SHIFT");
                    username = rs.getString("USERNAME");
                    hlmrflg = rs.getString("HLMARKFLAG");
                    project = rs.getString("PROJECT");
                    JSONObject userjsonobject = new JSONObject();
                    //JSONObject projectjson = new JSONObject();
                    JSONObject mrkjsonobject = new JSONObject();
                    JSONArray array = new JSONArray();
                    if (shiftjsonobject.has(shift)) {
                        JSONObject jsonObjectappend = shiftjsonobject.getJSONObject(shift);
                        mrkjsonobject.put("hmark", hlmrflg);
                        mrkjsonobject.put("PROJECT", project);
                        shiftjsonobject.remove(shift);
                        jsonObjectappend.append(username, mrkjsonobject);
                        //Arrays.sort(new String[]{shift});
                        shiftjsonobject.put(shift, jsonObjectappend);
                        cxpsLogger.info("Appended Json : [ " + shiftjsonobject + " ]");
                    } else {
                        mrkjsonobject.put("hmark", hlmrflg);
                        mrkjsonobject.put("PROJECT", project);
                        array.put(mrkjsonobject);
                        userjsonobject.put(username, array);
                        //Arrays.sort(new String[]{shift}
                        shiftjsonobject.put(shift, userjsonobject);

                    }
                }
                cxpsLogger.info("Json object for sending to UI : [ " + shiftjsonobject + " ]");
                return shiftjsonobject.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- coming!! Kindly check.");
        }
        return shiftjsonobject.toString();

    }


    @GET
    @Path("/getShift")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getShift() {
        String sh = "";
        JSONArray shift = new JSONArray();
        try {
            String querry = "SELECT DISTINCT SHIFT FROM CMS_SHIFT_TIMING";
            try (Connection con = Rdbms.getConnection();
                 PreparedStatement ps = con.prepareStatement(querry);
                 ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    sh = rs.getString("SHIFT");
                    shift.put(sh);
                    cxpsLogger.info("User: [" + shift + "]");
                }
                return shift;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return shift;
    }

    @POST
    @Path("/updatetime")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String updatetime(String shifts) {
        JSONObject jsonObject = new JSONObject(shifts);
        String usr= jsonObject.has("username")  ? jsonObject.getString("username") : "cannot able to fetch Current user";
        currentuser = jsonObject.has("username")  ? jsonObject.getString("username") : "cannot able to fetch Current user";
        JSONObject g_shift = jsonObject.getJSONObject("ap0");

        System.out.println("jsonObject json--> " + jsonObject);
        System.out.println("g_shift json--> " + g_shift);
        //String tex0= g_shift.getString("tex0");
        String fromhour0 = String.valueOf(g_shift.get("fromhour0"));
        String tominute0 = String.valueOf(g_shift.get("tominute0"));
        String fromminute0 = String.valueOf(g_shift.get("fromminute0"));
        String tohour0 = String.valueOf(g_shift.get("tohour0"));
        String tex0 = g_shift.getString("tex0");

        System.out.println("g_shift==============>" + g_shift.toString());
        JSONObject shift1 = jsonObject.getJSONObject("ap1");
        String fromhour1 = String.valueOf(shift1.get("fromhour1"));
        String tominute1 = String.valueOf(shift1.get("tominute1"));
        String fromminute1 = String.valueOf(shift1.get("fromminute1"));
        String tohour1 = String.valueOf(shift1.get("tohour1"));
        String tex1 = shift1.getString("tex1");

        System.out.println("shift1==>" + shift1.toString());
        JSONObject shift2 = jsonObject.getJSONObject("ap2");
        String fromhour2 = String.valueOf(shift2.get("fromhour2"));
        String tominute2 = String.valueOf(shift2.get("tominute2"));
        String fromminute2 = String.valueOf(shift2.get("fromminute2"));
        String tohour2 = String.valueOf(shift2.get("tohour2"));
        String tex2 = shift2.getString("tex2");

        System.out.println("shift2=====================>" + shift2.toString());
        JSONObject shift3 = jsonObject.getJSONObject("ap3");
        String fromhour3 = String.valueOf(shift3.get("fromhour3"));
        String tominute3 = String.valueOf(shift3.get("tominute3"));
        String fromminute3 = String.valueOf(shift3.get("fromminute3"));
        String tohour3 = String.valueOf(shift3.get("tohour3"));
        String tex3 = shift3.getString("tex3");
        System.out.println("=================================>shift3" + shift3.toString());

        String shifttotal = fromhour0 + ":" + tominute0 + ":" + fromminute0 + ":" + tohour0 + ":" + tex0 + "," + fromhour1 + ":" + tominute1 + ":" + fromminute1 + ":" + tohour1 + ":" + tex1 + "," + fromhour2 + ":" + tominute2 + ":" + fromminute2 + ":" + tohour2 + ":" + tex2 + "," + fromhour3 + ":" + tominute3 + ":" + fromminute3 + ":" + tohour3 + ":" + tex3;
        System.out.println("Total Timings=====+>" + shifttotal);
        int i = 0;
        String querry = "UPDATE CMS_SHIFT_TIMING SET FROMHOUR = ?,TOMINUTE = ?,FROMMINUTE = ?,TOHOUR = ? WHERE SHIFT=?";
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(querry)) {
            cxpsLogger.info("Shifts : [ " + shifts + " ]");
            String shiftt[] = shifttotal.split(",");
            for (String currShift : shiftt) {
                String time[] = currShift.split(":");
                cxpsLogger.info("time[]" + time);
                ps.setString(1, time[0]);
                ps.setString(2, time[1]);
                ps.setString(3, time[2]);
                ps.setString(4, time[3]);
                ps.setString(5, time[4]);
                i = ps.executeUpdate();
                cxpsLogger.info("Added : [ " + i + "]");
            }
            con.commit();
            cxpsLogger.info("Update time  sucessfully");
            if(i>0){
                operation= "UPDATING_SHIFT_TIME";
                auditing(currentuser,operation,usr);
            }
            return "true";
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("Unable to update Shift timings " + i);
        }
        return "false";
    }

    @GET
    @Path("/showShiftTime")
    @Produces(MediaType.TEXT_PLAIN)
    public String showShiftTime() {
        String shift = null;
        String fhour = null;
        String fmin = null;
        String thour = null;
        String tmin = null;
        ArrayList list = new ArrayList<String>();
        try {
            String sql = "SELECT SHIFT,FROMHOUR,TOMINUTE,FROMMINUTE,TOHOUR FROM CMS_SHIFT_TIMING ";
            try (Connection con = Rdbms.getConnection();
                 PreparedStatement ps = con.prepareStatement(sql);
                 ResultSet rs = ps.executeQuery()) {
                //System.out.println("SHIFT"+"\t"+"FROMHOUR"+"\t"+"TOMINUTE"+"\t"+"FROMMINUTE"+"\t"+"TOHOUR");
                while (rs.next()) {
                    shift = rs.getString("SHIFT");
                    fhour = rs.getString("FROMHOUR");
                    fmin = rs.getString("TOMINUTE");
                    thour = rs.getString("FROMMINUTE");
                    tmin = rs.getString("TOHOUR");
                    //System.out.println(""+rs.getString("SHIFT")+""+rs.getString("FROMHOUR")+""+rs.getString("TOMINUTE")+""+rs.getString("FROMMINUTE")+""+rs.getString("TOHOUR"));
                    list.add(shift + ":" + "" + fhour + ":" + "" + tmin + ":" + "" + fmin + ":" + "" + thour);
                }
                cxpsLogger.info("list: [ " + list + "]");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list.toString();
    }


    //--------------------------------------
    public String auditing( String currentuser,String operation, String usr){
        String sql= "INSERT INTO AUDIT_SHIFT(LOGIN_USER,OPERATION,CREATED_ON,SHIFT_USER) VALUES (?,?,systimestamp,?)";
        try(Connection con =Rdbms.getConnection();
            PreparedStatement ps1 =con.prepareStatement(sql)){
            ps1.setString(1,currentuser);
            ps1.setString(2,operation);
            //ps1.setString(3, String.valueOf(new Timestamp(System.currentTimeMillis())));
            ps1.setString(3, usr);
            ps1.executeUpdate();
            con.commit();
            cxpsLogger.info("Updated In Audit_shift");
        }catch (SQLException e){
            e.printStackTrace();
            cxpsLogger.error("Error in Inserting in Audit shift table");
        }
        return null;
    }


}
