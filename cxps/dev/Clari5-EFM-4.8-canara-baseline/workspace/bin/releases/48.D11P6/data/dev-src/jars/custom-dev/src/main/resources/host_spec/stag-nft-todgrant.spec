clari5.hfdb.entity.nft_todgrant{
attributes = [

        { name = CL5_FLG,  type= "string:10"}
        { name = CREATED_ON,  type= timestamp}
        { name = UPDATED_ON,  type= timestamp}
        { name = ID, type="string:2000", key = true}
        { name = sys_time, type="timestamp"}
        { name = cust_id, type = "string:20"}
        { name = account_id, type = "string:20"}
        { name = gl_code, type = "string:50"}
        { name = scheme_type, type = "string:20"}
        { name = scheme_code, type = "string:20"}
        { name = tod_remarks, type= "string:20"}
        { name = tod_amt, type = "number:18,2"}
        { name = dp_code, type = "string:20"}
        { name = pstd_user_id, type = "string:20"}

        ]
}
