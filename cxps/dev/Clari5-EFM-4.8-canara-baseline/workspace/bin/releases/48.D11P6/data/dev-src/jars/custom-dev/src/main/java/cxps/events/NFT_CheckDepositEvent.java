// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_CHECK_DEPOSIT", Schema="rice")
public class NFT_CheckDepositEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String accountId;
       @Field(size=20) public String schmCode;
       @Field(size=20) public String channel;
       @Field(size=20) public String cheq12;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String custId;
       @Field(size=20) public String unusedLvs;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=11) public Double txnAmt;
       @Field(size=20) public String schmType;
       @Field(size=20) public String hostId;
       @Field(size=20) public String chequeNumber;


    @JsonIgnore
    public ITable<NFT_CheckDepositEvent> t = AEF.getITable(this);

	public NFT_CheckDepositEvent(){}

    public NFT_CheckDepositEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("CheckDeposit");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setAccountId(json.getString("account_id"));
            setSchmCode(json.getString("schm_code"));
            setChannel(json.getString("channel"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setUnusedLvs(json.getString("unused_lvs"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setTxnAmt(EventHelper.toDouble(json.getString("txn_amt")));
            setSchmType(json.getString("schm_type"));
            setHostId(json.getString("host_id"));
            setChequeNumber(json.getString("cheque_number"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setCheq12(cxps.events.CustomFieldDerivator.getFirstTwelveNumber(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "CD"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getAccountId(){ return accountId; }

    public String getSchmCode(){ return schmCode; }

    public String getChannel(){ return channel; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getUnusedLvs(){ return unusedLvs; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getSchmType(){ return schmType; }

    public String getHostId(){ return hostId; }

    public String getChequeNumber(){ return chequeNumber; }
    public String getCheq12(){ return cheq12; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setSchmCode(String val){ this.schmCode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setUnusedLvs(String val){ this.unusedLvs = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setSchmType(String val){ this.schmType = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setChequeNumber(String val){ this.chequeNumber = val; }
    public void setCheq12(String val){ this.cheq12 = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_CheckDepositEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.chequeNumber);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
	System.out.println("Customer wskey is :" +this.custId +this.cheq12 );
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId + this.cheq12);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_CheckDeposit");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "CheckDeposit");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
