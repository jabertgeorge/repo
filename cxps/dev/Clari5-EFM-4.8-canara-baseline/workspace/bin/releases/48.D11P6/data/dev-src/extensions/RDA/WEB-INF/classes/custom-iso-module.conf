iso-core {
    class: clari5.iso.bootstrap.ISOBootstrapManager,
    resource-type: RUNNABLE,
    bulk-size: 500,
    time-out: 10000,
    q-name: ISO-SKIP-Q,
    num-workers: 1,

    event-mapping: {
        iso-default-event-class: "cxps.events.FT_CardTxnEvent",
        iso-mti: {
            1: {
                # based on MTI second digit and first two digits of ISO processing code field, event class would be derived
                iso-processing-codes: {
                    "38": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "10": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "00": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "21": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "01": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "40": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "31": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "20": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "52": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    }
                }
            },
            2: {
                iso-processing-codes: {
                    "00": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": true
                    },
                    "01": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": true
                    },
                    "02": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": true
                    },
                    "31": {
                        "class": "cxps.events.NFT_AcctInquiryEvent",
                        "process": true
                    },
                    "96": {
                        "class": "cxps.events.NFT_RegenEvent",
                        "process": true
                    },
                    "38": {
                        "class": "cxps.events.internal.NFT_AtmDebitPwdReqEvent",
                        "process": false
                    },
                    "10": {
                        "class": "cxps.events.internal.NFT_ATMFacilityCancelEvent",
                        "process": false
                    },
                    "21": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": false
                    },
                    "40": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": false
                    },
                    "20": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": false
                    },
                    "52": {
                        "class": "cxps.events.FT_CardTxnEvent",
                        "process": false
                    }
                }
            },
            4: {
                iso-processing-codes: {
                    "38": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "10": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "00": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "21": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "01": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "40": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "31": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "20": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    },
                    "52": {
                        "class": "cxps.events.FT_AccountTxnEvent",
                        "process": true
                    }
                }
            }
        }
    },

    rda-advice-codes: {
        # Standard ISO Responnse code for 'SUCCESS'
        allow: "00",

        # Standard ISO Responnse code for 'ISSUER DECLINED'
        decline: "01",

        # Standard ISO Responnse code for 'ISSUER DECLINED on special condition'
        challenge: "02"
    },

    iso-error-codes: {
        # Standard ISO Responnse code for 'Transaction Invalid'
        parsing-error: "12",

        # Standard ISO Responnse code for 'General Error'
        processing-error: "06",

        # Standard ISO Responnse code for 'Request still in progress'
        timeout: "09",

        # Standard ISO Responnse code for 'Do not honor'
        skip: "05"
    }

    include "mq-clari5.conf"
}

