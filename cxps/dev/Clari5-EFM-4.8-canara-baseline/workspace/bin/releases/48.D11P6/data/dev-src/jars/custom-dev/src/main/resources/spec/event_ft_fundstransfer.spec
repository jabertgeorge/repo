cxps.events.event.ft_funds_transfer{
table-name : EVENT_FT_FUNDSTRANSFER
  event-mnemonic: FTT
  workspaces : {
    ACCOUNT : "dr_account_id,cr_account_id"
	CUSTOMER : cust_id
	NONCUSTOMER : "mobile_no,dr_account_id,payee_id,benificary_id"
	TERMINAL : "cust_id, cr_account_id"
   }
  event-attributes : {
		host_id:{db : true ,raw_name : host_id, type :  "string:2"}
		channel: {db : true ,raw_name : channel ,type : "string:10"}
		user_id: {db : true ,raw_name : user_id ,type : "string:20"}
		user_type: {db : true ,raw_name : user_type ,type : "string:20"}
		cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
		benificary_id: {db : true ,raw_name : benificary_id ,type : "string:20"}
		benificary_name: {db : true ,raw_name : benificary_name ,type : "string:50"}
		device_id: {db : true ,raw_name : device_id ,type : "string:50"}
		ip_address: {db : true ,raw_name : ip_address ,type : "string:20"}
		ip_country: {db : true ,raw_name : ip_country ,type : "string:20"}
		ip_city: {db : true ,raw_name : ip_city ,type : "string:20"}
		cust_name: {db : true ,raw_name : cust_name ,type : "string:50"}
		succ_fail_flg: {db : true ,raw_name : succ_fail_flg ,type : "string:2"}
		error_code: {db : true ,raw_name : error_code ,type : "string:10"}
		error_desc: {db : true ,raw_name : error_desc ,type : "string:20"}
		dr_account_id: {db : true ,raw_name : dr_account_id ,type : "string:20"}
		txn_amt: {db : true ,raw_name : txn_amt ,type : "number:11,2"}
		avl_bal: {db : true ,raw_name : avl_bal ,type : "number:11,2"}
		sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
		cr_account_id: {db : true ,raw_name : cr_account_id ,type : "string:20"}
		cr_ifsc_code: {db : true ,raw_name : cr_ifsc_code ,type : "string:20"}
		payee_id: {db : true ,raw_name : payee_id ,type : "string:50"}
		payee_name: {db : true ,raw_name : payee_name ,type : "string:50"}
		is_payee_bank_cust: {db : true ,raw_name : is_payee_bank_cust ,type : "string:10"}
		payee_addition_date: {db : true ,raw_name : payee_addition_date ,type : timestamp}
		txn_type: {db : true ,raw_name : txn_type ,type : "string:20"}
		account_ownership: {db : true ,raw_name : account_ownership ,type : "string:20"}
		acct_open_date: {db : true ,raw_name : acct_open_date ,type : timestamp}
		avg_txn_amt: {db : true ,raw_name : avg_txn_amt ,type : "number:11,2"}
		part_tran_type: {db : true ,raw_name : part_tran_type ,type : "string:5"}
		daily_limit: {db : true ,raw_name : daily_limit ,type : "number:11,2"}
		mcc_code: {db : true ,raw_name : mcc_code ,type : "string:20"}
		tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
		mobile_no: {db : true ,raw_name : mobile_no ,type : "string:10"}
		country_code: {db : true ,raw_name : country_code ,type : "string:10",derivation : """ipToCountryCode(ip_address)"""}
		tran_mode: {db : true ,raw_name : tran_mode ,type : "string:20"}
		merchant_id: {db : true ,raw_name : merchant_id ,type : "string:50"}
		eff_avl_bal: {db : true ,raw_name : eff_avl_bal ,type : "number:11,2"}
		net_banking_type: {db : true ,raw_name : net_banking_type ,type : "string:20"}
		sender-vpa: {db : true ,raw_name : sender_vpa ,type : "string:20"}
		receiver-vpa: {db : true ,raw_name : receiver_vpa ,type : "string:20"}
		payee-mob-no: {db : true ,raw_name : payee_mob_no ,type : "string:20"}
		payer-mob-no: {db : true ,raw_name : payer_mob_no ,type : "string:20"}
		payee-ifsc-code: {db : true ,raw_name : payee_ifsc_code ,type : "string:20"}
		payer-ifsc-code: {db : true ,raw_name : payer_ifsc_code ,type : "string:20"}
		is_psp: {db : true ,raw_name : is_psp ,type : "string:20"}
		tran_mode_upi: {db : true ,raw_name : tran_mode_upi ,type : "string:20"}

        }
}
