cxps.events.event.nft-limitchange{  
  table-name : EVENT_NFT_LIMITCHANGE
  event-mnemonic: LC
  workspaces : {
    ACCOUNT : account_id
    CUSTOMER : cust_id 
} 
  event-attributes : {
        host-id: {db:true ,raw_name : host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        account-id: {db : true ,raw_name : account_id ,type : "string:20"}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:20"}
	scheme-type: {db : true ,raw_name : scheme_type ,type : "string:20"}
	scheme-code: {db : true ,raw_name : scheme_code ,type : "string:20"}
        gl-code: {db : true ,raw_name : gl_code ,type : "string:20"}
        previoustotallimit: {db : true ,raw_name : previoustotallimit ,type:"number:20,3"}
        newtotallimit: {db : true ,raw_name : newtotallimit ,type:"number:20,3"}
        limitenhancedornewflg: {db : true ,raw_name : limitenhancedornewflg ,type : "string:20"}
            
}
}

