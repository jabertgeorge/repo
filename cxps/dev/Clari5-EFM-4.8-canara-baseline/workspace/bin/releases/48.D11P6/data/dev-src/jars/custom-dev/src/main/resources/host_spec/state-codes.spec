cxps.noesis.glossary.entity.state_codes {
        db-name = STATE_CODES
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = state-code, column = State_code, type = "string:6", key = true }
                { name = description, column = Description, type = "string:50" }
        ]
}

