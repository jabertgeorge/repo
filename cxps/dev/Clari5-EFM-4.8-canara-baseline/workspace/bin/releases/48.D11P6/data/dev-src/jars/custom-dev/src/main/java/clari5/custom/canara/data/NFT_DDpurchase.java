package clari5.custom.canara.data;

public class NFT_DDpurchase extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_DD_PURCHASE";
    private String event_type = "NFT_DdPurchase";

    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String HOST_ID;
    private String SYS_TIME;
    private String DD_ISSUE_AMT;
    private String DD_ISSUE_ACCT;
    private String SCHM_TYPE;
    private String SCHM_CODE;
    private String DD_ISSUE_CUSTID;
    private String ACCT_NAME;
    private String ACCT_SOL_ID;
    private String ACCT_OWNERSHIP;
    private String ACCT_OPEN_DATE;
    private String TRAN_CRNCY_CODE;
    private String REF_TXN_AMT;
    private String BEN_NAME;
    private String PSTD_USER_ID;
    private String TRAN_CATEGORY;
    private String DD_ISSUE_DATE;
    private String DD_NUM;
    private String PART_TRAN_SRL_NUM;
    private String BEN_ACCOUNT_ID;
    private String BEN_BANK_CODE;
    private String BEN_SOL_ID;
    private String VALUE_DATE;
    private String ACCT_STATUS;
    private String TXN_BR_ID;
    private String MNEMONIC_CODE;
    private String TRAN_DATE;
    private String DD_PUR_NAME;
    private String TRAN_CODE;


    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getDD_ISSUE_AMT() {
        return DD_ISSUE_AMT;
    }

    public void setDD_ISSUE_AMT(String DD_ISSUE_AMT) {
        this.DD_ISSUE_AMT = DD_ISSUE_AMT;
    }

    public String getDD_ISSUE_ACCT() {
        return DD_ISSUE_ACCT;
    }

    public void setDD_ISSUE_ACCT(String DD_ISSUE_ACCT) {
        this.DD_ISSUE_ACCT = DD_ISSUE_ACCT;
    }

    public String getSCHM_TYPE() {
        return SCHM_TYPE;
    }

    public void setSCHM_TYPE(String SCHM_TYPE) {
        this.SCHM_TYPE = SCHM_TYPE;
    }

    public String getSCHM_CODE() {
        return SCHM_CODE;
    }

    public void setSCHM_CODE(String SCHM_CODE) {
        this.SCHM_CODE = SCHM_CODE;
    }

    public String getDD_ISSUE_CUSTID() {
        return DD_ISSUE_CUSTID;
    }

    public void setDD_ISSUE_CUSTID(String DD_ISSUE_CUSTID) {
        this.DD_ISSUE_CUSTID = DD_ISSUE_CUSTID;
    }

    public String getACCT_NAME() {
        return ACCT_NAME;
    }

    public void setACCT_NAME(String ACCT_NAME) {
        this.ACCT_NAME = ACCT_NAME;
    }

    public String getACCT_SOL_ID() {
        return ACCT_SOL_ID;
    }

    public void setACCT_SOL_ID(String ACCT_SOL_ID) {
        this.ACCT_SOL_ID = ACCT_SOL_ID;
    }

    public String getACCT_OWNERSHIP() {
        return ACCT_OWNERSHIP;
    }

    public void setACCT_OWNERSHIP(String ACCT_OWNERSHIP) {
        this.ACCT_OWNERSHIP = ACCT_OWNERSHIP;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getTRAN_CRNCY_CODE() {
        return TRAN_CRNCY_CODE;
    }

    public void setTRAN_CRNCY_CODE(String TRAN_CRNCY_CODE) {
        this.TRAN_CRNCY_CODE = TRAN_CRNCY_CODE;
    }

    public String getREF_TXN_AMT() {
        return REF_TXN_AMT;
    }

    public void setREF_TXN_AMT(String REF_TXN_AMT) {
        this.REF_TXN_AMT = REF_TXN_AMT;
    }

    public String getBEN_NAME() {
        return BEN_NAME;
    }

    public void setBEN_NAME(String BEN_NAME) {
        this.BEN_NAME = BEN_NAME;
    }

    public String getPSTD_USER_ID() {
        return PSTD_USER_ID;
    }

    public void setPSTD_USER_ID(String PSTD_USER_ID) {
        this.PSTD_USER_ID = PSTD_USER_ID;
    }

    public String getTRAN_CATEGORY() {
        return TRAN_CATEGORY;
    }

    public void setTRAN_CATEGORY(String TRAN_CATEGORY) {
        this.TRAN_CATEGORY = TRAN_CATEGORY;
    }

    public String getDD_ISSUE_DATE() {
        return DD_ISSUE_DATE;
    }

    public void setDD_ISSUE_DATE(String DD_ISSUE_DATE) {
        this.DD_ISSUE_DATE = DD_ISSUE_DATE;
    }

    public String getDD_NUM() {
        return DD_NUM;
    }

    public void setDD_NUM(String DD_NUM) {
        this.DD_NUM = DD_NUM;
    }

    public String getPART_TRAN_SRL_NUM() {
        return PART_TRAN_SRL_NUM;
    }

    public void setPART_TRAN_SRL_NUM(String PART_TRAN_SRL_NUM) {
        this.PART_TRAN_SRL_NUM = PART_TRAN_SRL_NUM;
    }

    public String getBEN_ACCOUNT_ID() {
        return BEN_ACCOUNT_ID;
    }

    public void setBEN_ACCOUNT_ID(String BEN_ACCOUNT_ID) {
        this.BEN_ACCOUNT_ID = BEN_ACCOUNT_ID;
    }

    public String getBEN_BANK_CODE() {
        return BEN_BANK_CODE;
    }

    public void setBEN_BANK_CODE(String BEN_BANK_CODE) {
        this.BEN_BANK_CODE = BEN_BANK_CODE;
    }

    public String getBEN_SOL_ID() {
        return BEN_SOL_ID;
    }

    public void setBEN_SOL_ID(String BEN_SOL_ID) {
        this.BEN_SOL_ID = BEN_SOL_ID;
    }

    public String getVALUE_DATE() {
        return VALUE_DATE;
    }

    public void setVALUE_DATE(String VALUE_DATE) {
        this.VALUE_DATE = VALUE_DATE;
    }

    public String getACCT_STATUS() {
        return ACCT_STATUS;
    }

    public void setACCT_STATUS(String ACCT_STATUS) {
        this.ACCT_STATUS = ACCT_STATUS;
    }

    public String getTXN_BR_ID() {
        return TXN_BR_ID;
    }

    public void setTXN_BR_ID(String TXN_BR_ID) {
        this.TXN_BR_ID = TXN_BR_ID;
    }

    public String getMNEMONIC_CODE() {
        return MNEMONIC_CODE;
    }

    public void setMNEMONIC_CODE(String MNEMONIC_CODE) {
        this.MNEMONIC_CODE = MNEMONIC_CODE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getDD_PUR_NAME() {
        return DD_PUR_NAME;
    }

    public void setDD_PUR_NAME(String DD_PUR_NAME) {
        this.DD_PUR_NAME = DD_PUR_NAME;
    }

    public String getTRAN_CODE() {
        return TRAN_CODE;
    }

    public void setTRAN_CODE(String TRAN_CODE) {
        this.TRAN_CODE = TRAN_CODE;
    }
}
