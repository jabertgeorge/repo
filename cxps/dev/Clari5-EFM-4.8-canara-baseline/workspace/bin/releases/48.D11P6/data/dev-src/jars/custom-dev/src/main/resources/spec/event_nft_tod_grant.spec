cxps.events.event.nft_tod_grant{
table-name : EVENT_NFT_TODGRANT
  event-mnemonic: NFTG
  workspaces : {
    ACCOUNT : account-id
    }
  event-attributes : {
        cust-id: {db : true ,raw_name : cust_id ,type : "string:20"}
        account-id: {db : true ,raw_name : account_id ,type : "string:20"}
        gl-code: {db : true ,raw_name : GL_Code ,type : "string:50"}
        scheme-type: {db : true ,raw_name : Scheme_Type ,type : "string:20"}
        scheme-code: {db : true ,raw_name : Scheme_Code ,type : "string:20"}
        tod-remarks: {db : true ,raw_name : TOD_Remarks ,type : "string:20"}
        tod-amt: {db : true ,raw_name : TOD_Amt ,type :  "number:18,2"}
        dp-code:{type:"string:20",db:true,raw_name:dp_code}
        pstd-user-id:{type:"string:20",db:true,raw_name:pstd_user_id}
        }
}
