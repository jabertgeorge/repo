package clari5.custom.dev.service;

import clari5.aml.cms.newjira.SyncerDaemon;
import clari5.custom.dev.service.util.EventServiceUtility;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.CxJson;
import clari5.platform.rdbms.RDBMS;
import cxps.eoi.Incident;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Shashank Devisetty
 * Organization: CustomerXPs
 * Created On: 29/04/2019
 * Reviewed By:
 */

@Api(value = "/incident/sync")
@WebServlet("/incident/sync")
public class SyncerEventService extends HttpServlet {
    private static final CxpsLogger logger = CxpsLogger.getLogger(SyncerEventService.class);
    static {
        logger.debug("Syncer Event Service Loaded successfully");
    }

    @ApiOperation(
            value = "Sync trigger to JIRA",
            produces = "Boolean: if synced or not",
            consumes = "Trigger Id of the Incident to be synced",
            response = String.class
    )
    @Override
    @SuppressWarnings("unchecked")
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String payload = EventServiceUtility.extractPayload(request);
        String type = request.getParameter("event-type");
        try {
            if (type != null && !type.trim().isEmpty() && type.trim().toLowerCase().equals("list")) {
                List<String> payloads = (List<String>) CxJson.json2obj(payload, ArrayList.class);
                EventServiceUtility.writeResponse(response, CxJson.obj2json(sync(payloads)));
            } else {
                EventServiceUtility.writeResponse(response, sync(payload).toString());
            }
        } catch (Exception e) {
            logger.error("Exception occurred while adding payload [" + payload + "] to HED: " + e.getMessage(), e);
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        logger.error("HED Event Service does not accept GET calls. Only accepts POST call with a payload message which returns derived events along with the workspace keys");
    }

    private Boolean sync(String triggerId) throws IOException {
        RDBMS rdbms = Clari5.rdbms();
        CxConnection connection = null;
        if (rdbms != null) connection = rdbms.getCxConnection();
        else throw new IOException("Unable to acquire DB Connection");
        try {
            Incident incident = new Incident(triggerId);
            if (incident.t.load(connection)) {
                SyncerDaemon syncer = (SyncerDaemon) Clari5.getResource("CMSSYNC");
                syncer.process(incident);
                return true;
            }
            return false;
        } catch (Exception e) {
            logger.error("Exception occurred while syncing trigger [" + triggerId + "] to JIRA: " + e.getMessage(), e);
            throw new IOException(e);
        }
    }

    private List<String> sync(List<String> triggerIds) {
        List<String> synced = new ArrayList<>();
        for (String triggerId : triggerIds) {
            try { if (sync(triggerId)) synced.add(triggerId); } catch (Exception e) {
                logger.error("Exception occurred while sync: " + e.getMessage(), e);
            }
        }
        return synced;
    }
}
