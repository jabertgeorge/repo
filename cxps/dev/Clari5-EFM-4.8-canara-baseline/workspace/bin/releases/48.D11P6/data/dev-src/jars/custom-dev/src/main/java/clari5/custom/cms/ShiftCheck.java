package clari5.custom.cms;

/**
 * Created by Nathiya on 18/11/19.
 */

import cxps.apex.utils.CxpsLogger;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ShiftCheck extends  Thread {


    protected static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);

    public void run() {

        while (true) {
            logger.info("RUNNING THREAD FOR SHIFT");
            logger.info("Iterating Shift based on current time via Thread");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
            LocalDateTime now = LocalDateTime.now();
            String current_time = dtf.format(now);
           // System.out.println("Current time is : [" + current_time+ "]");
                try{
                    for (Map.Entry<String, ConcurrentHashMap<String, String>> shifts : CustomCmsFormatter.nestedshift.entrySet()) {
                        String shift = shifts.getKey();
                            for (Map.Entry<String, String> nameEntry : shifts.getValue().entrySet()) {
                                   String fromtime = nameEntry.getKey();
                                    String totime = nameEntry.getValue();
                                    logger.info(shift + " timing is from " + fromtime + " to " + totime);
                                     if (current_time.equalsIgnoreCase(fromtime)) {
                                         //System.out.println("Current time :[" +current_time+ " ] : shift time : [ "+fromtime+ " ] ");
                                         Thread.sleep(1*30*1000);
                                         logger.info("Current time Equals Shift");
                                            CustomCmsFormatter.current_shift = shift;
                                         logger.info("Current shift is : [ " + CustomCmsFormatter.current_shift+ "]");
                                            CustomCmsFormatter.efmusermap.clear();
                                            CustomCmsFormatter.conCurrefmUsrMap.clear();
                                         logger.info("Cleared efm User Map");
                                            CustomCmsFormatter.cbsusermap.clear();
                                            CustomCmsFormatter.conCurrcbsUsrMap.clear();
                                         logger.info("Cleared cbs User Map");
                                            CustomCmsFormatter.rdeusermap.clear();
                                            CustomCmsFormatter.conCurrrdeUsrMap.clear();
                                         logger.info("Cleared rde User Map");

                                         logger.info("CLEARING GSHIFT MAP EFM");

                                         CustomCmsFormatter.efmuserGShift1map.clear();
                                         CustomCmsFormatter.conCurrefmUsrMapGshift1.clear();
                                         CustomCmsFormatter.efmuserGShift2map.clear();
                                         CustomCmsFormatter.conCurrefmUsrMapGshift2.clear();

                                         logger.info("CLEARING GSHIFT MAP RDE");

                                         CustomCmsFormatter.rdeuserGShift1map.clear();
                                         CustomCmsFormatter.conCurrrdeUsrMapGshift1.clear();;
                                         CustomCmsFormatter.rdeuserGShift2map.clear();
                                         CustomCmsFormatter.conCurrrdeUsrMapGshift2.clear();

                                         logger.info("CLEARING GSHIFT MAP CBS");

                                         CustomCmsFormatter.cbsuserGShift1map.clear();
                                         CustomCmsFormatter.conCurrcbsUsrMapGshift1.clear();
                                         CustomCmsFormatter.cbsuserGShift2map.clear();
                                         CustomCmsFormatter.conCurrcbsUsrMapGshift2.clear();

                                         logger.info("Current Shift after Clearing all Map : [ "+CustomCmsFormatter.current_shift+ " ]");



                                     }
                                     logger.info(shift + " : timing is from [ " + fromtime + " ] to [ " + totime+ " ]");
                             }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info("Exception in RUNNING THREAD FOR SHIFT");
               }
        }
    }


    public static String getCurrShift()
    {
        try{
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
            LocalDateTime now = LocalDateTime.now();
            String current_time = dtf.format(now);
            logger.info("Current time is : [" + current_time+ "]");
            for (Map.Entry<String, ConcurrentHashMap<String, String>> shifts : CustomCmsFormatter.nestedshift.entrySet()) {
                String shift = shifts.getKey();
                for (Map.Entry<String, String> nameEntry : shifts.getValue().entrySet()) {
                    String fromtime = nameEntry.getKey();
                    String totime = nameEntry.getValue();
                    logger.info(shift + " timing is from " + fromtime + " to " + totime);

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

                        Date d1 = sdf.parse(fromtime);
                        Date d2 = sdf.parse(totime);
                        Date cr = sdf.parse(current_time);

                        if(d1.getTime() < d2.getTime()) {
                            if (cr.getTime() >= d1.getTime() && cr.getTime() <= d2.getTime()) {
                                logger.info("Current time : [ " + cr.getTime() + " ] ");
                                logger.info("From time : [ " + d1.getTime() + " ] ");
                                logger.info("To time : [ " + d2.getTime() + " ] ");
                                logger.info("current shift : [ " + shift + " ] ");
                                return shift;
                            }
                        }
                        if(d1.getTime() > d2.getTime())
                        {
                            logger.info("Pretending that shift lies between two days");
                            if(cr.getTime()>=d1.getTime() || cr.getTime()<=d2.getTime())
                            {
                                return shift;
                            }
                        }

                    }
                }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

}

