// -- ASSISTED CODE --
package cxps.events;

import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
import clari5.iso8583.formatter.FieldFormatter;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;
import org.json.JSONObject;


@Table(Name="EVENT_FT_CC_CARDSTXN", Schema="rice")
public class FT_CCCardTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String userType;
       @Field(size=20) public Double posecomIntrLim;
       @Field(size=6) public String bin;
       @Field(size=20) public String channel;
       @Field(size=100) public String errorDesc;
       @Field(size=50) public String entryMode;
       @Field(size=2) public String txnSecuredFlag;
       @Field(size=50) public String deviceId;
       @Field(size=20) public String stateCode;
       @Field(size=5) public String hostId;
       @Field(size=10) public String chipPinFlag;
       @Field(size=50) public String custName;
       @Field(size=50) public String payeeName;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=10) public String branchId;
       @Field(size=6) public String errorCode;
       @Field(size=20) public String mccCode;
       @Field(size=50) public String terminalId;
       @Field(size=20) public Double atmIntrLim;
       @Field(size=16) public String custCardId;
       @Field(size=5) public String posModeVal;
       @Field(size=20) public Double avlBal;
       @Field(size=20) public String payeeId;
       @Field(size=50) public String acctOwnership;
       @Field(size=200) public String tranCde;
       @Field(size=50) public String productCode;
       @Field(size=200) public String respCde;
       @Field(size=20) public String drAccountId;
       @Field(size=50) public String acctOpenDate;
       @Field(size=2) public String countryCode;
       @Field(size=20) public Double atmDomLim;
       @Field(size=20) public String maskCardNo;
       @Field(size=5) public String posEntryMode;
       @Field(size=20) public String ipCountry;
       @Field(size=20) public String crAccountId;
       @Field(size=20) public Double txnAmount;
       @Field(size=10) public String custMobNo;
       @Field(size=20) public String ipCity;
       @Field(size=20) public String userId;
       @Field(size=200) public String tranParticular;
       @Field(size=50) public String custId;
       @Field(size=20) public String crIfscCode;
       @Field(size=50) public String devownerId;
       @Field(size=20) public String ipAddress;
       @Field(size=50) public String tranDate;
       @Field(size=20) public Double posecomdomLim;
       @Field(size=10) public String tranType;
       @Field(size=50) public String merchantId;
       @Field(size=2) public String succFailFlag;
       @Field(size=50) public String partTranType;


    @JsonIgnore
    public ITable<FT_CCCardTxnEvent> t = AEF.getITable(this);

	public FT_CCCardTxnEvent(){}

    public FT_CCCardTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("CCCardTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setUserType(json.getString("user_type"));
            setPosecomIntrLim(EventHelper.toDouble(json.getString("pos_ecom_intr_limit")));
            setBin(json.getString("bin"));
            setChannel(json.getString("channel"));
            setErrorDesc(json.getString("error_desc"));
            setEntryMode(json.getString("entry_mode"));
            setTxnSecuredFlag(json.getString("txn_secured_flag"));
            setDeviceId(json.getString("device_id"));
            setStateCode(json.getString("state_code"));
            setHostId(json.getString("host_id"));
            setChipPinFlag(json.getString("chip_pin_flg"));
            setCustName(json.getString("cust_name"));
            setPayeeName(json.getString("payee_name"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setErrorCode(json.getString("error_code"));
            setMccCode(json.getString("mcc_code"));
            setTerminalId(json.getString("terminal_id"));
            setAtmIntrLim(EventHelper.toDouble(json.getString("atm_intr_limit")));
            setCustCardId(json.getString("cust_card_id"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setPayeeId(json.getString("payee_id"));
            setAcctOwnership(json.getString("acct_ownership"));
            setTranCde(json.getString("tran_cde"));
            setProductCode(json.getString("product_code"));
            setRespCde(json.getString("resp_cde"));
            setDrAccountId(json.getString("dr_account_id"));
            setAcctOpenDate(json.getString("acct_open_date"));
            setCountryCode(json.getString("country_code"));
            setAtmDomLim(EventHelper.toDouble(json.getString("atm_dom_limit")));
            setMaskCardNo(json.getString("mask_card_no"));
            setPosEntryMode(json.getString("pos_entry_mode"));
            setIpCountry(json.getString("ip_country"));
            setCrAccountId(json.getString("cr_account_id"));
            setTxnAmount(EventHelper.toDouble(json.getString("txn_amt")));
            setCustMobNo(json.getString("cust_mob_no"));
            setIpCity(json.getString("ip_city"));
            setUserId(json.getString("user_id"));
            setTranParticular(json.getString("tran_particular"));
            setCustId(json.getString("cust_id"));
            setCrIfscCode(json.getString("cr_ifsc_code"));
            setDevownerId(json.getString("dev_owner_id"));
            setIpAddress(json.getString("ip_address"));
            setTranDate(json.getString("tran_date"));
            setPosecomdomLim(EventHelper.toDouble(json.getString("pos_ecom_dom_limit")));
            setTranType(json.getString("tran_type"));
            setMerchantId(json.getString("merchant_id"));
            setSuccFailFlag(json.getString("succ_fail_flg"));
            setPartTranType(json.getString("part_tran_type"));

        setDerivedValues();

    }


    private void setDerivedValues() {
 	setPosModeVal(cxps.events.CustomFieldDerivator.posValDerive(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FCC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getUserType(){ return userType; }

    public Double getPosecomIntrLim(){ return posecomIntrLim; }

    public String getBin(){ return bin; }

    public String getChannel(){ return channel; }

    public String getErrorDesc(){ return errorDesc; }

    public String getEntryMode(){ return entryMode; }

    public String getTxnSecuredFlag(){ return txnSecuredFlag; }

    public String getDeviceId(){ return deviceId; }

    public String getStateCode(){ return stateCode; }

    public String getHostId(){ return hostId; }

    public String getChipPinFlag(){ return chipPinFlag; }

    public String getCustName(){ return custName; }

    public String getPayeeName(){ return payeeName; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getErrorCode(){ return errorCode; }

    public String getMccCode(){ return mccCode; }

    public String getTerminalId(){ return terminalId; }

    public Double getAtmIntrLim(){ return atmIntrLim; }

    public String getCustCardId(){ return custCardId; }

    public Double getAvlBal(){ return avlBal; }

    public String getPayeeId(){ return payeeId; }

    public String getAcctOwnership(){ return acctOwnership; }

    public String getTranCde(){ return tranCde; }

    public String getProductCode(){ return productCode; }

    public String getRespCde(){ return respCde; }

    public String getDrAccountId(){ return drAccountId; }

    public String getAcctOpenDate(){ return acctOpenDate; }

    public String getCountryCode(){ return countryCode; }

    public Double getAtmDomLim(){ return atmDomLim; }

    public String getMaskCardNo(){ return maskCardNo; }

    public String getPosEntryMode(){ return posEntryMode; }

    public String getIpCountry(){ return ipCountry; }

    public String getCrAccountId(){ return crAccountId; }

    public Double getTxnAmount(){ return txnAmount; }

    public String getCustMobNo(){ return custMobNo; }

    public String getIpCity(){ return ipCity; }

    public String getUserId(){ return userId; }

    public String getTranParticular(){ return tranParticular; }

    public String getCustId(){ return custId; }

    public String getCrIfscCode(){ return crIfscCode; }

    public String getDevownerId(){ return devownerId; }

    public String getIpAddress(){ return ipAddress; }

    public String getTranDate(){ return tranDate; }

    public Double getPosecomdomLim(){ return posecomdomLim; }

    public String getTranType(){ return tranType; }

    public String getMerchantId(){ return merchantId; }

    public String getSuccFailFlag(){ return succFailFlag; }

    public String getPartTranType(){ return partTranType; }
    public String getPosModeVal(){ return posModeVal; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setUserType(String val){ this.userType = val; }
    public void setPosecomIntrLim(Double val){ this.posecomIntrLim = val; }
    public void setBin(String val){ this.bin = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setEntryMode(String val){ this.entryMode = val; }
    public void setTxnSecuredFlag(String val){ this.txnSecuredFlag = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setStateCode(String val){ this.stateCode = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setChipPinFlag(String val){ this.chipPinFlag = val; }
    public void setCustName(String val){ this.custName = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setMccCode(String val){ this.mccCode = val; }
    public void setTerminalId(String val){ this.terminalId = val; }
    public void setAtmIntrLim(Double val){ this.atmIntrLim = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setAcctOwnership(String val){ this.acctOwnership = val; }
    public void setTranCde(String val){ this.tranCde = val; }
    public void setProductCode(String val){ this.productCode = val; }
    public void setRespCde(String val){ this.respCde = val; }
    public void setDrAccountId(String val){ this.drAccountId = val; }
    public void setAcctOpenDate(String val){ this.acctOpenDate = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAtmDomLim(Double val){ this.atmDomLim = val; }
    public void setMaskCardNo(String val){ this.maskCardNo = val; }
    public void setPosEntryMode(String val){ this.posEntryMode = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setCrAccountId(String val){ this.crAccountId = val; }
    public void setTxnAmount(Double val){ this.txnAmount = val; }
    public void setCustMobNo(String val){ this.custMobNo = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setTranParticular(String val){ this.tranParticular = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setCrIfscCode(String val){ this.crIfscCode = val; }
    public void setDevownerId(String val){ this.devownerId = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setTranDate(String val){ this.tranDate = val; }
    public void setPosecomdomLim(Double val){ this.posecomdomLim = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setSuccFailFlag(String val){ this.succFailFlag = val; }
    public void setPartTranType(String val){ this.partTranType = val; }
    public void setPosModeVal(String val){ this.posModeVal = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_CCCardTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        if(getTerminalId() != null && !getTerminalId().equalsIgnoreCase("") && !getTerminalId().equalsIgnoreCase("NA") ) {
            String terminalKey = h.getCxKeyGivenHostKey(WorkspaceName.TERMINAL, getHostId(), this.terminalId);
            wsInfoSet.add(new WorkspaceInfo("Terminal", terminalKey));
        }
        if(getDrAccountId() != null && !getDrAccountId().equalsIgnoreCase("") && !getDrAccountId().equalsIgnoreCase("NA")) {
            String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.drAccountId);
            wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        }
        if(getCustCardId() != null && !getCustCardId().equalsIgnoreCase("")  && !getCustCardId().equalsIgnoreCase("NA")) {
            String paymentcardKey = h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
            wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));
        }
        if(getMerchantId() != null && !getMerchantId().equalsIgnoreCase("") && !getMerchantId().equalsIgnoreCase("NA")) {
            String merchantKey = h.getCxKeyGivenHostKey(WorkspaceName.MERCHANT, getHostId(), this.merchantId.replaceAll(":","").trim());
            wsInfoSet.add(new WorkspaceInfo("Merchant", merchantKey));
        }
        
        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_CCCardTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "CCCardTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("cust_card_id",getCustCardId());
        json.put("cust_id",getCustCardId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }

    @Override
    public String getJson(FieldFormatter fieldFormatter){

        String field47 = fieldFormatter.getField47();
        String field3 = fieldFormatter.getField3();
        String respcode = fieldFormatter.getField39();

        String eventname = field47.substring(17,35).trim();
        String  panval = field47.substring(55,74).trim();
        String trancdeval = field3.substring(0,2).trim();
        String binval = field47.substring(86,97).trim();
        String channelval = field47.substring(101,105).trim();
        String txnsecval = field47.substring(105,106).trim();
        String useridval = field47.substring(106,108).trim();
        String usertypeval= field47.substring(108,114).trim();
        String custidval= field47.substring(114,116).trim();
        String deviceidval= field47.substring(116,132).trim();
        String terminalidval= field47.substring(132,148).trim();
        String merchantidval= field47.substring(148,170).trim();
        String devownidval= field47.substring(170,185).trim();
        // String trandateval= field47.substring(185,195).trim();
        String ipaddressval= field47.substring(195,197).trim();
        String ipcountryval= field47.substring(197,200).trim();
        String ipcityval= field47.substring(200,213).trim();
        String statecodeval= field47.substring(213,216).trim();
        String chippinval = field47.substring(216,217).trim();
        String custnameval= field47.substring(217,219).trim();
        String errorcodeval= field47.substring(220,222).trim();
        String errordescval= field47.substring(222,252).trim();
        String draccidval= field47.substring(282,310).trim();
        String txnamtval= field47.substring(310,322).trim();
        String avlbalval= field47.substring(322,334).trim();
        String craccidval= field47.substring(334,347).trim();
        String crifscval= field47.substring(347,349).trim();
        String payeeidval= field47.substring(349,351).trim();
        String payeenameval= field47.substring(351,353).trim();
        String trantypeval = field47.substring(353,355).trim();
        String entrymodeval = field47.substring(355,358).trim();
        String posentrymodeval = field47.substring(358,361).trim();
        String countrycodeval = field47.substring(361,363).trim();
        String custcardval = field47.substring(363,382).trim();
        String mcccodeval = field47.substring(382,386).trim();
        String branchidval = field47.substring(386,388).trim();
        String acctownrval = field47.substring(388,392).trim();
        String acctopen = field47.substring(392,394).trim();
	String parttratype = field47.substring(394,395).trim();
        String custmobval = field47.substring(395,397).trim();
        String productcodeval = field47.substring(397,399).trim();
        String atmdomval = field47.substring(399,401).trim();
        String atmintrval = field47.substring(401,403).trim();
        String posecomdomval = field47.substring(403,405).trim();
        String posecomintrval = field47.substring(405,407).trim();
        String maskcardval = field47.substring(407,409).trim();
        String tranparval = field47.substring(409,415).trim();


        //Forming the json

        JSONObject eventJson = new JSONObject();
        JSONObject msbody = new JSONObject();

        eventJson.put("eventtype", "ft");
        eventJson.put("eventsubtype", "cccardtxn");
        eventJson.put("event-name", "ft_cccardtxn");


        msbody.put("host_id", "F");
        msbody.put("sys_time", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
        msbody.put("event_id", "ft_cccardtxn" + System.nanoTime());
        msbody.put("channel",channelval);
        msbody.put("user_id",useridval);
        msbody.put("user_type",usertypeval);
        msbody.put("cust_id",custidval);
        msbody.put("device_id",deviceidval);
        msbody.put("ip_address",ipaddressval);
        msbody.put("ip_country",ipcountryval);
        msbody.put("ip_city",ipcityval);
        msbody.put("cust_name",custnameval);
        if(respcode.equalsIgnoreCase("00")){
            msbody.put("succ_fail_flg","S");
        }else{
            msbody.put("succ_fail_flg","F");
        }
        msbody.put("error_code",errorcodeval);
        msbody.put("error_desc",errordescval);
        msbody.put("dr_account_id",draccidval);
        msbody.put("txn_amt",txnamtval.substring(0, txnamtval.length() - 2) + "." + txnamtval.substring(Math.max(txnamtval.length() - 2, 0)));
        msbody.put("avl_bal",avlbalval);
        msbody.put("cr_account_id",craccidval);
        msbody.put("cr_ifsc_code",crifscval);
        msbody.put("payee_id",payeeidval);
        msbody.put("payee_name",payeenameval);
        msbody.put("tran_type",trantypeval);
        msbody.put("entry_mode",entrymodeval);
        msbody.put("branch_id",branchidval);
        msbody.put("acct_ownership",acctownrval);
        msbody.put("acct_open_date", "01-01-2000");
        msbody.put("country_code",countrycodeval);
        if (channelval.equalsIgnoreCase("ATM")) {
            msbody.put("cust_card_id", panval);
        } else {
            msbody.put("cust_card_id", custcardval);
        }
        msbody.put("bin",binval);
        msbody.put("mcc_code",mcccodeval);
        msbody.put("terminal_id",terminalidval);
        msbody.put("merchant_id",merchantidval);
        msbody.put("cust_mob_no",custmobval);
        msbody.put("txn_secured_flag",txnsecval);
        msbody.put("product_code",productcodeval);
        msbody.put("dev_owner_id",devownidval);
        msbody.put("atm_dom_limit",atmdomval);
        msbody.put("atm_intr_limit",atmintrval);
        msbody.put("pos_ecom_dom_limit",posecomdomval);
        msbody.put("pos_ecom_intr_limit",posecomintrval);
        msbody.put("tran_date",new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
        msbody.put("mask_card_no",maskcardval);
        msbody.put("state_code",statecodeval);
        msbody.put("chip_pin_flg",chippinval);
        msbody.put("pos_entry_mode",posentrymodeval);
        msbody.put("tran_particular",tranparval);
        msbody.put("tran_cde",trancdeval);
        msbody.put("resp_cde",respcode);
        msbody.put("part_tran_type",parttratype);


        eventJson.put("msgBody", msbody.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

    return eventJson.toString();
    }

}
