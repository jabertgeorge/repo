package clari5.custom.cms;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;

import clari5.platform.applayer.Clari5;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jabert on 21/11/18.
 */
 /*checking for Git*/
public class CustomCmsFormatter extends DefaultCmsFormatter {

   String project = null;
    int start = 0;

    protected static String current_shift = null;

    protected static ConcurrentHashMap<String, String> efmusermap = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> cbsusermap = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> rdeusermap = new ConcurrentHashMap<String, String>();

    protected static ConcurrentHashMap<String, String> efmuserGShift1map = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> cbsuserGShift1map = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> rdeuserGShift1map = new ConcurrentHashMap<String, String>();

    protected static ConcurrentHashMap<String, String> efmuserGShift2map = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> cbsuserGShift2map = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> rdeuserGShift2map = new ConcurrentHashMap<String, String>();

    protected static ConcurrentHashMap<String, Integer> conCurrefmUsrMap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrcbsUsrMap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrrdeUsrMap = new ConcurrentHashMap<>();

    protected static ConcurrentHashMap<String, Integer> conCurrefmUsrMapGshift1 = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrcbsUsrMapGshift1 = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrrdeUsrMapGshift1 = new ConcurrentHashMap<>();

    protected static ConcurrentHashMap<String, Integer> conCurrefmUsrMapGshift2 = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrcbsUsrMapGshift2 = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrrdeUsrMapGshift2 = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, ConcurrentHashMap<String,String>> nestedshift=new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, String> shifitiming=new ConcurrentHashMap();

    protected static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);


    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public void startThread(int val){
        if(val==0) {
            ShiftCheck n = new ShiftCheck();
            Thread t1 = new Thread(n);
            t1.start();
            start++;
        }
    }

    public void EFMUser() {
        if (efmusermap.size() == 0) {
            getEFMUser();
            logger.info("efm user Map Loaded");
        }
    }

    public void CBSUser() {
        if (cbsusermap.size() == 0) {
            getCBSUser();
            logger.info("cbs user Map Loaded");
        }
    }

    public void RDEUser() {
        if (rdeusermap.size() == 0) {
            getRDEUser();
            logger.info("rde user Map Loaded");
        }
    }


    public String getIncidentAssignee(Incident incidentEvent) throws CmsException {

        String username = "cxpsaml";
        if (this.cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        //adding the shift in map
        //ShiftCheck.addShift();
        addShift();
        logger.info("Incident assignee");
        EFMUser();
        CBSUser();
        RDEUser();



        logger.info("Starting Thread");
        startThread(start);

        String currshift = ShiftCheck.getCurrShift();
        logger.info("Current shift in Custom cms formatter : [ "+currshift+ " ] ");


       if(currshift.equalsIgnoreCase("gshift")) {

           DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
           LocalDateTime now = LocalDateTime.now();
           String current_time = dtf.format(now);
           String  time[]  = current_time.split(":");
           int hour  = Integer.parseInt(time[0]);
           int min   = Integer.parseInt(time[1]);

           System.out.println("Current hour : [ "+hour+ " ] : min : [ "+min+ " ]");

           System.out.println("INTO GSHIFT");
           Map<String,String> gshiftTiming = nestedshift.get("GSHIFT");
           System.out.println("GSHIFT TIMING : [ "+gshiftTiming+ " ]");
           String fromtime  = String.valueOf(gshiftTiming.keySet().toArray()[0]);
           //System.out.println("FromTime GSHIFT : [ "+fromtime+ " ]");
           String totime =  String.valueOf(gshiftTiming.get(fromtime));
           //System.out.println("ToTime GSHIFT : [ "+totime+ " ]");

           String  frmtime[] = fromtime.split(":");
           String  totme[] = totime.split(":");

           int frmhr = Integer.parseInt(frmtime[0]);
           int frmmin = Integer.parseInt(frmtime[1]);
           System.out.println("GSHIFT FROM HOUR : [ "+frmhr+ " ] : FROM MIN : [ "+frmmin+ " ]");
           int tohr = Integer.parseInt(totme[0]);
           int tomin = Integer.parseInt(totme[1]);
           System.out.println("GSHIFT TO HOUR : [ "+tohr+ " ] : TO MIN : [ "+tomin+ " ]");


           Calendar genfrmtimeshift = Calendar.getInstance();
           genfrmtimeshift.set(Calendar.HOUR_OF_DAY, frmhr);
           genfrmtimeshift.set(Calendar.MINUTE, frmmin);
           genfrmtimeshift.set(Calendar.SECOND, 0);
           genfrmtimeshift.set(Calendar.MILLISECOND, 0);
           System.out.println("general shift from time: [ "+genfrmtimeshift.getTimeInMillis()+ " ]");
           System.out.println(System.currentTimeMillis());

           Calendar gentotimeshift = Calendar.getInstance();
           gentotimeshift.set(Calendar.HOUR_OF_DAY, tohr);
           gentotimeshift.set(Calendar.MINUTE, tomin);
           gentotimeshift.set(Calendar.SECOND, 0);
           gentotimeshift.set(Calendar.MILLISECOND, 0);
           System.out.println("general shift to time: [ "+gentotimeshift.getTimeInMillis()+ " ]");
           System.out.println(System.currentTimeMillis());



           //For Shift1
           Map<String,String> shifttiming1 = nestedshift.get("SHIFT1");
           String fromtimeshift1  = String.valueOf(shifttiming1.keySet().toArray()[0]);
          // System.out.println("FromTime SHIFT1 : [ "+fromtimeshift1+ " ]");
           String totimeShift1 =  String.valueOf(shifttiming1.get(fromtimeshift1));
          // System.out.println("ToTime SHIFT1 : [ "+totimeShift1+ " ]");
           String  frmtimeshift1[] = fromtimeshift1.split(":");
           String  totmeshift1[] = totimeShift1.split(":");
           int frmhrshift1 = Integer.parseInt(frmtimeshift1[0]);
           int frmminshift1 = Integer.parseInt(frmtimeshift1[1]);
           System.out.println("FROM HOUR shift1 : [ "+frmhrshift1+ " ] : FROM MIN : [ "+frmminshift1+ " ]");
           int tohrshift1 = Integer.parseInt(totmeshift1[0]);
           int tominshift1 = Integer.parseInt(totmeshift1[1]);
           System.out.println("TO HOUR shift1 : [ "+tohrshift1+ " ] : TO MIN : [ "+tominshift1+ " ]");


           Calendar calshift1 = Calendar.getInstance();
           calshift1.set(Calendar.HOUR_OF_DAY, tohrshift1);
           calshift1.set(Calendar.MINUTE, tominshift1);
           calshift1.set(Calendar.SECOND, 0);
           calshift1.set(Calendar.MILLISECOND, 0);
           System.out.println("shift 1 to time: [ "+calshift1.getTimeInMillis()+ " ]");
           System.out.println(System.currentTimeMillis());



           //For Shift2
           Map<String,String> shifttiming2 = nestedshift.get("SHIFT2");
           String fromtimeshift2  = String.valueOf(shifttiming2.keySet().toArray()[0]);
           //System.out.println("FromTime SHIFT2 : [ "+fromtimeshift2+ " ]");
           String totimeShift2 =  String.valueOf(shifttiming2.get(fromtimeshift2));
           //System.out.println("ToTime SHIFT2 : [ "+totimeShift2+ " ]");
           String  frmtimeshift2[] = fromtimeshift2.split(":");
           String  totmeshift2[] = totimeShift2.split(":");
           int frmhrshift2 = Integer.parseInt(frmtimeshift2[0]);
           int frmminshift2 = Integer.parseInt(frmtimeshift2[1]);
           System.out.println("FROM HOUR shift2 : [ "+frmhrshift2+ " ] : FROM MIN : [ "+frmminshift2+ " ]");
           int tohrshift2 = Integer.parseInt(totmeshift2[0]);
           int tominshift2 = Integer.parseInt(totmeshift2[1]);
           System.out.println("TO HOUR shift2 : [ "+tohrshift2+ " ] : TO MIN : [ "+tominshift2+ " ]");



           Calendar calshift2 = Calendar.getInstance();
           calshift2.set(Calendar.HOUR_OF_DAY, frmhrshift2);
           calshift2.set(Calendar.MINUTE, frmminshift2);
           calshift2.set(Calendar.SECOND, 0);
           calshift2.set(Calendar.MILLISECOND, 0);
           System.out.println("shift 2 from from time: [ "+calshift2.getTimeInMillis()+ " ]");
           System.out.println(System.currentTimeMillis());






           //current hourmin greater than gshift hourmin && currenthour min less than shift 1 hourmin
           if(System.currentTimeMillis() <= calshift1.getTimeInMillis() && System.currentTimeMillis() >=  genfrmtimeshift.getTimeInMillis()) {

               System.out.println("INTO GSHIFT & SHIFT1");

               if (efmuserGShift1map.size() == 0) {
                   getEFMGSsh1User();
               }
               if (rdeuserGShift1map.size() == 0) {
                   getRDEGSsh1User();
               }
               if (cbsuserGShift1map.size() == 0) {
                   getCBSGSsh1User();
               }

               project = incidentEvent.project;
               System.out.println("Project is : [ "+project+ " ]");
               if (project.equalsIgnoreCase("efm")) {
                   System.out.println("Into project GSHIFT1 & shift 1 EFM");
                   logger.info("SHIFT CURRENT : [ " + currshift + " ]");
                   username = getEFMUserSyncGshift1(currshift);
                   logger.info("Returning efm  GSHIFT1 user : [" + username + " ]");
                   return username;
               } else if (project.equalsIgnoreCase("cbs")) {
                   logger.info("Into project  GSHIFT1 CBS");
                   username = getCBSUserSyncGshift1(currshift);
                   logger.info("Returning cbs  GSHIFT1 user : [" + username + " ]");
                   return username;
               } else if (project.equalsIgnoreCase("rde")) {
                   logger.info("Into project  GSHIFT1 RDE");
                   username = getRDEUserSyncGshift1(currshift);
                   logger.info("Returning rde GSHIFT1 user  : [" + username + " ]");
                   return username;
               } else {
                   logger.info("Project not found : [ " + project + " ] : hence assigning to default as CXPS ");
                   return "cxpsaml";
               }


           }
           //
          // else if( ((hour >= frmhrshift2) && (min >= frmminshift2) && (hour <= tohr)) || ((hour <=tohr) && (min <= tomin)))  {

           else if( System.currentTimeMillis() >= calshift2.getTimeInMillis() && System.currentTimeMillis() <= gentotimeshift.getTimeInMillis()) {
               System.out.println("INTO GSHIFT & SHIFT2");

               if (efmuserGShift2map.size() == 0) {
                   System.out.println("EFM");
                   getEFMGSsh2User();
               }
               if (rdeuserGShift2map.size() == 0) {
                   System.out.println("RDE");
                   getRDEGSsh2User();
               }
               if (cbsuserGShift2map.size() == 0) {
                   System.out.println("CBS");
                   getCBSGSsh2User();
               }

               project = incidentEvent.project;
               System.out.println("Project is : [ "+project+ " ]");
               if (project.equalsIgnoreCase("efm")) {
                   logger.info("Into project EFM");
                   logger.info("SHIFT CURRENT : [ " + currshift + " ]");
                   username = getEFMUserSyncGshift2(currshift);
                   logger.info("Returning efm user : [" + username + " ]");
                   return username;
               } else if (project.equalsIgnoreCase("cbs")) {
                   logger.info("Into project CBS");
                   username = getCBSUserSyncGshift2(currshift);
                   logger.info("Returning cbs user : [" + username + " ]");
                   return username;
               } else if (project.equalsIgnoreCase("rde")) {
                   logger.info("Into project RDE");
                   username = getRDEUserSyncGshift2(currshift);
                   logger.info("Returning rde user : [" + username + " ]");
                   return username;
               } else {
                   logger.info("Project not found : [ " + project + " ] : hence assigning to default as CXPS ");
                   return "cxpsaml";
               }

           }
           else {

               System.out.println("UNABLE TO FIND SHIFT 1 or SHIFT 2  - CHECK THE GSHIFT TIME");
               //if required will take only general shift users
               logger.info("Time not equal in general shift: [ " + project + " ] : hence assigning to default as CXPS ");
               return "cxpsaml";
           }


       }
       else {
           project = incidentEvent.project;
           logger.info("Project is : [ " + project + " ] ");
           if (project.equalsIgnoreCase("efm")) {
               logger.info("Into project EFM");
               logger.info("SHIFT CURRENT : [ " + currshift + " ]");
               username = getEFMUserSync(currshift);
               logger.info("Returning efm user : [" + username + " ]");
               return username;
           } else if (project.equalsIgnoreCase("cbs")) {
               logger.info("Into project CBS");
               username = getCBSUserSync(currshift);
               logger.info("Returning cbs user : [" + username + " ]");
               return username;
           } else if (project.equalsIgnoreCase("rde")) {
               logger.info("Into project RDE");
               username = getRDEUserSync(currshift);
               logger.info("Returning rde user : [" + username + " ]");
               return username;
           } else {
               logger.info("Project not found : [ " + project + " ] : hence assigning to default as CXPS ");
               return "cxpsaml";
           }
       }
    }

    @Override
    public CxJson populateIncidentJson(Incident event, String jiraParentId, boolean isupdate) throws CmsException, IOException {

        logger.info("Into Custom PopulateIncident Json");
        CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.moduleId);
        CxJson json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
        String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();

        if (!isupdate && event.entityId.startsWith("P_F_")) {
            try {

                //String eventJson = event.convertToJson(event);
                //Hocon h = new Hocon(eventJson);
                CxJson wrapper = json.get("fields");

                Hocon conf = new Hocon();
                conf.loadFromContext("custom-fields.conf");

                Hocon mobEmailHocon = conf.get("custom-fields");
                HashMap<String, String> tableQueryMap = new HashMap<>();
                if (mobEmailHocon != null) {
                    List<String> keys = mobEmailHocon.getKeysAsList();
                    for (String key : keys) {
                        String val = mobEmailHocon.getString(key);
                        logger.info("the custom field for " + key + " : " + val);
                        tableQueryMap.put(key, val);
                    }
                }

                if (tableQueryMap != null && tableQueryMap.size() >= 2) {

                    List<String> mobEmList = getMobEmail(event.entityId.replace("P_F_", ""));
                    if (mobEmList != null && tableQueryMap != null) {
                        wrapper.put(tableQueryMap.get("mobileField"), mobEmList.get(0).toString());
                        wrapper.put(tableQueryMap.get("emailField"), mobEmList.get(1).toString());
                        return json.put("fields", wrapper);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return json;
            }
        }
        if (!isupdate && event.entityId.startsWith("C_F_")) {
            try {

                //String eventJson = event.convertToJson(event);
                //Hocon h = new Hocon(eventJson);
                CxJson wrapper = json.get("fields");

                Hocon conf = new Hocon();
                conf.loadFromContext("custom-fields.conf");

                Hocon mobEmailHocon = conf.get("custom-fields");
                HashMap<String, String> tableQueryMap = new HashMap<>();
                if (mobEmailHocon != null) {
                    List<String> keys = mobEmailHocon.getKeysAsList();
                    for (String key : keys) {
                        String val = mobEmailHocon.getString(key);
                        logger.info("the custom field for " + key + " : " + val);
                        tableQueryMap.put(key, val);
                    }
                }

                if (tableQueryMap != null && tableQueryMap.size() >= 2) {

                    List<String> mobEmList = getMobEmailDetails(event.entityId.replace("C_F_", ""));
                    if (mobEmList != null && tableQueryMap != null) {
                        wrapper.put(tableQueryMap.get("mobileField"), mobEmList.get(0).toString());
                        wrapper.put(tableQueryMap.get("emailField"), mobEmList.get(1).toString());
                        return json.put("fields", wrapper);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return json;
            }
        }
        return json;
    }

    public List<String> getMobEmail(String cardNo) {
        String mobileNo = null;
        String emailId = null;
        List<String> mobEmList = new ArrayList<String>();
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource not available...");
        try {
            String query = "select CUSTMOBILE,CUSTEMAIL from CUST_CARD_MASTER where" +
                    " CARD_NUMBER = '" + cardNo + "' and ROWNUM = 1";
            try (Connection connection = rdbms.getCxConnection(); PreparedStatement ps = connection.prepareStatement(query); ResultSet rs = ps.executeQuery()){
                while (rs.next()) {
                    mobileNo = rs.getString("CUSTMOBILE");
                    emailId = rs.getString("CUSTEMAIL");
                }
                logger.info("Card No : " + cardNo + " : mobile No : " + mobileNo + " : email Id : " + emailId);
                mobEmList.add((mobileNo != null) ? mobileNo : "Not Available");
                mobEmList.add((emailId != null) ? emailId : "Not Available");
                return mobEmList;
            }
        } catch (SQLException e) {
            logger.info("class name [ customcmsformatter ] ---> method [getMobile]");
            e.printStackTrace();
            e.getCause();
        }
        return null;
    }
    public List<String> getMobEmailDetails(String custId) {
        String mobileNo = null;
        String emailId = null;
        List<String> mobEmList = new ArrayList<String>();
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource not available...");
        try {
            String query =  "select \"custEmail1\", \"custMobile1\" from CUSTOMER where \"hostCustId\"='" + custId + "' ";
            try (Connection connection = rdbms.getCxConnection(); PreparedStatement ps = connection.prepareStatement(query); ResultSet rs = ps.executeQuery()){
                while (rs.next()) {
                    mobileNo = rs.getString("custMobile1");
                    emailId = rs.getString("custEmail1");
                }
                logger.info("Card No : " + custId + " : mobile No : " + mobileNo + " : email Id : " + emailId);
                mobEmList.add((mobileNo != null) ? mobileNo : "Not Available");
                mobEmList.add((emailId != null) ? emailId : "Not Available");
                return mobEmList;
            }
        } catch (SQLException e) {
            logger.info("class name [ customcmsformatter ] ---> method [getMobile]");
            e.printStackTrace();
            e.getCause();
        }
        return null;
    }

    public void getEFMUser() {

        try {
            String sql = "SELECT * FROM CMS_USER_SHIFTS WHERE PROJECT like '%EFM%' AND HLMARKFLAG='N'";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    logger.info("EFM users and shift time:" + efmusermap);

                    String shift = rs.getString("SHIFT");
                    String user = rs.getString("USERNAME");

                    if (efmusermap.containsKey(shift)) {
                        logger.info("Contains shift hence replacing : [ " + shift + " ]");
                        String usr = efmusermap.get(shift);
                        usr += "," + user;
                        logger.info(usr);
                        efmusermap.replace(shift, usr);

                    } else {
                        logger.info("At first shift : [ " + shift + " ]");
                        efmusermap.put(shift, user);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getEFMGSsh1User() {

        try {
            Set<String> userset = new HashSet<>();
            String sql = "SELECT * FROM CMS_USER_SHIFTS  WHERE PROJECT like '%EFM%' AND HLMARKFLAG='N' and SHIFT IN ('SHIFT1','GSHIFT')";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    System.out.println("EFM users on General Shift & shift 1:" + efmuserGShift1map);
                    String user = rs.getString("USERNAME");
                    System.out.println("user : [ "+user+ " ]");
                    userset.add(user);
                }
                String stringusr = String.join(", ", userset);
                System.out.println(stringusr);
                efmuserGShift1map.put("GSHIFT",stringusr.replaceAll("\\s+", ""));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getRDEGSsh1User() {

        try {
            Set<String> userset = new HashSet<>();
            String sql = "SELECT * FROM CMS_USER_SHIFTS  WHERE PROJECT like '%RDE%' AND HLMARKFLAG='N' and SHIFT IN ('SHIFT1','GSHIFT')";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    System.out.println("RDE users on General Shift & Shift 1:" + rdeuserGShift1map);
                    String user = rs.getString("USERNAME");
                    System.out.println("user : [ "+user+ " ]");
                    userset.add(user);
                }
                String stringusr = String.join(", ", userset);
                System.out.println(stringusr);
                rdeuserGShift1map.put("GSHIFT",stringusr.replaceAll("\\s+", ""));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void getCBSGSsh1User() {

        try {
            Set<String> userset = new HashSet<>();
            String sql = "SELECT * FROM CMS_USER_SHIFTS  WHERE PROJECT like '%CBS%' AND HLMARKFLAG='N' and SHIFT IN ('SHIFT1','GSHIFT')";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    System.out.println("CBS users on General Shift & shift 1:" + cbsuserGShift1map);
                    String user = rs.getString("USERNAME");
                    System.out.println("user : [ "+user+ " ]");
                    userset.add(user);
                }
                String stringusr = String.join(", ", userset);
                System.out.println(stringusr);
                cbsuserGShift1map.put("GSHIFT",stringusr.replaceAll("\\s+", ""));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void getEFMGSsh2User() {

        try {
            Set<String> userset = new HashSet<>();
            String sql = "SELECT * FROM CMS_USER_SHIFTS  WHERE PROJECT like '%EFM%' AND HLMARKFLAG='N' and SHIFT IN ('SHIFT2','GSHIFT')";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    System.out.println("EFM users on General Shift with Shift 2:" + efmuserGShift2map);
                    String user = rs.getString("USERNAME");
                    System.out.println("user : [ "+user+ " ]");
                    userset.add(user);
                }
                String stringusr = String.join(", ", userset);
                efmuserGShift2map.put("GSHIFT",stringusr.replaceAll("\\s+", ""));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getRDEGSsh2User() {

        try {
            Set<String> userset = new HashSet<>();
            String sql = "SELECT * FROM CMS_USER_SHIFTS  WHERE PROJECT like '%RDE%' AND HLMARKFLAG='N' and SHIFT IN ('SHIFT2','GSHIFT')";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    System.out.println("RDE users on General Shift with shift 2:" + rdeuserGShift2map);
                    String user = rs.getString("USERNAME");
                    System.out.println("user : [ "+user+ " ]");
                    userset.add(user);
                }
                String stringusr = String.join(", ", userset);
                System.out.println(stringusr);
                rdeuserGShift2map.put("GSHIFT",stringusr.replaceAll("\\s+", ""));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void getCBSGSsh2User() {

        try {
            Set<String> userset = new HashSet<>();
            String sql = "SELECT * FROM CMS_USER_SHIFTS  WHERE PROJECT like '%CBS%' AND HLMARKFLAG='N' and SHIFT IN ('SHIFT2','GSHIFT')";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    System.out.println("CBS users on General Shift & shift2:" + cbsuserGShift2map);
                    String user = rs.getString("USERNAME");
                    System.out.println("user : [ "+user+ " ]");
                    userset.add(user);
                }
                String stringusr = String.join(", ", userset);
                System.out.println(stringusr);
                cbsuserGShift2map.put("GSHIFT",stringusr.replaceAll("\\s+", ""));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }






    public void getRDEUser() {
        try {
            String sql = "SELECT * FROM CMS_USER_SHIFTS WHERE PROJECT like '%RDE%' AND HLMARKFLAG='N'";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    logger.info("RDE users and shift time:" + rdeusermap);

                    String shift = rs.getString("SHIFT");
                    String user = rs.getString("USERNAME");

                    if (rdeusermap.containsKey(shift)) {
                        logger.info("Contains shift hence replacing : [ " + shift + " ]");
                        String usr = rdeusermap.get(shift);
                        usr += "," + user;
                        logger.info(usr);
                        rdeusermap.replace(shift, usr);

                    } else {
                        logger.info("At first shift : [ " + shift + " ]");
                        rdeusermap.put(shift, user);
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getCBSUser() {
        try {
            String sql = "SELECT * FROM CMS_USER_SHIFTS WHERE PROJECT like'%CBS%' AND HLMARKFLAG='N'";
            try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    logger.info("CBS users and shift time:" + cbsusermap);

                    String shift = rs.getString("SHIFT");
                    String user = rs.getString("USERNAME");

                    if (cbsusermap.containsKey(shift)) {
                        logger.info("Contains shift hence replacing : [ " + shift + " ]");
                        String usr = cbsusermap.get(shift);
                        usr += "," + user;
                        logger.info(usr);
                        cbsusermap.replace(shift, usr);

                    } else {
                        logger.info("At first shift : [ " + shift + " ]");
                        cbsusermap.put(shift, user);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static synchronized String getEFMUserSync(String shift) {
        String assignUser = null;
        for (String key : efmusermap.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN EFM USER MAP");
                    if (efmusermap.get(key).contains(",")) {
                        String arr[] = efmusermap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrefmUsrMap.get(key) == null || conCurrefmUsrMap.size() == 0) {
                            conCurrefmUsrMap.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrefmUsrMap.size() > 0 && conCurrefmUsrMap.get(key) < length) {
                            int val = conCurrefmUsrMap.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrefmUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrefmUsrMap.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrefmUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrefmUsrMap.get(shift)];
                        logger.info("User is : [ "+assignUser+ " ]");
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + efmusermap.get(key) + " ] ");
                        assignUser = efmusermap.get(key);
                        logger.info("User hence no comma: [ "+assignUser+ " ]");
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in EFM user hence returning the first user");
                    return efmusermap.get(key).contains(",") ? efmusermap.get(key).split(",")[0] : efmusermap.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getEFMUserSyncGshift1(String shift) {
        String assignUser = null;
        for (String key : efmuserGShift1map.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    System.out.println("SHIFT PRESENT IN EFM USER MAP");
                    if (efmuserGShift1map.get(key).contains(",")) {
                        String arr[] = efmuserGShift1map.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrefmUsrMapGshift1.get(key) == null || conCurrefmUsrMapGshift1.size() == 0) {
                            conCurrefmUsrMapGshift1.put(key, 0);
                            System.out.println("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrefmUsrMapGshift1.size() > 0 && conCurrefmUsrMapGshift1.get(key) < length) {
                            int val = conCurrefmUsrMapGshift1.get(key);
                            System.out.println("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrefmUsrMapGshift1.replace(key, val + 1);
                        } else {
                            if (conCurrefmUsrMapGshift1.get(key) >= length) {
                                System.out.println("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrefmUsrMapGshift1.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrefmUsrMapGshift1.get(shift)];
                        System.out.println("User is : [ "+assignUser+ " ]");
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + efmuserGShift1map.get(key) + " ] ");
                        assignUser = efmuserGShift1map.get(key);
                        System.out.println("User hence no comma: [ "+assignUser+ " ]");
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in EFM user hence returning the first user");
                    return efmuserGShift1map.get(key).contains(",") ? efmuserGShift1map.get(key).split(",")[0] : efmuserGShift1map.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getEFMUserSyncGshift2(String shift) {
        String assignUser = null;
        for (String key : efmuserGShift2map.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN EFM USER MAP");
                    if (efmuserGShift2map.get(key).contains(",")) {
                        String arr[] = efmuserGShift2map.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrefmUsrMapGshift2.get(key) == null || conCurrefmUsrMapGshift2.size() == 0) {
                            conCurrefmUsrMapGshift2.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrefmUsrMapGshift2.size() > 0 && conCurrefmUsrMapGshift2.get(key) < length) {
                            int val = conCurrefmUsrMapGshift2.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrefmUsrMapGshift2.replace(key, val + 1);
                        } else {
                            if (conCurrefmUsrMapGshift2.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrefmUsrMapGshift2.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrefmUsrMapGshift2.get(shift)];
                        logger.info("User is : [ "+assignUser+ " ]");
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + efmuserGShift2map.get(key) + " ] ");
                        assignUser = efmuserGShift2map.get(key);
                        logger.info("User hence no comma: [ "+assignUser+ " ]");
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in EFM user hence returning the first user");
                    return efmuserGShift2map.get(key).contains(",") ? efmuserGShift2map.get(key).split(",")[0] : efmuserGShift2map.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getRDEUserSyncGshift1(String shift) {
        String assignUser = null;
        for (String key : rdeuserGShift1map.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                   logger.info("SHIFT PRESENT IN RDE USER MAP");
                    if (rdeuserGShift1map.get(key).contains(",")) {
                        String arr[] = rdeuserGShift1map.get(key).split(",");
                        int length = arr.length - 1;
                        //System.out.println("conCurrrdeUsrMap.get(key)--> " + conCurrrdeUsrMap.get(key));
                        if (conCurrrdeUsrMapGshift1.get(key) == null || conCurrrdeUsrMapGshift1.size() == 0) {
                            conCurrrdeUsrMapGshift1.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                           // System.out.println("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrrdeUsrMapGshift1.size() > 0 && conCurrrdeUsrMapGshift1.get(key) < length) {
                            int val = conCurrrdeUsrMapGshift1.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrrdeUsrMapGshift1.replace(key, val + 1);
                        } else {
                            if (conCurrrdeUsrMapGshift1.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrrdeUsrMapGshift1.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrrdeUsrMapGshift1.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + rdeuserGShift1map.get(key) + " ] ");
                        assignUser = rdeuserGShift1map.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in RDE user hence returning the first user");
                    return rdeuserGShift1map.get(key).contains(",") ? rdeuserGShift1map.get(key).split(",")[0] : rdeuserGShift1map.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getRDEUserSyncGshift2(String shift) {
        String assignUser = null;
        for (String key : rdeuserGShift2map.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN RDE USER MAP");
                    if (rdeuserGShift2map.get(key).contains(",")) {
                        String arr[] = rdeuserGShift2map.get(key).split(",");
                        int length = arr.length - 1;
                        //System.out.println("conCurrrdeUsrMap.get(key)--> " + conCurrrdeUsrMap.get(key));
                        if (conCurrrdeUsrMapGshift2.get(key) == null || conCurrrdeUsrMapGshift2.size() == 0) {
                            conCurrrdeUsrMapGshift2.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                            // System.out.println("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrrdeUsrMapGshift2.size() > 0 && conCurrrdeUsrMapGshift2.get(key) < length) {
                            int val = conCurrrdeUsrMapGshift2.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrrdeUsrMapGshift2.replace(key, val + 1);
                        } else {
                            if (conCurrrdeUsrMapGshift2.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrrdeUsrMapGshift2.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrrdeUsrMapGshift2.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + rdeuserGShift2map.get(key) + " ] ");
                        assignUser = rdeuserGShift2map.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in RDE user hence returning the first user");
                    return rdeuserGShift2map.get(key).contains(",") ? rdeuserGShift2map.get(key).split(",")[0] : rdeuserGShift2map.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getRDEUserSync(String shift) {
        String assignUser = null;
        for (String key : rdeusermap.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN RDE USER MAP");
                    if (rdeusermap.get(key).contains(",")) {
                        String arr[] = rdeusermap.get(key).split(",");
                        int length = arr.length - 1;
                        //System.out.println("conCurrrdeUsrMap.get(key)--> " + conCurrrdeUsrMap.get(key));
                        if (conCurrrdeUsrMap.get(key) == null || conCurrrdeUsrMap.size() == 0) {
                            conCurrrdeUsrMap.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                            // System.out.println("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrrdeUsrMap.size() > 0 && conCurrrdeUsrMap.get(key) < length) {
                            int val = conCurrrdeUsrMap.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrrdeUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrrdeUsrMap.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrrdeUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrrdeUsrMap.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + rdeusermap.get(key) + " ] ");
                        assignUser = rdeusermap.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in RDE user hence returning the first user");
                    return rdeusermap.get(key).contains(",") ? rdeusermap.get(key).split(",")[0] : rdeusermap.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getCBSUserSync(String shift) {
        String assignUser = null;
        for (String key : cbsusermap.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN CBS USER MAP");
                    if (cbsusermap.get(key).contains(",")) {
                        String arr[] = cbsusermap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrcbsUsrMap.get(key) == null || conCurrcbsUsrMap.size() == 0) {
                            conCurrcbsUsrMap.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrcbsUsrMap.size() > 0 && conCurrcbsUsrMap.get(key) < length) {
                            int val = conCurrcbsUsrMap.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrcbsUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrcbsUsrMap.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrcbsUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrcbsUsrMap.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + cbsusermap.get(key) + " ] ");
                        assignUser = cbsusermap.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in CBS user hence returning the first user");
                    return cbsusermap.get(key).contains(",") ? cbsusermap.get(key).split(",")[0] : cbsusermap.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getCBSUserSyncGshift1(String shift) {
        String assignUser = null;
        for (String key : cbsuserGShift1map.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN CBS USER MAP");
                    if (cbsuserGShift1map.get(key).contains(",")) {
                        String arr[] = cbsuserGShift1map.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrcbsUsrMapGshift1.get(key) == null || conCurrcbsUsrMapGshift1.size() == 0) {
                            conCurrcbsUsrMapGshift1.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrcbsUsrMapGshift1.size() > 0 && conCurrcbsUsrMapGshift1.get(key) < length) {
                            int val = conCurrcbsUsrMapGshift1.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrcbsUsrMapGshift1.replace(key, val + 1);
                        } else {
                            if (conCurrcbsUsrMapGshift1.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrcbsUsrMapGshift1.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrcbsUsrMapGshift1.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + cbsuserGShift1map.get(key) + " ] ");
                        assignUser = cbsuserGShift1map.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in CBS user hence returning the first user");
                    return cbsuserGShift1map.get(key).contains(",") ? cbsuserGShift1map.get(key).split(",")[0] : cbsuserGShift1map.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getCBSUserSyncGshift2(String shift) {
        String assignUser = null;
        for (String key : cbsuserGShift2map.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN CBS USER MAP");
                    if (cbsuserGShift2map.get(key).contains(",")) {
                        String arr[] = cbsuserGShift2map.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrcbsUsrMapGshift2.get(key) == null || conCurrcbsUsrMapGshift2.size() == 0) {
                            conCurrcbsUsrMapGshift2.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrcbsUsrMapGshift2.size() > 0 && conCurrcbsUsrMapGshift2.get(key) < length) {
                            int val = conCurrcbsUsrMapGshift2.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrcbsUsrMapGshift2.replace(key, val + 1);
                        } else {
                            if (conCurrcbsUsrMapGshift2.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrcbsUsrMapGshift2.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrcbsUsrMapGshift2.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + cbsuserGShift2map.get(key) + " ] ");
                        assignUser = cbsuserGShift2map.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in CBS user hence returning the first user");
                    return cbsuserGShift2map.get(key).contains(",") ? cbsuserGShift2map.get(key).split(",")[0] : cbsuserGShift2map.get(key);

                }
            }
        }
        return assignUser;
    }


    public  void addShift() {


        if (nestedshift.size() == 0) {
            logger.info("adding the shift in the map");
            try {
                String sql = "SELECT * FROM CMS_SHIFT_TIMING ";
                try(Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()){
                logger.info("selecting from cms shift timing and loading in map");
                while (rs.next()) {
                  String shift = rs.getString("SHIFT");
                  String frmhour =  rs.getString("FROMHOUR") ;
                  String frmmin = rs.getString("FROMMINUTE");
                  String tohr = rs.getString("TOHOUR") ;
                  String tomin  =rs.getString("TOMINUTE");

                  shifitiming.put(frmhour+":"+frmmin,tohr+":"+tomin);
                  nestedshift.put(shift,shifitiming);
                  shifitiming = new ConcurrentHashMap<>();

                }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.info("Exception in adding shift in table");
            }
        }
    }

}


