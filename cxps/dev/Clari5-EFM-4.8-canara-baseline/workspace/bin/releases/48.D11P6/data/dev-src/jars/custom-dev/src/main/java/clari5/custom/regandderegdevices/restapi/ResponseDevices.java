package clari5.custom.regandderegdevices.restapi;
import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.Rdbms;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
/*created by raghvendra patel
 * on 11/112/19
 */
@Path("/devices")
public class ResponseDevices {
    public static CxpsLogger logger = CxpsLogger.getLogger(ResponseDevices.class);

    @GET
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    public  String test(){

        return "Raghavendra";
    }

    @POST
    @Path("/trustedDevices")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String trustedDevices(@RequestBody User user){
        logger.info("In trustedDevices(-)");
        String userid=user.getUserid();
        String deviceid="";
        String ipaddress="";
        String time="";
        JSONObject devicejsonobject = new JSONObject();
        JSONArray array = new JSONArray();
        int i=0;
        logger.info(" Listing of Trusted Devices for Usre Id  "+userid);

        try {
            String sql="SELECT DEVICE_ID,IP_ADDRESS,SYS_TIME FROM TRUST_DEV_TBL WHERE DEL_FLAG!='Y'AND USER_ID='"+userid+"'";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                     deviceid=rs.getString("DEVICE_ID");
                     ipaddress=rs.getString("IP_ADDRESS");
                     time=rs.getString("SYS_TIME");
                     JSONObject userjsonobject = new JSONObject();
                     userjsonobject.put("deviceid",deviceid);
                     userjsonobject.put("ipaddress",ipaddress);
                     userjsonobject.put("time",time);
                     array.put(i,userjsonobject);
                    // System.out.println("JSON ARRAY "+array.toString());
                     //System.out.println("JSON Object "+userjsonobject.toString());
                     i++;

                }


                devicejsonobject.put("userid",userid);
                devicejsonobject.put("trusteddevices",array);
                logger.info("Result Json is "+devicejsonobject.toString());


            }catch (SQLException e){
                e.printStackTrace();
                logger.info("Exception in try block of trustedDevices()");

            }

        }catch (Exception e){
               e.printStackTrace();
        }

        logger.info("Response Json "+devicejsonobject.toString());
        return devicejsonobject.toString();


    }
    @POST
    @Path("/deRegisterDevice")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deRegisterDevice(@RequestBody User user){
        logger.info("In deRegisterDevice");
        JSONObject deregdevjsonobjec=new JSONObject();
        String userid=user.getUserid();
        String deviceid=user.getDeviceid();
        String ipaddress=user.getIpaddress();
        String time=user.getTime();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String updatedate = formatter.format(date);
        try {
            String sql="UPDATE TRUST_DEV_TBL SET DEL_FLAG=?, LAST_CHG_TS=? WHERE USER_ID=? AND DEVICE_ID=? AND DEL_FLAG!=?";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1,"Y");
                ps.setString(2,updatedate);
                ps.setString(3,userid);
                ps.setString(4,deviceid);
                ps.setString(5,"Y");

                int i=ps.executeUpdate();
                if(i>0){
                    deregdevjsonobjec.put("userid",userid);
                    deregdevjsonobjec.put("deviceid",deviceid);
                    deregdevjsonobjec.put("status","success");
                }else {
                    deregdevjsonobjec.put("userid",userid);
                    deregdevjsonobjec.put("deviceid",deviceid);
                    deregdevjsonobjec.put("status","Error");
                    logger.info("This Device is already de-register");
                }
                con.commit();
                logger.info("Committed sucessfully");
                logger.info("inserted: [ " + i + "]");

            }catch (SQLException e){
                e.printStackTrace();
                logger.info("Exception in try block of deRegisterDevice()");
            }

            }catch (Exception e){
            e.printStackTrace();
        }

        logger.info("Respose data for Soft De Register "+deregdevjsonobjec.toString());
        return deregdevjsonobjec.toString();
    }
}
