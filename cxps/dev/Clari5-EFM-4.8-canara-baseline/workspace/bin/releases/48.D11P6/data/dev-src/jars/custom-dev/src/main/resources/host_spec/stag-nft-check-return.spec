clari5.hfdb.entity.nft_check_return
{

  attributes = [
{ name = CL5_FLG,  type= "string:20"}
{ name = CREATED_ON,  type= timestamp}
{ name = UPDATED_ON,  type= timestamp}
{ name = ID, type="string:2000", key = true}
{ name = host_id, type = "string:20"}
{ name = sys_time, type = timestamp}
{ name = channel, type = "string:20"}
{ name = account_id, type = "string:20"}
{ name = acct_open_date, type = timestamp}
{ name = cheque_number, type = "string:20"}
{ name = txn_amt, type="number:11,2"}
{ name = reason_code, type = "string:20"}
{ name = reason_desc, type = "string:20"}
{ name = return_type, type = "string:20"}
{ name = schm_type, type = "string:20"}
{ name = schm_code, type = "string:20"}
{ name = cust_id, type="string:20"}
{ name = cheq1, type = "string:20" }
{ name = tran_date, type= timestamp}
{ name = cheque_flag, type = "string:20"}
   ]
}

