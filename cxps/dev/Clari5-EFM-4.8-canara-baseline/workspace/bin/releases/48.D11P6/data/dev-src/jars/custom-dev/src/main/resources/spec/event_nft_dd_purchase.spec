cxps.events.event.nft-dd-purchase{
  table-name : EVENT_NFT_DD_PURCHASE
  event-mnemonic: DPR
  workspaces : {
    NONCUSTOMER : dd-num
}
  event-attributes : {
        host-id: {db:true ,raw_name : host_id ,type:"string:2"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        dd-issue-amt: {db:true ,raw_name : dd_issue_amt ,type : "number:20,2"}
        dd-issue-acct: {db : true ,raw_name : dd_issue_acct ,type : "string:20"}
        schm-type: {db : true ,raw_name : schm_type ,type : "string:20"}
        schm-code: {db : true ,raw_name : schm_code ,type : "string:20"}
        dd-issue-custid: {db : true ,raw_name : dd_issue_custid ,type : "string:20"}
        acct-name: {db : true ,raw_name : acct_name ,type : "string:20"}
        acct-sol-id: {db : true ,raw_name : acct_sol_id ,type : "string:20"}
        acct-ownership: {db : true ,raw_name : acct_ownership ,type : "string:20"}
        acct-open-date: {db :true ,raw_name :acct_open_date ,type : timestamp}
        tran-crncy-code: {db :true ,raw_name : tran_crncy_code ,type : "string:20"}
        ref-txn-amt: {db : true ,raw_name : ref_txn_amt ,type : "number:20,2"}
        ben-name: {db : true ,raw_name : ben_name ,type : "string:20"}
        pstd-user-id: {db : true ,raw_name : pstd_user_id ,type : "string:20"}
        tran-category: {db : true ,raw_name : tran_category ,type : "string:20"}
        dd-issue-date: {db : true ,raw_name : dd_issue_date ,type : timestamp}
        dd-num: {db : true ,raw_name : dd_num ,type : "string:20"}
        part-tran-srl-num: {db : true ,raw_name : part_tran_srl_num ,type : "number:20,2"}
        ben-account-id: {db : true ,raw_name : ben_account_id ,type : "string:20"}
        ben-bank-code: {db : true ,raw_name : ben_bank_code ,type : "string:20"}
        ben-sol-id: {db :true ,raw_name : ben_sol_id ,type : "string:20"}
        value-date: {db :true ,raw_name : value_date ,type : timestamp}
        acct-status: {db : true ,raw_name : acct_status ,type : "string:10"}
        txn-br-id: {db : true ,raw_name : txn_br_id ,type : "string:10"}
        mnemonic-code: {db : true ,raw_name : mnemonic_code ,type : "string:10"}
        tran-date: {db : true ,raw_name : tran_date ,type : timestamp }
        dd-pur-name: {db : true ,raw_name : dd_pur_name ,type: "string:20" }
        tran-code: {db : true ,raw_name : tran_code ,type: "string:20" }
     

}
}
