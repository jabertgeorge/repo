// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_BranchadvancesEventMapper extends EventMapper<NFT_BranchadvancesEvent> {

public NFT_BranchadvancesEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_BranchadvancesEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_BranchadvancesEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_BranchadvancesEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_BranchadvancesEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_BranchadvancesEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_BranchadvancesEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getBranchCode());
            preparedStatement.setDouble(i++, obj.getPreviousDayAdvanceAmt());
            preparedStatement.setDouble(i++, obj.getPreviousMonthAdvanceAmt());
            preparedStatement.setDouble(i++, obj.getPreviousQuarterAdvanceAmt());
            preparedStatement.setDouble(i++, obj.getCurrentMonthAdvanceAmt());
            preparedStatement.setDouble(i++, obj.getCurrentDayAdvanceAmt());
            preparedStatement.setString(i++, obj.getBranchName());
            preparedStatement.setDouble(i++, obj.getCurrentQuarterAdvanceAmt());
            preparedStatement.setTimestamp(i++, obj.getEventDateTime());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_BRANCHADVANCES]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_BranchadvancesEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_BranchadvancesEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_BRANCHADVANCES"));
        putList = new ArrayList<>();

        for (NFT_BranchadvancesEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "BRANCH_CODE",  obj.getBranchCode());
            p = this.insert(p, "PREVIOUS_DAY_ADVANCE_AMT", String.valueOf(obj.getPreviousDayAdvanceAmt()));
            p = this.insert(p, "PREVIOUS_MONTH_ADVANCE_AMT", String.valueOf(obj.getPreviousMonthAdvanceAmt()));
            p = this.insert(p, "PREVIOUS_QUARTER_ADVANCE_AMT", String.valueOf(obj.getPreviousQuarterAdvanceAmt()));
            p = this.insert(p, "CURRENT_MONTH_ADVANCE_AMT", String.valueOf(obj.getCurrentMonthAdvanceAmt()));
            p = this.insert(p, "CURRENT_DAY_ADVANCE_AMT", String.valueOf(obj.getCurrentDayAdvanceAmt()));
            p = this.insert(p, "BRANCH_NAME",  obj.getBranchName());
            p = this.insert(p, "CURRENT_QUARTER_ADVANCE_AMT", String.valueOf(obj.getCurrentQuarterAdvanceAmt()));
            p = this.insert(p, "EVENT_DATE_TIME", String.valueOf(obj.getEventDateTime()));
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_BRANCHADVANCES"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_BRANCHADVANCES]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_BRANCHADVANCES]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_BranchadvancesEvent obj = new NFT_BranchadvancesEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setBranchCode(rs.getString("BRANCH_CODE"));
    obj.setPreviousDayAdvanceAmt(rs.getDouble("PREVIOUS_DAY_ADVANCE_AMT"));
    obj.setPreviousMonthAdvanceAmt(rs.getDouble("PREVIOUS_MONTH_ADVANCE_AMT"));
    obj.setPreviousQuarterAdvanceAmt(rs.getDouble("PREVIOUS_QUARTER_ADVANCE_AMT"));
    obj.setCurrentMonthAdvanceAmt(rs.getDouble("CURRENT_MONTH_ADVANCE_AMT"));
    obj.setCurrentDayAdvanceAmt(rs.getDouble("CURRENT_DAY_ADVANCE_AMT"));
    obj.setBranchName(rs.getString("BRANCH_NAME"));
    obj.setCurrentQuarterAdvanceAmt(rs.getDouble("CURRENT_QUARTER_ADVANCE_AMT"));
    obj.setEventDateTime(rs.getTimestamp("EVENT_DATE_TIME"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_BRANCHADVANCES]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_BranchadvancesEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_BranchadvancesEvent> events;
 NFT_BranchadvancesEvent obj = new NFT_BranchadvancesEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_BRANCHADVANCES"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_BranchadvancesEvent obj = new NFT_BranchadvancesEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setBranchCode(getColumnValue(rs, "BRANCH_CODE"));
            obj.setPreviousDayAdvanceAmt(EventHelper.toDouble(getColumnValue(rs, "PREVIOUS_DAY_ADVANCE_AMT")));
            obj.setPreviousMonthAdvanceAmt(EventHelper.toDouble(getColumnValue(rs, "PREVIOUS_MONTH_ADVANCE_AMT")));
            obj.setPreviousQuarterAdvanceAmt(EventHelper.toDouble(getColumnValue(rs, "PREVIOUS_QUARTER_ADVANCE_AMT")));
            obj.setCurrentMonthAdvanceAmt(EventHelper.toDouble(getColumnValue(rs, "CURRENT_MONTH_ADVANCE_AMT")));
            obj.setCurrentDayAdvanceAmt(EventHelper.toDouble(getColumnValue(rs, "CURRENT_DAY_ADVANCE_AMT")));
            obj.setBranchName(getColumnValue(rs, "BRANCH_NAME"));
            obj.setCurrentQuarterAdvanceAmt(EventHelper.toDouble(getColumnValue(rs, "CURRENT_QUARTER_ADVANCE_AMT")));
            obj.setEventDateTime(EventHelper.toTimestamp(getColumnValue(rs, "EVENT_DATE_TIME")));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_BRANCHADVANCES]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_BRANCHADVANCES]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"BRANCH_CODE\",\"PREVIOUS_DAY_ADVANCE_AMT\",\"PREVIOUS_MONTH_ADVANCE_AMT\",\"PREVIOUS_QUARTER_ADVANCE_AMT\",\"CURRENT_MONTH_ADVANCE_AMT\",\"CURRENT_DAY_ADVANCE_AMT\",\"BRANCH_NAME\",\"CURRENT_QUARTER_ADVANCE_AMT\",\"EVENT_DATE_TIME\"" +
              " FROM EVENT_NFT_BRANCHADVANCES";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [BRANCH_CODE],[PREVIOUS_DAY_ADVANCE_AMT],[PREVIOUS_MONTH_ADVANCE_AMT],[PREVIOUS_QUARTER_ADVANCE_AMT],[CURRENT_MONTH_ADVANCE_AMT],[CURRENT_DAY_ADVANCE_AMT],[BRANCH_NAME],[CURRENT_QUARTER_ADVANCE_AMT],[EVENT_DATE_TIME]" +
              " FROM EVENT_NFT_BRANCHADVANCES";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`BRANCH_CODE`,`PREVIOUS_DAY_ADVANCE_AMT`,`PREVIOUS_MONTH_ADVANCE_AMT`,`PREVIOUS_QUARTER_ADVANCE_AMT`,`CURRENT_MONTH_ADVANCE_AMT`,`CURRENT_DAY_ADVANCE_AMT`,`BRANCH_NAME`,`CURRENT_QUARTER_ADVANCE_AMT`,`EVENT_DATE_TIME`" +
              " FROM EVENT_NFT_BRANCHADVANCES";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_BRANCHADVANCES (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"BRANCH_CODE\",\"PREVIOUS_DAY_ADVANCE_AMT\",\"PREVIOUS_MONTH_ADVANCE_AMT\",\"PREVIOUS_QUARTER_ADVANCE_AMT\",\"CURRENT_MONTH_ADVANCE_AMT\",\"CURRENT_DAY_ADVANCE_AMT\",\"BRANCH_NAME\",\"CURRENT_QUARTER_ADVANCE_AMT\",\"EVENT_DATE_TIME\") values(?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[BRANCH_CODE],[PREVIOUS_DAY_ADVANCE_AMT],[PREVIOUS_MONTH_ADVANCE_AMT],[PREVIOUS_QUARTER_ADVANCE_AMT],[CURRENT_MONTH_ADVANCE_AMT],[CURRENT_DAY_ADVANCE_AMT],[BRANCH_NAME],[CURRENT_QUARTER_ADVANCE_AMT],[EVENT_DATE_TIME]) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`BRANCH_CODE`,`PREVIOUS_DAY_ADVANCE_AMT`,`PREVIOUS_MONTH_ADVANCE_AMT`,`PREVIOUS_QUARTER_ADVANCE_AMT`,`CURRENT_MONTH_ADVANCE_AMT`,`CURRENT_DAY_ADVANCE_AMT`,`BRANCH_NAME`,`CURRENT_QUARTER_ADVANCE_AMT`,`EVENT_DATE_TIME`) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

