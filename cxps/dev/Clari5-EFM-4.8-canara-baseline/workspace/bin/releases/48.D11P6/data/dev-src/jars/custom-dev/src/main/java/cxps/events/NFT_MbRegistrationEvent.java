// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_MBREG", Schema="rice")
public class NFT_MbRegistrationEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String deviceId;
       @Field(size=10) public String mobileNo;
       @Field(size=20) public String ipAddress;
       @Field(size=20) public String pwdResetType;
       @Field(size=2) public String hostId;
       @Field(size=10) public String countryCode;
       @Field(size=2) public String succFailFlg;
       @Field(size=20) public String userId;
       @Field(size=100) public String errorDesc;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=10) public String errorCode;
       @Field(size=20) public String custId;
       @Field(size=20) public String appId;


    @JsonIgnore
    public ITable<NFT_MbRegistrationEvent> t = AEF.getITable(this);

	public NFT_MbRegistrationEvent(){}

    public NFT_MbRegistrationEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("MbRegistration");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setDeviceId(json.getString("device_id"));
            setMobileNo(json.getString("mobile_no"));
            setIpAddress(json.getString("ip_address"));
            setPwdResetType(json.getString("pwd_reset_type"));
            setHostId(json.getString("host_id"));
            setCountryCode(json.getString("country_code"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setUserId(json.getString("user_id"));
            setErrorDesc(json.getString("error_desc"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setErrorCode(json.getString("error_code"));
            setCustId(json.getString("cust_id"));
            setAppId(json.getString("app_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NMB"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getDeviceId(){ return deviceId; }

    public String getMobileNo(){ return mobileNo; }

    public String getIpAddress(){ return ipAddress; }

    public String getPwdResetType(){ return pwdResetType; }

    public String getHostId(){ return hostId; }

    public String getCountryCode(){ return countryCode; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getUserId(){ return userId; }

    public String getErrorDesc(){ return errorDesc; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getErrorCode(){ return errorCode; }

    public String getCustId(){ return custId; }

    public String getAppId(){ return appId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setPwdResetType(String val){ this.pwdResetType = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setAppId(String val){ this.appId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_MbRegistrationEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.mobileNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_MbRegistration");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "MbRegistration");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}