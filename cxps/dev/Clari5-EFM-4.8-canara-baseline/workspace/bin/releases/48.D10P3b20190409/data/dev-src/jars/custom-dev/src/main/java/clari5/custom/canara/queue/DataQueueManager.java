package clari5.custom.canara.queue;

import clari5.custom.canara.config.BepCon;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

public class DataQueueManager {
	private static int tableCounter = 0;
	// As of now boolean, change later.
	private static HashMap<String,String> tableToClassMap;
	private static boolean doneTables = false;
	static {
		try{
		tableToClassMap = BepCon.getConfig().getTableClassMap();
		System.out.println("table to class map initialized");
	   }catch (Exception e){
			System.out.println("Table to class mapping is not proper please check BatchProcessor.config");
		}
	}
	public static Class<?> nextTable(String tableName) throws Exception {

		String className=tableToClassMap.get(tableName);
		System.out.println(" Class name is "+className);
		Class<?> tableC = null;
		tableC = Class.forName("clari5.custom.canara.data."+className);
		return tableC;
	}
	public static synchronized boolean isDoneTables() {
		return doneTables;
	}
	public static synchronized void setDoneTables(boolean doneTables) {
		DataQueueManager.doneTables = doneTables;
	}
}
