package clari5.custom.canara.db;

import clari5.custom.canara.config.BepCon;
import clari5.custom.canara.data.ITableData;
import clari5.rdbms.Rdbms;
import com.google.gson.Gson;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBTask {
    protected static CxpsLogger logger = CxpsLogger.getLogger(DBTask.class);

    public static synchronized <T> List<T> getRows(Class<T> T, String tableName) throws Exception {
        logger.info(Thread.currentThread().getName() + " Fetching records from table " + tableName);
        long startTime = System.currentTimeMillis();
        Connection con = null;
        QueryRunner run = new QueryRunner();

        // Use the BeanListHandler implementation to convert all
        // ResultSet rows into a List of Person JavaBeans.
        ResultSetHandler<List<T>> h = new BeanListHandler<T>(T);
        String updateQuery = null;
        // Execute the SQL statement and return the results in a List of
        // Person objects generated by the BeanListHandler.
        List<T> rows = null;
        //Set<String> eventIdSet = new HashSet<String>();
        List<String> eventIdSet = new ArrayList<String>();
        PreparedStatement ps = null;
        //Connection connection = null;
        try {
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            String selectFields = BepCon.tableSelectCols.get(tableName);
            if (selectFields == null)
                selectFields = "*";
            String query = "SELECT " + selectFields + "  FROM " + tableName + " where CL5_FLG = ? and rownum < "+BepCon.RESULT_SET_SIZE+" ";
            System.out.println("selecting fields from  table \n" + query);
            ps = con.prepareStatement(query);
            ps.setString(1, "NEW");
            ResultSet res = ps.executeQuery();
            while (res.next()) {
                eventIdSet.add(res.getString("ID"));
            }
            rows = run.query(con, "SELECT "+selectFields+" FROM " + tableName + " where CL5_FLG = 'NEW' and ID in ('" + StringUtils.join(eventIdSet, "','") + "')" + "", h);
            if (rows.size() > 0) {
                logger.info("Found record in table " + tableName + " Marking status to I");
                for (int i = 0; i < rows.size(); i++) {
                    updateQuery = "UPDATE " + tableName + " SET CL5_FLG = 'I' where CL5_FLG = 'NEW' and ID in ('" + StringUtils.join(eventIdSet, "','") + "')" + "";
                    run.update(con, updateQuery);
                }
                con.commit();
            }
        } catch (SQLException e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        long endTime = System.currentTimeMillis();
        logger.info(Thread.currentThread().getName() + "  fetched records from " + tableName + " row size " + rows.size() + "Time taken Millis [" + (endTime - startTime));
        return rows;
    }

    public static List<String> select(String tableName, String column, String[] key, String[] where) throws SQLException {

        Connection con = null;
        QueryRunner run = new QueryRunner();
        ResultSetHandler<List<String>> h = new ResultSetHandler<List<String>>() {
            @Override
            public List<String> handle(ResultSet rs) throws SQLException {
                List<String> result = new ArrayList<String>();
                while (rs.next()) {
                    result.add(rs.getString(1));
                }
                return result;
            }
        };
        List<String> result = null;
        String query = "SELECT " + column + " FROM " + tableName + " where ";
        try {
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            for (int i = 0; i < key.length; ++i) {
                query += buildQuery(key[i], where[i]);
                if (i != (key.length - 1)) {
                    query += " AND ";
                }
            }
            result = run.query(con, query, h);
        } catch (SQLException e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    private static String buildQuery(String key, String where) {
        String query = "";
        String[] keyandop = key.split("\\|");
        if (keyandop.length > 1) {
            switch (keyandop[1]) {
                case ("NOT_EQ"): {
                    query = keyandop[0] + " != " + "'" + where + "'";
                    break;
                }
                case ("EQ"): {
                    query = keyandop[0] + " = " + "'" + where + "'";
                    break;
                }
            }
        } else {
            query = keyandop[0] + " = " + "'" + where + "'";
        }
        return query;
    }

    public static void updateStatus(Map<ITableData, String> statuss) throws Exception {
        logger.info(Thread.currentThread().getName() + "  update status ");
        Connection con = null;
        QueryRunner run = new QueryRunner();
        try {
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            Gson gson;
            String s;
            JSONObject j;
            for (Map.Entry<ITableData, String> status : statuss.entrySet()) {
                String updateQuery = "";
                gson = new Gson();
                s = gson.toJson(status.getKey());
                j = new JSONObject(s);
                String event_id = "";
                if (j.has("ID")) {
                    event_id = j.getString("ID");
                }
                if (event_id.equals("") || event_id == null) {
                    event_id = j.getString("ID");
                }
                updateQuery = "UPDATE " + status.getKey().getTableName() + " SET CL5_FLG = '" + status.getValue() + "' WHERE ID='" + event_id + "'";
                run.update(con, updateQuery);
                con.commit();
                logger.info(Thread.currentThread().getName() + "  updated status successfully");
            }


        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void update(String tableName, String column, String value, String[] keys, String[] values) throws Exception {

        Connection con = null;
        QueryRunner run = new QueryRunner();
        try {
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            String updateQuery = "UPDATE " + tableName + " SET CL5_FLG = '" + value + "' WHERE ";
            for (int i = 0; i < keys.length; ++i) {
                updateQuery += keys[i] + " = '" + values[i] + "'";
                if (i != (keys.length - 1)) {
                    updateQuery += " AND ";
                }
            }
            run.update(updateQuery, con);
            con.commit();
            logger.info("Marked as processed initiated from a different table (e.g. Remittence) with query " + updateQuery);
        } catch (Exception e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static Map<String, String> selectAllColumns(String tableName, String[] key, String[] where) throws SQLException {
        Connection con = null;
        QueryRunner run = new QueryRunner();
        ResultSetHandler<Map<String, String>> h = new ResultSetHandler<Map<String, String>>() {
            @Override
            public Map<String, String> handle(ResultSet rs) throws SQLException {
                if (!rs.next()) {
                    return null;
                }
                Map<String, String> result = new HashMap<String, String>();
                java.sql.ResultSetMetaData meta = rs.getMetaData();
                int colCount = meta.getColumnCount();
                for (int columnIndex = 1; columnIndex <= colCount; ++columnIndex) {
                    result.put(meta.getColumnName(columnIndex), rs.getString(columnIndex));
                }
                return result;
            }
        };
        Map<String, String> result = null;
        String query = "SELECT * FROM " + tableName + " where ";
        try {
            for (int i = 0; i < key.length; ++i) {
                query += key[i] + " = " + "'" + where[i] + "'";
                if (i != (key.length - 1)) {
                    query += " AND ";
                }
            }
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            result = run.query(con, query, h);
        } catch (SQLException e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static void updateAudit(String[] audit) throws SQLException {
        /*SQLServerDataSource ds = new SQLServerDataSource();
		ds.setURL(url);
		ds.setUser(usr);
		ds.setPassword(pwd);*/
        Connection con = null;
        QueryRunner run = new QueryRunner();

        // Use the BeanListHandler implementation to convert all
        // ResultSet rows into a List of Person JavaBeans.
        ResultSetHandler<List<String>> h = new BeanListHandler<String>(String.class);
        try {
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            @SuppressWarnings("unused")
            List<String> result = run.insert(con, "INSERT INTO DATA_AUDIT_MASTER VALUES('" + audit[0] + "','" + audit[1] + "','" + audit[2] + "')", h);
        } catch (SQLException e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Test method.
    public static void insertRows() throws Exception {
        Connection con = null;
        QueryRunner run = new QueryRunner();
        try {
            con = Rdbms.getAppConnection();
            con.setAutoCommit(false);
            for (int i = 1; i < 4001; ++i) {
                run.update(con, "Insert into SampleTable5 (Id, Country, Bank, Product, Status) Values(" + i + ",\"Country" + i + "\",\"ubp" + i + "\",\"EFM" + i + "\",\"NEW\")");
                Thread.sleep(1000);
            }
        } catch (SQLException e) {
            logger.info(e);
            e.printStackTrace();
            logger.info(e.getMessage());
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unused")
    public static void main(String[] args) throws Exception {
        String[] keys = {"Name"};
        String[] where = {"Rajnikanth4001"};
    }
}
