package clari5.custom.canara.data;

public class NFT_Regen extends ITableData {

    private String tableName = "NFT_REGEN";
    private String event_type = "NFT_Regen";
    private String host_id;
    private String sys_time;
    private String event_id;
    private String channel;
    private String user_id;
    private String user_type;
    private String cust_id;
    private String device_id;
    private String ip_address;
    private String ip_country;
    private String ip_city;
    private String cust_card_id;
    private String succ_fail_flg;
    private String error_code;
    private String error_desc;
    private String regen_type;
    private String app_id;
    private String country_code;
    private String mobile_no;
    private String tran_date;
    private String mask_card_no;

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getIp_country() {
        return ip_country;
    }

    public void setIp_country(String ip_country) {
        this.ip_country = ip_country;
    }

    public String getIp_city() {
        return ip_city;
    }

    public void setIp_city(String ip_city) {
        this.ip_city = ip_city;
    }

    public String getCust_card_id() {
        return cust_card_id;
    }

    public void setCust_card_id(String cust_card_id) {
        this.cust_card_id = cust_card_id;
    }

    public String getSucc_fail_flg() {
        return succ_fail_flg;
    }

    public void setSucc_fail_flg(String succ_fail_flg) {
        this.succ_fail_flg = succ_fail_flg;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_desc() {
        return error_desc;
    }

    public void setError_desc(String error_desc) {
        this.error_desc = error_desc;
    }

    public String getRegen_type() {
        return regen_type;
    }

    public void setRegen_type(String regen_type) {
        this.regen_type = regen_type;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getMask_card_no() {
        return mask_card_no;
    }

    public void setMask_card_no(String mask_card_no) {
        this.mask_card_no = mask_card_no;
    }



}
