package clari5.custom.cms;

import clari5.platform.util.Hocon;

/**
 * Created by jabert on 03/06/19.
 */
class CustomField {
    private String jiraId;
    private String jiraType;
    private String taskType;
    private String caseFieldName;

    public CustomField(Hocon h) {
        this.jiraId = h.getString("jiraId");
        this.jiraType = h.getString("jiraType");
        this.taskType = h.getString("taskType");
        this.caseFieldName = null;
    }

    String getJiraId() {
        return this.jiraId;
    }

    String getJiraType() {
        return this.jiraType;
    }

    String getTaskType() {
        return this.taskType;
    }

    public String getCaseFieldName() {
        return this.caseFieldName;
    }

    public void setCaseFieldName(String caseFieldName) {
        this.caseFieldName = caseFieldName;
    }
}
