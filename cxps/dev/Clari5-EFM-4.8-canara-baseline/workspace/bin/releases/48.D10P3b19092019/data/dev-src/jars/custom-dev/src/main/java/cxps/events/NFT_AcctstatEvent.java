// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCTSTAT", Schema="rice")
public class NFT_AcctstatEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String accountId;
       @Field(size=20) public String schmCode;
       @Field(size=11) public Double avlBal;
       @Field(size=20) public String channel;
       @Field(size=20) public String schmType;
       @Field(size=2) public String hostId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String custId;
       @Field(size=20) public String intialAcctStatus;
       @Field(size=20) public String finalAcctStatus;
       @Field(size=50) public String acctName;


    @JsonIgnore
    public ITable<NFT_AcctstatEvent> t = AEF.getITable(this);

	public NFT_AcctstatEvent(){}

    public NFT_AcctstatEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Acctstat");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setAccountId(json.getString("account_id"));
            setSchmCode(json.getString("schm_code"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setChannel(json.getString("channel"));
            setSchmType(json.getString("schm_type"));
            setHostId(json.getString("host_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setIntialAcctStatus(json.getString("intial_acct_status"));
            setFinalAcctStatus(json.getString("final_acct_status"));
            setAcctName(json.getString("acct_name"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NFA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getAccountId(){ return accountId; }

    public String getSchmCode(){ return schmCode; }

    public Double getAvlBal(){ return avlBal; }

    public String getChannel(){ return channel; }

    public String getSchmType(){ return schmType; }

    public String getHostId(){ return hostId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getIntialAcctStatus(){ return intialAcctStatus; }

    public String getFinalAcctStatus(){ return finalAcctStatus; }

    public String getAcctName(){ return acctName; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setSchmCode(String val){ this.schmCode = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSchmType(String val){ this.schmType = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setIntialAcctStatus(String val){ this.intialAcctStatus = val; }
    public void setFinalAcctStatus(String val){ this.finalAcctStatus = val; }
    public void setAcctName(String val){ this.acctName = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_AcctstatEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Acctstat");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Acctstat");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}