package clari5.custom.canara;


import clari5.custom.canara.data.ITableData;
import clari5.custom.canara.db.DBTask;
import clari5.custom.canara.queue.DataQueue;
import clari5.custom.canara.queue.DataQueueManager;
import clari5.platform.applayer.CxpsDaemon;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import com.google.gson.Gson;
import cxps.apex.utils.CxpsLogger;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.sql.*;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class EventLoader{
    protected static CxpsLogger logger = CxpsLogger.getLogger(EventLoader.class);
    public List<ITableData> getFreshEvents(String tableName) throws Exception {

        DataQueue d=new DataQueue();
        List<ITableData>  data= d.load(tableName);
        return data;

        }
}