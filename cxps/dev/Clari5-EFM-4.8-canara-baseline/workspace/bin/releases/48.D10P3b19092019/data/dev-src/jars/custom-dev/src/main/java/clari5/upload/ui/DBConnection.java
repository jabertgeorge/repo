package clari5.upload.ui;
import java.sql.Connection;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

/**
 * Created by lisa
 */
public class DBConnection {
	private static CxpsLogger logger = CxpsLogger.getLogger(DBConnection.class);
	public static Connection getDBConnection   () {
		Connection con = null;
		try{
			con = Rdbms.getAppConnection();
            logger.info("Obtained Db Connection");
			return  con;
                }catch(Exception e){
                      throw new RuntimeException("Unable to get DB Connection"+e.getMessage() +" Cause "+e.getCause());
		}

	}
}





