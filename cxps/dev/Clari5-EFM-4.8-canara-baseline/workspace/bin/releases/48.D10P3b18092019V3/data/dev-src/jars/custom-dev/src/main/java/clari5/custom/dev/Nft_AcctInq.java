package clari5.custom.dev;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Nft_AcctInq {

    public static JSONObject setNftacctinq(SaxPojo saxPojo)
    {
        if(saxPojo != null) {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("host_id", "F");
            jsonObject.put("sys_time", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
            jsonObject.put("event_id", "nft_acctinq"+System.nanoTime());
            jsonObject.put("channel", saxPojo.getChannel());
            jsonObject.put("account_id", saxPojo.getDrAccountId());
            jsonObject.put("cust_id", saxPojo.getCustId());
            jsonObject.put("acct_name", saxPojo.getCustName());
            jsonObject.put("schm_type", "");
            jsonObject.put("schm_code", "");
            jsonObject.put("avl_bal", saxPojo.getAvlBal());
            jsonObject.put("acct_status","");
            jsonObject.put("branch_id", saxPojo.getBranchId());
            jsonObject.put("menu_id", "");
            jsonObject.put("user_id", saxPojo.getUserId());
            jsonObject.put("cust_card_id", saxPojo.getCustCardId());
            jsonObject.put("tran_date", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
            jsonObject.put("mask_card_no", saxPojo.getMaskCardNo());
            jsonObject.put("country_code", saxPojo.getCountryCode());
            jsonObject.put("addEntity1", "");
            jsonObject.put("addEntity2", "");
            jsonObject.put("addEntity3", "");
            jsonObject.put("addEntity4", "");
            jsonObject.put("addEntity5", "");

            return jsonObject;
        }

        return null;
    }
}
