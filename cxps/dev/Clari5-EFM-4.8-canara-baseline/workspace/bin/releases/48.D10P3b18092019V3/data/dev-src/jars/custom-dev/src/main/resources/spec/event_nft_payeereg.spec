cxps.events.event.nft_payee_reg{
table-name : EVENT_NFT_PAYEEREG
  event-mnemonic: NPR
  workspaces : {
    ACCOUNT : device_id
    NONCUSTOMER : payee_account_id
    CUSTOMER : cust_id
    }
  event-attributes : {
		host_id:{db : true ,raw_name : host_id, type :  "string:2"}
		channel: {db : true ,raw_name : channel ,type : "string:10"}
		user_id: {db : true ,raw_name : user_id ,type : "string:20"}
		user_type: {db : true ,raw_name : user_type ,type : "string:20"}
		cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
		device_id: {db : true ,raw_name : device_id ,type : "string:50"}
		ip_address: {db : true ,raw_name : ip_address ,type : "string:20"}
		ip_country: {db : true ,raw_name : ip_country ,type : "string:20"}
		ip_city: {db : true ,raw_name : ip_city ,type : "string:20"}
		succ_fail_flg: {db : true ,raw_name : succ_fail_flg ,type : "string:2"}
		error_code: {db : true ,raw_name : error_code ,type : "string:10"}
		error_desc: {db : true ,raw_name : error_desc ,type : "string:100"}
		sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
		payee_type: {db : true ,raw_name : payee_type ,type : "string:20"}
		payee_id: {db : true ,raw_name : payee_id ,type : "string:50"}
		payee_name: {db : true ,raw_name : payee_name ,type : "string:50"}
		payee_nickname: {db : true ,raw_name : payee_nickname ,type : "string:20"}
		payee_accounttype: {db : true ,raw_name : payee_accounttype ,type : "string:20"}
		payee_ifsc_code: {db : true ,raw_name : payee_ifsc_code ,type : "string:20"}
		payee_account_id: {db : true ,raw_name : payee_account_id ,type : "string:20"}
		payee_bank_name: {db : true ,raw_name : payee_bank_name ,type : "string:50"}
		payee_bank_id: {db : true ,raw_name : payee_bank_id ,type : "string:50"}
		payee_branch_name: {db : true ,raw_name : payee_branch_name ,type : "string:50"}
		payee_branch_id: {db : true ,raw_name : payee_branch_id ,type : "string:50"}
		cust_name: {db : true ,raw_name : cust_name ,type : "string:50"}
		tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
		payeeactivation_flg: {db : true ,raw_name : payeeactivation_flg ,type : "string:2"}
        }
}
