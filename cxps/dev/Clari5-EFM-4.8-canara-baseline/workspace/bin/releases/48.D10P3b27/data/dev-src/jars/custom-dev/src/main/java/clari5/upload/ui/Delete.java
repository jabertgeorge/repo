package clari5.upload.ui;

import java.util.List;

import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;


public class Delete {
    private static CxpsLogger logger = CxpsLogger.getLogger(DeleteServlet.class);
    List<String> headers;
    List<String> values;
    String tableName;

    Delete(List<String> headers, List<String> values, String tableName) {
        this.headers = headers;
        this.values = values;
        this.tableName = tableName;
    }

    public String getDeleteQuery() {
        logger.info("tableName for which recors will be deleted" + tableName);
        String deleteSql = "delete from " + tableName + " where ";
        logger.info("Delete Query [" + deleteSql + "]");
        String where = "";
        int noCol = headers.size();
        for (int i = 0; i < noCol; ++i) {
            String currentHeader = this.headers.get(i).toString();
            String currentValue = this.values.get(i).toString();
            if (this.values.get(i).equals("null") || this.values.get(i).isEmpty()) {
                //where += "\"" + currentHeader + "\" is null  ";
		where = where + currentHeader  +" IS NULL";

		
            } else {
		where= where + currentHeader + " = "+ "'" +currentValue + "'";		
        	// where += "\"" + currentHeader + "\" = '" + currentValue + "'";
		System.out.println("[getDeleteQuery] Framing Where Condition"+ where);
            }
            if (i != noCol - 1) where += " and ";
        }

        deleteSql += " " + where;
	System.out.println("[getDeleteQuery] Framing Query from Delete" + deleteSql);
        logger.info("Delete Query" + deleteSql);
        return deleteSql;
    }
}
