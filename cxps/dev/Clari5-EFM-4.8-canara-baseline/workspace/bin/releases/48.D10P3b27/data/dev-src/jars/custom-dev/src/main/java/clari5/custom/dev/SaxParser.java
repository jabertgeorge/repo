/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clari5.custom.dev;

import java.io.StringReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import clari5.custom.dev.SaxPojo;

/**
 *
 * @author lakshmisai
 */
public class SaxParser extends DefaultHandler {

    private SaxPojo saxPojo = null;

    boolean bstrtOfTxt;
    boolean bprodId;
    boolean brelNum;
    boolean bstat;
    boolean boriginator;
    boolean bresponder;
    boolean btyp;
    boolean bdat;
    boolean btranCde;
    boolean bfromAcctTyp;
    boolean btoAcctTyp;
    boolean btranAmt;
    boolean bxmitDatTim;
    boolean btraceNum;
    boolean btranTim;
    boolean btranDat;
    boolean bexpDat;
    boolean bcapDat;
    boolean bpTTranSpclCde;
    boolean bnum;
    boolean bseqNum;
    boolean bauthId;
    boolean brespCde;
    boolean btermId;
    boolean bcrdAccptIdCde;
    boolean btermOwner;
    boolean btermCity;
    boolean btermSt;
    boolean btermCntry;
    boolean beventType;
    boolean beventSubType;
    boolean beventName;
    boolean beventId;
    boolean bdateTime;
    boolean bpanData;
    boolean bsourceId;
    boolean bhostId;
    boolean bsysTime;
    boolean bbin;
    boolean bfiid;
    boolean bchannel;
    boolean btxnSecuredFlag;
    boolean buserId;
    boolean buserType;
    boolean bcustId;
    boolean bdeviceId;
    boolean bterminalId;
    boolean bmerchantId;
    boolean bdevOwnerId;
    boolean btranDate;
    boolean bipAddress;
    boolean bipCountry;
    boolean bipCity;
    boolean bstateCode;
    boolean bchipPinFlag;
    boolean bcustName;
    boolean bsuccFailFlg;
    boolean berrorCode;
    boolean berrorDesc;
    boolean bruleId;
    boolean bdrAccountId;
    boolean btxnAmt;
    boolean bavlBal;
    boolean bcrAccountId;
    boolean bcrIFSCCode;
    boolean bpayeeId;
    boolean bpayeeName;
    boolean btranType;
    boolean bentryMode;
    boolean bposEntryMode;
    boolean bcountryCode;
    boolean bcustCardId;
    boolean bMccCode;
    boolean bbranchId;
    boolean bacctOwnership;
    boolean bacctOpenDate;
    boolean bpartTranType;
    boolean bcustMobNo;
    boolean bproductCode;
    boolean bAtmDomLimit;
    boolean bAtmDIntrLimit;
    boolean bposEcomDomLimit;
    boolean bposEcomIntrLimit;
    boolean bmaskCardNo;
    boolean btranParticular;

    public SaxPojo reqParser(String respXml) {

        InputSource inputSource = null;
        StringReader stringReader = null;
        SAXParser saxParser = null;
        SAXParserFactory factory = null;

        try {
            saxPojo = new SaxPojo();
            stringReader = new StringReader(respXml);
            inputSource = new InputSource(stringReader);
            factory = SAXParserFactory.newInstance();
            saxParser = factory.newSAXParser();
            saxParser.parse(inputSource, this);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stringReader.close();
                inputSource = null;
                saxParser = null;
                factory = null;
            } catch (Exception e) {
                stringReader = null;
            }
        }
        return saxPojo;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("STRT_OF_TXT")) {
            bstrtOfTxt = true;
        }
        if (qName.equalsIgnoreCase("PROD_ID")) {
            bprodId = true;
        }
        if (qName.equalsIgnoreCase("REL_NUM")) {
            brelNum = true;
        }
        if (qName.equalsIgnoreCase("STAT")) {
            bstat = true;
        }
        if (qName.equalsIgnoreCase("ORIGINATOR")) {
            boriginator = true;
        }
        if (qName.equalsIgnoreCase("RESPONDER")) {
            bresponder = true;
        }
        if (qName.equalsIgnoreCase("TYP")) {
            btyp = true;
        }
        if (qName.equalsIgnoreCase("DAT")) {
            bdat = true;
        }
        if (qName.equalsIgnoreCase("TRAN_CDE")) {
            btranCde = true;
        }
        if (qName.equalsIgnoreCase("FROM_ACCT_TYP")) {
            bfromAcctTyp = true;
        }
        if (qName.equalsIgnoreCase("TO_ACCT_TYP")) {
            btoAcctTyp = true;
        }
        if (qName.equalsIgnoreCase("TRAN_AMT")) {
            btranAmt = true;
        }
        if (qName.equalsIgnoreCase("XMIT_DAT_TIM")) {
            bxmitDatTim = true;
        }
        if (qName.equalsIgnoreCase("TRACE_NUM")) {
            btraceNum = true;
        }
        if (qName.equalsIgnoreCase("TRAN_TIM")) {
            btranTim = true;
        }
        if (qName.equalsIgnoreCase("TRAN_DAT")) {
            btranDat = true;
        }
        if (qName.equalsIgnoreCase("EXP_DAT")) {
            bexpDat = true;
        }
        if (qName.equalsIgnoreCase("PT_TRAN_SPCL_CDE")) {
            bpTTranSpclCde = true;
        }
        if (qName.equalsIgnoreCase("NUM")) {
            bnum = true;
        }
        if (qName.equalsIgnoreCase("SEQ_NUM")) {
            bseqNum = true;
        }
        if (qName.equalsIgnoreCase("AUTH_ID_RESP")) {
            bauthId = true;
        }
        if (qName.equalsIgnoreCase("RESP_CDE")) {
            brespCde = true;
        }
        if (qName.equalsIgnoreCase("TERM_ID")) {
            btermId = true;
        }
        if (qName.equalsIgnoreCase("CRD_ACCPT_ID_CDE")) {
            bcrdAccptIdCde = true;
        }
        if (qName.equalsIgnoreCase("TERM_OWNER")) {
            btermOwner = true;
        }
        if (qName.equalsIgnoreCase("TERM_CITY")) {
            btermCity = true;
        }
        if (qName.equalsIgnoreCase("TERM_ST")) {
            btermSt = true;
        }
        if (qName.equalsIgnoreCase("TERM_CNTRY")) {
            btermCntry = true;
        }
        if (qName.equalsIgnoreCase("EVENTTYPE")) {
            beventType = true;
        }
        if (qName.equalsIgnoreCase("EVENTSUBTYPE")) {
            beventSubType = true;
        }
        if (qName.equalsIgnoreCase("EVENT_NAME")) {
            beventName = true;
        }
        if (qName.equalsIgnoreCase("EVENT_ID")) {
            beventId = true;
        }
        if (qName.equalsIgnoreCase("DATE_TIME")) {
            bdateTime = true;
        }
        if (qName.equalsIgnoreCase("PAN_DATA")) {
            bpanData = true;
        }
        if (qName.equalsIgnoreCase("SOURCE_ID")) {
            bsourceId = true;
        }
        if (qName.equalsIgnoreCase("HOST_ID")) {
            bhostId = true;
        }
        if (qName.equalsIgnoreCase("SYS_TIME")) {
            bsysTime = true;
        }
        if (qName.equalsIgnoreCase("BIN")) {
            bbin = true;
        }
        if (qName.equalsIgnoreCase("FIID")) {
            bfiid = true;
        }
        if (qName.equalsIgnoreCase("CHANNEL")) {
            bchannel = true;
        }
        if (qName.equalsIgnoreCase("TXN_SECURED_FLAG")) {
            btxnSecuredFlag = true;
        }
        if (qName.equalsIgnoreCase("USER_ID")) {
            buserId = true;
        }
        if (qName.equalsIgnoreCase("USER_TYPE")) {
            buserType = true;
        }
        if (qName.equalsIgnoreCase("CUST_ID")) {
            bcustId = true;
        }
        if (qName.equalsIgnoreCase("DEVICE_ID")) {
            bdeviceId = true;
        }
        if (qName.equalsIgnoreCase("TERMINAL_ID")) {
            bterminalId = true;
        }
        if (qName.equalsIgnoreCase("MERCHANT_ID")) {
            bmerchantId = true;
        }
        if (qName.equalsIgnoreCase("DEV_OWNER_ID")) {
            bdevOwnerId = true;
        }
        if (qName.equalsIgnoreCase("TRAN_DATE")) {
            btranDate = true;
        }
        if (qName.equalsIgnoreCase("IP_ADDRESS")) {
            bipAddress = true;
        }
        if (qName.equalsIgnoreCase("IP_COUNTRY")) {
            bipCountry = true;
        }
        if (qName.equalsIgnoreCase("IP_CITY")) {
            bipCity = true;
        }
        if (qName.equalsIgnoreCase("STATE_CODE")) {
            bstateCode = true;
        }
        if (qName.equalsIgnoreCase("CHIP_PIN_FLG")) {
            bchipPinFlag = true;
        }
        if (qName.equalsIgnoreCase("CUST_NAME")) {
            bcustName = true;
        }
        if (qName.equalsIgnoreCase("SUCC_FAIL_FLG")) {
            bsuccFailFlg = true;
        }
        if (qName.equalsIgnoreCase("ERROR_CODE")) {
            berrorCode = true;
        }
        if (qName.equalsIgnoreCase("ERROR_DESC")) {
            berrorDesc = true;
        }
        if (qName.equalsIgnoreCase("RULE_ID")) {
            bruleId = true;
        }
        if (qName.equalsIgnoreCase("DR_ACCOUNT_ID")) {
            bdrAccountId = true;
        }
        if (qName.equalsIgnoreCase("TXN_AMT")) {
            btxnAmt = true;
        }
        if (qName.equalsIgnoreCase("AVL_BAL")) {
            bavlBal = true;
        }
        if (qName.equalsIgnoreCase("CR_ACCOUNT_ID")) {
            bcrAccountId = true;
        }
        if (qName.equalsIgnoreCase("CR_IFSC_CODE")) {
            bcrIFSCCode = true;
        }
        if (qName.equalsIgnoreCase("PAYEE_ID")) {
            bpayeeId = true;
        }
        if (qName.equalsIgnoreCase("PAYEE_NAME")) {
            bpayeeName = true;
        }
        if (qName.equalsIgnoreCase("TRAN_TYP")) {
            btranType = true;
        }
        if (qName.equalsIgnoreCase("ENTRY_MODE")) {
            bentryMode = true;
        }
        if (qName.equalsIgnoreCase("POS_ENTRY_MODE")) {
            bposEntryMode = true;
        }
        if (qName.equalsIgnoreCase("COUNTRY_CODE")) {
            bcountryCode = true;
        }
        if (qName.equalsIgnoreCase("CUST_CARD_ID")) {
            bcustCardId = true;
        }
        if (qName.equalsIgnoreCase("MCC_CODE")) {
            bMccCode = true;
        }
        if (qName.equalsIgnoreCase("BRANCH_ID")) {
            bbranchId = true;
        }
        if (qName.equalsIgnoreCase("ACCT_OWNERSHIP")) {
            bacctOwnership = true;
        }
        if (qName.equalsIgnoreCase("ACCT_OPEN_DATE")) {
            bacctOpenDate = true;
        }
        if (qName.equalsIgnoreCase("PART_TRAN_TYPE")) {
            bpartTranType = true;
        }
        if (qName.equalsIgnoreCase("CUST_MOB_NO")) {
            bcustMobNo = true;
        }
        if (qName.equalsIgnoreCase("PRODUCT_CODE")) {
            bproductCode = true;
        }
        if (qName.equalsIgnoreCase("ATM_DOM_LIMIT")) {
            bAtmDomLimit = true;
        }
        if (qName.equalsIgnoreCase("ATM_INTR_LIMIT")) {
            bAtmDIntrLimit = true;
        }
        if (qName.equalsIgnoreCase("POS_ECOM_DOM_LMT")) {
            bposEcomDomLimit = true;
        }
        if (qName.equalsIgnoreCase("POS_ECOM_INTR_LMT")) {
            bposEcomIntrLimit = true;
        }
        if (qName.equalsIgnoreCase("MASK_CARD_NO")) {
            bmaskCardNo = true;
        }
        if (qName.equalsIgnoreCase("TRAN_PARTICULAR")) {
            btranParticular = true;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (bstrtOfTxt) {
            saxPojo.setStrtOfTxt(new String(ch, start, length));
            bstrtOfTxt = false;
        }
        if (bprodId) {
            saxPojo.setProdId(new String(ch, start, length));
            bprodId = false;
        }
        if (brelNum) {
            saxPojo.setRelNum(new String(ch, start, length));
            brelNum = false;
        }
        if (bstat) {
            saxPojo.setStat(new String(ch, start, length));
            bstat = false;
        }
        if (boriginator) {
            saxPojo.setOriginator(new String(ch, start, length));
            boriginator = false;
        }
        if (bresponder) {
            saxPojo.setResponder(new String(ch, start, length));
            bresponder = false;
        }
        if (btyp) {
            saxPojo.setTyp(new String(ch, start, length));
            btyp = false;
        }
        if (bdat) {
            saxPojo.setDat(new String(ch, start, length));
            bdat = false;
        }
        if (btranCde) {
            saxPojo.setTranCde(new String(ch, start, length));
            btranCde = false;
        }
        if (bfromAcctTyp) {
            saxPojo.setFromAcctTyp(new String(ch, start, length));
            bfromAcctTyp = false;
        }
        if (btoAcctTyp) {
            saxPojo.setToAcctTyp(new String(ch, start, length));
            btoAcctTyp = false;
        }
        if (btranAmt) {
            saxPojo.setTranAmt(new String(ch, start, length));
            btranAmt = false;
        }
        if (bxmitDatTim) {
            saxPojo.setXmitDatTim(new String(ch, start, length));
            bxmitDatTim = false;
        }
        if (btraceNum) {
            saxPojo.setTraceNum(new String(ch, start, length));
            btraceNum = false;
        }
        if (btranTim) {
            saxPojo.setTranTim(new String(ch, start, length));
            btranTim = false;
        }
        if (btranDat) {
            saxPojo.setTranDat(new String(ch, start, length));
            btranDat = false;
        }
        if (bexpDat) {
            saxPojo.setExpDat(new String(ch, start, length));
            bexpDat = false;
        }
        if (bcapDat) {
            saxPojo.setCapDat(new String(ch, start, length));
            bcapDat = false;
        }
        if (bpTTranSpclCde) {
            saxPojo.setpTTranSpclCde(new String(ch, start, length));
            bpTTranSpclCde = false;
        }
        if (bnum) {
            saxPojo.setNum(new String(ch, start, length));
            bnum = false;
        }
        if (bseqNum) {
            saxPojo.setSeqNum(new String(ch, start, length));
            bseqNum = false;
        }
        if (bauthId) {
            saxPojo.setAuthId(new String(ch, start, length));
            bauthId = false;
        }
        if (brespCde) {
            saxPojo.setRespCde(new String(ch, start, length));
            brespCde = false;
        }
        if (btermId) {
            saxPojo.setTermId(new String(ch, start, length));
            btermId = false;
        }
        if (bcrdAccptIdCde) {
            saxPojo.setCrdAccptIdCde(new String(ch, start, length));
            bcrdAccptIdCde = false;
        }
        if (btermOwner) {
            saxPojo.setTermOwner(new String(ch, start, length));
            btermOwner = false;
        }
        if (btermCity) {
            saxPojo.setTermCity(new String(ch, start, length));
            btermCity = false;
        }
        if (btermSt) {
            saxPojo.setTermSt(new String(ch, start, length));
            btermSt = false;
        }
        if (btermCntry) {
            saxPojo.setTermCntry(new String(ch, start, length));
            btermCntry = false;
        }
        if (beventType) {
            saxPojo.setEventType(new String(ch, start, length));
            beventType = false;
        }
        if (beventSubType) {
            saxPojo.setEventSubType(new String(ch, start, length));
            beventSubType = false;
        }
        if (beventName) {
            saxPojo.setEventName(new String(ch, start, length));
            beventName = false;
        }
        if (beventId) {
            saxPojo.setEventId(new String(ch, start, length));
            beventId = false;
        }
        if (bdateTime) {
            saxPojo.setDateTime(new String(ch, start, length));
            bdateTime = false;
        }
        if (bpanData) {
            saxPojo.setPanData(new String(ch, start, length));
            bpanData = false;
        }
        if (bsourceId) {
            saxPojo.setSourceId(new String(ch, start, length));
            bsourceId = false;
        }
        if (bhostId) {
            saxPojo.setHostId(new String(ch, start, length));
            bhostId = false;
        }
        if (bsysTime) {
            saxPojo.setSysTime(new String(ch, start, length));
            bsysTime = false;
        }
        if (bbin) {
            saxPojo.setBin(new String(ch, start, length));
            bbin = false;
        }
        if (bfiid) {
            saxPojo.setFiid(new String(ch, start, length));
            bfiid = false;
        }
        if (bchannel) {
            saxPojo.setChannel(new String(ch, start, length));
            bchannel = false;
        }
        if (btxnSecuredFlag) {
            saxPojo.setTxnSecuredFlag(new String(ch, start, length));
            btxnSecuredFlag = false;
        }
        if (buserId) {
            saxPojo.setUserId(new String(ch, start, length));
            buserId = false;
        }
        if (buserType) {
            saxPojo.setUserType(new String(ch, start, length));
            buserType = false;
        }
        if (bcustId) {
            saxPojo.setCustId(new String(ch, start, length));
            bcustId = false;
        }
        if (bdeviceId) {
            saxPojo.setDeviceId(new String(ch, start, length));
            bdeviceId = false;
        }
        if (bterminalId) {
            saxPojo.setTerminalId(new String(ch, start, length));
            bterminalId = false;
        }
        if (bmerchantId) {
            saxPojo.setMerchantId(new String(ch, start, length));
            bmerchantId = false;
        }
        if (bdevOwnerId) {
            saxPojo.setDevOwnerId(new String(ch, start, length));
            bdevOwnerId = false;
        }
        if (btranDate) {
            saxPojo.setTranDate(new String(ch, start, length));
            btranDate = false;
        }
        if (bipAddress) {
            saxPojo.setIpAddress(new String(ch, start, length));
            bipAddress = false;
        }
        if (bipCountry) {
            saxPojo.setIpCountry(new String(ch, start, length));
            bipCountry = false;
        }
        if (bipCity) {
            saxPojo.setIpCity(new String(ch, start, length));
            bipCity = false;
        }
        if (bstateCode) {
            saxPojo.setStateCode(new String(ch, start, length));
            bstateCode = false;
        }
        if (bchipPinFlag) {
            saxPojo.setChipPinFlag(new String(ch, start, length));
            bchipPinFlag = false;
        }
        if (bcustName) {
            saxPojo.setCustName(new String(ch, start, length));
            bcustName = false;
        }
        if (bsuccFailFlg) {
            saxPojo.setSuccFailFlg(new String(ch, start, length));
            bsuccFailFlg = false;
        }
        if (berrorCode) {
            saxPojo.setErrorCode(new String(ch, start, length));
            berrorCode = false;
        }
        if (berrorDesc) {
            saxPojo.setErrorDesc(new String(ch, start, length));
            berrorDesc = false;
        }
        if (bruleId) {
            saxPojo.setRuleId(new String(ch, start, length));
            bruleId = false;
        }
        if (bdrAccountId) {
            saxPojo.setDrAccountId(new String(ch, start, length));
            bdrAccountId = false;
        }
        if (btxnAmt) {
            saxPojo.setTxnAmt(new String(ch, start, length));
            btxnAmt = false;
        }
        if (bavlBal) {
            saxPojo.setAvlBal(new String(ch, start, length));
            bavlBal = false;
        }
        if (bcrAccountId) {
            saxPojo.setCrAccountId(new String(ch, start, length));
            bcrAccountId = false;
        }
        if (bcrIFSCCode) {
            saxPojo.setCrIFSCCode(new String(ch, start, length));
            bcrIFSCCode = false;
        }
        if (bpayeeId) {
            saxPojo.setPayeeId(new String(ch, start, length));
            bpayeeId = false;
        }
        if (bpayeeName) {
            saxPojo.setPayeeName(new String(ch, start, length));
            bpayeeName = false;
        }
        if (btranType) {
            saxPojo.setTranType(new String(ch, start, length));
            btranType = false;
        }
        if (bentryMode) {
            saxPojo.setEntryMode(new String(ch, start, length));
            bentryMode = false;
        }
        if (bposEntryMode) {
            saxPojo.setPosEntryMode(new String(ch, start, length));
            bposEntryMode = false;
        }
        if (bcountryCode) {
            saxPojo.setCountryCode(new String(ch, start, length));
            bcountryCode = false;
        }
        if (bcustCardId) {
            saxPojo.setCustCardId(new String(ch, start, length));
            bcustCardId = false;
        }
        if (bMccCode) {
            saxPojo.setMccCode(new String(ch, start, length));
            bMccCode = false;
        }
        if (bbranchId) {
            saxPojo.setBranchId(new String(ch, start, length));
            bbranchId = false;
        }
        if (bacctOwnership) {
            saxPojo.setAcctOwnership(new String(ch, start, length));
            bacctOwnership = false;
        }
        if (bacctOpenDate) {
            saxPojo.setAcctOpenDate(new String(ch, start, length));
            bacctOpenDate = false;
        }
        if (bpartTranType) {
            saxPojo.setPartTranType(new String(ch, start, length));
            bpartTranType = false;
        }
        if (bcustMobNo) {
            saxPojo.setCustMobNo(new String(ch, start, length));
            bcustMobNo = false;
        }
        if (bproductCode) {
            saxPojo.setProductCode(new String(ch, start, length));
            bproductCode = false;
        }
        if (bAtmDomLimit) {
            saxPojo.setAtmDomLimit(new String(ch, start, length));
            bAtmDomLimit = false;
        }
        if (bAtmDIntrLimit) {
            saxPojo.setAtmDIntrLimit(new String(ch, start, length));
            bAtmDIntrLimit = false;
        }
        if (bposEcomDomLimit) {
            saxPojo.setPosEcomDomLimit(new String(ch, start, length));
            bposEcomDomLimit = false;
        }
        if (bposEcomIntrLimit) {
            saxPojo.setPosEcomIntrLimit(new String(ch, start, length));
            bposEcomIntrLimit = false;
        }
        if (bmaskCardNo) {
            saxPojo.setMaskCardNo(new String(ch, start, length));
            bmaskCardNo = false;
        }
        if (btranParticular) {
            saxPojo.setTranParticular(new String(ch, start, length));
            btranParticular = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);

        if (qName.equalsIgnoreCase("STRT_OF_TXT")) {
            bstrtOfTxt = false;
        }
        if (qName.equalsIgnoreCase("PROD_ID")) {
            bprodId = false;
        }
        if (qName.equalsIgnoreCase("REL_NUM")) {
            brelNum = false;
        }
        if (qName.equalsIgnoreCase("STAT")) {
            bstat = false;
        }
        if (qName.equalsIgnoreCase("ORIGINATOR")) {
            boriginator = false;
        }
        if (qName.equalsIgnoreCase("RESPONDER")) {
            bresponder = false;
        }
        if (qName.equalsIgnoreCase("TYP")) {
            btyp = false;
        }
        if (qName.equalsIgnoreCase("DAT")) {
            bdat = false;
        }
        if (qName.equalsIgnoreCase("TRAN_CDE")) {
            btranCde = false;
        }
        if (qName.equalsIgnoreCase("FROM_ACCT_TYP")) {
            bfromAcctTyp = false;
        }
        if (qName.equalsIgnoreCase("TO_ACCT_TYP")) {
            btoAcctTyp = false;
        }
        if (qName.equalsIgnoreCase("TRAN_AMT")) {
            btranAmt = false;
        }
        if (qName.equalsIgnoreCase("XMIT_DAT_TIM")) {
            bxmitDatTim = false;
        }
        if (qName.equalsIgnoreCase("TRACE_NUM")) {
            btraceNum = false;
        }
        if (qName.equalsIgnoreCase("TRAN_TIM")) {
            btranTim = false;
        }
        if (qName.equalsIgnoreCase("TRAN_DAT")) {
            btranDat = false;
        }
        if (qName.equalsIgnoreCase("EXP_DAT")) {
            bexpDat = false;
        }
        if (qName.equalsIgnoreCase("PT_TRAN_SPCL_CDE")) {
            bpTTranSpclCde = false;
        }
        if (qName.equalsIgnoreCase("NUM")) {
            bnum = false;
        }
        if (qName.equalsIgnoreCase("SEQ_NUM")) {
            bseqNum = false;
        }
        if (qName.equalsIgnoreCase("AUTH_ID_RESP")) {
            bauthId = false;
        }
        if (qName.equalsIgnoreCase("RESP_CDE")) {
            brespCde = false;
        }
        if (qName.equalsIgnoreCase("TERM_ID")) {
            btermId = false;
        }
        if (qName.equalsIgnoreCase("CRD_ACCPT_ID_CDE")) {
            bcrdAccptIdCde = false;
        }
        if (qName.equalsIgnoreCase("TERM_OWNER")) {
            btermOwner = false;
        }
        if (qName.equalsIgnoreCase("TERM_CITY")) {
            btermCity = false;
        }
        if (qName.equalsIgnoreCase("TERM_ST")) {
            btermSt = false;
        }
        if (qName.equalsIgnoreCase("TERM_CNTRY")) {
            btermCntry = false;
        }
        if (qName.equalsIgnoreCase("EVENTTYPE")) {
            beventType = false;
        }
        if (qName.equalsIgnoreCase("EVENTSUBTYPE")) {
            beventSubType = false;
        }
        if (qName.equalsIgnoreCase("EVENT_NAME")) {
            beventName = false;
        }
        if (qName.equalsIgnoreCase("EVENT_ID")) {
            beventId = false;
        }
        if (qName.equalsIgnoreCase("DATE_TIME")) {
            bdateTime = false;
        }
        if (qName.equalsIgnoreCase("PAN_DATA")) {
            bpanData = false;
        }
        if (qName.equalsIgnoreCase("SOURCE_ID")) {
            bsourceId = false;
        }
        if (qName.equalsIgnoreCase("HOST_ID")) {
            bhostId = false;
        }
        if (qName.equalsIgnoreCase("SYS_TIME")) {
            bsysTime = false;
        }
        if (qName.equalsIgnoreCase("BIN")) {
            bbin = false;
        }
        if (qName.equalsIgnoreCase("FIID")) {
            bfiid = false;
        }
        if (qName.equalsIgnoreCase("CHANNEL")) {
            bchannel = false;
        }
        if (qName.equalsIgnoreCase("TXN_SECURED_FLAG")) {
            btxnSecuredFlag = false;
        }
        if (qName.equalsIgnoreCase("USER_ID")) {
            buserId = false;
        }
        if (qName.equalsIgnoreCase("USER_TYPE")) {
            buserType = false;
        }
        if (qName.equalsIgnoreCase("CUST_ID")) {
            bcustId = false;
        }
        if (qName.equalsIgnoreCase("DEVICE_ID")) {
            bdeviceId = false;
        }
        if (qName.equalsIgnoreCase("TERMINAL_ID")) {
            bterminalId = false;
        }
        if (qName.equalsIgnoreCase("MERCHANT_ID")) {
            bmerchantId = false;
        }
        if (qName.equalsIgnoreCase("DEV_OWNER_ID")) {
            bdevOwnerId = false;
        }
        if (qName.equalsIgnoreCase("TRAN_DATE")) {
            btranDate = false;
        }
        if (qName.equalsIgnoreCase("IP_ADDRESS")) {
            bipAddress = false;
        }
        if (qName.equalsIgnoreCase("IP_COUNTRY")) {
            bipCountry = false;
        }
        if (qName.equalsIgnoreCase("IP_CITY")) {
            bipCity = false;
        }
        if (qName.equalsIgnoreCase("STATE_CODE")) {
            bstateCode = false;
        }
        if (qName.equalsIgnoreCase("CHIP_PIN_FLG")) {
            bchipPinFlag = false;
        }
        if (qName.equalsIgnoreCase("CUST_NAME")) {
            bcustName = false;
        }
        if (qName.equalsIgnoreCase("SUCC_FAIL_FLG")) {
            bsuccFailFlg = false;
        }
        if (qName.equalsIgnoreCase("ERROR_CODE")) {
            berrorCode = false;
        }
        if (qName.equalsIgnoreCase("ERROR_DESC")) {
            berrorDesc = false;
        }
        if (qName.equalsIgnoreCase("RULE_ID")) {
            bruleId = false;
        }
        if (qName.equalsIgnoreCase("DR_ACCOUNT_ID")) {
            bdrAccountId = false;
        }
        if (qName.equalsIgnoreCase("TXN_AMT")) {
            btxnAmt = false;
        }
        if (qName.equalsIgnoreCase("AVL_BAL")) {
            bavlBal = false;
        }
        if (qName.equalsIgnoreCase("CR_ACCOUNT_ID")) {
            bcrAccountId = false;
        }
        if (qName.equalsIgnoreCase("CR_IFSC_CODE")) {
            bcrIFSCCode = false;
        }
        if (qName.equalsIgnoreCase("PAYEE_ID")) {
            bpayeeId = false;
        }
        if (qName.equalsIgnoreCase("PAYEE_NAME")) {
            bpayeeName = false;
        }
        if (qName.equalsIgnoreCase("TRAN_TYP")) {
            btranType = false;
        }
        if (qName.equalsIgnoreCase("ENTRY_MODE")) {
            bentryMode = false;
        }
        if (qName.equalsIgnoreCase("POS_ENTRY_MODE")) {
            bposEntryMode = false;
        }
        if (qName.equalsIgnoreCase("COUNTRY_CODE")) {
            bcountryCode = false;
        }
        if (qName.equalsIgnoreCase("CUST_CARD_ID")) {
            bcustCardId = false;
        }
        if (qName.equalsIgnoreCase("MCC_CODE")) {
            bMccCode = false;
        }
        if (qName.equalsIgnoreCase("BRANCH_ID")) {
            bbranchId = false;
        }
        if (qName.equalsIgnoreCase("ACCT_OWNERSHIP")) {
            bacctOwnership = false;
        }
        if (qName.equalsIgnoreCase("ACCT_OPEN_DATE")) {
            bacctOpenDate = false;
        }
        if (qName.equalsIgnoreCase("PART_TRAN_TYPE")) {
            bpartTranType = false;
        }
        if (qName.equalsIgnoreCase("CUST_MOB_NO")) {
            bcustMobNo = false;
        }
        if (qName.equalsIgnoreCase("PRODUCT_CODE")) {
            bproductCode = false;
        }
        if (qName.equalsIgnoreCase("ATM_DOM_LIMIT")) {
            bAtmDomLimit = false;
        }
        if (qName.equalsIgnoreCase("ATM_INTR_LIMIT")) {
            bAtmDIntrLimit = false;
        }
        if (qName.equalsIgnoreCase("POS_ECOM_DOM_LMT")) {
            bposEcomDomLimit = false;
        }
        if (qName.equalsIgnoreCase("POS_ECOM_INTR_LMT")) {
            bposEcomIntrLimit = false;
        }
        if (qName.equalsIgnoreCase("MASK_CARD_NO")) {
            bmaskCardNo = false;
        }
        if (qName.equalsIgnoreCase("TRAN_PARTICULAR")) {
            btranParticular = false;
        }
    }

    /*
    public static void main(String[] args) {
        try {
//            String respXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sch=\"http://tempuri.org/FRMWS/schemas\"><soapenv:Header/><soapenv:Body><sch:FRMREQ><sch:STRT_OF_TXT>Canara</sch:STRT_OF_TXT><sch:PROD_ID>1234</sch:PROD_ID><sch:REL_NUM>001100</sch:REL_NUM><sch:STAT>?</sch:STAT><sch:ORIGINATOR>?</sch:ORIGINATOR><sch:RESPONDER>?</sch:RESPONDER><sch:TYP>?</sch:TYP><sch:EVENTTYPE>?</sch:EVENTTYPE><sch:EVENTSUBTYPE>?</sch:EVENTSUBTYPE><sch:EVENT_NAME>?</sch:EVENT_NAME><sch:EVENT_ID><sch:DATE_TIME>?</sch:DATE_TIME><sch:PAN_DATA>?</sch:PAN_DATA></sch:EVENT_ID><sch:SOURCE_ID>?</sch:SOURCE_ID><sch:HOSTID>?</sch:HOSTID><sch:SYSTIME>?</sch:SYSTIME><sch:PRI_BIT_MAP>?</sch:PRI_BIT_MAP><sch:SECNDRY_BIT_MAP>?</sch:SECNDRY_BIT_MAP><sch:PAN><sch:LEN>?</sch:LEN><sch:DAT>?</sch:DAT></sch:PAN><sch:PROC_CDE><sch:TRAN_CDE>?</sch:TRAN_CDE><sch:FROM_ACCT_TYP>?</sch:FROM_ACCT_TYP><sch:TO_ACCT_TYP>?</sch:TO_ACCT_TYP></sch:PROC_CDE><sch:TRAN_AMT>?</sch:TRAN_AMT><sch:SETL_AMT>?</sch:SETL_AMT><sch:BILL_AMT>?</sch:BILL_AMT><sch:XMIT_DAT_TIM>?</sch:XMIT_DAT_TIM><sch:TRACE_NUM>?</sch:TRACE_NUM><sch:TRAN_TIM>?</sch:TRAN_TIM><sch:TRAN_DAT>?</sch:TRAN_DAT><sch:EXP_DAT>?</sch:EXP_DAT><sch:SETL_DAT>?</sch:SETL_DAT><sch:CONV_DAT>?</sch:CONV_DAT><sch:CAP_DAT>?</sch:CAP_DAT><sch:MRCHT_TYP_CDE>?</sch:MRCHT_TYP_CDE><sch:ACQ_INST_CNTRY_CDE>?</sch:ACQ_INST_CNTRY_CDE><sch:FRWD_INST_CNTRYCDE>?</sch:FRWD_INST_CNTRYCDE><sch:ENTRY_MDE>?</sch:ENTRY_MDE><sch:MBR_NUM>?</sch:MBR_NUM><sch:PT_TRAN_SPCL_CDE>?</sch:PT_TRAN_SPCL_CDE><sch:ACQ_INST_ID><sch:LEN>?</sch:LEN><sch:NUM>?</sch:NUM></sch:ACQ_INST_ID><sch:FRWD_INST_ID><sch:LEN>?</sch:LEN><sch:NUM>?</sch:NUM></sch:FRWD_INST_ID><sch:TRACK2><sch:LEN>?</sch:LEN><sch:DAT>?</sch:DAT></sch:TRACK2><sch:RETRVL_REF_NUM><sch:SEQ_NUM>?</sch:SEQ_NUM></sch:RETRVL_REF_NUM><sch:RESP_CDE>?</sch:RESP_CDE><sch:SERVICE_CDE>?</sch:SERVICE_CDE><sch:TERM_ID>?</sch:TERM_ID><sch:CRD_ACCPT_ID_CDE>?</sch:CRD_ACCPT_ID_CDE><sch:CRD_ACCPT_NAME_LOC><sch:TERM_OWNER>?</sch:TERM_OWNER><sch:TERM_CITY>?</sch:TERM_CITY><sch:TERM_ST>?</sch:TERM_ST><sch:TERM_CNTRY>?</sch:TERM_CNTRY></sch:CRD_ACCPT_NAME_LOC><sch:RESP_DATA><sch:LEN>?</sch:LEN><sch:BAL><sch:PRESENCE_IND>?</sch:PRESENCE_IND><sch:LEDG_BAL>?</sch:LEDG_BAL><sch:AVAIL_BAL>?</sch:AVAIL_BAL></sch:BAL></sch:RESP_DATA><sch:ADD_DATA_NATL><sch:LEN>?</sch:LEN><sch:FRM><sch:BIN>?</sch:BIN><sch:FIID>?</sch:FIID><sch:CHANNEL>?</sch:CHANNEL><sch:SECURED_FLAG>?</sch:SECURED_FLAG><sch:USER_ID>?</sch:USER_ID><sch:USER_TYPE>?</sch:USER_TYPE><sch:CUST_ID>?</sch:CUST_ID><sch:IP_ADDRESS>?</sch:IP_ADDRESS><sch:IP_COUNTRY>?</sch:IP_COUNTRY><sch:CUST_NAME>?</sch:CUST_NAME><sch:SUCC_FAIL_FLG>?</sch:SUCC_FAIL_FLG><sch:CR_ACCOUNT_ID>?</sch:CR_ACCOUNT_ID><sch:CR_IFSC_CODE>?</sch:CR_IFSC_CODE><sch:PAYEE_ID>?</sch:PAYEE_ID><sch:PAYEE_NAME>?</sch:PAYEE_NAME><sch:TRAN_TYP>?</sch:TRAN_TYP><sch:BRANCH_ID>?</sch:BRANCH_ID><sch:ACCT_OWNERSHIP>?</sch:ACCT_OWNERSHIP><sch:ACCT_OPEN_DATE>?</sch:ACCT_OPEN_DATE><sch:PART_TRAN_TYPE>?</sch:PART_TRAN_TYPE><sch:CUST_MOB_NO>?</sch:CUST_MOB_NO><sch:PRODUCT_CODE>?</sch:PRODUCT_CODE><sch:ATM_DOM_LIMIT>?</sch:ATM_DOM_LIMIT><sch:ATM_INTR_LIMIT>?</sch:ATM_INTR_LIMIT><sch:ECOM_DOM_LMT>?</sch:ECOM_DOM_LMT><sch:ECOM_INTR_LMT>?</sch:ECOM_INTR_LMT><sch:MASK_CARD_NO>?</sch:MASK_CARD_NO><sch:TRAN_PARTICULAR>?</sch:TRAN_PARTICULAR><sch:RESERVED>?</sch:RESERVED></sch:FRM></sch:ADD_DATA_NATL><sch:ADD_DATA_PRVT><sch:LEN>?</sch:LEN><sch:ATM><sch:SHRG_GRP>?</sch:SHRG_GRP><sch:TERM_TRAN_ALLOWED>?</sch:TERM_TRAN_ALLOWED><sch:TERM_ST>?</sch:TERM_ST><sch:TERM_CNTY>?</sch:TERM_CNTY><sch:TERM_CNTRY>?</sch:TERM_CNTRY><sch:TERM_RTE_GRP>?</sch:TERM_RTE_GRP></sch:ATM></sch:ADD_DATA_PRVT><sch:CRNCY_CDE>?</sch:CRNCY_CDE><sch:SETL_CRNCY>?</sch:SETL_CRNCY><sch:BILL_CRNCY>?</sch:BILL_CRNCY><sch:ADD_AMTS><sch:LEN>?</sch:LEN><sch:B24_DEF><sch:AMT>?</sch:AMT><sch:SETL_AMT>?</sch:SETL_AMT></sch:B24_DEF></sch:ADD_AMTS><sch:PRI_RSRVD1_PRVT><sch:LEN>?</sch:LEN><sch:POS><sch:TERM_FIID>?</sch:TERM_FIID><sch:TERM_LN>?</sch:TERM_LN><sch:TERM_TIME_OFST>?</sch:TERM_TIME_OFST><sch:PSEUDO_TERM_ID>?</sch:PSEUDO_TERM_ID></sch:POS></sch:PRI_RSRVD1_PRVT><sch:PRI_RSRVD2_PRVT><sch:LEN>?</sch:LEN><sch:POS><sch:CRD_FIID>?</sch:CRD_FIID><sch:CRD_LN>?</sch:CRD_LN><sch:CATEGORY>?</sch:CATEGORY><sch:SAVE_ACCT_TYP>?</sch:SAVE_ACCT_TYP><sch:ICHG_RESP>?</sch:ICHG_RESP><sch:RESERVED>?</sch:RESERVED></sch:POS></sch:PRI_RSRVD2_PRVT><sch:PRI_RSRVD3_PRVT><sch:LEN>?</sch:LEN><sch:B24_DEF><sch:POSTAL_CDE>?</sch:POSTAL_CDE></sch:B24_DEF></sch:PRI_RSRVD3_PRVT><sch:ORIG_INFO><sch:TYP>?</sch:TYP><sch:TRACE_NUM>?</sch:TRACE_NUM><sch:XMIT_DAT_TIM>?</sch:XMIT_DAT_TIM><sch:ACQ_INST_ID_NUM>?</sch:ACQ_INST_ID_NUM><sch:FRWD_INST_ID_NUM>?</sch:FRWD_INST_ID_NUM></sch:ORIG_INFO><sch:RCV_INST><sch:LEN>?</sch:LEN><sch:ID_NUM>?</sch:ID_NUM></sch:RCV_INST><sch:ACCT1><sch:LEN>?</sch:LEN><sch:NUM>?</sch:NUM></sch:ACCT1><sch:ACCT2><sch:LEN>?</sch:LEN><sch:NUM>?</sch:NUM></sch:ACCT2><sch:SECNDRY_RSRVD6_PRVT><sch:LEN>?</sch:LEN><sch:DAT1>?</sch:DAT1></sch:SECNDRY_RSRVD6_PRVT><sch:SECNDRY_RSRVD7_PRVT><sch:LEN>?</sch:LEN><sch:DAT1>?</sch:DAT1></sch:SECNDRY_RSRVD7_PRVT></sch:FRMREQ></soapenv:Body></soapenv:Envelope>";
            String respXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\"><SOAP-ENV:Body><FHMRQST><STRT_OF_TXT>ISO</STRT_OF_TXT><PROD_ID>02</PROD_ID><REL_NUM>60</REL_NUM><STAT>000</STAT><ORIGINATOR>7</ORIGINATOR><RESPONDER>5</RESPONDER><TYP>0220</TYP><PAN><DAT>4601340404000064</DAT></PAN><PROC_CDE><TRAN_CDE>00</TRAN_CDE><FROM_ACCT_TYP>10</FROM_ACCT_TYP><TO_ACCT_TYP>00</TO_ACCT_TYP></PROC_CDE><TRAN_AMT>1200</TRAN_AMT><XMIT_DAT_TIM>416065632</XMIT_DAT_TIM><TRACE_NUM>7</TRACE_NUM><TRAN_TIM>193849</TRAN_TIM><TRAN_DAT>415</TRAN_DAT><EXP_DAT>2101</EXP_DAT><CAP_DAT>415</CAP_DAT><PT_TRAN_SPCL_CDE>0</PT_TRAN_SPCL_CDE><ACQ_INST_ID><NUM>12345678901</NUM></ACQ_INST_ID><SEQ_NUM>910514000547</SEQ_NUM><AUTH_ID_RESP>931244</AUTH_ID_RESP><RESP_CDE>00</RESP_CDE><TERM_ID>TERMID01</TERM_ID><CRD_ACCPT_ID_CDE>CARD ACCEPTOR</CRD_ACCPT_ID_CDE><CRD_ACCPT_NAME_LOC><TERM_OWNER>ACQUIRER NAME</TERM_OWNER><TERM_CITY>CITY NAME</TERM_CITY><TERM_ST/><TERM_CNTRY>US</TERM_CNTRY></CRD_ACCPT_NAME_LOC><FRM_DATA><EVENTTYPE>ft</EVENTTYPE><EVENTSUBTYPE>dccardtxn</EVENTSUBTYPE><EVENT_NAME>ft_dccardtxn</EVENT_NAME><EVENT_ID></EVENT_ID><DATE_TIME>20190415194258250769</DATE_TIME><PAN_DATA>4601340404000064</PAN_DATA><SOURCE_ID>B</SOURCE_ID><HOST_ID>F</HOST_ID><SYS_TIME>415141258</SYS_TIME><BIN>460134</BIN><FIID/><CHANNEL>POS</CHANNEL><TXN_SECURED_FLAG></TXN_SECURED_FLAG><USER_ID>NA</USER_ID><USER_TYPE>RETAIL</USER_TYPE><CUST_ID>NA</CUST_ID><DEVICE_ID>TERMID01</DEVICE_ID><TERMINAL_ID>TERMID01</TERMINAL_ID><MERCHANT_ID>ACQUIRER NAME</MERCHANT_ID><DEV_OWNER_ID>CARD ACCEPTOR</DEV_OWNER_ID><TRAN_DATE>0415193849</TRAN_DATE><IP_ADDRESS>NA</IP_ADDRESS><IP_COUNTRY>840</IP_COUNTRY><IP_CITY>CITY NAME</IP_CITY><STATE_CODE/><CHIP_PIN_FLG>N</CHIP_PIN_FLG><CUST_NAME>NA</CUST_NAME><SUCC_FAIL_FLG>S</SUCC_FAIL_FLG><ERROR_CODE>00</ERROR_CODE><ERROR_DESC/><RULE_ID/><DR_ACCOUNT_ID>0219111021752</DR_ACCOUNT_ID><TXN_AMT>000000001200</TXN_AMT><AVL_BAL>000000000000</AVL_BAL><CR_ACCOUNT_ID>NANANANANA052</CR_ACCOUNT_ID><CR_IFSC_CODE>05</CR_IFSC_CODE><PAYEE_ID>2U</PAYEE_ID><PAYEE_NAME>SN</PAYEE_NAME><TRAN_TYP>A</TRAN_TYP><ENTRY_MODE/><POS_ENTRY_MODE/><COUNTRY_CODE/><CUST_CARD_ID> 5999NACARDN</CUST_CARD_ID><MCC_CODE>ADNA</MCC_CODE><BRANCH_ID>NA</BRANCH_ID><ACCT_OWNERSHIP>NANA</ACCT_OWNERSHIP><ACCT_OPEN_DATE>NA</ACCT_OPEN_DATE><PART_TRAN_TYPE>N</PART_TRAN_TYPE><CUST_MOB_NO>AN</CUST_MOB_NO><PRODUCT_CODE>A</PRODUCT_CODE><ATM_DOM_LIMIT></ATM_DOM_LIMIT></FRM_DATA></FHMRQST></SOAP-ENV:Body></SOAP-ENV:Envelope>";

            new SaxParser().reqParser(removeXmlStringNamespaceAndPreamble(respXml));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
        return xmlString.replaceAll("(<\\?[^<]*\\?>)?", "").
                replaceAll("xmlns.*?(\"|\').*?(\"|\')", "")
                .replaceAll("(<)(\\w+:)(.*?>)", "$1$3")
                .replaceAll("(</)(\\w+:)(.*?>)", "$1$3");

    }
     */
}
