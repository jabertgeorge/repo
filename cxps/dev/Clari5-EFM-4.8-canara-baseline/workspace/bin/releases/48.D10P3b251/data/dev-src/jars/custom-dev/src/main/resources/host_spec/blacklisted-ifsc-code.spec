# Generated Code
cxps.noesis.glossary.entity.Blacklisted_ifsc_code {
    db-name = BLACKLISTED_IFSC_CODE
    generate = false
    db_column_quoted = true
    
    tablespace = CXPS_USERS
    attributes = [ 
        { name = ifsc_code, column = ifsc-code, type = "string:10", key=true }
        
        ]
}
