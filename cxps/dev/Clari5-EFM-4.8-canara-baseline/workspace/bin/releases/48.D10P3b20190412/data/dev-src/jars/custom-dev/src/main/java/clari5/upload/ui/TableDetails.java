package clari5.upload.ui;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import clari5.rdbms.Rdbms;

public class TableDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	 
	 
	/**
	* @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	*/
	private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private ResultSetMetaData rsMetaData=null;
    private int numberOfColumnsDB = 0;
	private static CxpsLogger logger = CxpsLogger.getLogger(TableDetails.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request,response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    String tableName;
    HttpSession session;
		tableName = request.getParameter("tableName");
		 session = request.getSession();
		 session.setAttribute("tableName",tableName);
		try {
			con = Rdbms.getAppConnection();
	        ps= con.prepareStatement("select * from "+tableName);
	        logger.info("Table Selected :["+tableName+"]");
            rs = ps.executeQuery();
	        rsMetaData = rs.getMetaData();
	        numberOfColumnsDB = rsMetaData.getColumnCount();
	        JSONArray arr = new JSONArray();
	        
	        for (int i = 1; i <= numberOfColumnsDB; i++) {	        
	        	JSONObject object = new JSONObject();
	        	object.append("coulmnName", rsMetaData.getColumnName(i));
				object.append("columnType", rsMetaData.getColumnTypeName(i));
	        	object.append("columnSize", rsMetaData.getColumnDisplaySize(i));
	        	arr.put(object);
	        }
	        response.setContentType("text/html");    
	        response.getWriter().println(arr.toString());
	        }
	        catch (SQLException | JSONException e) {
		    logger.info("Exception while Getting Table Details ["+e.getMessage()+"] and the Cause is ["+e.getCause()+"]");
	       }
	       finally {
            try { if (con != null) con.close();}catch (SQLException ex){ ex.printStackTrace();}
            try { if (ps != null) ps.close();}catch (SQLException ex){ ex.printStackTrace();}
            try { if (rs != null) rs.close();}catch (SQLException ex){ ex.printStackTrace();}
        }
		}
	    }
