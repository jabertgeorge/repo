clari5.hfdb.entity.nft_limchange { 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = ID, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = user_id ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = old_limit ,type = "string:20"}
{name = new_limit ,type = "string:20"}
{name = card_number ,type = "string:50"}
{name = card_type ,type = "string:20"}
{name = tran_date ,type = timestamp}
{name = lim_change_date ,type = timestamp}

    ]
}
