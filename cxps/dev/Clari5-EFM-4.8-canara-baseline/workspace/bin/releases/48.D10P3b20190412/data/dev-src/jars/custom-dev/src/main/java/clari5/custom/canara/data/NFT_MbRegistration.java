package clari5.custom.canara.data;

public class NFT_MbRegistration extends ITableData {

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "NFT_MBREGISTRATION";
    private String event_type = "NFT_MbRegistration";

    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String USER_ID ;
    private String CUST_ID ;
    private String APP_ID ;
    private String DEVICE_ID ;
    private String ERROR_CODE ;
    private String COUNTRY_CODE ;
    private String SUCC_FAIL_FLG ;
    private String MOBILE_NO ;
    private String IP_ADDRESS ;
    private String ERROR_DESC ;
    private String TRAN_DATE ;
    private String PWD_RESET_TYPE ;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getAPP_ID() {
        return APP_ID;
    }

    public void setAPP_ID(String APP_ID) {
        this.APP_ID = APP_ID;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public String getERROR_CODE() {
        return ERROR_CODE;
    }

    public void setERROR_CODE(String ERROR_CODE) {
        this.ERROR_CODE = ERROR_CODE;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getSUCC_FAIL_FLG() {
        return SUCC_FAIL_FLG;
    }

    public void setSUCC_FAIL_FLG(String SUCC_FAIL_FLG) {
        this.SUCC_FAIL_FLG = SUCC_FAIL_FLG;
    }

    public String getMOBILE_NO() {
        return MOBILE_NO;
    }

    public void setMOBILE_NO(String MOBILE_NO) {
        this.MOBILE_NO = MOBILE_NO;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
    }

    public String getERROR_DESC() {
        return ERROR_DESC;
    }

    public void setERROR_DESC(String ERROR_DESC) {
        this.ERROR_DESC = ERROR_DESC;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getPWD_RESET_TYPE() {
        return PWD_RESET_TYPE;
    }

    public void setPWD_RESET_TYPE(String PWD_RESET_TYPE) {
        this.PWD_RESET_TYPE = PWD_RESET_TYPE;
    }


}

