// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_FundsTransferEventMapper extends EventMapper<FT_FundsTransferEvent> {

public FT_FundsTransferEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_FundsTransferEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_FundsTransferEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_FundsTransferEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_FundsTransferEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_FundsTransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_FundsTransferEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setDouble(i++, obj.getEffAvlBal());
            preparedStatement.setString(i++, obj.getBenificaryId());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getMobileNo());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getMerchantId());
            preparedStatement.setString(i++, obj.getTranMode());
            preparedStatement.setString(i++, obj.getUserType());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getPartTranType());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustName());
            preparedStatement.setString(i++, obj.getBenificaryName());
            preparedStatement.setString(i++, obj.getMccCode());
            preparedStatement.setDouble(i++, obj.getDailyLimit());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getAccountOwnership());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setDouble(i++, obj.getAvgTxnAmt());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getCrIfscCode());
            preparedStatement.setString(i++, obj.getTxnType());
            preparedStatement.setString(i++, obj.getDrAccountId());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setString(i++, obj.getNetBankingType());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getCrAccountId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_FundsTransferEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_FundsTransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FUNDSTRANSFER"));
        putList = new ArrayList<>();

        for (FT_FundsTransferEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "EFF_AVL_BAL", String.valueOf(obj.getEffAvlBal()));
            p = this.insert(p, "BENIFICARY_ID",  obj.getBenificaryId());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "MOBILE_NO",  obj.getMobileNo());
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "MERCHANT_ID",  obj.getMerchantId());
            p = this.insert(p, "TRAN_MODE",  obj.getTranMode());
            p = this.insert(p, "USER_TYPE",  obj.getUserType());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "PART_TRAN_TYPE",  obj.getPartTranType());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_NAME",  obj.getCustName());
            p = this.insert(p, "BENIFICARY_NAME",  obj.getBenificaryName());
            p = this.insert(p, "MCC_CODE",  obj.getMccCode());
            p = this.insert(p, "DAILY_LIMIT", String.valueOf(obj.getDailyLimit()));
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "ACCOUNT_OWNERSHIP",  obj.getAccountOwnership());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "AVG_TXN_AMT", String.valueOf(obj.getAvgTxnAmt()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "CR_IFSC_CODE",  obj.getCrIfscCode());
            p = this.insert(p, "TXN_TYPE",  obj.getTxnType());
            p = this.insert(p, "DR_ACCOUNT_ID",  obj.getDrAccountId());
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "NET_BANKING_TYPE",  obj.getNetBankingType());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "CR_ACCOUNT_ID",  obj.getCrAccountId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_FUNDSTRANSFER"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_FundsTransferEvent obj = new FT_FundsTransferEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setEffAvlBal(rs.getDouble("EFF_AVL_BAL"));
    obj.setBenificaryId(rs.getString("BENIFICARY_ID"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setMobileNo(rs.getString("MOBILE_NO"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setMerchantId(rs.getString("MERCHANT_ID"));
    obj.setTranMode(rs.getString("TRAN_MODE"));
    obj.setUserType(rs.getString("USER_TYPE"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setPartTranType(rs.getString("PART_TRAN_TYPE"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustName(rs.getString("CUST_NAME"));
    obj.setBenificaryName(rs.getString("BENIFICARY_NAME"));
    obj.setMccCode(rs.getString("MCC_CODE"));
    obj.setDailyLimit(rs.getDouble("DAILY_LIMIT"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setAccountOwnership(rs.getString("ACCOUNT_OWNERSHIP"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setAvgTxnAmt(rs.getDouble("AVG_TXN_AMT"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setCrIfscCode(rs.getString("CR_IFSC_CODE"));
    obj.setTxnType(rs.getString("TXN_TYPE"));
    obj.setDrAccountId(rs.getString("DR_ACCOUNT_ID"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setNetBankingType(rs.getString("NET_BANKING_TYPE"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setCrAccountId(rs.getString("CR_ACCOUNT_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_FundsTransferEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_FundsTransferEvent> events;
 FT_FundsTransferEvent obj = new FT_FundsTransferEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FUNDSTRANSFER"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_FundsTransferEvent obj = new FT_FundsTransferEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setEffAvlBal(EventHelper.toDouble(getColumnValue(rs, "EFF_AVL_BAL")));
            obj.setBenificaryId(getColumnValue(rs, "BENIFICARY_ID"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setMobileNo(getColumnValue(rs, "MOBILE_NO"));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setMerchantId(getColumnValue(rs, "MERCHANT_ID"));
            obj.setTranMode(getColumnValue(rs, "TRAN_MODE"));
            obj.setUserType(getColumnValue(rs, "USER_TYPE"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setPartTranType(getColumnValue(rs, "PART_TRAN_TYPE"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustName(getColumnValue(rs, "CUST_NAME"));
            obj.setBenificaryName(getColumnValue(rs, "BENIFICARY_NAME"));
            obj.setMccCode(getColumnValue(rs, "MCC_CODE"));
            obj.setDailyLimit(EventHelper.toDouble(getColumnValue(rs, "DAILY_LIMIT")));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setAccountOwnership(getColumnValue(rs, "ACCOUNT_OWNERSHIP"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setAvgTxnAmt(EventHelper.toDouble(getColumnValue(rs, "AVG_TXN_AMT")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setCrIfscCode(getColumnValue(rs, "CR_IFSC_CODE"));
            obj.setTxnType(getColumnValue(rs, "TXN_TYPE"));
            obj.setDrAccountId(getColumnValue(rs, "DR_ACCOUNT_ID"));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setNetBankingType(getColumnValue(rs, "NET_BANKING_TYPE"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setCrAccountId(getColumnValue(rs, "CR_ACCOUNT_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"EFF_AVL_BAL\",\"BENIFICARY_ID\",\"CHANNEL\",\"IP_COUNTRY\",\"MOBILE_NO\",\"PAYEE_ID\",\"MERCHANT_ID\",\"TRAN_MODE\",\"USER_TYPE\",\"AVL_BAL\",\"SUCC_FAIL_FLG\",\"PART_TRAN_TYPE\",\"ERROR_DESC\",\"SYS_TIME\",\"CUST_NAME\",\"BENIFICARY_NAME\",\"MCC_CODE\",\"DAILY_LIMIT\",\"TXN_AMT\",\"TRAN_DATE\",\"ACCOUNT_OWNERSHIP\",\"DEVICE_ID\",\"IP_ADDRESS\",\"AVG_TXN_AMT\",\"HOST_ID\",\"COUNTRY_CODE\",\"USER_ID\",\"IP_CITY\",\"CR_IFSC_CODE\",\"TXN_TYPE\",\"DR_ACCOUNT_ID\",\"PAYEE_NAME\",\"NET_BANKING_TYPE\",\"ERROR_CODE\",\"ACCT_OPEN_DATE\",\"CUST_ID\",\"CR_ACCOUNT_ID\"" +
              " FROM EVENT_FT_FUNDSTRANSFER";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [EFF_AVL_BAL],[BENIFICARY_ID],[CHANNEL],[IP_COUNTRY],[MOBILE_NO],[PAYEE_ID],[MERCHANT_ID],[TRAN_MODE],[USER_TYPE],[AVL_BAL],[SUCC_FAIL_FLG],[PART_TRAN_TYPE],[ERROR_DESC],[SYS_TIME],[CUST_NAME],[BENIFICARY_NAME],[MCC_CODE],[DAILY_LIMIT],[TXN_AMT],[TRAN_DATE],[ACCOUNT_OWNERSHIP],[DEVICE_ID],[IP_ADDRESS],[AVG_TXN_AMT],[HOST_ID],[COUNTRY_CODE],[USER_ID],[IP_CITY],[CR_IFSC_CODE],[TXN_TYPE],[DR_ACCOUNT_ID],[PAYEE_NAME],[NET_BANKING_TYPE],[ERROR_CODE],[ACCT_OPEN_DATE],[CUST_ID],[CR_ACCOUNT_ID]" +
              " FROM EVENT_FT_FUNDSTRANSFER";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`EFF_AVL_BAL`,`BENIFICARY_ID`,`CHANNEL`,`IP_COUNTRY`,`MOBILE_NO`,`PAYEE_ID`,`MERCHANT_ID`,`TRAN_MODE`,`USER_TYPE`,`AVL_BAL`,`SUCC_FAIL_FLG`,`PART_TRAN_TYPE`,`ERROR_DESC`,`SYS_TIME`,`CUST_NAME`,`BENIFICARY_NAME`,`MCC_CODE`,`DAILY_LIMIT`,`TXN_AMT`,`TRAN_DATE`,`ACCOUNT_OWNERSHIP`,`DEVICE_ID`,`IP_ADDRESS`,`AVG_TXN_AMT`,`HOST_ID`,`COUNTRY_CODE`,`USER_ID`,`IP_CITY`,`CR_IFSC_CODE`,`TXN_TYPE`,`DR_ACCOUNT_ID`,`PAYEE_NAME`,`NET_BANKING_TYPE`,`ERROR_CODE`,`ACCT_OPEN_DATE`,`CUST_ID`,`CR_ACCOUNT_ID`" +
              " FROM EVENT_FT_FUNDSTRANSFER";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_FUNDSTRANSFER (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"EFF_AVL_BAL\",\"BENIFICARY_ID\",\"CHANNEL\",\"IP_COUNTRY\",\"MOBILE_NO\",\"PAYEE_ID\",\"MERCHANT_ID\",\"TRAN_MODE\",\"USER_TYPE\",\"AVL_BAL\",\"SUCC_FAIL_FLG\",\"PART_TRAN_TYPE\",\"ERROR_DESC\",\"SYS_TIME\",\"CUST_NAME\",\"BENIFICARY_NAME\",\"MCC_CODE\",\"DAILY_LIMIT\",\"TXN_AMT\",\"TRAN_DATE\",\"ACCOUNT_OWNERSHIP\",\"DEVICE_ID\",\"IP_ADDRESS\",\"AVG_TXN_AMT\",\"HOST_ID\",\"COUNTRY_CODE\",\"USER_ID\",\"IP_CITY\",\"CR_IFSC_CODE\",\"TXN_TYPE\",\"DR_ACCOUNT_ID\",\"PAYEE_NAME\",\"NET_BANKING_TYPE\",\"ERROR_CODE\",\"ACCT_OPEN_DATE\",\"CUST_ID\",\"CR_ACCOUNT_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[EFF_AVL_BAL],[BENIFICARY_ID],[CHANNEL],[IP_COUNTRY],[MOBILE_NO],[PAYEE_ID],[MERCHANT_ID],[TRAN_MODE],[USER_TYPE],[AVL_BAL],[SUCC_FAIL_FLG],[PART_TRAN_TYPE],[ERROR_DESC],[SYS_TIME],[CUST_NAME],[BENIFICARY_NAME],[MCC_CODE],[DAILY_LIMIT],[TXN_AMT],[TRAN_DATE],[ACCOUNT_OWNERSHIP],[DEVICE_ID],[IP_ADDRESS],[AVG_TXN_AMT],[HOST_ID],[COUNTRY_CODE],[USER_ID],[IP_CITY],[CR_IFSC_CODE],[TXN_TYPE],[DR_ACCOUNT_ID],[PAYEE_NAME],[NET_BANKING_TYPE],[ERROR_CODE],[ACCT_OPEN_DATE],[CUST_ID],[CR_ACCOUNT_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`EFF_AVL_BAL`,`BENIFICARY_ID`,`CHANNEL`,`IP_COUNTRY`,`MOBILE_NO`,`PAYEE_ID`,`MERCHANT_ID`,`TRAN_MODE`,`USER_TYPE`,`AVL_BAL`,`SUCC_FAIL_FLG`,`PART_TRAN_TYPE`,`ERROR_DESC`,`SYS_TIME`,`CUST_NAME`,`BENIFICARY_NAME`,`MCC_CODE`,`DAILY_LIMIT`,`TXN_AMT`,`TRAN_DATE`,`ACCOUNT_OWNERSHIP`,`DEVICE_ID`,`IP_ADDRESS`,`AVG_TXN_AMT`,`HOST_ID`,`COUNTRY_CODE`,`USER_ID`,`IP_CITY`,`CR_IFSC_CODE`,`TXN_TYPE`,`DR_ACCOUNT_ID`,`PAYEE_NAME`,`NET_BANKING_TYPE`,`ERROR_CODE`,`ACCT_OPEN_DATE`,`CUST_ID`,`CR_ACCOUNT_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

