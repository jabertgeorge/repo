/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clari5.custom.dev;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPPart;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author lakshmisai
 */
public class SoapServlet extends HttpServlet {

    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void soapRequest(HttpServletRequest request, HttpServletResponse response) {

        String strMsg = "";

        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            InputStream inStream = request.getInputStream();
            SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), inStream);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            strMsg = new String(out.toByteArray());

	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            System.out.println("Before Parsing=>" + sdf.format(new Date()));
            
	    JSONObject jsonObject = new JSONObject();
            jsonObject = new FormatJson().getJsonObject(new SaxParser().reqParser(removeXmlStringNamespaceAndPreamble(strMsg)));

	    System.out.println("After Parsing=>" + sdf.format(new Date()));
            SendEvent sendEvent = new SendEvent();
System.out.println("Before Calling Clarif5=>" + sdf.format(new Date()));
            String decideresponse = sendEvent.sendDecideEvent(jsonObject);
System.out.println("Before Calling Clarif5=>" + sdf.format(new Date()));

            System.out.println(" response =>" +decideresponse);

            strMsg = strMsg.replaceAll("FRMRQST","FRMRESP");

            if(decideresponse.equalsIgnoreCase("allow"))
            {
                if(strMsg.contains("<TYP>0200</TYP>")) {
                    strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                    if(strMsg.contains("<RESP_CDE>"))
                    {
                        int j = strMsg.indexOf("<RESP_CDE>");
                        String substring = strMsg.substring(j, j + 23);
                        strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                    }
                    else
                    {
                        strMsg =  strMsg.replace("<FRMRESP>","<FRMRESP><RESP_CDE>00</RESP_CDE>");
                    }
                }
                else{
                    System.out.println("Invalid xml => <TYP> not present in xml");
                }
                System.out.println("response => final xml for allow "+strMsg);

                response.setStatus(200);
                response.setContentType("text/xml");
                PrintWriter writer = response.getWriter();
                writer.append(strMsg);
            }
           else if(decideresponse.equalsIgnoreCase("decline"))
            {
                if(strMsg.contains("<TYP>0200</TYP>")) {

                    strMsg =  strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                    if(strMsg.contains("<RESP_CDE>")) {
                        int j = strMsg.indexOf("<RESP_CDE>");
                        String substring = strMsg.substring(j, j + 23);
                        strMsg = strMsg.replaceAll(substring, "<RESP_CDE>05</RESP_CDE>");
                    }
                    else {
                        strMsg =  strMsg.replace("<FRMRESP>","<FRMRESP><RESP_CDE>05</RESP_CDE>");
                    }
                }
                else{
                    System.out.println("Invalid xml => <TYP> not present in xml");
                }
                System.out.println("response => final xml for decline "+strMsg);
                response.setStatus(200);
                response.setContentType("text/xml");
                PrintWriter writer = response.getWriter();
                writer.append(strMsg);
            }
            else {

                System.out.println("INVALID RESPONSE ON DECIDE CALL");
                response.setStatus(200);
                response.setContentType("text/xml");
                PrintWriter writer = response.getWriter();
                writer.append(strMsg);
            }
	    System.out.println("End of the request=>" + sdf.format(new Date()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            // Retrieve different parts
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
            // Two ways to extract headers
            SOAPHeader soapHeader = soapEnvelope.getHeader();
            soapHeader = soapMessage.getSOAPHeader();
            // Two ways to extract body
            SOAPBody soapBody = soapEnvelope.getBody();
            soapBody = soapMessage.getSOAPBody();
            // To add some element
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            Name bodyName = soapFactory.createName("getEmployeeDetails", "ns1", "urn:MySoapServices");
            SOAPBodyElement purchaseLineItems = soapBody.addBodyElement(bodyName);
            Name childName = soapFactory.createName("param1");
            SOAPElement order = purchaseLineItems.addChildElement(childName);
            order.addTextNode("1016577");

        } catch (Exception e) {
            e.printStackTrace();
        }
         */
    }

    public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
        return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
                replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
                .replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
                .replaceAll("(</)(\\w+:)(.*?>)", "$1$3");
       /* remove closing tags prefix */
    }
}
