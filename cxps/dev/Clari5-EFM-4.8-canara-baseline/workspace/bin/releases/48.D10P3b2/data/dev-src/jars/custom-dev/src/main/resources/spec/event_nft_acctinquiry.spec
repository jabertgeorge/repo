cxps.events.event.nft_acct_inquiry{
table-name : EVENT_NFT_ACCTINQUIRY
  event-mnemonic: NA
  workspaces : {
    PAYMENTCARD : cust_card_id
    }
  event-attributes : {
	host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        channel: {db : true ,raw_name : channel ,type : "string:20"}
        user_id: {db : true ,raw_name : user_id ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        branch_id: {db : true ,raw_name : branch_id ,type : "string:20"}
        menu_id: {db : true ,raw_name : menu_id ,type : "string:50"}
        acct_name: {db : true ,raw_name : acct_name ,type : "string:50"}
        schm_type: {db : true ,raw_name : schm_type ,type : "string:20"}
        schm_code: {db : true ,raw_name : schm_code ,type : "string:20"}
        acct_status: {db : true ,raw_name : acct_status ,type : "string:20"}
        avl_bal: {db : true ,raw_name : avl_bal ,type :  "number:11,2"}
        account_id: {db : true ,raw_name : account_id ,type : "string:20"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust_card_id: {db : true ,raw_name : cust_card_id ,type : "string:20"}
	tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
	mask_card_no: {db : true ,raw_name : mask_card_no ,type : "string:20"}
        }
}
