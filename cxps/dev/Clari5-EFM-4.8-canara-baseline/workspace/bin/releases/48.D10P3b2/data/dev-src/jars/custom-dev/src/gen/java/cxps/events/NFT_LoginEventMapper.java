// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_LoginEventMapper extends EventMapper<NFT_LoginEvent> {

public NFT_LoginEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_LoginEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_LoginEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_LoginEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_LoginEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_LoginEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_LoginEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getMobileNo());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setTimestamp(i++, obj.getLastLoginTs());
            preparedStatement.setString(i++, obj.getMacId());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setString(i++, obj.getUserType());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getLastLoginIp());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getCustName());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getAppId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_LOGIN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_LoginEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_LoginEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_LOGIN"));
        putList = new ArrayList<>();

        for (NFT_LoginEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "MOBILE_NO",  obj.getMobileNo());
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "LAST_LOGIN_TS", String.valueOf(obj.getLastLoginTs()));
            p = this.insert(p, "MAC_ID",  obj.getMacId());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "USER_TYPE",  obj.getUserType());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "LAST_LOGIN_IP",  obj.getLastLoginIp());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "CUST_NAME",  obj.getCustName());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "APP_ID",  obj.getAppId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_LOGIN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_LOGIN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_LOGIN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_LoginEvent obj = new NFT_LoginEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setMobileNo(rs.getString("MOBILE_NO"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setLastLoginTs(rs.getTimestamp("LAST_LOGIN_TS"));
    obj.setMacId(rs.getString("MAC_ID"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setUserType(rs.getString("USER_TYPE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setLastLoginIp(rs.getString("LAST_LOGIN_IP"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setCustName(rs.getString("CUST_NAME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setAppId(rs.getString("APP_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_LOGIN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_LoginEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_LoginEvent> events;
 NFT_LoginEvent obj = new NFT_LoginEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_LOGIN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_LoginEvent obj = new NFT_LoginEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setMobileNo(getColumnValue(rs, "MOBILE_NO"));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setLastLoginTs(EventHelper.toTimestamp(getColumnValue(rs, "LAST_LOGIN_TS")));
            obj.setMacId(getColumnValue(rs, "MAC_ID"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setUserType(getColumnValue(rs, "USER_TYPE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setLastLoginIp(getColumnValue(rs, "LAST_LOGIN_IP"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setCustName(getColumnValue(rs, "CUST_NAME"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setAppId(getColumnValue(rs, "APP_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_LOGIN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_LOGIN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"TRAN_DATE\",\"DEVICE_ID\",\"CHANNEL\",\"IP_COUNTRY\",\"MOBILE_NO\",\"IP_ADDRESS\",\"HOST_ID\",\"LAST_LOGIN_TS\",\"MAC_ID\",\"COUNTRY_CODE\",\"USER_TYPE\",\"ACCOUNT_ID\",\"USER_ID\",\"IP_CITY\",\"ERROR_DESC\",\"LAST_LOGIN_IP\",\"SYS_TIME\",\"ERROR_CODE\",\"CUST_NAME\",\"CUST_ID\",\"APP_ID\"" +
              " FROM EVENT_NFT_LOGIN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [TRAN_DATE],[DEVICE_ID],[CHANNEL],[IP_COUNTRY],[MOBILE_NO],[IP_ADDRESS],[HOST_ID],[LAST_LOGIN_TS],[MAC_ID],[COUNTRY_CODE],[USER_TYPE],[ACCOUNT_ID],[USER_ID],[IP_CITY],[ERROR_DESC],[LAST_LOGIN_IP],[SYS_TIME],[ERROR_CODE],[CUST_NAME],[CUST_ID],[APP_ID]" +
              " FROM EVENT_NFT_LOGIN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`TRAN_DATE`,`DEVICE_ID`,`CHANNEL`,`IP_COUNTRY`,`MOBILE_NO`,`IP_ADDRESS`,`HOST_ID`,`LAST_LOGIN_TS`,`MAC_ID`,`COUNTRY_CODE`,`USER_TYPE`,`ACCOUNT_ID`,`USER_ID`,`IP_CITY`,`ERROR_DESC`,`LAST_LOGIN_IP`,`SYS_TIME`,`ERROR_CODE`,`CUST_NAME`,`CUST_ID`,`APP_ID`" +
              " FROM EVENT_NFT_LOGIN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_LOGIN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"TRAN_DATE\",\"DEVICE_ID\",\"CHANNEL\",\"IP_COUNTRY\",\"MOBILE_NO\",\"IP_ADDRESS\",\"HOST_ID\",\"LAST_LOGIN_TS\",\"MAC_ID\",\"COUNTRY_CODE\",\"USER_TYPE\",\"ACCOUNT_ID\",\"USER_ID\",\"IP_CITY\",\"ERROR_DESC\",\"LAST_LOGIN_IP\",\"SYS_TIME\",\"ERROR_CODE\",\"CUST_NAME\",\"CUST_ID\",\"APP_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[TRAN_DATE],[DEVICE_ID],[CHANNEL],[IP_COUNTRY],[MOBILE_NO],[IP_ADDRESS],[HOST_ID],[LAST_LOGIN_TS],[MAC_ID],[COUNTRY_CODE],[USER_TYPE],[ACCOUNT_ID],[USER_ID],[IP_CITY],[ERROR_DESC],[LAST_LOGIN_IP],[SYS_TIME],[ERROR_CODE],[CUST_NAME],[CUST_ID],[APP_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`TRAN_DATE`,`DEVICE_ID`,`CHANNEL`,`IP_COUNTRY`,`MOBILE_NO`,`IP_ADDRESS`,`HOST_ID`,`LAST_LOGIN_TS`,`MAC_ID`,`COUNTRY_CODE`,`USER_TYPE`,`ACCOUNT_ID`,`USER_ID`,`IP_CITY`,`ERROR_DESC`,`LAST_LOGIN_IP`,`SYS_TIME`,`ERROR_CODE`,`CUST_NAME`,`CUST_ID`,`APP_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

