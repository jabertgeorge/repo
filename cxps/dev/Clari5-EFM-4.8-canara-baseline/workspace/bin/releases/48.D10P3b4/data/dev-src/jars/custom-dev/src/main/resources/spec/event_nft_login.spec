cxps.events.event.nft_login{
table-name : EVENT_NFT_LOGIN
  event-mnemonic: NFL
  workspaces : {
	CUSTOMER : "cust_id,ip_address"
	NONCUSTOMER : "ip_address,device_id"
    }
  event-attributes : {
	host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        channel: {db : true ,raw_name : channel ,type : "string:20"}
        user_id: {db : true ,raw_name : user_id ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        user_type: {db : true ,raw_name : user_type ,type : "string:20"}
        device_id: {db : true ,raw_name : device_id ,type : "string:50"}
        ip_address: {db : true ,raw_name : ip_address ,type : "string:20"}
        ip_country: {db : true ,raw_name : ip_country ,type : "string:20"}
        ip_city: {db : true ,raw_name : ip_city ,type : "string:20"}
        error_code: {db : true ,raw_name : error_code ,type : "string:10"}
        error_desc: {db : true ,raw_name : error_desc ,type : "string:100"}
        last_login_ip: {db : true ,raw_name : last_login_ip ,type : "string:20"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        last_login_ts: {db : true ,raw_name : last_login_ts ,type : timestamp}
	cust_name: {db : true ,raw_name : cust_name ,type : "string:50"}
	app_id: {db : true ,raw_name : app_id ,type : "string:20"}
	country_code: {db : true ,raw_name : country_code ,type : "string:20"}
	account_id: {db : true ,raw_name : account_id ,type : "string:20"}
	mac_id: {db : true ,raw_name : mac_id ,type : "string:50"}
	mobile_no: {db : true ,raw_name : mobile_no ,type : "string:10"}
	tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
        }
}
