cxps.events.event.nft_mb_registration{
table-name : EVENT_NFT_MBREG
  event-mnemonic: NMB
  workspaces : {
    ACCOUNT : device_id
    }
  event-attributes : {
	host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        user_id: {db : true ,raw_name : user_id ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        app_id: {db : true ,raw_name : app_id ,type : "string:20"}
        device_id: {db : true ,raw_name : device_id ,type : "string:20"}
        error_code: {db : true ,raw_name : error_code ,type : "string:10"}
        country_code: {db : true ,raw_name : country_code ,type : "string:10"}
        succ_fail_flg: {db : true ,raw_name : succ_fail_flg ,type : "string:2"}
        mobile_no: {db : true ,raw_name : mobile_no ,type :  "string:10"}
        ip_address: {db : true ,raw_name : ip_address ,type : "string:20"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        error_desc: {db : true ,raw_name : error_desc ,type : "string:100"}
	tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
	pwd_reset_type: {db : true ,raw_name : pwd_reset_type ,type : "string:20"}
        }
}
