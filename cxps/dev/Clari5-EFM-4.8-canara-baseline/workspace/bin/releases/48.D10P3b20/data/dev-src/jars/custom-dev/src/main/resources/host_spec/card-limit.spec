# Generated Code
cxps.noesis.glossary.entity.Card_Limit {
    db-name = CARD_LIMIT
    generate = false
    db_column_quoted = true
    
    tablespace = CXPS_USERS
    attributes = [ 
        { name = card-type, column = Card_type, type = "string:10", key=false }
        { name = Card-Number, column = card_number, type = "date", key=true }
        { name = atm-limit, column = atm_limit, type = "date", key=false }
        { name = pos-limit, column = pos_limit, type = "date", key=false }
        { name = ecom-limit, column = ecom_limit, type = "date", key=false }
            ]
}
