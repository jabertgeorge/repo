// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_BRANCHADVANCES", Schema="rice")
public class NFT_BranchadvancesEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String branchCode;
       @Field(size=18) public Double previousDayAdvanceAmt;
       @Field(size=18) public Double previousMonthAdvanceAmt;
       @Field(size=18) public Double previousQuarterAdvanceAmt;
       @Field(size=18) public Double currentMonthAdvanceAmt;
       @Field(size=18) public Double currentDayAdvanceAmt;
       @Field(size=100) public String branchName;
       @Field(size=18) public Double currentQuarterAdvanceAmt;
       @Field public java.sql.Timestamp eventDateTime;


    @JsonIgnore
    public ITable<NFT_BranchadvancesEvent> t = AEF.getITable(this);

	public NFT_BranchadvancesEvent(){}

    public NFT_BranchadvancesEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Branchadvances");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBranchCode(json.getString("BranchCode"));
            setPreviousDayAdvanceAmt(EventHelper.toDouble(json.getString("PreviousDayAdvanceAmt")));
            setPreviousMonthAdvanceAmt(EventHelper.toDouble(json.getString("PreviousMonthAdvanceAmt")));
            setPreviousQuarterAdvanceAmt(EventHelper.toDouble(json.getString("PreviousQuarterAdvanceAmt")));
            setCurrentMonthAdvanceAmt(EventHelper.toDouble(json.getString("CurrentMonthAdvanceAmt")));
            setCurrentDayAdvanceAmt(EventHelper.toDouble(json.getString("CurrentDayAdvanceAmt")));
            setBranchName(json.getString("BranchName"));
            setCurrentQuarterAdvanceAmt(EventHelper.toDouble(json.getString("CurrentQuarterAdvanceAmt")));
            setEventDateTime(EventHelper.toTimestamp(json.getString("EventDateTime")));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NBA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getBranchCode(){ return branchCode; }

    public Double getPreviousDayAdvanceAmt(){ return previousDayAdvanceAmt; }

    public Double getPreviousMonthAdvanceAmt(){ return previousMonthAdvanceAmt; }

    public Double getPreviousQuarterAdvanceAmt(){ return previousQuarterAdvanceAmt; }

    public Double getCurrentMonthAdvanceAmt(){ return currentMonthAdvanceAmt; }

    public Double getCurrentDayAdvanceAmt(){ return currentDayAdvanceAmt; }

    public String getBranchName(){ return branchName; }

    public Double getCurrentQuarterAdvanceAmt(){ return currentQuarterAdvanceAmt; }

    public java.sql.Timestamp getEventDateTime(){ return eventDateTime; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBranchCode(String val){ this.branchCode = val; }
    public void setPreviousDayAdvanceAmt(Double val){ this.previousDayAdvanceAmt = val; }
    public void setPreviousMonthAdvanceAmt(Double val){ this.previousMonthAdvanceAmt = val; }
    public void setPreviousQuarterAdvanceAmt(Double val){ this.previousQuarterAdvanceAmt = val; }
    public void setCurrentMonthAdvanceAmt(Double val){ this.currentMonthAdvanceAmt = val; }
    public void setCurrentDayAdvanceAmt(Double val){ this.currentDayAdvanceAmt = val; }
    public void setBranchName(String val){ this.branchName = val; }
    public void setCurrentQuarterAdvanceAmt(Double val){ this.currentQuarterAdvanceAmt = val; }
    public void setEventDateTime(java.sql.Timestamp val){ this.eventDateTime = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_BranchadvancesEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String branchKey= h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(), this.branchCode);
        wsInfoSet.add(new WorkspaceInfo("Branch", branchKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Branchadvances");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Branchadvances");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}