package clari5.custom.canara.data;

public class NFT_AcctStat extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_ACCTSTAT";
    private String event_type = "NFT_Acctstat";

    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String HOST_ID;
    private String CHANNEL;
    private String CUST_ID;
    private String ACCOUNT_ID;
    private String ACCT_NAME;
    private String SCHM_TYPE;
    private String SCHM_CODE;
    private String INTIAL_ACCT_STATUS;
    private String FINAL_ACCT_STATUS;
    private String AVL_BAL;
    private String SYS_TIME;
    private String ACCT_OPEN_DATE;


    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getACCT_NAME() {
        return ACCT_NAME;
    }

    public void setACCT_NAME(String ACCT_NAME) {
        this.ACCT_NAME = ACCT_NAME;
    }

    public String getSCHM_TYPE() {
        return SCHM_TYPE;
    }

    public void setSCHM_TYPE(String SCHM_TYPE) {
        this.SCHM_TYPE = SCHM_TYPE;
    }

    public String getSCHM_CODE() {
        return SCHM_CODE;
    }

    public void setSCHM_CODE(String SCHM_CODE) {
        this.SCHM_CODE = SCHM_CODE;
    }

    public String getINTIAL_ACCT_STATUS() {
        return INTIAL_ACCT_STATUS;
    }

    public void setINTIAL_ACCT_STATUS(String INTIAL_ACCT_STATUS) {
        this.INTIAL_ACCT_STATUS = INTIAL_ACCT_STATUS;
    }

    public String getFINAL_ACCT_STATUS() {
        return FINAL_ACCT_STATUS;
    }

    public void setFINAL_ACCT_STATUS(String FINAL_ACCT_STATUS) {
        this.FINAL_ACCT_STATUS = FINAL_ACCT_STATUS;
    }

    public String getAVL_BAL() {
        return AVL_BAL;
    }

    public void setAVL_BAL(String AVL_BAL) {
        this.AVL_BAL = AVL_BAL;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }
}
