package clari5.custom.canara.data;

public class NFT_AcctClosing extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_ACCTCLOSING";
    private String event_type = "NFT_Acctclosing";



    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String SYS_TIME;
    private String HOST_ID;
    private String CHANNEL;
    private String ACCOUNT_ID;
    private String ACCT_OPEN_DATE;
    private String LOAN_TERMS;
    private String SCHEME_CODE;
    private String INTEREST_RATE;
    private String CUST_ID;
    private String SCHEME_TYPE;
    private String SAME_YEAR_FLG;

    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getLOAN_TERMS() {
        return LOAN_TERMS;
    }

    public void setLOAN_TERMS(String LOAN_TERMS) {
        this.LOAN_TERMS = LOAN_TERMS;
    }

    public String getSCHEME_CODE() {
        return SCHEME_CODE;
    }

    public void setSCHEME_CODE(String SCHEME_CODE) {
        this.SCHEME_CODE = SCHEME_CODE;
    }

    public String getINTEREST_RATE() {
        return INTEREST_RATE;
    }

    public void setINTEREST_RATE(String INTEREST_RATE) {
        this.INTEREST_RATE = INTEREST_RATE;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getSCHEME_TYPE() {
        return SCHEME_TYPE;
    }

    public void setSCHEME_TYPE(String SCHEME_TYPE) {
        this.SCHEME_TYPE = SCHEME_TYPE;
    }

    public String getSAME_YEAR_FLG() {
        return SAME_YEAR_FLG;
    }

    public void setSAME_YEAR_FLG(String SAME_YEAR_FLG) {
        this.SAME_YEAR_FLG = SAME_YEAR_FLG;
    }
}
