package clari5.custom.canara.data;

public class NFT_Todgrant extends ITableData{
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_TODGRANT";
    private String event_type = "NFT_TodGrant";

    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String SYS_TIME;
    private String CUST_ID;
    private String ACCOUNT_ID;
    private String GL_CODE;
    private String SCHEME_TYPE;
    private String SCHEME_CODE;
    private String TOD_REMARKS;
    private String TOD_AMT;
    private String DP_CODE;
    private String PSTD_USER_ID;


    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getGL_CODE() {
        return GL_CODE;
    }

    public void setGL_CODE(String GL_CODE) {
        this.GL_CODE = GL_CODE;
    }

    public String getSCHEME_TYPE() {
        return SCHEME_TYPE;
    }

    public void setSCHEME_TYPE(String SCHEME_TYPE) {
        this.SCHEME_TYPE = SCHEME_TYPE;
    }

    public String getSCHEME_CODE() {
        return SCHEME_CODE;
    }

    public void setSCHEME_CODE(String SCHEME_CODE) {
        this.SCHEME_CODE = SCHEME_CODE;
    }

    public String getTOD_REMARKS() {
        return TOD_REMARKS;
    }

    public void setTOD_REMARKS(String TOD_REMARKS) {
        this.TOD_REMARKS = TOD_REMARKS;
    }

    public String getTOD_AMT() {
        return TOD_AMT;
    }

    public void setTOD_AMT(String TOD_AMT) {
        this.TOD_AMT = TOD_AMT;
    }

    public String getDP_CODE() {
        return DP_CODE;
    }

    public void setDP_CODE(String DP_CODE) {
        this.DP_CODE = DP_CODE;
    }

    public String getPSTD_USER_ID() {
        return PSTD_USER_ID;
    }

    public void setPSTD_USER_ID(String PSTD_USER_ID) {
        this.PSTD_USER_ID = PSTD_USER_ID;
    }
}
