package clari5.custom.shiftuser.rest;

/*created by vignesh
 * on 15/11/19
 */

import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.Rdbms;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;


@Path("user")
public class ResponseUser {
    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(ResponseUser.class);

    @POST
    @Path("/addUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String addUser(@QueryParam("user") String userDetails) {
        //insert to table dont forget to check  if already exists.
        int i = 0;
        cxpsLogger.info("Userdetails : [ " + userDetails + " ]");
        String ar[] = userDetails.split(":");
        cxpsLogger.info("array :" + ar.toString());
        String query = "INSERT INTO CMS_USER_SHIFTS (SHIFT,PROJECT,USERNAME,HLMARKFLAG) VALUES (?,?,?,?)";
        try {
            if (ar.length > 0) {
                try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
                    ps.setString(1, ar[0]);
                    ps.setString(2, ar[1]);
                    ps.setString(3, ar[2]);
                    ps.setString(4, "N");
                    i = ps.executeUpdate();
                    con.commit();
                    cxpsLogger.info("inserted: [ " + i + "]");
                    return "true";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User already exists");
        }
        return "false";
    }

    //Update user with only Holiday Marking for the user
    @POST
    @Path("/updateUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String updateUser(@QueryParam("upuser") String userhmark) {

        int i = 0;
        String arr[] = userhmark.split(":");
        cxpsLogger.info("arr[]" + arr);

        try {
            String sql = "UPDATE CMS_USER_SHIFTS SET HLMARKFLAG =? WHERE USERNAME = ?";
            if (arr.length > 0) {
                try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
                    ps.setString(1, arr[0]);
                    ps.setString(2, arr[1]);
                    i = ps.executeUpdate();
                    con.commit();
                    cxpsLogger.info("inserted: [ " + i + "]");
                    return "true";
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User has already marked as" + i);
        }
        return "false";
    }

    @POST
    @Path("/deleteUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteUser(@QueryParam("deluser") String deluser) {
        //Remove the user from the table with id or username
        int i = 0;
        cxpsLogger.info("Userdetails : [ " + deluser + " ]");
        try {
            String sql = "DELETE FROM CMS_USER_SHIFTS WHERE USERNAME=?";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1, deluser);
                i = ps.executeUpdate();
                con.commit();
                return "True";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User cannot delete ");
        }
        return "False";
    }

    @GET
    @Path("/getCl5User")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getCl5User() {
        JSONArray userjson = new JSONArray();
        String user = null;
        try {
            String sql = "SELECT DISTINCT USER_ID FROM CL5_USER_TBL";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    user = rs.getString("USER_ID");
                    userjson.put(user);
                    cxpsLogger.info("USER_ID : [" + user + "]");
                }
                return userjson;
            }
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- Not getting user");
        }
        return userjson;
    }

    @GET
    @Path("/getShiftUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getShiftUser() {

        String shift = "";
        String username = "";
        String hlmrflg = "";
        String project = "";

        JSONObject shiftjsonobject = new JSONObject();

        try {
            String sql = "SELECT  SHIFT,PROJECT,USERNAME,HLMARKFLAG FROM CMS_USER_SHIFTS ORDER BY SHIFT";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    shift = rs.getString("SHIFT");
                    username = rs.getString("USERNAME");
                    hlmrflg = rs.getString("HLMARKFLAG");
                    project = rs.getString("PROJECT");
                    JSONObject userjsonobject = new JSONObject();
                    //JSONObject projectjson = new JSONObject();
                    JSONObject mrkjsonobject = new JSONObject();
                    JSONArray array = new JSONArray();
                    if (shiftjsonobject.has(shift)) {
                        JSONObject jsonObjectappend = shiftjsonobject.getJSONObject(shift);
                        mrkjsonobject.put("hmark", hlmrflg);
                        mrkjsonobject.put("PROJECT",project);
                        shiftjsonobject.remove(shift);
                        jsonObjectappend.append(username, mrkjsonobject);
                        //Arrays.sort(new String[]{shift});
                        shiftjsonobject.put(shift, jsonObjectappend);
                        cxpsLogger.info("Appended Json : [ " + shiftjsonobject + " ]");
                    } else {
                        mrkjsonobject.put("hmark", hlmrflg);
                        mrkjsonobject.put("PROJECT",project);
                        array.put(mrkjsonobject);
                        userjsonobject.put(username, array);
                        //Arrays.sort(new String[]{shift}
                        shiftjsonobject.put(shift, userjsonobject);
                        
                    }
                }
                cxpsLogger.info("Json object for sending to UI : [ " + shiftjsonobject + " ]");
                return shiftjsonobject.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- coming!! Kindly check.");
        }
        return shiftjsonobject.toString();

    }


    @GET
    @Path("/getShift")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getShift() {
        String sh = "";
        JSONArray shift = new JSONArray();
        try {
            String querry = "SELECT DISTINCT SHIFT FROM CMS_SHIFT_TIMING";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(querry); ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    sh = rs.getString("SHIFT");
                    shift.put(sh);
                    cxpsLogger.info("User: [" + shift + "]");
                }
                return shift;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return shift;
    }

    @POST
    @Path("/updatetime")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String updatetime(@QueryParam("uptime") String shifts) {
        int i = 0;
        try {
            String querry = "UPDATE CMS_SHIFT_TIMING SET FROMHOUR = ?,TOMINUTE = ?,FROMMINUTE = ?,TOHOUR = ? WHERE SHIFT=?";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(querry)) {
                cxpsLogger.info("Shifts : [ " + shifts + " ]");
                String shift[] = shifts.split(",");
                for (String currShift : shift) {
                    String time[] = currShift.split(":");
                    cxpsLogger.info("time[]" + time);
                    ps.setString(1, time[0]);
                    ps.setString(2, time[1]);
                    ps.setString(3, time[2]);
                    ps.setString(4, time[3]);
                    ps.setString(5, time[4]);
                    i = ps.executeUpdate();
                    cxpsLogger.info("Added : [ " + i + "]");
                }
                con.commit();
                cxpsLogger.info("Committed sucessfully");
                return "true";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("Unable to update Shift timings " + i);
        }
        return "false";
    }

    @POST
    @Path("/showShiftTime")
    @Produces(MediaType.TEXT_PLAIN)
    public String showShiftTime() {
        String shift = null;
        String fhour = null;
        String fmin = null;
        String thour = null;
        String tmin = null;
        ArrayList list = new ArrayList<String>();
        try {
            String sql = "SELECT SHIFT,FROMHOUR,TOMINUTE,FROMMINUTE,TOHOUR FROM CMS_SHIFT_TIMING ";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                //System.out.println("SHIFT"+"\t"+"FROMHOUR"+"\t"+"TOMINUTE"+"\t"+"FROMMINUTE"+"\t"+"TOHOUR");
                while (rs.next()) {
                    shift = rs.getString("SHIFT");
                    fhour = rs.getString("FROMHOUR");
                    fmin = rs.getString("TOMINUTE");
                    thour = rs.getString("FROMMINUTE");
                    tmin = rs.getString("TOHOUR");
                    //System.out.println(""+rs.getString("SHIFT")+""+rs.getString("FROMHOUR")+""+rs.getString("TOMINUTE")+""+rs.getString("FROMMINUTE")+""+rs.getString("TOHOUR"));
                    list.add(shift + ":" + "" + fhour + ":" + "" + tmin + ":" + "" + fmin + ":" + "" + thour);
                }
                cxpsLogger.info("list: [ " + list + "]");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list.toString();
    }

}