package clari5.custom.shiftuser.rest;


import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

import clari5.custom.regandderegdevices.restapi.ResponseDevices;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;


public class RequestHandler extends Application {

    private Set<Object> singletons = new HashSet<>();

    @Override
    public Set<Class<?>> getClasses() {
        return null;
    }

    public RequestHandler() {
        singletons.add(new ResponseUser());
        singletons.add(new ResponseDevices());
    }

    @Override
    public Set<Object> getSingletons() {
        return this.singletons;
    }
}
