package clari5.custom.dev.service;

import clari5.custom.dev.service.util.EventServiceUtility;
import clari5.platform.applayer.Clari5;
import clari5.platform.fileq.Clari5Payload;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.CxJson;
import org.json.JSONArray;
import org.json.JSONObject;
import cxps.apex.ed.services.ProcessEvent;
import cxps.eoi.Incident;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: Shashank Devisetty
 * Organization: CustomerXPs
 * Created On: 10/04/2019
 * Reviewed By:
 */

@Api(value = "/event/process")
@WebServlet("/event/process")
public class NoesisEventService extends HttpServlet {
    private static final CxpsLogger logger = CxpsLogger.getLogger(HEDEventService.class);
    static {
        logger.debug("Noesis Event Service Loaded successfully");
    }

    @ApiOperation(
            value = "Process event through NOESIS",
            produces = "Case(s) / Incident(s) triggered in JSON format",
            consumes = "Output of HED with workspace and derived events information",
            response = String.class
    )
    @Override
    @SuppressWarnings("unchecked")
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String payload = EventServiceUtility.extractPayload(request);
        String type = request.getParameter("event-type");
        try {
            if (type != null && !type.trim().isEmpty() && type.trim().toLowerCase().equals("list")) {
                JSONArray array = new JSONArray(payload);
                Iterator iterator = array.iterator();
                List<Clari5Payload> payloads = new ArrayList<>();
                while (iterator.hasNext()) {
                    JSONObject json = (JSONObject) iterator.next();
                    Clari5Payload clari5Payload = CxJson.json2obj(json.toString(), Clari5Payload.class);
                    payloads.add(clari5Payload);
                }
                EventServiceUtility.writeResponse(response, process(payloads));
            } else {
                Clari5Payload clari5Payload = CxJson.json2obj(payload, Clari5Payload.class);
                EventServiceUtility.writeResponse(response, process(clari5Payload));
            }
        } catch (Exception e) {
            logger.error("Exception occurred while adding payload [" + payload + "] to HED: " + e.getMessage(), e);
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        logger.error("HED Event Service does not accept GET calls. Only accepts POST call with a payload message which returns derived events along with the workspace keys");
    }

    private String process(Clari5Payload payload) {
        if (payload == null) return null;
        ProcessEvent event = (ProcessEvent) Clari5.getResource("NOESIS");
        String eventId = EventServiceUtility.extractEventId(payload);
        event.process(payload);
        List<Incident> incidents = EventServiceUtility.loadIncidents(eventId);
        if (incidents != null) return CxJson.obj2json(incidents);
        return null;
    }

    private String process(List<Clari5Payload> payloads) {
        ProcessEvent event = (ProcessEvent) Clari5.getResource("NOESIS");
        Set<String> eventIds = payloads.stream().map(EventServiceUtility::extractEventId).filter(Objects::nonNull).collect(Collectors.toSet());
        event.processBulk(payloads);
        Map<String, List<Incident>> incidentMap = eventIds.stream().collect(Collectors.toMap(eventId -> eventId, EventServiceUtility::loadIncidents, (a, b) -> b));
        return CxJson.obj2json(incidentMap);
    }
}
