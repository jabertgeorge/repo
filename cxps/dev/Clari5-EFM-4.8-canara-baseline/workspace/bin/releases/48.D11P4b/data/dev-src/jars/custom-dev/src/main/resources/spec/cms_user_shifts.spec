cxps.noesis.glossary.entity.cms_user_shifts {
	db-name = CMS_USER_SHIFTS
	generate = false
	db_column_quoted = true
	tablespace = CXPS_USERS
	attributes = [
                 { name = USERNAME, column = USERNAME, type = "string:200", key:true }
                 { name = PROJECT, column = PROJECT, type = "string:20" }
            	 { name = SHIFT, column = SHIFT, type = "string:20"}
               	 { name = HLMARKFLAG, column = HLMARKFLAG, type = "string:20"}
		         ]
}
