// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_TradingEventMapper extends EventMapper<FT_TradingEvent> {

public FT_TradingEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_TradingEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_TradingEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_TradingEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_TradingEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_TradingEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_TradingEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setDouble(i++, obj.getBuyAmount1());
            preparedStatement.setTimestamp(i++, obj.getValueDate());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setDouble(i++, obj.getExchangeRate());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setDouble(i++, obj.getMaturityAmt());
            preparedStatement.setString(i++, obj.getDealNo());
            preparedStatement.setString(i++, obj.getAmtCurrency2());
            preparedStatement.setString(i++, obj.getCounterparty());
            preparedStatement.setDouble(i++, obj.getSellAmount2());
            preparedStatement.setString(i++, obj.getTranReference());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getProductCode());
            preparedStatement.setString(i++, obj.getStatusFlag());
            preparedStatement.setString(i++, obj.getAmtCurrency1());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_TRADING]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_TradingEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_TradingEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADING"));
        putList = new ArrayList<>();

        for (FT_TradingEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "BUY_AMOUNT1", String.valueOf(obj.getBuyAmount1()));
            p = this.insert(p, "VALUE_DATE", String.valueOf(obj.getValueDate()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "EXCHANGE_RATE", String.valueOf(obj.getExchangeRate()));
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "MATURITY_AMT", String.valueOf(obj.getMaturityAmt()));
            p = this.insert(p, "DEAL_NO",  obj.getDealNo());
            p = this.insert(p, "AMT_CURRENCY2",  obj.getAmtCurrency2());
            p = this.insert(p, "COUNTERPARTY",  obj.getCounterparty());
            p = this.insert(p, "SELL_AMOUNT2", String.valueOf(obj.getSellAmount2()));
            p = this.insert(p, "TRAN_REFERENCE",  obj.getTranReference());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "PRODUCT_CODE",  obj.getProductCode());
            p = this.insert(p, "STATUS_FLAG",  obj.getStatusFlag());
            p = this.insert(p, "AMT_CURRENCY1",  obj.getAmtCurrency1());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_TRADING"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_TRADING]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_TRADING]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_TradingEvent obj = new FT_TradingEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setBuyAmount1(rs.getDouble("BUY_AMOUNT1"));
    obj.setValueDate(rs.getTimestamp("VALUE_DATE"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setExchangeRate(rs.getDouble("EXCHANGE_RATE"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setMaturityAmt(rs.getDouble("MATURITY_AMT"));
    obj.setDealNo(rs.getString("DEAL_NO"));
    obj.setAmtCurrency2(rs.getString("AMT_CURRENCY2"));
    obj.setCounterparty(rs.getString("COUNTERPARTY"));
    obj.setSellAmount2(rs.getDouble("SELL_AMOUNT2"));
    obj.setTranReference(rs.getString("TRAN_REFERENCE"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setProductCode(rs.getString("PRODUCT_CODE"));
    obj.setStatusFlag(rs.getString("STATUS_FLAG"));
    obj.setAmtCurrency1(rs.getString("AMT_CURRENCY1"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_TRADING]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_TradingEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_TradingEvent> events;
 FT_TradingEvent obj = new FT_TradingEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADING"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_TradingEvent obj = new FT_TradingEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setBuyAmount1(EventHelper.toDouble(getColumnValue(rs, "BUY_AMOUNT1")));
            obj.setValueDate(EventHelper.toTimestamp(getColumnValue(rs, "VALUE_DATE")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setExchangeRate(EventHelper.toDouble(getColumnValue(rs, "EXCHANGE_RATE")));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setMaturityAmt(EventHelper.toDouble(getColumnValue(rs, "MATURITY_AMT")));
            obj.setDealNo(getColumnValue(rs, "DEAL_NO"));
            obj.setAmtCurrency2(getColumnValue(rs, "AMT_CURRENCY2"));
            obj.setCounterparty(getColumnValue(rs, "COUNTERPARTY"));
            obj.setSellAmount2(EventHelper.toDouble(getColumnValue(rs, "SELL_AMOUNT2")));
            obj.setTranReference(getColumnValue(rs, "TRAN_REFERENCE"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setProductCode(getColumnValue(rs, "PRODUCT_CODE"));
            obj.setStatusFlag(getColumnValue(rs, "STATUS_FLAG"));
            obj.setAmtCurrency1(getColumnValue(rs, "AMT_CURRENCY1"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADING]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADING]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"BUY_AMOUNT1\",\"VALUE_DATE\",\"SYS_TIME\",\"EXCHANGE_RATE\",\"TRAN_DATE\",\"MATURITY_AMT\",\"DEAL_NO\",\"AMT_CURRENCY2\",\"COUNTERPARTY\",\"SELL_AMOUNT2\",\"TRAN_REFERENCE\",\"HOST_ID\",\"PRODUCT_CODE\",\"STATUS_FLAG\",\"AMT_CURRENCY1\"" +
              " FROM EVENT_FT_TRADING";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [BUY_AMOUNT1],[VALUE_DATE],[SYS_TIME],[EXCHANGE_RATE],[TRAN_DATE],[MATURITY_AMT],[DEAL_NO],[AMT_CURRENCY2],[COUNTERPARTY],[SELL_AMOUNT2],[TRAN_REFERENCE],[HOST_ID],[PRODUCT_CODE],[STATUS_FLAG],[AMT_CURRENCY1]" +
              " FROM EVENT_FT_TRADING";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`BUY_AMOUNT1`,`VALUE_DATE`,`SYS_TIME`,`EXCHANGE_RATE`,`TRAN_DATE`,`MATURITY_AMT`,`DEAL_NO`,`AMT_CURRENCY2`,`COUNTERPARTY`,`SELL_AMOUNT2`,`TRAN_REFERENCE`,`HOST_ID`,`PRODUCT_CODE`,`STATUS_FLAG`,`AMT_CURRENCY1`" +
              " FROM EVENT_FT_TRADING";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_TRADING (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"BUY_AMOUNT1\",\"VALUE_DATE\",\"SYS_TIME\",\"EXCHANGE_RATE\",\"TRAN_DATE\",\"MATURITY_AMT\",\"DEAL_NO\",\"AMT_CURRENCY2\",\"COUNTERPARTY\",\"SELL_AMOUNT2\",\"TRAN_REFERENCE\",\"HOST_ID\",\"PRODUCT_CODE\",\"STATUS_FLAG\",\"AMT_CURRENCY1\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[BUY_AMOUNT1],[VALUE_DATE],[SYS_TIME],[EXCHANGE_RATE],[TRAN_DATE],[MATURITY_AMT],[DEAL_NO],[AMT_CURRENCY2],[COUNTERPARTY],[SELL_AMOUNT2],[TRAN_REFERENCE],[HOST_ID],[PRODUCT_CODE],[STATUS_FLAG],[AMT_CURRENCY1]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`BUY_AMOUNT1`,`VALUE_DATE`,`SYS_TIME`,`EXCHANGE_RATE`,`TRAN_DATE`,`MATURITY_AMT`,`DEAL_NO`,`AMT_CURRENCY2`,`COUNTERPARTY`,`SELL_AMOUNT2`,`TRAN_REFERENCE`,`HOST_ID`,`PRODUCT_CODE`,`STATUS_FLAG`,`AMT_CURRENCY1`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

