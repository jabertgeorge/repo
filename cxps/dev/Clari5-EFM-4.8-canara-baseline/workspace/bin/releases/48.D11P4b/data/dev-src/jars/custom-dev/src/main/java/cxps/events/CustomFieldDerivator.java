package cxps.events;


import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.sql.*;
import clari5.rdbms.Rdbms;

public class CustomFieldDerivator {
/*
    static Hocon ignoreWorkspaceKey;
    static ArrayList<String> ignrWsKeys = new ArrayList<>();
    static {
        ignoreWorkspaceKey = new Hocon();
        ignoreWorkspaceKey.loadFromContext("workspace-derivator.conf");
        ignrWsKeys.addAll(ignoreWorkspaceKey.getStringList("derivators.ignoredEntities"));
    }

    private static final CxpsLogger logger = CxpsLogger.getLogger(CustomFieldDerivator.class);
    private boolean isAccountClosed;  */

    public static String getFirstTwelevNumber(NFT_CheckReturnEvent event)
    {
       
                       String input = event.getChequeNumber();     
                       String FirstTwelevNumber = "";     
                       if (input.length() > 12)
                      {
                      FirstTwelevNumber = input.substring(0, 12);
                      }

                       return FirstTwelevNumber;
    }

 public static String TrustedDevFlag(NFT_LoginEvent event)
{
  String trustedDevice = "";
   ResultSet rs = null;
   Statement stmt = null;
  String query = "SELECT USER_ID,DEVICE_ID FROM TRUST_DEV_TBL WHERE USER_ID='"+event.getUserId()+"' AND DEVICE_ID='"+event.getDeviceId()+"'";
  
     try (Connection con = Rdbms.getConnection()) {
	   stmt = con.createStatement();
           rs = stmt.executeQuery(query);
           if(rs.next()){
              trustedDevice = "Y";
          }
          else
            trustedDevice = "N";
       }
     catch(Exception e){
        e.printStackTrace();
     }
     finally{
        if(rs !=null)
          {

           try{
                rs.close();
             }
           catch(Exception e)
            { 

               e.printStackTrace();
            }
          }
        if(stmt !=null)
          {

           try{
                stmt.close();
             }
           catch(Exception e)
            { 

               e.printStackTrace();
            }
          }
     }
 return trustedDevice;
}
    public static String flagset(NFT_AcctclosingEvent event)
    {

    int year=0;
	event.sameYearFlg = "N";
	Timestamp accOpenDate = event.acctOpenDate;

	Timestamp financialYearStartDate = null;
	Timestamp financialYearEndDate = null;
	String startDate = "-04-01 00:00:00.000";
	String endDate = "-03-31 23:59:59.999";

	Calendar cal1 = Calendar.getInstance();
	cal1.setTime(accOpenDate);

	Calendar cal = Calendar.getInstance();
	cal.setTime(event.eventDate);
	int year1=cal.get(Calendar.YEAR);


	if(year1==cal1.get(Calendar.YEAR)){
		year = cal.get(Calendar.YEAR);
	}else {
		year = (cal.get(Calendar.YEAR)-1);
	}



	Date date= new Date();
	long time = date.getTime();
	Timestamp ts = new Timestamp(time);
	Timestamp YearStartDate = Timestamp.valueOf(String.valueOf(year)+startDate);

	if (ts.before(YearStartDate))
 	{	
 	financialYearStartDate = Timestamp.valueOf(String.valueOf(year-1)+startDate);
	 financialYearEndDate = Timestamp.valueOf(String.valueOf(year)+endDate);
	}cx
	else
	{
	financialYearStartDate = Timestamp.valueOf(String.valueOf(year) + startDate);
	financialYearEndDate = Timestamp.valueOf(String.valueOf(year + 1) + endDate);
	}	

	if (accOpenDate.after(financialYearStartDate) && accOpenDate.before(financialYearEndDate))
	{
	event.sameYearFlg = "Y";
	}
	return event.sameYearFlg;
	}

}

 /*   public static String getChannelFromTxnNumonicCode(String txnNumonicCode) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String channel = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select TRANSACTION_CHANNEL from TRANSACTION_NUMONIC_CODE where TRANSACTION_NUMONIC_CODE = ?");
            st.setString(1, txnNumonicCode);
            rs = st.executeQuery();
            if (rs.next()) {
                channel = rs.getString("TRANSACTION_CHANNEL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        System.out.println("getChannelFromTxnNumonicCode : " + channel);
        return channel;
    }

    public static String getSubtranmodeFromTxnNumonicCode(String txnNumonicCode) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String subtranmode = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select SUB_TRAN_MODE from TRANSACTION_NUMONIC_CODE where TRANSACTION_NUMONIC_CODE = ?");
            st.setString(1, txnNumonicCode);
            rs = st.ex/cuteQuery();
            if (rs.next()) {
                subtranmode = rs.getString("SUB_TRAN_MODE");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        System.out.println("getSubtranmodeFromTxnNumonicCode : " + subtranmode);
        return subtranmode;
    }


    public static String getaddedentitydetails(FT_FundtransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        Statement stmt = null;
        ResultSet result = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String addedentity = "";
        try {
            con = Rdbms.getAppConnection();
            stmt = con.createStatement();
//        String query = "select EVENT_DATE from EVENT_NFT_CREDENTIALCHANGE  where to_char(EVENT_DATE, 'YYYY-MM-DD HH:MI:SS') < ? and CUST_ID=?";
            String query = "select EVENT_DATE from EVENT_NFT_CREDENTIALCHANGE where EVENT_DATE >= to_timestamp(?,'DD/MM/YY HH24:MI:SS.FF') - 2 and CUST_ID=?";
            psmt = con.prepareStatement(query);
            psmt.setString(1, simpleDateFormat.format(event.eventDate));
            psmt.setString(2, event.custId);
            result = stmt.executeQuery(query);
            if (result.next()) {
                addedentity = "Y";
            } else addedentity = "N";


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }

                if (con != null) {
                    con.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
        return addedentity;
    }


    public static String deriveNonHomeBranch(FT_CoreAcctTxnEvent event) {
        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Connection con = null;
        PreparedStatement ps = null, ps1 = null, ps2 = null, ps3 = null;
        ResultSet result = null, result1 = null, result2 = null, result3 = null;
        int count = 0;
        try {
            con = Rdbms.getAppConnection();
            QueryMapper mapper = QueryMapper.getInstance(Rdbms.getAppDbType(), "clari5.custom");
            String userId = event.getUserId();
            String custId = event.getCustId();
            if (userId != null && !"".equals(userId)) {
                String loadquery = mapper.get(Queries.SELECT_TYPE);
                String loadbranchquery = mapper.get(Queries.SELECT_BRANCH);
                ps = con.prepareStatement(loadquery);
                ps.setString(1, userId);
                result = ps.executeQuery();
                if (result.next()) {
                    event.setUserType("MANUAL");
                    ps3 = con.prepareStatement(loadbranchquery);
                    ps3.setString(1, result.getString("BRANCH"));
                    result3 = ps3.executeQuery();
                    if (result3.next()) {
                        return "Y";
                    } else {
                        return "N";
                    }
                } else {
                    event.setUserType("SYSTEM");
                }
                //TODO set default value for non home branch if above condition fails
            }

            //TODO move this out in a separate function for better readability.
            if (custId != null && !"".equals(custId)) {
                String loadcustType = mapper.get(Queries.SELECT_CUST_TYPE);
                ps1 = con.prepareStatement(loadcustType);
                ps1.setString(1, custId);
                result1 = ps1.executeQuery();
                String custType = "";
                while (result1.next()) {
                    custType = result1.getString("CUSTOMER_TYPE");
                    String loadcustTyp = mapper.get(Queries.SELECT_CUST_FLG);
                    ps2 = con.prepareStatement(loadcustTyp);
                    ps2.setString(1, custType);
                    result2 = ps2.executeQuery();
                    if (result2.next()) {
                        String flag = result2.getString("FLAG");
                        event.setCustType(flag);
                    }
                }
            }

        } catch (Exception e) {

        } finally {
            try {
                if (result != null) result.close();
                if (ps != null) ps.close();
            } catch (Exception e) {
                return "";
            }
            try {
                if (con != null) con.close();
            } catch (Exception e) {
                return "";
            }
        }
        return "";
    }

    public static String getTransactionModeFromTxnNumonicCode(String trannumonicCode) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String transactionmode = "";
        int trancode = Integer.parseInt(trannumonicCode);
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select TRANSACTION_MODE from MNEMONIC_MASTER_NATM where COD_TXN_MNEMONIC = ?");
            st.setInt(1, trancode);
            rs = st.executeQuery();
            if (rs.next()) {
                transactionmode = rs.getString("TRANSACTION_MODE");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        System.out.println("getTransactionModeFromTxnNumonicCode : " + transactionmode);
        return transactionmode;
    }

    public static String getWhitelistflagforAccount(FT_FundtransferEvent event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String watchlist_flag = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select * from ENTITY_WHITELIST where ACCOUNTNO = ? and IFSC = ?");
            st.setString(1, event.accountNumber);
            st.setString(2, event.ifscCode);
            rs = st.executeQuery();
            if (rs.next()) {

                watchlist_flag = "Y";
            } else {
                watchlist_flag = "N";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return watchlist_flag;
    }

    public static String deriveUserType(FT_CoreAcctTxnEvent event) {
        CxKeyHelper h = Hfdb.getCxKeyHelper();
        Connection con = null;
        PreparedStatement ps = null, ps1 = null;
        ResultSet result = null, result1 = null;
        String userType = "";
        try {
            con = Rdbms.getAppConnection();
            String userId = event.getUserId();
            ps = con.prepareStatement("select * from EMPLOYEEINFORMATION where EMPLOYEE_ID = ?");
            ps.setString(1, userId);
            result = ps.executeQuery();
            if (result.next()) {
                userType = "MANUAL";
            } else {
                userType = "SYSTEM";
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (result != null) result.close();
                if (ps != null) ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (con != null) con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return userType;
    }

    public static String getPayeecombination(FT_FundtransferEvent event) {

        String Combination_key = "";
        try {
            Combination_key = event.beneficiaryType1 + event.beneficaryUniqueIdentifier1;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return Combination_key;
    }

    public static String getRiskBand(String session_id, String cust_id) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String risk_band = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select RISK_BAND from EVENT_NFT_IBLOGIN where SESSION_ID = ? and CUST_ID = ?");
            st.setString(1, session_id);
            st.setString(2, cust_id);
            rs = st.executeQuery();
            while (rs.next()) {
                risk_band = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return risk_band;

    }

    public static String checkInstanceofEvent(Object event) {

        if (event instanceof FT_FundtransferEvent)
            return (String) getRiskBand(((FT_FundtransferEvent) event).getSessionId(), ((FT_FundtransferEvent) event).getCustId());

        if (event instanceof FT_BillpaymentEvent)
            return (String) getRiskBand(((FT_BillpaymentEvent) event).getSessionId(), ((FT_BillpaymentEvent) event).getCustId());

        if (event instanceof FT_EipoapplicationEvent)
            return (String) getRiskBand(((FT_EipoapplicationEvent) event).getSessionId(), ((FT_EipoapplicationEvent) event).getCustId());

        if (event instanceof FT_GenericenEvent)
            return (String) getRiskBand(((FT_GenericenEvent) event).getSessionId(), ((FT_GenericenEvent) event).getCustId());

        if (event instanceof FT_EpiEvent)
            return (String) getRiskBand(((FT_EpiEvent) event).getSessionId(), ((FT_EpiEvent) event).getCustId());

        if (event instanceof FT_WmsEvent)
            return (String) getRiskBand(((FT_WmsEvent) event).getSessionId(), ((FT_WmsEvent) event).getCustId());

        if (event instanceof NFT_CredentialChangeEvent)
            return (String) getRiskBand(((NFT_CredentialChangeEvent) event).getSessionId(), ((NFT_CredentialChangeEvent) event).getCustId());

        if (event instanceof NFT_BeneficiaryEvent)
            return (String) getRiskBand(((NFT_BeneficiaryEvent) event).getSessionId(), ((NFT_BeneficiaryEvent) event).getCustId());

        return null;
    }

    public static double getDistanceBetweenIP(NFT_IbLoginEvent event) {
        // con = Rdbms.getAppConnection();
        double distance = 0;
        String current_ip = "";
        // If cust_id is not available in the DISTANCE_IP table
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                if (checkAvailablecustomer(event.custId).equals("N")) {
                    try (PreparedStatement statement = connection.prepareStatement("INSERT INTO DISTANCE_IP (CUST_ID,CURRENT_IP) VALUES (?,?)")) {
                        statement.setString(1, event.custId);
                        statement.setString(2, event.getAddrNetwork());
                        statement.executeQuery();
                        connection.commit();
                    }
                } else {
                    if (checkAvailablecustomer(event.custId).equals("Y")) {
                        //if cust_id is available in DISTANCE_IP table
                        try (PreparedStatement ps = connection.prepareStatement("SELECT CURRENT_IP FROM DISTANCE_IP WHERE CUST_ID=?")) {
                            ps.setString(1, event.custId);
                            try (ResultSet rs = ps.executeQuery()) {
                                while (rs.next()) {
                                    current_ip = rs.getString("CURRENT_IP");
                                }
                                if (current_ip == null || current_ip.equalsIgnoreCase("null")) {
                                    //when customer is available in Distance_IP but current_IP is null.
                                    try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE DISTANCE_IP SET CURRENT_IP =? WHERE CUST_ID=?")) {
                                        preparedStatement.setString(1, event.getAddrNetwork());
                                        preparedStatement.setString(2, event.custId);
                                        preparedStatement.executeUpdate();
                                        connection.commit();
                                    }
                                } else {
                                    //swap current_ip with last_ip if CURRENT_IP is not null
                                    try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE DISTANCE_IP SET CURRENT_IP =?,LAST_IP=? WHERE CUST_ID=?")) {
                                        preparedStatement.setString(1, event.getAddrNetwork());
                                        preparedStatement.setString(2, current_ip);
                                        preparedStatement.setString(3, event.custId);
                                        preparedStatement.executeUpdate();
                                        connection.commit();
                                    }
                                }
                                distance = MaxmindUtil.getDistance(current_ip, event.getAddrNetwork());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.debug("Error processing data for event id: ---> " + event.getEventId() +
                        " and cust id from login event: ---> " + event.getCustId());
                e.printStackTrace();
            }
        }
        return distance;
    }

    public static String checkAvailablecustomer(String customer_id) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String Available_flag = "";
        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select * from DISTANCE_IP where CUST_ID = ?");
            st.setString(1, customer_id);
            rs = st.executeQuery();
            if (rs.next()) {
                Available_flag = "Y";
            } else {
                Available_flag = "N";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return Available_flag;

    }

    public static String SaveBeneficiaryDb(NFT_BeneficiaryEvent event) {
        // Method for beneficiary updated date
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String Benf_flag = "";
        try {
            con = Rdbms.getAppConnection();

            if (((NFT_BeneficiaryEvent) event).getCustId() != null && ((NFT_BeneficiaryEvent) event).getGroupPayeeId() != null && ((NFT_BeneficiaryEvent) event).getBenfFlag1() != null) {
                if (!event.benfFlag1.equals("E") || event.benfFlag1 != null) {

                    if (event.benfFlag1.equals("N")) {
                        InsertBeneficiarydetails(event);
                        Benf_flag = "created";
                    }
                    if (event.benfFlag1.equals("M")) {
                        UpdateBeneficiaryDetails(event);
                        Benf_flag = "updated";
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        return Benf_flag;

    }

    public static java.sql.Timestamp getBeneficiarydetails(FT_FundtransferEvent event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Timestamp result = null;

        try {
            con = Rdbms.getAppConnection();
            st = con.prepareStatement("select CREATED_ON from BENEFICIARY_UDATE where GROUP_PAYEE_ID = ? and BENF_PAYEE_ID = ? and CUST_ID = ?");
            st.setString(1, event.groupPayeeId);
            st.setString(2, event.benfPayeeId);
            st.setString(3, event.custId);
            rs = st.executeQuery();
            while (rs.next()) {
                result = rs.getTimestamp(1);
            }
            return result != null ? result : new Timestamp(10000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {

            }
        }
        return result != null ? result : new Timestamp(10000);
    }

    public static void InsertBeneficiarydetails(Object event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            con = Rdbms.getAppConnection();
            if (event instanceof NFT_BeneficiaryEvent) {
                st = con.prepareStatement("INSERT INTO BENEFICIARY_UDATE (PAYEE_ACCTTYPE, CUST_ID, BENF_UNIQUE_VALUE, GROUP_PAYEE_ID, BENF_PAYEE_ID, CREATED_ON, BENF_FLAG, BENEFICIARY_NICKNAME, UPDATED_ON, BENF_TYPE) VALUES(?,?,?,?,?,?,?,?,?,?)");

                int i = 1;
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getPayeeAccttype1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getCustId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfUniqueValue1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getGroupPayeeId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfPayeeId1());
                st.setTimestamp(i++, ((NFT_BeneficiaryEvent) event).getSysTime());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfFlag1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfNickname1());
                st.setTimestamp(i++, ((NFT_BeneficiaryEvent) event).getSysTime());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfType1());

                st.executeUpdate();
                con.commit();
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
    }

    public static void UpdateBeneficiaryDetails(Object event) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            con = Rdbms.getAppConnection();
            if (event instanceof NFT_BeneficiaryEvent) {
                st = con.prepareStatement("UPDATE BENEFICIARY_UDATE SET PAYEE_ACCTTYPE=?, BENF_UNIQUE_VALUE=?, BENF_FLAG=?, BENEFICIARY_NICKNAME=?, UPDATED_ON=?, BENF_TYPE=? WHERE CUST_ID=? AND GROUP_PAYEE_ID=? AND BENF_PAYEE_ID=?");

                int i = 1;
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getPayeeAccttype1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfUniqueValue1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfFlag1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfNickname1());
                st.setTimestamp(i++, ((NFT_BeneficiaryEvent) event).getSysTime());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfType1());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getCustId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getGroupPayeeId());
                st.setString(i++, ((NFT_BeneficiaryEvent) event).getBenfPayeeId1());

                st.executeUpdate();
                con.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                st.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }

        }
    }*/


