package clari5.custom.dev;

import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author lakshmisai
 */
public class SoapEnqServlet extends HttpServlet {

    private static CxpsLogger logger = CxpsLogger.getLogger(SoapEnqServlet.class);

    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void soapRequest(HttpServletRequest request, HttpServletResponse response) {

        logger.info("Incoming Enq request from switch");
        String strMsg = "";
        String retStr = "";

        MessageFactory messageFactory = null;
        InputStream inStream = null;
        SOAPMessage soapMessage = null;
        ByteArrayOutputStream out = null;
        PrintWriter writer = null;

        try {

            messageFactory = MessageFactory.newInstance();
            inStream = request.getInputStream();
            soapMessage = messageFactory.createMessage(new MimeHeaders(), inStream);
            out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            strMsg = new String(out.toByteArray());
            // strMsg = out.toString();
             logger.info("Received xml from switch for Enq "+strMsg);
             if(strMsg != null && strMsg.contains("FRMRQST")) {
                 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                 logger.info("Before Parsing=>" + sdf.format(new Date()));


                 JSONObject jsonObject = new JSONObject();
                 jsonObject = new FormatJson().getJsonObject(new SaxParser().reqParser(removeXmlStringNamespaceAndPreamble(strMsg)));

                 if( jsonObject != null) {
                     logger.info("Enq  after Parsing=>" + sdf.format(new Date()));
                     SendEvent sendEvent = new SendEvent();
                     retStr = sendEvent.sendEnqueueEvent(jsonObject);

                     logger.info("Enq  After Clarif5 Response=>" + sdf.format(new Date()));
                     logger.info("Enqueue status=>" + retStr);
                     strMsg = strMsg.replaceAll("FRMRQST", "FRMRESP");

                     if (retStr != null && retStr.equalsIgnoreCase("SUCESS")) {
                         if (strMsg.contains("<TYP>0220</TYP>")) {
                             strMsg = strMsg.replaceAll("<TYP>0220</TYP>", "<TYP>0230</TYP>");

                             if (strMsg.contains("<RESP_CDE>")) {
                                 int j = strMsg.indexOf("<RESP_CDE>");
                                 String substring = strMsg.substring(j, j + 23);
                                 strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                             } else {
                                 strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                             }
                         }
                         logger.info("response => Final enqueue xml is " + strMsg);
                         response.setStatus(200);
                         response.setContentType("text/xml");
                         writer = response.getWriter();
                         writer.append(strMsg);
                         writer.flush();
                     } else {

                         if (strMsg.contains("<TYP>0220</TYP>")) {
                             strMsg = strMsg.replaceAll("<TYP>0220</TYP>", "<TYP>0230</TYP>");

                             if (strMsg.contains("<RESP_CDE>")) {
                                 int j = strMsg.indexOf("<RESP_CDE>");
                                 String substring = strMsg.substring(j, j + 23);
                                 strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                             } else {
                                 strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                             }
                         }

                         logger.info("INVALID ENQUEUE RESPONSE");
                         response.setStatus(200);
                         response.setContentType("text/xml");
                         writer = response.getWriter();
                         writer.append(strMsg);
                         writer.flush();
                     }
                     logger.info("ENQ End of the request=>" + sdf.format(new Date()));
                 }
                 else {
                     logger.info("json is invalid");
                     if (strMsg.contains("<TYP>0220</TYP>")) {
                         strMsg = strMsg.replaceAll("<TYP>0220</TYP>", "<TYP>0230</TYP>");

                         if (strMsg.contains("<RESP_CDE>")) {
                             int j = strMsg.indexOf("<RESP_CDE>");
                             String substring = strMsg.substring(j, j + 23);
                             strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                         } else {
                             strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                         }
                     }
                     response.setStatus(200);
                     response.setContentType("text/xml");
                     writer = response.getWriter();
                     writer.append(strMsg);
                     writer.flush();
                 }
             }
             else {
                 logger.info("Invalid xml");
                 response.setStatus(200);
                 response.setContentType("text/xml");
                 writer = response.getWriter();
                 writer.append(strMsg);
                 writer.flush();
             }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("exception");
            if (strMsg.contains("<TYP>0220</TYP>")) {
                strMsg = strMsg.replaceAll("<TYP>0220</TYP>", "<TYP>0230</TYP>");
                strMsg = strMsg.replaceAll("FRMRQST", "FRMRESP");
                if (strMsg.contains("<RESP_CDE>")) {
                    int j = strMsg.indexOf("<RESP_CDE>");
                    String substring = strMsg.substring(j, j + 23);
                    strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                } else {
                    strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                }
            }
            response.setStatus(200);
            response.setContentType("text/xml");
            try {
                writer = response.getWriter();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            writer.append(strMsg);
            writer.flush();

        } finally {
            try {
                inStream.close();
                out.close();
                writer.close();
                messageFactory = null;
                soapMessage = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            // Retrieve different parts
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
            // Two ways to extract headers
            SOAPHeader soapHeader = soapEnvelope.getHeader();
            soapHeader = soapMessage.getSOAPHeader();
            // Two ways to extract body
            SOAPBooapBody = soapEnvelope.getBody();
            soapBody = soapMessage.getSOAPBody();
            // To add some element
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            Name bodyName = soapFactory.createName("getEmployeeDetails", "ns1", "urn:MySoapServices");
            SOAPBodyElement purchaseLineItems = soapBody.addBodyElement(bodyName);
            Name childName = soapFactory.createName("param1");
            SOAPElement order = purchaseLineItems.addChildElement(childName);
            order.addTextNode("1016577");

        } catch (Exception e) {
            e.printStackTrace();

        }
         */
    }

    public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
        return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
                replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
                .replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
                .replaceAll("(</)(\\w+:)(.*?>)", "$1$3");
        /* remove closing tags prefix */
    }

}
