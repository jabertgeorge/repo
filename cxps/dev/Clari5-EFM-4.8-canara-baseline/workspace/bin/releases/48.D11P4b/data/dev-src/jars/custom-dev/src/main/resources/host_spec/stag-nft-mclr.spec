clari5.hfdb.entity.nft_mclr{
attributes = [

        { name = CL5_FLG,  type= "string:10"}
        { name = CREATED_ON,  type= timestamp}
        { name = UPDATED_ON,  type= timestamp}
        { name = ID, type="string:2000", key = true}
        { name = sys_time, type="timestamp"}
        { name = host_id, type = "string:2"}
        { name = channel, type = "string:10"}
        { name = cust_id, type = "string:20"}
        { name = account_id, type = "string:20"}
        { name = scheme_type, type = "string:50"}
        { name = scheme_code, type = "string:20"}
        { name = gl_code, type = "string:20"}
        { name = chq_start_no, type = "string:20"}
        { name = chq_end_no, type = "string:10"}
        { name = roi, type = "number:20,3"}
        { name = mclr, type = "number:20,3"}
        { name = ren_ext_new_flg, type = "string:20"}

        ]
}

