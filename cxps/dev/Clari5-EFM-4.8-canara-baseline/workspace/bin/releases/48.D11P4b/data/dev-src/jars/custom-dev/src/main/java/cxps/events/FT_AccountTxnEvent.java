// -- ASSISTED CODE --
package cxps.events;

import java.sql.*;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;

import clari5.rdbms.Rdbms;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.utils.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_ACCOUNT_TXN", Schema="rice")
public class FT_AccountTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String contraCustid;
       @Field(size=20) public String txnRefId;
       @Field(size=20) public String originIpAddr;
       @Field(size=20) public String pstdUserId;
       @Field(size=20) public String acctSolCity;
       @Field(size=20) public String channel;
       @Field public java.sql.Timestamp valueDate;
       @Field(size=20) public String mnemonicCode;
       @Field(size=20) public Double prevBal;
       @Field(size=20) public String onlineBatch;
       @Field(size=20) public String tranSubType;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String txnDrCr;
       @Field(size=20) public String hostId;
       @Field(size=20) public String entityId;
       @Field(size=20) public String contraSchmCode;
       @Field(size=20) public String eventSubType;
       @Field(size=20) public String accountId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String branchId;
       @Field(size=20) public String tranCrncyCode;
       @Field(size=20) public String tranCode;
       @Field(size=20) public String acctName;
       @Field(size=20) public String custCardId;
       @Field(size=20) public String tranSrlNo;
       @Field(size=20) public String contraGlFlag;
       @Field(size=20) public String eventName;
       @Field(size=20) public Double avlBal;
       @Field(size=20) public String dpCode;
       @Field(size=20) public String acctOwnership;
       @Field(size=20) public String schemeType;
       @Field(size=20) public String txnBrCity;
       @Field(size=20) public String pstdFlg;
       @Field(size=20) public String contraAcctId;
       @Field(size=20) public String acctStatus;
       @Field(size=20) public String entityType;
       @Field(size=20) public String addEntity5;
       @Field(size=20) public String addEntity6;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String addEntity3;
       @Field(size=20) public String glFlag;
       @Field(size=20) public String ifscCode;
       @Field(size=20) public String addEntity4;
       @Field(size=20) public String addEntity1;
       @Field(size=20) public String schemeCode;
       @Field(size=20) public String addEntity2;
       @Field(size=20) public String penalInterestFlag;
       @Field public java.sql.Timestamp instrmntDate;
       @Field(size=20) public String addEntity9;
       @Field(size=20) public String tdLiquidationFlag;
       @Field(size=20) public String addEntity7;
       @Field(size=11) public Double clrBalAmt;
       @Field(size=20) public String addEntity8;
       @Field(size=11) public Double rate;
       @Field public java.sql.Timestamp acctClosedDate;
       @Field(size=20) public Double txnAmt;
       @Field(size=20) public String tranCategory;
       @Field(size=20) public String instrmntNum;
       @Field(size=20) public String entryUser;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String acctSolId;
       @Field public java.sql.Timestamp reconId;
       @Field(size=20) public String eventType;
       @Field(size=20) public String refCurrency;
       @Field(size=20) public String contraBankCode;
       @Field(size=20) public String contraSolId;
       @Field public java.sql.Timestamp lastTranDate;
       @Field(size=20) public String tranParticular;
       @Field(size=20) public String custId;
       @Field public java.sql.Timestamp acctMaturityDate;
       @Field(size=20) public String txnBrId;
       @Field(size=20) public String tranType;
       @Field(size=20) public String instrumentType;
       @Field(size=11) public Double refTranAmt;
       @Field public java.sql.Timestamp reActivationDate;
       @Field(size=20) public String staffFlag;
       @Field(size=20) public String addEntity10;


    @JsonIgnore
    public ITable<FT_AccountTxnEvent> t = AEF.getITable(this);

	public FT_AccountTxnEvent(){}

    public FT_AccountTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("AccountTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setContraCustid(json.getString("contra_custid"));
            setTxnRefId(json.getString("tran_id"));
            setOriginIpAddr(json.getString("ip_address"));
            setPstdUserId(json.getString("pstd_user_id"));
            setAcctSolCity(json.getString("acct_sol_city"));
            setChannel(json.getString("channel"));
            setValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setMnemonicCode(json.getString("mnemonic_code"));
            setPrevBal(EventHelper.toDouble(json.getString("prev_bal")));
            setOnlineBatch(json.getString("online_batch"));
            setTranSubType(json.getString("tran_sub_type"));
            setDeviceId(json.getString("device_id"));
            setTxnDrCr(json.getString("part_tran_type"));
            setHostId(json.getString("host_id"));
            setEntityId(json.getString("EntityId"));
            setContraSchmCode(json.getString("contra_schm_code"));
            setEventSubType(json.getString("EventSubType"));
            setAccountId(json.getString("account_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTranCode(json.getString("tran_code"));
            setAcctName(json.getString("acct_name"));
            setCustCardId(json.getString("cust_card_id"));
            setTranSrlNo(json.getString("part_tran_srl_num"));
            setContraGlFlag(json.getString("contra_gl_flag"));
            setEventName(json.getString("event_name"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setDpCode(json.getString("dp_code"));
            setAcctOwnership(json.getString("acct_ownership"));
            setSchemeType(json.getString("schm_type"));
            setTxnBrCity(json.getString("txn_br_city"));
            setPstdFlg(json.getString("pstd_flg"));
            setContraAcctId(json.getString("contra_account_id"));
            setAcctStatus(json.getString("Acct_Stat"));
            setEntityType(json.getString("EntityType"));
            setAddEntity5(json.getString("addentity5"));
            setAddEntity6(json.getString("addentity6"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setAddEntity3(json.getString("addentity3"));
            setGlFlag(json.getString("gl_flag"));
            setIfscCode(json.getString("ifsc_code"));
            setAddEntity4(json.getString("addentity4"));
            setAddEntity1(json.getString("addentity1"));
            setSchemeCode(json.getString("schm_code"));
            setAddEntity2(json.getString("addentity2"));
            setPenalInterestFlag(json.getString("penal_interest_flag"));
            setInstrmntDate(EventHelper.toTimestamp(json.getString("instrmnt_date")));
            setAddEntity9(json.getString("addentity9"));
            setTdLiquidationFlag(json.getString("td_liquidation_flg"));
            setAddEntity7(json.getString("addentity7"));
            setClrBalAmt(EventHelper.toDouble(json.getString("clr_bal_amt")));
            setAddEntity8(json.getString("addentity8"));
            setRate(EventHelper.toDouble(json.getString("rate")));
            setAcctClosedDate(EventHelper.toTimestamp(json.getString("acct_closed_date")));
            setTxnAmt(EventHelper.toDouble(json.getString("txn_amt")));
            setTranCategory(json.getString("tran_category"));
            setInstrmntNum(json.getString("instrmnt_num"));
            setEntryUser(json.getString("entry_user"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setAcctSolId(json.getString("acct_sol_id"));
            setReconId(EventHelper.toTimestamp(json.getString("reconId")));
            setEventType(json.getString("EventType"));
            setRefCurrency(json.getString("ref_tran_crncy"));
            setContraBankCode(json.getString("contra_bank_code"));
            setContraSolId(json.getString("contra_sol_id"));
            setLastTranDate(EventHelper.toTimestamp(json.getString("last_tran_date")));
            setTranParticular(json.getString("tran_particular"));
            setCustId(json.getString("cust_id"));
            setAcctMaturityDate(EventHelper.toTimestamp(json.getString("acct_maturity_date")));
            setTxnBrId(json.getString("txn_br_id"));
            setTranType(json.getString("tran_type"));
            setInstrumentType(json.getString("instrmnt_type"));
            setRefTranAmt(EventHelper.toDouble(json.getString("ref_txn_amt")));
            setReActivationDate(EventHelper.toTimestamp(json.getString("re_activation_date")));
            setStaffFlag(json.getString("staff_flag"));
            setAddEntity10(json.getString("addentity10"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FTA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getContraCustid(){ return contraCustid; }

    public String getTxnRefId(){ return txnRefId; }

    public String getOriginIpAddr(){ return originIpAddr; }

    public String getPstdUserId(){ return pstdUserId; }

    public String getAcctSolCity(){ return acctSolCity; }

    public String getChannel(){ return channel; }

    public java.sql.Timestamp getValueDate(){ return valueDate; }

    public String getMnemonicCode(){ return mnemonicCode; }

    public Double getPrevBal(){ return prevBal; }

    public String getOnlineBatch(){ return onlineBatch; }

    public String getTranSubType(){ return tranSubType; }

    public String getDeviceId(){ return deviceId; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getHostId(){ return hostId; }

    public String getEntityId(){ return entityId; }

    public String getContraSchmCode(){ return contraSchmCode; }

    public String getEventSubType(){ return eventSubType; }

    public String getAccountId(){ return accountId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getTranCode(){ return tranCode; }

    public String getAcctName(){ return acctName; }

    public String getCustCardId(){ return custCardId; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public String getContraGlFlag(){ return contraGlFlag; }

    public String getEventName(){ return eventName; }

    public Double getAvlBal(){ return avlBal; }

    public String getDpCode(){ return dpCode; }

    public String getAcctOwnership(){ return acctOwnership; }

    public String getSchemeType(){ return schemeType; }

    public String getTxnBrCity(){ return txnBrCity; }

    public String getPstdFlg(){ return pstdFlg; }

    public String getContraAcctId(){ return contraAcctId; }

    public String getAcctStatus(){ return acctStatus; }

    public String getEntityType(){ return entityType; }

    public String getAddEntity5(){ return addEntity5; }

    public String getAddEntity6(){ return addEntity6; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getAddEntity3(){ return addEntity3; }

    public String getGlFlag(){ return glFlag; }

    public String getIfscCode(){ return ifscCode; }

    public String getAddEntity4(){ return addEntity4; }

    public String getAddEntity1(){ return addEntity1; }

    public String getSchemeCode(){ return schemeCode; }

    public String getAddEntity2(){ return addEntity2; }

    public String getPenalInterestFlag(){ return penalInterestFlag; }

    public java.sql.Timestamp getInstrmntDate(){ return instrmntDate; }

    public String getAddEntity9(){ return addEntity9; }

    public String getTdLiquidationFlag(){ return tdLiquidationFlag; }

    public String getAddEntity7(){ return addEntity7; }

    public Double getClrBalAmt(){ return clrBalAmt; }

    public String getAddEntity8(){ return addEntity8; }

    public Double getRate(){ return rate; }

    public java.sql.Timestamp getAcctClosedDate(){ return acctClosedDate; }

    public Double getTxnAmt(){ return txnAmt; }

    public String getTranCategory(){ return tranCategory; }

    public String getInstrmntNum(){ return instrmntNum; }

    public String getEntryUser(){ return entryUser; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getAcctSolId(){ return acctSolId; }

    public java.sql.Timestamp getReconId(){ return reconId; }

    public String getEventType(){ return eventType; }

    public String getRefCurrency(){ return refCurrency; }

    public String getContraBankCode(){ return contraBankCode; }

    public String getContraSolId(){ return contraSolId; }

    public java.sql.Timestamp getLastTranDate(){ return lastTranDate; }

    public String getTranParticular(){ return tranParticular; }

    public String getCustId(){ return custId; }

    public java.sql.Timestamp getAcctMaturityDate(){ return acctMaturityDate; }

    public String getTxnBrId(){ return txnBrId; }

    public String getTranType(){ return tranType; }

    public String getInstrumentType(){ return instrumentType; }

    public Double getRefTranAmt(){ return refTranAmt; }

    public java.sql.Timestamp getReActivationDate(){ return reActivationDate; }

    public String getStaffFlag(){ return staffFlag; }

    public String getAddEntity10(){ return addEntity10; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setContraCustid(String val){ this.contraCustid = val; }
    public void setTxnRefId(String val){ this.txnRefId = val; }
    public void setOriginIpAddr(String val){ this.originIpAddr = val; }
    public void setPstdUserId(String val){ this.pstdUserId = val; }
    public void setAcctSolCity(String val){ this.acctSolCity = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setValueDate(java.sql.Timestamp val){ this.valueDate = val; }
    public void setMnemonicCode(String val){ this.mnemonicCode = val; }
    public void setPrevBal(Double val){ this.prevBal = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setEntityId(String val){ this.entityId = val; }
    public void setContraSchmCode(String val){ this.contraSchmCode = val; }
    public void setEventSubType(String val){ this.eventSubType = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranCode(String val){ this.tranCode = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setContraGlFlag(String val){ this.contraGlFlag = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setDpCode(String val){ this.dpCode = val; }
    public void setAcctOwnership(String val){ this.acctOwnership = val; }
    public void setSchemeType(String val){ this.schemeType = val; }
    public void setTxnBrCity(String val){ this.txnBrCity = val; }
    public void setPstdFlg(String val){ this.pstdFlg = val; }
    public void setContraAcctId(String val){ this.contraAcctId = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setEntityType(String val){ this.entityType = val; }
    public void setAddEntity5(String val){ this.addEntity5 = val; }
    public void setAddEntity6(String val){ this.addEntity6 = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setAddEntity3(String val){ this.addEntity3 = val; }
    public void setGlFlag(String val){ this.glFlag = val; }
    public void setIfscCode(String val){ this.ifscCode = val; }
    public void setAddEntity4(String val){ this.addEntity4 = val; }
    public void setAddEntity1(String val){ this.addEntity1 = val; }
    public void setSchemeCode(String val){ this.schemeCode = val; }
    public void setAddEntity2(String val){ this.addEntity2 = val; }
    public void setPenalInterestFlag(String val){ this.penalInterestFlag = val; }
    public void setInstrmntDate(java.sql.Timestamp val){ this.instrmntDate = val; }
    public void setAddEntity9(String val){ this.addEntity9 = val; }
    public void setTdLiquidationFlag(String val){ this.tdLiquidationFlag = val; }
    public void setAddEntity7(String val){ this.addEntity7 = val; }
    public void setClrBalAmt(Double val){ this.clrBalAmt = val; }
    public void setAddEntity8(String val){ this.addEntity8 = val; }
    public void setRate(Double val){ this.rate = val; }
    public void setAcctClosedDate(java.sql.Timestamp val){ this.acctClosedDate = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setInstrmntNum(String val){ this.instrmntNum = val; }
    public void setEntryUser(String val){ this.entryUser = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setReconId(java.sql.Timestamp val){ this.reconId = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setRefCurrency(String val){ this.refCurrency = val; }
    public void setContraBankCode(String val){ this.contraBankCode = val; }
    public void setContraSolId(String val){ this.contraSolId = val; }
    public void setLastTranDate(java.sql.Timestamp val){ this.lastTranDate = val; }
    public void setTranParticular(String val){ this.tranParticular = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setAcctMaturityDate(java.sql.Timestamp val){ this.acctMaturityDate = val; }
    public void setTxnBrId(String val){ this.txnBrId = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setRefTranAmt(Double val){ this.refTranAmt = val; }
    public void setReActivationDate(java.sql.Timestamp val){ this.reActivationDate = val; }
    public void setStaffFlag(String val){ this.staffFlag = val; }
    public void setAddEntity10(String val){ this.addEntity10 = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_AccountTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();
        CxKeyHelper h = Hfdb.getCxKeyHelper();
        boolean flag = false ;
        String value = null;
        String custId = getCustId();
        String query = "SELECT \"custcategory\" FROM customer where \"hostCustId\" = '" + custId + "'";
        try {
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    flag = true;
                    value = rs.getString("custcategory");

                }

            }catch (SQLException se){
                se.printStackTrace();
            }

        }catch (Exception e){
           e.printStackTrace();
        }


        if(((StringUtils.isNullOrEmpty(getContraBankCode()))&&(flag=true))&&(StringUtils.isNullOrEmpty(getInstrmntNum()))) {

           String noncustomerKey=  h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.custId + txnAmt);
           wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        }
        if(((!StringUtils.isNullOrEmpty(getContraBankCode()))&&(StringUtils.isNullOrEmpty(getInstrmntNum())))) {
        String noncustomerKey=  h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.contraBankCode + contraAcctId );
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        }
        if (((!StringUtils.isNullOrEmpty(getInstrmntNum())))) {
                String noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.instrmntNum);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
            }




        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
       
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_AccountTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "AccountTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
