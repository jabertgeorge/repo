package clari5.custom.canara.data;

public class NFT_ChqReturn extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_CHECK_RETURN";
    private String event_type = "NFT_CheckReturn";

    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String HOST_ID;
    private String SYS_TIME;
    private String CHANNEL;
    private String ACCOUNT_ID;
    private String ACCT_OPEN_DATE;
    private String CHEQUE_NUMBER;
    private String TXN_AMT;
    private String REASON_CODE;
    private String REASON_DESC;
    private String RETURN_TYPE;
    private String SCHM_TYPE;
    private String SCHM_CODE;
    private String CUST_ID;
    private String CHEQ1;
    private String TRAN_DATE;
    private String CHEQUE_FLAG;


    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getCHEQUE_NUMBER() {
        return CHEQUE_NUMBER;
    }

    public void setCHEQUE_NUMBER(String CHEQUE_NUMBER) {
        this.CHEQUE_NUMBER = CHEQUE_NUMBER;
    }

    public String getTXN_AMT() {
        return TXN_AMT;
    }

    public void setTXN_AMT(String TXN_AMT) {
        this.TXN_AMT = TXN_AMT;
    }

    public String getREASON_CODE() {
        return REASON_CODE;
    }

    public void setREASON_CODE(String REASON_CODE) {
        this.REASON_CODE = REASON_CODE;
    }

    public String getREASON_DESC() {
        return REASON_DESC;
    }

    public void setREASON_DESC(String REASON_DESC) {
        this.REASON_DESC = REASON_DESC;
    }

    public String getRETURN_TYPE() {
        return RETURN_TYPE;
    }

    public void setRETURN_TYPE(String RETURN_TYPE) {
        this.RETURN_TYPE = RETURN_TYPE;
    }

    public String getSCHM_TYPE() {
        return SCHM_TYPE;
    }

    public void setSCHM_TYPE(String SCHM_TYPE) {
        this.SCHM_TYPE = SCHM_TYPE;
    }

    public String getSCHM_CODE() {
        return SCHM_CODE;
    }

    public void setSCHM_CODE(String SCHM_CODE) {
        this.SCHM_CODE = SCHM_CODE;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getCHEQ1() {
        return CHEQ1;
    }

    public void setCHEQ1(String CHEQ1) {
        this.CHEQ1 = CHEQ1;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getCHEQUE_FLAG() {
        return CHEQUE_FLAG;
    }

    public void setCHEQUE_FLAG(String CHEQUE_FLAG) {
        this.CHEQUE_FLAG = CHEQUE_FLAG;
    }
}
