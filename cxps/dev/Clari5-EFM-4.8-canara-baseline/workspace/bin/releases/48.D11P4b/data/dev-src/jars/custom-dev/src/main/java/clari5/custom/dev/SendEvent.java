package clari5.custom.dev;

import clari5.cxq.rest.MQ;
import clari5.rda.RDA;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;

import clari5.platform.util.ECClient;

public class SendEvent {

    private static CxpsLogger logger = CxpsLogger.getLogger(SendEvent.class);


    public String sendDecideEvent(JSONObject jsonObject)
    {

        try {
            logger.info("Json value for Decide is "+jsonObject.toString());
            RDA rda = new RDA();
            String response =  rda.process(jsonObject.toString());
            //System.out.println("The respone is " +response);
            return  response;
        }
        catch (Exception e){
            System.out.println("Excpetion");

        }
        return null;
    }

    public String sendEnqueueEvent(JSONObject jsonObject)
    {

        logger.info("Json Value for Enqueue "+jsonObject.toString());

        String msgBody = jsonObject.getString("msgBody");
        JSONObject jsonObject1 = new JSONObject(msgBody);
        String entityId = jsonObject1.getString("cust_card_id");
        logger.info("entity id "+entityId);
        //ECClient.configure(null);
        //boolean a = ECClient.enqueue("HOST", System.nanoTime() + "" + Thread.currentThread().getId(), jsonObject.toString());
        boolean a = MQ.writer.write("HOST", ((entityId != null) ? entityId : "NULLCARD"+System.nanoTime()), System.nanoTime() + "" + Thread.currentThread().getId(), System.nanoTime(), jsonObject.toString());
        if(a == true)
        {
           logger.info(" the enqueue status is "+a);
            return "SUCESS" ;
        }
        return  null;
    }
}
