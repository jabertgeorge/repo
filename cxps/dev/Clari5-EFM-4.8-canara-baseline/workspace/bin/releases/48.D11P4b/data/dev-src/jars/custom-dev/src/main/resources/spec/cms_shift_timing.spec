cxps.noesis.glossary.entity.cms_shift_timing{
	db-name = CMS_SHIFT_TIMING
	generate = false
	db_column_quoted = true
	tablespace = CXPS_USERS
	attributes = [
		{ name = SHIFT, column = SHIFT, type = "string:20", key:true}
		{ name = FROMHOUR,   column = FROMHOUR,    type = "string:20" }
		{ name = FROMMINUTE,   column = FROMMINUTE,    type = "string:20" }
		{ name = TOHOUR,   column = TOHOUR,    type = "string:20" }
        { name = TOMINUTE,   column = TOMINUTE,    type = "string:20" }
		         ]
}
