package clari5.custom.canara.data;

public class NFT_BrnAdvnc extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_BRANCHADVANCES";
    private String event_type = "NFT_Branchadvances";


    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String SYS_TIME;
    private String BRANCH_CODE;
    private String BRANCH_NAME;
    private String PREVIOUS_MONTH_ADVANCE_AMT;
    private String CURRENT_MONTH_ADVANCE_AMT;
    private String PREVIOUS_QUARTER_ADVANCE_AMT;
    private String CURRENT_QUARTER_ADVANCE_AMT;
    private String PREVIOUS_DAY_ADVANCE_AMT;
    private String CURRENT_DAY_ADVANCE_AMT;
    private String EVENT_DATE_TIME;

    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getBRANCH_CODE() {
        return BRANCH_CODE;
    }

    public void setBRANCH_CODE(String BRANCH_CODE) {
        this.BRANCH_CODE = BRANCH_CODE;
    }

    public String getBRANCH_NAME() {
        return BRANCH_NAME;
    }

    public void setBRANCH_NAME(String BRANCH_NAME) {
        this.BRANCH_NAME = BRANCH_NAME;
    }

    public String getPREVIOUS_MONTH_ADVANCE_AMT() {
        return PREVIOUS_MONTH_ADVANCE_AMT;
    }

    public void setPREVIOUS_MONTH_ADVANCE_AMT(String PREVIOUS_MONTH_ADVANCE_AMT) {
        this.PREVIOUS_MONTH_ADVANCE_AMT = PREVIOUS_MONTH_ADVANCE_AMT;
    }

    public String getCURRENT_MONTH_ADVANCE_AMT() {
        return CURRENT_MONTH_ADVANCE_AMT;
    }

    public void setCURRENT_MONTH_ADVANCE_AMT(String CURRENT_MONTH_ADVANCE_AMT) {
        this.CURRENT_MONTH_ADVANCE_AMT = CURRENT_MONTH_ADVANCE_AMT;
    }

    public String getPREVIOUS_QUARTER_ADVANCE_AMT() {
        return PREVIOUS_QUARTER_ADVANCE_AMT;
    }

    public void setPREVIOUS_QUARTER_ADVANCE_AMT(String PREVIOUS_QUARTER_ADVANCE_AMT) {
        this.PREVIOUS_QUARTER_ADVANCE_AMT = PREVIOUS_QUARTER_ADVANCE_AMT;
    }

    public String getCURRENT_QUARTER_ADVANCE_AMT() {
        return CURRENT_QUARTER_ADVANCE_AMT;
    }

    public void setCURRENT_QUARTER_ADVANCE_AMT(String CURRENT_QUARTER_ADVANCE_AMT) {
        this.CURRENT_QUARTER_ADVANCE_AMT = CURRENT_QUARTER_ADVANCE_AMT;
    }

    public String getPREVIOUS_DAY_ADVANCE_AMT() {
        return PREVIOUS_DAY_ADVANCE_AMT;
    }

    public void setPREVIOUS_DAY_ADVANCE_AMT(String PREVIOUS_DAY_ADVANCE_AMT) {
        this.PREVIOUS_DAY_ADVANCE_AMT = PREVIOUS_DAY_ADVANCE_AMT;
    }

    public String getCURRENT_DAY_ADVANCE_AMT() {
        return CURRENT_DAY_ADVANCE_AMT;
    }

    public void setCURRENT_DAY_ADVANCE_AMT(String CURRENT_DAY_ADVANCE_AMT) {
        this.CURRENT_DAY_ADVANCE_AMT = CURRENT_DAY_ADVANCE_AMT;
    }

    public String getEVENT_DATE_TIME() {
        return EVENT_DATE_TIME;
    }

    public void setEVENT_DATE_TIME(String EVENT_DATE_TIME) {
        this.EVENT_DATE_TIME = EVENT_DATE_TIME;
    }
}
