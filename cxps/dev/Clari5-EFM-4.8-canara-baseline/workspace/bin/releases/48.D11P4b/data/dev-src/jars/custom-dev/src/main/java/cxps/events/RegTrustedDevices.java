package cxps.events;
/*created by raghvendra patel
 * on 11/112/19
 */

/* put the this two line in IBLogin event in from() before  setDerivedValues();
    boolean isTxn=isPostTransaction();
    RegTrustedDevices.regTrustedDevice(json.getString("sys_time"),json.getString("user_id"),json.getString("cust_id").replaceAll("\\+",""),json.getString("ip_address"),json.getString("device_id"),json.getString("succ_fail_flg"),isTxn);
     */
import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.Rdbms;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegTrustedDevices{

    public static CxpsLogger logger = CxpsLogger.getLogger(RegTrustedDevices.class);
    public static void regTrustedDevice(String sys_time,String user_id,String cust_id,String ip_address,String device_id,String succ_fail_flg,boolean istxn){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        try {
            String strDate = formatter.format(date);
            if(!isRecordExist(user_id,device_id,sys_time,ip_address)&& istxn){

                if(succ_fail_flg.equals("T") && istxn){
                    System.out.println("Inside the if block");
                    String query = "INSERT INTO TRUST_DEV_TBL (SYS_TIME,USER_ID,CUST_ID,IP_ADDRESS,DEVICE_ID,DEL_FLAG, LAST_CHG_TS, RE_CRE_TS) VALUES (?,?,?,?,?,?,?,?)";
                    System.out.println("Inside the if block");
                    try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
                        ps.setString(1,sys_time);
                        ps.setString(2,user_id);
                        ps.setString(3,cust_id);
                        ps.setString(4,ip_address);
                        ps.setString(5,device_id);
                        ps.setString(6,"N");
                        ps.setString(7,strDate);
                        ps.setString(8,strDate);
                        int i=ps.executeUpdate();
                        con.commit();

                    }catch (SQLException e){
                        e.printStackTrace();
                        logger.info("Exception in try of regTrustedDevice()");
                    }

                }

            }


        }catch (Exception e){
            e.printStackTrace();
            logger.info("Exception in regTrustedDevice()");

        }

    }
    public static boolean isRecordExist(String userid,String deviceid,String systime,String ipaddress){


        try {
            String sql="UPDATE TRUST_DEV_TBL SET IP_ADDRESS=?, SYS_TIME=? WHERE USER_ID=? AND DEVICE_ID=?";
            //String sql="SELECT USER_ID,DEVICE_ID FROM TRUST_DEV_TBL WHERE USER_ID='"+userid+"' AND DEVICE_ID='"+deviceid+"'";
            try (Connection con = Rdbms.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1,ipaddress);
                ps.setString(2,systime);
                ps.setString(3,userid);
                ps.setString(4,deviceid);
                int i=ps.executeUpdate();
                con.commit();
                if(i>0) {
                    return true;
                }

            }catch (SQLException e){
                e.printStackTrace();

            }
        }catch (Exception e){
           e.printStackTrace();
        }
       return false;
    }

}