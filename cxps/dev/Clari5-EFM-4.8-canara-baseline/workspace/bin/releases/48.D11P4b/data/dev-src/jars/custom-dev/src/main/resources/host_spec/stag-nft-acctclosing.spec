clari5.hfdb.entity.nft_acctclosing{
  attributes = [

        {name = CL5_FLG,  type= "string:20"}
        {name = CREATED_ON,  type= timestamp}
        {name = UPDATED_ON,  type= timestamp}
        {name = sys_time, type = timestamp}
    	{name = host_id, type="string:20"}
        { name = ID, type="string:50", key = true}
    	{name = channel, type="string:20"}
    	{name = account_id, type="string:20"}
    	{name = acct_open_date, type=timestamp}
    	{name = loan_terms, type="string:20"}
    	{name = scheme_code, type="string:20"}
    	{name = interest_rate, type="string:20"}
    	{name = cust_id, type="string:20"}
    	{name = scheme_type, type="string:20"}
	{name = same_year_flg, type="string:2"}

        ]
}

