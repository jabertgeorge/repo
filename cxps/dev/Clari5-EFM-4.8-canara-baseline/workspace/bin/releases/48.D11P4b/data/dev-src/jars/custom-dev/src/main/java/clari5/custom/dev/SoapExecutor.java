package clari5.custom.dev;

import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class SoapExecutor extends Thread {

    public static CxpsLogger logger = CxpsLogger.getLogger(SoapServlet.class);
    public HttpServletRequest request;
    public String responseDecision;
    public String defaultResponse;


    public SoapExecutor(HttpServletRequest request){
        this.request = request;
    }

    public Optional<String> getResponseDecision() {
        return (responseDecision == null) ? Optional.empty() : Optional.of(responseDecision);
    }

    public String getDefaultResponse(){
        return defaultResponse;
    }

    @Override
    public void run(){

        String strMsg = "";

        MessageFactory messageFactory = null;
        InputStream inStream = null;
        SOAPMessage soapMessage = null;
        ByteArrayOutputStream out = null;
        //PrintWriter writer = null;

        try {
            messageFactory = MessageFactory.newInstance();
            inStream = request.getInputStream();
            soapMessage = messageFactory.createMessage(new MimeHeaders(), inStream);
            out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            strMsg = new String(out.toByteArray());
            //strMsg = out.toString();

            logger.info("Received xml from switch for RDE "+strMsg);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            if( strMsg.contains("FRMRQST"))
            {
                JSONObject jsonObject = new FormatJson().getJsonObject(new SaxParser().reqParser(removeXmlStringNamespaceAndPreamble(strMsg)));

                if( jsonObject != null) {

                    //just setting the default response in XML format to write...
                    defaultResponse = strMsg;
                    defaultResponse = defaultResponse.replaceAll("FRMRQST", "FRMRESP");
                    if(defaultResponse.contains("<TYP>0200</TYP>")){
                        defaultResponse = defaultResponse.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                    }
                    if (defaultResponse.contains("<RESP_CDE>")) {
                        int j = defaultResponse.indexOf("<RESP_CDE>");
                        String substring = defaultResponse.substring(j, j + 23);
                        defaultResponse = defaultResponse.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                    } else {
                        defaultResponse = defaultResponse.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                    }


                    SendEvent sendEvent = new SendEvent();
                    String decideresponse = sendEvent.sendDecideEvent(jsonObject);
                    logger.info(" response =>" + decideresponse);

                    strMsg = strMsg.replaceAll("FRMRQST", "FRMRESP");

                    if (decideresponse != null && decideresponse.equalsIgnoreCase("allow")) {
                        if (strMsg.contains("<TYP>0200</TYP>")) {
                            strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                            if (strMsg.contains("<RESP_CDE>")) {
                                int j = strMsg.indexOf("<RESP_CDE>");
                                String substring = strMsg.substring(j, j + 23);
                                strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                            } else {
                                strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                            }
                        } else {
                            logger.info("Invalid xml => <TYP> not present in xml 1");
                        }
                        logger.info("response => final xml for allow " + strMsg);
                        responseDecision = strMsg;

                    } else if (decideresponse != null && decideresponse.contains("|")) {
                        String arg[] = decideresponse.split("_");
                        if (strMsg.contains("<TYP>0200</TYP>")) {

                            strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                            if (strMsg.contains("<RESP_CDE>")) {
                                int j = strMsg.indexOf("<RESP_CDE>");
                                String substring = strMsg.substring(j, j + 23);
                                strMsg = strMsg.replaceAll(substring, "<RESP_CDE>05</RESP_CDE>");
                                strMsg = strMsg.replaceAll("<RULE_ID/>", "<RULE_ID>" + arg[1] + "</RULE_ID>");
                            } else {
                                strMsg = strMsg.replaceAll("<RULE_ID/>", "<RULE_ID>" + arg[1] + "</RULE_ID>");
                                strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>05</RESP_CDE><RULE_ID>" + arg[1] + "</RULE_ID>");
                            }
                        } else {
                            logger.info("Invalid xml => <TYP> not present in xml 2");
                        }

                        logger.info("response => final xml for decline " + strMsg);
                        responseDecision = strMsg;
                        /*writer = response.getWriter();
                        writer.append(strMsg);
                        writer.flush();*/
                    } else {

                        if (strMsg.contains("<TYP>0200</TYP>")) {
                            strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                            if (strMsg.contains("<RESP_CDE>")) {
                                int j = strMsg.indexOf("<RESP_CDE>");
                                String substring = strMsg.substring(j, j + 23);
                                strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                            } else {
                                strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                            }
                        } else {
                            logger.info("Invalid xml => <TYP> not present in xml 3");
                        }

                        logger.info("Invalid response on decide hence allowing");
                        responseDecision = strMsg;
                        /*writer = response.getWriter();
                        writer.append(strMsg);
                        writer.flush();*/
                    }
                    logger.info("End of the request=>" + sdf.format(new Date()));
                }
                else {
                    logger.info("json is invalid");
                    if (strMsg.contains("<TYP>0200</TYP>")) {
                        strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                        if (strMsg.contains("<RESP_CDE>")) {
                            int j = strMsg.indexOf("<RESP_CDE>");
                            String substring = strMsg.substring(j, j + 23);
                            strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                        } else {
                            strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                        }
                    } else {
                        logger.info("Invalid xml => <TYP> not present in xml 4");
                    }

                    responseDecision = strMsg;
                    /*writer = response.getWriter();
                    writer.append(strMsg);
                    writer.flush();*/
                }
            }
            else {
                logger.info("Invalid xml or xml is null");
                responseDecision = strMsg;
                /*writer = response.getWriter();
                writer.append(strMsg);
                writer.flush();*/
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("exception");
            if (strMsg.contains("<TYP>0200</TYP>")) {
                strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                strMsg = strMsg.replaceAll("FRMRQST", "FRMRESP");
                if (strMsg.contains("<RESP_CDE>")) {
                    int j = strMsg.indexOf("<RESP_CDE>");
                    String substring = strMsg.substring(j, j + 23);
                    strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                } else {
                    strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                }
            } else {
                logger.info("Invalid xml => <TYP> not present in xml 5");
            }

            responseDecision = strMsg;
            /*try {
                writer = response.getWriter();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            writer.append(strMsg);
            writer.flush();*/

        } finally {
            try {
                if( inStream != null) {
                    inStream.close();
                }
                if( out != null) {
                    out.close();
                }
                messageFactory = null;
                soapMessage = null;
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            // Retrieve different parts
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
            // Two ways to extract headers
            SOAPHeader soapHeader = soapEnvelope.getHeader();
            soapHeader = soapMessage.getSOAPHeader();
            // Two ways to extract body
            SOAPBody soapBody = soapEnvelope.getBody();
            soapBody = soapMessage.getSOAPBody();
            // To add some element
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            Name bodyName = soapFactory.createName("getEmployeeDetails", "ns1", "urn:MySoapServices");
            SOAPBodyElement purchaseLineItems = soapBody.addBodyElement(bodyName);
            Name childName = soapFactory.createName("param1");
            SOAPElement order = purchaseLineItems.addChildElement(childName);
            order.addTextNode("1016577");

        } catch (Exception e) {
            e.printStackTrace();
        }
             */
        }
    }


    public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
        return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
                replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
                .replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
                .replaceAll("(</)(\\w+:)(.*?>)", "$1$3");
        /* remove closing tags prefix */
    }
}
