// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_CheckDepositEventMapper extends EventMapper<NFT_CheckDepositEvent> {

public NFT_CheckDepositEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_CheckDepositEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_CheckDepositEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_CheckDepositEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_CheckDepositEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_CheckDepositEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_CheckDepositEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getCheq1());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getSchmCode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getUnusedLvs());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setDouble(i++, obj.getTxnAmt());
            preparedStatement.setString(i++, obj.getSchmType());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getChequeNumber());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_CHECK_DEPOSIT]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_CheckDepositEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_CheckDepositEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CHECK_DEPOSIT"));
        putList = new ArrayList<>();

        for (NFT_CheckDepositEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "CHEQ1",  obj.getCheq1());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "SCHM_CODE",  obj.getSchmCode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "UNUSED_LVS",  obj.getUnusedLvs());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "TXN_AMT", String.valueOf(obj.getTxnAmt()));
            p = this.insert(p, "SCHM_TYPE",  obj.getSchmType());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CHEQUE_NUMBER",  obj.getChequeNumber());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_CHECK_DEPOSIT"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_CHECK_DEPOSIT]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_CHECK_DEPOSIT]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_CheckDepositEvent obj = new NFT_CheckDepositEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setCheq1(rs.getString("CHEQ1"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setSchmCode(rs.getString("SCHM_CODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setUnusedLvs(rs.getString("UNUSED_LVS"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setTxnAmt(rs.getDouble("TXN_AMT"));
    obj.setSchmType(rs.getString("SCHM_TYPE"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setChequeNumber(rs.getString("CHEQUE_NUMBER"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_CHECK_DEPOSIT]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_CheckDepositEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_CheckDepositEvent> events;
 NFT_CheckDepositEvent obj = new NFT_CheckDepositEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CHECK_DEPOSIT"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_CheckDepositEvent obj = new NFT_CheckDepositEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setCheq1(getColumnValue(rs, "CHEQ1"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setSchmCode(getColumnValue(rs, "SCHM_CODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setUnusedLvs(getColumnValue(rs, "UNUSED_LVS"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setTxnAmt(EventHelper.toDouble(getColumnValue(rs, "TXN_AMT")));
            obj.setSchmType(getColumnValue(rs, "SCHM_TYPE"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setChequeNumber(getColumnValue(rs, "CHEQUE_NUMBER"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CHECK_DEPOSIT]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CHECK_DEPOSIT]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCT_OPEN_DATE\",\"CHEQ1\",\"ACCOUNT_ID\",\"SCHM_CODE\",\"CHANNEL\",\"SYS_TIME\",\"CUST_ID\",\"UNUSED_LVS\",\"TRAN_DATE\",\"TXN_AMT\",\"SCHM_TYPE\",\"HOST_ID\",\"CHEQUE_NUMBER\"" +
              " FROM EVENT_NFT_CHECK_DEPOSIT";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCT_OPEN_DATE],[CHEQ1],[ACCOUNT_ID],[SCHM_CODE],[CHANNEL],[SYS_TIME],[CUST_ID],[UNUSED_LVS],[TRAN_DATE],[TXN_AMT],[SCHM_TYPE],[HOST_ID],[CHEQUE_NUMBER]" +
              " FROM EVENT_NFT_CHECK_DEPOSIT";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCT_OPEN_DATE`,`CHEQ1`,`ACCOUNT_ID`,`SCHM_CODE`,`CHANNEL`,`SYS_TIME`,`CUST_ID`,`UNUSED_LVS`,`TRAN_DATE`,`TXN_AMT`,`SCHM_TYPE`,`HOST_ID`,`CHEQUE_NUMBER`" +
              " FROM EVENT_NFT_CHECK_DEPOSIT";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_CHECK_DEPOSIT (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCT_OPEN_DATE\",\"CHEQ1\",\"ACCOUNT_ID\",\"SCHM_CODE\",\"CHANNEL\",\"SYS_TIME\",\"CUST_ID\",\"UNUSED_LVS\",\"TRAN_DATE\",\"TXN_AMT\",\"SCHM_TYPE\",\"HOST_ID\",\"CHEQUE_NUMBER\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCT_OPEN_DATE],[CHEQ1],[ACCOUNT_ID],[SCHM_CODE],[CHANNEL],[SYS_TIME],[CUST_ID],[UNUSED_LVS],[TRAN_DATE],[TXN_AMT],[SCHM_TYPE],[HOST_ID],[CHEQUE_NUMBER]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCT_OPEN_DATE`,`CHEQ1`,`ACCOUNT_ID`,`SCHM_CODE`,`CHANNEL`,`SYS_TIME`,`CUST_ID`,`UNUSED_LVS`,`TRAN_DATE`,`TXN_AMT`,`SCHM_TYPE`,`HOST_ID`,`CHEQUE_NUMBER`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

