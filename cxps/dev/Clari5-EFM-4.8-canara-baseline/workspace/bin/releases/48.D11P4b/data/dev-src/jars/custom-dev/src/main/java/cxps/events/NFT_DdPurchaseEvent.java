// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_DD_PURCHASE", Schema="rice")
public class NFT_DdPurchaseEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String benBankCode;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String ddNum;
       @Field(size=20) public String benAccountId;
       @Field(size=20) public String pstdUserId;
       @Field public java.sql.Timestamp valueDate;
       @Field public java.sql.Timestamp ddIssueDate;
       @Field(size=10) public String mnemonicCode;
       @Field(size=20) public Double refTxnAmt;
       @Field(size=20) public Double ddIssueAmt;
       @Field(size=20) public String benSolId;
       @Field(size=2) public String hostId;
       @Field(size=20) public String tranCategory;
       @Field(size=20) public String acctSolId;
       @Field(size=20) public String schmCode;
       @Field(size=20) public String benName;
       @Field(size=20) public Double partTranSrlNum;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String ddIssueCustid;
       @Field(size=20) public String tranCrncyCode;
       @Field(size=20) public String tranCode;
       @Field(size=20) public String acctName;
       @Field(size=20) public String ddIssueAcct;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=10) public String txnBrId;
       @Field(size=20) public String ddPurName;
       @Field(size=20) public String schmType;
       @Field(size=20) public String acctOwnership;
       @Field(size=10) public String acctStatus;


    @JsonIgnore
    public ITable<NFT_DdPurchaseEvent> t = AEF.getITable(this);

	public NFT_DdPurchaseEvent(){}

    public NFT_DdPurchaseEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("DdPurchase");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBenBankCode(json.getString("ben_bank_code"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setDdNum(json.getString("dd_num"));
            setBenAccountId(json.getString("ben_account_id"));
            setPstdUserId(json.getString("pstd_user_id"));
            setValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setDdIssueDate(EventHelper.toTimestamp(json.getString("dd_issue_date")));
            setMnemonicCode(json.getString("mnemonic_code"));
            setRefTxnAmt(EventHelper.toDouble(json.getString("ref_txn_amt")));
            setDdIssueAmt(EventHelper.toDouble(json.getString("dd_issue_amt")));
            setBenSolId(json.getString("ben_sol_id"));
            setHostId(json.getString("host_id"));
            setTranCategory(json.getString("tran_category"));
            setAcctSolId(json.getString("acct_sol_id"));
            setSchmCode(json.getString("schm_code"));
            setBenName(json.getString("ben_name"));
            setPartTranSrlNum(EventHelper.toDouble(json.getString("part_tran_srl_num")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setDdIssueCustid(json.getString("dd_issue_custid"));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTranCode(json.getString("tran_code"));
            setAcctName(json.getString("acct_name"));
            setDdIssueAcct(json.getString("dd_issue_acct"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setTxnBrId(json.getString("txn_br_id"));
            setDdPurName(json.getString("dd_pur_name"));
            setSchmType(json.getString("schm_type"));
            setAcctOwnership(json.getString("acct_ownership"));
            setAcctStatus(json.getString("acct_status"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "DPR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getBenBankCode(){ return benBankCode; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getDdNum(){ return ddNum; }

    public String getBenAccountId(){ return benAccountId; }

    public String getPstdUserId(){ return pstdUserId; }

    public java.sql.Timestamp getValueDate(){ return valueDate; }

    public java.sql.Timestamp getDdIssueDate(){ return ddIssueDate; }

    public String getMnemonicCode(){ return mnemonicCode; }

    public Double getRefTxnAmt(){ return refTxnAmt; }

    public Double getDdIssueAmt(){ return ddIssueAmt; }

    public String getBenSolId(){ return benSolId; }

    public String getHostId(){ return hostId; }

    public String getTranCategory(){ return tranCategory; }

    public String getAcctSolId(){ return acctSolId; }

    public String getSchmCode(){ return schmCode; }

    public String getBenName(){ return benName; }

    public Double getPartTranSrlNum(){ return partTranSrlNum; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getDdIssueCustid(){ return ddIssueCustid; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getTranCode(){ return tranCode; }

    public String getAcctName(){ return acctName; }

    public String getDdIssueAcct(){ return ddIssueAcct; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getTxnBrId(){ return txnBrId; }

    public String getDdPurName(){ return ddPurName; }

    public String getSchmType(){ return schmType; }

    public String getAcctOwnership(){ return acctOwnership; }

    public String getAcctStatus(){ return acctStatus; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBenBankCode(String val){ this.benBankCode = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setDdNum(String val){ this.ddNum = val; }
    public void setBenAccountId(String val){ this.benAccountId = val; }
    public void setPstdUserId(String val){ this.pstdUserId = val; }
    public void setValueDate(java.sql.Timestamp val){ this.valueDate = val; }
    public void setDdIssueDate(java.sql.Timestamp val){ this.ddIssueDate = val; }
    public void setMnemonicCode(String val){ this.mnemonicCode = val; }
    public void setRefTxnAmt(Double val){ this.refTxnAmt = val; }
    public void setDdIssueAmt(Double val){ this.ddIssueAmt = val; }
    public void setBenSolId(String val){ this.benSolId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setSchmCode(String val){ this.schmCode = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setPartTranSrlNum(Double val){ this.partTranSrlNum = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setDdIssueCustid(String val){ this.ddIssueCustid = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranCode(String val){ this.tranCode = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setDdIssueAcct(String val){ this.ddIssueAcct = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setTxnBrId(String val){ this.txnBrId = val; }
    public void setDdPurName(String val){ this.ddPurName = val; }
    public void setSchmType(String val){ this.schmType = val; }
    public void setAcctOwnership(String val){ this.acctOwnership = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_DdPurchaseEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.ddNum);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_DdPurchase");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "DdPurchase");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}