package clari5.custom.dev;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Nft_PinRegen {

    public static JSONObject setNftPinregen(SaxPojo saxPojo)
    {
        if(saxPojo != null) {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("host_id","F");
            jsonObject.put("sys_time", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
            jsonObject.put("event_id","nft_regen" + System.nanoTime());
            jsonObject.put("channel",saxPojo.getChannel());
            jsonObject.put("user_id",saxPojo.getUserId());
            jsonObject.put("user_type",saxPojo.getUserType());
            jsonObject.put("cust_id",saxPojo.getCustId());
            jsonObject.put("device_id",saxPojo.getDeviceId());
            jsonObject.put("ip_address",saxPojo.getIpAddress());
            jsonObject.put("ip_country",saxPojo.getIpCountry());
            jsonObject.put("ip_city",saxPojo.getIpCity());
            jsonObject.put("succ_fail_flg",saxPojo.getSuccFailFlg());
            jsonObject.put("error_code",saxPojo.getErrorCode());
            jsonObject.put("error_desc",saxPojo.getErrorDesc());
            jsonObject.put("regen_type","DPIN");
            jsonObject.put("country_code", saxPojo.getCountryCode());
            jsonObject.put("mobile_no", saxPojo.getCustMobNo());
            jsonObject.put("tran_date",new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
            jsonObject.put("app_id",saxPojo.getDeviceId());
            jsonObject.put("cust_card_id",saxPojo.getCustCardId());
            jsonObject.put("mask_card_no",saxPojo.getMaskCardNo());
            jsonObject.put("addEntity1"," ");
            jsonObject.put("addEntity2"," ");
            jsonObject.put("addEntity3"," ");
            jsonObject.put("addEntity4"," ");
            jsonObject.put("addEntity5"," ");

            return jsonObject;
        }

        return null;
    }
}
