clari5.hfdb.entity.ft_accounttxn { 
attributes=[
{ name =CL5_FLG,  type= "string:20"}
{ name =CREATED_ON,  type= timestamp}
{ name =UPDATED_ON,  type= timestamp}
{ name = ID, type="string:50", key = true}
{ name = sys_time, type =  timestamp}
{ name = host_id, type =  "string:5"}
{ name = channel, type =  "string:10"}
{ name = account_id, type =  "string:50"}
{ name = schm_type, type =  "string:20"}
{ name = schm_code, type =  "string:20"}
{ name = cust_id, type =  "string:50"}
{ name = acct_name, type =  "string:100"}
{ name = acct_sol_id, type =  "string:20"}
{ name = acct_ownership, type =  "string:20"}
{ name = acct_open_date, type= timestamp}
{ name = avl_bal, type =  "number:20,3"}
{ name = clr_bal_amt, type =  "number:20,3"}
{ name = tran_type, type =  "string:20"}
{ name = tran_sub_type, type =  "string:20"}
{ name = part_tran_type, type =  "string:20"}
{ name = tran_date, type= timestamp}
{ name = tran_id, type =  "string:50"}
{ name = part_tran_srl_num, type =  "string:50"}
{ name = txn_amt, type =  "number:20,3"}
{ name = tran_crncy_code, type =  "string:20"}
{ name = ref_txn_amt, type =  "number:20,3"}
{ name = ref_tran_crncy, type =  "string:20"}
{ name = rate, type =  "number:20,3"}
{ name = tran_particular, type =  "string:1000"}
{ name = ip_address, type =  "string:20"}
{ name = pstd_flg, type =  "string:10"}
{ name = online_batch, type =  "string:20"}
{ name = pstd_user_id, type =  "string:20"}
{ name = entry_user, type =  "string:20"}
{ name = cust_card_id, type =  "string:20"}
{ name = device_id, type =  "string:20"}
{ name = contra_bank_code, type =  "string:20"}
{ name = contra_sol_id, type =  "string:20"}
{ name = tran_category, type =  "string:20"}
{ name = last_tran_date, type= timestamp}
{ name = branch_id, type =  "string:20"}
{ name = value_date, type= timestamp}
{ name = instrmnt_date, type =  timestamp}
{ name = instrmnt_num, type =  "string:30"}
{ name = prev_bal, type =  "number:20,3"}
{ name = acct_stat, type =  "string:20"}
{ name = txn_br_id, type =  "string:20"}
{ name = acct_sol_city, type =  "string:50"}
{ name = txn_br_city, type =  "string:50"}
{ name = instrmnt_type, type =  "string:20"}
{ name = mnemonic_code, type =  "string:20"}
{ name = reconid, type =  "string:50"}
{ name = contra_custid, type =  "string:20"}
{ name = contra_gl_flag, type =  "string:2"}
{ name = gl_flag, type =  "string:2"}
{ name = ifsc_code, type =  "string:20"}
{ name = penal_interest_flag, type =  "string:2"}
{ name = staff_flag, type =  "string:2"}
{ name = acct_closed_date, type =  timestamp}
{ name = acct_maturity_date, type =  timestamp}
{ name = re_activation_date, type =  timestamp}
{ name = tran_code, type =  "string:20"}
{ name = dp_code, type =  "string:20"}
{ name = td_liquidation_flg, type =  "string:2"}
{ name = contra_account_id, type =  "string:20"}
]
} 




