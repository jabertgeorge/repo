cxps.events.event.nft-loan{
  table-name : EVENT_NFT_LOAN
  event-mnemonic: LN
  workspaces : {
    ACCOUNT : "account_id"
    CUSTOMER : "cust_id"
}
  event-attributes : {
        host-id: {db:true ,raw_name : host_id ,type:"string:2"}
        cust-id: {db:true ,raw_name : cust_id ,type: "string:20"}
      	account-id: {db:true ,raw_name : account_id ,type: "string:20"}
        Scheme-Type: {db : true ,raw_name : scheme_type ,type : "string:20"}
        Scheme-Code: {db : true ,raw_name : scheme_code ,type : "string:20"}
        system-type: {db : true ,raw_name : system_type ,type : "string:20"}
       	Gl-Code: {db : true ,raw_name : gl_code ,type : "string:20"}
       	Loan-amt: {db : true ,raw_name : loan_amt ,type : "number:20,3"}
        Opened-date-time: {db : true ,raw_name : opened_date_time ,type : timestamp}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp}

}
}
