cxps.events.event.nft_acctclosing{
table-name : EVENT_NFT_ACCTCLOSING
  event-mnemonic: NAC
  workspaces : {
    ACCOUNT : account_id,
    CUSTOMER: cust-id
    }
  event-attributes : {
    	host-id:{type:"string:20",db:true,raw_name:host_id}
    	sys-time:{type:"timestamp",db:true,raw_name:sys_time}
    	channel:{type:"string:20",db:true,raw_name:channel}
    	account-id:{type:"string:20",db:true,raw_name:account_id}
    	acct-open-date:{type:"timestamp",db:true,raw_name:acct_open_date}
    	loan-terms:{type:"string:20",db:true,raw_name:loan_terms}
    	scheme-code:{type:"string:20",db:true,raw_name:scheme_code}
    	interest-rate:{type:"string:20",db:true,raw_name:interest_rate}
    	cust-id:{type:"string:20",db:true,raw_name:cust_id}
    	scheme-type:{type:"string:20",db:true,raw_name:scheme_type}
	same-year-flg:{type:"string:2",db:true,raw_name:same_year_flg,derivation:"""cxps.events.CustomFieldDerivator.flagset(this)"""}
        }
}

