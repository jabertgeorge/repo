// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_REMITTANCE", Schema="rice")
public class FT_RemittanceEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String remAdd1;
       @Field(size=20) public String remType;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String remAdd3;
       @Field(size=20) public String remAdd2;
       @Field(size=20) public String purposeCode;
       @Field(size=20) public Double usdAmount;
       @Field(size=20) public String benCity;
       @Field(size=20) public String benCntryCode;
       @Field(size=10) public String benAdd3;
       @Field(size=20) public String purposeDesc;
       @Field(size=10) public String benAdd2;
       @Field(size=20) public String remAcctNo;
       @Field(size=20) public Double inrAmt;
       @Field(size=20) public String remName;
       @Field(size=10) public String benAdd1;
       @Field(size=20) public String accountCatagory;
       @Field(size=20) public String benBic;
       @Field(size=2) public String hostId;
       @Field(size=20) public String tranRefNo;
       @Field(size=20) public Double tranAmt;
       @Field(size=20) public String remCustId;
       @Field(size=20) public String impExpAdvance;
       @Field(size=20) public String benName;
       @Field(size=20) public String systemType;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String branchId;
       @Field(size=20) public String benAcctNo;
       @Field(size=20) public String benCustId;
       @Field(size=20) public String remCntryCode;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String system;
       @Field public java.sql.Timestamp tranYear;
       @Field(size=20) public String tranCurr;
       @Field(size=20) public String remCity;
       @Field(size=20) public String remBic;


    @JsonIgnore
    public ITable<FT_RemittanceEvent> t = AEF.getITable(this);

	public FT_RemittanceEvent(){}

    public FT_RemittanceEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Remittance");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setRemAdd1(json.getString("rem_add1"));
            setRemType(json.getString("rem_type"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setRemAdd3(json.getString("rem_add3"));
            setRemAdd2(json.getString("rem_add2"));
            setPurposeCode(json.getString("purpose_code"));
            setUsdAmount(EventHelper.toDouble(json.getString("usd_amount")));
            setBenCity(json.getString("ben_city"));
            setBenCntryCode(json.getString("ben_cntry_code"));
            setBenAdd3(json.getString("ben_add3"));
            setPurposeDesc(json.getString("purpose_desc"));
            setBenAdd2(json.getString("ben_add2"));
            setRemAcctNo(json.getString("rem_acct_no"));
            setInrAmt(EventHelper.toDouble(json.getString("inr_amt")));
            setRemName(json.getString("rem_name"));
            setBenAdd1(json.getString("ben_add1"));
            setAccountCatagory(json.getString("account_catagory"));
            setBenBic(json.getString("ben_bic"));
            setHostId(json.getString("host_id"));
            setTranRefNo(json.getString("tran_ref_no"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setRemCustId(json.getString("rem_cust_id"));
            setImpExpAdvance(json.getString("imp_exp_advance"));
            setBenName(json.getString("ben_name"));
            setSystemType(json.getString("system_type"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setBenAcctNo(json.getString("ben_acct_no"));
            setBenCustId(json.getString("ben_cust_id"));
            setRemCntryCode(json.getString("rem_cntry_code"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setSystem(json.getString("system"));
            setTranYear(EventHelper.toTimestamp(json.getString("tran_year")));
            setTranCurr(json.getString("tran_curr"));
            setRemCity(json.getString("rem_city"));
            setRemBic(json.getString("rem_bic"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getRemAdd1(){ return remAdd1; }

    public String getRemType(){ return remType; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getRemAdd3(){ return remAdd3; }

    public String getRemAdd2(){ return remAdd2; }

    public String getPurposeCode(){ return purposeCode; }

    public Double getUsdAmount(){ return usdAmount; }

    public String getBenCity(){ return benCity; }

    public String getBenCntryCode(){ return benCntryCode; }

    public String getBenAdd3(){ return benAdd3; }

    public String getPurposeDesc(){ return purposeDesc; }

    public String getBenAdd2(){ return benAdd2; }

    public String getRemAcctNo(){ return remAcctNo; }

    public Double getInrAmt(){ return inrAmt; }

    public String getRemName(){ return remName; }

    public String getBenAdd1(){ return benAdd1; }

    public String getAccountCatagory(){ return accountCatagory; }

    public String getBenBic(){ return benBic; }

    public String getHostId(){ return hostId; }

    public String getTranRefNo(){ return tranRefNo; }

    public Double getTranAmt(){ return tranAmt; }

    public String getRemCustId(){ return remCustId; }

    public String getImpExpAdvance(){ return impExpAdvance; }

    public String getBenName(){ return benName; }

    public String getSystemType(){ return systemType; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getBenAcctNo(){ return benAcctNo; }

    public String getBenCustId(){ return benCustId; }

    public String getRemCntryCode(){ return remCntryCode; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getSystem(){ return system; }

    public java.sql.Timestamp getTranYear(){ return tranYear; }

    public String getTranCurr(){ return tranCurr; }

    public String getRemCity(){ return remCity; }

    public String getRemBic(){ return remBic; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setRemAdd1(String val){ this.remAdd1 = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setRemAdd3(String val){ this.remAdd3 = val; }
    public void setRemAdd2(String val){ this.remAdd2 = val; }
    public void setPurposeCode(String val){ this.purposeCode = val; }
    public void setUsdAmount(Double val){ this.usdAmount = val; }
    public void setBenCity(String val){ this.benCity = val; }
    public void setBenCntryCode(String val){ this.benCntryCode = val; }
    public void setBenAdd3(String val){ this.benAdd3 = val; }
    public void setPurposeDesc(String val){ this.purposeDesc = val; }
    public void setBenAdd2(String val){ this.benAdd2 = val; }
    public void setRemAcctNo(String val){ this.remAcctNo = val; }
    public void setInrAmt(Double val){ this.inrAmt = val; }
    public void setRemName(String val){ this.remName = val; }
    public void setBenAdd1(String val){ this.benAdd1 = val; }
    public void setAccountCatagory(String val){ this.accountCatagory = val; }
    public void setBenBic(String val){ this.benBic = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranRefNo(String val){ this.tranRefNo = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setRemCustId(String val){ this.remCustId = val; }
    public void setImpExpAdvance(String val){ this.impExpAdvance = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setSystemType(String val){ this.systemType = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setBenAcctNo(String val){ this.benAcctNo = val; }
    public void setBenCustId(String val){ this.benCustId = val; }
    public void setRemCntryCode(String val){ this.remCntryCode = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setSystem(String val){ this.system = val; }
    public void setTranYear(java.sql.Timestamp val){ this.tranYear = val; }
    public void setTranCurr(String val){ this.tranCurr = val; }
    public void setRemCity(String val){ this.remCity = val; }
    public void setRemBic(String val){ this.remBic = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_RemittanceEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

     String accountKey="";

if(this. remType.equalsIgnoreCase("I") )
       {

       accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.benAcctNo);
      
        }else if (this.remType.equalsIgnoreCase("O")){
         accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.remAcctNo);
          } 

      wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

String customerKey="";

if(this. remType.equalsIgnoreCase("I") )
       {

       accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.benCustId);
      
        }else if (this.remType.equalsIgnoreCase("O")){
         accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.remCustId);
          } 

      wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));


        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Remittance");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Remittance");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
