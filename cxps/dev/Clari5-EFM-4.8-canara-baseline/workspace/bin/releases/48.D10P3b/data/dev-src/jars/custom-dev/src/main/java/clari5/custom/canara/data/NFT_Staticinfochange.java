package clari5.custom.canara.data;

public class NFT_Staticinfochange extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "NFT_STATICINFOCHANGE";
    private String event_type = "NFT_Staticinfochange";
    private String HOST_ID;
    private String SYS_TIME;
    private String ID;
    private String CHANNEL;
    private String ACCOUNT_ID;
    private String CUST_ID;
    private String CUST_NAME;
    private String ENTITY_TYPE;
    private String INITIAL_VALUE;
    private String FINAL_VALUE;
    private String TRAN_DATE;

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getCUST_NAME() {
        return CUST_NAME;
    }

    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    public String getENTITY_TYPE() {
        return ENTITY_TYPE;
    }

    public void setENTITY_TYPE(String ENTITY_TYPE) {
        this.ENTITY_TYPE = ENTITY_TYPE;
    }

    public String getINITIAL_VALUE() {
        return INITIAL_VALUE;
    }

    public void setINITIAL_VALUE(String INITIAL_VALUE) {
        this.INITIAL_VALUE = INITIAL_VALUE;
    }

    public String getFINAL_VALUE() {
        return FINAL_VALUE;
    }

    public void setFINAL_VALUE(String FINAL_VALUE) {
        this.FINAL_VALUE = FINAL_VALUE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }
}
