package clari5.custom.cms;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;

import clari5.platform.applayer.Clari5;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.IncidentEvent;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jabert on 03/06/19.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    String project = null;
    int start = 0;

    protected static String current_shift = null;

    protected static ConcurrentHashMap<String, String> efmusermap = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> cbsusermap = new ConcurrentHashMap<String, String>();
    protected static ConcurrentHashMap<String, String> rdeusermap = new ConcurrentHashMap<String, String>();

    protected static ConcurrentHashMap<String, Integer> conCurrefmUsrMap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrcbsUsrMap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrrdeUsrMap = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, ConcurrentHashMap<String,String>> nestedshift=new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, String> shifitiming=new ConcurrentHashMap();


    PreparedStatement ps = null;
    Connection connection = null;
    ResultSet rs = null;
    protected static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);


    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public void startThread(int val){
        if(val==0) {
            ShiftCheck n = new ShiftCheck();
            Thread t1 = new Thread(n);
            t1.start();
            start++;
        }
    }

    public void EFMUser() {
        if (efmusermap.size() == 0) {
            getEFMUser();
            logger.info("efm user Map Loaded");
        }
    }

    public void CBSUser() {
        if (cbsusermap.size() == 0) {
            getCBSUser();
            logger.info("cbs user Map Loaded");
        }
    }

    public void RDEUser() {
        if (rdeusermap.size() == 0) {
            getRDEUser();
            logger.info("rde user Map Loaded");
        }
    }


    public String getIncidentAssignee(IncidentEvent incidentEvent) throws CmsException {

        String username = "";
        if (this.cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        //adding the shift in map
        //ShiftCheck.addShift();
        addShift();
        System.out.println("Incident assignee");
        EFMUser();
        CBSUser();
        RDEUser();

        logger.info("Starting Thread");
        startThread(start);

        String currshift = ShiftCheck.getCurrShift();
        System.out.println("Current shift in Custom cms formatter : [ "+currshift+ " ] ");


        project = incidentEvent.getProject();
        System.out.println("Project is : [ "+project+ " ] ");
        if (project.equalsIgnoreCase("efm")) {
            logger.info("Into project EFM");
            System.out.println("SHIFT CURRENT : [ "+currshift+ " ]");
            username = getEFMUserSync(currshift);
            logger.info("Returning efm user : [" + username + " ]");
            return username;
        } else if (project.equalsIgnoreCase("cbs")) {
            logger.info("Into project CBS");
            username = getCBSUserSync(currshift);
            logger.info("Returning cbs user : [" + username + " ]");
            return username;
        } else if (project.equalsIgnoreCase("rde")) {
            logger.info("Into project RDE");
            username = getRDEUserSync(currshift);
            logger.info("Returning rde user : [" + username + " ]");
            return username;
        } else {
            logger.info("Project not found : [ " + project + " ] : hence assigning to default as CXPS ");
            return "cxpsaml";
        }
    }

    @Override
    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException {

        logger.info("Into Custom PopulateIncident Json");
        CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
        CxJson json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();

        if (!isupdate && event.getEntityId().startsWith("P_F_")) {
            try {

                String eventJson = event.convertToJson(event);
                Hocon h = new Hocon(eventJson);
                CxJson wrapper = json.get("fields");

                Hocon conf = new Hocon();
                conf.loadFromContext("custom-fields.conf");

                Hocon mobEmailHocon = conf.get("custom-fields");
                HashMap<String, String> tableQueryMap = new HashMap<>();
                if (mobEmailHocon != null) {
                    List<String> keys = mobEmailHocon.getKeysAsList();
                    for (String key : keys) {
                        String val = mobEmailHocon.getString(key);
                        logger.info("the custom field for " + key + " : " + val);
                        tableQueryMap.put(key, val);
                    }
                }

                if (tableQueryMap != null && tableQueryMap.size() >= 2) {

                    List<String> mobEmList = getMobEmail(event.getEntityId().replace("P_F_", ""));
                    if (mobEmList != null && tableQueryMap != null) {
                        wrapper.put(tableQueryMap.get("mobileField"), mobEmList.get(0).toString());
                        wrapper.put(tableQueryMap.get("emailField"), mobEmList.get(1).toString());
                        return json.put("fields", wrapper);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return json;
            }
        }
        return json;
    }

    public List<String> getMobEmail(String cardNo) {
        String mobileNo = null;
        String emailId = null;
        List<String> mobEmList = new ArrayList<String>();
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource not available...");
        try {
            connection = rdbms.getCxConnection();
            String query = "select CUSTMOBILE,CUSTEMAIL from CUST_CARD_MASTER where" +
                    " CARD_NUMBER = '" + cardNo + "' and ROWNUM = 1";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                mobileNo = rs.getString("CUSTMOBILE");
                emailId = rs.getString("CUSTEMAIL");
            }
            logger.info("Card No : " + cardNo + " : mobile No : " + mobileNo + " : email Id : " + emailId);
            mobEmList.add((mobileNo != null) ? mobileNo : "Not Available");
            mobEmList.add((emailId != null) ? emailId : "Not Available");
            return mobEmList;
        } catch (SQLException e) {
            logger.info("class name [ customcmsformatter ] ---> method [getMobile]");
            e.printStackTrace();
            e.getCause();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                    ps.close();
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    public void getEFMUser() {

        try {
            String sql = "SELECT * FROM CMS_USER_SHIFTS WHERE PROJECT like '%EFM%' AND HLMARKFLAG='N'";
            connection = Rdbms.getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                logger.info("EFM users and shift time:" + efmusermap);

                String shift = rs.getString("SHIFT");
                String user = rs.getString("USERNAME");

                if (efmusermap.containsKey(shift)) {
                    System.out.println("Contains shift hence replacing : [ "+shift+ " ]");
                    String usr  = efmusermap.get(shift);
                    usr += ","+user;
                    System.out.println(usr);
                    efmusermap.replace(shift,usr);

                } else {
                    System.out.println("At first shift : [ "+shift+ " ]");
                    efmusermap.put(shift,user);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                    ps.close();
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void getRDEUser() {
        try {
            String sql = "SELECT * FROM CMS_USER_SHIFTS WHERE PROJECT like '%RDE%' AND HLMARKFLAG='N'";
            connection = Rdbms.getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                logger.info("RDE users and shift time:" + rdeusermap);

                String shift = rs.getString("SHIFT");
                String user = rs.getString("USERNAME");

                if (rdeusermap.containsKey(shift)) {
                    System.out.println("Contains shift hence replacing : [ "+shift+ " ]");
                    String usr  = rdeusermap.get(shift);
                    usr += ","+user;
                    System.out.println(usr);
                    rdeusermap.replace(shift,usr);

                } else {
                    System.out.println("At first shift : [ "+shift+ " ]");
                    rdeusermap.put(shift,user);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                    ps.close();
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void getCBSUser() {
        try {
            String sql = "SELECT * FROM CMS_USER_SHIFTS WHERE PROJECT like'%CBS%' AND HLMARKFLAG='N'";
            connection = Rdbms.getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                logger.info("CBS users and shift time:" + cbsusermap);

                String shift = rs.getString("SHIFT");
                String user = rs.getString("USERNAME");

                if (cbsusermap.containsKey(shift)) {
                    System.out.println("Contains shift hence replacing : [ "+shift+ " ]");
                    String usr  = cbsusermap.get(shift);
                    usr += ","+user;
                    System.out.println(usr);
                    cbsusermap.replace(shift,usr);

                } else {
                    System.out.println("At first shift : [ "+shift+ " ]");
                    cbsusermap.put(shift,user);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                    ps.close();
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    public static synchronized String getEFMUserSync(String shift) {
        String assignUser = null;
        for (String key : efmusermap.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    System.out.println("SHIFT PRESENT IN EFM USER MAP");
                    if (efmusermap.get(key).contains(",")) {
                        String arr[] = efmusermap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrefmUsrMap.get(key) == null || conCurrefmUsrMap.size() == 0) {
                            conCurrefmUsrMap.put(key, 0);
                            System.out.println("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrefmUsrMap.size() > 0 && conCurrefmUsrMap.get(key) < length) {
                            int val = conCurrefmUsrMap.get(key);
                            System.out.println("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrefmUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrefmUsrMap.get(key) >= length) {
                                System.out.println("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrefmUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrefmUsrMap.get(shift)];
                        System.out.println("User is : [ "+assignUser+ " ]");
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + efmusermap.get(key) + " ] ");
                        assignUser = efmusermap.get(key);
                        System.out.println("User hence no comma: [ "+assignUser+ " ]");
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in EFM user hence returning the first user");
                    return efmusermap.get(key).contains(",") ? efmusermap.get(key).split(",")[0] : efmusermap.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getRDEUserSync(String shift) {
        String assignUser = null;
        for (String key : rdeusermap.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN CBS USER MAP");
                    if (rdeusermap.get(key).contains(",")) {
                        String arr[] = rdeusermap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrrdeUsrMap.get(key) == null || conCurrrdeUsrMap.size() == 0) {
                            conCurrrdeUsrMap.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrrdeUsrMap.size() > 0 && conCurrcbsUsrMap.get(key) < length) {
                            int val = conCurrrdeUsrMap.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrrdeUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrrdeUsrMap.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrrdeUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrrdeUsrMap.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + rdeusermap.get(key) + " ] ");
                        assignUser = rdeusermap.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in RDE user hence returning the first user");
                    return rdeusermap.get(key).contains(",") ? rdeusermap.get(key).split(",")[0] : rdeusermap.get(key);

                }
            }
        }
        return assignUser;
    }

    public static synchronized String getCBSUserSync(String shift) {
        String assignUser = null;
        for (String key : cbsusermap.keySet()) {
            try {
                if (key.equalsIgnoreCase(shift)) {
                    logger.info("SHIFT PRESENT IN CBS USER MAP");
                    if (cbsusermap.get(key).contains(",")) {
                        String arr[] = cbsusermap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrcbsUsrMap.get(key) == null || conCurrcbsUsrMap.size() == 0) {
                            conCurrcbsUsrMap.put(key, 0);
                            logger.info("Initially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrcbsUsrMap.size() > 0 && conCurrcbsUsrMap.get(key) < length) {
                            int val = conCurrcbsUsrMap.get(key);
                            logger.info("Assigning User --- > [ " + arr[val + 1] + " ] ");
                            conCurrcbsUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrcbsUsrMap.get(key) >= length) {
                                logger.info("Returning first user again---> [" + arr[0] + " ] ");
                                conCurrcbsUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrcbsUsrMap.get(shift)];
                        return assignUser;
                    } else {
                        logger.info("return user since it don't have comma --->  [ " + cbsusermap.get(key) + " ] ");
                        assignUser = cbsusermap.get(key);
                        return assignUser;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(key.equalsIgnoreCase(shift));
                {
                    logger.info("Exception  in CBS user hence returning the first user");
                    return cbsusermap.get(key).contains(",") ? cbsusermap.get(key).split(",")[0] : cbsusermap.get(key);

                }
            }
        }
        return assignUser;
    }



    public  void addShift() {


        if (nestedshift.size() == 0) {
            logger.info("adding the shift in the map");
            try {
                logger.info("selecting from cms shift timing and loading in map");
                String sql = "SELECT * FROM CMS_SHIFT_TIMING ";
                connection = Rdbms.getConnection();
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();

                while (rs.next()) {
                  String shift = rs.getString("SHIFT");
                  String frmhour =  rs.getString("FROMHOUR") ;
                  String frmmin = rs.getString("FROMMINUTE");
                  String tohr = rs.getString("TOHOUR") ;
                  String tomin  =rs.getString("TOMINUTE");

                  shifitiming.put(frmhour+":"+frmmin,tohr+":"+tomin);
                  nestedshift.put(shift,shifitiming);
                  shifitiming = new ConcurrentHashMap<>();

                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.info("Exception in adding shift in table");
            } finally {
                if(connection != null) {
                    try {
                        connection.close();
                        ps.close();
                        rs.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }
}

