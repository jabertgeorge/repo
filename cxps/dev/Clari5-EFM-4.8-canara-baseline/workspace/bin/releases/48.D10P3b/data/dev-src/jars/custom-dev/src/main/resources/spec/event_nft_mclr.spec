cxps.events.event.nft_mclr{
table-name : EVENT_NFT_MCLR
  event-mnemonic: MR
  workspaces : {
    CUSTOMER : cust_id,
    ACCOUNT : account_id
    }
  event-attributes : {
        host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        channel: {db : true ,raw_name : channel ,type : "string:10"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        account_id: {db : true ,raw_name : account_id ,type : "string:20"}
        scheme_type: {db : true ,raw_name : scheme_type ,type : "string:50"}
        scheme_code: {db : true ,raw_name : scheme_code ,type : "string:20"}
        gl_code: {db : true ,raw_name : gl_code ,type : "string:20"}
        chq_start_no: {db : true ,raw_name : chq_start_no ,type : "string:20"}
        chq_end_no: {db : true ,raw_name : chq_end_no ,type : "string:10"}
        roi: {db : true ,raw_name : roi ,type : "number:20,3"}
        mclr: {db : true ,raw_name : mclr ,type : "number:20,3"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        ren_ext_new_flg: {db : true ,raw_name : ren_ext_new_flg ,type : "string:20"}
        }
}

