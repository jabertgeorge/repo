package clari5.custom.cms;

import cxps.apex.utils.CxpsLogger;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ShiftCheck extends  Thread {


    protected static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);

    public void run() {

        while (true) {
            logger.info("RUNNING THREAD FOR SHIFT");
            logger.info("Iterating Shift based on current time via Thread");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
            LocalDateTime now = LocalDateTime.now();
            String current_time = dtf.format(now);
           // System.out.println("Current time is : [" + current_time+ "]");

                try{
                    for (Map.Entry<String, ConcurrentHashMap<String, String>> shifts : CustomCmsFormatter.nestedshift.entrySet()) {
                        String shift = shifts.getKey();
                            for (Map.Entry<String, String> nameEntry : shifts.getValue().entrySet()) {
                                   String fromtime = nameEntry.getKey();
                                    String totime = nameEntry.getValue();
                                    logger.info(shift + " timing is from " + fromtime + " to " + totime);
                                     if (current_time.equalsIgnoreCase(fromtime)) {
                                         //System.out.println("Current time :[" +current_time+ " ] : shift time : [ "+fromtime+ " ] ");
                                         Thread.sleep(1*60*1000);
                                         System.out.println("Current time Equals Shift");
                                            CustomCmsFormatter.current_shift = shift;
                                         System.out.println("Current shift is : [ " + CustomCmsFormatter.current_shift+ "]");
                                            CustomCmsFormatter.efmusermap.clear();
                                            CustomCmsFormatter.conCurrefmUsrMap.clear();
                                         System.out.println("Cleared efm User Map");
                                            CustomCmsFormatter.cbsusermap.clear();
                                            CustomCmsFormatter.conCurrcbsUsrMap.clear();
                                         System.out.println("Cleared cbs User Map");
                                            CustomCmsFormatter.rdeusermap.clear();
                                            CustomCmsFormatter.conCurrrdeUsrMap.clear();
                                         System.out.println("Cleared rde User Map");
                                         System.out.println("Current Shift after Clearing all Map : [ "+CustomCmsFormatter.current_shift+ " ]");
                                     }
                                     logger.info(shift + " : timing is from [ " + fromtime + " ] to [ " + totime+ " ]");
                             }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info("Exception in RUNNING THREAD FOR SHIFT");
               }
        }
    }


    public static String getCurrShift()
    {
        try{
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
            LocalDateTime now = LocalDateTime.now();
            String current_time = dtf.format(now);
            System.out.println("Current time is : [" + current_time+ "]");
            for (Map.Entry<String, ConcurrentHashMap<String, String>> shifts : CustomCmsFormatter.nestedshift.entrySet()) {
                String shift = shifts.getKey();
                for (Map.Entry<String, String> nameEntry : shifts.getValue().entrySet()) {
                    String fromtime = nameEntry.getKey();
                    String totime = nameEntry.getValue();
                    logger.info(shift + " timing is from " + fromtime + " to " + totime);

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

                        Date d1 = sdf.parse(fromtime);
                        Date d2 = sdf.parse(totime);
                        Date cr = sdf.parse(current_time);

                        if(d1.getTime() < d2.getTime()) {
                            if (cr.getTime() >= d1.getTime() && cr.getTime() <= d2.getTime()) {
                                System.out.println("Current time : [ " + cr.getTime() + " ] ");
                                System.out.println("From time : [ " + d1.getTime() + " ] ");
                                System.out.println("To time : [ " + d2.getTime() + " ] ");
                                System.out.println("current shift : [ " + shift + " ] ");
                                return shift;
                            }
                        }
                        if(d1.getTime() > d2.getTime())
                        {
                            logger.info("Pretending that shift lies between two days");
                            if(cr.getTime()>=d1.getTime() || cr.getTime()<=d2.getTime())
                            {
                                return shift;
                            }
                        }

                    }
                }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

}

