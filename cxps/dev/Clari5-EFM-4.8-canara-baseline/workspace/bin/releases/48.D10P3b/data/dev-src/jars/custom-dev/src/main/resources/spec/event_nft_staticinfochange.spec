cxps.events.event.nft_staticinfochange{
table-name : EVENT_NFT_STATICINFOCHANGE
  event-mnemonic: NS
  workspaces : {
    ACCOUNT : account_id,
    CUSTOMER : cust_id
    }
  event-attributes : {
        host_id:{db : true ,raw_name : host_id, type :  "string:20"}
        channel: {db : true ,raw_name : channel ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        account_id: {db : true ,raw_name : account_id ,type : "string:20"}
        entity_type: {db : true ,raw_name : entity_type ,type : "string:50"}
        cust_name: {db : true ,raw_name : cust_name ,type : "string:20"}
        initial_value: {db : true ,raw_name : initial_value ,type : "string:20"}
        final_value: {db : true ,raw_name : final_value ,type : "string:20"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        tran_date: {db : true ,raw_name : tran_date ,type : timestamp}

        }
}
