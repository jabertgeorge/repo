package clari5.custom.shiftuser.rest;


/*created by vignesh
 * on 15/11/19
 */

import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.Rdbms;
import groovy.sql.Sql;
import org.json.JSONArray;
import org.json.JSONObject;
import org.stringtemplate.v4.ST;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.util.ArrayList;


@Path("user")
public class ResponseUser {
    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(ResponseUser.class);


    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @POST
    @Path("/addUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean addUser(@QueryParam("user") String userDetails) {
        //insert to table dont forget to check  if already exists.
        int i = 0;
        System.out.println("Userdetails : [ " + userDetails + " ]");
        String ar[] = userDetails.split(":");
        System.out.println("array :" + ar.toString());
        try {
            String query = "INSERT INTO CMS_USER_SHIFTS (USERNAME,SHIFT,PROJECT,HLMARKFLAG) VALUES (?,?,?,?)";
            con = Rdbms.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, ar[0]);
            ps.setString(2, ar[1]);
            ps.setString(3, ar[2]);
            ps.setString(4, "N");
            i = ps.executeUpdate();
            con.commit();
            System.out.println("inserted: [ " + i + "]");
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User already exists");
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (i == 0) {
            return false;
        } else {
            return true;
        }
    }


    @POST
    @Path("/updateUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String updateUser(@QueryParam("upuser") String userhmark) {
        //Update user with only Holiday Marking for the user
        int i = 0;

        String arr[] = userhmark.split(":");
        cxpsLogger.info("arr[]" + arr);
        try {
            String sql = "UPDATE CMS_USER_SHIFTS SET HLMARKFLAG =? WHERE USERNAME = ?";
            con = Rdbms.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, arr[0]);
            ps.setString(2, arr[1]);
            i = ps.executeUpdate();
            con.commit();
            System.out.println("inserted: [ " + i + "]");
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User has already marked as" + i);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (i == 0) {
            return "false";
        } else {
            return "true";
        }
    }


    @POST
    @Path("/deleteUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteUser(@QueryParam("deluser") String deluser) {
        //Remove the user from the table with id or username
        int i = 0;
        System.out.println("Userdetails : [ " + deluser + " ]");
        try {
            String sql = "DELETE FROM CMS_USER_SHIFTS WHERE USERNAME=?";
            con = Rdbms.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, deluser);
            i = ps.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("User cannot delete ");
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (i == 0) {
            return "false";
        } else {
            return "true";
        }
    }


    @GET
    @Path("/getCl5User")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getCl5User() {
        JSONArray userjson = new JSONArray();
        String user = null;
        try {
            String sql = "SELECT  DISTINCT USER_ID  FROM CL5_USER_TBL";
            con = Rdbms.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                user = rs.getString("user");
                userjson.put(user);
                cxpsLogger.info("User: [" + user + "]");
            }
            con.close();
            ps.close();
            rs.close();
            return userjson;
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- Not getting user");
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return userjson;
    }


    @GET
    @Path("/getShiftUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getShiftUser() {

        String shift = "";
        String username = "";
        String hlmrflg = "";
        String project="";

        JSONObject shiftjsonobject = new JSONObject();

        try {

            String sql = "SELECT SHIFT,USERNAME,HLMARKFLAG FROM CMS_USER_SHIFTS";
            con = Rdbms.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                shift = rs.getString("SHIFT");
                username = rs.getString("USERNAME");
                hlmrflg = rs.getString("HLMARKFLAG");
                //project = rs.getString("PROJECT");

                JSONObject userjsonobject = new JSONObject();
                //JSONObject projectjson = new JSONObject();
                JSONObject mrkjsonobject = new JSONObject();
                JSONArray array = new JSONArray();

                if (shiftjsonobject.has(shift)) {
                    JSONObject jsonObjectappend = shiftjsonobject.getJSONObject(shift);
                    mrkjsonobject.put("hmark", hlmrflg);
                    shiftjsonobject.remove(shift);
                    jsonObjectappend.append(username, mrkjsonobject);
                    shiftjsonobject.put(shift, jsonObjectappend);
                    System.out.println("Appended Json : [ " + shiftjsonobject + " ]");
                } else {
                    mrkjsonobject.put("hmark", hlmrflg);
                    array.put(mrkjsonobject);
                    userjsonobject.put(username, array);
                    shiftjsonobject.put(shift, userjsonobject);
                }
            }
            cxpsLogger.info("Json object for sending to UI : [ " + shiftjsonobject + " ]");
            con.close();
            ps.close();
            rs.close();
            return shiftjsonobject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception :- coming!! Kindly check.");
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return shiftjsonobject.toString();

    }


    @GET
    @Path("/getShift")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONArray getShift() {
        String sh = "";
        JSONArray shift = new JSONArray();
        try {
            String querry = "SELECT DISTINCT SHIFT FROM CMS_SHIFT_TIMING";
            con = Rdbms.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            while (rs.next()) {
                sh = rs.getString("SHIFT");
                shift.put(sh);
                cxpsLogger.info("User: [" + shift + "]");
            }
            con.close();
            ps.close();
            rs.close();
            return shift;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return shift;
        }


    }


    @POST
    @Path("/updatetime")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String updatetime(@QueryParam("uptime") String shifts) {
        int i = 0;
        try {
            con = Rdbms.getConnection();
            System.out.println("Shifts : [ " + shifts + " ]");
            String shift[] = shifts.split(",");
            for (String currShift : shift) {
                String time[] = currShift.split(":");
                cxpsLogger.info("time[]" + time);
                String querry = "UPDATE CMS_SHIFT_TIMING SET FROMHOUR = ?,TOMINUTE = ?,FROMMINUTE = ?,TOHOUR = ? WHERE SHIFT=?";
                ps = con.prepareStatement(querry);
                ps.setString(1, time[0]);
                ps.setString(2, time[1]);
                ps.setString(3, time[2]);
                ps.setString(4, time[3]);
                ps.setString(5, time[4]);
                i = ps.executeUpdate();
                System.out.println("Added : [ " + i + "]");
            }
            con.commit();
            cxpsLogger.info("Committed sucessfully");
        } catch (SQLException e) {
            e.printStackTrace();
            cxpsLogger.info("Unable to update Shift timings " + i);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (i == 0) {
            return "false";
        } else {
            return "true";
        }

    }

    @POST
    @Path("/showShiftTime")
    @Produces(MediaType.TEXT_PLAIN)
        public String showShiftTime() {
        String shift = null;
        String fhour = null;
        String fmin = null;
        String thour = null;
        String tmin = null;
        ArrayList list = new ArrayList<String>();
        try {
            con = Rdbms.getConnection();
            String sql = "SELECT * FROM CMS_SHIFT_TIMING ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            //System.out.println("SHIFT"+"\t"+"FROMHOUR"+"\t"+"TOMINUTE"+"\t"+"FROMMINUTE"+"\t"+"TOHOUR");
            while (rs.next()) {
                shift = rs.getString("SHIFT");
                fhour = rs.getString("FROMHOUR");
                fmin = rs.getString("TOMINUTE");
                thour = rs.getString("FROMMINUTE");
                tmin = rs.getString("TOHOUR");
                //System.out.println(""+rs.getString("SHIFT")+""+rs.getString("FROMHOUR")+""+rs.getString("TOMINUTE")+""+rs.getString("FROMMINUTE")+""+rs.getString("TOHOUR"));
                list.add(shift + ":" + "" + fhour + ":" + "" + tmin + ":" + "" + fmin + ":" + "" + thour);
            }
            System.out.println("list: [ " + list + "]");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return list.toString();

    }

}
    



