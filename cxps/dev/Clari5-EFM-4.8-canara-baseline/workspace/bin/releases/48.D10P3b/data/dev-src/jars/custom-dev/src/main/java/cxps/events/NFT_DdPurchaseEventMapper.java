// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_DdPurchaseEventMapper extends EventMapper<NFT_DdPurchaseEvent> {

public NFT_DdPurchaseEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_DdPurchaseEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_DdPurchaseEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_DdPurchaseEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_DdPurchaseEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_DdPurchaseEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_DdPurchaseEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getBenBankCode());
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setDouble(i++, obj.getDdNum());
            preparedStatement.setString(i++, obj.getBenAccountId());
            preparedStatement.setString(i++, obj.getPstdUserId());
            preparedStatement.setTimestamp(i++, obj.getValueDate());
            preparedStatement.setTimestamp(i++, obj.getDdIssueDate());
            preparedStatement.setString(i++, obj.getMnemonicCode());
            preparedStatement.setDouble(i++, obj.getRefTxnAmt());
            preparedStatement.setDouble(i++, obj.getDdIssueAmt());
            preparedStatement.setString(i++, obj.getBenSolId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getTranCategory());
            preparedStatement.setString(i++, obj.getAcctSolId());
            preparedStatement.setString(i++, obj.getSchmCode());
            preparedStatement.setString(i++, obj.getBenName());
            preparedStatement.setDouble(i++, obj.getPartTranSrlNum());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getDdIssueCustid());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getTranCode());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getDdIssueAcct());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getTxnBrId());
            preparedStatement.setString(i++, obj.getDdPurName());
            preparedStatement.setString(i++, obj.getSchmType());
            preparedStatement.setString(i++, obj.getAcctOwnership());
            preparedStatement.setString(i++, obj.getAcctStatus());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_DD_PURCHASE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_DdPurchaseEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_DdPurchaseEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DD_PURCHASE"));
        putList = new ArrayList<>();

        for (NFT_DdPurchaseEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "BEN_BANK_CODE",  obj.getBenBankCode());
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "DD_NUM", String.valueOf(obj.getDdNum()));
            p = this.insert(p, "BEN_ACCOUNT_ID",  obj.getBenAccountId());
            p = this.insert(p, "PSTD_USER_ID",  obj.getPstdUserId());
            p = this.insert(p, "VALUE_DATE", String.valueOf(obj.getValueDate()));
            p = this.insert(p, "DD_ISSUE_DATE", String.valueOf(obj.getDdIssueDate()));
            p = this.insert(p, "MNEMONIC_CODE",  obj.getMnemonicCode());
            p = this.insert(p, "REF_TXN_AMT", String.valueOf(obj.getRefTxnAmt()));
            p = this.insert(p, "DD_ISSUE_AMT", String.valueOf(obj.getDdIssueAmt()));
            p = this.insert(p, "BEN_SOL_ID",  obj.getBenSolId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "TRAN_CATEGORY",  obj.getTranCategory());
            p = this.insert(p, "ACCT_SOL_ID",  obj.getAcctSolId());
            p = this.insert(p, "SCHM_CODE",  obj.getSchmCode());
            p = this.insert(p, "BEN_NAME",  obj.getBenName());
            p = this.insert(p, "PART_TRAN_SRL_NUM", String.valueOf(obj.getPartTranSrlNum()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "DD_ISSUE_CUSTID",  obj.getDdIssueCustid());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_CODE",  obj.getTranCode());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "DD_ISSUE_ACCT",  obj.getDdIssueAcct());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "TXN_BR_ID",  obj.getTxnBrId());
            p = this.insert(p, "DD_PUR_NAME",  obj.getDdPurName());
            p = this.insert(p, "SCHM_TYPE",  obj.getSchmType());
            p = this.insert(p, "ACCT_OWNERSHIP",  obj.getAcctOwnership());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_DD_PURCHASE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_DD_PURCHASE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_DD_PURCHASE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_DdPurchaseEvent obj = new NFT_DdPurchaseEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setBenBankCode(rs.getString("BEN_BANK_CODE"));
    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setDdNum(rs.getDouble("DD_NUM"));
    obj.setBenAccountId(rs.getString("BEN_ACCOUNT_ID"));
    obj.setPstdUserId(rs.getString("PSTD_USER_ID"));
    obj.setValueDate(rs.getTimestamp("VALUE_DATE"));
    obj.setDdIssueDate(rs.getTimestamp("DD_ISSUE_DATE"));
    obj.setMnemonicCode(rs.getString("MNEMONIC_CODE"));
    obj.setRefTxnAmt(rs.getDouble("REF_TXN_AMT"));
    obj.setDdIssueAmt(rs.getDouble("DD_ISSUE_AMT"));
    obj.setBenSolId(rs.getString("BEN_SOL_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setTranCategory(rs.getString("TRAN_CATEGORY"));
    obj.setAcctSolId(rs.getString("ACCT_SOL_ID"));
    obj.setSchmCode(rs.getString("SCHM_CODE"));
    obj.setBenName(rs.getString("BEN_NAME"));
    obj.setPartTranSrlNum(rs.getDouble("PART_TRAN_SRL_NUM"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setDdIssueCustid(rs.getString("DD_ISSUE_CUSTID"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranCode(rs.getString("TRAN_CODE"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setDdIssueAcct(rs.getString("DD_ISSUE_ACCT"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setTxnBrId(rs.getString("TXN_BR_ID"));
    obj.setDdPurName(rs.getString("DD_PUR_NAME"));
    obj.setSchmType(rs.getString("SCHM_TYPE"));
    obj.setAcctOwnership(rs.getString("ACCT_OWNERSHIP"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_DD_PURCHASE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_DdPurchaseEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_DdPurchaseEvent> events;
 NFT_DdPurchaseEvent obj = new NFT_DdPurchaseEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DD_PURCHASE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_DdPurchaseEvent obj = new NFT_DdPurchaseEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setBenBankCode(getColumnValue(rs, "BEN_BANK_CODE"));
            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setDdNum(EventHelper.toDouble(getColumnValue(rs, "DD_NUM")));
            obj.setBenAccountId(getColumnValue(rs, "BEN_ACCOUNT_ID"));
            obj.setPstdUserId(getColumnValue(rs, "PSTD_USER_ID"));
            obj.setValueDate(EventHelper.toTimestamp(getColumnValue(rs, "VALUE_DATE")));
            obj.setDdIssueDate(EventHelper.toTimestamp(getColumnValue(rs, "DD_ISSUE_DATE")));
            obj.setMnemonicCode(getColumnValue(rs, "MNEMONIC_CODE"));
            obj.setRefTxnAmt(EventHelper.toDouble(getColumnValue(rs, "REF_TXN_AMT")));
            obj.setDdIssueAmt(EventHelper.toDouble(getColumnValue(rs, "DD_ISSUE_AMT")));
            obj.setBenSolId(getColumnValue(rs, "BEN_SOL_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setTranCategory(getColumnValue(rs, "TRAN_CATEGORY"));
            obj.setAcctSolId(getColumnValue(rs, "ACCT_SOL_ID"));
            obj.setSchmCode(getColumnValue(rs, "SCHM_CODE"));
            obj.setBenName(getColumnValue(rs, "BEN_NAME"));
            obj.setPartTranSrlNum(EventHelper.toDouble(getColumnValue(rs, "PART_TRAN_SRL_NUM")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setDdIssueCustid(getColumnValue(rs, "DD_ISSUE_CUSTID"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranCode(getColumnValue(rs, "TRAN_CODE"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setDdIssueAcct(getColumnValue(rs, "DD_ISSUE_ACCT"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setTxnBrId(getColumnValue(rs, "TXN_BR_ID"));
            obj.setDdPurName(getColumnValue(rs, "DD_PUR_NAME"));
            obj.setSchmType(getColumnValue(rs, "SCHM_TYPE"));
            obj.setAcctOwnership(getColumnValue(rs, "ACCT_OWNERSHIP"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DD_PURCHASE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DD_PURCHASE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"BEN_BANK_CODE\",\"ACCT_OPEN_DATE\",\"DD_NUM\",\"BEN_ACCOUNT_ID\",\"PSTD_USER_ID\",\"VALUE_DATE\",\"DD_ISSUE_DATE\",\"MNEMONIC_CODE\",\"REF_TXN_AMT\",\"DD_ISSUE_AMT\",\"BEN_SOL_ID\",\"HOST_ID\",\"TRAN_CATEGORY\",\"ACCT_SOL_ID\",\"SCHM_CODE\",\"BEN_NAME\",\"PART_TRAN_SRL_NUM\",\"SYS_TIME\",\"DD_ISSUE_CUSTID\",\"TRAN_CRNCY_CODE\",\"TRAN_CODE\",\"ACCT_NAME\",\"DD_ISSUE_ACCT\",\"TRAN_DATE\",\"TXN_BR_ID\",\"DD_PUR_NAME\",\"SCHM_TYPE\",\"ACCT_OWNERSHIP\",\"ACCT_STATUS\"" +
              " FROM EVENT_NFT_DD_PURCHASE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [BEN_BANK_CODE],[ACCT_OPEN_DATE],[DD_NUM],[BEN_ACCOUNT_ID],[PSTD_USER_ID],[VALUE_DATE],[DD_ISSUE_DATE],[MNEMONIC_CODE],[REF_TXN_AMT],[DD_ISSUE_AMT],[BEN_SOL_ID],[HOST_ID],[TRAN_CATEGORY],[ACCT_SOL_ID],[SCHM_CODE],[BEN_NAME],[PART_TRAN_SRL_NUM],[SYS_TIME],[DD_ISSUE_CUSTID],[TRAN_CRNCY_CODE],[TRAN_CODE],[ACCT_NAME],[DD_ISSUE_ACCT],[TRAN_DATE],[TXN_BR_ID],[DD_PUR_NAME],[SCHM_TYPE],[ACCT_OWNERSHIP],[ACCT_STATUS]" +
              " FROM EVENT_NFT_DD_PURCHASE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`BEN_BANK_CODE`,`ACCT_OPEN_DATE`,`DD_NUM`,`BEN_ACCOUNT_ID`,`PSTD_USER_ID`,`VALUE_DATE`,`DD_ISSUE_DATE`,`MNEMONIC_CODE`,`REF_TXN_AMT`,`DD_ISSUE_AMT`,`BEN_SOL_ID`,`HOST_ID`,`TRAN_CATEGORY`,`ACCT_SOL_ID`,`SCHM_CODE`,`BEN_NAME`,`PART_TRAN_SRL_NUM`,`SYS_TIME`,`DD_ISSUE_CUSTID`,`TRAN_CRNCY_CODE`,`TRAN_CODE`,`ACCT_NAME`,`DD_ISSUE_ACCT`,`TRAN_DATE`,`TXN_BR_ID`,`DD_PUR_NAME`,`SCHM_TYPE`,`ACCT_OWNERSHIP`,`ACCT_STATUS`" +
              " FROM EVENT_NFT_DD_PURCHASE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_DD_PURCHASE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"BEN_BANK_CODE\",\"ACCT_OPEN_DATE\",\"DD_NUM\",\"BEN_ACCOUNT_ID\",\"PSTD_USER_ID\",\"VALUE_DATE\",\"DD_ISSUE_DATE\",\"MNEMONIC_CODE\",\"REF_TXN_AMT\",\"DD_ISSUE_AMT\",\"BEN_SOL_ID\",\"HOST_ID\",\"TRAN_CATEGORY\",\"ACCT_SOL_ID\",\"SCHM_CODE\",\"BEN_NAME\",\"PART_TRAN_SRL_NUM\",\"SYS_TIME\",\"DD_ISSUE_CUSTID\",\"TRAN_CRNCY_CODE\",\"TRAN_CODE\",\"ACCT_NAME\",\"DD_ISSUE_ACCT\",\"TRAN_DATE\",\"TXN_BR_ID\",\"DD_PUR_NAME\",\"SCHM_TYPE\",\"ACCT_OWNERSHIP\",\"ACCT_STATUS\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[BEN_BANK_CODE],[ACCT_OPEN_DATE],[DD_NUM],[BEN_ACCOUNT_ID],[PSTD_USER_ID],[VALUE_DATE],[DD_ISSUE_DATE],[MNEMONIC_CODE],[REF_TXN_AMT],[DD_ISSUE_AMT],[BEN_SOL_ID],[HOST_ID],[TRAN_CATEGORY],[ACCT_SOL_ID],[SCHM_CODE],[BEN_NAME],[PART_TRAN_SRL_NUM],[SYS_TIME],[DD_ISSUE_CUSTID],[TRAN_CRNCY_CODE],[TRAN_CODE],[ACCT_NAME],[DD_ISSUE_ACCT],[TRAN_DATE],[TXN_BR_ID],[DD_PUR_NAME],[SCHM_TYPE],[ACCT_OWNERSHIP],[ACCT_STATUS]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`BEN_BANK_CODE`,`ACCT_OPEN_DATE`,`DD_NUM`,`BEN_ACCOUNT_ID`,`PSTD_USER_ID`,`VALUE_DATE`,`DD_ISSUE_DATE`,`MNEMONIC_CODE`,`REF_TXN_AMT`,`DD_ISSUE_AMT`,`BEN_SOL_ID`,`HOST_ID`,`TRAN_CATEGORY`,`ACCT_SOL_ID`,`SCHM_CODE`,`BEN_NAME`,`PART_TRAN_SRL_NUM`,`SYS_TIME`,`DD_ISSUE_CUSTID`,`TRAN_CRNCY_CODE`,`TRAN_CODE`,`ACCT_NAME`,`DD_ISSUE_ACCT`,`TRAN_DATE`,`TXN_BR_ID`,`DD_PUR_NAME`,`SCHM_TYPE`,`ACCT_OWNERSHIP`,`ACCT_STATUS`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

