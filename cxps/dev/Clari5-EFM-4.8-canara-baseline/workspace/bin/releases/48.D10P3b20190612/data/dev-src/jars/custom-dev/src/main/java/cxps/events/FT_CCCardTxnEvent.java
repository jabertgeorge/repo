// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_CC_CARDSTXN", Schema="rice")
public class FT_CCCardTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String userType;
       @Field(size=20) public String drAccountId;
       @Field(size=50) public String acctOpenDate;
       @Field(size=2) public String countryCode;
       @Field(size=20) public Double atmDomLim;
       @Field(size=20) public Double posecomIntrLim;
       @Field(size=6) public String bin;
       @Field(size=20) public String maskCardNo;
       @Field(size=5) public String posEntryMode;
       @Field(size=20) public String channel;
       @Field(size=100) public String errorDesc;
       @Field(size=50) public String entryMode;
       @Field(size=2) public String txnSecuredFlag;
       @Field(size=20) public String ipCountry;
       @Field(size=50) public String deviceId;
       @Field(size=20) public String crAccountId;
       @Field(size=20) public String stateCode;
       @Field(size=20) public Double txnAmount;
       @Field(size=5) public String hostId;
       @Field(size=10) public String chipPinFlag;
       @Field(size=10) public String custMobNo;
       @Field(size=20) public String ipCity;
       @Field(size=20) public String userId;
       @Field(size=50) public String custName;
       @Field(size=50) public String payeeName;
       @Field(size=200) public String tranParticular;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=10) public String branchId;
       @Field(size=50) public String custId;
       @Field(size=6) public String errorCode;
       @Field(size=20) public String mccCode;
       @Field(size=50) public String terminalId;
       @Field(size=20) public String crIfscCode;
       @Field(size=20) public Double atmIntrLim;
       @Field(size=16) public String custCardId;
       @Field(size=50) public String devownerId;
       @Field(size=20) public String ipAddress;
       @Field(size=50) public String tranDate;
       @Field(size=20) public Double posecomdomLim;
       @Field(size=10) public String tranType;
       @Field(size=20) public Double avlBal;
       @Field(size=20) public String payeeId;
       @Field(size=50) public String acctOwnership;
       @Field(size=50) public String merchantId;
       @Field(size=2) public String succFailFlag;
       @Field(size=50) public String productCode;
       @Field(size=50) public String partTranType;


    @JsonIgnore
    public ITable<FT_CCCardTxnEvent> t = AEF.getITable(this);

	public FT_CCCardTxnEvent(){}

    public FT_CCCardTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("CCCardTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setUserType(json.getString("user_type"));
            setDrAccountId(json.getString("dr_account_id"));
            setAcctOpenDate(json.getString("acct_open_date"));
            setCountryCode(json.getString("country_code"));
            setAtmDomLim(EventHelper.toDouble(json.getString("atm_dom_limit")));
            setPosecomIntrLim(EventHelper.toDouble(json.getString("pos_ecom_intr_limit")));
            setBin(json.getString("bin"));
            setMaskCardNo(json.getString("mask_card_no"));
            setPosEntryMode(json.getString("pos_entry_mode"));
            setChannel(json.getString("channel"));
            setErrorDesc(json.getString("error_desc"));
            setEntryMode(json.getString("entry_mode"));
            setTxnSecuredFlag(json.getString("txn_secured_flag"));
            setIpCountry(json.getString("ip_country"));
            setDeviceId(json.getString("device_id"));
            setCrAccountId(json.getString("cr_account_id"));
            setStateCode(json.getString("state_code"));
            setTxnAmount(EventHelper.toDouble(json.getString("txn_amt")));
            setHostId(json.getString("host_id"));
            setChipPinFlag(json.getString("chip_pin_flg"));
            setCustMobNo(json.getString("cust_mob_no"));
            setIpCity(json.getString("ip_city"));
            setUserId(json.getString("user_id"));
            setCustName(json.getString("cust_name"));
            setPayeeName(json.getString("payee_name"));
            setTranParticular(json.getString("tran_particular"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setMccCode(json.getString("mcc_code"));
            setTerminalId(json.getString("terminal_id"));
            setCrIfscCode(json.getString("cr_ifsc_code"));
            setAtmIntrLim(EventHelper.toDouble(json.getString("atm_intr_limit")));
            setCustCardId(json.getString("cust_card_id"));
            setDevownerId(json.getString("dev_owner_id"));
            setIpAddress(json.getString("ip_address"));
            setTranDate(json.getString("tran_date"));
            setPosecomdomLim(EventHelper.toDouble(json.getString("pos_ecom_dom_limit")));
            setTranType(json.getString("tran_type"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setPayeeId(json.getString("payee_id"));
            setAcctOwnership(json.getString("acct_ownership"));
            setMerchantId(json.getString("merchant_id"));
            setSuccFailFlag(json.getString("succ_fail_flg"));
            setProductCode(json.getString("product_code"));
            setPartTranType(json.getString("part_tran_type"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FCC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getUserType(){ return userType; }

    public String getDrAccountId(){ return drAccountId; }

    public String getAcctOpenDate(){ return acctOpenDate; }

    public String getCountryCode(){ return countryCode; }

    public Double getAtmDomLim(){ return atmDomLim; }

    public Double getPosecomIntrLim(){ return posecomIntrLim; }

    public String getBin(){ return bin; }

    public String getMaskCardNo(){ return maskCardNo; }

    public String getPosEntryMode(){ return posEntryMode; }

    public String getChannel(){ return channel; }

    public String getErrorDesc(){ return errorDesc; }

    public String getEntryMode(){ return entryMode; }

    public String getTxnSecuredFlag(){ return txnSecuredFlag; }

    public String getIpCountry(){ return ipCountry; }

    public String getDeviceId(){ return deviceId; }

    public String getCrAccountId(){ return crAccountId; }

    public String getStateCode(){ return stateCode; }

    public Double getTxnAmount(){ return txnAmount; }

    public String getHostId(){ return hostId; }

    public String getChipPinFlag(){ return chipPinFlag; }

    public String getCustMobNo(){ return custMobNo; }

    public String getIpCity(){ return ipCity; }

    public String getUserId(){ return userId; }

    public String getCustName(){ return custName; }

    public String getPayeeName(){ return payeeName; }

    public String getTranParticular(){ return tranParticular; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getMccCode(){ return mccCode; }

    public String getTerminalId(){ return terminalId; }

    public String getCrIfscCode(){ return crIfscCode; }

    public Double getAtmIntrLim(){ return atmIntrLim; }

    public String getCustCardId(){ return custCardId; }

    public String getDevownerId(){ return devownerId; }

    public String getIpAddress(){ return ipAddress; }

    public String getTranDate(){ return tranDate; }

    public Double getPosecomdomLim(){ return posecomdomLim; }

    public String getTranType(){ return tranType; }

    public Double getAvlBal(){ return avlBal; }

    public String getPayeeId(){ return payeeId; }

    public String getAcctOwnership(){ return acctOwnership; }

    public String getMerchantId(){ return merchantId; }

    public String getSuccFailFlag(){ return succFailFlag; }

    public String getProductCode(){ return productCode; }

    public String getPartTranType(){ return partTranType; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setUserType(String val){ this.userType = val; }
    public void setDrAccountId(String val){ this.drAccountId = val; }
    public void setAcctOpenDate(String val){ this.acctOpenDate = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAtmDomLim(Double val){ this.atmDomLim = val; }
    public void setPosecomIntrLim(Double val){ this.posecomIntrLim = val; }
    public void setBin(String val){ this.bin = val; }
    public void setMaskCardNo(String val){ this.maskCardNo = val; }
    public void setPosEntryMode(String val){ this.posEntryMode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setEntryMode(String val){ this.entryMode = val; }
    public void setTxnSecuredFlag(String val){ this.txnSecuredFlag = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setCrAccountId(String val){ this.crAccountId = val; }
    public void setStateCode(String val){ this.stateCode = val; }
    public void setTxnAmount(Double val){ this.txnAmount = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setChipPinFlag(String val){ this.chipPinFlag = val; }
    public void setCustMobNo(String val){ this.custMobNo = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setCustName(String val){ this.custName = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setTranParticular(String val){ this.tranParticular = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setMccCode(String val){ this.mccCode = val; }
    public void setTerminalId(String val){ this.terminalId = val; }
    public void setCrIfscCode(String val){ this.crIfscCode = val; }
    public void setAtmIntrLim(Double val){ this.atmIntrLim = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setDevownerId(String val){ this.devownerId = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setTranDate(String val){ this.tranDate = val; }
    public void setPosecomdomLim(Double val){ this.posecomdomLim = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setAcctOwnership(String val){ this.acctOwnership = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setSuccFailFlag(String val){ this.succFailFlag = val; }
    public void setProductCode(String val){ this.productCode = val; }
    public void setPartTranType(String val){ this.partTranType = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_CCCardTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String terminalKey= h.getCxKeyGivenHostKey(WorkspaceName.TERMINAL, getHostId(), this.terminalId);
        wsInfoSet.add(new WorkspaceInfo("Terminal", terminalKey));
        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.drAccountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));
        String merchantKey= h.getCxKeyGivenHostKey(WorkspaceName.MERCHANT, getHostId(), this.merchantId);
        wsInfoSet.add(new WorkspaceInfo("Merchant", merchantKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_CCCardTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "CCCardTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("cust_id",getCustCardId());
        json.put("cust_card_id",getCustCardId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
