package clari5.upload.ui;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Arrays;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

		/*This performs the
         *  DELETE operation  based on the primary Key */

public class DeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static CxpsLogger logger = CxpsLogger.getLogger(DeleteServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    @SuppressWarnings("null")
    private void doEither(HttpServletRequest request,
                          HttpServletResponse response) throws IOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PrintWriter out = response.getWriter();
        HttpSession session;
        String tableName = "";
        int count = 0;
        String userId;
        String status;
        String deleteSql;
        try {
            session = request.getSession();
            con = Rdbms.getAppConnection();

            tableName = (String) session.getAttribute("myId");
            userId = (String) session.getAttribute("userId");
            if (session == null || userId== null|| StringUtils.isNullOrEmpty(userId)){
//                try {
//                    request.getRequestDispatcher("/expiry.jsp").forward(request,response);
//
//                } catch (ServletException e) {
//                    logger.info("Exception in servlet "+e.getMessage()+"Cause ["+e.getCause()+"]");
//                }
            }
            Delete dc;
            for (String s : request.getParameterValues("deletebox")) {
                String[] columnWithHeaders = s.split("#");
                dc = new Delete(Arrays.asList(columnWithHeaders[0].split(",")),
                        Arrays.asList(columnWithHeaders[1].split(",")), tableName);
		System.out.println("[doEither] Calling Delete"+ dc.toString());
                deleteSql = dc.getDeleteQuery();
                try {
                    ps = con.prepareStatement(deleteSql);
                    int result = ps.executeUpdate();
                    if (result > 0)
                        ++count;
                    con.commit();
                    status = "Success";
                } catch (Exception e) {
                    logger.info("Exception while deleting the record ["+e.getMessage()+"] Cause ["+e.getCause()+"]" );
                    status = "Failure";
                }
                //status=UploadUiAudit.updateAudit(tableName, userId, status, columnWithHeaders[1].split(",")[0], "", "Delete");
                //logger.info("Error in insertingg into Audit Table " +status);
            }
            response.setContentType("text/html");
            out.println("<script type=\"text/javascript\">");
            out.println("alert(\"" + count + "\"+\" ROW(S) SUCCESSFULLY DELETED\");");
            out.println("window.location.href='/efm/DisplayTableData?table_name='+'" + tableName + "';");
            out.println("</script>");


        } catch (Exception e) {
            logger.info("Exception in Deleting the data [" + e.getMessage() + "] Cause[" + e.getCause() + "]");
            out.println("<script type=\"text/javascript\">");
            out.println("alert(\"" + count + "\"+\" ROW(S) COULDNOT BE DELETED\");");
            out.println("window.location.href='/efm/DisplayTableData?table_name='+'" + tableName + "';");
            out.println("</script>");
        } finally {
            try {
                if (con != null)
                    con.close();
            } catch (SQLException ex) {
                logger.info("Exception in closing connection [" + ex.getMessage() + "] Cause [" + ex.getCause() + "]");
            }
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
