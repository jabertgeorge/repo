
package clari5.custom.dev;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

/**
 *
 * @author lakshmisai
 */
public class SoapServlet extends HttpServlet {

    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void soapRequest(HttpServletRequest request, HttpServletResponse response) {

        String strMsg = "";
        try {

            MessageFactory messageFactory = MessageFactory.newInstance();
            InputStream inStream = request.getInputStream();
            SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), inStream);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            strMsg = new String(out.toByteArray());

            response.setStatus(200);
            response.setContentType("text/xml");

            PrintWriter writer = response.getWriter();
            writer.append(strMsg);

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            // Retrieve different parts
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
            // Two ways to extract headers
            SOAPHeader soapHeader = soapEnvelope.getHeader();
            soapHeader = soapMessage.getSOAPHeader();
            // Two ways to extract body
            SOAPBody soapBody = soapEnvelope.getBody();
            soapBody = soapMessage.getSOAPBody();
            // To add some element
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            Name bodyName = soapFactory.createName("getEmployeeDetails", "ns1", "urn:MySoapServices");
            SOAPBodyElement purchaseLineItems = soapBody.addBodyElement(bodyName);
            Name childName = soapFactory.createName("param1");
            SOAPElement order = purchaseLineItems.addChildElement(childName);
            order.addTextNode("1016577");

        } catch (Exception e) {
            e.printStackTrace();

        }
         */
    }

}

