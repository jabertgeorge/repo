// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_CCCardTxnEventMapper extends EventMapper<FT_CCCardTxnEvent> {

public FT_CCCardTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_CCCardTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_CCCardTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_CCCardTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_CCCardTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_CCCardTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_CCCardTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getUserType());
            preparedStatement.setString(i++, obj.getDrAccountId());
            preparedStatement.setString(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setDouble(i++, obj.getAtmDomLim());
            preparedStatement.setDouble(i++, obj.getPosecomIntrLim());
            preparedStatement.setString(i++, obj.getBin());
            preparedStatement.setString(i++, obj.getMaskCardNo());
            preparedStatement.setString(i++, obj.getPosEntryMode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getEntryMode());
            preparedStatement.setString(i++, obj.getTxnSecuredFlag());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getCrAccountId());
            preparedStatement.setString(i++, obj.getStateCode());
            preparedStatement.setDouble(i++, obj.getTxnAmount());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getChipPinFlag());
            preparedStatement.setString(i++, obj.getCustMobNo());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getCustName());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setString(i++, obj.getTranParticular());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getMccCode());
            preparedStatement.setString(i++, obj.getTerminalId());
            preparedStatement.setString(i++, obj.getCrIfscCode());
            preparedStatement.setDouble(i++, obj.getAtmIntrLim());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setString(i++, obj.getDevownerId());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setString(i++, obj.getTranDate());
            preparedStatement.setDouble(i++, obj.getPosecomdomLim());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getAcctOwnership());
            preparedStatement.setString(i++, obj.getMerchantId());
            preparedStatement.setString(i++, obj.getSuccFailFlag());
            preparedStatement.setString(i++, obj.getProductCode());
            preparedStatement.setString(i++, obj.getPartTranType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_CC_CARDSTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_CCCardTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_CCCardTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_CC_CARDSTXN"));
        putList = new ArrayList<>();

        for (FT_CCCardTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "USER_TYPE",  obj.getUserType());
            p = this.insert(p, "DR_ACCOUNT_ID",  obj.getDrAccountId());
            p = this.insert(p, "ACCT_OPEN_DATE",  obj.getAcctOpenDate());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "ATM_DOM_LIM", String.valueOf(obj.getAtmDomLim()));
            p = this.insert(p, "POSECOM_INTR_LIM", String.valueOf(obj.getPosecomIntrLim()));
            p = this.insert(p, "BIN",  obj.getBin());
            p = this.insert(p, "MASK_CARD_NO",  obj.getMaskCardNo());
            p = this.insert(p, "POS_ENTRY_MODE",  obj.getPosEntryMode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "ENTRY_MODE",  obj.getEntryMode());
            p = this.insert(p, "TXN_SECURED_FLAG",  obj.getTxnSecuredFlag());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "CR_ACCOUNT_ID",  obj.getCrAccountId());
            p = this.insert(p, "STATE_CODE",  obj.getStateCode());
            p = this.insert(p, "TXN_AMOUNT", String.valueOf(obj.getTxnAmount()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CHIP_PIN_FLAG",  obj.getChipPinFlag());
            p = this.insert(p, "CUST_MOB_NO",  obj.getCustMobNo());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "CUST_NAME",  obj.getCustName());
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "TRAN_PARTICULAR",  obj.getTranParticular());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "MCC_CODE",  obj.getMccCode());
            p = this.insert(p, "TERMINAL_ID",  obj.getTerminalId());
            p = this.insert(p, "CR_IFSC_CODE",  obj.getCrIfscCode());
            p = this.insert(p, "ATM_INTR_LIM", String.valueOf(obj.getAtmIntrLim()));
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "DEVOWNER_ID",  obj.getDevownerId());
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "TRAN_DATE",  obj.getTranDate());
            p = this.insert(p, "POSECOMDOM_LIM", String.valueOf(obj.getPosecomdomLim()));
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "ACCT_OWNERSHIP",  obj.getAcctOwnership());
            p = this.insert(p, "MERCHANT_ID",  obj.getMerchantId());
            p = this.insert(p, "SUCC_FAIL_FLAG",  obj.getSuccFailFlag());
            p = this.insert(p, "PRODUCT_CODE",  obj.getProductCode());
            p = this.insert(p, "PART_TRAN_TYPE",  obj.getPartTranType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_CC_CARDSTXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_CC_CARDSTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_CC_CARDSTXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_CCCardTxnEvent obj = new FT_CCCardTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setUserType(rs.getString("USER_TYPE"));
    obj.setDrAccountId(rs.getString("DR_ACCOUNT_ID"));
    obj.setAcctOpenDate(rs.getString("ACCT_OPEN_DATE"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setAtmDomLim(rs.getDouble("ATM_DOM_LIM"));
    obj.setPosecomIntrLim(rs.getDouble("POSECOM_INTR_LIM"));
    obj.setBin(rs.getString("BIN"));
    obj.setMaskCardNo(rs.getString("MASK_CARD_NO"));
    obj.setPosEntryMode(rs.getString("POS_ENTRY_MODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setEntryMode(rs.getString("ENTRY_MODE"));
    obj.setTxnSecuredFlag(rs.getString("TXN_SECURED_FLAG"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setCrAccountId(rs.getString("CR_ACCOUNT_ID"));
    obj.setStateCode(rs.getString("STATE_CODE"));
    obj.setTxnAmount(rs.getDouble("TXN_AMOUNT"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setChipPinFlag(rs.getString("CHIP_PIN_FLAG"));
    obj.setCustMobNo(rs.getString("CUST_MOB_NO"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setCustName(rs.getString("CUST_NAME"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setTranParticular(rs.getString("TRAN_PARTICULAR"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setMccCode(rs.getString("MCC_CODE"));
    obj.setTerminalId(rs.getString("TERMINAL_ID"));
    obj.setCrIfscCode(rs.getString("CR_IFSC_CODE"));
    obj.setAtmIntrLim(rs.getDouble("ATM_INTR_LIM"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setDevownerId(rs.getString("DEVOWNER_ID"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setTranDate(rs.getString("TRAN_DATE"));
    obj.setPosecomdomLim(rs.getDouble("POSECOMDOM_LIM"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setAcctOwnership(rs.getString("ACCT_OWNERSHIP"));
    obj.setMerchantId(rs.getString("MERCHANT_ID"));
    obj.setSuccFailFlag(rs.getString("SUCC_FAIL_FLAG"));
    obj.setProductCode(rs.getString("PRODUCT_CODE"));
    obj.setPartTranType(rs.getString("PART_TRAN_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_CC_CARDSTXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_CCCardTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_CCCardTxnEvent> events;
 FT_CCCardTxnEvent obj = new FT_CCCardTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_CC_CARDSTXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_CCCardTxnEvent obj = new FT_CCCardTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setUserType(getColumnValue(rs, "USER_TYPE"));
            obj.setDrAccountId(getColumnValue(rs, "DR_ACCOUNT_ID"));
            obj.setAcctOpenDate(getColumnValue(rs, "ACCT_OPEN_DATE"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setAtmDomLim(EventHelper.toDouble(getColumnValue(rs, "ATM_DOM_LIM")));
            obj.setPosecomIntrLim(EventHelper.toDouble(getColumnValue(rs, "POSECOM_INTR_LIM")));
            obj.setBin(getColumnValue(rs, "BIN"));
            obj.setMaskCardNo(getColumnValue(rs, "MASK_CARD_NO"));
            obj.setPosEntryMode(getColumnValue(rs, "POS_ENTRY_MODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setEntryMode(getColumnValue(rs, "ENTRY_MODE"));
            obj.setTxnSecuredFlag(getColumnValue(rs, "TXN_SECURED_FLAG"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setCrAccountId(getColumnValue(rs, "CR_ACCOUNT_ID"));
            obj.setStateCode(getColumnValue(rs, "STATE_CODE"));
            obj.setTxnAmount(EventHelper.toDouble(getColumnValue(rs, "TXN_AMOUNT")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setChipPinFlag(getColumnValue(rs, "CHIP_PIN_FLAG"));
            obj.setCustMobNo(getColumnValue(rs, "CUST_MOB_NO"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setCustName(getColumnValue(rs, "CUST_NAME"));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setTranParticular(getColumnValue(rs, "TRAN_PARTICULAR"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setMccCode(getColumnValue(rs, "MCC_CODE"));
            obj.setTerminalId(getColumnValue(rs, "TERMINAL_ID"));
            obj.setCrIfscCode(getColumnValue(rs, "CR_IFSC_CODE"));
            obj.setAtmIntrLim(EventHelper.toDouble(getColumnValue(rs, "ATM_INTR_LIM")));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setDevownerId(getColumnValue(rs, "DEVOWNER_ID"));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setTranDate(getColumnValue(rs, "TRAN_DATE"));
            obj.setPosecomdomLim(EventHelper.toDouble(getColumnValue(rs, "POSECOMDOM_LIM")));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setAcctOwnership(getColumnValue(rs, "ACCT_OWNERSHIP"));
            obj.setMerchantId(getColumnValue(rs, "MERCHANT_ID"));
            obj.setSuccFailFlag(getColumnValue(rs, "SUCC_FAIL_FLAG"));
            obj.setProductCode(getColumnValue(rs, "PRODUCT_CODE"));
            obj.setPartTranType(getColumnValue(rs, "PART_TRAN_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_CC_CARDSTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_CC_CARDSTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"USER_TYPE\",\"DR_ACCOUNT_ID\",\"ACCT_OPEN_DATE\",\"COUNTRY_CODE\",\"ATM_DOM_LIM\",\"POSECOM_INTR_LIM\",\"BIN\",\"MASK_CARD_NO\",\"POS_ENTRY_MODE\",\"CHANNEL\",\"ERROR_DESC\",\"ENTRY_MODE\",\"TXN_SECURED_FLAG\",\"IP_COUNTRY\",\"DEVICE_ID\",\"CR_ACCOUNT_ID\",\"STATE_CODE\",\"TXN_AMOUNT\",\"HOST_ID\",\"CHIP_PIN_FLAG\",\"CUST_MOB_NO\",\"IP_CITY\",\"USER_ID\",\"CUST_NAME\",\"PAYEE_NAME\",\"TRAN_PARTICULAR\",\"SYS_TIME\",\"BRANCH_ID\",\"CUST_ID\",\"ERROR_CODE\",\"MCC_CODE\",\"TERMINAL_ID\",\"CR_IFSC_CODE\",\"ATM_INTR_LIM\",\"CUST_CARD_ID\",\"DEVOWNER_ID\",\"IP_ADDRESS\",\"TRAN_DATE\",\"POSECOMDOM_LIM\",\"TRAN_TYPE\",\"AVL_BAL\",\"PAYEE_ID\",\"ACCT_OWNERSHIP\",\"MERCHANT_ID\",\"SUCC_FAIL_FLAG\",\"PRODUCT_CODE\",\"PART_TRAN_TYPE\"" +
              " FROM EVENT_FT_CC_CARDSTXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [USER_TYPE],[DR_ACCOUNT_ID],[ACCT_OPEN_DATE],[COUNTRY_CODE],[ATM_DOM_LIM],[POSECOM_INTR_LIM],[BIN],[MASK_CARD_NO],[POS_ENTRY_MODE],[CHANNEL],[ERROR_DESC],[ENTRY_MODE],[TXN_SECURED_FLAG],[IP_COUNTRY],[DEVICE_ID],[CR_ACCOUNT_ID],[STATE_CODE],[TXN_AMOUNT],[HOST_ID],[CHIP_PIN_FLAG],[CUST_MOB_NO],[IP_CITY],[USER_ID],[CUST_NAME],[PAYEE_NAME],[TRAN_PARTICULAR],[SYS_TIME],[BRANCH_ID],[CUST_ID],[ERROR_CODE],[MCC_CODE],[TERMINAL_ID],[CR_IFSC_CODE],[ATM_INTR_LIM],[CUST_CARD_ID],[DEVOWNER_ID],[IP_ADDRESS],[TRAN_DATE],[POSECOMDOM_LIM],[TRAN_TYPE],[AVL_BAL],[PAYEE_ID],[ACCT_OWNERSHIP],[MERCHANT_ID],[SUCC_FAIL_FLAG],[PRODUCT_CODE],[PART_TRAN_TYPE]" +
              " FROM EVENT_FT_CC_CARDSTXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`USER_TYPE`,`DR_ACCOUNT_ID`,`ACCT_OPEN_DATE`,`COUNTRY_CODE`,`ATM_DOM_LIM`,`POSECOM_INTR_LIM`,`BIN`,`MASK_CARD_NO`,`POS_ENTRY_MODE`,`CHANNEL`,`ERROR_DESC`,`ENTRY_MODE`,`TXN_SECURED_FLAG`,`IP_COUNTRY`,`DEVICE_ID`,`CR_ACCOUNT_ID`,`STATE_CODE`,`TXN_AMOUNT`,`HOST_ID`,`CHIP_PIN_FLAG`,`CUST_MOB_NO`,`IP_CITY`,`USER_ID`,`CUST_NAME`,`PAYEE_NAME`,`TRAN_PARTICULAR`,`SYS_TIME`,`BRANCH_ID`,`CUST_ID`,`ERROR_CODE`,`MCC_CODE`,`TERMINAL_ID`,`CR_IFSC_CODE`,`ATM_INTR_LIM`,`CUST_CARD_ID`,`DEVOWNER_ID`,`IP_ADDRESS`,`TRAN_DATE`,`POSECOMDOM_LIM`,`TRAN_TYPE`,`AVL_BAL`,`PAYEE_ID`,`ACCT_OWNERSHIP`,`MERCHANT_ID`,`SUCC_FAIL_FLAG`,`PRODUCT_CODE`,`PART_TRAN_TYPE`" +
              " FROM EVENT_FT_CC_CARDSTXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_CC_CARDSTXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"USER_TYPE\",\"DR_ACCOUNT_ID\",\"ACCT_OPEN_DATE\",\"COUNTRY_CODE\",\"ATM_DOM_LIM\",\"POSECOM_INTR_LIM\",\"BIN\",\"MASK_CARD_NO\",\"POS_ENTRY_MODE\",\"CHANNEL\",\"ERROR_DESC\",\"ENTRY_MODE\",\"TXN_SECURED_FLAG\",\"IP_COUNTRY\",\"DEVICE_ID\",\"CR_ACCOUNT_ID\",\"STATE_CODE\",\"TXN_AMOUNT\",\"HOST_ID\",\"CHIP_PIN_FLAG\",\"CUST_MOB_NO\",\"IP_CITY\",\"USER_ID\",\"CUST_NAME\",\"PAYEE_NAME\",\"TRAN_PARTICULAR\",\"SYS_TIME\",\"BRANCH_ID\",\"CUST_ID\",\"ERROR_CODE\",\"MCC_CODE\",\"TERMINAL_ID\",\"CR_IFSC_CODE\",\"ATM_INTR_LIM\",\"CUST_CARD_ID\",\"DEVOWNER_ID\",\"IP_ADDRESS\",\"TRAN_DATE\",\"POSECOMDOM_LIM\",\"TRAN_TYPE\",\"AVL_BAL\",\"PAYEE_ID\",\"ACCT_OWNERSHIP\",\"MERCHANT_ID\",\"SUCC_FAIL_FLAG\",\"PRODUCT_CODE\",\"PART_TRAN_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[USER_TYPE],[DR_ACCOUNT_ID],[ACCT_OPEN_DATE],[COUNTRY_CODE],[ATM_DOM_LIM],[POSECOM_INTR_LIM],[BIN],[MASK_CARD_NO],[POS_ENTRY_MODE],[CHANNEL],[ERROR_DESC],[ENTRY_MODE],[TXN_SECURED_FLAG],[IP_COUNTRY],[DEVICE_ID],[CR_ACCOUNT_ID],[STATE_CODE],[TXN_AMOUNT],[HOST_ID],[CHIP_PIN_FLAG],[CUST_MOB_NO],[IP_CITY],[USER_ID],[CUST_NAME],[PAYEE_NAME],[TRAN_PARTICULAR],[SYS_TIME],[BRANCH_ID],[CUST_ID],[ERROR_CODE],[MCC_CODE],[TERMINAL_ID],[CR_IFSC_CODE],[ATM_INTR_LIM],[CUST_CARD_ID],[DEVOWNER_ID],[IP_ADDRESS],[TRAN_DATE],[POSECOMDOM_LIM],[TRAN_TYPE],[AVL_BAL],[PAYEE_ID],[ACCT_OWNERSHIP],[MERCHANT_ID],[SUCC_FAIL_FLAG],[PRODUCT_CODE],[PART_TRAN_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`USER_TYPE`,`DR_ACCOUNT_ID`,`ACCT_OPEN_DATE`,`COUNTRY_CODE`,`ATM_DOM_LIM`,`POSECOM_INTR_LIM`,`BIN`,`MASK_CARD_NO`,`POS_ENTRY_MODE`,`CHANNEL`,`ERROR_DESC`,`ENTRY_MODE`,`TXN_SECURED_FLAG`,`IP_COUNTRY`,`DEVICE_ID`,`CR_ACCOUNT_ID`,`STATE_CODE`,`TXN_AMOUNT`,`HOST_ID`,`CHIP_PIN_FLAG`,`CUST_MOB_NO`,`IP_CITY`,`USER_ID`,`CUST_NAME`,`PAYEE_NAME`,`TRAN_PARTICULAR`,`SYS_TIME`,`BRANCH_ID`,`CUST_ID`,`ERROR_CODE`,`MCC_CODE`,`TERMINAL_ID`,`CR_IFSC_CODE`,`ATM_INTR_LIM`,`CUST_CARD_ID`,`DEVOWNER_ID`,`IP_ADDRESS`,`TRAN_DATE`,`POSECOMDOM_LIM`,`TRAN_TYPE`,`AVL_BAL`,`PAYEE_ID`,`ACCT_OWNERSHIP`,`MERCHANT_ID`,`SUCC_FAIL_FLAG`,`PRODUCT_CODE`,`PART_TRAN_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

