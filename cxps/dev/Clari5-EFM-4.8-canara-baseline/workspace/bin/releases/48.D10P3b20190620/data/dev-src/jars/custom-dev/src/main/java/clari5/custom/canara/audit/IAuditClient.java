package clari5.custom.canara.audit;

public interface IAuditClient {
	void log(String message);
	void log(LogLevel level, String message);
}
