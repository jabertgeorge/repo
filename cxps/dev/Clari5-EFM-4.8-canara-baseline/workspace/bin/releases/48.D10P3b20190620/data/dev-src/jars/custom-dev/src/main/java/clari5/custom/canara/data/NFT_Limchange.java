package clari5.custom.canara.data;

public class NFT_Limchange extends ITableData {

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "NFT_LIMCHANGE";
    private String event_type = "NFT_Limchange";
    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String USER_ID ;
    private String CUST_ID ;
    private String OLD_LIMIT ;
    private String NEW_LIMIT ;
    private String CARD_NUMBER ;
    private String CARD_TYPE ;
    private String TRAN_DATE ;
    private String LIM_CHANGE_DATE ;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getOLD_LIMIT() {
        return OLD_LIMIT;
    }

    public void setOLD_LIMIT(String OLD_LIMIT) {
        this.OLD_LIMIT = OLD_LIMIT;
    }

    public String getNEW_LIMIT() {
        return NEW_LIMIT;
    }

    public void setNEW_LIMIT(String NEW_LIMIT) {
        this.NEW_LIMIT = NEW_LIMIT;
    }

    public String getCARD_NUMBER() {
        return CARD_NUMBER;
    }

    public void setCARD_NUMBER(String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }

    public String getCARD_TYPE() {
        return CARD_TYPE;
    }

    public void setCARD_TYPE(String CARD_TYPE) {
        this.CARD_TYPE = CARD_TYPE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getLIM_CHANGE_DATE() {
        return LIM_CHANGE_DATE;
    }

    public void setLIM_CHANGE_DATE(String LIM_CHANGE_DATE) {
        this.LIM_CHANGE_DATE = LIM_CHANGE_DATE;
    }



}

