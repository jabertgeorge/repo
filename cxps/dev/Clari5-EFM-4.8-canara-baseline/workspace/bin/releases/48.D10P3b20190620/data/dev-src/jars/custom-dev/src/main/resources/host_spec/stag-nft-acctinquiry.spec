clari5.hfdb.entity.nft_acctinquiry { 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = ID, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = channel ,type = "string:20"}
{name = user_id ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = branch_id ,type = "string:20"}
{name = menu_id ,type = "string:50"}
{name = acct_name ,type = "string:50"}
{name = schm_type ,type = "string:20"}
{name = schm_code ,type = "string:20"}
{name = acct_status ,type = "string:20"}
{name = avl_bal ,type =  "number:11,2"}
{name = account_id ,type = "string:20"}
{name = cust_card_id ,type = "string:20"}
{name = tran_date ,type = timestamp}
{name = mask_card_no ,type = "string:20"}
{name = country_code ,type = "string:20"}
        ]
}
