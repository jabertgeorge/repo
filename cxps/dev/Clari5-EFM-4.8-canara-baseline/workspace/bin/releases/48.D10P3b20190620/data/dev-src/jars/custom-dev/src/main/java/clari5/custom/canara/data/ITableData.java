package clari5.custom.canara.data;

import clari5.custom.canara.builder.EventBuilder;
import clari5.custom.canara.builder.MsgMetaData;
import clari5.custom.canara.data.bootstrap.TableMap;

public abstract class ITableData {

	public boolean process() throws Exception {
		MsgMetaData m = TableMap.getTableMap().get(this.getTableName());
		EventBuilder eb = new EventBuilder();
		eb.process(this);
		return true;
	}
	public abstract String getTableName();
}
