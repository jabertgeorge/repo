package clari5.custom.cms;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.hfdb.Hfdb;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import clari5.tas.core.utils.STR;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Evidence;
import cxps.eoi.EvidenceData;
import cxps.eoi.IncidentEvent;
import org.json.JSONException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by jabert on 03/06/19.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    PreparedStatement ps = null;
    Connection con = null;
    protected static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);


    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }
    /*
    Adding Mobile number and email for custCard_Id in payment card workspace
     */
    @Override
    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException {

            logger.info("Into Custom PopulateIncident Json");
            CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
            CxJson json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
            String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();

            if(!isupdate && event.getEntityId().startsWith("P_F_")){
                try {

                    String eventJson = event.convertToJson(event);
                    Hocon h = new Hocon(eventJson);
                    CxJson wrapper = json.get("fields");

                    Hocon conf = new Hocon();
                    conf.loadFromContext("custom-fields.conf");

                    Hocon mobEmailHocon = conf.get("custom-fields");
                    HashMap<String, String> tableQueryMap = new HashMap<>();
                    if (mobEmailHocon != null) {
                        List<String> keys = mobEmailHocon.getKeysAsList();
                        for (String key : keys) {
                            String val = mobEmailHocon.getString(key);
                            logger.info("the custom field for " + key + " : " + val);
                            tableQueryMap.put(key, val);
                        }
                    }

                    if ( tableQueryMap != null && tableQueryMap.size() >= 2) {

                        List<String> mobEmList = getMobEmail(event.getEntityId().replace("P_F_", ""));
                        if (mobEmList != null && tableQueryMap != null) {
                            wrapper.put(tableQueryMap.get("mobileField"), mobEmList.get(0).toString());
                            wrapper.put(tableQueryMap.get("emailField"), mobEmList.get(1).toString());
                            return json.put("fields", wrapper);
                        }
                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return json;
                }
            }
            return json;
    }

    public List<String> getMobEmail(String cardNo)
    {
        String mobileNo = null;
        String emailId = null;
        List<String> mobEmList = new ArrayList<String>();
        con = Rdbms.getConnection();
        try {

            String query = "select CUSTMOBILE,CUSTEMAIL from CUST_CARD_MASTER where CARD_NUMBER = '" + cardNo + "' and ROWNUM = 1";
            ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                mobileNo = rs.getString("CUSTMOBILE");
                emailId = rs.getString("CUSTEMAIL");
            }
            con.close();
            ps.close();
            logger.info("Card No : "+cardNo+" : mobile No : "+mobileNo+ " : email Id : "+emailId);
            mobEmList.add((mobileNo != null) ? mobileNo : "Not Available");
            mobEmList.add((emailId != null) ? emailId : "Not Available" );
            rs.close();
            return mobEmList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }


}

