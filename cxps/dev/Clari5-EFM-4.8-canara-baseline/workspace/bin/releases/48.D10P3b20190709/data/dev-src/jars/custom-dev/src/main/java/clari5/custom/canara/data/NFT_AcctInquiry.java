package clari5.custom.canara.data;

public class NFT_AcctInquiry extends ITableData{

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "NFT_ACCTINQUIRY";
    private String event_type = "NFT_AcctInquiry";

    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String CHANNEL ;
    private String USER_ID ;
    private String CUST_ID ;
    private String BRANCH_ID ;
    private String MENU_ID ;
    private String ACCT_NAME ;
    private String SCHM_TYPE ;
    private String SCHM_CODE ;
    private String ACCT_STATUS ;
    private String AVL_BAL ;
    private String ACCOUNT_ID ;
    private String CUST_CARD_ID ;
    private String TRAN_DATE ;
    private String MASK_CARD_NO ;
    private String COUNTRY_CODE ;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getMENU_ID() {
        return MENU_ID;
    }

    public void setMENU_ID(String MENU_ID) {
        this.MENU_ID = MENU_ID;
    }

    public String getACCT_NAME() {
        return ACCT_NAME;
    }

    public void setACCT_NAME(String ACCT_NAME) {
        this.ACCT_NAME = ACCT_NAME;
    }

    public String getSCHM_TYPE() {
        return SCHM_TYPE;
    }

    public void setSCHM_TYPE(String SCHM_TYPE) {
        this.SCHM_TYPE = SCHM_TYPE;
    }

    public String getSCHM_CODE() {
        return SCHM_CODE;
    }

    public void setSCHM_CODE(String SCHM_CODE) {
        this.SCHM_CODE = SCHM_CODE;
    }

    public String getACCT_STATUS() {
        return ACCT_STATUS;
    }

    public void setACCT_STATUS(String ACCT_STATUS) {
        this.ACCT_STATUS = ACCT_STATUS;
    }

    public String getAVL_BAL() {
        return AVL_BAL;
    }

    public void setAVL_BAL(String AVL_BAL) {
        this.AVL_BAL = AVL_BAL;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getCUST_CARD_ID() {
        return CUST_CARD_ID;
    }

    public void setCUST_CARD_ID(String CUST_CARD_ID) {
        this.CUST_CARD_ID = CUST_CARD_ID;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getMASK_CARD_NO() {
        return MASK_CARD_NO;
    }

    public void setMASK_CARD_NO(String MASK_CARD_NO) {
        this.MASK_CARD_NO = MASK_CARD_NO;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }




}

