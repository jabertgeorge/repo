package clari5.custom.cms;

import clari5.aml.cms.jira.IssueManagerImpl;
import clari5.hfdb.Hfdb;
import clari5.aml.cms.jira.display.Formatter;
import clari5.hfdb.HostEvent;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.trace.TracerUtil;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import com.maxmind.geoip.MaxmindUtil;
import cxps.apex.utils.CxpsLogger;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;


public class CustomFormatter extends Formatter {
    private static CxpsLogger logger = CxpsLogger.getLogger(CustomFormatter.class);

    public static String convertToNonScientific(double value) {
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(2);
        return df.format(value);
    }

    public static String convertToDateTime(Long dateInLong) {
        if (dateInLong == 0) {
            return IssueManagerImpl.NA;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInLong);
        return calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DATE) +
                " " + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND) + "." + calendar.get(Calendar.MILLISECOND);
    }

    public static String getEnglish(HostEvent ev) {

        System.out.println("Custom Formatter ------");
        String[] fields = Hfdb.getEventFieldContainer().getEventFields(ev.getEventName());
        if (fields == null) {
            return ev.getEventId();
        }

        Hocon hocon = new Hocon();
        hocon.loadJson(ev.getEvent());

        String body = TracerUtil.getMsgBody(hocon).replaceAll("[']", "\"").replaceAll("[^\\x00-\\x7F]", " ");

        String countryCode = null;
        String cntry_name = null;
        String region_name = null;
        String city_name = null;
        String postal_code = null;
        String latitude = null;
        String longitude = null;
        CxJson json = null;
        String ip = null;
        try {
            json = CxJson.parse(body);
            json.toJson();
            ip = (json.get("addr_network").equals("") || json.get("addr_network").equals(null)) ? null : json.get("addr_network").toString();
        } catch (NullPointerException e) {
            System.out.println("inside CustomFormatter Exception message for ip = null is: "+e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("IP from JSON to fetch country code: "+ip);

        if (ip!= null && !ip.trim().equalsIgnoreCase("") && !ip.isEmpty()) {
            try {
                countryCode = MaxmindUtil.getCountryCode(ip);
                cntry_name = getLocation(ip).countryName == null ? "" : getLocation(ip).countryName;
                region_name = getLocation(ip).region ==  null ? "" : getLocation(ip).region;
                city_name = getLocation(ip).city == null ? "" : getLocation(ip).city;
                postal_code = getLocation(ip).postalCode == null ? "" : getLocation(ip).postalCode;
                latitude = String.valueOf(getLocation(ip).latitude) == null ? "" : String.valueOf(getLocation(ip).latitude);
                longitude = String.valueOf(getLocation(ip).longitude) == null ? "" : String.valueOf(getLocation(ip).longitude);
            }catch (NullPointerException e){
                System.out.println("data for cntry is null: "+e.getMessage());
            }
        }

        /*System.out.println(countryCode+" --> " + cntry_name +" --> " + region_name +" --> " + city_name +" --> "+postal_code+
        " --> "+latitude+" --> "+longitude);*/
        Hocon msgBody = new Hocon();
        msgBody.loadJson(body);
        StringBuilder sb = new StringBuilder("|");
        boolean firstTime = true;
        for (String field : fields) {
            if (firstTime) {
                firstTime = false;
            } else {
                sb.append("|");
            }
            String fval = msgBody.getString(field, "not available");

            if (field.equalsIgnoreCase("cntry_code")) {
                fval = countryCode == null ? ""  : countryCode;
            }
            if (field.equalsIgnoreCase("cntry_name")) {
                fval = cntry_name == null ? "" : cntry_name;
            }
            if (field.equalsIgnoreCase("region_name")) {
                fval = region_name == null ? "" : region_name;
            }
            if (field.equalsIgnoreCase("city_name")) {
                fval = city_name == null ? "": city_name;
            }
            if (field.equalsIgnoreCase("postal_code")) {
                fval = postal_code == null ? "" : postal_code;
            }
            if (field.equalsIgnoreCase("lattitude")) {
                fval = latitude == null ? "" : latitude;
            }
            if (field.equalsIgnoreCase("longitude")) {
                fval = longitude == null ? "" : longitude;
            }

            if (fval != null && fval.trim().equalsIgnoreCase(""))
                fval = "Not Available";

            sb.append(fval);
        }
        sb.append("|");
        return sb.toString();
    }

    private static Location getLocation(String ipAddress) {
        LookupService service = null;
        try {
            service = new LookupService("/cxps/BASE/product/caches/GeoLiteCity.dat", LookupService.GEOIP_MEMORY_CACHE);
        } catch (NullPointerException e) {
            System.out.println("returning null from catch of getLocation");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return service == null ? null : service.getLocation(ipAddress);
    }
}

