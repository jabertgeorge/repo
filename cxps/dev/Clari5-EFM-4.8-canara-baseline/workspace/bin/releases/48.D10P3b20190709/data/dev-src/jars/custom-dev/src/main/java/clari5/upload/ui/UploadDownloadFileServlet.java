package clari5.upload.ui;

import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import clari5.rdbms.Rdbms;
import sun.java2d.pipe.SpanShapeRenderer;

import java.text.ParseException;
import java.util.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.poi.ss.util.CellReference.NameType.ROW;

public class UploadDownloadFileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static CxpsLogger logger = CxpsLogger.getLogger(UploadDownloadFileServlet.class);
    private Connection con = null;
    private PreparedStatement ps = null;
    private PreparedStatement ps1 = null;
    private ResultSet rs = null;
    private ResultSet rs1=null;
    private String status;
    private String timeStamp = null;
    private int numberOfColumnsDB = 0;
    private String insertSql = null;
    private int rowNum = 0;
    private int columnNum = 0;
    static int count = 0;
    private String requestFileName = null;
    private String fileBackupPath = null;
    private String requestFileNameSplit = null;
    Map<Integer, String> hmap = new HashMap<Integer, String>();
    private String sql1=new String("Select systimestamp from Dual");
    private static String dateValue=null;

    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public String getDateValue() {
        return this.dateValue;

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	    String requestTableName;
        HttpSession session;
        ResultSetMetaData rsData;
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        session = request.getSession();
        String userId = (String) session.getAttribute("userId");
        requestTableName = (String) session.getAttribute("tableName");

        fileBackupPath = (String) session.getAttribute("FilePath");
        requestFileName = (String) session.getAttribute("FileName");
        logger.debug("UploadDownloadFileServlet : "+userId+":"+requestTableName+":"+fileBackupPath+":"+requestFileName);
        //reading the type of file (xls,xlsx)
        int s = fileBackupPath.lastIndexOf('.');
        logger.info("index" + s);
        if (s >= 0)
            requestFileNameSplit = fileBackupPath.substring(s + 1);

        //calling the respective insert function based on file type
        if (requestFileNameSplit.equalsIgnoreCase("xlsx")) {
            count = 0;
            xlsxFileInsert(requestTableName, response);
        } else if (requestFileNameSplit.equalsIgnoreCase("xls")) {
            count = 0;
            xlsFileInsert(requestTableName, response);
        }

        logger.info("File Type[" + requestFileNameSplit + "] and the table is [" + requestTableName + "]");
        //UploadUiAudit.updateAudit(requestTableName, userId, status, "", requestFileName, "Insert");
        request.setAttribute("TOTAL", count);
        request.getRequestDispatcher("/DisplayInsert").include(request, response);
    }

    private String getCellValue(Cell cell){
        String value = "",type="";
        switch (cell.getCellType()) {
            
            case NUMERIC:
                type="Numeric";

                value=cell.getNumericCellValue()+"";
                if(DateUtil.isCellDateFormatted(cell)){
                    Date date=DateUtil.getJavaDate(Double.parseDouble(value));
                    value=new SimpleDateFormat("YYYY-MM-dd").format(date);

                }
                else
                {
                    value=String.valueOf(cell.getNumericCellValue());

                }


                break;
            case STRING:
                type = "String";
                value = cell.getStringCellValue().toString();
                System.out.println("[getCellValue] Before [Type ]: "+type + " [Value] "+value);

                break;
            case FORMULA:
                type = "Formula";
                value = "";
               // System.out.println("[getCellValue] Type: "+type + " Value "+value);
                break;
            case BLANK:
                type = "Blank";
		        // value="";
                value = cell.getStringCellValue();
                if (value.equals("") || null == value) value = null;
                System.out.println("[getCellValue] Before [Type ]: "+type + "[ Value] "+value);
                break;

            case BOOLEAN:
                type = "Boolean";
                value = cell.getBooleanCellValue()+"";
                //System.out.println("[getCellValue] Type: "+type + " Value "+value);
                break;
            case ERROR:
                type = "Error";
                value = cell.getErrorCellValue()+"";
                //System.out.println("[getCellValue] Type: "+type + " Value "+value);
                break;
            default:
                type = "Default Date";
                value = cell.getDateCellValue().toString();
                SimpleDateFormat formatter=new SimpleDateFormat("dd/MM/yy");
                value=formatter.format(value);
                System.out.println("[getCellValue] Type: "+type + " Value "+value);
                break;
        }
        logger.debug("getCellValue : "+cell.getCellType()+":"+type+" : "+value);
        return value;
    }

    public void xlsFileInsert(String requestTableName, HttpServletResponse response) {
        int numberOfRecords=0;
        HSSFWorkbook wb;
        HSSFSheet ws;
        BigInteger temp = null;
        int error = 0;
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            con = Rdbms.getAppConnection();
            wb = new HSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum();
            columnNum = ws.getRow(0).getLastCellNum();
            Iterator<Row> rowIterator = ws.iterator();
            System.out.println("[xlsFileInsert] UploadDownloadFileServlet : "+rowNum+" - "+columnNum);
            int rowNum = 0;
            String colNames = "",psArgs="";
            List<String> list=new LinkedList<>();
            List<String> celllist=null;
            while (rowIterator.hasNext()) {
                int cellPos = 0;
                Row row1 = rowIterator.next();
                celllist=new ArrayList<>();
                for(int k=0;k<ws.getRow(0).getLastCellNum();k++){
                        Cell cell=row1.getCell(k,Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                        if(row1.getRowNum()==0) {
                            list.add(getCellValue(cell));
                        }
                        else if(row1.getRowNum()>0) {
                            if (cell == null || "".equals(cell)) {
                                System.out.println("Found Null value For");
                                celllist.add(null);
                                System.out.println(celllist + "--");
                            } else {
                                celllist.add(getCellValue(cell));

                            }
                        }
                    } // end of for
                logger.info("[xlsFileInsert] Printing Cell values for Row" +celllist);
                boolean execute = false;
                if(celllist.size()>0) {
                    ps1=con.prepareStatement(sql1);
                    rs1=ps1.executeQuery();

                    while(rs1.next()){
                        dateValue=rs1.getString(1);

                    }
                    setDateValue(dateValue);
                    System.out.println(dateValue);
                    colNames = getColumnName(list);
                    psArgs = getRecords(celllist);
                    execute=true;
                 }else{
                    continue;
                }
                try {
                    if (execute) {
                        //System.out.println("[xlsFileInsert] Inside the xls insert method--");
                        //System.out.println("[xlsFileInsert] xls insert : " + "insert into " + requestTableName + " (" + colNames + ") values (" + psArgs + ")");
                        logger.debug("xls insert : " + "insert  into " + requestTableName + " (" + colNames + ") values (" + psArgs + ")");
                        ps = con.prepareStatement("insert into " + requestTableName + " (" + colNames + ") values (" + psArgs + ")");
                        numberOfRecords = ps.executeUpdate();
                        //System.out.println("[xlsFileInsert] Rows Inserted " + numberOfRecords);

                        psArgs = "";
                    }
                    count++;
                } catch (SQLException e) {
                    e.printStackTrace();
                    error++;
                    status = "Failure";
                    logger.info("Exception while inserting the records [" + e.getMessage() + "and Cause [" + e.getCause() + "]");
                }
                rowNum++;
                numberOfRecords++;
            }
            System.out.println("[xlsFileInsert] Printing Cell values for Column " +list);


                if (error > 0) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert(\"Error in Inserting " + error + " records  Kindly check the logs for more information \" " + ");");
                    out.println("</script>");

                }
                con.commit();
                con.close();
                if (error == 0) {
                    status = "Success";
		            out.println("<script type=\"text/javascript\">");
                    out.println("alert(\"Records Inserted!! " +  " \");");
                    out.println("window.close();");
                    out.println("</script>");
		        }

        } catch (SQLException | IOException e) {

            status = "Failure";
            logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private String getRecords(List<String> rowData) {
        logger.info("[getSqlQuery] Returning Row Names"+rowData);

        String sql="";
        if(rowData.size()>0){

            for(int i=0;i<rowData.size();i++){
               // sql=sql+rowData.get(i)+",";

                if(rowData.get(i)==null || rowData.equals(""))
                {

                    sql += rowData.get(i) + ",";

                }
                else{
                    sql+="'"+rowData.get(i)+"'"+",";

                }


            }

        }

        sql+="'"+getDateValue()+"'";
        return sql=sql.substring(0,sql.length());


    }

    private String getColumnName(List<String> colNames) {

        logger.info("[getSqlQuery] Returning Column Names"+colNames);

        String sql="";
        if(colNames.size()>0){

            for(int i=0;i<colNames.size();i++){
                sql=sql+colNames.get(i)+",";
            }



        }
        sql=sql+"UPLOAD_DATE";

        return sql=sql.substring(0,sql.length());


    }


    private void xlsxFileInsert(String requestTableName, HttpServletResponse response) {

	    int numberOfRecords=0;
        XSSFWorkbook wb;
        XSSFSheet ws;
        int error = 0;
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            con = Rdbms.getAppConnection();
	        logger.debug("[xlsxFileInsert] Trying to Read the file "+fileBackupPath);

            wb = new XSSFWorkbook(new FileInputStream(fileBackupPath));
            ws = wb.getSheetAt(0);
            rowNum = ws.getLastRowNum() + 1;
            int rowNum = 0;
            columnNum = ws.getRow(0).getLastCellNum();
            String colNames = "";
            String psArgs = "";
            List<String> list=new LinkedList<>();
            List<String> celllist=null;
            Iterator<Row> rowIterator = ws.iterator();
            while (rowIterator.hasNext()) {
                int cellPos = 0;
                Row row1 = rowIterator.next();
                celllist=new ArrayList<>();
                for(int k=0;k<ws.getRow(0).getLastCellNum();k++){
                    Cell cell=row1.getCell(k,Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                    if(row1.getRowNum()==0) {
                        list.add(getCellValue(cell));
                    }
                    else if(row1.getRowNum()>0) {
                        if (cell == null || "".equals(cell)) {
                            System.out.println("Found Null value For");
                            celllist.add(null);
                            System.out.println(celllist + "--");
                        } else {
                            celllist.add(getCellValue(cell));

                        }
                    }
                } // end of for
                logger.info("[xlsFileInsert ] Printing Cell Values for Row "+celllist);
                boolean execute=false;

                if(celllist.size()>0) {
                    ps1=con.prepareStatement(sql1);
                    rs1=ps1.executeQuery();

                    while(rs1.next()){
                        dateValue=rs1.getString(1);

                    }
                    setDateValue(dateValue);
                    System.out.println(dateValue);
                    colNames = getColumnName(list);
                    psArgs = getRecords(celllist);
                    execute=true;
                }


                try {
                    if(execute) {
			            //System.out.println("[xlsxFileInsert] Inside the xlsx insert method--");
                        //System.out.println("[xlsxFileInsert] xlsx insert : "+"insert into "+requestTableName+" ("+colNames+") values ("+psArgs+")");

                        logger.debug("xlsx insert : "+"insert ingore into "+requestTableName+" ("+colNames+") values ("+psArgs+")");
                        ps = con.prepareStatement("insert ignore into "+requestTableName+" ("+colNames+") values ("+psArgs+")");
                        numberOfRecords=ps.executeUpdate();
                        //System.out.println("[xlsxFileInsert] Rows Inserted " + numberOfRecords);
			            psArgs = "";
                    }
                    count++;
                } catch (SQLException e) {
                    error++;
                    status = "Failure";
                    logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
                }
                rowNum++;
                numberOfRecords++;
            }
            if (error > 0) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Error in Inserting " + error + " records  Kindly check the logs for more information \");");
                out.println("</script>");

            }

            con.commit();
            con.close();
            if (error == 0) {
                status = "Success";
                out.println("<script type=\"text/javascript\">");
                out.println("alert(\"Records Inserted!! "  + " \");");
                out.println("window.close();");
                out.println("</script>");
            }

        } catch (SQLException | IOException e) {

            status = "Failure";
            logger.info("Exception while inserting the records ["+e.getMessage()+"and Cause ["+e.getCause()+"]");
            hmap.clear();
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

    }


}
