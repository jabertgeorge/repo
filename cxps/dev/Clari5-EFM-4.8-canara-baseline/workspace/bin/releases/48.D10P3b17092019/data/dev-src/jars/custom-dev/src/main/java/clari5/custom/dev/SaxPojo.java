/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clari5.custom.dev;

/**
 *
 * @author lakshmisai
 */
public class SaxPojo {

    private String strtOfTxt;
    private String prodId;
    private String relNum;
    private String stat;
    private String originator;
    private String responder;
    private String typ;
    private String dat;
    private String tranCde;
    private String fromAcctTyp;
    private String toAcctTyp;
    private String tranAmt;
    private String xmitDatTim;
    private String traceNum;
    private String tranTim;
    private String tranDat;
    private String expDat;
    private String capDat;
    private String pTTranSpclCde;
    private String num;
    private String seqNum;
    private String authId;
    private String respCde;
    private String termId;
    private String crdAccptIdCde;
    private String termOwner;
    private String termCity;
    private String termSt;
    private String termCntry;
    private String eventType;
    private String eventSubType;
    private String eventName;
    private String eventId;
    private String dateTime;
    private String panData;
    private String sourceId;
    private String hostId;
    private String sysTime;
    private String bin;
    private String fiid;
    private String channel;
    private String txnSecuredFlag;
    private String userId;
    private String userType;
    private String custId;
    private String deviceId;
    private String terminalId;
    private String merchantId;
    private String devOwnerId;
    private String tranDate;
    private String ipAddress;
    private String ipCountry;
    private String ipCity;
    private String stateCode;
    private String chipPinFlag;
    private String custName;
    private String succFailFlg;
    private String errorCode;
    private String errorDesc;
    private String ruleId;
    private String drAccountId;
    private String txnAmt;
    private String avlBal;
    private String crAccountId;
    private String crIFSCCode;
    private String payeeId;
    private String payeeName;
    private String tranType;
    private String entryMode;
    private String posEntryMode;
    private String countryCode;
    private String custCardId;
    private String MccCode;
    private String branchId;
    private String acctOwnership;
    private String acctOpenDate;
    private String partTranType;
    private String custMobNo;
    private String productCode;
    private String AtmDomLimit;
    private String AtmDIntrLimit;
    private String posEcomDomLimit;
    private String posEcomIntrLimit;
    private String maskCardNo;
    private String tranParticular;


    public String getStrtOfTxt() {
        return strtOfTxt;
    }

    public void setStrtOfTxt(String strtOfTxt) {
        this.strtOfTxt = strtOfTxt;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getRelNum() {
        return relNum;
    }

    public void setRelNum(String relNum) {
        this.relNum = relNum;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getDat() {
        return dat;
    }

    public void setDat(String dat) {
        this.dat = dat;
    }

    public String getTranCde() {
        return tranCde;
    }

    public void setTranCde(String tranCde) {
        this.tranCde = tranCde;
    }

    public String getFromAcctTyp() {
        return fromAcctTyp;
    }

    public void setFromAcctTyp(String fromAcctTyp) {
        this.fromAcctTyp = fromAcctTyp;
    }

    public String getToAcctTyp() {
        return toAcctTyp;
    }

    public void setToAcctTyp(String toAcctTyp) {
        this.toAcctTyp = toAcctTyp;
    }

    public String getTranAmt() {
        return tranAmt;
    }

    public void setTranAmt(String tranAmt) {
        this.tranAmt = tranAmt;
    }

    public String getXmitDatTim() {
        return xmitDatTim;
    }

    public void setXmitDatTim(String xmitDatTim) {
        this.xmitDatTim = xmitDatTim;
    }

    public String getTraceNum() {
        return traceNum;
    }

    public void setTraceNum(String traceNum) {
        this.traceNum = traceNum;
    }

    public String getTranTim() {
        return tranTim;
    }

    public void setTranTim(String tranTim) {
        this.tranTim = tranTim;
    }

    public String getTranDat() {
        return tranDat;
    }

    public void setTranDat(String tranDat) {
        this.tranDat = tranDat;
    }

    public String getExpDat() {
        return expDat;
    }

    public void setExpDat(String expDat) {
        this.expDat = expDat;
    }

    public String getCapDat() {
        return capDat;
    }

    public void setCapDat(String capDat) {
        this.capDat = capDat;
    }

    public String getpTTranSpclCde() {
        return pTTranSpclCde;
    }

    public void setpTTranSpclCde(String pTTranSpclCde) {
        this.pTTranSpclCde = pTTranSpclCde;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(String seqNum) {
        this.seqNum = seqNum;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getRespCde() {
        return respCde;
    }

    public void setRespCde(String respCde) {
        this.respCde = respCde;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getCrdAccptIdCde() {
        return crdAccptIdCde;
    }

    public void setCrdAccptIdCde(String crdAccptIdCde) {
        this.crdAccptIdCde = crdAccptIdCde;
    }

    public String getTermOwner() {
        return termOwner;
    }

    public void setTermOwner(String termOwner) {
        this.termOwner = termOwner;
    }

    public String getTermCity() {
        return termCity;
    }

    public void setTermCity(String termCity) {
        this.termCity = termCity;
    }

    public String getTermSt() {
        return termSt;
    }

    public void setTermSt(String termSt) {
        this.termSt = termSt;
    }

    public String getTermCntry() {
        return termCntry;
    }

    public void setTermCntry(String termCntry) {
        this.termCntry = termCntry;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventSubType() {
        return eventSubType;
    }

    public void setEventSubType(String eventSubType) {
        this.eventSubType = eventSubType;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPanData() {
        return panData;
    }

    public void setPanData(String panData) {
        this.panData = panData;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getSysTime() {
        return sysTime;
    }

    public void setSysTime(String sysTime) {
        this.sysTime = sysTime;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getFiid() {
        return fiid;
    }

    public void setFiid(String fiid) {
        this.fiid = fiid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTxnSecuredFlag() {
        return txnSecuredFlag;
    }

    public void setTxnSecuredFlag(String txnSecuredFlag) {
        this.txnSecuredFlag = txnSecuredFlag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getDevOwnerId() {
        return devOwnerId;
    }

    public void setDevOwnerId(String devOwnerId) {
        this.devOwnerId = devOwnerId;
    }

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpCountry() {
        return ipCountry;
    }

    public void setIpCountry(String ipCountry) {
        this.ipCountry = ipCountry;
    }

    public String getIpCity() {
        return ipCity;
    }

    public void setIpCity(String ipCity) {
        this.ipCity = ipCity;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getChipPinFlag() {
        return chipPinFlag;
    }

    public void setChipPinFlag(String chipPinFlag) {
        this.chipPinFlag = chipPinFlag;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getSuccFailFlg() {
        return succFailFlg;
    }

    public void setSuccFailFlg(String succFailFlg) {
        this.succFailFlg = succFailFlg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getDrAccountId() {
        return drAccountId;
    }

    public void setDrAccountId(String drAccountId) {
        this.drAccountId = drAccountId;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getAvlBal() {
        return avlBal;
    }

    public void setAvlBal(String avlBal) {
        this.avlBal = avlBal;
    }

    public String getCrAccountId() {
        return crAccountId;
    }

    public void setCrAccountId(String crAccountId) {
        this.crAccountId = crAccountId;
    }

    public String getCrIFSCCode() {
        return crIFSCCode;
    }

    public void setCrIFSCCode(String crIFSCCode) {
        this.crIFSCCode = crIFSCCode;
    }

    public String getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public String getEntryMode() {
        return entryMode;
    }

    public void setEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    public String getPosEntryMode() {
        return posEntryMode;
    }

    public void setPosEntryMode(String posEntryMode) {
        this.posEntryMode = posEntryMode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCustCardId() {
        return custCardId;
    }

    public void setCustCardId(String custCardId) {
        this.custCardId = custCardId;
    }

    public String getMccCode() {
        return MccCode;
    }

    public void setMccCode(String MccCode) {
        this.MccCode = MccCode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getAcctOwnership() {
        return acctOwnership;
    }

    public void setAcctOwnership(String acctOwnership) {
        this.acctOwnership = acctOwnership;
    }

    public String getAcctOpenDate() {
        return acctOpenDate;
    }

    public void setAcctOpenDate(String acctOpenDate) {
        this.acctOpenDate = acctOpenDate;
    }

    public String getPartTranType() {
        return partTranType;
    }

    public void setPartTranType(String partTranType) {
        this.partTranType = partTranType;
    }

    public String getCustMobNo() {
        return custMobNo;
    }

    public void setCustMobNo(String custMobNo) {
        this.custMobNo = custMobNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getAtmDomLimit() {
        return AtmDomLimit;
    }

    public void setAtmDomLimit(String AtmDomLimit) {
        this.AtmDomLimit = AtmDomLimit;
    }

    public String getAtmDIntrLimit() {
        return AtmDIntrLimit;
    }

    public void setAtmDIntrLimit(String AtmDIntrLimit) {
        this.AtmDIntrLimit = AtmDIntrLimit;
    }

    public String getPosEcomDomLimit() {
        return posEcomDomLimit;
    }

    public void setPosEcomDomLimit(String posEcomDomLimit) {
        this.posEcomDomLimit = posEcomDomLimit;
    }

    public String getPosEcomIntrLimit() {
        return posEcomIntrLimit;
    }

    public void setPosEcomIntrLimit(String posEcomIntrLimit) {
        this.posEcomIntrLimit = posEcomIntrLimit;
    }

    public String getMaskCardNo() {
        return maskCardNo;
    }

    public void setMaskCardNo(String maskCardNo) {
        this.maskCardNo = maskCardNo;
    }

    public String getTranParticular() {
        return tranParticular;
    }

    public void setTranParticular(String tranParticular) {
        this.tranParticular = tranParticular;
    }
}