// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;

import static cxps.noesis.core.EventHelper.ipToCountryCode;


@Table(Name="EVENT_FT_FUNDSTRANSFER", Schema="rice")
public class FT_FundsTransferEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=11) public Double effAvlBal;
       @Field(size=20) public String benificaryId;
       @Field(size=10) public String channel;
       @Field(size=20) public String ipCountry;
       @Field(size=10) public String mobileNo;
       @Field(size=50) public String payeeId;
       @Field(size=50) public String merchantId;
       @Field(size=20) public String tranMode;
       @Field(size=20) public String userType;
       @Field(size=11) public Double avlBal;
       @Field(size=2) public String succFailFlg;
       @Field(size=5) public String partTranType;
       @Field(size=20) public String errorDesc;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=50) public String custName;
       @Field(size=50) public String benificaryName;
       @Field(size=20) public String mccCode;
       @Field(size=11) public Double dailyLimit;
       @Field(size=11) public Double txnAmt;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String accountOwnership;
       @Field(size=50) public String deviceId;
       @Field(size=20) public String ipAddress;
       @Field(size=11) public Double avgTxnAmt;
       @Field(size=2) public String hostId;
       @Field(size=10) public String countryCode;
       @Field(size=20) public String userId;
       @Field(size=20) public String ipCity;
       @Field(size=20) public String crIfscCode;
       @Field(size=20) public String txnType;
       @Field(size=20) public String drAccountId;
       @Field(size=50) public String payeeName;
       @Field(size=20) public String netBankingType;
       @Field(size=10) public String errorCode;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String custId;
       @Field(size=20) public String crAccountId;


    @JsonIgnore
    public ITable<FT_FundsTransferEvent> t = AEF.getITable(this);

	public FT_FundsTransferEvent(){}

    public FT_FundsTransferEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("FundsTransfer");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setEffAvlBal(EventHelper.toDouble(json.getString("eff_avl_bal")));
            setBenificaryId((json.getString("benificary_id") != null) ? (json.getString("benificary_id")) : "NA");
            setChannel(json.getString("channel"));
            setIpCountry(json.getString("ip_country"));
            setMobileNo(json.getString("mobile_no"));
            setPayeeId(json.getString("payee_id"));
            setMerchantId(json.getString("merchant_id"));
            setTranMode(json.getString("tran_mode"));
            setUserType(json.getString("user_type"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setPartTranType(json.getString("part_tran_type"));
            setErrorDesc(json.getString("error_desc"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustName(json.getString("cust_name"));
            setBenificaryName((json.getString("benificary_name") != null) ? (json.getString("benificary_name")) : "NA");
            setMccCode(json.getString("mcc_code"));
            setDailyLimit(EventHelper.toDouble(json.getString("daily_limit")));
            setTxnAmt(EventHelper.toDouble(json.getString("txn_amt")));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setAccountOwnership(json.getString("account_ownership"));
            setDeviceId(json.getString("device_id"));
            setIpAddress(json.getString("ip_address"));
            setAvgTxnAmt(EventHelper.toDouble(json.getString("avg_txn_amt")));
            setHostId(json.getString("host_id"));
            setCountryCode(json.getString("country_code"));
            setUserId(json.getString("user_id"));
            setIpCity(json.getString("ip_city"));
            setCrIfscCode(json.getString("cr_ifsc_code"));
            setTxnType(json.getString("txn_type"));
            setDrAccountId(json.getString("dr_account_id").trim());
            setPayeeName(json.getString("payee_name"));
            setNetBankingType(json.getString("net_banking_type"));
            setErrorCode(json.getString("error_code"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setCustId(json.getString("cust_id"));
            setCrAccountId(json.getString("cr_account_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {

        if(getIpAddress() != null)
        {
            setCountryCode(ipToCountryCode(getIpAddress()));
        }
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FTT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public Double getEffAvlBal(){ return effAvlBal; }

    public String getBenificaryId(){ return benificaryId; }

    public String getChannel(){ return channel; }

    public String getIpCountry(){ return ipCountry; }

    public String getMobileNo(){ return mobileNo; }

    public String getPayeeId(){ return payeeId; }

    public String getMerchantId(){ return merchantId; }

    public String getTranMode(){ return tranMode; }

    public String getUserType(){ return userType; }

    public Double getAvlBal(){ return avlBal; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getPartTranType(){ return partTranType; }

    public String getErrorDesc(){ return errorDesc; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustName(){ return custName; }

    public String getBenificaryName(){ return benificaryName; }

    public String getMccCode(){ return mccCode; }

    public Double getDailyLimit(){ return dailyLimit; }

    public Double getTxnAmt(){ return txnAmt; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getAccountOwnership(){ return accountOwnership; }

    public String getDeviceId(){ return deviceId; }

    public String getIpAddress(){ return ipAddress; }

    public Double getAvgTxnAmt(){ return avgTxnAmt; }

    public String getHostId(){ return hostId; }

    public String getCountryCode(){ return countryCode; }

    public String getUserId(){ return userId; }

    public String getIpCity(){ return ipCity; }

    public String getCrIfscCode(){ return crIfscCode; }

    public String getTxnType(){ return txnType; }

    public String getDrAccountId(){ return drAccountId; }

    public String getPayeeName(){ return payeeName; }

    public String getNetBankingType(){ return netBankingType; }

    public String getErrorCode(){ return errorCode; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getCustId(){ return custId; }

    public String getCrAccountId(){ return crAccountId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setEffAvlBal(Double val){ this.effAvlBal = val; }
    public void setBenificaryId(String val){ this.benificaryId = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setTranMode(String val){ this.tranMode = val; }
    public void setUserType(String val){ this.userType = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setPartTranType(String val){ this.partTranType = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustName(String val){ this.custName = val; }
    public void setBenificaryName(String val){ this.benificaryName = val; }
    public void setMccCode(String val){ this.mccCode = val; }
    public void setDailyLimit(Double val){ this.dailyLimit = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setAccountOwnership(String val){ this.accountOwnership = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setAvgTxnAmt(Double val){ this.avgTxnAmt = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setCrIfscCode(String val){ this.crIfscCode = val; }
    public void setTxnType(String val){ this.txnType = val; }
    public void setDrAccountId(String val){ this.drAccountId = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setNetBankingType(String val){ this.netBankingType = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setCrAccountId(String val){ this.crAccountId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_FundsTransferEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= null;
        String accountKey1= null;



        if(this. partTranType.equalsIgnoreCase("D") && !this.channel.equalsIgnoreCase("MB"))
        {
            accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.drAccountId.trim());
      
        }else if (this.partTranType.equalsIgnoreCase("C") && !this.channel.equalsIgnoreCase("MB")){
           accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.crAccountId);
        }
        else if(this.channel.equalsIgnoreCase("MB"))
        {
            accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.drAccountId.trim());
            accountKey1=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.crAccountId);

        }
        else {
            accountKey=  h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.drAccountId.trim());
        }
        if( accountKey != null) {
           wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        }
        if( accountKey1 != null){
            wsInfoSet.add(new WorkspaceInfo("Account", accountKey1));
        }


     String noncustomerKey= null;

     if(this. txnType.equalsIgnoreCase("CM")||this. txnType.equalsIgnoreCase("SM") || this.txnType.equalsIgnoreCase("CR") || this.txnType.equalsIgnoreCase("DR") && !getMobileNo().equalsIgnoreCase("919999999999") && !getMobileNo().equalsIgnoreCase("912222222222"))
       {

          noncustomerKey=  h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.mobileNo);

      
       }else if (this.txnType.equalsIgnoreCase("XFT")){
          noncustomerKey=  h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.drAccountId+this.crAccountId);
       }
         else {
            if(getBenificaryId() != null && !getBenificaryId().equalsIgnoreCase("NA")){
               noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.benificaryId);
            }
          else {
                noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.payeeId);
            }
       }

       if(noncustomerKey != null) {
           wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
       }


       if(getCustId() != null && !getCustId().equalsIgnoreCase("NA") && !getCustId().equalsIgnoreCase("919999999999") && !getCustId().equalsIgnoreCase("912222222222")) {
           String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
           wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
       }

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_FundsTransfer");
        json.put("event_type", "FT");
        json.put("event_sub_type", "FundsTransfer");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("cust_id",getCustId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
