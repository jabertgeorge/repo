package clari5.upload.ui;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.*;
import java.util.Date;
import cxps.apex.utils.CxpsLogger;

import clari5.rdbms.Rdbms;


/**
 * Servlet implementation class UpdateTable
 */

public class UpdateTable extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static CxpsLogger logger = CxpsLogger.getLogger(UpdateTable.class);


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    private void doEither(HttpServletRequest request, HttpServletResponse response) throws IOException {

        PrintWriter out;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HttpSession session;
        ResultSetMetaData rsData;
        session = request.getSession();
        String tableName;
        int columnCount;
        response.setContentType("text/html");
        out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' type='text/css' href='upload/css/pagination.css'>");
        out.println("<script type='text/javascript' src='upload/js/jquery.min.js'></script>");
        out.println("<script type='text/javascript' src='upload/js/jquery.dataTables.min.js'></script>");
        out.println("<script type='text/javascript' src='upload/js/search.js'>");
        out.println("</script>");
        out.println(" <link rel='stylesheet' href='upload/css/bootstrap.min.css'>");
        out.println("<script src='upload/js/bootstrap.min.js'></script>");
        out.println("<script type='text/javascript' src='upload/js/paginationScript.js'>");
        out.println("</script>");
        out.println("</head>");
        tableName = request.getParameter("table_name");
        logger.info("Table Name in UpdateTable [" + tableName + "]");
        try {

            con = Rdbms.getAppConnection();
            String query  = "Select * from "+tableName;
            logger.info("Query ["+query+"]");
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            session = request.getSession();
            session.setAttribute("myId", tableName);
            //if (session == null || session.getAttribute("userId")== null){
            //    request.getRequestDispatcher("/expiry.jsp").forward(request,response);
			//}

            rsData = rs.getMetaData();
            columnCount = rsData.getColumnCount();
            out.println("<body>");
            out.println("<div class='row'><div class='col-xs-4'></div>");
            out.println("<div class='col-xs-4'>");
            out.println("<h4 font-family: 'Verdana'>Table Selected : " + tableName + "</h4></div>");
            out.println("<div class='col-xs-4'></div></div>");
            out.println("<br>");
            //textbox for search
            out.println("<div class='row'><div class='col-xs-4'></div>");
            out.println("<div class='col-xs-4'>");
            out.println("<input class='form-control' id='filter' placeholder='Enter The Value to Search'/>");
            out.println("</div>");
            out.println("<div class='col-xs-4'>");
            out.println("</div></div><br>");
            out.println("<div class='row'>");
            out.println("<div class='col-xs-1'>");
            out.println("</div>");
            out.println("<div class='col-xs-8'>");
            //To display the table data on UI
            out.println("<table class='table table-bordered table-hover'id='tablepaging'>");
            out.println("<tr>");
            out.println("<thead class='bg-info'>");
            String columnHeader = "";
            for (int i = 1; i <= columnCount; i++) {
                String s = rsData.getColumnName(i);
                columnHeader += s + ",";
                out.println("<th>" + s + "</th>");
            }
            out.println("<th>Action</th>");
            out.println("</thead>");
            out.println("</tr>");

            columnHeader += "~";
            while (rs.next()) {
                String columnValues = "";
                out.println("<tr>");

                for (int i = 1; i <= columnCount; i++) {
                    if ("date".equals(rs.getMetaData().getColumnTypeName(i).toLowerCase())) {
                        if(rs.getDate(i)!=null){
                            Date s = rs.getDate(i);
                            columnValues += s.toString() + ",";
                            out.println("<td>" + s + "</td>");

                        }
                        else{
                           // Date s = rs.getDate(i);
                            //columnValues += s.toString() + ",";
                            String s=null;
                            columnValues+=s+",";
                            out.println("<td>" + s + "</td>");
                        }
                    } else if("timestamp".equals(rs.getMetaData().getColumnTypeName(i).toLowerCase())) {
                        //Timestamp timestamp = rs.getTimestamp(i);
                        //timestamp.getTime();
                        //columnValues += timestamp.toString() + ",";
                        out.println("<td></td>");
                    } else {
                        String s = rs.getString(i);
                        columnValues += s + ",";
                        out.println("<td>" + s + "</td>");
                    }
                }
                String columnHeaderValues = columnHeader + columnValues;
                logger.info("Update values ["+columnHeaderValues+"]");
                out.println("<td><a href=upload/updateTableFile.jsp?id=" + URLEncoder.encode(columnHeaderValues, "UTF-8") + ">Edit</a></td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</div> <div class='col-xs-3'></div> </div>");

            //pagination
            out.println("<br>");
            out.println("<div id='pageNavPosition' style='padding-top: 20px' align='center'>");
            out.println("</div>");
            out.println("<script type='text/javascript'>");
            out.println("var pager = new Pager('tablepaging', 5,'pager','pageNavPosition')");
            out.println("pager.init()");
            out.println("pager.showPage(1)");
            out.println("</script>");
            out.println("</body></html>");


        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while showing the update table details [" + e.getMessage() + "] Cause [" + e.getCause() + "]");
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (rs != null) rs.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
