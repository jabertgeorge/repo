package clari5.custom.cms;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.hfdb.Hfdb;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.jira.JiraClientException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Evidence;
import cxps.eoi.EvidenceData;
import cxps.eoi.IncidentEvent;
import org.json.JSONException;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by bisman on 12/16/16.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    protected static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);


    public CustomCmsFormatter() throws CmsException {
    }


    @Override
    public String getParentEntityId(IncidentEvent incidentEvent) {
        String entityName = incidentEvent.getEntityName();
        //Written By Puskar
        /*
        This is to populate mask card number in Case and Incidents
        For this code to work please add mask Card no in Scenario evidence
        If maskcard no is not added in scenario this will return default value.
         */
        try {
            if (entityName != null && !"".equals(entityName) && "paymentcard".equalsIgnoreCase(entityName)) {
                Evidence evidence = (Evidence) incidentEvent.getEvidence();
                EvidenceData evidenceData = evidence.getEvidenceData().get(0);
                HashSet<String> maskCardset = (HashSet) evidenceData.getValues("maskCardNo");
                String maskCardNo = maskCardset.stream().findFirst().orElse(null);
                System.out.println(" Mask Card no " + maskCardNo);
                if (!"".equalsIgnoreCase(maskCardNo) && maskCardNo != null)
                    return maskCardNo;
                else
                    return incidentEvent.getCaseEntityId();
            }
        } catch (Exception e) {
            System.err.print(" Exception while fetching maskcard no for FactName" + incidentEvent.getFactname() + " Entity id " + incidentEvent.getEntityId());
            e.printStackTrace();
        }
        return incidentEvent.getCaseEntityId();
    }

    public String getUserWithLeastCase(Map<String, Integer> map) {
        Set<Entry<String, Integer>> set = map.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        for (Map.Entry<String, Integer> entry : list) {
            logger.info(entry.getKey() + " ==== " + entry.getValue());
        }
        String user = list.get(0).getKey();

        logger.info("User with least no of cases " + user);
        return user;
    }

    public static String checkSummarylength(String summary) {
        if (summary.length() > 250) {
            System.out.println("Summary length is " + summary.length());
            summary = summary.substring(0, 251) + "...";
            System.out.println("final summary is " + summary);
            return summary;
        } else {
            return summary;
        }
    }

    public CxJson populateIncidentJson(IncidentEvent event, String jiraParentId, boolean isupdate) throws CmsException, IOException {
      /*  CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.getModuleId());
        String entityName=event.getEntityName();
        CxJson json = CMSUtility.getInstance(event.getModuleId()).populateIncidentJson(event, jiraParentId, isupdate);
        String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();
        String eventJson = event.convertToJson(event);
        System.out.println("Display Key: " + event.getCaseDisplayKey());
        Hocon h = new Hocon(eventJson);
        CxJson wrapper = json.get("fields");
        String displayKey = h.getString("displayKey");
        String plainCardNum = displayKey;
        try {
            if (entityName != null && !entityName.equals("") && entityName.equalsIgnoreCase("paymentcard")) {
                String hashedCardNum = displayKey.split("\\|")[0].trim();
                plainCardNum = CardDecryptor.getUnshashedCard(hashedCardNum);
                displayKey = plainCardNum;
            } else if (entityName != null && !entityName.equals("") && entityName.equalsIgnoreCase("noncustomer")) {
                logger.info("NonCustomer scenario");
                String entityId = displayKey.split("\\|")[0].trim();
                logger.info("Entity id is " + entityId);
                if (entityId != null && !"".equalsIgnoreCase(entityId) && entityId.endsWith("card")) {
                    String hashedCardNum = entityId.substring(0, 16);
                    plainCardNum = CardDecryptor.getUnshashedCard(hashedCardNum);
                    displayKey = plainCardNum;
                    displayKey = displayKey.concat(entityId.substring(16, entityId.length() - 4));
                }
            }
            String displayKeyJiraId = cmsMod.getCustomFields().get("CmsIncident-displayKey").getJiraId();
            wrapper.put(displayKeyJiraId, displayKey);
        } catch (Exception e) {
            logger.info("Exception occurred while unhashing cardnum");
            String displayKeyJiraId = cmsMod.getCustomFields().get("CmsIncident-displayKey").getJiraId();
            wrapper.put(displayKeyJiraId, displayKey);
       }
        String summary = displayKey + ":" + wrapper.getString(summaryId);
        wrapper.put("summary", checkSummarylength(summary));
        return json.put("fields", wrapper);*/
        return new CxJson();
    }


}

