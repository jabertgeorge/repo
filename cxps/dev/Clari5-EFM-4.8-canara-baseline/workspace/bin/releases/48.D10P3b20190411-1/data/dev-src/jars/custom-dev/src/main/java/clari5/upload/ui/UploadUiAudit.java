package clari5.upload.ui;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import cxps.apex.utils.CxpsLogger;

import clari5.rdbms.Rdbms;

/**
 * Created by didhin
 * Date :  8/4/16.
 */
public class UploadUiAudit {
    private static CxpsLogger logger = CxpsLogger.getLogger(UploadUiAudit.class);

    public static String updateAudit(String tableName, String userId, String status, String primaryKey, String FileName, String action) {
        Connection con = null;
        PreparedStatement ps = null;
        String sql;
        sql = "INSERT INTO UPLOAD_FILE_AUDIT(USERNAME,TABLENAME,ACTION,PRIMARYKEY,FILENAME,STATUS,DATEUPLOAD) VALUES('" +userId+"','" + tableName + "','" + action + "','" + primaryKey + "','" + FileName + "','" + status + "',CURRENT_TIMESTAMP)";

        logger.info("Audit table insert Query [" + sql + "]");


        try {
            con = Rdbms.getAppConnection();
            ps = con.prepareStatement(sql);
            ps.executeQuery();
            con.commit();
            status = "Success";
        } catch (Exception e) {
	    e.printStackTrace();
            logger.info("Exception in Inserting into Audit Table [" + e.getMessage() + "] Cause [" + e.getCause() + "]");
            status = "Failure";
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return status;
    }
}
