clari5.hfdb.entity.nft_mbregistration{ 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = event_id, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = user_id ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = app_id ,type = "string:20"}
{name = device_id ,type = "string:20"}
{name = error_code ,type = "string:10"}
{name = country_code ,type = "string:10"}
{name = succ_fail_flg ,type = "string:2"}
{name = mobile_no ,type =  "string:10"}
{name = ip_address ,type = "string:20"}
{name = error_desc ,type = "string:100"}
{name = tran_date ,type = timestamp}
{name = pwd_reset_type ,type = "string:20"}
        ]
}
