clari5.hfdb.entity.nft_login { 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = event_id, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = channel ,type = "string:20"}
{name = user_id ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = user_type ,type = "string:20"}
{name = device_id ,type = "string:50"}
{name = ip_address ,type = "string:20"}
{name = ip_country ,type = "string:20"}
{name = ip_city ,type = "string:20"}
{name = error_code ,type = "string:10"}
{name = error_desc ,type = "string:100"}
{name = succ_fail_flg ,type = "string:2"}
{name = last_login_ip ,type = "string:20"}
{name = last_login_ts ,type = timestamp}
{name = cust_name ,type = "string:50"}
{name = app_id ,type = "string:20"}
{name = country_code ,type = "string:20"}
{name = account_id ,type = "string:20"}
{name = mac_id ,type = "string:50"}
{name = mobile_no ,type = "string:10"}
{name = tran_date ,type = timestamp}
        ]
}
