clari5.hfdb.entity.nft_payeereg { 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = event_id, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = channel ,type = "string:10"}
{name = user_id ,type = "string:20"}
{name = user_type ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = device_id ,type = "string:50"}
{name = ip_address ,type = "string:20"}
{name = ip_country ,type = "string:20"}
{name = ip_city ,type = "string:20"}
{name = succ_fail_flg ,type = "string:2"}
{name = error_code ,type = "string:10"}
{name = error_desc ,type = "string:100"}
{name = payee_type ,type = "string:20"}
{name = payee_id ,type = "string:50"}
{name = payee_name ,type = "string:50"}
{name = payee_nickname ,type = "string:20"}
{name = payee_accounttype ,type = "string:20"}
{name = payee_ifsc_code ,type = "string:20"}
{name = payee_account_id ,type = "string:20"}
{name = payee_bank_name ,type = "string:50"}
{name = payee_bank_id ,type = "string:50"}
{name = payee_branch_name ,type = "string:50"}
{name = payee_branch_id ,type = "string:50"}
{name = cust_name ,type = "string:50"}
{name = tran_date ,type = timestamp}
{name = payeeactivation_flg ,type = "string:2"}

        ]
}
