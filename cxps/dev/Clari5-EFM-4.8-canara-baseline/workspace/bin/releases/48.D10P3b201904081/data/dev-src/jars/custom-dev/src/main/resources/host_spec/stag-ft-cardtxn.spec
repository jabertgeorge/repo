clari5.hfdb.entity.ft_cardtxn { 
attributes=[ 
{ name =CL5_FLG,  type= "string:20"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = event_id, type="string:50", key = true} 
{name = host_id, type =  "string:5"}
{name = sys_time, type =  timestamp}
{name = channel, type =  "string:20"}
{name = user_id, type =  "string:20"}
{name = user_type, type =  "string:50"}
{name = cust_id, type =  "string:50"}
{name = device_id, type =  "string:50"}
{name = ip_address, type =  "string:20"}
{name = ip_country, type =  "string:20"}
{name = ip_city, type =  "string:20"}
{name = cust_name, type =  "string:50"}
{name = succ_fail_flg, type =  "string:2"}
{name = error_code, type =  "string:6"}
{name = error_desc, type =  "string:100"}
{name = dr_account_id, type =  "string:20"}
{name = txn_amt, type =  "number:20,3"}
{name = avl_bal, type =  "number:20,3"}
{name = cr_account_id, type =  "string:20"}
{name = cr_ifsc_code, type =  "string:20"}
{name = payee_id, type =  "string:20"}
{name = payee_name, type =  "string:50"}
{name = tran_type, type =  "string:10"}
{name = entry_mode, type =  "string:50"}
{name = branch_id, type =  "string:10"}
{name = acct_ownership, type =  "string:50"}
{name = acct_open_date, type =  timestamp}
{name = part_tran_type, type =  "string:50"}
{name = country_code, type =  "string:2"}
{name = cust_card_id, type =  "string:16"}
{name = bin, type =  "string:6"}
{name = mcc_code, type =  "string:20"}
{name = terminal_id, type =  "string:50"}
{name = merchant_id, type =  "string:50"}
{name = cust_mob_no, type =  "string:10"}
{name = txn_secured_flag, type =  "string:2"}
{name = product_code, type =  "string:50"}
{name = dev_owner_id, type =  "string:50"}
{name = atm_dom_limit, type =  "number:20,3"}
{name = atm_intr_limit, type =  "number:20,3"}
{name = pos_ecom_dom_limit, type =  "number:20,3"}
{name = pos_ecom_intr_limit, type =  "number:20,3"}
{name = tran_date, type =  timestamp}
{name = mask_card_no, type =  "string:20"}
{name = state_code, type =  "string:20"}
{name = tran_particular, type =  "string:200"}
{name = pos_entry_mode, type =  "string:5"}
{name = chip_pin_flg, type =  "string:10"}
]
}
