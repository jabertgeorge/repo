clari5.hfdb.entity.ft_fundstransfer { 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = event_id, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = channel ,type = "string:10"}
{name = user_id ,type = "string:20"}
{name = user_type ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = benificary_id ,type = "string:20"}
{name = benificary_name ,type = "string:50"}
{name = device_id ,type = "string:50"}
{name = ip_address ,type = "string:20"}
{name = ip_country ,type = "string:20"}
{name = ip_city ,type = "string:20"}
{name = cust_name ,type = "string:50"}
{name = succ_fail_flg ,type = "string:2"}
{name = error_code ,type = "string:10"}
{name = error_desc ,type = "string:20"}
{name = dr_account_id ,type = "string:20"}
{name = txn_amt ,type = "number:11,2"}
{name = avl_bal ,type = "number:11,2"}
{name = cr_account_id ,type = "string:20"}
{name = cr_ifsc_code ,type = "string:20"}
{name = payee_id ,type = "string:50"}
{name = payee_name ,type = "string:50"}
{name = txn_type ,type = "string:20"}
{name = account_ownership ,type = "string:20"}
{name = acct_open_date ,type = timestamp}
{name = avg_txn_amt ,type = "number:11,2"}
{name = part_tran_type ,type = "string:5"}
{name = daily_limit ,type = "number:11,2"}
{name = mcc_code ,type = "string:20"}
{name = tran_date ,type = timestamp}
{name = mobile_no ,type = "string:10"}
{name = country_code ,type = "string:10"}
{name = tran_mode ,type = "string:20"}
{name = merchant_id ,type = "string:50"}
{name = eff_avl_bal ,type = "number:11,2"}
{name = net_banking_type ,type = "string:20"}
        ]
}
