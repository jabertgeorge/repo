# Issue commands to the system using which it creates the dev-src

# ---------------------------------------------------------------
#  Examples of usage (using this you can extend the development)
# ---------------------------------------------------------------
# delete    *
# extend    ./conf
# replace   ./jars/custom-dev/src/main/resources/
# replace   ./jars/custom-dev/src/main/conf
# link      ./angular
# delete    ./jars/custom-dev/build

# ---------------------------------------------------------------
# Main configuration starts from here
# Overrides for SNAPSHOT
# ---------------------------------------------------------------
replace     ./jars/custom-dev
replace     ./settings.gradle
replace     ./KEN
replace     ./db
replace     ./extensions
