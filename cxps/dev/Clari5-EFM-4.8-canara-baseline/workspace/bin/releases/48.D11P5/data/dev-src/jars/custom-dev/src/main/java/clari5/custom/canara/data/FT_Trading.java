package clari5.custom.canara.data;

public class FT_Trading extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "FT_TRADING";
    private String event_type = "FT_TradingEvent";

    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String COUNTERPARTY;
    private String BUY_AMOUNT1;
    private String SELL_AMOUNT2;
    private String EXCHANGE_RATE;
    private String TRAN_REFERENCE;
    private String VALUE_DATE;
    private String TRAN_DATE;
    private String AMT_CURRENCY1;
    private String AMT_CURRENCY2;
    private String DEAL_NO;
    private String PRODUCT_CODE;
    private String STATUS_FLAG;
    private String MATURITY_AMT;

    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCOUNTERPARTY() {
        return COUNTERPARTY;
    }

    public void setCOUNTERPARTY(String COUNTERPARTY) {
        this.COUNTERPARTY = COUNTERPARTY;
    }

    public String getBUY_AMOUNT1() {
        return BUY_AMOUNT1;
    }

    public void setBUY_AMOUNT1(String BUY_AMOUNT1) {
        this.BUY_AMOUNT1 = BUY_AMOUNT1;
    }

    public String getSELL_AMOUNT2() {
        return SELL_AMOUNT2;
    }

    public void setSELL_AMOUNT2(String SELL_AMOUNT2) {
        this.SELL_AMOUNT2 = SELL_AMOUNT2;
    }

    public String getEXCHANGE_RATE() {
        return EXCHANGE_RATE;
    }

    public void setEXCHANGE_RATE(String EXCHANGE_RATE) {
        this.EXCHANGE_RATE = EXCHANGE_RATE;
    }

    public String getTRAN_REFERENCE() {
        return TRAN_REFERENCE;
    }

    public void setTRAN_REFERENCE(String TRAN_REFERENCE) {
        this.TRAN_REFERENCE = TRAN_REFERENCE;
    }

    public String getVALUE_DATE() {
        return VALUE_DATE;
    }

    public void setVALUE_DATE(String VALUE_DATE) {
        this.VALUE_DATE = VALUE_DATE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getAMT_CURRENCY1() {
        return AMT_CURRENCY1;
    }

    public void setAMT_CURRENCY1(String AMT_CURRENCY1) {
        this.AMT_CURRENCY1 = AMT_CURRENCY1;
    }

    public String getAMT_CURRENCY2() {
        return AMT_CURRENCY2;
    }

    public void setAMT_CURRENCY2(String AMT_CURRENCY2) {
        this.AMT_CURRENCY2 = AMT_CURRENCY2;
    }

    public String getDEAL_NO() {
        return DEAL_NO;
    }

    public void setDEAL_NO(String DEAL_NO) {
        this.DEAL_NO = DEAL_NO;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public void setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
    }

    public String getSTATUS_FLAG() {
        return STATUS_FLAG;
    }

    public void setSTATUS_FLAG(String STATUS_FLAG) {
        this.STATUS_FLAG = STATUS_FLAG;
    }

    public String getMATURITY_AMT() {
        return MATURITY_AMT;
    }

    public void setMATURITY_AMT(String MATURITY_AMT) {
        this.MATURITY_AMT = MATURITY_AMT;
    }
}
