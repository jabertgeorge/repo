cxps.events.event.nft_branchadvances{
table-name : EVENT_NFT_BRANCHADVANCES
  event-mnemonic: NBA
  workspaces : {
    BRANCH : branch-code
    }
  event-attributes : {
        branch-code: {db : true ,raw_name : BranchCode ,type : "string:20"}
        branch-name: {db : true ,raw_name : BranchName ,type : "string:100"}
        previous-month-advance-amt: {db : true ,raw_name : PreviousMonthAdvanceAmt, type : "number:18,2"}
        current-month-advance-amt: {db : true ,raw_name : CurrentMonthAdvanceAmt, type : "number:18,2"}
        previous-quarter-advance-amt: {db : true ,raw_name : PreviousQuarterAdvanceAmt, type : "number:18,2"}
        current-quarter-advance-amt: {db : true ,raw_name : CurrentQuarterAdvanceAmt, type : "number:18,2"}
        previous-day-advance-amt: {db : true ,raw_name : PreviousDayAdvanceAmt, type : "number:18,2"}
        current-day-advance-amt: {db : true ,raw_name : CurrentDayAdvanceAmt, type : "number:18,2"}
        event-date-time: {db : true ,raw_name : EventDateTime, type :timestamp}
        }
}
