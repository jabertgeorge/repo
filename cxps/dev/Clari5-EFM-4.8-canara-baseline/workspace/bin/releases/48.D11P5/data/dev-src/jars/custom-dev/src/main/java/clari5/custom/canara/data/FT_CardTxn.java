package clari5.custom.canara.data;

public class FT_CardTxn extends ITableData {

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "FT_CARDTXN";
    private String event_type = "FT_CardTxn";
    private String HOST_ID;
    private String SYS_TIME;
    private String ID;
    private String CHANNEL;
    private String USER_ID;
    private String USER_TYPE;
    private String DR_ACCOUNT_ID;
    private String CR_ACCOUNT_ID;
    private String CUST_ID;
    private String DEVICE_ID;
    private String IP_ADDRESS;
    private String IP_COUNTRY;
    private String IP_CITY;
    private String CUST_NAME;
    private String SUCC_FAIL_FLG;
    private String ERROR_CODE;
    private String ERROR_DESC;
    private String TXN_AMT;
    private String AVL_BAL;
    private String CR_IFSC_CODE;
    private String PAYEE_ID;
    private String PAYEE_NAME;
    private String TRAN_TYPE;
    private String ENTRY_MODE;
    private String BRANCH_ID;
    private String ACCT_OWNERSHIP;
    private String ACCT_OPEN_DATE;
    private String PART_TRAN_TYPE;
    private String COUNTRY_CODE;
    private String CUST_CARD_ID;
    private String BIN;
    private String MCC_CODE;
    private String TERMINAL_ID;
    private String MERCHANT_ID;
    private String CUST_MOB_NO;
    private String TXN_SECURED_FLAG;
    private String PRODUCT_CODE;
    private String DEV_OWNER_ID;
    private String ATM_DOM_LIMIT;
    private String ATM_INTR_LIMIT;
    private String POS_ECOM_DOM_LIMIT;
    private String POS_ECOM_INTR_LIMIT;
    private String TRAN_DATE;
    private String MASK_CARD_NO;
    private String STATE_CODE;
    private String TRAN_PARTICULAR;
    private String POS_ENTRY_MODE;
    private String CHIP_PIN_FLG;



    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSER_TYPE() {
        return USER_TYPE;
    }

    public void setUSER_TYPE(String USER_TYPE) {
        this.USER_TYPE = USER_TYPE;
    }

    public String getDR_ACCOUNT_ID() {
        return DR_ACCOUNT_ID;
    }

    public void setDR_ACCOUNT_ID(String DR_ACCOUNT_ID) {
        this.DR_ACCOUNT_ID = DR_ACCOUNT_ID;
    }

    public String getCR_ACCOUNT_ID() {
        return CR_ACCOUNT_ID;
    }

    public void setCR_ACCOUNT_ID(String CR_ACCOUNT_ID) {
        this.CR_ACCOUNT_ID = CR_ACCOUNT_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
    }

    public String getIP_COUNTRY() {
        return IP_COUNTRY;
    }

    public void setIP_COUNTRY(String IP_COUNTRY) {
        this.IP_COUNTRY = IP_COUNTRY;
    }

    public String getIP_CITY() {
        return IP_CITY;
    }

    public void setIP_CITY(String IP_CITY) {
        this.IP_CITY = IP_CITY;
    }

    public String getCUST_NAME() {
        return CUST_NAME;
    }

    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    public String getSUCC_FAIL_FLG() {
        return SUCC_FAIL_FLG;
    }

    public void setSUCC_FAIL_FLG(String SUCC_FAIL_FLG) {
        this.SUCC_FAIL_FLG = SUCC_FAIL_FLG;
    }

    public String getERROR_CODE() {
        return ERROR_CODE;
    }

    public void setERROR_CODE(String ERROR_CODE) {
        this.ERROR_CODE = ERROR_CODE;
    }

    public String getERROR_DESC() {
        return ERROR_DESC;
    }

    public void setERROR_DESC(String ERROR_DESC) {
        this.ERROR_DESC = ERROR_DESC;
    }

    public String getTXN_AMT() {
        return TXN_AMT;
    }

    public void setTXN_AMT(String TXN_AMT) {
        this.TXN_AMT = TXN_AMT;
    }

    public String getAVL_BAL() {
        return AVL_BAL;
    }

    public void setAVL_BAL(String AVL_BAL) {
        this.AVL_BAL = AVL_BAL;
    }

    public String getCR_IFSC_CODE() {
        return CR_IFSC_CODE;
    }

    public void setCR_IFSC_CODE(String CR_IFSC_CODE) {
        this.CR_IFSC_CODE = CR_IFSC_CODE;
    }

    public String getPAYEE_ID() {
        return PAYEE_ID;
    }

    public void setPAYEE_ID(String PAYEE_ID) {
        this.PAYEE_ID = PAYEE_ID;
    }

    public String getPAYEE_NAME() {
        return PAYEE_NAME;
    }

    public void setPAYEE_NAME(String PAYEE_NAME) {
        this.PAYEE_NAME = PAYEE_NAME;
    }

    public String getTRAN_TYPE() {
        return TRAN_TYPE;
    }

    public void setTRAN_TYPE(String TRAN_TYPE) {
        this.TRAN_TYPE = TRAN_TYPE;
    }

    public String getENTRY_MODE() {
        return ENTRY_MODE;
    }

    public void setENTRY_MODE(String ENTRY_MODE) {
        this.ENTRY_MODE = ENTRY_MODE;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getACCT_OWNERSHIP() {
        return ACCT_OWNERSHIP;
    }

    public void setACCT_OWNERSHIP(String ACCT_OWNERSHIP) {
        this.ACCT_OWNERSHIP = ACCT_OWNERSHIP;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getPART_TRAN_TYPE() {
        return PART_TRAN_TYPE;
    }

    public void setPART_TRAN_TYPE(String PART_TRAN_TYPE) {
        this.PART_TRAN_TYPE = PART_TRAN_TYPE;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getCUST_CARD_ID() {
        return CUST_CARD_ID;
    }

    public void setCUST_CARD_ID(String CUST_CARD_ID) {
        this.CUST_CARD_ID = CUST_CARD_ID;
    }

    public String getBIN() {
        return BIN;
    }

    public void setBIN(String BIN) {
        this.BIN = BIN;
    }

    public String getMCC_CODE() {
        return MCC_CODE;
    }

    public void setMCC_CODE(String MCC_CODE) {
        this.MCC_CODE = MCC_CODE;
    }

    public String getTERMINAL_ID() {
        return TERMINAL_ID;
    }

    public void setTERMINAL_ID(String TERMINAL_ID) {
        this.TERMINAL_ID = TERMINAL_ID;
    }

    public String getMERCHANT_ID() {
        return MERCHANT_ID;
    }

    public void setMERCHANT_ID(String MERCHANT_ID) {
        this.MERCHANT_ID = MERCHANT_ID;
    }

    public String getCUST_MOB_NO() {
        return CUST_MOB_NO;
    }

    public void setCUST_MOB_NO(String CUST_MOB_NO) {
        this.CUST_MOB_NO = CUST_MOB_NO;
    }

    public String getTXN_SECURED_FLAG() {
        return TXN_SECURED_FLAG;
    }

    public void setTXN_SECURED_FLAG(String TXN_SECURED_FLAG) {
        this.TXN_SECURED_FLAG = TXN_SECURED_FLAG;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public void setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
    }

    public String getDEV_OWNER_ID() {
        return DEV_OWNER_ID;
    }

    public void setDEV_OWNER_ID(String DEV_OWNER_ID) {
        this.DEV_OWNER_ID = DEV_OWNER_ID;
    }

    public String getATM_DOM_LIMIT() {
        return ATM_DOM_LIMIT;
    }

    public void setATM_DOM_LIMIT(String ATM_DOM_LIMIT) {
        this.ATM_DOM_LIMIT = ATM_DOM_LIMIT;
    }

    public String getATM_INTR_LIMIT() {
        return ATM_INTR_LIMIT;
    }

    public void setATM_INTR_LIMIT(String ATM_INTR_LIMIT) {
        this.ATM_INTR_LIMIT = ATM_INTR_LIMIT;
    }

    public String getPOS_ECOM_DOM_LIMIT() {
        return POS_ECOM_DOM_LIMIT;
    }

    public void setPOS_ECOM_DOM_LIMIT(String POS_ECOM_DOM_LIMIT) {
        this.POS_ECOM_DOM_LIMIT = POS_ECOM_DOM_LIMIT;
    }

    public String getPOS_ECOM_INTR_LIMIT() {
        return POS_ECOM_INTR_LIMIT;
    }

    public void setPOS_ECOM_INTR_LIMIT(String POS_ECOM_INTR_LIMIT) {
        this.POS_ECOM_INTR_LIMIT = POS_ECOM_INTR_LIMIT;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getMASK_CARD_NO() {
        return MASK_CARD_NO;
    }

    public void setMASK_CARD_NO(String MASK_CARD_NO) {
        this.MASK_CARD_NO = MASK_CARD_NO;
    }

    public String getSTATE_CODE() {
        return STATE_CODE;
    }

    public void setSTATE_CODE(String STATE_CODE) {
        this.STATE_CODE = STATE_CODE;
    }

    public String getTRAN_PARTICULAR() {
        return TRAN_PARTICULAR;
    }

    public void setTRAN_PARTICULAR(String TRAN_PARTICULAR) {
        this.TRAN_PARTICULAR = TRAN_PARTICULAR;
    }

    public String getPOS_ENTRY_MODE() {
        return POS_ENTRY_MODE;
    }

    public void setPOS_ENTRY_MODE(String POS_ENTRY_MODE) {
        this.POS_ENTRY_MODE = POS_ENTRY_MODE;
    }

    public String getCHIP_PIN_FLG() {
        return CHIP_PIN_FLG;
    }

    public void setCHIP_PIN_FLG(String CHIP_PIN_FLG) {
        this.CHIP_PIN_FLG = CHIP_PIN_FLG;
    }



}
