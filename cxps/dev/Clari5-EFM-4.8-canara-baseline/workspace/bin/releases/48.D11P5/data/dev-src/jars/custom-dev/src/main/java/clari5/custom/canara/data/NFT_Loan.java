package clari5.custom.canara.data;

public class NFT_Loan extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_LOAN";
    private String event_type = "NFT_Loan";
    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String SYS_TIME;
    private String ID;
    private String HOST_ID;
    private String CUST_ID;
    private String ACCOUNT_ID;
    private String SCHEME_TYPE;
    private String SCHEME_CODE;
    private String SYSTEM_TYPE;
    private String GL_CODE;
    private String LOAN_AMT;
    private String OPENED_DATE_TIME;


    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getSCHEME_TYPE() {
        return SCHEME_TYPE;
    }

    public void setSCHEME_TYPE(String SCHEME_TYPE) {
        this.SCHEME_TYPE = SCHEME_TYPE;
    }

    public String getSCHEME_CODE() {
        return SCHEME_CODE;
    }

    public void setSCHEME_CODE(String SCHEME_CODE) {
        this.SCHEME_CODE = SCHEME_CODE;
    }

    public String getSYSTEM_TYPE() {
        return SYSTEM_TYPE;
    }

    public void setSYSTEM_TYPE(String SYSTEM_TYPE) {
        this.SYSTEM_TYPE = SYSTEM_TYPE;
    }

    public String getGL_CODE() {
        return GL_CODE;
    }

    public void setGL_CODE(String GL_CODE) {
        this.GL_CODE = GL_CODE;
    }

    public String getLOAN_AMT() {
        return LOAN_AMT;
    }

    public void setLOAN_AMT(String LOAN_AMT) {
        this.LOAN_AMT = LOAN_AMT;
    }

    public String getOPENED_DATE_TIME() {
        return OPENED_DATE_TIME;
    }

    public void setOPENED_DATE_TIME(String OPENED_DATE_TIME) {
        this.OPENED_DATE_TIME = OPENED_DATE_TIME;
    }






}
