// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_FiTxnEventMapper extends EventMapper<FT_FiTxnEvent> {

public FT_FiTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_FiTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_FiTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_FiTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_FiTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_FiTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_FiTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getOriginIpAddr());
            preparedStatement.setString(i++, obj.getPayeeCode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getPayerAcctId());
            preparedStatement.setString(i++, obj.getPayerName());
            preparedStatement.setString(i++, obj.getMerState());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getPayeeAcctId());
            preparedStatement.setString(i++, obj.getPayerIfscCode());
            preparedStatement.setString(i++, obj.getAgentCode());
            preparedStatement.setString(i++, obj.getTranDesc());
            preparedStatement.setString(i++, obj.getMerCityName());
            preparedStatement.setString(i++, obj.getSegment());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getHostUserId());
            preparedStatement.setString(i++, obj.getMerchantBcFlg());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getMerNameAddr());
            preparedStatement.setString(i++, obj.getCardNumber());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getMerCountryCode());
            preparedStatement.setTimestamp(i++, obj.getLastOffusMbcTxn());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getTranRmks());
            preparedStatement.setString(i++, obj.getTerminalId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setTimestamp(i++, obj.getLastMbcTxn());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getErrorCodeDescription());
            preparedStatement.setString(i++, obj.getPayeeIfscCode());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getAcctStatus());
            preparedStatement.setString(i++, obj.getBcAccountNo());
            preparedStatement.setString(i++, obj.getCorporateId());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getPayeeNickName());
            preparedStatement.setString(i++, obj.getBcMobileNo());
            preparedStatement.setString(i++, obj.getMbcLocation());
            preparedStatement.setString(i++, obj.getPayerNickName());
            preparedStatement.setString(i++, obj.getPayerCode());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getAuthType());
            preparedStatement.setString(i++, obj.getPayerId());
            preparedStatement.setString(i++, obj.getNetworkType());
            preparedStatement.setString(i++, obj.getIsPayerBankCust());
            preparedStatement.setString(i++, obj.getIsPayeeBankCust());
            preparedStatement.setString(i++, obj.getBcDistrictCode());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getMobileNo());
            preparedStatement.setString(i++, obj.getMerchantId());
            preparedStatement.setString(i++, obj.getPayeeMobNo());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_FITXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_FiTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_FiTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FITXN"));
        putList = new ArrayList<>();

        for (FT_FiTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ORIGIN_IP_ADDR",  obj.getOriginIpAddr());
            p = this.insert(p, "PAYEE_CODE",  obj.getPayeeCode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "PAYER_ACCT_ID",  obj.getPayerAcctId());
            p = this.insert(p, "PAYER_NAME",  obj.getPayerName());
            p = this.insert(p, "MER_STATE",  obj.getMerState());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "PAYEE_ACCT_ID",  obj.getPayeeAcctId());
            p = this.insert(p, "PAYER_IFSC_CODE",  obj.getPayerIfscCode());
            p = this.insert(p, "AGENT_CODE",  obj.getAgentCode());
            p = this.insert(p, "TRAN_DESC",  obj.getTranDesc());
            p = this.insert(p, "MER_CITY_NAME",  obj.getMerCityName());
            p = this.insert(p, "SEGMENT",  obj.getSegment());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "HOST_USER_ID",  obj.getHostUserId());
            p = this.insert(p, "MERCHANT_BC_FLG",  obj.getMerchantBcFlg());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "MER_NAME_ADDR",  obj.getMerNameAddr());
            p = this.insert(p, "CARD_NUMBER",  obj.getCardNumber());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "MER_COUNTRY_CODE",  obj.getMerCountryCode());
            p = this.insert(p, "LAST_OFFUS_MBC_TXN", String.valueOf(obj.getLastOffusMbcTxn()));
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_RMKS",  obj.getTranRmks());
            p = this.insert(p, "TERMINAL_ID",  obj.getTerminalId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "LAST_MBC_TXN", String.valueOf(obj.getLastMbcTxn()));
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "ERROR_CODE_DESCRIPTION",  obj.getErrorCodeDescription());
            p = this.insert(p, "PAYEE_IFSC_CODE",  obj.getPayeeIfscCode());
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            p = this.insert(p, "BC_ACCOUNT_NO",  obj.getBcAccountNo());
            p = this.insert(p, "CORPORATE_ID",  obj.getCorporateId());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "PAYEE_NICK_NAME",  obj.getPayeeNickName());
            p = this.insert(p, "BC_MOBILE_NO",  obj.getBcMobileNo());
            p = this.insert(p, "MBC_LOCATION",  obj.getMbcLocation());
            p = this.insert(p, "PAYER_NICK_NAME",  obj.getPayerNickName());
            p = this.insert(p, "PAYER_CODE",  obj.getPayerCode());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "AUTH_TYPE",  obj.getAuthType());
            p = this.insert(p, "PAYER_ID",  obj.getPayerId());
            p = this.insert(p, "NETWORK_TYPE",  obj.getNetworkType());
            p = this.insert(p, "IS_PAYER_BANK_CUST",  obj.getIsPayerBankCust());
            p = this.insert(p, "IS_PAYEE_BANK_CUST",  obj.getIsPayeeBankCust());
            p = this.insert(p, "BC_DISTRICT_CODE",  obj.getBcDistrictCode());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "MOBILE_NO",  obj.getMobileNo());
            p = this.insert(p, "MERCHANT_ID",  obj.getMerchantId());
            p = this.insert(p, "PAYEE_MOB_NO",  obj.getPayeeMobNo());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_FITXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_FITXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_FITXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_FiTxnEvent obj = new FT_FiTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setOriginIpAddr(rs.getString("ORIGIN_IP_ADDR"));
    obj.setPayeeCode(rs.getString("PAYEE_CODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setPayerAcctId(rs.getString("PAYER_ACCT_ID"));
    obj.setPayerName(rs.getString("PAYER_NAME"));
    obj.setMerState(rs.getString("MER_STATE"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setPayeeAcctId(rs.getString("PAYEE_ACCT_ID"));
    obj.setPayerIfscCode(rs.getString("PAYER_IFSC_CODE"));
    obj.setAgentCode(rs.getString("AGENT_CODE"));
    obj.setTranDesc(rs.getString("TRAN_DESC"));
    obj.setMerCityName(rs.getString("MER_CITY_NAME"));
    obj.setSegment(rs.getString("SEGMENT"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setHostUserId(rs.getString("HOST_USER_ID"));
    obj.setMerchantBcFlg(rs.getString("MERCHANT_BC_FLG"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setMerNameAddr(rs.getString("MER_NAME_ADDR"));
    obj.setCardNumber(rs.getString("CARD_NUMBER"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setMerCountryCode(rs.getString("MER_COUNTRY_CODE"));
    obj.setLastOffusMbcTxn(rs.getTimestamp("LAST_OFFUS_MBC_TXN"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranRmks(rs.getString("TRAN_RMKS"));
    obj.setTerminalId(rs.getString("TERMINAL_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setLastMbcTxn(rs.getTimestamp("LAST_MBC_TXN"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setErrorCodeDescription(rs.getString("ERROR_CODE_DESCRIPTION"));
    obj.setPayeeIfscCode(rs.getString("PAYEE_IFSC_CODE"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));
    obj.setBcAccountNo(rs.getString("BC_ACCOUNT_NO"));
    obj.setCorporateId(rs.getString("CORPORATE_ID"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setPayeeNickName(rs.getString("PAYEE_NICK_NAME"));
    obj.setBcMobileNo(rs.getString("BC_MOBILE_NO"));
    obj.setMbcLocation(rs.getString("MBC_LOCATION"));
    obj.setPayerNickName(rs.getString("PAYER_NICK_NAME"));
    obj.setPayerCode(rs.getString("PAYER_CODE"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setAuthType(rs.getString("AUTH_TYPE"));
    obj.setPayerId(rs.getString("PAYER_ID"));
    obj.setNetworkType(rs.getString("NETWORK_TYPE"));
    obj.setIsPayerBankCust(rs.getString("IS_PAYER_BANK_CUST"));
    obj.setIsPayeeBankCust(rs.getString("IS_PAYEE_BANK_CUST"));
    obj.setBcDistrictCode(rs.getString("BC_DISTRICT_CODE"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setMobileNo(rs.getString("MOBILE_NO"));
    obj.setMerchantId(rs.getString("MERCHANT_ID"));
    obj.setPayeeMobNo(rs.getString("PAYEE_MOB_NO"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_FITXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_FiTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_FiTxnEvent> events;
 FT_FiTxnEvent obj = new FT_FiTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FITXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_FiTxnEvent obj = new FT_FiTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setOriginIpAddr(getColumnValue(rs, "ORIGIN_IP_ADDR"));
            obj.setPayeeCode(getColumnValue(rs, "PAYEE_CODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setPayerAcctId(getColumnValue(rs, "PAYER_ACCT_ID"));
            obj.setPayerName(getColumnValue(rs, "PAYER_NAME"));
            obj.setMerState(getColumnValue(rs, "MER_STATE"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setPayeeAcctId(getColumnValue(rs, "PAYEE_ACCT_ID"));
            obj.setPayerIfscCode(getColumnValue(rs, "PAYER_IFSC_CODE"));
            obj.setAgentCode(getColumnValue(rs, "AGENT_CODE"));
            obj.setTranDesc(getColumnValue(rs, "TRAN_DESC"));
            obj.setMerCityName(getColumnValue(rs, "MER_CITY_NAME"));
            obj.setSegment(getColumnValue(rs, "SEGMENT"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setHostUserId(getColumnValue(rs, "HOST_USER_ID"));
            obj.setMerchantBcFlg(getColumnValue(rs, "MERCHANT_BC_FLG"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setMerNameAddr(getColumnValue(rs, "MER_NAME_ADDR"));
            obj.setCardNumber(getColumnValue(rs, "CARD_NUMBER"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setMerCountryCode(getColumnValue(rs, "MER_COUNTRY_CODE"));
            obj.setLastOffusMbcTxn(EventHelper.toTimestamp(getColumnValue(rs, "LAST_OFFUS_MBC_TXN")));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranRmks(getColumnValue(rs, "TRAN_RMKS"));
            obj.setTerminalId(getColumnValue(rs, "TERMINAL_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setLastMbcTxn(EventHelper.toTimestamp(getColumnValue(rs, "LAST_MBC_TXN")));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setErrorCodeDescription(getColumnValue(rs, "ERROR_CODE_DESCRIPTION"));
            obj.setPayeeIfscCode(getColumnValue(rs, "PAYEE_IFSC_CODE"));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));
            obj.setBcAccountNo(getColumnValue(rs, "BC_ACCOUNT_NO"));
            obj.setCorporateId(getColumnValue(rs, "CORPORATE_ID"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setPayeeNickName(getColumnValue(rs, "PAYEE_NICK_NAME"));
            obj.setBcMobileNo(getColumnValue(rs, "BC_MOBILE_NO"));
            obj.setMbcLocation(getColumnValue(rs, "MBC_LOCATION"));
            obj.setPayerNickName(getColumnValue(rs, "PAYER_NICK_NAME"));
            obj.setPayerCode(getColumnValue(rs, "PAYER_CODE"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setAuthType(getColumnValue(rs, "AUTH_TYPE"));
            obj.setPayerId(getColumnValue(rs, "PAYER_ID"));
            obj.setNetworkType(getColumnValue(rs, "NETWORK_TYPE"));
            obj.setIsPayerBankCust(getColumnValue(rs, "IS_PAYER_BANK_CUST"));
            obj.setIsPayeeBankCust(getColumnValue(rs, "IS_PAYEE_BANK_CUST"));
            obj.setBcDistrictCode(getColumnValue(rs, "BC_DISTRICT_CODE"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setMobileNo(getColumnValue(rs, "MOBILE_NO"));
            obj.setMerchantId(getColumnValue(rs, "MERCHANT_ID"));
            obj.setPayeeMobNo(getColumnValue(rs, "PAYEE_MOB_NO"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FITXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FITXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ORIGIN_IP_ADDR\",\"PAYEE_CODE\",\"CHANNEL\",\"PAYER_ACCT_ID\",\"PAYER_NAME\",\"MER_STATE\",\"DEVICE_ID\",\"PAYEE_ACCT_ID\",\"PAYER_IFSC_CODE\",\"AGENT_CODE\",\"TRAN_DESC\",\"MER_CITY_NAME\",\"SEGMENT\",\"TRAN_ID\",\"HOST_USER_ID\",\"MERCHANT_BC_FLG\",\"TRAN_AMT\",\"MER_NAME_ADDR\",\"CARD_NUMBER\",\"ACCOUNT_ID\",\"MER_COUNTRY_CODE\",\"LAST_OFFUS_MBC_TXN\",\"PAYEE_NAME\",\"SYS_TIME\",\"BRANCH_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TERMINAL_ID\",\"ERROR_CODE\",\"LAST_MBC_TXN\",\"ACCT_NAME\",\"ERROR_CODE_DESCRIPTION\",\"PAYEE_IFSC_CODE\",\"PAYEE_ID\",\"ACCT_STATUS\",\"BC_ACCOUNT_NO\",\"CORPORATE_ID\",\"COUNTRY_CODE\",\"ACCT_OPEN_DATE\",\"SUCC_FAIL_FLG\",\"PAYEE_NICK_NAME\",\"BC_MOBILE_NO\",\"MBC_LOCATION\",\"PAYER_NICK_NAME\",\"PAYER_CODE\",\"USER_ID\",\"CUST_ID\",\"AUTH_TYPE\",\"PAYER_ID\",\"NETWORK_TYPE\",\"IS_PAYER_BANK_CUST\",\"IS_PAYEE_BANK_CUST\",\"BC_DISTRICT_CODE\",\"TRAN_TYPE\",\"MOBILE_NO\",\"MERCHANT_ID\",\"PAYEE_MOB_NO\"" +
              " FROM EVENT_FT_FITXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ORIGIN_IP_ADDR],[PAYEE_CODE],[CHANNEL],[PAYER_ACCT_ID],[PAYER_NAME],[MER_STATE],[DEVICE_ID],[PAYEE_ACCT_ID],[PAYER_IFSC_CODE],[AGENT_CODE],[TRAN_DESC],[MER_CITY_NAME],[SEGMENT],[TRAN_ID],[HOST_USER_ID],[MERCHANT_BC_FLG],[TRAN_AMT],[MER_NAME_ADDR],[CARD_NUMBER],[ACCOUNT_ID],[MER_COUNTRY_CODE],[LAST_OFFUS_MBC_TXN],[PAYEE_NAME],[SYS_TIME],[BRANCH_ID],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TERMINAL_ID],[ERROR_CODE],[LAST_MBC_TXN],[ACCT_NAME],[ERROR_CODE_DESCRIPTION],[PAYEE_IFSC_CODE],[PAYEE_ID],[ACCT_STATUS],[BC_ACCOUNT_NO],[CORPORATE_ID],[COUNTRY_CODE],[ACCT_OPEN_DATE],[SUCC_FAIL_FLG],[PAYEE_NICK_NAME],[BC_MOBILE_NO],[MBC_LOCATION],[PAYER_NICK_NAME],[PAYER_CODE],[USER_ID],[CUST_ID],[AUTH_TYPE],[PAYER_ID],[NETWORK_TYPE],[IS_PAYER_BANK_CUST],[IS_PAYEE_BANK_CUST],[BC_DISTRICT_CODE],[TRAN_TYPE],[MOBILE_NO],[MERCHANT_ID],[PAYEE_MOB_NO]" +
              " FROM EVENT_FT_FITXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ORIGIN_IP_ADDR`,`PAYEE_CODE`,`CHANNEL`,`PAYER_ACCT_ID`,`PAYER_NAME`,`MER_STATE`,`DEVICE_ID`,`PAYEE_ACCT_ID`,`PAYER_IFSC_CODE`,`AGENT_CODE`,`TRAN_DESC`,`MER_CITY_NAME`,`SEGMENT`,`TRAN_ID`,`HOST_USER_ID`,`MERCHANT_BC_FLG`,`TRAN_AMT`,`MER_NAME_ADDR`,`CARD_NUMBER`,`ACCOUNT_ID`,`MER_COUNTRY_CODE`,`LAST_OFFUS_MBC_TXN`,`PAYEE_NAME`,`SYS_TIME`,`BRANCH_ID`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TERMINAL_ID`,`ERROR_CODE`,`LAST_MBC_TXN`,`ACCT_NAME`,`ERROR_CODE_DESCRIPTION`,`PAYEE_IFSC_CODE`,`PAYEE_ID`,`ACCT_STATUS`,`BC_ACCOUNT_NO`,`CORPORATE_ID`,`COUNTRY_CODE`,`ACCT_OPEN_DATE`,`SUCC_FAIL_FLG`,`PAYEE_NICK_NAME`,`BC_MOBILE_NO`,`MBC_LOCATION`,`PAYER_NICK_NAME`,`PAYER_CODE`,`USER_ID`,`CUST_ID`,`AUTH_TYPE`,`PAYER_ID`,`NETWORK_TYPE`,`IS_PAYER_BANK_CUST`,`IS_PAYEE_BANK_CUST`,`BC_DISTRICT_CODE`,`TRAN_TYPE`,`MOBILE_NO`,`MERCHANT_ID`,`PAYEE_MOB_NO`" +
              " FROM EVENT_FT_FITXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_FITXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ORIGIN_IP_ADDR\",\"PAYEE_CODE\",\"CHANNEL\",\"PAYER_ACCT_ID\",\"PAYER_NAME\",\"MER_STATE\",\"DEVICE_ID\",\"PAYEE_ACCT_ID\",\"PAYER_IFSC_CODE\",\"AGENT_CODE\",\"TRAN_DESC\",\"MER_CITY_NAME\",\"SEGMENT\",\"TRAN_ID\",\"HOST_USER_ID\",\"MERCHANT_BC_FLG\",\"TRAN_AMT\",\"MER_NAME_ADDR\",\"CARD_NUMBER\",\"ACCOUNT_ID\",\"MER_COUNTRY_CODE\",\"LAST_OFFUS_MBC_TXN\",\"PAYEE_NAME\",\"SYS_TIME\",\"BRANCH_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TERMINAL_ID\",\"ERROR_CODE\",\"LAST_MBC_TXN\",\"ACCT_NAME\",\"ERROR_CODE_DESCRIPTION\",\"PAYEE_IFSC_CODE\",\"PAYEE_ID\",\"ACCT_STATUS\",\"BC_ACCOUNT_NO\",\"CORPORATE_ID\",\"COUNTRY_CODE\",\"ACCT_OPEN_DATE\",\"SUCC_FAIL_FLG\",\"PAYEE_NICK_NAME\",\"BC_MOBILE_NO\",\"MBC_LOCATION\",\"PAYER_NICK_NAME\",\"PAYER_CODE\",\"USER_ID\",\"CUST_ID\",\"AUTH_TYPE\",\"PAYER_ID\",\"NETWORK_TYPE\",\"IS_PAYER_BANK_CUST\",\"IS_PAYEE_BANK_CUST\",\"BC_DISTRICT_CODE\",\"TRAN_TYPE\",\"MOBILE_NO\",\"MERCHANT_ID\",\"PAYEE_MOB_NO\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ORIGIN_IP_ADDR],[PAYEE_CODE],[CHANNEL],[PAYER_ACCT_ID],[PAYER_NAME],[MER_STATE],[DEVICE_ID],[PAYEE_ACCT_ID],[PAYER_IFSC_CODE],[AGENT_CODE],[TRAN_DESC],[MER_CITY_NAME],[SEGMENT],[TRAN_ID],[HOST_USER_ID],[MERCHANT_BC_FLG],[TRAN_AMT],[MER_NAME_ADDR],[CARD_NUMBER],[ACCOUNT_ID],[MER_COUNTRY_CODE],[LAST_OFFUS_MBC_TXN],[PAYEE_NAME],[SYS_TIME],[BRANCH_ID],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TERMINAL_ID],[ERROR_CODE],[LAST_MBC_TXN],[ACCT_NAME],[ERROR_CODE_DESCRIPTION],[PAYEE_IFSC_CODE],[PAYEE_ID],[ACCT_STATUS],[BC_ACCOUNT_NO],[CORPORATE_ID],[COUNTRY_CODE],[ACCT_OPEN_DATE],[SUCC_FAIL_FLG],[PAYEE_NICK_NAME],[BC_MOBILE_NO],[MBC_LOCATION],[PAYER_NICK_NAME],[PAYER_CODE],[USER_ID],[CUST_ID],[AUTH_TYPE],[PAYER_ID],[NETWORK_TYPE],[IS_PAYER_BANK_CUST],[IS_PAYEE_BANK_CUST],[BC_DISTRICT_CODE],[TRAN_TYPE],[MOBILE_NO],[MERCHANT_ID],[PAYEE_MOB_NO]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ORIGIN_IP_ADDR`,`PAYEE_CODE`,`CHANNEL`,`PAYER_ACCT_ID`,`PAYER_NAME`,`MER_STATE`,`DEVICE_ID`,`PAYEE_ACCT_ID`,`PAYER_IFSC_CODE`,`AGENT_CODE`,`TRAN_DESC`,`MER_CITY_NAME`,`SEGMENT`,`TRAN_ID`,`HOST_USER_ID`,`MERCHANT_BC_FLG`,`TRAN_AMT`,`MER_NAME_ADDR`,`CARD_NUMBER`,`ACCOUNT_ID`,`MER_COUNTRY_CODE`,`LAST_OFFUS_MBC_TXN`,`PAYEE_NAME`,`SYS_TIME`,`BRANCH_ID`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TERMINAL_ID`,`ERROR_CODE`,`LAST_MBC_TXN`,`ACCT_NAME`,`ERROR_CODE_DESCRIPTION`,`PAYEE_IFSC_CODE`,`PAYEE_ID`,`ACCT_STATUS`,`BC_ACCOUNT_NO`,`CORPORATE_ID`,`COUNTRY_CODE`,`ACCT_OPEN_DATE`,`SUCC_FAIL_FLG`,`PAYEE_NICK_NAME`,`BC_MOBILE_NO`,`MBC_LOCATION`,`PAYER_NICK_NAME`,`PAYER_CODE`,`USER_ID`,`CUST_ID`,`AUTH_TYPE`,`PAYER_ID`,`NETWORK_TYPE`,`IS_PAYER_BANK_CUST`,`IS_PAYEE_BANK_CUST`,`BC_DISTRICT_CODE`,`TRAN_TYPE`,`MOBILE_NO`,`MERCHANT_ID`,`PAYEE_MOB_NO`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

