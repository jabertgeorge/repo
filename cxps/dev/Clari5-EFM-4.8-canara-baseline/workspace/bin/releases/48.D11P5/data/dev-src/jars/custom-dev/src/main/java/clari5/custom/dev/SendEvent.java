package clari5.custom.dev;

import clari5.cxq.rest.MQ;
import clari5.platform.fileq.Clari5Payload;
import clari5.rda.RDA;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;

import clari5.platform.util.ECClient;

public class SendEvent {

    private static CxpsLogger logger = CxpsLogger.getLogger(SendEvent.class);


    public String sendDecideEvent(JSONObject jsonObject)
    {

        try {
            logger.info("Json value for Decide is "+jsonObject.toString());
            RDA rda = new RDA();
            String response =  rda.process(jsonObject.toString());
            //System.out.println("The respone is " +response);
            return  response;
        }
        catch (Exception e){
            System.out.println("Excpetion");

        }
        return null;
    }

    public String sendEnqueueEvent(JSONObject jsonObject)
    {

        logger.info("Json Value for Enqueue "+jsonObject.toString());

        String msgBody = jsonObject.getString("msgBody");
        JSONObject jsonObject1 = new JSONObject(msgBody);
        String entityId = jsonObject1.getString("cust_card_id");
        logger.info("entity id "+entityId);
        //ECClient.configure(null);
        String eventId = System.nanoTime() + "" + Thread.currentThread().getId();
        long eventTs = System.nanoTime();
        String message = jsonObject.toString();
        String batchName = "batchName";
        String qName = "HOST";
        String entityid = ((entityId != null) ? entityId : "NULLCARD"+System.nanoTime());

       // public Clari5Payload(String batchName, String entityid, String eventId, long eventTs, String message, String qname) {
        logger.info("BatchName : [ "+batchName+ " ]  , entityid : [ "+entityid+ " ]  , EventId : [ "+eventId+ " ] , eventTs : [ "+eventTs+ " ] , message : [ "+jsonObject.toString()+ " ] , qName : [ "+qName+ " ]" );
        Clari5Payload cp = new Clari5Payload(batchName, entityid, eventId, eventTs, message, qName);
        //boolean a = ECClient.enqueue("HOST", System.nanoTime() + "" + Thread.currentThread().getId(), jsonObject.toString());
        //boolean a = MQ.writer.write("HOST", ((entityId != null) ? entityId : "NULLCARD"+System.nanoTime()), System.nanoTime() + "" + Thread.currentThread().getId(), System.nanoTime(), jsonObject.toString());
        boolean a = MQ.writer.write(cp);
        if(a == true)
        {
           logger.info(" the enqueue status is "+a);
            return "SUCCESS" ;
        }
        return  null;
    }
}
