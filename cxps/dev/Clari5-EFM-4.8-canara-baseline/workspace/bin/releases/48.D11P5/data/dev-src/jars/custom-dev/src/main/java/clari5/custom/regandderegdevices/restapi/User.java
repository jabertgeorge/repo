package clari5.custom.regandderegdevices.restapi;
/*created by raghvendra patel
 * on 12/112/19
 */
public class User {
    private  String userid;
    private  String deviceid;
    private  String ipaddress;
    private  String time;
    private  String status;

    public User() {
    }

    public User(String userid, String deviceid, String ipaddress, String time, String status) {
        this.userid = userid;
        this.deviceid = deviceid;
        this.ipaddress = ipaddress;
        this.time = time;
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
