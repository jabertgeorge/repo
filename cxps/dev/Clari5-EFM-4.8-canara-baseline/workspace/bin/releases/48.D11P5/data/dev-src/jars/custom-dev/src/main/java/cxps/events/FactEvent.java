package cxps.events;

import clari5.platform.rdbms.RDBMSSession;
import cxps.apex.exceptions.InvalidDataException;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;

import java.util.Map;
import java.util.Set;

/* "{\"event-name\": \"cxps.events.FactEvent\",\"event-id\": \"" + eventId + "\",\"msgBody\": \"{'host-id':'F','name':'" + factName + "','score':'" + score + "','action':'" + action + "','sys-time':'" + sysTime + "','event-id':'" + eventId + "'}\"}"; */
public class FactEvent extends Event {
    private String name;
    private Double score;
    private String action;

    public FactEvent() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void fromMap(Map<String, ? extends Object> msgMap) throws InvalidDataException {
        super.fromMap(msgMap);
        setHostId((String) msgMap.get("host-id"));
        setName((String) msgMap.get("name"));
        setScore((Double) msgMap.get("score"));
        setAction((String) msgMap.get("action"));
        setEventName("FactEvent");
    }

    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        return null;
    }

}
    
