package clari5.custom.canara.data;

public class FT_AccountTxn extends ITableData {

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "FT_ACCOUNTTXN";
    private String event_type = "FT_AccountTxn";
    private String HOST_ID;
    private String SYS_TIME;
    private String CHANNEL;
    private String ACCOUNT_ID;
    private String SCHM_TYPE;
    private String SCHM_CODE;
    private String CUST_ID;
    private String ACCT_NAME;
    private String ACCT_SOL_ID;
    private String ACCT_OWNERSHIP;
    private String ACCT_OPEN_DATE;
    private String AVL_BAL;
    private String CLR_BAL_AMT;
    private String TRAN_TYPE;
    private String TRAN_SUB_TYPE;
    private String PART_TRAN_TYPE;
    private String TRAN_DATE;
    private String TRAN_ID;
    private String TXN_AMT;
    private String PART_TRAN_SRL_NUM;
    private String TRAN_CRNCY_CODE;
    private String REF_TXN_AMT;
    private String REF_TXN_CRNCY;
    private String RATE;
    private String TRAN_PARTICULAR;
    private String IP_ADDRESS;
    private String PSTD_FLG;
    private String PSTD_USER_ID;
    private String ENTRY_USER;
    private String CUST_CARD_ID;
    private String DEVICE_ID;
    private String CONTRA_BANK_CODE;
    private String CONTRA_SOL_ID;
    private String TRAN_CATEGORY;
    private String LAST_TRAN_DATE;
    private String BRANCH_ID;
    private String VALUE_DATE;
    private String INSTRMNT_DATE;
    private String INSTRMNT_NUM;
    private String PREV_BAL;
    private String ACCT_STAT;
    private String TXN_BR_ID;
    private String ACCT_SOL_CITY;
    private String TXN_BR_CITY;
    private String INSTRMNT_TYPE;
    private String MNEMONIC_CODE;
    private String RECONID;
    private String CONTRA_CUSTID;
    private String CONTRA_GL_FLAG;
    private String GL_FLAG;
    private String IFSC_CODE;
    private String PENAL_INTEREST_FLAG;
    private String STAFF_FLAG;
    private String ACCT_CLOSED_DATE;
    private String ACCT_MATURITY_DATE;
    private String RE_ACTIVATION_DATE;
    private String TRAN_CODE;
    private String DP_CODE;
    private String TD_LIQUIDATION_FLG;
    private String ID;

    public String getCONTRA_ACCOUNT_ID() {
        return CONTRA_ACCOUNT_ID;
    }

    public void setCONTRA_ACCOUNT_ID(String CONTRA_ACCOUNT_ID) {
        this.CONTRA_ACCOUNT_ID = CONTRA_ACCOUNT_ID;
    }

    private String CONTRA_ACCOUNT_ID;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public String getIFSC_CODE() {
        return IFSC_CODE;
    }

    public void setIFSC_CODE(String IFSC_CODE) {
        this.IFSC_CODE = IFSC_CODE;
    }

    public String getPENAL_INTEREST_FLAG() {
        return PENAL_INTEREST_FLAG;
    }

    public void setPENAL_INTEREST_FLAG(String PENAL_INTEREST_FLAG) {
        this.PENAL_INTEREST_FLAG = PENAL_INTEREST_FLAG;
    }

    public String getSTAFF_FLAG() {
        return STAFF_FLAG;
    }

    public void setSTAFF_FLAG(String STAFF_FLAG) {
        this.STAFF_FLAG = STAFF_FLAG;
    }

    public String getACCT_CLOSED_DATE() {
        return ACCT_CLOSED_DATE;
    }

    public void setACCT_CLOSED_DATE(String ACCT_CLOSED_DATE) {
        this.ACCT_CLOSED_DATE = ACCT_CLOSED_DATE;
    }

    public String getACCT_MATURITY_DATE() {
        return ACCT_MATURITY_DATE;
    }

    public void setACCT_MATURITY_DATE(String ACCT_MATURITY_DATE) {
        this.ACCT_MATURITY_DATE = ACCT_MATURITY_DATE;
    }

    public String getRE_ACTIVATION_DATE() {
        return RE_ACTIVATION_DATE;
    }

    public void setRE_ACTIVATION_DATE(String RE_ACTIVATION_DATE) {
        this.RE_ACTIVATION_DATE = RE_ACTIVATION_DATE;
    }

    public String getTRAN_CODE() {
        return TRAN_CODE;
    }

    public void setTRAN_CODE(String TRAN_CODE) {
        this.TRAN_CODE = TRAN_CODE;
    }

    public String getDP_CODE() {
        return DP_CODE;
    }

    public void setDP_CODE(String DP_CODE) {
        this.DP_CODE = DP_CODE;
    }

    public String getTD_LIQUIDATION_FLG() {
        return TD_LIQUIDATION_FLG;
    }

    public void setTD_LIQUIDATION_FLG(String TD_LIQUIDATION_FLG) {
        this.TD_LIQUIDATION_FLG = TD_LIQUIDATION_FLG;
    }



    public String getTXN_BR_CITY() {
        return TXN_BR_CITY;
    }

    public void setTXN_BR_CITY(String TXN_BR_CITY) {
        this.TXN_BR_CITY = TXN_BR_CITY;
    }

    public String getINSTRMNT_TYPE() {
        return INSTRMNT_TYPE;
    }

    public void setINSTRMNT_TYPE(String INSTRMNT_TYPE) {
        this.INSTRMNT_TYPE = INSTRMNT_TYPE;
    }

    public String getMNEMONIC_CODE() {
        return MNEMONIC_CODE;
    }

    public void setMNEMONIC_CODE(String MNEMONIC_CODE) {
        this.MNEMONIC_CODE = MNEMONIC_CODE;
    }

    public String getRECONID() {
        return RECONID;
    }

    public void setRECONID(String RECONID) {
        this.RECONID = RECONID;
    }

    public String getCONTRA_CUSTID() {
        return CONTRA_CUSTID;
    }

    public void setCONTRA_CUSTID(String CONTRA_CUSTID) {
        this.CONTRA_CUSTID = CONTRA_CUSTID;
    }

    public String getCONTRA_GL_FLAG() {
        return CONTRA_GL_FLAG;
    }

    public void setCONTRA_GL_FLAG(String CONTRA_GL_FLAG) {
        this.CONTRA_GL_FLAG = CONTRA_GL_FLAG;
    }

    public String getGL_FLAG() {
        return GL_FLAG;
    }

    public void setGL_FLAG(String GL_FLAG) {
        this.GL_FLAG = GL_FLAG;
    }

    public String getINSTRMNT_DATE() {
        return INSTRMNT_DATE;
    }

    public void setINSTRMNT_DATE(String INSTRMNT_DATE) {
        this.INSTRMNT_DATE = INSTRMNT_DATE;
    }

    public String getINSTRMNT_NUM() {
        return INSTRMNT_NUM;
    }

    public void setINSTRMNT_NUM(String INSTRMNT_NUM) {
        this.INSTRMNT_NUM = INSTRMNT_NUM;
    }

    public String getPREV_BAL() {
        return PREV_BAL;
    }

    public void setPREV_BAL(String PREV_BAL) {
        this.PREV_BAL = PREV_BAL;
    }

    public String getACCT_STAT() {
        return ACCT_STAT;
    }

    public void setACCT_STAT(String ACCT_STAT) {
        this.ACCT_STAT = ACCT_STAT;
    }

    public String getTXN_BR_ID() {
        return TXN_BR_ID;
    }

    public void setTXN_BR_ID(String TXN_BR_ID) {
        this.TXN_BR_ID = TXN_BR_ID;
    }

    public String getACCT_SOL_CITY() {
        return ACCT_SOL_CITY;
    }

    public void setACCT_SOL_CITY(String ACCT_SOL_CITY) {
        this.ACCT_SOL_CITY = ACCT_SOL_CITY;
    }

    public String getCONTRA_BANK_CODE() {
        return CONTRA_BANK_CODE;
    }

    public void setCONTRA_BANK_CODE(String CONTRA_BANK_CODE) {
        this.CONTRA_BANK_CODE = CONTRA_BANK_CODE;
    }

    public String getCONTRA_SOL_ID() {
        return CONTRA_SOL_ID;
    }

    public void setCONTRA_SOL_ID(String CONTRA_SOL_ID) {
        this.CONTRA_SOL_ID = CONTRA_SOL_ID;
    }

    public String getTRAN_CATEGORY() {
        return TRAN_CATEGORY;
    }

    public void setTRAN_CATEGORY(String TRAN_CATEGORY) {
        this.TRAN_CATEGORY = TRAN_CATEGORY;
    }

    public String getLAST_TRAN_DATE() {
        return LAST_TRAN_DATE;
    }

    public void setLAST_TRAN_DATE(String LAST_TRAN_DATE) {
        this.LAST_TRAN_DATE = LAST_TRAN_DATE;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getVALUE_DATE() {
        return VALUE_DATE;
    }

    public void setVALUE_DATE(String VALUE_DATE) {
        this.VALUE_DATE = VALUE_DATE;
    }

    public String getENTRY_USER() {
        return ENTRY_USER;
    }

    public void setENTRY_USER(String ENTRY_USER) {
        this.ENTRY_USER = ENTRY_USER;
    }

    public String getCUST_CARD_ID() {
        return CUST_CARD_ID;
    }

    public void setCUST_CARD_ID(String CUST_CARD_ID) {
        this.CUST_CARD_ID = CUST_CARD_ID;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
    }

    public String getPSTD_FLG() {
        return PSTD_FLG;
    }

    public void setPSTD_FLG(String PSTD_FLG) {
        this.PSTD_FLG = PSTD_FLG;
    }

    public String getPSTD_USER_ID() {
        return PSTD_USER_ID;
    }

    public void setPSTD_USER_ID(String PSTD_USER_ID) {
        this.PSTD_USER_ID = PSTD_USER_ID;
    }

    public String getTXN_AMT() {
        return TXN_AMT;
    }

    public void setTXN_AMT(String TXN_AMT) {
        this.TXN_AMT = TXN_AMT;
    }

    public String getTRAN_CRNCY_CODE() {
        return TRAN_CRNCY_CODE;
    }

    public void setTRAN_CRNCY_CODE(String TRAN_CRNCY_CODE) {
        this.TRAN_CRNCY_CODE = TRAN_CRNCY_CODE;
    }

    public String getREF_TXN_AMT() {
        return REF_TXN_AMT;
    }

    public void setREF_TXN_AMT(String REF_TXN_AMT) {
        this.REF_TXN_AMT = REF_TXN_AMT;
    }

    public String getREF_TXN_CRNCY() {
        return REF_TXN_CRNCY;
    }

    public void setREF_TXN_CRNCY(String REF_TXN_CRNCY) {
        this.REF_TXN_CRNCY = REF_TXN_CRNCY;
    }

    public String getRATE() {
        return RATE;
    }

    public void setRATE(String RATE) {
        this.RATE = RATE;
    }

    public String getTRAN_PARTICULAR() {
        return TRAN_PARTICULAR;
    }

    public void setTRAN_PARTICULAR(String TRAN_PARTICULAR) {
        this.TRAN_PARTICULAR = TRAN_PARTICULAR;
    }
    public String getCLR_BAL_AMT() {
        return CLR_BAL_AMT;
    }

    public void setCLR_BAL_AMT(String CLR_BAL_AMT) {
        this.CLR_BAL_AMT = CLR_BAL_AMT;
    }

    public String getTRAN_TYPE() {
        return TRAN_TYPE;
    }

    public void setTRAN_TYPE(String TRAN_TYPE) {
        this.TRAN_TYPE = TRAN_TYPE;
    }

    public String getTRAN_SUB_TYPE() {
        return TRAN_SUB_TYPE;
    }

    public void setTRAN_SUB_TYPE(String TRAN_SUB_TYPE) {
        this.TRAN_SUB_TYPE = TRAN_SUB_TYPE;
    }

    public String getPART_TRAN_TYPE() {
        return PART_TRAN_TYPE;
    }

    public void setPART_TRAN_TYPE(String PART_TRAN_TYPE) {
        this.PART_TRAN_TYPE = PART_TRAN_TYPE;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getTRAN_ID() {
        return TRAN_ID;
    }

    public void setTRAN_ID(String TRAN_ID) {
        this.TRAN_ID = TRAN_ID;
    }

    public String getPART_TRAN_SRL_NUM() {
        return PART_TRAN_SRL_NUM;
    }

    public void setPART_TRAN_SRL_NUM(String PART_TRAN_SRL_NUM) {
        this.PART_TRAN_SRL_NUM = PART_TRAN_SRL_NUM;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getACCT_NAME() {
        return ACCT_NAME;
    }

    public void setACCT_NAME(String ACCT_NAME) {
        this.ACCT_NAME = ACCT_NAME;
    }

    public String getACCT_SOL_ID() {
        return ACCT_SOL_ID;
    }

    public void setACCT_SOL_ID(String ACCT_SOL_ID) {
        this.ACCT_SOL_ID = ACCT_SOL_ID;
    }

    public String getACCT_OWNERSHIP() {
        return ACCT_OWNERSHIP;
    }

    public void setACCT_OWNERSHIP(String ACCT_OWNERSHIP) {
        this.ACCT_OWNERSHIP = ACCT_OWNERSHIP;
    }

    public String getACCT_OPEN_DATE() {
        return ACCT_OPEN_DATE;
    }

    public void setACCT_OPEN_DATE(String ACCT_OPEN_DATE) {
        this.ACCT_OPEN_DATE = ACCT_OPEN_DATE;
    }

    public String getAVL_BAL() {
        return AVL_BAL;
    }

    public void setAVL_BAL(String AVL_BAL) {
        this.AVL_BAL = AVL_BAL;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getSCHM_TYPE() {
        return SCHM_TYPE;
    }

    public void setSCHM_TYPE(String SCHM_TYPE) {
        this.SCHM_TYPE = SCHM_TYPE;
    }

    public String getSCHM_CODE() {
        return SCHM_CODE;
    }

    public void setSCHM_CODE(String SCHM_CODE) {
        this.SCHM_CODE = SCHM_CODE;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }



}

