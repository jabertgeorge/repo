clari5.hfdb.entity.nft_branchadvances{

  attributes = [
        { name = CL5_FLG,  type= "string:10"}
        { name = CREATED_ON,  type= timestamp}
        { name = UPDATED_ON,  type= timestamp}
        { name = ID, type="string:2000", key = true}
        { name = sys_time, type="timestamp"}
        { name = branch_code, type ="string:20"}
        { name = branch_name, type ="string:100"}
        { name = previous_month_advance_amt, type ="number:18,2"}
        { name = current_month_advance_amt, type ="number:18,2"}
        { name = previous_quarter_advance_amt, type ="number:18,2"}
        { name = current_quarter_advance_amt, type ="number:18,2"}
        { name = previous_day_advance_amt, type ="number:18,2"}
        { name = current_day_advance_amt, type ="number:18,2"}
        { name = event_date_time, type  = timestamp}

        ]
}
