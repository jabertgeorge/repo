cxps.events.event.ft_c_c_card_txn{
table-name : EVENT_FT_CC_CARDSTXN
event-mnemonic : FCC 
workspaces : {
ACCOUNT : dr_account_id,
PAYMENTCARD : cust-card-id
MERCHANT : merchant-id
TERMINAL : terminal-id 
}

event-attributes : {
host-id:{db : true ,raw_name : host_id, type :  "string:5"}
sys-time:{db : true ,raw_name : sys_time, type :  timestamp}
channel:{db : true ,raw_name : channel, type :  "string:20"}
user-id:{db : true ,raw_name : user_id, type :  "string:20"}
user-type:{db : true ,raw_name : user_type, type :  "string:50"}
cust-id:{db : true ,raw_name : cust_id, type :  "string:50",fr : true}
device-id:{db : true ,raw_name : device_id, type :  "string:50"}
ip-address:{db : true ,raw_name : ip_address, type :  "string:20"}
ip-country:{db : true ,raw_name : ip_country, type :  "string:20"}
ip-city:{db : true ,raw_name : ip_city, type :  "string:20"}
cust-name:{db : true ,raw_name : cust_name, type :  "string:50"}
succ-fail-flag:{db : true ,raw_name : succ_fail_flg, type :  "string:2"}
error-code:{db : true ,raw_name : error_code, type :  "string:6"}
error-desc:{db : true ,raw_name : error_desc, type :  "string:100"}
dr-account-id:{db : true ,raw_name : dr_account_id, type :  "string:20"}
txn-amount:{db : true ,raw_name : txn_amt, type :  "number:20,3"}
avl-bal:{db : true ,raw_name : avl_bal, type :  "number:20,3"}
cr-account-id:{db : true ,raw_name : cr_account_id, type :  "string:20"}
cr-ifsc-code:{db : true ,raw_name : cr_ifsc_code, type :  "string:20"}
payee-id:{db : true ,raw_name : payee_id, type :  "string:20"}
payee-name:{db : true ,raw_name : payee_name, type :  "string:50"}
tran-type:{db : true ,raw_name : tran_type, type :  "string:10"}
entry-mode:{db : true ,raw_name : entry_mode, type :  "string:50"}
branch-id:{db : true ,raw_name : branch_id, type :  "string:10"}
acct-ownership:{db : true ,raw_name : acct_ownership, type :  "string:50"}
acct-open-date:{db : true ,raw_name : acct_open_date, type :  "string:"}
part-tran-type:{db : true ,raw_name : part_tran_type, type :  "string:50"}
country-code:{db : true ,raw_name : country_code, type :  "string:2"}
cust-card-id:{db : true ,raw_name : cust_card_id, type :  "string:16",fr : true}
bin:{db : true ,raw_name : bin, type :  "string:6"}
mcc-code:{db : true ,raw_name : mcc_code, type :  "string:20"}
terminal-id:{db : true ,raw_name : terminal_id, type :  "string:50"}
merchant-id:{db : true ,raw_name : merchant_id, type :  "string:50"}
cust-mob-no:{db : true ,raw_name : cust_mob_no, type :  "string:10"}
txn-secured-flag:{db : true ,raw_name : txn_secured_flag, type :  "string:2"}
product-code:{db : true ,raw_name : product_code, type :  "string:50"}
devowner-id:{db : true ,raw_name : dev_owner_id, type :  "string:50"}
atm-dom-lim:{db : true ,raw_name : atm_dom_limit, type :  "number:20,3"}
atm-intr-lim:{db : true ,raw_name : atm_intr_limit, type :  "number:20,3"}
posecomdom-lim:{db : true ,raw_name : pos_ecom_dom_limit, type :  "number:20,3"}
posecom-intr-lim:{db : true ,raw_name : pos_ecom_intr_limit, type :  "number:20,3"}
tran-date:{db : true ,raw_name : tran_date, type :  "string:"}
mask-card-no:{db : true ,raw_name : mask_card_no, type :  "string:20"}
state-code:{db : true ,raw_name : state_code, type :  "string:20"}
tran-particular:{db : true ,raw_name : tran_particular, type :  "string:200"}
pos-entry-mode:{db : true ,raw_name : pos_entry_mode, type :  "string:5"}
chip-pin-flag:{db : true ,raw_name : chip_pin_flg, type :  "string:10"}
tran-cde:{db : true ,raw_name : tran_cde, type :  "string:200"}
resp-cde:{db : true ,raw_name : resp_cde, type :  "string:200"}
}}
