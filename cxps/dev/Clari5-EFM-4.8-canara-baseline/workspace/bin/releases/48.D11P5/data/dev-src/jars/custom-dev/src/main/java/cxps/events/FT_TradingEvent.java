// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_TRADING", Schema="rice")
public class FT_TradingEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public Double buyAmount1;
       @Field public java.sql.Timestamp valueDate;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public Double exchangeRate;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public Double maturityAmt;
       @Field(size=20) public String dealNo;
       @Field(size=20) public String amtCurrency2;
       @Field(size=20) public String counterparty;
       @Field(size=20) public Double sellAmount2;
       @Field(size=20) public String tranReference;
       @Field(size=20) public String hostId;
       @Field(size=20) public String productCode;
       @Field(size=20) public String statusFlag;
       @Field(size=20) public String amtCurrency1;


    @JsonIgnore
    public ITable<FT_TradingEvent> t = AEF.getITable(this);

	public FT_TradingEvent(){}

    public FT_TradingEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("TradingEvent");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBuyAmount1(EventHelper.toDouble(json.getString("buy_amount1")));
            setValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setExchangeRate(EventHelper.toDouble(json.getString("exchange_rate")));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setMaturityAmt(EventHelper.toDouble(json.getString("maturity_amt")));
            setDealNo(json.getString("dealno"));
            setAmtCurrency2(json.getString("amt_currency2"));
            setCounterparty(json.getString("counterparty"));
            setSellAmount2(EventHelper.toDouble(json.getString("sell_amount2")));
            setTranReference(json.getString("tran_reference"));
            setHostId(json.getString("host_id"));
            setProductCode(json.getString("product_code"));
            setStatusFlag(json.getString("status_flag"));
            setAmtCurrency1(json.getString("amt_currrency1"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FTE"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public Double getBuyAmount1(){ return buyAmount1; }

    public java.sql.Timestamp getValueDate(){ return valueDate; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public Double getExchangeRate(){ return exchangeRate; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public Double getMaturityAmt(){ return maturityAmt; }

    public String getDealNo(){ return dealNo; }

    public String getAmtCurrency2(){ return amtCurrency2; }

    public String getCounterparty(){ return counterparty; }

    public Double getSellAmount2(){ return sellAmount2; }

    public String getTranReference(){ return tranReference; }

    public String getHostId(){ return hostId; }

    public String getProductCode(){ return productCode; }

    public String getStatusFlag(){ return statusFlag; }

    public String getAmtCurrency1(){ return amtCurrency1; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBuyAmount1(Double val){ this.buyAmount1 = val; }
    public void setValueDate(java.sql.Timestamp val){ this.valueDate = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setExchangeRate(Double val){ this.exchangeRate = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setMaturityAmt(Double val){ this.maturityAmt = val; }
    public void setDealNo(String val){ this.dealNo = val; }
    public void setAmtCurrency2(String val){ this.amtCurrency2 = val; }
    public void setCounterparty(String val){ this.counterparty = val; }
    public void setSellAmount2(Double val){ this.sellAmount2 = val; }
    public void setTranReference(String val){ this.tranReference = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setProductCode(String val){ this.productCode = val; }
    public void setStatusFlag(String val){ this.statusFlag = val; }
    public void setAmtCurrency1(String val){ this.amtCurrency1 = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_TradingEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.counterparty);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.dealNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_TradingEvent");
        json.put("event_type", "FT");
        json.put("event_sub_type", "TradingEvent");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
