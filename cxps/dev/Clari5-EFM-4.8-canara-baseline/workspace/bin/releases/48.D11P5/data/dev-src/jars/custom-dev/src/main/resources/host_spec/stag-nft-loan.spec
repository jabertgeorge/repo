clari5.hfdb.entity.nft_loan{
  attributes = [
                        {name = CL5_FLG,  type= "string:20"}
                        {name = CREATED_ON,  type= timestamp}
                        {name = UPDATED_ON,  type= timestamp}
                        {name = sys_time, type = timestamp}
                        {name = ID, type="string:2000", key = true}
						{name = host_id, type="string:2"}
						{name = cust_id, type= "string:20"}
						{name = account_id, type= "string:20"}
						{name = scheme_type, type = "string:20"}
						{name = scheme_code, type = "string:20"}
						{name = system_type, type = "string:20"}
						{name = gl_code, type = "string:20"}
						{name = loan_amt, type = "number:20,3"}
						{name = opened_date_time, type = timestamp}

]
}
