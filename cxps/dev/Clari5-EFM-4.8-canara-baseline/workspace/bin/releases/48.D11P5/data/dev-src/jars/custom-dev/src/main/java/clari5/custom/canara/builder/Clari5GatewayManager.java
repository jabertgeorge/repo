package clari5.custom.canara.builder;

import clari5.custom.canara.audit.AuditManager;
import clari5.custom.canara.config.BepCon;
import clari5.cxq.rest.MQ;
import clari5.platform.fileq.Clari5FileWriter;
import clari5.platform.fileq.Clari5Payload;
import clari5.platform.util.CxRest;
import clari5.platform.util.Hocon;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;
import clari5.platform.util.ECClient;

public class Clari5GatewayManager {
    public static CxpsLogger logger = CxpsLogger.getLogger(Clari5GatewayManager.class);

    static {
        try {
              ECClient.configure(null);
              Hocon hocon = new Hocon();
              hocon.loadFromContext("mq-clari5.conf");
             Clari5FileWriter.configure(hocon.get("writer"));
        }catch (Exception e){
            e.printStackTrace();
            logger.info(" Exception in Clari5GatewayManager");
        }


    }

    public static boolean send(String event_id,JSONObject json, long event_ts) throws Exception {
        String entity_id = "canarabankEntity"+System.nanoTime();

        String eventId = System.nanoTime() + "" + Thread.currentThread().getId();
        long eventTs = System.nanoTime();
        String message = json.toString();
        String batchName = "batchName";
        String qName = "HOST";
        //boolean status= ECClient.enqueue("HOST",entity_id,event_id,json.toString());
        //boolean status = MQ.writer.write("HOST", entity_id+System.nanoTime(), System.nanoTime() + "" + Thread.currentThread().getId(), System.nanoTime(), json.toString());
        Clari5Payload cp = new Clari5Payload(batchName, entity_id, eventId, eventTs, message, qName);
        boolean status = MQ.writer.write(cp);
        logger.info("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        return status;
    }
}
