// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_FITXN", Schema="rice")
public class FT_FiTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String originIpAddr;
       @Field(size=20) public String payeeCode;
       @Field(size=20) public String channel;
       @Field(size=20) public String payerAcctId;
       @Field(size=20) public String payerName;
       @Field(size=20) public String merState;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String payeeAcctId;
       @Field(size=20) public String payerIfscCode;
       @Field(size=20) public String agentCode;
       @Field(size=20) public String tranDesc;
       @Field(size=20) public String merCityName;
       @Field(size=20) public String segment;
       @Field(size=20) public String tranId;
       @Field(size=20) public String hostUserId;
       @Field(size=20) public String merchantBcFlg;
       @Field(size=11) public Double tranAmt;
       @Field(size=20) public String merNameAddr;
       @Field(size=20) public String cardNumber;
       @Field(size=20) public String accountId;
       @Field(size=20) public String merCountryCode;
       @Field public java.sql.Timestamp lastOffusMbcTxn;
       @Field(size=20) public String payeeName;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String branchId;
       @Field(size=20) public String tranCrncyCode;
       @Field(size=20) public String tranRmks;
       @Field(size=20) public String terminalId;
       @Field(size=20) public String errorCode;
       @Field public java.sql.Timestamp lastMbcTxn;
       @Field(size=20) public String acctName;
       @Field(size=20) public String errorCodeDescription;
       @Field(size=20) public String payeeIfscCode;
       @Field(size=20) public String payeeId;
       @Field(size=20) public String acctStatus;
       @Field(size=20) public String bcAccountNo;
       @Field(size=20) public String corporateId;
       @Field(size=20) public String countryCode;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=20) public String succFailFlg;
       @Field(size=20) public String payeeNickName;
       @Field(size=20) public String bcMobileNo;
       @Field(size=20) public String mbcLocation;
       @Field(size=20) public String payerNickName;
       @Field(size=20) public String payerCode;
       @Field(size=20) public String userId;
       @Field(size=20) public String custId;
       @Field(size=20) public String authType;
       @Field(size=20) public String payerId;
       @Field(size=20) public String networkType;
       @Field(size=20) public String isPayerBankCust;
       @Field(size=20) public String isPayeeBankCust;
       @Field(size=20) public String bcDistrictCode;
       @Field(size=20) public String tranType;
       @Field(size=20) public String mobileNo;
       @Field(size=20) public String merchantId;
       @Field(size=20) public String payeeMobNo;


    @JsonIgnore
    public ITable<FT_FiTxnEvent> t = AEF.getITable(this);

	public FT_FiTxnEvent(){}

    public FT_FiTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("FiTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setOriginIpAddr(json.getString("ip_address"));
            setPayeeCode(json.getString("payee_code"));
            setChannel(json.getString("channel"));
            setPayerAcctId(json.getString("payer_acct_id"));
            setPayerName(json.getString("payer_name"));
            setMerState(json.getString("mer_state"));
            setDeviceId(json.getString("device_id"));
            setPayeeAcctId(json.getString("payee_acct_id"));
            setPayerIfscCode(json.getString("payer_ifsc_code"));
            setAgentCode(json.getString("agent_code"));
            setTranDesc(json.getString("Tran_desc"));
            setMerCityName(json.getString("mer_city_name"));
            setSegment(json.getString("segment"));
            setTranId(json.getString("tran_id"));
            setHostUserId(json.getString("host_user_id"));
            setMerchantBcFlg(json.getString("merchant_bc_flg"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setMerNameAddr(json.getString("mer_name_addr"));
            setCardNumber(json.getString("card_number"));
            setAccountId(json.getString("account_id"));
            setMerCountryCode(json.getString("mer_country_code"));
            setLastOffusMbcTxn(EventHelper.toTimestamp(json.getString("last_offus_mbc_txn")));
            setPayeeName(json.getString("payee_name"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTranRmks(json.getString("tran_rmks"));
            setTerminalId(json.getString("terminal_id"));
            setErrorCode(json.getString("error_code"));
            setLastMbcTxn(EventHelper.toTimestamp(json.getString("last_mbc_txn")));
            setAcctName(json.getString("acct_name"));
            setErrorCodeDescription(json.getString("error_code_description"));
            setPayeeIfscCode(json.getString("payee_ifsc_code"));
            setPayeeId(json.getString("payee_id"));
            setAcctStatus(json.getString("acct_status"));
            setBcAccountNo(json.getString("bc_account_no"));
            setCorporateId(json.getString("corporate_id"));
            setCountryCode(json.getString("country_code"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setPayeeNickName(json.getString("payee_nick_name"));
            setBcMobileNo(json.getString("bc_mobile_no"));
            setMbcLocation(json.getString("mbc_location"));
            setPayerNickName(json.getString("payer_nick_name"));
            setPayerCode(json.getString("payer_code"));
            setUserId(json.getString("user_id"));
            setCustId(json.getString("cust_id"));
            setAuthType(json.getString("auth_type"));
            setPayerId(json.getString("payer_id"));
            setNetworkType(json.getString("network_type"));
            setIsPayerBankCust(json.getString("is_payer_bank_cust"));
            setIsPayeeBankCust(json.getString("is_payee_bank_cust"));
            setBcDistrictCode(json.getString("bc_district_code"));
            setTranType(json.getString("tran_type"));
            setMobileNo(json.getString("mobile_no"));
            setMerchantId(json.getString("merchant_id"));
            setPayeeMobNo(json.getString("payee_mob_no"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FIT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getOriginIpAddr(){ return originIpAddr; }

    public String getPayeeCode(){ return payeeCode; }

    public String getChannel(){ return channel; }

    public String getPayerAcctId(){ return payerAcctId; }

    public String getPayerName(){ return payerName; }

    public String getMerState(){ return merState; }

    public String getDeviceId(){ return deviceId; }

    public String getPayeeAcctId(){ return payeeAcctId; }

    public String getPayerIfscCode(){ return payerIfscCode; }

    public String getAgentCode(){ return agentCode; }

    public String getTranDesc(){ return tranDesc; }

    public String getMerCityName(){ return merCityName; }

    public String getSegment(){ return segment; }

    public String getTranId(){ return tranId; }

    public String getHostUserId(){ return hostUserId; }

    public String getMerchantBcFlg(){ return merchantBcFlg; }

    public Double getTranAmt(){ return tranAmt; }

    public String getMerNameAddr(){ return merNameAddr; }

    public String getCardNumber(){ return cardNumber; }

    public String getAccountId(){ return accountId; }

    public String getMerCountryCode(){ return merCountryCode; }

    public java.sql.Timestamp getLastOffusMbcTxn(){ return lastOffusMbcTxn; }

    public String getPayeeName(){ return payeeName; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getTranRmks(){ return tranRmks; }

    public String getTerminalId(){ return terminalId; }

    public String getErrorCode(){ return errorCode; }

    public java.sql.Timestamp getLastMbcTxn(){ return lastMbcTxn; }

    public String getAcctName(){ return acctName; }

    public String getErrorCodeDescription(){ return errorCodeDescription; }

    public String getPayeeIfscCode(){ return payeeIfscCode; }

    public String getPayeeId(){ return payeeId; }

    public String getAcctStatus(){ return acctStatus; }

    public String getBcAccountNo(){ return bcAccountNo; }

    public String getCorporateId(){ return corporateId; }

    public String getCountryCode(){ return countryCode; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getPayeeNickName(){ return payeeNickName; }

    public String getBcMobileNo(){ return bcMobileNo; }

    public String getMbcLocation(){ return mbcLocation; }

    public String getPayerNickName(){ return payerNickName; }

    public String getPayerCode(){ return payerCode; }

    public String getUserId(){ return userId; }

    public String getCustId(){ return custId; }

    public String getAuthType(){ return authType; }

    public String getPayerId(){ return payerId; }

    public String getNetworkType(){ return networkType; }

    public String getIsPayerBankCust(){ return isPayerBankCust; }

    public String getIsPayeeBankCust(){ return isPayeeBankCust; }

    public String getBcDistrictCode(){ return bcDistrictCode; }

    public String getTranType(){ return tranType; }

    public String getMobileNo(){ return mobileNo; }

    public String getMerchantId(){ return merchantId; }

    public String getPayeeMobNo(){ return payeeMobNo; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setOriginIpAddr(String val){ this.originIpAddr = val; }
    public void setPayeeCode(String val){ this.payeeCode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setPayerAcctId(String val){ this.payerAcctId = val; }
    public void setPayerName(String val){ this.payerName = val; }
    public void setMerState(String val){ this.merState = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setPayeeAcctId(String val){ this.payeeAcctId = val; }
    public void setPayerIfscCode(String val){ this.payerIfscCode = val; }
    public void setAgentCode(String val){ this.agentCode = val; }
    public void setTranDesc(String val){ this.tranDesc = val; }
    public void setMerCityName(String val){ this.merCityName = val; }
    public void setSegment(String val){ this.segment = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setMerchantBcFlg(String val){ this.merchantBcFlg = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setMerNameAddr(String val){ this.merNameAddr = val; }
    public void setCardNumber(String val){ this.cardNumber = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setMerCountryCode(String val){ this.merCountryCode = val; }
    public void setLastOffusMbcTxn(java.sql.Timestamp val){ this.lastOffusMbcTxn = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranRmks(String val){ this.tranRmks = val; }
    public void setTerminalId(String val){ this.terminalId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setLastMbcTxn(java.sql.Timestamp val){ this.lastMbcTxn = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setErrorCodeDescription(String val){ this.errorCodeDescription = val; }
    public void setPayeeIfscCode(String val){ this.payeeIfscCode = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setBcAccountNo(String val){ this.bcAccountNo = val; }
    public void setCorporateId(String val){ this.corporateId = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setPayeeNickName(String val){ this.payeeNickName = val; }
    public void setBcMobileNo(String val){ this.bcMobileNo = val; }
    public void setMbcLocation(String val){ this.mbcLocation = val; }
    public void setPayerNickName(String val){ this.payerNickName = val; }
    public void setPayerCode(String val){ this.payerCode = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setAuthType(String val){ this.authType = val; }
    public void setPayerId(String val){ this.payerId = val; }
    public void setNetworkType(String val){ this.networkType = val; }
    public void setIsPayerBankCust(String val){ this.isPayerBankCust = val; }
    public void setIsPayeeBankCust(String val){ this.isPayeeBankCust = val; }
    public void setBcDistrictCode(String val){ this.bcDistrictCode = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setPayeeMobNo(String val){ this.payeeMobNo = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_FiTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.terminalId + cardNumber);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String merchantKey="";
	if(!this.terminalId.equalsIgnoreCase("REGISTER") )
		merchantKey= h.getCxKeyGivenHostKey(WorkspaceName.MERCHANT, getHostId(), this.terminalId); 

        wsInfoSet.add(new WorkspaceInfo("Merchant", merchantKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_FiTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "FiTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
