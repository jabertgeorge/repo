clari5.hfdb.entity.nft_dd_purchase{

   attributes = [

        {name = CL5_FLG,  type= "string:20"}
        {name = CREATED_ON,  type= timestamp}
        {name = UPDATED_ON,  type= timestamp}
        {name = ID, type="string:50", key = true}
        {name =  host_id ,type="string:2"}
        {name = sys_time ,type = timestamp}
		{name =  dd_issue_amt ,type = "number:20,2"}
		{name = dd_issue_acct ,type = "string:20"}
        {name = schm_type ,type = "string:20"}
        {name = schm_code ,type = "string:20"}
        {name = dd_issue_custid ,type = "string:20"}
        {name = acct_name ,type = "string:20"}
        {name = acct_sol_id ,type = "string:20"}
        {name = acct_ownership ,type = "string:20"}
        {name = acct_open_date ,type = timestamp}
        {name =  tran_crncy_code ,type = "string:20"}
        {name = ref_txn_amt ,type = "number:20,2"}
        {name = ben_name ,type = "string:20"}
        {name = pstd_user_id ,type = "string:20"}
        {name = tran_category ,type = "string:20"}
        {name = dd_issue_date ,type = timestamp}
        {name = dd_num ,type = "number:20,2"}
        {name = part_tran_srl_num ,type = "number:20,2"}
        {name = ben_account_id ,type = "string:20"}
        {name = ben_bank_code ,type = "string:20"}
        {name =  ben_sol_id ,type = "string:20"}
        {name =  value_date ,type = timestamp}
        {name = acct_status ,type = "string:10"}
        {name = txn_br_id ,type = "string:10"}
        {name = mnemonic_code ,type = "string:10"}
        {name = tran_date ,type = timestamp }
        {name = dd_pur_name ,type= "string:20" }
        {name = tran_code ,type= "string:20" }
     

   ]
}
