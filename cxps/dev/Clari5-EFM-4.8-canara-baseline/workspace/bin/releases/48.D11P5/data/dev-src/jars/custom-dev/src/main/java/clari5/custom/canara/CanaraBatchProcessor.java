package clari5.custom.canara;

import clari5.custom.canara.builder.EventBuilder;
import clari5.custom.canara.config.BepCon;
import clari5.custom.canara.data.ITableData;
import clari5.custom.canara.db.DBTask;
import clari5.custom.canara.query.QueryBuilder;
import clari5.platform.applayer.CxpsDaemon;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;

public class CanaraBatchProcessor extends CxpsDaemon implements ICxResource{

    public  static CxpsLogger cxpsLogger = CxpsLogger.getLogger(CanaraBatchProcessor.class);
    static ConcurrentLinkedQueue<String> tables = new ConcurrentLinkedQueue<String>();
    static ConcurrentLinkedQueue<ITableData> dataque = new ConcurrentLinkedQueue<ITableData>();

    Set<String> inprogress = new ConcurrentSkipListSet<>();
    private final static Object lock = new Object();
    private int count;
    Connection con = null;
    PreparedStatement ps = null;
    boolean staus ;


    @Override
    public Object getData() throws RuntimeFatalException {
        List<String> events =new ArrayList<>();
         cxpsLogger.info("Get Data called");
        synchronized (lock) {
            try {
                if (tables.isEmpty()) {
                    String tableName[] = BepCon.getConfig().getOrderOfTableProc();
                    for (String table : tableName) {
                        tables.add(table);
                    }
                }
            } catch (Exception e) {
                cxpsLogger.info("Not able to fetch the tablename");
            }
            cxpsLogger.info("TAble Added  " + Thread.currentThread().getName());
        }


        synchronized (lock) {


            if (dataque.isEmpty()) {
                String tableName = tables.poll();
                cxpsLogger.info("Table: " + tableName);
                if (tableName != null && !"".equals(tableName)) {
                    QueryBuilder qb = new QueryBuilder();
                    try {
                        staus = qb.select(BepCon.getConfig().getEventApptable(), tableName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(staus) {
                        List<ITableData> data = fetchData(tableName);
                        while (data != null && data.size() > 0 ) {
                            dataque.addAll(data);
                            cxpsLogger.info("Added all the data hence clearing the list [ "+data.size() + " ] ");
                            data.clear();
                           // data = fetchData(tableName);
                        }
                        staus = qb.delete(tableName);
                        if(staus)
                        {
                           cxpsLogger.info("sucessfully removed lock");
                        }
                        else {
                            cxpsLogger.info("error in removing lock");
                        }

                    }
                }
            }
        }

        if (dataque.isEmpty()) {
            return new Object();
        } else {
            return new Object();
        }

    }

    public List<ITableData> fetchData(String tableName){
        synchronized (lock) {
            cxpsLogger.info("Fetch Data from " + tableName);
            List<ITableData> list = new ArrayList<ITableData>();

            try {
                EventLoader loader = new EventLoader();
                list = loader.getFreshEvents(tableName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            cxpsLogger.info("Fetch Data completed for  " + tableName + " with size " + list.size());
            return list;
        }
    }

    @Override
    public void processData(Object o) throws RuntimeFatalException {

        cxpsLogger.info("Process Data");

        ITableData row = null;
        Map<ITableData, String> statusmap = new HashMap<ITableData, String>();
        while ((row = dataque.poll()) != null) {
            cxpsLogger.info("dataque size" + dataque.size() + " by thread" + Thread.currentThread().getName());
            String status = "";
            try {
                //row.process();
                EventBuilder eb = new EventBuilder();
                if (eb.process(row))
                    status = "SUCCESS";
                else
                    status = "FAILED";
            } catch (Exception e) {
                System.out.println("ERROR while processing event.");
                e.printStackTrace();
                cxpsLogger.error("Exception caught while processing data for the row, ", row);
                status = getStackTrc(e);
                if (status.length() > 4000) {
                    status = status.substring(0, 4000);
                }
            }
            statusmap.put(row, status);
        }
        try {
            DBTask.updateStatus(statusmap);
        } catch (Exception e) {
            cxpsLogger.error("Error while updating timestamp");
        }

    }
    public static String getStackTrc(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
    @Override
    public void configure(Hocon h) {
        System.out.println("configuring daemon"+h.toString());
    }

    @Override
    public void release() {

    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public void refresh() {

    }

    private void updateStatus(Map<ITableData, String> statusmap) throws Exception {
        try {
            DBTask.updateStatus(statusmap);
        } catch (Exception e) {
            //////////////////////////////////////////////////////
            // If update status itself fails, what can we do. Think.
            //////////////////////////////////////////////////////
            throw e;
        }
    }
}
