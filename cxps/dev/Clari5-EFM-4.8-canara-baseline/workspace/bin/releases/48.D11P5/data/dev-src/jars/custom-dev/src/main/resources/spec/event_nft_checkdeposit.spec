cxps.events.event.nft_check_deposit {
table-name : EVENT_NFT_CHECK_DEPOSIT
  event-mnemonic: CD
  workspaces : {
    ACCOUNT : account-id
    CUSTOMER: cust-id
    NONCUSTOMER: cheque-number
  }
  event-attributes : {

host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
channel: {db : true ,raw_name : channel ,type : "string:20"}
account-id: {db : true ,raw_name : account_id ,type : "string:20"}
acct-open-date: {db : true ,raw_name : acct_open_date ,type : timestamp}
cheque-number: {db : true ,raw_name : cheque_number ,type : "string:20"}
txn-amt:{type:"number:11,2",db:true,raw_name:txn_amt}
schm-type: {db : true ,raw_name : schm_type ,type : "string:20"}
schm-code: {db : true ,raw_name : schm_code ,type : "string:20"}
cust-id:{type:"string:20",db:true,raw_name:cust_id}
unused-lvs:{type:"string:20",db:true,raw_name:unused_lvs}
cheq1: {db : true ,raw_name : cheq1 ,type : "string:20" }
tran-date:{type:"timestamp",db:true,raw_name:tran_date}
   }
}
