cxps.events.event.nft_regen{
table-name : EVENT_NFT_REGEN
  event-mnemonic: NFR
  workspaces : {
    PAYMENTCARD : cust_card_id,
    CUSTOMER : cust_id,
    NONCUSTOMER : mobile_no
    }
  event-attributes : {
	host_id:{db : true ,raw_name : host_id, type :  "string:2"}
        channel: {db : true ,raw_name : channel ,type : "string:10"}
        user_id: {db : true ,raw_name : user_id ,type : "string:20"}
        cust_id: {db : true ,raw_name : cust_id ,type : "string:20"}
        user_type: {db : true ,raw_name : user_type ,type : "string:20"}
        device_id: {db : true ,raw_name : device_id ,type : "string:50"}
        ip_address: {db : true ,raw_name : ip_address ,type : "string:20"}
        ip_country: {db : true ,raw_name : ip_country ,type : "string:20"}
        ip_city: {db : true ,raw_name : ip_city ,type : "string:20"}
        error_code: {db : true ,raw_name : error_code ,type : "string:10"}
        error_desc: {db : true ,raw_name : error_desc ,type : "string:100"}
        succ_fail_flg: {db : true ,raw_name : succ_fail_flg ,type : "string:2"}
        sys_time: {db : true ,raw_name : sys_time ,type : timestamp}
        cust_card_id: {db : true ,raw_name : cust_card_id ,type : "string:20"}
	regen_type: {db : true ,raw_name : regen_type ,type : "string:20"}
	app_id: {db : true ,raw_name : app_id ,type : "string:50"}
	country_code: {db : true ,raw_name : country_code ,type : "string:20",derivation : """ipToCountryCode(ip_address)"""}
	mobile_no: {db : true ,raw_name : mobile_no ,type : "string:10"}
	tran_date: {db : true ,raw_name : tran_date ,type : timestamp}
	mask_card_no: {db : true ,raw_name : mask_card_no ,type : "string:20"}
        }
}
