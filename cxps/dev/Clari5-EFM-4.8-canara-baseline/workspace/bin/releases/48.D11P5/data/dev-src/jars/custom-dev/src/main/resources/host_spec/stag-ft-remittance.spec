clari5.hfdb.entity.ft_remittance { 
attributes=[ 
{ name =CL5_FLG,  type= "string:20"}
{ name =CREATED_ON,  type= timestamp}
{ name =UPDATED_ON,  type= timestamp}
{ name = ID, type="string:50", key = true} 
{ name = rem_add1, type="string:100", key = true} 
{ name = rem_type, type =  "string:20"}
{ name = acct_open_date, type =  timestamp}
{ name = rem_add3, type =  "string:100"}
{ name = rem_add2, type =  "string:100"}
{ name = purpose_code, type =  "string:50"}
{ name = usd_amount, type =  "number:20,3"}
{ name = ben_city, type =  "string:50"}
{ name = ben_cntry_code, type =  "string:20"}
{ name = ben_add3, type =  "string:200"}
{ name = purpose_desc, type =  "string:200"}
{ name = ben_add2, type =  "string:100"}
{ name = rem_acct_no, type =  "string:20"}
{ name = inr_amt, type =  "number:20,3"}
{ name = rem_name, type =  "string:50"}
{ name = ben_add1, type =  "string:20"}
{ name = account_catagory, type =  "string:20"}
{ name = ben_bic, type =  "string:20"}
{ name = host_id, type =  "string:2"}
{ name = tran_ref_no, type =  "string:30"}
{ name = tran_amt, type =  "number:20,3"}
{ name = rem_cust_id, type =  "string:30"}
{ name = imp_exp_advance, type =  "string:20"}
{ name = ben_name, type =  "string:50"}
{ name = system_type, type =  "string:50"}
{ name = sys_time, type= timestamp}
{ name = branch_id, type =  "string:30"}
{ name = ben_acct_no, type =  "string:30"}
{ name = ben_cust_id, type =  "string:20"}
{ name = rem_cntry_code, type =  "string:20"}
{ name = tran_date,type =  timestamp}
{ name = system, type =  "string:20"}
{ name = tran_year, type =  "string:30"}
{ name = tran_curr, type =  "string:20"}
{ name = rem_city, type =  "string:30"}
{ name = rem_bic, type =  "string:20"}
]
}

