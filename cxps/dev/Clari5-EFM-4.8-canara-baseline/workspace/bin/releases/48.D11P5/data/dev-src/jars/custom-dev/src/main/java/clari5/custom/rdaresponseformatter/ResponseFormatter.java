package clari5.custom.rdaresponseformatter;


import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.CxJson;
import clari5.rda.DecisionMaker;
import clari5.rda.FactSupressible;
import clari5.rda.FormatterType;
import clari5.rda.IResponseFormatter;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.apex.shared.IWSEvent;
import org.json.JSONObject;
import java.util.Set;

public class ResponseFormatter implements  IResponseFormatter{

    private static final CxpsLogger logger = CxpsLogger.getLogger(ResponseFormatter.class);

    private FormatterType type;
    private String action;
    private int score;
    private String scenarioName;
    private Action isFrRequired;
    private Action isAlertRequired;
    private String customRemarks;

    public String getAction() {
        return this.action;
    }

    public JSONObject tojson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", this.type);
        jsonObject.put("action", this.action);
        jsonObject.put("score", this.score);
        jsonObject.put("scenarioName", this.scenarioName);
        jsonObject.put("isFrRequired", this.isFrRequired);
        jsonObject.put("isAlertRequired", this.isAlertRequired);
        jsonObject.put("customRemarks", this.customRemarks);
        return jsonObject;
    }

    public IResponseFormatter fromJson(JSONObject jsonObject) {
        this.action = jsonObject.getString("action");
        this.isFrRequired = Action.valueOf(jsonObject.getString("isFrRequired"));
        this.isAlertRequired = Action.valueOf(jsonObject.getString("isAlertRequired"));
        this.score = jsonObject.getInt("score");
        this.type = FormatterType.valueOf(jsonObject.getString("type"));
        this.scenarioName = jsonObject.getString("scenarioName");
        if (jsonObject.has("customRemarks")) {
            this.customRemarks = jsonObject.getString("customRemarks");
        }

        return this;
    }

    public void setFormatterType(FormatterType type) {
        this.type = type;
    }

    public Action isFrRequired() {
        return this.isFrRequired;
    }

    public String getCustomRemarks() {
        return this.customRemarks;
    }

    public Action isAlertRequired() {
        return this.isAlertRequired;
    }

    public IResponseFormatter populate(DecisionMaker.ResponseDecision responseDecision, FactSupressible factSupressible, IWSEvent event, Set<WorkspaceInfo> workspaceInfos) {
        this.action = responseDecision.orginalResponse;
        if (responseDecision.finalActionObj != null) {
            //this.scenarioName = responseDecision.finalActionObj.scenarioName;
           // this.score = responseDecision.finalActionObj.score;
            this.scenarioName = responseDecision.finalActionObj.getScenarioName();
            this.score = responseDecision.finalActionObj.getScore();


        }

        return this;
    }

    public ResponseFormatter() {
        this.isAlertRequired = Action.YES;
        this.isFrRequired = Action.YES;
    }

    public String getFormattedResponse() {

        switch (type){
            case DECISION:
                return this.action;

            case DECISIONWITHSCN:
                if(this.scenarioName != null)
                    return this.action +"|"+ this.scenarioName;
                else
                    return this.action;

            case CUSTOM:
                if(this.scenarioName != null)
                    return this.action +"|"+ this.scenarioName;
                else
                    return this.action;

            case DECISIONASJSON:
                CxJson cxJson = new CxJson();
                cxJson.put("advice", this.action);
                return cxJson.toJson();

            case DECISIONWITHSCNASJSON:
                CxJson respJson = new CxJson();
                respJson.put("advice", this.action);
                respJson.put("scenario-name", this.scenarioName);
                return respJson.toJson();
            default:
                return this.action;


        }
    }


}