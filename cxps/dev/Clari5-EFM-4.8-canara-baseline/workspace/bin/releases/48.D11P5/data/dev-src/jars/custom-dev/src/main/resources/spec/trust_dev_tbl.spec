cxps.noesis.glossary.entity.trust_dev_tbl {
db-name = TRUST_DEV_TBL
generate = false
db_column_quoted = true
tablespace = CXPS_USERS
attributes = [
                { name = SYS-TIME, column = SYS_TIME, type = "string:30" }
                { name = USER-ID, column = USER_ID, type = "string:20" , key:true}
           	{ name = CUST-ID, column = CUST_ID, type = "string:20"}
              { name = IP-ADDRESS, column = IP_ADDRESS, type = "string:20"}
                { name = DEVICE-ID, column = DEVICE_ID, type = "string:20", key:true}
                { name = DEL-FLAG, column = DEL_FLAG, type = "string:20"}
                { name = LAST-CHG-TS, column = LAST_CHG_TS, type = "string:20"}
                { name = RE-CRE-TS, column = RE_CRE_TS, type = "string:20"}
]
}
