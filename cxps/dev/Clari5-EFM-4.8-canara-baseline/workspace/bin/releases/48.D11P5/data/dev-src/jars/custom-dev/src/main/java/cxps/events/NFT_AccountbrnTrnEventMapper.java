// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AccountbrnTrnEventMapper extends EventMapper<NFT_AccountbrnTrnEvent> {

public NFT_AccountbrnTrnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AccountbrnTrnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AccountbrnTrnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AccountbrnTrnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AccountbrnTrnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AccountbrnTrnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AccountbrnTrnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getGLCode());
            preparedStatement.setString(i++, obj.getSchemeCode());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getSchemeType());
            preparedStatement.setDouble(i++, obj.getAvailableBal());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getBaseBranchCode());
            preparedStatement.setString(i++, obj.getCurrentBranchCode());
            preparedStatement.setString(i++, obj.getNewBranchCode());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_ACCOUNTBRN_TRN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AccountbrnTrnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AccountbrnTrnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCOUNTBRN_TRN"));
        putList = new ArrayList<>();

        for (NFT_AccountbrnTrnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "G_L_CODE",  obj.getGLCode());
            p = this.insert(p, "SCHEME_CODE",  obj.getSchemeCode());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "SCHEME_TYPE",  obj.getSchemeType());
            p = this.insert(p, "AVAILABLE_BAL", String.valueOf(obj.getAvailableBal()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "BASE_BRANCH_CODE",  obj.getBaseBranchCode());
            p = this.insert(p, "CURRENT_BRANCH_CODE",  obj.getCurrentBranchCode());
            p = this.insert(p, "NEW_BRANCH_CODE",  obj.getNewBranchCode());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_ACCOUNTBRN_TRN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_ACCOUNTBRN_TRN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_ACCOUNTBRN_TRN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AccountbrnTrnEvent obj = new NFT_AccountbrnTrnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setGLCode(rs.getString("G_L_CODE"));
    obj.setSchemeCode(rs.getString("SCHEME_CODE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setSchemeType(rs.getString("SCHEME_TYPE"));
    obj.setAvailableBal(rs.getDouble("AVAILABLE_BAL"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setBaseBranchCode(rs.getString("BASE_BRANCH_CODE"));
    obj.setCurrentBranchCode(rs.getString("CURRENT_BRANCH_CODE"));
    obj.setNewBranchCode(rs.getString("NEW_BRANCH_CODE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_ACCOUNTBRN_TRN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AccountbrnTrnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AccountbrnTrnEvent> events;
 NFT_AccountbrnTrnEvent obj = new NFT_AccountbrnTrnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCOUNTBRN_TRN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AccountbrnTrnEvent obj = new NFT_AccountbrnTrnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setGLCode(getColumnValue(rs, "G_L_CODE"));
            obj.setSchemeCode(getColumnValue(rs, "SCHEME_CODE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setSchemeType(getColumnValue(rs, "SCHEME_TYPE"));
            obj.setAvailableBal(EventHelper.toDouble(getColumnValue(rs, "AVAILABLE_BAL")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setBaseBranchCode(getColumnValue(rs, "BASE_BRANCH_CODE"));
            obj.setCurrentBranchCode(getColumnValue(rs, "CURRENT_BRANCH_CODE"));
            obj.setNewBranchCode(getColumnValue(rs, "NEW_BRANCH_CODE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCOUNTBRN_TRN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCOUNTBRN_TRN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"G_L_CODE\",\"SCHEME_CODE\",\"ACCOUNT_ID\",\"SCHEME_TYPE\",\"AVAILABLE_BAL\",\"CUST_ID\",\"BASE_BRANCH_CODE\",\"CURRENT_BRANCH_CODE\",\"NEW_BRANCH_CODE\"" +
              " FROM EVENT_NFT_ACCOUNTBRN_TRN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [G_L_CODE],[SCHEME_CODE],[ACCOUNT_ID],[SCHEME_TYPE],[AVAILABLE_BAL],[CUST_ID],[BASE_BRANCH_CODE],[CURRENT_BRANCH_CODE],[NEW_BRANCH_CODE]" +
              " FROM EVENT_NFT_ACCOUNTBRN_TRN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`G_L_CODE`,`SCHEME_CODE`,`ACCOUNT_ID`,`SCHEME_TYPE`,`AVAILABLE_BAL`,`CUST_ID`,`BASE_BRANCH_CODE`,`CURRENT_BRANCH_CODE`,`NEW_BRANCH_CODE`" +
              " FROM EVENT_NFT_ACCOUNTBRN_TRN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_ACCOUNTBRN_TRN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"G_L_CODE\",\"SCHEME_CODE\",\"ACCOUNT_ID\",\"SCHEME_TYPE\",\"AVAILABLE_BAL\",\"CUST_ID\",\"BASE_BRANCH_CODE\",\"CURRENT_BRANCH_CODE\",\"NEW_BRANCH_CODE\") values(?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[G_L_CODE],[SCHEME_CODE],[ACCOUNT_ID],[SCHEME_TYPE],[AVAILABLE_BAL],[CUST_ID],[BASE_BRANCH_CODE],[CURRENT_BRANCH_CODE],[NEW_BRANCH_CODE]) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`G_L_CODE`,`SCHEME_CODE`,`ACCOUNT_ID`,`SCHEME_TYPE`,`AVAILABLE_BAL`,`CUST_ID`,`BASE_BRANCH_CODE`,`CURRENT_BRANCH_CODE`,`NEW_BRANCH_CODE`) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

