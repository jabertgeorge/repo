package clari5.custom.canara.data;

public class NFT_Mclr extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    private String tableName = "NFT_MCLR";
    private String event_type = "NFT_Mclr";

    private String CL5_FLG;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String CHANNEL;
    private String CUST_ID;
    private String ACCOUNT_ID;
    private String SCHEME_TYPE;
    private String SCHEME_CODE;
    private String GL_CODE;
    private String CHQ_START_NO;
    private String CHQ_END_NO;
    private String ROI;
    private String MCLR;
    private String REN_EXT_NEW_FLG;


    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getSCHEME_TYPE() {
        return SCHEME_TYPE;
    }

    public void setSCHEME_TYPE(String SCHEME_TYPE) {
        this.SCHEME_TYPE = SCHEME_TYPE;
    }

    public String getSCHEME_CODE() {
        return SCHEME_CODE;
    }

    public void setSCHEME_CODE(String SCHEME_CODE) {
        this.SCHEME_CODE = SCHEME_CODE;
    }

    public String getGL_CODE() {
        return GL_CODE;
    }

    public void setGL_CODE(String GL_CODE) {
        this.GL_CODE = GL_CODE;
    }

    public String getCHQ_START_NO() {
        return CHQ_START_NO;
    }

    public void setCHQ_START_NO(String CHQ_START_NO) {
        this.CHQ_START_NO = CHQ_START_NO;
    }

    public String getCHQ_END_NO() {
        return CHQ_END_NO;
    }

    public void setCHQ_END_NO(String CHQ_END_NO) {
        this.CHQ_END_NO = CHQ_END_NO;
    }

    public String getROI() {
        return ROI;
    }

    public void setROI(String ROI) {
        this.ROI = ROI;
    }

    public String getMCLR() {
        return MCLR;
    }

    public void setMCLR(String MCLR) {
        this.MCLR = MCLR;
    }

    public String getREN_EXT_NEW_FLG() {
        return REN_EXT_NEW_FLG;
    }

    public void setREN_EXT_NEW_FLG(String REN_EXT_NEW_FLG) {
        this.REN_EXT_NEW_FLG = REN_EXT_NEW_FLG;
    }





}
