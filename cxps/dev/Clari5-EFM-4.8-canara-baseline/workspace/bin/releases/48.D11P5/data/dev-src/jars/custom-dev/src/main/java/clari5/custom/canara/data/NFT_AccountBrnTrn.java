package clari5.custom.canara.data;

public class NFT_AccountBrnTrn extends ITableData {
    @Override
    public String getTableName() {
        return tableName;
    }

    private String tableName = "NFT_ACCOUNTBRN_TRN";
    private String event_type = "NFT_AccountbrnTrn";

    private String CL5_FLG;
    private String ID;
    private String CREATED_ON;
    private String UPDATED_ON;
    private String SYS_TIME;
    private String CUST_ID;
    private String ACCOUNT_ID;
    private String SCHEME_TYPE;
    private String SCHEME_CODE;
    private String GL_CODE;
    private String CURRENT_BRANCH_CODE;
    private String NEW_BRANCH_CODE;
    private String AVAILABLE_BAL;
    private String BASE_BRANCH_CODE;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getCL5_FLG() {
        return CL5_FLG;
    }

    public void setCL5_FLG(String CL5_FLG) {
        this.CL5_FLG = CL5_FLG;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCREATED_ON() {
        return CREATED_ON;
    }

    public void setCREATED_ON(String CREATED_ON) {
        this.CREATED_ON = CREATED_ON;
    }

    public String getUPDATED_ON() {
        return UPDATED_ON;
    }

    public void setUPDATED_ON(String UPDATED_ON) {
        this.UPDATED_ON = UPDATED_ON;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getACCOUNT_ID() {
        return ACCOUNT_ID;
    }

    public void setACCOUNT_ID(String ACCOUNT_ID) {
        this.ACCOUNT_ID = ACCOUNT_ID;
    }

    public String getSCHEME_TYPE() {
        return SCHEME_TYPE;
    }

    public void setSCHEME_TYPE(String SCHEME_TYPE) {
        this.SCHEME_TYPE = SCHEME_TYPE;
    }

    public String getSCHEME_CODE() {
        return SCHEME_CODE;
    }

    public void setSCHEME_CODE(String SCHEME_CODE) {
        this.SCHEME_CODE = SCHEME_CODE;
    }

    public String getGL_CODE() {
        return GL_CODE;
    }

    public void setGL_CODE(String GL_CODE) {
        this.GL_CODE = GL_CODE;
    }

    public String getCURRENT_BRANCH_CODE() {
        return CURRENT_BRANCH_CODE;
    }

    public void setCURRENT_BRANCH_CODE(String CURRENT_BRANCH_CODE) {
        this.CURRENT_BRANCH_CODE = CURRENT_BRANCH_CODE;
    }

    public String getNEW_BRANCH_CODE() {
        return NEW_BRANCH_CODE;
    }

    public void setNEW_BRANCH_CODE(String NEW_BRANCH_CODE) {
        this.NEW_BRANCH_CODE = NEW_BRANCH_CODE;
    }

    public String getAVAILABLE_BAL() {
        return AVAILABLE_BAL;
    }

    public void setAVAILABLE_BAL(String AVAILABLE_BAL) {
        this.AVAILABLE_BAL = AVAILABLE_BAL;
    }

    public String getBASE_BRANCH_CODE() {
        return BASE_BRANCH_CODE;
    }

    public void setBASE_BRANCH_CODE(String BASE_BRANCH_CODE) {
        this.BASE_BRANCH_CODE = BASE_BRANCH_CODE;
    }








}
