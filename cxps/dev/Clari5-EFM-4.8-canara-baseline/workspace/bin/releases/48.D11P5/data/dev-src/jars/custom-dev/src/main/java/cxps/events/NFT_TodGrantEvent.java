// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_TODGRANT", Schema="rice")
public class NFT_TodGrantEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String glCode;
       @Field(size=20) public String schemeCode;
       @Field(size=20) public String pstdUserId;
       @Field(size=20) public String accountId;
       @Field(size=18) public Double todAmt;
       @Field(size=20) public String dpCode;
       @Field(size=20) public String todRemarks;
       @Field(size=20) public String schemeType;
       @Field(size=20) public String custId;


    @JsonIgnore
    public ITable<NFT_TodGrantEvent> t = AEF.getITable(this);

	public NFT_TodGrantEvent(){}

    public NFT_TodGrantEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("TodGrant");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setGlCode(json.getString("GL_Code"));
            setSchemeCode(json.getString("Scheme_Code"));
            setPstdUserId(json.getString("pstd_user_id"));
            setAccountId(json.getString("account_id"));
            setTodAmt(EventHelper.toDouble(json.getString("TOD_Amt")));
            setDpCode(json.getString("dp_code"));
            setTodRemarks(json.getString("TOD_Remarks"));
            setSchemeType(json.getString("Scheme_Type"));
            setCustId(json.getString("cust_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NFTG"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getGlCode(){ return glCode; }

    public String getSchemeCode(){ return schemeCode; }

    public String getPstdUserId(){ return pstdUserId; }

    public String getAccountId(){ return accountId; }

    public Double getTodAmt(){ return todAmt; }

    public String getDpCode(){ return dpCode; }

    public String getTodRemarks(){ return todRemarks; }

    public String getSchemeType(){ return schemeType; }

    public String getCustId(){ return custId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setGlCode(String val){ this.glCode = val; }
    public void setSchemeCode(String val){ this.schemeCode = val; }
    public void setPstdUserId(String val){ this.pstdUserId = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setTodAmt(Double val){ this.todAmt = val; }
    public void setDpCode(String val){ this.dpCode = val; }
    public void setTodRemarks(String val){ this.todRemarks = val; }
    public void setSchemeType(String val){ this.schemeType = val; }
    public void setCustId(String val){ this.custId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_TodGrantEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_TodGrant");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "TodGrant");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}