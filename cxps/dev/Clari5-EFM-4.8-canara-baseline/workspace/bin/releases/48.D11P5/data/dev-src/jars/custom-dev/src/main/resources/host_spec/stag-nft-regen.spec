clari5.hfdb.entity.nft_regen { 
attributes=[ 

{ name =CL5_FLG,  type= "string:10"}
{ name =CREATED_ON,  type= timestamp}
{name =UPDATED_ON,  type= timestamp}
{ name = ID, type="string:50", key = true} 
{name = sys_time, type =  timestamp}
{name = host_id, type =  "string:2"}
{name = channel ,type = "string:10"}
{name = user_id ,type = "string:20"}
{name = cust_id ,type = "string:20"}
{name = user_type ,type = "string:20"}
{name = device_id ,type = "string:50"}
{name = ip_address ,type = "string:20"}
{name = ip_country ,type = "string:20"}
{name = ip_city ,type = "string:20"}
{name = error_code ,type = "string:10"}
{name = error_desc ,type = "string:100"}
{name = succ_fail_flg ,type = "string:2"}
{name = cust_card_id ,type = "string:20"}
{name = regen_type ,type = "string:20"}
{name = app_id ,type = "string:50"}
{name = country_code ,type = "string:20"}
{name = mobile_no ,type = "string:10"}
{name = tran_date ,type = timestamp}
{name = mask_card_no ,type = "string:20"}
	]
}
