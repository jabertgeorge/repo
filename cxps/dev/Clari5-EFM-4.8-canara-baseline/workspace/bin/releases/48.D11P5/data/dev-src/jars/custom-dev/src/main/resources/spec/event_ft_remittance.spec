cxps.events.event.ft-remittance{  
  table-name : EVENT_FT_REMITTANCE
  event-mnemonic: FR
  workspaces : {
    ACCOUNT : "ben_acct_no,rem_acct_no"
    CUSTOMER : "ben_cust_id,rem_cust_id"
} 
  event-attributes : {
        host-id: {db:true ,raw_name : host_id ,type:"string:2"}
        system: {db:true ,raw_name : system ,type: "string:20"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        rem-type: {db:true ,raw_name : rem_type ,type: "string:20"}
        branch-id: {db : true ,raw_name : branch_id ,type : "string:20"}
        account-catagory: {db : true ,raw_name : account_catagory ,type : "string:20"}
        system-type: {db : true ,raw_name : system_type ,type : "string:20"}
        tran-year: {db : true ,raw_name : tran_year ,type : timestamp}
	tran-ref-no: {db : true ,raw_name : tran_ref_no ,type : "string:20"}
        tran-curr: {db : true ,raw_name : tran_curr ,type : "string:20"}
        tran-amt: {db : true ,raw_name : tran_amt ,type : "number:20,3"}
        inr-amt: {db :true ,raw_name : inr_amt ,type : "number:20,3"}
        usd-amount: {db :true ,raw_name : usd_amount ,type : "number:20,3"}
	purpose-code: {db : true ,raw_name : purpose_code ,type : "string:20"}
	purpose-desc: {db : true ,raw_name : purpose_desc ,type : "string:20"}
        rem-cust-id: {db : true ,raw_name : rem_cust_id ,type : "string:20"}
	rem-name: {db : true ,raw_name : rem_name ,type : "string:20"}
	rem-add1: {db : true ,raw_name : rem_add1 ,type : "string:20"}
        rem-add2: {db : true ,raw_name : rem_add2 ,type : "string:20"}
        rem-add3: {db : true ,raw_name : rem_add3 ,type : "string:20"}
        rem-city: {db : true ,raw_name : rem_city ,type : "string:20"}
        rem-cntry-code: {db : true ,raw_name : rem_cntry_code ,type : "string:20"}
        ben-cust-id: {db :true ,raw_name : ben_cust_id ,type : "string:20"}
        ben-name: {db :true ,raw_name : ben_name ,type : "string:20"}
	ben-add1: {db : true ,raw_name : ben_add1 ,type : "string:10"}
        ben-add2: {db : true ,raw_name : ben_add2 ,type : "string:10"}
        ben-add3: {db : true ,raw_name : ben_add3 ,type : "string:10"}
	ben-city: {db : true ,raw_name : ben_city ,type: "string:20" }
        ben-cntry-code: {db : true ,raw_name : ben_cntry_code ,type: "string:20" }
        ben-acct-no: {db : true ,raw_name : ben_acct_no ,type: "string:20" }
	ben-bic: {db : true ,raw_name : ben_bic ,type: "string:20" }
        rem-acct-no: {db : true ,raw_name : rem_acct_no ,type: "string:20"}
        rem-bic: {db : true ,raw_name : rem_bic ,type: "string:20"}
        tran-date: {db : true ,raw_name : tran_date ,type: timestamp}
	acct-open-date: {db : true ,raw_name : acct_open_date ,type: timestamp}
        imp-exp-advance: {db : true ,raw_name : imp_exp_advance, type: "string:20"}

        
}
}



