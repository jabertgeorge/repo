package clari5.custom.cms;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.jira.display.DefaultEvidenceFormatter;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.HostEvent;
import clari5.hfdb.HostEventHelper;
import clari5.hfdb.scenario.ScenarioHelper;
import clari5.hfdb.scenario.ScnMetaInfo;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
//import cxps.events.NFT_IBLoginEvent;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class CustomEvidenceFormatter extends DefaultEvidenceFormatter {

    private static CxpsLogger logger = CxpsLogger.getLogger(CustomEvidenceFormatter.class);

  /*  public String formatEvidenceRisk(JsonElement eventElement, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        if(eventElement == null) {
            return "There is no evidence for this fact. This can happen if the fact evaluation led to no matching conditions. If you wish to avoid seeing details of such facts, please contact the application adminstrators so that they can modify the Risk Level Assessment and Action Maintenance for this fact.";
        }
        String evidStr = eventElement.toString();
        Hocon h = new Hocon(evidStr);
        StringBuilder resEvid= new StringBuilder();
        if(h.getString("risk1",null) != null) {
            String ev = PPrintRisk.PPrint(h.getString("risk1").replaceAll("`", "\""));
            if (ev.length() > 0) {
                resEvid.append(ev);
            }
        }
        if(h.getString("risk2",null) != null) {
            String remitterevd = PPrintRisk.PPrint(h.getString("risk2").replaceAll("`", "\""));
            if (remitterevd.length() > 0) {
                resEvid.append(remitterevd);
            }
        }
        return resEvid.toString();
    }*/

    public String formatEvidenceSam(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws Exception {
        return formatEvidenceSam(e, hfdbEventDetailsMap, -1);
    }

    public String formatEvidenceSam(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap, int maxEventCount) throws Exception {
        System.out.println("Custom Evidence Formatter");
        Set<String> hfdbEventIds = new HashSet<>();
        CxJson eventJson = e;
        CxJson freeTextEle = eventJson;
        if (freeTextEle == null) {
            logger.error("Unable to find freeText element in SAM evidence.");
            return "Unable to parse evidence details for SAM.";
        }
        String evidStr = freeTextEle.toString();
        StringBuilder resEvid = new StringBuilder();
        StringBuilder evidenceList = new StringBuilder();
        //JsonParser parser = new JsonParser();
        CxJson evidJson = CxJson.parse(evidStr);

        hfdbEventIds.addAll(getUniqueEventIds(eventJson));
        List<String> eventIdList = new ArrayList<>();
        eventIdList.addAll(hfdbEventIds);

        RDBMSSession session = null;
        try {
            session = Rdbms.getAppSession();
            Map<String, HostEvent> hfdbEventDetails = Hfdb.getHostEventHelper().getEventMap(session, eventIdList);
            hfdbEventDetailsMap.putAll(hfdbEventDetails);
        }
        catch(Exception ex){
            System.out.println("Exception caught during improper evidence creation : "+ex.toString());
            throw new CmsException.Retry(ex.getMessage());
        }
        /*catch (CmsException.Retry ex1){
            throw new CmsException.Retry(ex1.getMessage());
        }
        //CUSTOMCHANGE_P_2 : Changes made by prateek on June 18, 2018 to rectify the improper format of JSON
        //-------------------------
        catch(SQLException sqle){
            System.out.println("SQL Exception caught during improper evidence creation : "+sqle.toString());
            throw new CmsException.Retry(sqle.getMessage());
            //logger.error("SQL exception caught during improper evidence creation : "+sqle.toString());
        }*/
        //----------------------------

        finally {
            if (session != null) {
                session.close();
            }
        }

        Map<String, Map<String, List<String>>> evidenceMap = new HashMap<>();

        if (!(evidJson.getString("addOnName") != null && "".equals(evidJson.getString("addOnName")))) {
            Map<String, List<String>> headerMap = new HashMap<>();

             populateHeaderMap(headerMap, evidJson, hfdbEventDetailsMap, maxEventCount);
            if (headerMap.size() > 0) evidenceMap.put(evidJson.getString("addOnName"), headerMap);
        } else {
            Map<String, List<String>> headerMap = new HashMap<>();
             populateHeaderMap(headerMap, evidJson, hfdbEventDetailsMap, maxEventCount);
            if (headerMap.size() > 0) evidenceMap.put("opinion", headerMap);
        }

        for (Map.Entry<String, Map<String, List<String>>> evid : evidenceMap.entrySet()) {
            evidenceList.append("Evidence for ");
            String blockName = evid.getKey();
            Map<String, List<String>> details = evid.getValue();
            if (blockName.equals("opinion")) {
                evidenceList.append("trigger");
            } else {
                evidenceList.append("push up ").append(blockName);
            }
            evidenceList.append("\r\n");
            for (Map.Entry<String, List<String>> entry : details.entrySet()) {
                List<String> evidValues = entry.getValue();
                for (String evStr : evidValues) {
                    if (hfdbEventIds.contains(evStr)) {
                        HostEvent ev = hfdbEventDetailsMap.get(evStr);
                        String english = CustomFormatter.getEnglish(ev);
                        if (english != null) {
                            evidenceList.append(english);
                            evidenceList.append("\r\n");
                        }
                    } else {
                        evidenceList.append(evStr);
                        evidenceList.append("\r\n");
                    }
                }
            }
            evidenceList.append("\r\n");
        }
        resEvid.append(evidenceList.toString());
	    System.out.println("CustomEvidence headers with respective values: " + resEvid.toString());
        return resEvid.toString();
    }

    public String formatSwiftMessage(CxJson eventElement, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        String freeTextEle = (eventElement.toString()).replaceAll("`", "\"");
        freeTextEle = freeTextEle.replaceAll("#curlystart#", "\\\\(").replaceAll("#curlyend#", "\\\\)").replaceAll("#eol#", " \\\\\\\\ ");
        return freeTextEle;
    }

    public String formatEvidenceDefault(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        CxJson eventJson = e;
        StringBuilder resEvid = new StringBuilder();
        logger.error("Error : Evidence source is not identified. It has to be one of the following : - RDE, SAM, WL.");
        resEvid.append(eventJson.toString());
        return resEvid.toString();
    }

    /**
     * Accepts tranDetail in the sequence ||Txn Amount||Txn Type|| Txn Time
     *
     * @param e the evidence json element
     * @return
     * @throws JSONException {
     *                       "addOnName":null,
     *                       "content":"WSKEY:A_F_0100001234512|tranDetail:11111.0 pipe T pipe 1464087871001#/#rdetesting#/#null#/#11111",
     *                       "eventIds":["11111"],
     *                       "evidenceMap":{},
     *                       "incidentSource":"RDE",
     *                       "modified":true,
     *                       "risk1":null,
     *                       "risk2":null,
     *                       "scnName":"rdetesting",
     *                       "swiftMessage":null
     *                       }
     */
    /*public String formatEvidenceRde(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {

        StringBuilder evidenceList = new StringBuilder("");

        try {
            String evidStr = e.getString("content");

            evidenceList.append("||Txn Amount||Txn Type|| Txn Time ||");

            if (evidStr != null) {
                String lhs = evidStr.split("#/#")[0];
                String tranDet = lhs.split("\\|")[1];
                tranDet = tranDet.replaceAll("tranDetail", "").replaceAll(":", "").trim();
                String[] params = tranDet.split("pipe");
                evidenceList.append("\r\n|");
                evidenceList.append(params[0]).append("|").append(params[1]).append("|").append(params[2]).append("|");

            } else {
                logger.error("content is is null in evidenceDetails...");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return evidenceList.toString();
    }*/

    public String formatEvidenceRde(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {

        System.out.println("format Evidence Rde called: using default");
        String sb = null;
        try {
            sb = formatEvidenceSam(e,hfdbEventDetailsMap);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return sb;
        /*StringBuilder sb = new StringBuilder();
        ScnMetaInfo scnMetaInfo = null;
        if (e.getString("addOnName").isEmpty()) {
            sb.append("Evidence For Trigger").append("\r\n");
        } else if (!e.getString("addOnName").isEmpty()) {
            String scnName = e.getString("scnName");
            String content = e.getString("content");
            String[] fields_delimiter = content.split("\\|");
            ScenarioHelper scenarioHelper = new ScenarioHelper();
            String ws = CxKeyHelper.getWorkspaceNameGivenCxKey(fields_delimiter[0].split(":")[1]);
            scnMetaInfo = scenarioHelper.getScnMetaInfo(scnName, ws);
            if (scnMetaInfo != null && scnMetaInfo.getAddOnDetails().get(e.getString("addOnName")) != null) {
                sb.append("\r\n Evidence for push up ").append(scnMetaInfo.getAddOnDetails().get(e.getString("addOnName"))).append("\r\n");
            } else {
                sb.append("\r\n Evidence for push up ").append(e.getString("addOnName")).append("\r\n");
            }
        }

        String evidStr = e.getString("content", "");
        System.out.println("data in evidstr is: "+evidStr);
        if (evidStr.isEmpty()) {
            logger.error("content is null in evidenceDetails...");
            return "||||\r\n||";
        }

        String lhs = evidStr.split("#/#")[0];
        String tranDetails = null;
        String hashedCardNumber = null;
        if (lhs.split("\\|").length > 1){
            String lhsData[] = lhs.split("\\|");
            for (int i = 0; i< lhsData.length; i++){
                if (lhsData[i].startsWith("tranDetail:")){
                    tranDetails = lhsData[i];
                }
                if (lhsData[i].startsWith("WSKEY:P_F_")){
                    String cardno = lhsData[i].substring(10);
                    hashedCardNumber = cardno;
                }
            }
        }else {
            tranDetails = "";
        }

        String tranDet = tranDetails; //lhs.split("\\|").length > 1 ? lhs.split("\\|")[2] : "";
        tranDet = tranDet.replaceAll("tranDetail", "")
                .replaceAll(":", "")
                .replace("{", "")
                .replace("}", "").trim();

        List<String> headers = new ArrayList<>(), params = new ArrayList<>();
        for (String anEviHeader : tranDet.split(";")) {
            String header = anEviHeader.split("-")[0];
            header = header.substring(1, header.length() - 1);
            headers.add(header);
        }

        for (String param : tranDet.split(";")) {
            String firstParam = param.split("-")[1];
            firstParam = firstParam.substring(1, firstParam.length() - 1);
            if (firstParam.equals(hashedCardNumber))
            {
                String unhashedCarNumber = hashUnhash(hashedCardNumber);
                firstParam = unhashedCarNumber;
            }
            String paramToAppend = firstParam.isEmpty() ? "NA" : firstParam;
            params.add(paramToAppend);
        }
        sb.append("||").append(String.join("||", headers)).append("||");
        sb.append("\r\n|").append(String.join("|", params)).append("|");
        System.out.println("returning sb value: "+sb.toString());
        return sb.toString();*/

    }

    public String formatMSTREvidence(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        System.out.println("In ManualSTr Evidence");
        CxJson eventJson = e;
        CxJson responseJson = new CxJson();
        responseJson.put("className", "EvidenceData");
        responseJson.put("incidentSource", eventJson.get("incidentSource"));
        responseJson.put("eventIds", eventJson.get("eventIds"));

        StringBuilder resEvid = new StringBuilder();
        logger.error("Error : Evidence source is not identified. It has to be one of the following : - RDE, SAM, WL.");
        resEvid.append(responseJson.toString());
        System.out.println("response " + responseJson);
        System.out.println("res " + resEvid.toString());
        return resEvid.toString();
    }

    protected Set<String> getUniqueEventIds(CxJson addOnJson) {

        Set<String> out = new HashSet<>();
        CxJson evIdEle = addOnJson.get("eventIds");
        if (evIdEle == null) return out;
        //JsonArray eventIds = evIdEle.getAsJsonArray();
        Iterator<CxJson> eventIds = evIdEle.iterator();
        for (Iterator<CxJson> it = eventIds; it.hasNext(); ) {
            out.add(it.next().toString());
        }
        return out;
    }

    protected void populateHeaderMap(Map<String, List<String>> headerMap, CxJson addOnJson, Map<String, HostEvent> hfdbEventDetailsMap) {
        populateHeaderMap(headerMap, addOnJson, hfdbEventDetailsMap, -1);
    }

    protected void populateHeaderMap(Map<String, List<String>> headerMap, CxJson addOnJson, Map<String, HostEvent> hfdbEventDetailsMap, int maxEventCount) {
        CxJson evIdEle = addOnJson.get("eventIds");
        if (evIdEle == null) return;
        //JsonArray eventIds = evIdEle.getAsJsonArray();
        Iterator<CxJson> eventIds = evIdEle.iterator();
        List<String> eventIdList = new ArrayList<>();
        for (Iterator<CxJson> it = eventIds; it.hasNext(); ) {
            eventIdList.add(it.next().toString());
        }

        /* TODO avoid creation of sorted list in every call by creating the sorted master list once in the caller*/
        List<String> sortedEventIdList = sortByTime(eventIdList, hfdbEventDetailsMap);
        if (maxEventCount > 0 && eventIdList.size() > maxEventCount) {
            sortedEventIdList = sortedEventIdList.subList(0, maxEventCount);
        }

        for (String eventId : sortedEventIdList) {

           // System.out.println("sorted event id " +eventId);
            String eventName = hfdbEventDetailsMap.get(eventId).getEventName();
           // System.out.println("hfdbEventDetailsMap event name "+eventName);
            if (!headerMap.containsKey(eventName)) {
                List<String> addOnValue = new ArrayList<>();
                headerMap.put(eventName, addOnValue);
                String header = getEventHeader(eventName);
                addOnValue.add(header);
            }
            headerMap.get(eventName).add(eventId);
        }
    }

    public static String hashUnhash(String cardNumber) {
        String res = "";
        for(int i = 0;i < cardNumber.length();i++){
            if(cardNumber.charAt(i) == '0')
                res += "1";
            else if(cardNumber.charAt(i) == '1')
                res += "0";
            else
                res += 11 - Integer.parseInt(String.valueOf(cardNumber.charAt(i)));
        }
        return res;
    }

    protected List<String> sortByTime(List<String> eventIdList, Map<String, HostEvent> hfdbEventDetailsMap) {
        List<String> tsEvId = new ArrayList<>();


            for (HostEvent h : hfdbEventDetailsMap.values()) {
            /* assumes all timestamp long values are the same length */
                    if( h != null) {
                       // System.out.println("Event Id is "+h.getEventId());
                       // System.out.println("Event timestamp is " + h.getEventTs());
                        //System.out.println(" Event is " + h.getEvent());
                        if( h.getEventTs() != null ) {
                          //  System.out.println("Inside adding event Id for Evidence" + h.getEventId());
                           // System.out.println(h.getEventTs().getTime());
                            tsEvId.add(String.valueOf(h.getEventTs().getTime()) + ":" + h.getEventId());
                        }
                        else {

                            System.out.println("events is null");
                            if( h.getEventId() != null && h.getEvent().contains("systime")) {

                                JSONObject jsonObject = new JSONObject(h.getEvent());
                                String datetime = jsonObject.getString("systime");
                             //   System.out.println("systime " +datetime );
                                if(datetime != null && !datetime.equalsIgnoreCase("")) {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                                    Date parsedDate = null;
                                    try {
                                        parsedDate = dateFormat.parse(datetime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
                                //    System.out.println(timestamp.getTime());
                                 //   System.out.println("Inside adding event Id for Evidence if eventts is null" + h.getEventId());
                                    tsEvId.add(String.valueOf(timestamp.getTime()) + ":" + h.getEventId());
                                }
                            }
                        }
                    }
            }
            
        Collections.sort(tsEvId);

        List<String> out = new ArrayList<>();
        for (String s : tsEvId) {
            out.add(s.substring(s.indexOf(":") + 1));
        }
        return out;
    }

    protected static String getEventHeader(String eventName) {

        try {
            StringBuilder finalHeader = new StringBuilder("||");
            String header = HostEventHelper.getHeaderFormat(eventName);
            if (header != null) {
                header = header.replaceAll(",", "||");
                finalHeader.append(header);
                return finalHeader.toString();
            }
        } catch (Exception ex) {
            logger.error("Error : Couldn't form header for evidence details :" + ex.getMessage());
        }
        String fallbackHeader = Hfdb.getRefCodeVal("HOST_EVENT_NAME_TO_DESC", eventName);
        return fallbackHeader == null ? eventName : fallbackHeader;


    }
}

