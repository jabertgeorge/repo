package clari5.custom.dev.ISO;

import clari5.iso.formatter.DefaultISOFieldFormatter;
import clari5.iso8583.core.ISO8583;
import clari5.iso8583.formatter.FieldFormatter;
import clari5.platform.logger.CxpsLogger;

import java.nio.charset.Charset;

/*created by vignesh
 * on 4/12/19
 */

public class CustomISOFieldFormatter extends  DefaultISOFieldFormatter{

    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(CustomISOFieldFormatter.class);

    @Override
    public FieldFormatter formatISOMapper(FieldFormatter formatter, ISO8583 iso8583, Charset charset,
                                          String processorResponse, String rawMessage, String eventJson) {
        String res = null;
        cxpsLogger.info("Into custom Iso field formatter");
         String field47 = formatter.getField47();
         cxpsLogger.info("field 47 value is : [ "+field47+ " ]");

         StringBuilder builderf47 = new StringBuilder();
         builderf47.append(field47);

        try {

             res = processorResponse;
             if(res != null) {
                 cxpsLogger.info("Response  from  prod is : [ " + res + "]");
                 if (res.length() > 30) {

                     cxpsLogger.info("Trimming since the response is so long ");
                     res = res.substring(0, 30);
                     cxpsLogger.info("Trimmed Response is  :- [" + res + "]");
                     builderf47.replace(252, 282, res);
                     formatter.setField47(builderf47.toString());
                     cxpsLogger.info("field 47 with Rule id : [ " + builderf47.toString() + " ]");
                     return formatter;

                 } else {
                     cxpsLogger.info("Replacing the field 47 with Rule id");
                     builderf47.replace(252, 282, res);
                     formatter.setField47(builderf47.toString());
                     cxpsLogger.info("field 47 with Rule id : [ " + builderf47.toString() + " ]");
                     return formatter;
                 }
             }
             else {
                 cxpsLogger.info("Response is Null");
             }


        }catch (Exception e){
            e.printStackTrace();
        }
        return formatter;
    }



}