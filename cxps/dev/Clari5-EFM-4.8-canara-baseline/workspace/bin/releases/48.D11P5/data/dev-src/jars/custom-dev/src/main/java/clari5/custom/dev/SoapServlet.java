/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clari5.custom.dev;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPPart;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

/**
 *
 * @author lakshmisai
 */
public class SoapServlet extends HttpServlet {

    public static CxpsLogger logger = CxpsLogger.getLogger(SoapServlet.class);

    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void soapRequest(HttpServletRequest request, HttpServletResponse response) {

        logger.info("Incoming Decide Request from switch");

        String strMsg = "";

        MessageFactory messageFactory = null;
        InputStream inStream = null;
        SOAPMessage soapMessage = null;
        ByteArrayOutputStream out = null;
        PrintWriter writer = null;

        try {
            messageFactory = MessageFactory.newInstance();
            inStream = request.getInputStream();
            soapMessage = messageFactory.createMessage(new MimeHeaders(), inStream);
            logger.info("soap message "+soapMessage);
            logger.info("soap Message to string" +soapMessage.toString());
            //logger.info("soap message get content Description"+soapMessage.getContentDescription());
            //logger.info("soap message get soap header"+soapMessage.getSOAPHeader());
            //logger.info("soap Message get body "+soapMessage.getSOAPBody());
            //logger.info("soap Message get body to string"+soapMessage.getSOAPBody().toString());
            out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            logger.info("After writing to ByteArrayoutputstream .."+out);
            logger.info("ByteArrayoutputstream to string"+out.toString());
            logger.info("ByteArrayoutputstream to byteArray"+out.toByteArray());
            strMsg = new String(out.toByteArray());
            //strMsg = out.toString();

            logger.info("Received xml from switch for RDE "+strMsg);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            if( strMsg != null && strMsg.contains("FRMRQST"))
            {
            //logger.info("Before Parsing=>" + sdf.format(new Date()));
            logger.info("Before Parsing=>" + sdf.format(new Date()));

            JSONObject jsonObject = new JSONObject();
            jsonObject = new FormatJson().getJsonObject(new SaxParser().reqParser(removeXmlStringNamespaceAndPreamble(strMsg)));

            if( jsonObject != null) {
                //logger.info("After Parsing=>" + sdf.format(new Date()));
                logger.info("After Parsing=>" + sdf.format(new Date()));
                SendEvent sendEvent = new SendEvent();

                logger.info("Before Calling Clarif5=>" + sdf.format(new Date()));
                String decideresponse = sendEvent.sendDecideEvent(jsonObject);
                logger.info("After Calling Clarif5=>" + sdf.format(new Date()));

                logger.info(" response =>" + decideresponse);

                strMsg = strMsg.replaceAll("FRMRQST", "FRMRESP");

                if (decideresponse != null && decideresponse.equalsIgnoreCase("allow")) {
                    if (strMsg.contains("<TYP>0200</TYP>")) {
                        strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                        if (strMsg.contains("<RESP_CDE>")) {
                            int j = strMsg.indexOf("<RESP_CDE>");
                            String substring = strMsg.substring(j, j + 23);
                            strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                        } else {
                            strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                        }
                    } else {
                        logger.info("Invalid xml => <TYP> not present in xml");
                    }
                    logger.info("response => final xml for allow " + strMsg);

                    response.setStatus(200);
                    response.setContentType("text/xml");
                    writer = response.getWriter();
                    writer.append(strMsg);
                    writer.flush();
                } else if (decideresponse != null && decideresponse.contains("|")) {
                    String arg[] = decideresponse.split("_");
                    if (strMsg.contains("<TYP>0200</TYP>")) {

                        strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                        if (strMsg.contains("<RESP_CDE>")) {
                            int j = strMsg.indexOf("<RESP_CDE>");
                            String substring = strMsg.substring(j, j + 23);
                            strMsg = strMsg.replaceAll(substring, "<RESP_CDE>05</RESP_CDE>");
                            strMsg = strMsg.replaceAll("<RULE_ID/>", "<RULE_ID>" + arg[1] + "</RULE_ID>");
                        } else {
                            strMsg = strMsg.replaceAll("<RULE_ID/>", "<RULE_ID>" + arg[1] + "</RULE_ID>");
                            strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>05</RESP_CDE><RULE_ID>" + arg[1] + "</RULE_ID>");
                        }
                    } else {
                        logger.info("Invalid xml => <TYP> not present in xml");
                    }

                    logger.info("response => final xml for decline " + strMsg);
                    response.setStatus(200);
                    response.setContentType("text/xml");
                    writer = response.getWriter();
                    writer.append(strMsg);
                    writer.flush();
                } else {

                    if (strMsg.contains("<TYP>0200</TYP>")) {
                        strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                        if (strMsg.contains("<RESP_CDE>")) {
                            int j = strMsg.indexOf("<RESP_CDE>");
                            String substring = strMsg.substring(j, j + 23);
                            strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                        } else {
                            strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                        }
                    } else {
                        logger.info("Invalid xml => <TYP> not present in xml");
                    }

                    logger.info("Invalid response on decide hence allowing");
                    response.setStatus(200);
                    response.setContentType("text/xml");
                    writer = response.getWriter();
                    writer.append(strMsg);
                    writer.flush();
                }
                logger.info("End of the request=>" + sdf.format(new Date()));
             }
             else {
                logger.info("json is invalid");
                if (strMsg.contains("<TYP>0200</TYP>")) {
                    strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                    if (strMsg.contains("<RESP_CDE>")) {
                        int j = strMsg.indexOf("<RESP_CDE>");
                        String substring = strMsg.substring(j, j + 23);
                        strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                    } else {
                        strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                    }
                } else {
                    logger.info("Invalid xml => <TYP> not present in xml");
                }
                response.setStatus(200);
                response.setContentType("text/xml");
                writer = response.getWriter();
                writer.append(strMsg);
                writer.flush();
              }
            }
            else {
                logger.info("Invalid xml or xml is null");
                response.setStatus(200);
                response.setContentType("text/xml");
                writer = response.getWriter();
                writer.append(strMsg);
                writer.flush();
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("exception");
            if (strMsg.contains("<TYP>0200</TYP>")) {
                strMsg = strMsg.replaceAll("<TYP>0200</TYP>", "<TYP>0210</TYP>");
                strMsg = strMsg.replaceAll("FRMRQST", "FRMRESP");
                if (strMsg.contains("<RESP_CDE>")) {
                    int j = strMsg.indexOf("<RESP_CDE>");
                    String substring = strMsg.substring(j, j + 23);
                    strMsg = strMsg.replaceAll(substring, "<RESP_CDE>00</RESP_CDE>");
                } else {
                    strMsg = strMsg.replace("<FRMRESP>", "<FRMRESP><RESP_CDE>00</RESP_CDE>");
                }
            } else {
                logger.info("Invalid xml => <TYP> not present in xml");
            }
            response.setStatus(200);
            response.setContentType("text/xml");
            try {
                writer = response.getWriter();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            writer.append(strMsg);
            writer.flush();

        } finally {
            try {
                if(inStream != null) {
                    inStream.close();
                }
                if( out != null) {
                    out.close();
                }
                //writer.close();
                messageFactory = null;
                soapMessage = null;
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            // Retrieve different parts
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
            // Two ways to extract headers
            SOAPHeader soapHeader = soapEnvelope.getHeader();
            soapHeader = soapMessage.getSOAPHeader();
            // Two ways to extract body
            SOAPBody soapBody = soapEnvelope.getBody();
            soapBody = soapMessage.getSOAPBody();
            // To add some element
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            Name bodyName = soapFactory.createName("getEmployeeDetails", "ns1", "urn:MySoapServices");
            SOAPBodyElement purchaseLineItems = soapBody.addBodyElement(bodyName);
            Name childName = soapFactory.createName("param1");
            SOAPElement order = purchaseLineItems.addChildElement(childName);
            order.addTextNode("1016577");

        } catch (Exception e) {
            e.printStackTrace();
        }
             */
        }
    }

    

    

    public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
        return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
                replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
                .replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
                .replaceAll("(</)(\\w+:)(.*?>)", "$1$3");
        /* remove closing tags prefix */
    }
}
