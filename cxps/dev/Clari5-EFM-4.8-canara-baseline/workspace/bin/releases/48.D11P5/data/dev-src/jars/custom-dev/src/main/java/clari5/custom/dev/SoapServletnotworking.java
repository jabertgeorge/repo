/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clari5.custom.dev;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPPart;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

/**
 *
 * @author lakshmisai
 */
public class SoapServletnotworking extends HttpServlet {

    public static CxpsLogger logger = CxpsLogger.getLogger(SoapServlet.class);
    public static long timeout = 500;

    public void init() throws ServletException {

        Hocon hocon = new Hocon();
        hocon.loadFromContext("rda-clari5.conf");
        timeout = hocon.getLong("clari5.response.timeout", timeout);

        // Upper limit is 2 seconds
        if (timeout > 2000) timeout = 2000;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        soapRequest(request, response);
    }

    protected void soapRequest(HttpServletRequest request, HttpServletResponse response) {

        long start = System.currentTimeMillis();
        boolean timeExceeded = false;
        PrintWriter writer = null;

        SoapExecutor soapExecutor = new SoapExecutor(request);
        soapExecutor.start();

        try {
            soapExecutor.join(timeout);
        } catch (InterruptedException e) { }

        if(soapExecutor.isAlive()) timeExceeded = true;

        String responseDecision = null;
        try {

            responseDecision = soapExecutor.getResponseDecision().orElse(soapExecutor.getDefaultResponse());

            response.setStatus(200);
            response.setContentType("text/xml");
            writer = response.getWriter();
            writer.append(responseDecision);
            writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(writer != null) writer.close();
        }

        long end = System.currentTimeMillis();

        logger.debug("Time to soap Request took " + (end-start) +" Time exceeded [ " + timeExceeded + " ] response [" + responseDecision );
    }

}
