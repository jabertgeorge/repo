{
  "container": {
    "name": "tomcat",
    "version": "9.0.8"
  },
  "env-params": {
    "CMQ Archive Location": {
      "attributes": [
        {
          "name": "CMQ_ARCHIVE_LOCATION",
          "tag": "CMQ ARCHIVE Directory",
          "value": ""
        }
      ],
      "description": "ARCHIVE Location For All CMQ Queues. (Ideally mounted on NAS disk with at-least 100 GB of Free Space)",
      "type": "Directory Location",
      "validation-class": "clari5.cc.metaconf.DirectoryValidator",
      "validation-fields": {
        "path": "CMQ_ARCHIVE_LOCATION"
      }
    },
    "CMQ Base Location": {
      "attributes": [
        {
          "name": "BASE_Q_LOCATION",
          "tag": "BASE Q Directory",
          "value": ""
        }
      ],
      "description": "Directory Location For BASE Folder for various queues. (Ideally mounted on NAS disk with at-least 100 GB of Free Space)",
      "type": "Directory Location",
      "validation-class": "clari5.cc.metaconf.DirectoryValidator",
      "validation-fields": {
        "path": "BASE_Q_LOCATION"
      }
    },
    "debug-mode": {
      "attributes": [
        {
          "allowed": [
            "true",
            "false"
          ],
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "DEBUG_MODE"
          "tag": "Turn On App DEBUG Logging"
          "type": "boolean",
          "value": "true"
        }
      ],
      "description": "Turn On App DEBUG Logging",
      "type": "DEBUG FLAG"
    },
    "rice": {
      "attributes": [
        {
          "allowed": [
            "oracle",
            "sqlserver"
          ],
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "DB_TYPE",
          "tag": "Database Type",
          "type": "string",
          "value": ""
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "DB_IP",
          "tag": "Database IP",
          "type": "string",
          "validationPattern": "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)",
          "value": ""
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "number",
          "editable": true,
          "mandatory": true,
          "name": "DB_PORT",
          "tag": "Database Port",
          "type": "integer",
          "value": ""
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "DB_SID",
          "tag": "Database SID",
          "type": "string",
          "value": ""
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "DB_USER",
          "tag": "Database User",
          "type": "string",
          "value": ""
        },
        {
          "allowed": [
            "SIMPLE",
            "1WAY"
          ],
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RICE_PMTYPE",
          "tag": "Password type",
          "type": "string",
          "value": ""
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "password",
          "editable": true,
          "mandatory": true,
          "name": "RICE_PMSEED",
          "tag": "Password Seed",
          "type": "string",
          "value": ""
        }
      ],
      "description": "Database parameters for Application DB",
      "type": "RDBMS",
      "validation-class": "clari5.cc.metaconf.RDBMSValidator",
      "validation-fields": {
        "db-ip": "DB_IP",
        "db-pmseed": "RICE_PMSEED",
        "db-pmtype": "RICE_PMTYPE",
        "db-port": "DB_PORT",
        "db-sid": "DB_SID",
        "db-type": "DB_TYPE",
        "db-user": "DB_USER"
      }
    },
    "wl": {
      "attributes": [
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CX_DATA_DIR",
          "tag": "Data Directory",
          "type": "string",
          "value": ""
        }
      ],
      "description": "Data Directory for Watch-list ",
      "type": "Watch-List"
    }
  },
  "load-balancer": {
    "failonstatus": "503",
    "lbmethod": "bybusyness",
    "lbparameters": "retry=120",
    "stickysession": "random",
    "uris": [
      {
        "ackUri": "",
        "rewrite": "canaraeventprocessor/",
        "security": "CAS",
        "uri": "canaraeventprocessor"
      },
      {
        "ackUri": "",
        "rewrite": "canaraeventprocessor/rest",
        "security": "Basic",
        "uri": "canaraeventprocessor/rest"
      }
    ]
  },
  "system-params": {
    "CANARAEVENTPROCESSOR": {
      "attributes": [
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "number",
          "editable": true,
          "mandatory": true,
          "name": "CANARAEVENTPROCESSOR_C3P0_MIN_POOL",
          "tag": "C3P0 minimum pool size",
          "type": "integer",
          "value": 0
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "number",
          "editable": true,
          "mandatory": true,
          "name": "CANARAEVENTPROCESSOR_C3P0_MAX_POOL",
          "tag": "C3P0 maximum pool size",
          "type": "integer",
          "value": 5
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "number",
          "editable": true,
          "mandatory": true,
          "name": "CANARAEVENTPROCESSOR_C3P0_MAX_STATEMENTS",
          "tag": "C3P0 maximum statements",
          "type": "integer",
          "value": 0
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "number",
          "editable": true,
          "mandatory": true,
          "name": "CANARAEVENTPROCESSOR_C3P0_C3P0_TIMEOUT",
          "tag": "C3P0 timeout",
          "type": "integer",
          "value": 0
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "number",
          "editable": true,
          "mandatory": true,
          "name": "CANARAEVENTPROCESSOR_C3P0_C3P0_HELPERS",
          "tag": "C3P0 helpers",
          "type": "integer",
          "value": 10
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "JIRA_DISABLE_RESOURCE_CHECK",
          "tag": "Disable Resource Check for JIRA",
          "type": "boolean",
          "value": false
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_ENAME",
          "tag": "case event entity name",
          "type": "string",
          "value": "customfield_10513"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_EID",
          "tag": "case event entity id",
          "type": "string",
          "value": "customfield_11400"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_DKEY",
          "tag": "case event display key",
          "type": "string",
          "value": "customfield_10516"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_SCORE",
          "tag": "case event score",
          "type": "string",
          "value": "customfield_10106"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_PRIORITY",
          "tag": "case event priority",
          "type": "string",
          "value": "customfield_10304"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_DOI",
          "tag": "cms suspision details in case of investigation",
          "type": "string",
          "value": "customfield_10603"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_AI",
          "tag": "cms suspision details for alert indicator",
          "type": "string",
          "value": "customfield_11101"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_MERCHANT",
          "tag": "cms incident merchent",
          "type": "string",
          "value": "customfield_10704"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_DEVICE",
          "tag": "cms incident device",
          "type": "string",
          "value": "customfield_10703"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_OPERATOR",
          "tag": "cms incident operator",
          "type": "string",
          "value": "customfield_10705"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_MESSAGE",
          "tag": "cms incident message",
          "type": "string",
          "value": "customfield_10900"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_FNAME",
          "tag": "cms incident fact name",
          "type": "string",
          "value": "customfield_10211"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_SCORE",
          "tag": "cms incident score",
          "type": "string",
          "value": "customfield_10212"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_TS",
          "tag": "cms incident timestamp",
          "type": "string",
          "value": "customfield_10213"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_EID",
          "tag": "cms incident entity id",
          "type": "string",
          "value": "customfield_11401"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_DKEY",
          "tag": "cms incident display key",
          "type": "string",
          "value": "customfield_10214"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_CITY",
          "tag": "cms incident city",
          "type": "string",
          "value": "customfield_10511"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_CUMCDEP",
          "tag": "cms incident cumulative cash deposit",
          "type": "string",
          "value": "customfield_10302"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_CUMCWDL",
          "tag": "cms incident cumulative cash withdrawl",
          "type": "string",
          "value": "customfield_10303"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_CUMCR",
          "tag": "cms incident cumulative creditor",
          "type": "string",
          "value": "customfield_10300"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_CUMDR",
          "tag": "cms incident cumulative debitor",
          "type": "string",
          "value": "customfield_10301"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_BRANCH",
          "tag": "cms incident branch",
          "type": "string",
          "value": "customfield_10215"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_COUNTRY",
          "tag": "cms incident country",
          "type": "string",
          "value": "customfield_10504"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_ENAME",
          "tag": "cms incident entity name",
          "type": "string",
          "value": "customfield_10505"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_C_AGE",
          "tag": "cms incident customer age",
          "type": "string",
          "value": "customfield_10506"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_C_TYPE",
          "tag": "cms incident customer type",
          "type": "string",
          "value": "customfield_10507"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_RL",
          "tag": "cms incident risk level",
          "type": "string",
          "value": "customfield_10610"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_EVIDENCE",
          "tag": "cms incident evidence",
          "type": "string",
          "value": "customfield_10503"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CR_RESOLUTION",
          "tag": "cms resolution resolution",
          "type": "string",
          "value": "resolution_type"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CR_FNAME",
          "tag": "cms resolution fact name",
          "type": "string",
          "value": "customfield_10408"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CR_FTYPE",
          "tag": "cms resolution fact type",
          "type": "string",
          "value": "customfield_10407"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RD_FR_DDATE",
          "tag": "cms resolution details fraud detection date",
          "type": "string",
          "value": "customfield_10400"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RD_EXPOSURE",
          "tag": "cms resolution details exposure",
          "type": "string",
          "value": "customfield_10401"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RD_SAVINGS",
          "tag": "cms resolution details savings",
          "type": "string",
          "value": "customfield_10403"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RD_ACTIONTAKEN",
          "tag": "cms resolution details action taken",
          "type": "string",
          "value": "customfield_10404"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RD_LOCATION",
          "tag": "cms resolution details location",
          "type": "string",
          "value": "customfield_10405"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "RD_FR_TYPE",
          "tag": "cms resolution details fraud type",
          "type": "string",
          "value": "customfield_10406"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_NOOFSTR",
          "tag": "case event no of STR",
          "type": "string",
          "value": "customfield_11301"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CE_LF_STR",
          "tag": "case event last filed STR",
          "type": "string",
          "value": "customfield_11300"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_MAIN_PNAME",
          "tag": "cms suspision details main person name",
          "type": "string",
          "value": "customfield_10000"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_SOA",
          "tag": "cms suspision details source of alert",
          "type": "string",
          "value": "customfield_10607"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_ATT_TXN",
          "tag": "cms suspision details attempted transaction",
          "type": "string",
          "value": "customfield_10602"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_LEA_INF",
          "tag": "cms suspision details LEA informed",
          "type": "string",
          "value": "customfield_10604"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_ADD_DOCS",
          "tag": "cms suspision details additional documents",
          "type": "string",
          "value": "customfield_10600"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_PR_RATING",
          "tag": "cms suspision details priority rating",
          "type": "string",
          "value": "customfield_10605"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_RC",
          "tag": "cms suspision details report coverage",
          "type": "string",
          "value": "customfield_10606"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_CX_TXN",
          "tag": "cms suspision details complex transaction",
          "type": "string",
          "value": "customfield_10207"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_FIN_TERROR",
          "tag": "cms suspision details financing terrorism",
          "type": "string",
          "value": "customfield_10208"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_NOECORATIONALE",
          "tag": "cms suspision details no economic rationale",
          "type": "string",
          "value": "customfield_10209"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_PRO_CRIME",
          "tag": "cms suspision details proceeds of crime",
          "type": "string",
          "value": "customfield_10210"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_LEA_DETAILS",
          "tag": "cms suspision details LEA details",
          "type": "string",
          "value": "customfield_11100"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_A_DETAILS",
          "tag": "cms suspision details accounts details",
          "type": "string",
          "value": "customfield_10114"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_IDETAILS",
          "tag": "cms suspision details grounds of suspision isolated details",
          "type": "string",
          "value": "customfield_11102"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_BG",
          "tag": "cms suspision details grounds of suspision background",
          "type": "string",
          "value": "customfield_11103"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_REL_BEG_DATE",
          "tag": "cms suspision details grounds of suspision relationship begin date",
          "type": "string",
          "value": "customfield_10101"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_PDET",
          "tag": "cms suspision details grounds of suspision procedure for detection",
          "type": "string",
          "value": "customfield_11104"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_EXP_SUB",
          "tag": "cms suspision details grounds of suspision explaination from subject",
          "type": "string",
          "value": "customfield_11105"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_SUMMARY",
          "tag": "cms suspision details grounds of suspision summary of suspicion",
          "type": "string",
          "value": "customfield_11106"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_TRAN_LINK",
          "tag": "cms suspision details grounds of suspision transaction linkage",
          "type": "string",
          "value": "customfield_11107"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_BEN_DETAILS",
          "tag": "cms suspision details grounds of suspision benefitiary details",
          "type": "string",
          "value": "customfield_11108"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_TXNS",
          "tag": "cms suspision details grounds of suspision volume of transaction",
          "type": "string",
          "value": "customfield_10205"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "SD_GS_ADDINFO",
          "tag": "cms suspision details grounds of suspision additional information",
          "type": "string",
          "value": "customfield_11109"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_NOOFSTR",
          "tag": "cms incident no of STR",
          "type": "string",
          "value": "customfield_10508"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "CI_LF_STR",
          "tag": "cms incident last filed STR",
          "type": "string",
          "value": "customfield_11200"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "TRIGGER_ID",
          "tag": "trigger identifier",
          "type": "string",
          "value": "customfield_11607"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "editable": true,
          "mandatory": true,
          "name": "JAVA_OPTS",
          "tag": "Memory requirement (RAM)",
          "type": "string",
          "value": "-Xms128m -Xmx1024m"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "JIRA_EVIDENCE_FORMATTER",
          "tag": "jira evidence formatter class",
          "type": "string",
          "value": "clari5.aml.cms.jira.display.DefaultEvidenceFormatter"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "JIRA_FORMATTER",
          "tag": "jira formatter class",
          "type": "string",
          "value": "clari5.aml.cms.newjira.DefaultCmsFormatter"
        },
        {
          "css": [
            "editable",
            "mandatory"
          ],
          "displayType": "text",
          "editable": true,
          "mandatory": true,
          "name": "JIRA_CUSTOM_FIELD_APPENDER",
          "tag": "jira custom field appender, if not required, input 'none'.",
          "type": "string",
          "value": "NONE"
        }
      ],
      "description": "Configuration parameters for Application",
      "type": "APP"
    }
  }
}

