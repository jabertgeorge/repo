// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCTINQUIRY", Schema="rice")
public class NFT_AcctInquiryEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String channel;
       @Field(size=20) public String maskCardNo;
       @Field(size=2) public String hostId;
       @Field(size=20) public String schmType;
       @Field(size=20) public String countryCode;
       @Field(size=20) public String custCardId;
       @Field(size=11) public Double avlBal;
       @Field(size=20) public String accountId;
       @Field(size=20) public String schmCode;
       @Field(size=20) public String userId;
       @Field(size=20) public String branchId;
       @Field(size=20) public String acctStatus;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=50) public String acctName;
       @Field(size=20) public String custId;
       @Field(size=50) public String menuId;


    @JsonIgnore
    public ITable<NFT_AcctInquiryEvent> t = AEF.getITable(this);

	public NFT_AcctInquiryEvent(){}

    public NFT_AcctInquiryEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AcctInquiry");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setChannel(json.getString("channel"));
            setMaskCardNo(json.getString("mask_card_no"));
            setHostId(json.getString("host_id"));
            setSchmType(json.getString("schm_type"));
            setCountryCode(json.getString("country_code"));
            setCustCardId(json.getString("cust_card_id"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setAccountId(json.getString("account_id"));
            setSchmCode(json.getString("schm_code"));
            setUserId(json.getString("user_id"));
            setBranchId(json.getString("branch_id"));
            setAcctStatus(json.getString("acct_status"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setAcctName(json.getString("acct_name"));
            setCustId(json.getString("cust_id"));
            setMenuId(json.getString("menu_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getChannel(){ return channel; }

    public String getMaskCardNo(){ return maskCardNo; }

    public String getHostId(){ return hostId; }

    public String getSchmType(){ return schmType; }

    public String getCountryCode(){ return countryCode; }

    public String getCustCardId(){ return custCardId; }

    public Double getAvlBal(){ return avlBal; }

    public String getAccountId(){ return accountId; }

    public String getSchmCode(){ return schmCode; }

    public String getUserId(){ return userId; }

    public String getBranchId(){ return branchId; }

    public String getAcctStatus(){ return acctStatus; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getAcctName(){ return acctName; }

    public String getCustId(){ return custId; }

    public String getMenuId(){ return menuId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setMaskCardNo(String val){ this.maskCardNo = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSchmType(String val){ this.schmType = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setSchmCode(String val){ this.schmCode = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setMenuId(String val){ this.menuId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_AcctInquiryEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AcctInquiry");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AcctInquiry");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}