cxps.noesis.glossary.entity.HIGH_RISKY_TBL {
	db-name = HIGH_RISKY_TBL
	generate = false
	db_column_quoted = true
	
	tablespace = CXPS_USERS
	attributes = [ 
		{ name = country-code, column = Country_Code, type = "string:2" }
		{ name = country-name, column = Country_Name, type = "string:20" }
		{ name = risky-ip, column = Risky_ip, type = "string:10", key=false }
		{ name = risky-mcc, column = Risky_mcc, type = "string:20", key=false }
		{ name = risky-atm-id, column = Risky_Atm_Id, type = "string:20", key=false }
		{ name = risky-marchant-id, column = Risky_Marchant_Id, type = "string:20", key=false }		
	]
}
