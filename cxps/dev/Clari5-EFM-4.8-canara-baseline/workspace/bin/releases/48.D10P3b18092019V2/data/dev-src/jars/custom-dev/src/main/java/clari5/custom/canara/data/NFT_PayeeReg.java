package clari5.custom.canara.data;

public class NFT_PayeeReg extends ITableData {

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String tableName = "NFT_PAYEEREG";
    private String event_type = "NFT_PayeeReg";

    private String ID;
    private String SYS_TIME;
    private String HOST_ID;
    private String CHANNEL ;
    private String USER_ID ;
    private String USER_TYPE ;
    private String CUST_ID ;
    private String DEVICE_ID ;
    private String IP_ADDRESS ;
    private String IP_COUNTRY ;
    private String IP_CITY ;
    private String SUCC_FAIL_FLG ;
    private String ERROR_CODE ;
    private String ERROR_DESC ;
    private String PAYEE_TYPE ;
    private String PAYEE_ID ;
    private String PAYEE_NAME ;
    private String PAYEE_NICKNAME ;
    private String PAYEE_ACCOUNTTYPE ;
    private String PAYEE_IFSC_CODE ;
    private String PAYEE_ACCOUNT_ID ;
    private String PAYEE_BANK_NAME ;
    private String PAYEE_BANK_ID ;
    private String PAYEE_BRANCH_NAME ;
    private String PAYEE_BRANCH_ID ;
    private String CUST_NAME ;
    private String TRAN_DATE ;
    private String PAYEEACTIVATION_FLG ;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSYS_TIME() {
        return SYS_TIME;
    }

    public void setSYS_TIME(String SYS_TIME) {
        this.SYS_TIME = SYS_TIME;
    }

    public String getHOST_ID() {
        return HOST_ID;
    }

    public void setHOST_ID(String HOST_ID) {
        this.HOST_ID = HOST_ID;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSER_TYPE() {
        return USER_TYPE;
    }

    public void setUSER_TYPE(String USER_TYPE) {
        this.USER_TYPE = USER_TYPE;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
    }

    public String getIP_COUNTRY() {
        return IP_COUNTRY;
    }

    public void setIP_COUNTRY(String IP_COUNTRY) {
        this.IP_COUNTRY = IP_COUNTRY;
    }

    public String getIP_CITY() {
        return IP_CITY;
    }

    public void setIP_CITY(String IP_CITY) {
        this.IP_CITY = IP_CITY;
    }

    public String getSUCC_FAIL_FLG() {
        return SUCC_FAIL_FLG;
    }

    public void setSUCC_FAIL_FLG(String SUCC_FAIL_FLG) {
        this.SUCC_FAIL_FLG = SUCC_FAIL_FLG;
    }

    public String getERROR_CODE() {
        return ERROR_CODE;
    }

    public void setERROR_CODE(String ERROR_CODE) {
        this.ERROR_CODE = ERROR_CODE;
    }

    public String getERROR_DESC() {
        return ERROR_DESC;
    }

    public void setERROR_DESC(String ERROR_DESC) {
        this.ERROR_DESC = ERROR_DESC;
    }

    public String getPAYEE_TYPE() {
        return PAYEE_TYPE;
    }

    public void setPAYEE_TYPE(String PAYEE_TYPE) {
        this.PAYEE_TYPE = PAYEE_TYPE;
    }

    public String getPAYEE_ID() {
        return PAYEE_ID;
    }

    public void setPAYEE_ID(String PAYEE_ID) {
        this.PAYEE_ID = PAYEE_ID;
    }

    public String getPAYEE_NAME() {
        return PAYEE_NAME;
    }

    public void setPAYEE_NAME(String PAYEE_NAME) {
        this.PAYEE_NAME = PAYEE_NAME;
    }

    public String getPAYEE_NICKNAME() {
        return PAYEE_NICKNAME;
    }

    public void setPAYEE_NICKNAME(String PAYEE_NICKNAME) {
        this.PAYEE_NICKNAME = PAYEE_NICKNAME;
    }

    public String getPAYEE_ACCOUNTTYPE() {
        return PAYEE_ACCOUNTTYPE;
    }

    public void setPAYEE_ACCOUNTTYPE(String PAYEE_ACCOUNTTYPE) {
        this.PAYEE_ACCOUNTTYPE = PAYEE_ACCOUNTTYPE;
    }

    public String getPAYEE_IFSC_CODE() {
        return PAYEE_IFSC_CODE;
    }

    public void setPAYEE_IFSC_CODE(String PAYEE_IFSC_CODE) {
        this.PAYEE_IFSC_CODE = PAYEE_IFSC_CODE;
    }

    public String getPAYEE_ACCOUNT_ID() {
        return PAYEE_ACCOUNT_ID;
    }

    public void setPAYEE_ACCOUNT_ID(String PAYEE_ACCOUNT_ID) {
        this.PAYEE_ACCOUNT_ID = PAYEE_ACCOUNT_ID;
    }

    public String getPAYEE_BANK_NAME() {
        return PAYEE_BANK_NAME;
    }

    public void setPAYEE_BANK_NAME(String PAYEE_BANK_NAME) {
        this.PAYEE_BANK_NAME = PAYEE_BANK_NAME;
    }

    public String getPAYEE_BANK_ID() {
        return PAYEE_BANK_ID;
    }

    public void setPAYEE_BANK_ID(String PAYEE_BANK_ID) {
        this.PAYEE_BANK_ID = PAYEE_BANK_ID;
    }

    public String getPAYEE_BRANCH_NAME() {
        return PAYEE_BRANCH_NAME;
    }

    public void setPAYEE_BRANCH_NAME(String PAYEE_BRANCH_NAME) {
        this.PAYEE_BRANCH_NAME = PAYEE_BRANCH_NAME;
    }

    public String getPAYEE_BRANCH_ID() {
        return PAYEE_BRANCH_ID;
    }

    public void setPAYEE_BRANCH_ID(String PAYEE_BRANCH_ID) {
        this.PAYEE_BRANCH_ID = PAYEE_BRANCH_ID;
    }

    public String getCUST_NAME() {
        return CUST_NAME;
    }

    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    public String getTRAN_DATE() {
        return TRAN_DATE;
    }

    public void setTRAN_DATE(String TRAN_DATE) {
        this.TRAN_DATE = TRAN_DATE;
    }

    public String getPAYEEACTIVATION_FLG() {
        return PAYEEACTIVATION_FLG;
    }

    public void setPAYEEACTIVATION_FLG(String PAYEEACTIVATION_FLG) {
        this.PAYEEACTIVATION_FLG = PAYEEACTIVATION_FLG;
    }


}

