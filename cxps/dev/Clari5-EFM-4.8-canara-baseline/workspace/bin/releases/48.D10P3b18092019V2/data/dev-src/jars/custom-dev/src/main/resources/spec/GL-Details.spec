cxps.noesis.glossary.entity.GL-Details {
	db-name = GL_Details
	generate = false
	db_column_quoted = true
	tablespace = CXPS_USERS
	attributes = [
                { name = main-head-seq-no, column = MAIN_HEAD_SEQ_NO, type = "number:10", key=false }
		{ name = main-head-desc, column = MAIN_HEAD_DESC, type = "string:20", key=false }
		{ name = sub-head-seq-no, column = SUB_HEAD_SEQ_NO, type = "number:10", key=false }
		{ name = sub-head-desc, column = SUB_HEAD_DESC, type = "string:20", key=false }
		{ name = leaf, column = LEAF, type = "string:20", key=true }
		{ name = rbi-code, column = RBI_CODE, type = "number:20", key=false }
		{ name = gl-head-seq-no, column = GL_HEAD_SEQ_NO, type = "number:10", key=false }
		{ name = bs-code, column = BS_CODE, type = "number:10", key=false }
		{ name = treasury-1499, column = TREASURY_1499, type = "string:50", key=false }
                { name = treasury-1197, column = TREASURY_1197, type = "string:50", key=false }
		{ name = v-gl-desc, column = V_GL_DESC, type = "string:80", key=false }
		]
}
