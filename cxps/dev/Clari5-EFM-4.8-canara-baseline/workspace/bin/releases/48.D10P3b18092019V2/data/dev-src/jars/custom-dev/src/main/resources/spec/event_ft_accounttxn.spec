cxps.events.event.ft_account_txn {
table-name : EVENT_FT_ACCOUNT_TXN
  event-mnemonic: FTA
  workspaces : {
    ACCOUNT : account-id
    CUSTOMER: cust-id
  }
  event-attributes : {

    entity-id:{type:"string:20",db:true,raw_name:EntityId}
    entity-type:{type:"string:20",db:true,raw_name:EntityType}
    event-type:{type:"string:20",db:true,raw_name:EventType}
    event-sub-type:{type:"string:20",db:true,raw_name:EventSubType}
    event-name:{type:"string:20",db:true,raw_name:event_name}
    host-id:{type:"string:20",db:true,raw_name:host_id}
    sys-time:{type:"timestamp",db:true,raw_name:sys_time}
    channel:{type:"string:20",db:true,raw_name:channel}
    account-id:{type:"string:20",db:true,raw_name:account_id}
    scheme-type:{type:"string:20",db:true,raw_name:schm_type}
    scheme-code:{type:"string:20",db:true,raw_name:schm_code}
    cust-id:{type:"string:20",db:true,raw_name:cust_id}
    acct-name:{type:"string:20",db:true,raw_name:acct_name}
    acct-sol-id:{type:"string:20",db:true,raw_name:acct_sol_id}
    acct-ownership:{type:"string:20",db:true,raw_name:acct_ownership}
    acct-open-date:{type:"timestamp",db:true,raw_name:acct_open_date}
    avl-bal:{type:"number:20,2",db:true,raw_name:avl_bal}
    clr-bal-amt:{type:"number:11,2",db:true,raw_name:clr_bal_amt}
    tran-type:{type:"string:20",db:true,raw_name:tran_type}
    tran-sub-type:{type:"string:20",db:true,raw_name:tran_sub_type}
    txn-dr-cr:{type:"string:20",db:true,raw_name:part_tran_type}
    tran_date:{type:"timestamp",db:true,raw_name:tran_date}
    txn-ref-id:{type:"string:20",db:true,raw_name:tran_id}
    tran-srl-no:{type:"string:20",db:true,raw_name:part_tran_srl_num}
    txn-amt:{type:"number:20,2",db:true,raw_name:txn_amt}
    tran-crncy-code:{type:"string:20",db:true,raw_name:tran_crncy_code}
    ref-tran-amt:{type:"number:11,2",db:true,raw_name:ref_txn_amt}
    ref-currency:{type:"string:20",db:true,raw_name:ref_tran_crncy}
    rate:{type:"number:11,2",db:true,raw_name:rate}
    tran-particular:{type:"string:20",db:true,raw_name:tran_particular}
    origin-ip-addr:{type:"string:20",db:true,raw_name:ip_address}
    pstd-flg:{type:"string:20",db:true,raw_name:pstd_flg}
    online-batch:{type:"string:20",db:true,raw_name:online_batch}
    pstd-user-id:{type:"string:20",db:true,raw_name:pstd_user_id}
    entry-user:{type:"string:20",db:true,raw_name:entry_user}
    cust-card-id:{type:"string:20",db:true,raw_name:cust_card_id}
    device-id:{type:"string:20",db:true,raw_name:device_id}
    contra-acct-id:{type:"string:20",db:true,raw_name:contra_account_id}
    contra-bank-code:{type:"string:20",db:true,raw_name:contra_bank_code}
    contra-sol-id:{type:"string:20",db:true,raw_name:contra_sol_id}
    tran-category:{type:"string:20",db:true,raw_name:tran_category}
    last-tran-date:{type:"timestamp",db:true,raw_name:last_tran_date}
    branch-id:{type:"string:20",db:true,raw_name:branch_id}
    value-date:{type:"timestamp",db:true,raw_name:value_date}
    instrmnt-date:{type:"timestamp",db:true,raw_name:instrmnt_date}
    instrmnt-num:{type:"string:20",db:true,raw_name:instrmnt_num}
    prev-bal:{type:"number:20,2",db:true,raw_name:prev_bal}
    acct-status:{type:"string:20",db:true,raw_name:Acct_Stat}
    txn-br-id:{type:"string:20",db:true,raw_name:txn_br_id}
    acct-sol-city:{type:"string:20",db:true,raw_name:acct_sol_city}
    txn-br-city:{type:"string:20",db:true,raw_name:txn_br_city}
    instrument-type:{type:"string:20",db:true,raw_name:instrmnt_type}
    mnemonic-code:{type:"string:20",db:true,raw_name:mnemonic_code}
    recon-id:{type:"string:20",db:true,raw_name:reconId}
    contra-custid:{type:"string:20",db:true,raw_name:contra_custid}
    contra-gl-flag:{type:"string:20",db:true,raw_name:contra_gl_flag}
    gl-flag:{type:"string:20",db:true,raw_name:gl_flag}
    ifsc-code:{type:"string:20",db:true,raw_name:ifsc_code}
    penal-interest-flag:{type:"string:20",db:true,raw_name:penal_interest_flag}
    staff-flag:{type:"string:20",db:true,raw_name:staff_flag}
    acct-closed-date:{type:"timestamp",db:true,raw_name:acct_closed_date}
    acct-maturity-date:{type:"timestamp",db:true,raw_name:acct_maturity_date}
    re-activation-date:{type:"timestamp",db:true,raw_name:re_activation_date}
    tran-code:{type:"string:20",db:true,raw_name:tran_code}
    dp-code:{type:"string:20",db:true,raw_name:dp_code}
    td-liquidation-flag:{type:"string:20",db:true,raw_name:td_liquidation_flg}
    add-entity_1:{type:"string:20",db:true,raw_name:addentity_1}
    add-entity_2:{type:"string:20",db:true,raw_name:addentity_2}
    add-entity_3:{type:"string:20",db:true,raw_name:addentity_3}
    add-entity_4:{type:"string:20",db:true,raw_name:addentity_4}
    add-entity_5:{type:"string:20",db:true,raw_name:addentity_5}
    add-entity_6:{type:"string:20",db:true,raw_name:addentity_6}
    add-entity_7:{type:"string:20",db:true,raw_name:addentity_7}
    add-entity_8:{type:"string:20",db:true,raw_name:addentity_8}
    add-entity_9:{type:"string:20",db:true,raw_name:addentity_9}
    add-entity_10:{type:"string:20",db:true,raw_name:addentity_10}
   }
}

