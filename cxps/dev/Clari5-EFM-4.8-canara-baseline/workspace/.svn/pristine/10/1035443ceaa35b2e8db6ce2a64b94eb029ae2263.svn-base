apply plugin: 'java'
	compileJava {
		options.fork = true
		options.encoding = 'utf-8'
		options.compilerArgs = ['-Xlint:unchecked','-Xlint:deprecation','-g']
	}

apply plugin: 'groovy'

dependencies {
	// compile 'org.codehaus.groovy:groovy-all:2.4.8'
}

clean {
	delete 'src/gen', 'src/test/gen', 'src/main/resources/gen_spec', 'gen', 'dsm/main/gen'
}

// Make sure that Mapper XML files are also copied
processResources {
	from('src/gen/java'){
		include '**/*.xml'
	}
	from('src/main/java'){
		include '**/*.xml'
	}
	//from('src/gen/groovy'){
	//	include '**/*.xml'
	//}
	//from('src/main/groovy'){
	//	include '**/*.xml'
	//}
}

// Make sure that Mapper XML files are also copied
processTestResources {
	from('src/test/gen/java'){
		include '**/*.xml'
	}
	//from('src/test/gen/groovy'){
	//	include '**/*.xml'
	//}
	from('src/test/java'){
		include '**/*.xml'
	}
	//from('src/test/groovy'){
	//	include '**/*.xml'
	//}
}

sourceSets {
	main {
		resources {
			srcDir 'src/main/resources'
			srcDir 'src/gen/resources'
		}
		java {
            srcDirs = [
                'src/main/java'
                , 'src/gen/java'
            ]
		}
		//groovy {
        //    srcDirs = [
        //        'src/main/java'
        //        , 'src/main/groovy'
        //        , 'src/gen/java'
        //        , 'src/gen/groovy'
        //    ]
		//}
	}
	test {
		resources {
			srcDir 'src/test/resources'
			srcDir 'src/test/gen/resources'
		}
		java {
            srcDirs = [
			    'src/test/java'
			    , 'src/test/gen/java'
            ]
		}
		//groovy {
        //    srcDirs = [
		//	    'src/test/java'
		//	    , 'src/test/gen/java'
		//	    , 'src/test/gen/groovy'
		//	    , 'src/test/groovy'
        //    ]
		//}
	}
}

apply plugin: 'maven'

configurations {
	deployerJars
}

dependencies {
	deployerJars "org.apache.maven.wagon:wagon-ssh:2.2"
}

uploadArchives {
	repositories.mavenDeployer {
		configuration = configurations.deployerJars
		repository(url: "scp://192.168.5.56/home/mvnrep/localrep") {
			authentication(userName: "mvnrep", password: "mvnrep")
		}
	}
}

apply plugin: 'maven-publish'

publishing {
	publications {
		mavenJava(MavenPublication){
			from components.java
		}
	}
	repositories {
		maven {
			url "$rootDir/repo"
		}
	}
}

task listJars {
    doLast {
        configurations.compile.each { File file -> println file.name }
    }
}

def getRevision() {
    // Derive svn version
    File versionFile = new File("${workspace}/.version")

    def svnversion = null
    if (versionFile.exists()) svnversion = versionFile.text

    if (!svnversion) {
        svnversion = "svnversion -c $projectDir".execute().in.text.trim().split(":")
        svnversion = (svnversion.length >1)? svnversion[1] : svnversion[0]
    }
    
    return svnversion
}

/*
 * --------------------------
 * Add manifest in jar file
 * --------------------------
 */

jar.doFirst {
    // Include fields in manifest
    manifest {
        attributes \
            'Built-By': System.getProperty('user.name'), \
            'Built-JDK': System.getProperty('java.version'), \
            'svn-version' : getRevision()
    }
}

/*
 * ---------------------------------------------------------
 * Function to populate map with internal and external jars
 * ---------------------------------------------------------
 */
def addDepsRecur (def dMap, def prd) {
    prd.children.each{
        addDepsRecur(dMap, it)
    }

    prd.moduleArtifacts.each {
        if (prd.moduleGroup.startsWith("clari5")) {
            dMap.int << ["${it.name}": it.file]
        } else {
            dMap.ext << it.file
        }
    }
}

/*
 * ---------------------------------------------------------------------------
 * This task copies all jar dependencies to DEPLOYMENT/lib folder
 * It also creates a file with jarlist in metaconf folder.
 * ---------------------------------------------------------------------------
 */
task copyDeps {
    doLast {
        String appName = project.name
        if (project.plugins.hasPlugin("war")) {
            appName = tasks.war.archiveName.replace('.war','')
        }

        File intLib = new File(installbase + "/rep/ilib")
        File extLib = new File(installbase + "/rep/elib")
        intLib.mkdirs()
        extLib.mkdirs()

        File conf = new File(installbase + "/meta")
        conf.mkdirs()

        File intList = new File(conf.path + "/" + appName + '-ijar.list')
        File extList = new File(conf.path + "/" + appName + '-ejar.list')
        intList.delete()
        extList.delete()
        intList.createNewFile()
        extList.createNewFile()

        def found_c3p0latest = false
        configurations.runtime { 
            if (it.name == "c3p0-0.9.5.jar") found_c3p0latest = true
        }

        // Dependency map
        def dMap = ["int" : [] as Set, "ext" : [] as Set]

        configurations.runtime.resolvedConfiguration.firstLevelModuleDependencies.each{
            addDepsRecur(dMap, it)
        }

        dMap.int.each { Map dep ->
            dep.each { k,v ->

                def toCopy = true
                File repFile = new File("${intLib.path}/${v.name}")
                if (repFile.exists()) {
                    ant.checksum file:v.path, property: "new_${v.name}"
                    ant.checksum file:repFile.path, property: "old_${v.name}"
                    if (ant.properties["new_${v.name}"] == ant.properties["old_${v.name}"])
                        toCopy = false
                }

                if (toCopy) {
                    copy {
                        into intLib.path
                        from v.path
                    }
                }
                // Get SVN version
                java.util.jar.Manifest mf = new java.util.jar.JarFile(v).getManifest()
                def revision = (mf != null)? mf.mainAttributes.getValue("svn-version") : ""
                intList.append("${v.name}|${revision}\n")
            }
        }

        dMap.ext.each { dep ->
            copy {
                into extLib.path
                from dep.path
                if (found_c3p0latest) exclude "c3p0-0.9.1.jar"
            }
            if(dep.name != 'c3p0-0.9.1.jar' || !found_c3p0latest){
                extList.append("${dep.name}\n")
            }
        }

        // Add the project jar as well
        File bld = new File("$buildDir/libs/" + project.name + '-' + project.version + '.jar');
        if (bld.exists()) {
            def toCopy = true
            File repFile = new File("${intLib.path}/${bld.name}")
            if (repFile.exists()) {
                ant.checksum file:bld.path, property: "new_${bld.name}"
                ant.checksum file:repFile.path, property: "old_${bld.name}"
                if (ant.properties["new_${bld.name}"] == ant.properties["old_${bld.name}"])
                    toCopy = false
            }

            if (toCopy) {
                copy {
                    into intLib.path
                    from bld.path
                }
            }
            
            java.util.jar.Manifest mf = new java.util.jar.JarFile(bld).getManifest()
            def revision = (mf != null)? mf.mainAttributes.getValue("svn-version") : ""
            intList.append("${bld.name}|${revision}\n")
        }
    }
}

/*
 * ---------------------------------------------------------------------------
 * This task creates a WAR file
 * ---------------------------------------------------------------------------
 */

task creWar {
    doLast {
        if (project.plugins.hasPlugin('war')) {

            String appName = tasks.war.archiveName.replace('.war','')

            tasks.war.rootSpec.exclude("**/*.jar")
            tasks.processResources.execute()
            tasks.classes.execute()
            tasks.war.execute()
            tasks.copyDeps.execute()
            tasks.copyConfs.execute()

            File archive = new File("$buildDir/libs/${tasks.war.archiveName}")
            File apps = new File("${installbase}/rep/apps/")
            apps.mkdirs()
            copy {
                from archive.path
                into apps.path
            }

            // Create a revision file
            File rev = new File("${installbase}/meta/${appName}.revision")
            rev.text = getRevision()
        }
    }
}

/*
 * ---------------------------------------------------------------------------
 * This task creates application.
 * It generates simple SHELL script in bbin folder.
 * It also copies dependencies using copyDeps in lib folder
 * ---------------------------------------------------------------------------
 */
task creApp(dependsOn: ['copyDeps','copyConfs']) {

    doLast {
        File bbin = new File(installbase + "/bbin")
        bbin.mkdirs()
        File binfile = new File(bbin.path + "/" + project.name)
        binfile.delete()

        // Create the main binary
        binfile.append('''#!/usr/bin/env bash
# -- GENERATED CODE --
debug(){
    [ -z "$DEBUG" ] || echo $*
}

cwd=`pwd`
cd $cwd
cd `dirname $0`
APPBIN=`pwd`
cd ..
DEP=`/bin/pwd -P`
REMOVE='/bin/rm'

debug "DEPLOYMENT=[$DEP]"
LIBDIR=$DEP/lib

# Cleanup
${REMOVE} -rf $DEP/rt/${app}

mkdir -p $DEP/rt/${app}/logs
mkdir -p $DEP/rt/${app}/conf
if [ -e ${DEP}/metaconf/${app}-conf.list ]; then
    for file in `cat ${DEP}/metaconf/${app}-conf.list`
    do
        cp ${DEP}/conf/${file} $DEP/rt/${app}/conf
    done
fi

# Create classpath
classPath="$DEP/rt/${app}/conf:$DEP/custom/conf"
if [ -e ${DEP}/metaconf/${app}-ijar.list ]; then
    for file in `cat ${DEP}/metaconf/${app}-ijar.list | cut -d '|' -f1`
    do
        classPath="$classPath:${DEP}/ilib/$file"
    done
fi
if [ -e ${DEP}/metaconf/${app}-ejar.list ]; then
    for file in `cat ${DEP}/metaconf/${app}-ejar.list`
    do
        classPath="$classPath:${DEP}/elib/$file"
    done
fi
debug "CLASSPATH=[$classPath]"

cd $DEP/rt/${app}/logs

export CXPS_DEPLOYMENT=${DEP}
exec > >(tee -a $DEP/rt/${app}/logs/sys.out)
exec 2> >(tee -a $DEP/rt/${app}/logs/sys.err)
debug "java -cp $classPath ${className} $*"
java ${JAVA_OPTS} -cp $classPath ${className} $*

'''.replace('${app}',project.name).replace('${className}', project.ext.mainClassName))

        binfile.setExecutable(true)
    }
}

/*
 * ---------------------------------------------------------------------------
 * This task creates tool.
 * It copies jar dependencies in tools/${project}/lib folder
 * It also copies scripts dependencies in tools/${project}/bin folder
 * ---------------------------------------------------------------------------
 */
task creTool(dependsOn : jar) {

    doLast {
        def name = project.name + "-" + project.version
        File toolD = new File(installbase + "/tools/" + name)
        toolD.mkdirs()
        delete toolD.path

        // Copy lib and project jar
        File lib = new File(toolD.path + "/lib")
        lib.mkdirs()
        copy {
            from project.projectDir.path + "/lib"
            from buildDir.path + "/libs"
            into lib.path
        }

        File base = new File(projectDir.path + "/BASE")
        if (base.exists()) {
            copy {
                from base.path
                into toolD.path
            }
        } else {
            // Copy bin
            File bin = new File(toolD.path + "/bin")
            bin.mkdirs()
            copy {
                from project.projectDir.path + "/bin"
                into bin.path
            }

            // Copy INSTALL
            File ins = new File(toolD.path + "/INSTALL")
            ins.mkdirs()
            copy {
                from project.projectDir.path + "/INSTALL"
                into ins.path
            }
        }

        // Copy all dependencies
        configurations.runtime.each { file ->
            copy {
                from file.path
                into lib.path
            }
        }
    }
}

task copyConfs {

    doLast {
        String appName = project.name
        if (project.plugins.hasPlugin("war")) {
            appName = tasks.war.archiveName.replace('.war','')
        }

        def depList = []
        def confList = []

        File mconf = new File(installbase + "/meta")
        File conf = new File(installbase + "/rep/conf")

        conf.mkdirs() 
        mconf.mkdirs()

        addProject(project, depList)
        depList.each { dep ->
            File dir = new File(dep.projectDir.path + "/src/main/conf")
            if (dir.exists()) {
                dir.traverse {
                    if (!confList.contains(it.name)) confList.add(it.name)
                }
                copy {
                    from dir.path
                    into conf.path
                }
            }
        }

        File pconf = new File(mconf.path + "/" + appName + "-conf.list" )
        pconf.delete()
        pconf.createNewFile()

        confList.each {
            if(!it.contains("template")) {
                pconf.append(it + "\n")
            }
        }

        // Copy meta conf into meta dir
        File metaconf = new File(projectDir.path + "/src/main/meta/${appName}-meta.conf")
        if (metaconf.exists()) {
            copy {
                from metaconf.path
                into mconf.path
            }
        }
    }
}

def addProject (def proj, def list) {
    proj.configurations.runtime.allDependencies.withType(ProjectDependency) {        
        addProject(it.dependencyProject, list)
    }
    if(!list.contains(proj)) list.add(proj)
}

// Tasks for controlling customization
task bundles {
    doLast {
        if (project.ext.wars) project.ext.wars.each { war -> println war }
    }
}
task revision {
    doLast {
        println getRevision()
    }
}
task getProjJars {
    doLast {
        String out = ""
        File f = new File("${buildDir}/copyDeps.txt")
        f.parentFile.mkdirs()
        configurations.runtime.resolvedConfiguration.firstLevelModuleDependencies.each { dep->
            dep.moduleArtifacts.each {
                if (dep.moduleGroup.startsWith("clari5")) {
                    out += it.file.name + "\n"
                }
            }
        }

        f.write(out[0..-2])
    }
}

