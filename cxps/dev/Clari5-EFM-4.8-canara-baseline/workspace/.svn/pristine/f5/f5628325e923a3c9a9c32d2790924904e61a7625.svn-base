<scenario app="EFM" desc="Alert is generated when a Credit card transaction (ATM/POS/Ecom) is observed in high risk country or high risk MCC or high-risk ATM machine or high risk Merchant ID" focus="FRAUD" group="EFM" insertFact="N" name="CC_83_CARD_TRN_HIGH_RISK_ENTITY" rdaActionReqd="N" solnType="SERVICE" workspace="Paymentcard">
<def-query collection="FT_CCCardTxnEvent" count="-1" countParam="_a2._q3.ftr.cnt" desc="FT_CCCardTxnEvent" durationEnd="1" durationStart="0" durationType="ABS" durationTypeParam="_a2._q3.ftr.dr_typ" endParam="_a2._q3.ftr.end" fromWS="Paymentcard" id="_q3" startParam="_a2._q3.ftr.st" timeUnit="D" unitParam="_a2._q3.ftr.tu">
<filter>
<or>
<and>
<cond>
<lhs extract="h" extractParam="_a2._q3.ftr.cond.or.0.and.0.dt.eventTS.extr">eventTS</lhs>
<op>GTE</op>
<rhs>23</rhs>
</cond>
<cond>
<lhs extract="m" extractParam="_a2._q3.ftr.cond.or.0.and.1.dt.eventTS.extr">eventTS</lhs>
<op>GTE</op>
<rhs>30</rhs>
</cond>
</and>
<cond>
<lhs extract="h" extractParam="_a2._q3.ftr.cond.or.1.dt.eventTS.extr">eventTS</lhs>
<op>LT</op>
<rhs>5</rhs>
</cond>
</or>
</filter>
<aggregate/>
</def-query>
<def-query collection="FT_CCCardTxnEvent" count="-1" countParam="_a1._q2.ftr.cnt" desc="FT_CCCardTxnEvent" durationEnd="1" durationStart="0" durationType="ABS" durationTypeParam="_a1._q2.ftr.dr_typ" endParam="_a1._q2.ftr.end" fromWS="Paymentcard" id="_q2" startParam="_a1._q2.ftr.st" timeUnit="D" unitParam="_a1._q2.ftr.tu">
<filter>
<and>
<cond lhs="partTranType" op="I_EQ" valParam="_a1._q2.ftr.cond.and.0.partTranType">"C"</cond>
<or>
<cond lhs="channel" op="I_EQ" valParam="_a1._q2.ftr.cond.and.1.or.0.channel">"ATM"</cond>
<cond lhs="channel" op="I_EQ" valParam="_a1._q2.ftr.cond.and.1.or.1.channel">"POS"</cond>
<cond lhs="channel" op="I_EQ" valParam="_a1._q2.ftr.cond.and.1.or.2.channel">"ECOM"</cond>
</or>
</and>
</filter>
<aggregate>
<sum over="txnAmount"/>
</aggregate>
</def-query>
<def-query collection="FT_CCCardTxnEvent" count="-1" countParam="_op._q1.ftr.cnt" desc="FT_CCCardTxnEvent" durationEnd="1" durationStart="0" durationType="ABS" durationTypeParam="_op._q1.ftr.dr_typ" endParam="_op._q1.ftr.end" fromWS="Paymentcard" id="_q1" startParam="_op._q1.ftr.st" timeUnit="D" unitParam="_op._q1.ftr.tu">
<filter>
<and>
<cond lhs="partTranType" op="I_EQ" valParam="_op._q1.ftr.cond.and.0.partTranType">"C"</cond>
<or>
<cond lhs="channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.0.channel">"ATM"</cond>
<cond lhs="channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.1.channel">"POS"</cond>
<cond lhs="channel" op="I_EQ" valParam="_op._q1.ftr.cond.and.1.or.2.channel">"ECOM"</cond>
</or>
</and>
</filter>
<aggregate>
<sum over="txnAmount"/>
</aggregate>
</def-query>
<def-opinion duration="7D" durationCountParam="_op.life.cnt" durationUnitParam="_op.life.tu" intuWt="1.0" intuition="CC_83_CARD_TRN_HIGH_RISK_ENTITY_INTU" wt="0.4" wtParam="_op.wt">
<condition>
<and>
<query id="_q1">
<cond lhs="count" op="GT" valParam="_op.mc.and.0._q1.count">0</cond>
</query>
<or>
<view fromWS="Paymentcard" id="__HIGH_RISKY_TBL_ATM" name="HIGH_RISKY_TBL_ATM"/>
<view fromWS="Paymentcard" id="__HIGH_RISKY_TBL_MERCHANT" name="HIGH_RISKY_TBL_MERCHANT"/>
<view fromWS="Paymentcard" id="__HIGH_RISK_COUNTRY" name="HIGH_RISK_COUNTRY"/>
<view fromWS="Paymentcard" id="__HIGH_RISK_MCCs" name="HIGH_RISK_MCCs"/>
</or>
</and>
</condition>
<messages>
<msg channelId="BRANCH">Credit Card $WSKEY$ transaction observed in high risk Country or MCC or ATM for Amount $AMOUNT$ </msg>
</messages>
<evidence-data>
<field name="CreditCardNo" type="String" value="_q1.last.maskCardNo"/>
<field name="AMOUNT" type="Integer" value="_q1.sum(txnAmount)"/>
</evidence-data>
</def-opinion>
<def-addOn desc="Up to X% of daily card limit" name="_a1" wt="0.15" wtParam="_a1.wt">
<condition>
<and>
<view fromWS="Paymentcard" id="__CARD_LIMIT_DAILY" name="CARD_LIMIT_DAILY"/>
<query id="_q2">
<or>
<cond constParam="_a1.mc.and.1._q2.or.0.sum(txnAmount).const" lhs="sum(txnAmount)" multParam="_a1.mc.and.1._q2.or.0.sum(txnAmount).mult" op="GTE">0.8*__CARD_LIMIT_DAILY.ATM_LIMIT+0</cond>
<cond constParam="_a1.mc.and.1._q2.or.1.sum(txnAmount).const" lhs="sum(txnAmount)" multParam="_a1.mc.and.1._q2.or.1.sum(txnAmount).mult" op="GTE">0.8*__CARD_LIMIT_DAILY.POS_LIMIT+0</cond>
<cond constParam="_a1.mc.and.1._q2.or.2.sum(txnAmount).const" lhs="sum(txnAmount)" multParam="_a1.mc.and.1._q2.or.2.sum(txnAmount).mult" op="GTE">0.8*__CARD_LIMIT_DAILY.ECOM_LIMIT+0</cond>
</or>
</query>
</and>
</condition>
<messages>
<msg channelId="BRANCH">UPTO 80 PERCENT OF DAILY LIMIT </msg>
</messages>
</def-addOn>
<def-addOn desc="Odd Hour  11:30 pm to 5:00 am" name="_a2" wt="0.15" wtParam="_a2.wt">
<condition>
<query id="_q3">
<cond lhs="count" op="GT" valParam="_a2.mc._q3.count">0</cond>
</query>
</condition>
<messages>
<msg channelId="BRANCH">ODD HOUR BETWEEN 11 30 PM TO 5 AM </msg>
</messages>
</def-addOn>
</scenario>
