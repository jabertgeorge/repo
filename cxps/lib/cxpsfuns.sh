#!/usr/bin/env bash

unset shrink 2>/dev/null
shrink(){
	if [ $# -eq 0 ] ; then
		___VARNAME=PATH
	else
		___VARNAME=$1
	fi
	___VAR="\${`echo $___VARNAME`}"
	___VARVALUE=`eval echo $___VAR`
	___NEWVALUE=""
	for dir in `echo $___VARVALUE | tr ' ' '@' | tr ':' '\012'`
	do
		if ! echo $___NEWVALUE | ${GREP} -q ":${dir}:"
		then
			___NEWVALUE=${___NEWVALUE}:${dir}:
		fi
	done
	# Remove consecutive '::' and substitute spaces back
	___NEWVALUE=`echo $___NEWVALUE | ${SED} -e 's/::/:/g' -e 's/@/ /g'`

	___VAR="`echo $___VARNAME`='$___NEWVALUE'"
	# echo $___VAR
	eval $___VAR
}

unset f 2>/dev/null
unalias f 2>/dev/null
f(){
	if [ $# -eq 0 ] ; then
		echo "Error in usage - f() is equivalent to find \$*| ${GREP} -v .svn"
	else
		find $* | ${GREP} -v -e '.svn'
	fi
}

unset upg 2>/dev/null
unalias upg 2>/dev/null
upg(){
	if [ "$1" = "reset" ] ; then
		_lastUpged=0
		shift 1
	fi
	if [ -z "$_lastUpged" ] ; then
		bash cxupg.sh $*
		_lastUpged=`date +%s`
	else
		_x=`date +%s`
		let _xdiff=_x-_lastUpged
		if [ $_xdiff -gt 300 ] ; then
			bash cxupg.sh $*
			_lastUpged=`date +%s`
		else
			echo "Skipping ... b'cos I checked $_xdiff/300 seconds ago. To force upgrade do 'upg reset'" >&2
		fi
	fi
}

unset sw 2>/dev/null
sw(){
	cwd=`pwd`

	# Upgrade the devenv in case new is available
	upg patch 2>/dev/null

	# Check if particular revision is requested
	SVN_REVISION=""
	if echo "$1" | grep -q 'rev='
	then
		SVN_REVISION="`echo $1 | cut -f2 -d'='`"
		shift 1
	fi

	# Main code
	if [ $# -eq 2 ] ; then
		PRODUCT=$1
		VERSION=$2
	else
		TMPFILE=/tmp/cxlister.tmpfile
		\rm -f $TMPFILE
		while :
		do
			python ${CXPS_LIB}/cxlister.py $1 $TMPFILE
			___sts=$?
			if [ $___sts -eq 3 ] ; then
				if isLocalSVN
				then
					echo "Switching to Remote SVN." && swRemoteSVN
				else
					echo "Switching to Local SVN." && swLocalSVN
				fi
			elif [ $___sts -eq 2 ] ; then
				if isLocalSVN
				then
					printf "Are you working remotely [y/n] ? "
					read ans
					if [ "$ans" = "y" -o "$ans" = "Y" ] ; then
						echo "Switching to Remote SVN" && swRemoteSVN
					fi
				else
					printf "You are working remotely. Switch to local [y/n] ? "
					if [ "$ans" = "y" -o "$ans" = "Y" ] ; then
						echo "Switching to Local SVN" && swLocalSVN
					fi
				fi
			else
				break
			fi
		done

		[ $___sts -ne 0 ] && return 1
		[ -f $TMPFILE ] && eval `cat $TMPFILE` && \rm -f $TMPFILE
		# This will output PRODUCT, VERSION and MODDIR
	fi

	# Check if revision is embedded in VERSION
	if echo $VERSION | ${GREP} -q -- '-r[0-9][0-9]*$'
	then
		SVN_REVISION=`echo $VERSION | ${SED} 's/^.*-r\([0-9][0-9]*\)$/\1/'`
		VERSION=`echo $VERSION | ${SED} 's/^\(.*\)-r[0-9][0-9]*$/\1/'`
	fi
	# echo $SVN_REVISION

	if [ -z "$VERSION" -a -z "${SVN_REVISION}" ] ; then
		PRODVER=$PRODUCT
	elif [ -z "$SVN_REVISION" ] ; then
		PRODVER=${PRODUCT}-${VERSION}
	elif [ -z "$VERSION" ] ; then
		PRODVER=${PRODUCT}-r${SVN_REVISION}
	else
		PRODVER=${PRODUCT}-${VERSION}-r${SVN_REVISION}
	fi

	# Remove all folders having the previous product from PATH
	if [ ! -z "$CXPS_PRODUCT" ] ; then
		shrink
		PATH=`echo $PATH | tr ':' '\012' | ${GREP} -v "/${CXPS_PRODUCT}/" | ${GREP} -v '^[ 	]*$' | tr '\012' ':'`
	fi

	# Set the main variables
	export CXPS_PRODUCT_NAME=${PRODUCT}
	export CXPS_PRODUCT_VERSION=${VERSION}
	export CXPS_PRODUCT_SVN_REVISION=${SVN_REVISION}
	export CXPS_MODDIR=${MODDIR}
	export CXPS_PRODUCT=${PRODVER}
	export CXPS_WORKSPACE=${CXPS_DEV}/${CXPS_PRODUCT}/workspace
	export CXPS_DEPLOYMENT=${CXPS_DEV}/${CXPS_PRODUCT}/deployment

	# In case the product is not available, set up the product as well.
	if [ ! -d "${CXPS_DEV}/${PRODVER}" ] ; then
		echo "Executing prodsetup.sh ..."
		# echo ${CXPS_BIN}/prodsetup.sh ${PRODUCT} ${VERSION} ${SVN_REVISION}
		${CXPS_BIN}/prodsetup.sh "${PRODUCT}" "${VERSION}" "${SVN_REVISION}" "${MODDIR}"
	fi

	# Run the profile for the product
	if [ -f ${CXPS_WORKSPACE}/bin/profile.sh ] ; then
		echo "Executing ${CXPS_WORKSPACE}/bin/profile.sh ..."
		chmod +x ${CXPS_WORKSPACE}/bin/profile.sh
		. ${CXPS_WORKSPACE}/bin/profile.sh
	fi

	shrink

	svnsw

	cd $cwd
}

setdbtypes(){

	DEPFILE=$1
	[ -z "$DEPFILE" ] && DEPFILE=$CXPS_WORKSPACE/bin/deps.cfg
	[ ! -f $DEPFILE ] && echo "setdbtypes:Invalid dep file $DEPFILE" && return 1

	count=`${GREP} -e mysql -e oracle -e postgresql -e sqlserver $DEPFILE | wc -l`
	if [ $count -eq 0 ] ; then
		echo "This product does not have any Database dependencies" >&2
		export DB_TYPES=""
		return 0
	elif [ $count -eq 1 ] ; then
		if ${GREP} -q -e mysql $DEPFILE
		then
			export DB_TYPES=mysql
		elif ${GREP} -q -e oracleXE $DEPFILE ; then
			export DB_TYPES=oracleXE
		elif ${GREP} -q -e sqlserver $DEPFILE ; then
			export DB_TYPES=sqlserver
		elif ${GREP} -q -e postgresql $DEPFILE ; then
			export DB_TYPES=postgresql
		else
			export DB_TYPES=oracle
		fi
		return 1
	else
		export DB_TYPES="mysql oracle oracleXE postgresql sqlserver"
		return 2
	fi
	DB_TYPES=`echo $DB_TYPES | ${SED} 's/[ 	][ 	]*/ /g'`
}

sdb(){

    export DB_TYPE

	dbpref=$1
	setdbtypes
	__status=$?
	__done=1

	if [ $__status -eq 1 ] ; then
		export DB_TYPE=$DB_TYPES
		__done=0
	fi

	if [ ! -z "$dbpref" ] ; then
		if echo $DB_TYPES | ${GREP} -q -w $dbpref
		then
			export DB_TYPE=$dbpref
			__done=0
		else
			echo "Invalid db preference $dbpref"
			return 1
		fi
	fi

	if [ $__done -eq 1 -a $__status -eq 2 ] ; then

		echo "Current DB_TYPE=$DB_TYPE"

		while :
		do
			let i=1
			for db_type in `echo $DB_TYPES`
			do
				out="$i) $db_type"
				echo $out
				let i=i+1
			done
			echo "------------"
			echo "q) quit"
			printf "Enter Choice: "
			read ans
			[ "$ans" = "q" ] && echo "Database is $DB_TYPE" && return 2
			[ -z "$ans" ] && continue

			if echo "$ans" | ${GREP} -q '^[0-9][0-9]*$'
			then
				if [ $ans -le `echo $DB_TYPES | wc -w` ] ; then
					DB_TYPE=`echo $DB_TYPES | cut -f$ans -d' '`
					break
				fi
			fi
			echo "Wrong Choice!"
		done
	fi

	echo "Database is $DB_TYPE"
	if [ -f ${CXPS_WORKSPACE}/bin/devvars.sh ] ; then
		. ${CXPS_WORKSPACE}/bin/devvars.sh
	elif [ -f ${CXPS_WORKSPACE}/bin/depvars.sh ] ; then
		. ${CXPS_WORKSPACE}/bin/depvars.sh
	fi
	return 0
}

# This function checks if the current WORKSPACE folder is in sync with the SVN.
# If not, it will relocate the SVN folder else it just comes out.

svnsw()
{
	local cwd curIP svnIP suffix prefix fromURL toURL
	cwd=`pwd`
	cd $CXPS_WORKSPACE
	curIP=`svn info | grep ^URL | cut -f3 -d'/' | cut -f1 -d:`
	svnIP=`echo $GLB_SVN_URL | cut -f3 -d'/' | cut -f1 -d:`
	if [ "$curIP" != "$svnIP" ] ; then
		suffix=`svn info | grep ^URL | cut -f2- -d':' | cut -f4- -d'/'`
		if isLocalSVN
		then
			# Move from remote to local
			prefix=`echo $GLB_LOCAL_SVN_URL | cut -f1-3 -d'/'`
            echo "Relocating [remote -> local]. This may take few moments ..."
		else
			# Move from local to remote
			prefix=`echo $GLB_REMOTE_SVN_URL | cut -f1-3 -d'/'`
            echo "Relocating [local -> remote]. This may take few moments ..."
		fi
		fromURL=$( svn info | grep ^URL | cut -c6- | cut -f-3 -d'/' )
		toURL=${prefix}

		printf "Continue [y/n] : "
		read ans
		if [ "$ans" = "y" ] ; then
			printf "Executing [svn relocate $fromURL $toURL] ... "
			svn relocate ${fromURL} ${toURL}
			curIP=`svn info | grep ^URL | cut -f3 -d'/' | cut -f1 -d:`
			if [ "$curIP" != "$svnIP" ] ; then
				echo " failed!"
				echo "Please run this command directly from \$CXPS_WORKSPACE."
			else
				echo " done!"
			fi
		fi
	fi
	cd $cwd
}

# Private function
# This function reads a variable with default value

unset __readVar 2>/dev/null
unalias __readVar 2>/dev/null
__readVar(){
    while :
    do
        printf "$1 [$2] : " >&2
        read __fl
        if [ -z "$__fl" -a ! -z "$2" ] ; then
            ret=$2
            break
        elif [ ! -z "$__fl" ] ; then
            ret=$__fl
            break
        fi
    done
    echo $ret | tr -d '"' | tr '|' '\;'
}

# Private function
# This function reads a description

unset __readDesc 2>/dev/null
unalias __readDesc 2>/dev/null
__readDesc(){
    echo "$1 (put 'EOT' to end) : " >&2
    ret=""
    while :
    do
        read __fl
        [ "$__fl" = "EOT" ] && break
        ret="$ret $__fl"
    done
    echo $ret | tr -d '"' | tr '|' '\;'
}

# This function is used to commit in the right way
unset commit 2>/dev/null
unalias commit 2>/dev/null
commit () {

    local _id _rev _dev _msg _tag _args
    while :
    do
        case $1 in
            "-c"|"-m"|"--message"|"--comment")
                shift 1
                _msg="$1"
                shift 1
                ;;
            "-i"|"--issue")
                shift 1
                _id="$1"
                shift 1
                ;;
            "-u"|"-d"|"--user"|"--developer")
                shift 1
                _dev="$1"
                shift 1
                ;;
            "-r"|"--reviewer")
                shift 1
                _rev="$1"
                shift 1
                ;;
            *)
                _args=( ${_args[@]} $1 )
                shift 1
                ;;
        esac

        [ $# -eq 0 ] && break
    done

    ( [ -z "${_id}" ] || [ -z "${_msg}" ] ) \
        && echo "Usage: commit -i <issue> [-d|-u <developer>] -c|-m <comment> [-r <reviewer>]" \
        && return 1

    [ -z "${_dev}" ] && _dev=${USER}
    _tag="ISSUE-ID:${_id} | Comment: ${_msg} | Developer: ${_dev}"
    [ ! -z "${_rev}" ] && _tag="${_tag} | Reviewer: ${_rev}"

    svn commit -m "${_tag}" ${_args[@]}
}

# Public function
# This function allows one to find the difference between svn version
# for a given file.
unset dff 2>/dev/null
unalias dff 2>/dev/null
dff(){
	case $# in
		1) __dff_r1='-0'; __dff_r2='-1';;
		2) __dff_r1='-0'; __dff_r2="$1"; shift 1;;
		3) __dff_r1="$1"; __dff_r2="$2"; shift 2;;
		*) echo "Usage: dff [-<r1>][-<r2>] file" >&2 ; return ;;
	esac
	__dff_valid=0
	if echo $__dff_r1 | grep '^-[0-9][0-9]*$' >/dev/null
	then
		if echo $__dff_r2 | grep '^-[0-9][0-9]*$' >/dev/null
		then
			__dff_r1=`echo $__dff_r1 | cut -f2 -d-`
			__dff_r2=`echo $__dff_r2 | cut -f2 -d-`
			__dff_valid=1
		fi
	fi
	[ $__dff_valid -eq 0 ] && echo "Invalid rev. Use -0 for current, -1 for previous, etc." >&2 && return

	__dff_r1v=`svn log -l$__dff_r1 $1 2>/dev/null | grep '^r[0-9][0-9]' | tail -1 | ${SED} 's/^.*r\([0-9][0-9]*\).*$/\1/'`
	__dff_r2v=`svn log -l$__dff_r2 $1 2>/dev/null | grep '^r[0-9][0-9]' | tail -1 | ${SED} 's/^.*r\([0-9][0-9]*\).*$/\1/'`

	if [ -z "$__dff_r1v" -a -z "$__dff_r2v" ] ; then
		echo "Invalid file $1" >&2
	elif [ -z "$__dff_r1v" ] ; then
		#echo svn diff -r${__dff_r2v} $1
		svn diff -r${__dff_r2v} $1
	elif [ -z "$__dff_r2v" ] ; then
		#echo svn diff -r${__dff_r1v} $1
		svn diff -r${__dff_r1v} $1
	else
		#echo svn diff -r${__dff_r1v}:${__dff_r2v} $1
		svn diff -r${__dff_r1v}:${__dff_r2v} $1
	fi
}
