#!/usr/bin/env python

import os, time, sys
import getpass, requests, builtins

from builtins import input

if sys.version_info[0] == 2:
    import cPickle as cPK
else :
    import _pickle as cPK

import cxCache

class SVNFetch :

	def __init__(self, force=False) :
		self.FORCE = force
		self.cache = cxCache.cxCache('svnreg')	# Name of the cache file
		if self.FORCE :
			self.user = None
			self.pswd = None
		else :
			m = self.cache.get(['user','pswd'])
			self.user = m.get('user',None)
			if self.user :
			    self.user = self.user.decode("utf-8")
			self.pswd = m.get('pswd',None)
			if self.pswd :
			    self.pswd = self.pswd.decode("utf-8")
		self.userPwChanged = True

	# This method inputs the User / Password from the User.
	def getUserPw(self) :
		if self.user == None :
			self.userPwChanged = True
			userid = getpass.getuser()
			self.user = input("SVN user-name ["+userid+"]: ").decode("utf-8")
			if self.user == "" :
				self.user = userid
				self.pswd = None
		if self.user != None and self.pswd == None :
			self.userPwChanged = True
			self.user = self.user.decode("utf-8")
			self.pswd = getpass.getpass('SVN Password ['+self.user+']: ')

	# This method fetches the given folder from SVN
	def fetch(self, folder = '.') :

		if not self.FORCE :
			m = self.cache.get([folder])
			if folder in m and m[folder] != None :
				(ts,folders) = cPK.loads(m[folder])
				# Check if 5 mins have not elapsed
				if time.time() - ts <= 300 :
					return ('OK',folders)

		url = os.environ['GLB_SVN_URL']
		if folder != '.' :
			url += '/' + folder
		try :
			# print "url=[%s]" % (url)
			# HTTP(S) request with authorization/timeout and no SSL verification
			r = requests.get(url, auth=(self.user, self.pswd), timeout=2, verify=False)
		except requests.exceptions.Timeout :
			return ('TIMEOUT',None)
		except requests.packages.urllib3.exceptions.LocationParseError :
			return ('URLError',[url])
		except requests.exceptions.ConnectionError :
			return ('CONNECTError',None)

		if r.status_code == 200 :	# Success!
			folders = []
			for line in r.text.split('\n') :
				if line.find('<li><a href="') >= 0 :
					folders.append(line.split('"')[1].replace('/',''))
			# In case userPw was changed, it should be saved
			if self.userPwChanged :
				self.cache.put({ 'user' : self.user, 'pswd' : self.pswd })
				self.userPwChanged = False
			self.cache.put({ folder : cPK.dumps((time.time(), folders), protocol=2) })
			return ('OK',folders)
		elif r.status_code == 401 :	# Wrong Password!
			self.user = None
			self.pswd = None
			return ('INVALID_USPW',None)
		else :
			return ('ERROR',[r.status_code])

	# This method fetches module details and returns
	# - releases
	# - branches
	# - module-baseline
	def fetchModule(self, module) :
		(status,folders) = self.fetch(module)
		if status != 'OK' : return (status,None)
		if 'baseline' not in folders :
			return ('OK_BUT',None)
		moddirs = ['/baseline']
		for folder in ['releases','branches']:
			if folder in folders :
				(status,dirs) = self.fetch(module + '/' + folder)
				if status == 'OK' :
					dirs.remove('..')
					for d in dirs :
						moddirs.append( folder + '/' + d)
		return (status, moddirs)
